﻿namespace TCPIPServer
{
    partial class frmServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ColumnHeader GPS;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmServer));
            this.Acction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelMyIP = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.ClientIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Computer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Version = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ConnectionID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ClientName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PingTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Battery = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CommunicationsDisplay = new System.Windows.Forms.WebBrowser();
            this.label1 = new System.Windows.Forms.Label();
            this.hub = new System.Windows.Forms.TextBox();
            this._btnCN = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this._btnDCN = new System.Windows.Forms.Button();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.lbPort = new System.Windows.Forms.Label();
            this.lbConnect = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            GPS = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // GPS
            // 
            GPS.Text = "GPS";
            GPS.Width = 70;
            // 
            // Acction
            // 
            this.Acction.Text = "Acction";
            // 
            // labelMyIP
            // 
            this.labelMyIP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMyIP.AutoSize = true;
            this.labelMyIP.ForeColor = System.Drawing.Color.LightYellow;
            this.labelMyIP.Location = new System.Drawing.Point(17, 195);
            this.labelMyIP.Name = "labelMyIP";
            this.labelMyIP.Size = new System.Drawing.Size(34, 13);
            this.labelMyIP.TabIndex = 68;
            this.labelMyIP.Text = "My IP";
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ClientIP,
            this.Acction,
            this.Computer,
            this.Version,
            this.ConnectionID,
            this.ClientName,
            this.PingTime,
            GPS,
            this.Battery,
            this.Status});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 12);
            this.listView1.Name = "listView1";
            this.listView1.ShowItemToolTips = true;
            this.listView1.Size = new System.Drawing.Size(924, 174);
            this.listView1.TabIndex = 67;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // ClientIP
            // 
            this.ClientIP.Text = "Client IP";
            this.ClientIP.Width = 136;
            // 
            // Computer
            // 
            this.Computer.Text = "Computer";
            this.Computer.Width = 117;
            // 
            // Version
            // 
            this.Version.Text = "IMEI";
            this.Version.Width = 120;
            // 
            // ConnectionID
            // 
            this.ConnectionID.Text = "ClientID";
            this.ConnectionID.Width = 70;
            // 
            // ClientName
            // 
            this.ClientName.Text = "Name";
            this.ClientName.Width = 80;
            // 
            // PingTime
            // 
            this.PingTime.Text = "Ping Time";
            this.PingTime.Width = 120;
            // 
            // Battery
            // 
            this.Battery.Text = "Battery";
            // 
            // Status
            // 
            this.Status.Text = "Status";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.CommunicationsDisplay);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.LightYellow;
            this.groupBox2.Location = new System.Drawing.Point(12, 222);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(927, 307);
            this.groupBox2.TabIndex = 66;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Communication Events";
            // 
            // CommunicationsDisplay
            // 
            this.CommunicationsDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CommunicationsDisplay.Location = new System.Drawing.Point(3, 17);
            this.CommunicationsDisplay.MinimumSize = new System.Drawing.Size(20, 20);
            this.CommunicationsDisplay.Name = "CommunicationsDisplay";
            this.CommunicationsDisplay.Size = new System.Drawing.Size(921, 287);
            this.CommunicationsDisplay.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(390, 198);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 70;
            this.label1.Text = "Hub:";
            // 
            // hub
            // 
            this.hub.Enabled = false;
            this.hub.Location = new System.Drawing.Point(426, 195);
            this.hub.Name = "hub";
            this.hub.Size = new System.Drawing.Size(267, 20);
            this.hub.TabIndex = 69;
            // 
            // _btnCN
            // 
            this._btnCN.Location = new System.Drawing.Point(842, 193);
            this._btnCN.Name = "_btnCN";
            this._btnCN.Size = new System.Drawing.Size(75, 23);
            this._btnCN.TabIndex = 71;
            this._btnCN.Text = "Conection";
            this._btnCN.UseVisualStyleBackColor = true;
            this._btnCN.Click += new System.EventHandler(this._btnCN_ClickAsync);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this._timer_TickAsync);
            // 
            // _btnDCN
            // 
            this._btnDCN.Enabled = false;
            this._btnDCN.Location = new System.Drawing.Point(842, 193);
            this._btnDCN.Name = "_btnDCN";
            this._btnDCN.Size = new System.Drawing.Size(75, 23);
            this._btnDCN.TabIndex = 72;
            this._btnDCN.Text = "Disconnect";
            this._btnDCN.UseVisualStyleBackColor = true;
            this._btnDCN.Click += new System.EventHandler(this._btnDCN_Click);
            // 
            // txtPort
            // 
            this.txtPort.Enabled = false;
            this.txtPort.Location = new System.Drawing.Point(347, 195);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(37, 20);
            this.txtPort.TabIndex = 73;
            // 
            // lbPort
            // 
            this.lbPort.Location = new System.Drawing.Point(311, 198);
            this.lbPort.Name = "lbPort";
            this.lbPort.Size = new System.Drawing.Size(30, 13);
            this.lbPort.TabIndex = 74;
            this.lbPort.Text = "Port:";
            // 
            // lbConnect
            // 
            this.lbConnect.ForeColor = System.Drawing.Color.Red;
            this.lbConnect.Location = new System.Drawing.Point(747, 198);
            this.lbConnect.Name = "lbConnect";
            this.lbConnect.Size = new System.Drawing.Size(75, 16);
            this.lbConnect.TabIndex = 75;
            this.lbConnect.Text = "Disconnected";
            // 
            // lbStatus
            // 
            this.lbStatus.Location = new System.Drawing.Point(699, 198);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(42, 16);
            this.lbStatus.TabIndex = 76;
            this.lbStatus.Text = "Status:";
            // 
            // frmServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(951, 541);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.lbConnect);
            this.Controls.Add(this.lbPort);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.hub);
            this.Controls.Add(this.labelMyIP);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this._btnCN);
            this.Controls.Add(this._btnDCN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmServer";
            this.Text = "Server App - TN Mobike";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmServer_FormClosing);
            this.Load += new System.EventHandler(this.frmServer_Load);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMyIP;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader ClientIP;
        private System.Windows.Forms.ColumnHeader Computer;
        private System.Windows.Forms.ColumnHeader Version;
        private System.Windows.Forms.ColumnHeader ConnectionID;
        private System.Windows.Forms.ColumnHeader ClientName;
        private System.Windows.Forms.ColumnHeader PingTime;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.WebBrowser CommunicationsDisplay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _btnCN;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox hub;
        public System.Windows.Forms.ColumnHeader Battery;
        private System.Windows.Forms.ColumnHeader Acction;
        private System.Windows.Forms.ColumnHeader Status;
        private System.Windows.Forms.Button _btnDCN;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label lbPort;
        private System.Windows.Forms.Label lbConnect;
        private System.Windows.Forms.Label lbStatus;
    }
}

