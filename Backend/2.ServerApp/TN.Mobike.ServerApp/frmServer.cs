﻿using CommonClassLibs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCPServer;
using static TCPServer.Server;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using System.Data.SqlClient;
using Dapper;
using System.Configuration;
using Microsoft.AspNetCore.Http.Connections;
using TN.Mobike.ServerApp;
using TN.Mobike.ServerApp.Code;
using TN.Mobike.ServerApp.Entity;
using Quartz.Util;

namespace TCPIPServer
{
    public partial class frmServer : Form
    {
        byte[] login_imei = null;

        public enum INK
        {
            CLR_BLACK = 0,
            CLR_RED = 1,
            CLR_BLUE = 2,
            CLR_GREEN = 3,
            CLR_PURPLE = 4
        }
        public enum PROTOCOL_NUMBER
        {
            LOGIN_INFORMATION = 0x01,
            HEARTBEAT_PACKET = 0x23,
            ONLINE_COMMAND_RESPONSE_BY_TERMINAL = 0x21,
            GPS_LOCATION_INFORMATION = 0x32,
            LOCATION_INFORMATION_ALARM = 0x33,
            ONLINECOMMAND = 0x80,
            INFORMATION_TRANSMISSION_PACKET = 0x98,
            NONE
        }
        private bool ValidBrowser = false;
        private bool displayReady = false;

        bool ServerIsExiting = false;
        int MyPort = new AppSetting().PortService;

        /*******************************************************/

        /// <summary>
        /// TCPiP server
        /// </summary>
        Server svr = null;

        private Dictionary<int, MotherOfRawPackets> dClientRawPacketList = null;
        private Queue<FullPacket> FullPacketList = null;
        static AutoResetEvent autoEvent;//mutex
        static AutoResetEvent autoEvent2;//mutex
        private Thread DataProcessThread = null;
        private Thread FullPacketDataProcessThread = null;
        private bool isRetry = false;
        /*******************************************************/
        System.Timers.Timer timerGarbagePatrol = null;
        System.Timers.Timer timerPing = null;

        public frmServer()
        {
            InitializeComponent();



            listView1.DoubleBuffered(true);
            listView1.FullRowSelect = true;
            ListViewExtender extender = new ListViewExtender(listView1);
            // extend 2nd column
            ListViewButtonColumn buttonAction = new ListViewButtonColumn(1);
            buttonAction.Click += OnButtonActionClick;
            //buttonAction.FixedWidth = true;
            extender.AddColumn(buttonAction);
            hub.Text = new AppSetting().HubConnect;
            txtPort.Text = new AppSetting().PortService.ToString();
            InitHub();

            _connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                isRetry = true;
                OnCommunications($"Closed connection to hub: {hub.Text}", INK.CLR_RED);



            };
        }




        private void frmServer_Load(object sender, EventArgs e)
        {
            /**********************************************/
            // Init the communications window so we cann whats going on
            ValidBrowser = BrowserVersion();
            // Setup data monitor
            CommunicationsDisplay.Navigate("about:blank");

            OnCommunications($"Loading...", INK.CLR_BLUE);
            /**********************************************/

            /**********************************************/
            //Create a directory we can write stuff too
            CheckOnApplicationDirectory();
            /**********************************************/

            /**********************************************/
            //Start listening for TCPIP client connections
            StartPacketCommunicationsServiceThread();
            /**********************************************/

            /********************************************************/
            // Create some timers for maintenence
            timerPing = new System.Timers.Timer();
            timerPing.Interval = 240000;// 4 minute ping timer
            timerPing.Enabled = true;
            timerPing.Elapsed += timerPing_Elapsed;

            timerGarbagePatrol = new System.Timers.Timer();
            timerGarbagePatrol.Interval = 600000; // 5 minute connection integrity patrol
            timerGarbagePatrol.Enabled = true;
            timerGarbagePatrol.Elapsed += timerGarbagePatrol_Elapsed;

            //timerUnlock = new System.Timers.Timer();
            //timerUnlock.Interval = 60000; // 1 minute unlock timer
            //timerUnlock.Enabled = true;
            //timerUnlock.Elapsed += timerUnlock_Elapsed;
            /********************************************************/

            // enumerate my IP's
            SetHostNameAndAddress();
        }
        private void frmServer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (svr != null)
            {
                PACKET_DATA xdata = new PACKET_DATA();

                xdata.Packet_Type = (UInt16)PACKETTYPES.TYPE_HostExiting;
                xdata.Data_Type = 0;
                xdata.Packet_Size = 16;
                xdata.maskTo = 0;
                xdata.idTo = 0;
                xdata.idFrom = 0;

                byte[] byData = PACKET_FUNCTIONS.StructureToByteArray(xdata);

                svr.SendMessage(byData);

                Thread.Sleep(250);
            }

            ServerIsExiting = true;
            try
            {
                if (timerGarbagePatrol != null)
                {
                    timerGarbagePatrol.Stop();
                    timerGarbagePatrol.Elapsed -= timerGarbagePatrol_Elapsed;
                    timerGarbagePatrol.Dispose();
                    timerGarbagePatrol = null;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_FormClosing", ex.ToString());
            }

            try
            {
                if (timerPing != null)
                {
                    timerPing.Stop();
                    timerPing.Elapsed -= timerPing_Elapsed;
                    timerPing.Dispose();
                    timerPing = null;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_FormClosing", ex.ToString());
            }

            KillTheServer();
        }


        private void StartPacketCommunicationsServiceThread()
        {
            try
            {
                //Packet processor mutex and loop
                autoEvent = new AutoResetEvent(false); //the RawPacket data mutex
                autoEvent2 = new AutoResetEvent(false);//the FullPacket data mutex
                DataProcessThread = new Thread(new ThreadStart(NormalizeThePackets));
                FullPacketDataProcessThread = new Thread(new ThreadStart(ProcessRecievedData));


                //Lists
                dClientRawPacketList = new Dictionary<int, MotherOfRawPackets>();
                FullPacketList = new Queue<FullPacket>();

                //Create HostServer
                svr = new Server();

                svr.Listen(MyPort);//MySettings.HostPort);
                svr.OnReceiveData += new Server.ReceiveDataCallback(OnDataReceived);
                svr.OnClientConnect += new Server.ClientConnectCallback(NewClientConnected);
                svr.OnClientDisconnect += new Server.ClientDisconnectCallback(ClientDisconnect);

                DataProcessThread.Start();
                FullPacketDataProcessThread.Start();

                OnCommunications($"TCPiP Server is listening on port {MyPort}", INK.CLR_GREEN);
            }
            catch (Exception ex)
            {
                var exceptionMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                //Debug.WriteLine($"EXCEPTION IN: StartPacketCommunicationsServiceThread - {exceptionMessage}");
                OnCommunications($"EXCEPTION: TCPiP FAILED TO START, exception: {exceptionMessage}", INK.CLR_RED);
            }
        }

        private void KillTheServer()
        {
            try
            {
                if (svr != null)
                {
                    svr.Stop();
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_KillTheServer", ex.ToString());
            }
            try
            {
                if (autoEvent != null)
                {
                    autoEvent.Set();

                    Thread.Sleep(30);
                    autoEvent.Close();
                    autoEvent.Dispose();
                    autoEvent = null;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_KillTheServer", ex.ToString());
            }
            try
            {
                if (autoEvent2 != null)
                {
                    autoEvent2.Set();

                    Thread.Sleep(30);
                    autoEvent2.Close();
                    autoEvent2.Dispose();
                    autoEvent2 = null;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_KillTheServer", ex.ToString());
            }
            Thread.Sleep(15);

            try
            {
                if (dClientRawPacketList != null)
                {
                    dClientRawPacketList.Clear();
                    dClientRawPacketList = null;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_KillTheServer", ex.ToString());
            }
            svr = null;
        }

        #region TIMERS
        //void timerUnlock_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    Unlock();
        //}
        private void OnButtonActionClick(object sender, ListViewColumnMouseEventArgs e)
        {
            Unlock(e.Item.SubItems[3].Text);
        }

        void Unlock(string imei)
        {
            lock (svr.workerSockets)
            {
                // Get List of values. [3]
                List<UserSock> valueList = new List<UserSock>(svr.workerSockets.Values);
                // Display them.
                foreach (var value in valueList)
                {
                    if (value.szIMEI == imei)
                    {
                        UserSock userSock = svr.workerSockets[value.iClientID];
                        byte[] sendUnlock = { 0x78, 0x78, 0x11, 0x80, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x55, 0x4E, 0x4C, 0x4F, 0x43, 0x4B, 0x23, 0x00, 0x01, 0x53, 0x54, 0x0D, 0x0A };
                        Send(userSock.UserSocket, sendUnlock);
                        OnCommunications($"Unlock: ClientID = {value.iClientID}; IMEI = {value.szIMEI}", INK.CLR_RED);
                    }
                }

            }
        }

        /// <summary>
        /// Fires every 4 minutes
        /// </summary>
        void timerPing_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            PingTheConnections();
        }
        void PingTheConnections()
        {
            if (svr == null)
                return;

            try
            {
                PACKET_DATA xdata = new PACKET_DATA();

                xdata.Packet_Type = (UInt16)PACKETTYPES.TYPE_Ping;
                xdata.Data_Type = 0;
                xdata.Packet_Size = 16;
                xdata.maskTo = 0;
                xdata.idTo = 0;
                xdata.idFrom = 0;

                xdata.DataLong1 = DateTime.UtcNow.Ticks;

                byte[] byData = PACKET_FUNCTIONS.StructureToByteArray(xdata);

                //Stopwatch sw = new Stopwatch();

                //sw.Start();
                lock (svr.workerSockets)
                {
                    foreach (Server.UserSock s in svr.workerSockets.Values)
                    {
                        //Console.WriteLine("Ping id - " + s.iClientID.ToString());
                        //Thread.Sleep(25);//allow a slight moment so all the replies dont happen at the same time
                        s.PingStatClass.StartTheClock();

                        try
                        {
                            svr.SendMessage(s.iClientID, byData);

                        }
                        catch (Exception ex)
                        {
                            Utilities.WriteErrorLog("frmServer_PingTheConnections", ex.ToString());
                        }
                    }
                }
                //sw.Stop();
                //Debug.WriteLine("TimeAfterSend: " + sw.ElapsedMilliseconds.ToString() + "ms");
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_PingTheConnections", ex.ToString());
            }
        }
        /**********************************************************************************************************************/

        //private void GarbagePatrol_Tick(object sender, EventArgs e)
        private void timerGarbagePatrol_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                CheckConnectionTimersGarbagePatrol();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_CheckConnectionTimer", ex.ToString());
            }
        }

        private void CheckConnectionTimersGarbagePatrol()
        {
            List<int> ClientIDsToClear = new List<int>();

            Debug.WriteLine($"{svr.workerSockets.Values.Count} - List Count: {svr.workerSockets.Values.Count}");

            lock (svr.workerSockets)
            {
                foreach (Server.UserSock s in svr.workerSockets.Values)
                {
                    TimeSpan diff = DateTime.Now - s.dTimer;
                    //Debug.WriteLine("iClientID: " + s.iClientID + " - " + "Time: " + diff.TotalSeconds.ToString());

                    if (diff.TotalSeconds >= 600 || s.UserSocket.Connected == false)//10 minutes
                    {
                        //Punt the ListVeiw item here but we must make a list of
                        //clients that we have lost connection with, its not good to remove
                        //the Servers internal client item while inside its foreach loop;
                        //listView1.Items.RemoveByKey(s.iClientID.ToString());
                        ClientIDsToClear.Add(s.iClientID);
                    }
                }
            }

            Debug.WriteLine($"{DateTime.Now.ToLongTimeString()} - Garbage Patrol num of IDs to remove: {ClientIDsToClear.Count}");

            //Ok remove any internal data items we may have
            if (ClientIDsToClear.Count > 0)
            {
                foreach (int cID in ClientIDsToClear)
                {
                    SendMessageOfClientDisconnect(cID);

                    CleanupDeadClient(cID);
                    Thread.Sleep(5);
                }
            }
        }

        private delegate void CleanupDeadClientDelegate(int clientNumber);
        private void CleanupDeadClient(int clientNumber)
        {
            if (InvokeRequired)
            {
                this.Invoke(new CleanupDeadClientDelegate(CleanupDeadClient), new object[] { clientNumber });
                return;
            }

            try
            {
                lock (dClientRawPacketList)//http://www.albahari.com/threading/part2.aspx#_Locking
                {
                    if (dClientRawPacketList.ContainsKey(clientNumber))
                    {
                        dClientRawPacketList[clientNumber].ClearList();
                        dClientRawPacketList.Remove(clientNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_CleanupDeadClient", ex.ToString());
            }

            try
            {
                lock (svr.workerSockets)
                {
                    if (svr.workerSockets.ContainsKey(clientNumber))
                    {
                        svr.workerSockets[clientNumber].UserSocket.Close();
                        svr.workerSockets.Remove(clientNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_CleanupDeadClient", ex.ToString());
            }

            try
            {
                if (listView1.Items.ContainsKey(clientNumber.ToString()))
                {
                    listView1.Items.RemoveByKey(clientNumber.ToString());
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_CleanupDeadClient", ex.ToString());
            }

        }
        #endregion

        #region TCPIP Layer incoming data
        private void OnDataReceived(int clientNumber, byte[] message, int messageSize)
        {
            if (dClientRawPacketList.ContainsKey(clientNumber))
            {
                byte[] buffer = new byte[messageSize];
                Array.Copy(message, buffer, messageSize);
                dClientRawPacketList[clientNumber].AddToList(buffer, messageSize);
                ProcessMessages(buffer, svr.workerSockets[clientNumber].UserSocket, clientNumber);
                Debug.WriteLine("Raw Data From: " + clientNumber.ToString() + ", Size of Packet: " + messageSize.ToString() + ", Message: " + string.Join("", buffer.Select(x => x.ToString("X2"))));
                autoEvent.Set();//Fire in the hole
            }
        }
        #endregion
        #region MyRegion
        public DateTime getDateTimeLocal(byte[] DateTimeUtc)
        {
            DateTime dateTimeUTC = new DateTime(DateTimeUtc[0] + 2000, DateTimeUtc[1], DateTimeUtc[2], DateTimeUtc[3], DateTimeUtc[4], DateTimeUtc[5]);
            return dateTimeUTC.ToLocalTime();
        }
        public double getLatitudeLongitude(byte[] LatitudeLongitude)
        {
            return (double)(Convert.ToInt32(BytesToString(LatitudeLongitude), 16)) / 1800000;
        }
        public double getPercentVoltage(byte[] VoltageLevel)
        {
            double volNumber = (Convert.ToInt64(BytesToString(VoltageLevel), 16));
            return Math.Round((((volNumber / 100 - 3.50d) / 0.66) * 100), 2);
        }
        public char[] getTerminalInformationContent(byte[] TerminalInformationContent)
        {
            char[] TerminalInfo = (Convert.ToString(TerminalInformationContent[0], 2)).ToArray();
            char[] TerminalOut = { TerminalInfo[1], TerminalInfo[5], TerminalInfo[7] };
            return TerminalOut;
        }


        public void ProcessMessages(byte[] receiveMessage, Socket userSock, int clientNumber)
        {
            UInt16 sendCRC = 0;
            byte[] packetStartBit = null;
            byte[] packetStopBit = { 0x0D, 0x0A };//fix
            byte[] packetReceiveLength = null;
            byte[] packetSendLength = null;
            byte[] packetProtocol = null;
            byte[] packetSerialNumber = null;
            byte[] packetErrorCheck = null;


            //Login packet
            byte[] login_modelCode = null;
            byte[] login_timeZone = null;

            //Heartbeat packet
            byte[] heartbeat_terminalInformationContent = null;
            byte[] heartbeat_voltageLevel = null;
            byte[] heartbeat_gSMSignalStrength = null;
            byte[] heartbeat_Language = null;

            //Location packet
            byte[] location_DateTimeUTC = null;
            byte[] location_GPSInfoLength = null;
            byte[] location_QuantityGPSInfo = null;
            byte[] location_Latitude = null;
            byte[] location_Longitude = null;
            byte[] location_CourseStatus = null;
            byte[] location_MainBaseStation_Length = null;
            byte[] location_MCC = null;
            byte[] location_MNC = null;
            byte[] location_LAC = null;
            byte[] location_CI = null;
            byte[] location_RSSI = null;
            byte[] location_SubBaseStation_Length = null;
            byte[] location_NLAC1 = null;
            byte[] location_NLAC2 = null;
            byte[] location_NCI1 = null;
            byte[] location_NCI2 = null;
            byte[] location_NRSSI1 = null;
            byte[] location_NRSSI2 = null;
            byte[] location_WIFI_MessageLength = null;
            byte[] location_WIFI_MAC_1 = null;
            byte[] location_WIFI_MAC_2 = null;
            byte[] location_WIFI_Strength_1 = null;
            byte[] location_WIFI_Strength_2 = null;
            byte[] location_Status = null;
            byte[] location_ReservedExtensionBit_Length = null;
            byte[] location_ReservedExtensionBit = null;
            byte[] location_Speed = null;



            bool isLogin = false;
            bool isSendLock = false;
            PROTOCOL_NUMBER protocolNumber = PROTOCOL_NUMBER.NONE;
            try
            {
                if (receiveMessage.Length < 1)
                {
                    protocolNumber = PROTOCOL_NUMBER.NONE;
                }
                else if (receiveMessage[1] == 121)
                {
                    protocolNumber = (PROTOCOL_NUMBER)receiveMessage[4];
                }
                else
                {
                    protocolNumber = (PROTOCOL_NUMBER)receiveMessage[3];
                }

                List<byte> DateTimeUTC = new List<byte>();
                DateTimeUTC.AddRange(TransformBytes(19, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Month, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Day, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Hour, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Minute, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Second, 1));
                switch (protocolNumber)
                {
                    #region LOGIN_INFORMATION
                    case PROTOCOL_NUMBER.LOGIN_INFORMATION:
                        packetStartBit = receiveMessage.Take(2).ToArray();
                        packetReceiveLength = receiveMessage.Skip(2).Take(1).ToArray();
                        packetSendLength = new byte[] { 0x0C };
                        packetProtocol = new byte[] { 0x01 };
                        this.login_imei = receiveMessage.Skip(4).Take(8).ToArray();
                        login_modelCode = receiveMessage.Skip(12).Take(2).ToArray();
                        login_timeZone = receiveMessage.Skip(14).Take(2).ToArray();
                        packetSerialNumber = receiveMessage.Skip(16).Take(2).ToArray();
                        packetErrorCheck = receiveMessage.Skip(18).Take(2).ToArray();

                        byte[] loginMessageResponse = packetStartBit
                            .Concat(packetSendLength)
                            .Concat(packetProtocol)
                            .Concat(DateTimeUTC.ToArray())
                            .Concat(new byte[] { 0x00 })
                            .Concat(packetSerialNumber)
                            .Concat(packetErrorCheck)
                            .Concat(packetStopBit)
                            .ToArray();

                        byte[] checkCRC = loginMessageResponse.Skip(2).Take(11).ToArray();
                        sendCRC = Crc_bytes(checkCRC); //https://crccalc.com/ CRC-16/X-25
                        loginMessageResponse[loginMessageResponse.Length - 4] = (byte)((sendCRC >> 8) & 0xFF);
                        loginMessageResponse[loginMessageResponse.Length - 3] = (byte)((sendCRC) & 0xFF);
                        string errorCheck = BytesToString(loginMessageResponse.Skip(13).Take(2).ToArray());

                        OnCommunications($"Received Login Message Packet", INK.CLR_PURPLE);
                        OnCommunications($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}", INK.CLR_PURPLE);
                        OnCommunications($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}", INK.CLR_PURPLE);
                        OnCommunications($"login imei Value: {BytesToString(login_imei)}", INK.CLR_PURPLE);

                        //OnCommunications($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Packet Length                (1 byte): {BytesToString(packetReceiveLength)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}", INK.CLR_PURPLE);
                        //OnCommunications($"- IMEI                         (8 byte): {BytesToString(login_imei)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Model Indentification Code   (2 byte): {BytesToString(login_modelCode)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Time Zone Language           (2 byte): {BytesToString(login_timeZone)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Information Serial Number    (2 byte): {BytesToString(packetSerialNumber)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Error Check                  (2 byte): {BytesToString(packetErrorCheck)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}", INK.CLR_PURPLE);
                        OnCommunications($"Send Login Message Response", INK.CLR_PURPLE);
                        OnCommunications($"Hexadecimal Value: : {BytesToString(loginMessageResponse)}", INK.CLR_PURPLE);
                        OnCommunications($"Error Check: {BytesToString(checkCRC)} ~ {sendCRC} ~ {errorCheck}", INK.CLR_PURPLE);
                        //OnCommunications($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Packet Length                (1 byte): {BytesToString(packetSendLength)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}", INK.CLR_PURPLE);
                        //OnCommunications($"- DateTime (UTC)               (6 byte): {BytesToString(DateTimeUTC.ToArray())}", INK.CLR_PURPLE);
                        //OnCommunications($"- Reserved Extension BitLength (1 byte): {BytesToString(new byte[] { 0x00 })}", INK.CLR_PURPLE);
                        //OnCommunications($"- Information Serial Number    (2 byte): {BytesToString(packetSerialNumber)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Error Check                  (2 byte): {errorCheck}", INK.CLR_PURPLE);
                        //OnCommunications($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}", INK.CLR_PURPLE);
                        OnCommunications("===============================================================", INK.CLR_PURPLE);
                        Send(userSock, loginMessageResponse);

                        ClientImeiConnected(clientNumber, BytesToString(this.login_imei));
                        break;
                    #endregion
                    #region HEARTBEAT_PACKET
                    case PROTOCOL_NUMBER.HEARTBEAT_PACKET:
                        packetStartBit = receiveMessage.Take(2).ToArray();
                        packetReceiveLength = receiveMessage.Skip(2).Take(1).ToArray();
                        packetSendLength = new byte[] { 0x05 };
                        packetProtocol = new byte[] { 0x23 };
                        heartbeat_terminalInformationContent = receiveMessage.Skip(4).Take(1).ToArray();
                        heartbeat_voltageLevel = receiveMessage.Skip(5).Take(2).ToArray();
                        heartbeat_gSMSignalStrength = receiveMessage.Skip(7).Take(1).ToArray();
                        heartbeat_Language = receiveMessage.Skip(9).Take(2).ToArray();
                        packetSerialNumber = receiveMessage.Skip(10).Take(2).ToArray();
                        packetErrorCheck = receiveMessage.Skip(12).Take(2).ToArray();

                        byte[] hearbeatMessageResponse = packetStartBit
                            .Concat(packetSendLength)
                            .Concat(packetProtocol)
                            .Concat(packetSerialNumber)
                            .Concat(packetErrorCheck)
                            .Concat(packetStopBit)
                            .ToArray();

                        byte[] checkCRC_heartbeat = hearbeatMessageResponse.Skip(2).Take(4).ToArray();
                        sendCRC = Crc_bytes(checkCRC_heartbeat); //https://crccalc.com/ CRC-16/X-25

                        hearbeatMessageResponse[hearbeatMessageResponse.Length - 4] = (byte)((sendCRC >> 8) & 0xFF);
                        hearbeatMessageResponse[hearbeatMessageResponse.Length - 3] = (byte)((sendCRC) & 0xFF);
                        string errorCheck_heartbeat = BytesToString(hearbeatMessageResponse.Skip(6).Take(2).ToArray());

                        OnCommunications($"Received Heartbeat Packet Sent By Terminal (16 bytes)", INK.CLR_PURPLE);
                        OnCommunications($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}", INK.CLR_PURPLE);
                        OnCommunications($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}", INK.CLR_PURPLE);

                        //OnCommunications($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Packet Length                (1 byte): {BytesToString(packetReceiveLength)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}", INK.CLR_PURPLE);
                        OnCommunications($"- Terminal Information Content   (1 byte): {BytesToString(heartbeat_terminalInformationContent)} = {getTerminalInformationContent(heartbeat_terminalInformationContent)}", INK.CLR_PURPLE);
                        OnCommunications($"- VoltageLevel                   (2 byte): {BytesToString(heartbeat_voltageLevel)} = {getPercentVoltage(heartbeat_voltageLevel)}%", INK.CLR_PURPLE);
                        //OnCommunications($"- GSM Signal Strength          (1 byte): {BytesToString(login_timeZone)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Language                     (2 byte): {BytesToString(login_timeZone)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Serial Number                (2 byte): {BytesToString(packetSerialNumber)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Error Check                  (2 byte): {BytesToString(packetErrorCheck)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}", INK.CLR_PURPLE);
                        OnCommunications($"Send  Server Responds The Heartbeat Packet (10 bytes)", INK.CLR_PURPLE);
                        OnCommunications($"Hexadecimal Value: : {BytesToString(hearbeatMessageResponse)}", INK.CLR_PURPLE);
                        OnCommunications($"Error Check: {BytesToString(checkCRC_heartbeat)} ~ {sendCRC} ~ {errorCheck_heartbeat}", INK.CLR_PURPLE);
                        //OnCommunications($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Packet Length                (1 byte): {BytesToString(packetSendLength)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}", INK.CLR_PURPLE);
                        //OnCommunications($"- SerialNumber                 (1 byte): {BytesToString(packetSerialNumber)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Error Check                  (2 byte): {errorCheck_heartbeat}", INK.CLR_PURPLE);
                        //OnCommunications($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}", INK.CLR_PURPLE);
                        OnCommunications("===============================================================", INK.CLR_PURPLE);
                        Send(userSock, hearbeatMessageResponse);

                        char[] TerminalInfo = getTerminalInformationContent(heartbeat_terminalInformationContent);
                        double PercentVoltage = getPercentVoltage(heartbeat_voltageLevel);
                        ClientInfoConnected(clientNumber, TerminalInfo, PercentVoltage);

                        break;
                    #endregion
                    #region GPS_LOCATION_INFORMATION
                    case PROTOCOL_NUMBER.GPS_LOCATION_INFORMATION:
                        packetStartBit = receiveMessage.Take(2).ToArray();
                        packetReceiveLength = receiveMessage.Skip(2).Take(2).ToArray();
                        packetSendLength = new byte[] { 0x0C }; //quest ?
                        packetProtocol = receiveMessage.Skip(4).Take(1).ToArray();
                        location_DateTimeUTC = receiveMessage.Skip(5).Take(6).ToArray();
                        location_GPSInfoLength = receiveMessage.Skip(11).Take(1).ToArray();
                        location_QuantityGPSInfo = receiveMessage.Skip(12).Take(1).ToArray();
                        location_Latitude = receiveMessage.Skip(13).Take(4).ToArray();
                        location_Longitude = receiveMessage.Skip(17).Take(4).ToArray();
                        location_Speed = receiveMessage.Skip(21).Take(1).ToArray();
                        location_CourseStatus = receiveMessage.Skip(22).Take(2).ToArray();
                        location_MainBaseStation_Length = receiveMessage.Skip(24).Take(1).ToArray();
                        location_MCC = receiveMessage.Skip(25).Take(2).ToArray();
                        location_MNC = receiveMessage.Skip(27).Take(1).ToArray();
                        location_LAC = receiveMessage.Skip(28).Take(2).ToArray();
                        location_CI = receiveMessage.Skip(30).Take(3).ToArray();
                        location_RSSI = receiveMessage.Skip(33).Take(1).ToArray();
                        location_SubBaseStation_Length = receiveMessage.Skip(34).Take(1).ToArray();
                        location_NLAC1 = receiveMessage.Skip(35).Take(2).ToArray();
                        location_NCI1 = receiveMessage.Skip(37).Take(3).ToArray();
                        location_NRSSI1 = receiveMessage.Skip(40).Take(1).ToArray();
                        location_NLAC2 = receiveMessage.Skip(41).Take(2).ToArray();
                        location_NCI2 = receiveMessage.Skip(43).Take(3).ToArray();
                        location_NRSSI2 = receiveMessage.Skip(46).Take(1).ToArray();
                        location_WIFI_MessageLength = receiveMessage.Skip(47).Take(1).ToArray();
                        location_WIFI_MAC_1 = receiveMessage.Skip(48).Take(6).ToArray();
                        location_WIFI_Strength_1 = receiveMessage.Skip(54).Take(1).ToArray();
                        location_WIFI_MAC_2 = receiveMessage.Skip(55).Take(6).ToArray();
                        location_WIFI_Strength_2 = receiveMessage.Skip(61).Take(1).ToArray();
                        location_Status = receiveMessage.Skip(62).Take(1).ToArray();
                        location_ReservedExtensionBit_Length = receiveMessage.Skip(63).Take(1).ToArray();
                        packetSerialNumber = receiveMessage.Skip(64).Take(2).ToArray();
                        packetErrorCheck = receiveMessage.Skip(66).Take(2).ToArray();
                        SaveGPSInfo(
                            BytesToString(this.login_imei),
                            getLatitudeLongitude(location_Latitude),
                            getLatitudeLongitude(location_Longitude),
                            getDateTimeLocal(location_DateTimeUTC.ToArray())
                            );


                        OnCommunications($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}  Received GPS_LOCATION_INFORMATION", INK.CLR_PURPLE);
                        OnCommunications($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}", INK.CLR_PURPLE);
                        OnCommunications($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}", INK.CLR_PURPLE);

                        //OnCommunications($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Packet Length                (2 byte): {BytesToString(packetReceiveLength)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}", INK.CLR_PURPLE);
                        //OnCommunications($"- DateTime UTC                 (6 byte): {BytesToString(location_DateTimeUTC)} = DateTime Local: {getDateTimeLocal(location_DateTimeUTC.ToArray())}", INK.CLR_PURPLE);
                        OnCommunications($"- GPS Info Length              (1 byte): {BytesToString(location_GPSInfoLength)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Quantity GPS Info            (1 byte): {BytesToString(location_QuantityGPSInfo)}", INK.CLR_PURPLE);
                        OnCommunications($"- Latitude                     (4 byte): {BytesToString(location_Latitude)} = {getLatitudeLongitude(location_Latitude)} ", INK.CLR_PURPLE);
                        OnCommunications($"- Longitude                    (4 byte): {BytesToString(location_Longitude)} = {getLatitudeLongitude(location_Longitude)} ", INK.CLR_PURPLE);
                        OnCommunications($"- Speed                        (1 byte): {BytesToString(location_Speed)}", INK.CLR_PURPLE);
                        OnCommunications($"- View on Google map: https://maps.google.com?q={getLatitudeLongitude(location_Latitude)},{getLatitudeLongitude(location_Longitude)}", INK.CLR_PURPLE);
                        //OnCommunications($"- CourseStatus                 (2 byte): {BytesToString(location_CourseStatus)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Main Base Station Length     (2 byte): {BytesToString(location_MainBaseStation_Length)}", INK.CLR_PURPLE);
                        //OnCommunications($"- MCC                          (2 byte): {BytesToString(location_MCC)}", INK.CLR_PURPLE);
                        //OnCommunications($"- MNC                          (1 byte): {BytesToString(location_MNC)}", INK.CLR_PURPLE);
                        //OnCommunications($"- LAC                          (2 byte): {BytesToString(location_LAC)}", INK.CLR_PURPLE);
                        //OnCommunications($"- CI                           (3 byte): {BytesToString(location_CI)}", INK.CLR_PURPLE);
                        //OnCommunications($"- RSSI                         (1 byte): {BytesToString(location_RSSI)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Sub BaseStationbLength       (1 byte): {BytesToString(location_SubBaseStation_Length)}", INK.CLR_PURPLE);
                        //OnCommunications($"- NLAC1                        (2 byte): {BytesToString(location_NLAC1)}", INK.CLR_PURPLE);
                        //OnCommunications($"- NCI1                         (3 byte): {BytesToString(location_NCI1)}", INK.CLR_PURPLE);
                        //OnCommunications($"- NRSSI1                       (1 byte): {BytesToString(location_NRSSI1)}", INK.CLR_PURPLE);
                        //OnCommunications($"- NLAC2                        (2 byte): {BytesToString(location_NLAC2)}", INK.CLR_PURPLE);
                        //OnCommunications($"- NCI2                         (3 byte): {BytesToString(location_NCI2)}", INK.CLR_PURPLE);
                        //OnCommunications($"- NRSSI2                       (1 byte): {BytesToString(location_NRSSI2)}", INK.CLR_PURPLE);
                        //OnCommunications($"- WIFI Message Length          (1 byte): {BytesToString(location_WIFI_MessageLength)}", INK.CLR_PURPLE);
                        //OnCommunications($"- WIFI MAC 1                   (6 byte): {BytesToString(location_WIFI_MAC_1)}", INK.CLR_PURPLE);
                        //OnCommunications($"- WIFI Strength 1              (1 byte): {BytesToString(location_WIFI_Strength_1)}", INK.CLR_PURPLE);
                        //OnCommunications($"- WIFI MAC 2                   (6 byte): {BytesToString(location_WIFI_MAC_2)}", INK.CLR_PURPLE);
                        //OnCommunications($"- WIFI Strength 2              (1 byte): {BytesToString(location_WIFI_Strength_2)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Status                       (1 byte): {BytesToString(location_Status)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Reserved Extension Bit Length(1 byte): {BytesToString(location_ReservedExtensionBit_Length)}", INK.CLR_PURPLE);
                        //OnCommunications($"- SerialNumber                 (2 byte): {BytesToString(packetSerialNumber)}", INK.CLR_PURPLE);
                        //OnCommunications($"- ErrorCheck                   (2 byte): {BytesToString(packetErrorCheck)}", INK.CLR_PURPLE);
                        //OnCommunications($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}", INK.CLR_PURPLE);

                        break;
                        #endregion
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_ProcessMessages", ex.ToString());
            }
        }
        private delegate void ClientImeiConnectedImeiDelegate(int clientNumber, string imei);
        private delegate void ClientInfoConnectedDelegate(int clientNumber, char[] TerminalInfo, double PercentVoltage);
        /// <summary>
        /// return bool, TRUE if its a FullClient Connection
        /// </summary>
        /// <param name="clientNumber"></param>
        /// <param name="message"></param>
        private void ClientImeiConnected(int clientNumber, string imei)
        {
            if (InvokeRequired)
            {
                this.Invoke(new ClientImeiConnectedImeiDelegate(ClientImeiConnected), new object[] { clientNumber, imei });
                return;
            }

            try
            {
                lock (svr.workerSockets) 
                {
                    string ComputerName = "Concox";
                    string ClientsName = "BL10";

                    listView1.Items[clientNumber.ToString()].SubItems[2].Text = ComputerName;
                    listView1.Items[clientNumber.ToString()].SubItems[3].Text = imei;
                    listView1.Items[clientNumber.ToString()].SubItems[5].Text = ClientsName;

                    if (svr.workerSockets.ContainsKey(clientNumber))
                    {
                        svr.workerSockets[clientNumber].szStationName = ComputerName;
                        svr.workerSockets[clientNumber].szIMEI = imei;
                        svr.workerSockets[clientNumber].szClientName = ClientsName;

                        OnCommunications(string.Format("Registered Connection ({0}) for '{1}'", clientNumber, imei), INK.CLR_GREEN);
                    }
                }//end lock

            }
            catch (Exception ex)
            {
                OnCommunications($"EXCEPTION: PostUserCredentials on client {clientNumber}, EMEI = {imei}, exception: {ex.Message}", INK.CLR_RED);
                Utilities.WriteErrorLog("frmServer_ClientImeiConnected", ex.ToString());

            }
        }
        private void ClientInfoConnected(int clientNumber, char[] TerminalInfo, double PercentVoltage)
        {

            if (InvokeRequired)
            {
                this.Invoke(new ClientInfoConnectedDelegate(ClientInfoConnected), new object[] { clientNumber, TerminalInfo, PercentVoltage });
                return;
            }

            try
            {
                lock (svr.workerSockets)
                {
                    string ComputerName = "Concox";
                    string ClientsName = "BL10";
                    string Gps = (int)Char.GetNumericValue(TerminalInfo[0]) == 1 ? "Positioning" : "No Positioning";
                    string Battery = (int)Char.GetNumericValue(TerminalInfo[1]) == 1 ? $"{PercentVoltage}%(Charging)" : $"{PercentVoltage}%";
                    string Lock = (int)Char.GetNumericValue(TerminalInfo[2]) == 1 ? "Lock" : "Unlock";

                    OnCommunications($"info terminal: Gps {TerminalInfo[0]}", INK.CLR_PURPLE);
                    OnCommunications($"info terminal: Battery {TerminalInfo[1]}", INK.CLR_PURPLE);
                    OnCommunications($"info terminal: Lock {TerminalInfo[2]}", INK.CLR_PURPLE);
                    listView1.Items[clientNumber.ToString()].SubItems[2].Text = ComputerName;
                    listView1.Items[clientNumber.ToString()].SubItems[5].Text = ClientsName;
                    listView1.Items[clientNumber.ToString()].SubItems[7].Text = Gps;
                    listView1.Items[clientNumber.ToString()].SubItems[8].Text = Battery;
                    listView1.Items[clientNumber.ToString()].SubItems[9].Text = Lock;
                    listView1.Items[clientNumber.ToString()].SubItems[1].Text = "Unlock";
                    //listView1.Items[clientNumber.ToString()].SubItems.Add("_btnUL");

                    UpdateDockInfoIntoDb(
                        svr.workerSockets[clientNumber].szIMEI,
                        (int)Char.GetNumericValue(TerminalInfo[1]) == 1 ? true : false, // Trạng thái sạc của pin
                        PercentVoltage, // Phần trăm pin
                        (int)Char.GetNumericValue(TerminalInfo[2]) == 1 ? true : false // Trạng thái khóa đang đóng hay mở
                        );
                }//end lock

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_ClientInfoConnected", ex.ToString());
                OnCommunications($"EXCEPTION: PostUserCredentials on client {clientNumber}, exception: {ex.Message}", INK.CLR_RED);
            }
        }

        #region{SQL Query}

        private void SaveGPSInfo(string IMEI, double Lat, double Long, DateTime CreateTime)
        {
            try
            {
                var dockInfo = GetDockInfo(IMEI);
                string sql = "INSERT INTO GPSData (BikeId, DockId, IMEI, Lat, Long, CreateTime) Values(@BikeId, @DockId, @IMEI, @Lat, @Long, @CreateTime);";

                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
                {
                    var affectedRows = connection.Execute(sql, new { BikeId = dockInfo.CurrentBikeId, DockId = dockInfo.Id, IMEI = IMEI, Lat = Lat, Long = Long, CreateTime = CreateTime });
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_SaveGPSInfo", ex.ToString());
                OnCommunications($"error_SaveGPSInfo                  :{ex}", INK.CLR_PURPLE);

            }
        }

        private Dock GetDockInfo(string IMEI)
        {
            Dock dock = new Dock();
            try
            {
                string sql = "SELECT * FROM Dock WHERE IMEI = @IMEI";

                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
                {

                    var reader = connection.ExecuteReader(sql, new { IMEI = IMEI });
                    if (reader != null)
                    {
                        reader.Read();
                        dock.Id = reader.GetInt32("Id");
                        //dock.BikeAlarm = reader.GetInt32("BikeAlarm") == 1;
                        //dock.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
                        //dock.CreatedSystemUserId = reader.GetInt32("CreatedSystemUserId");
                        dock.CurrentBikeId = reader.GetInt32("CurrentBikeId");
                        //dock.IMEI = reader.GetString("IMEI");
                        //dock.LockStatus = reader.GetInt32("LockStatus") == 1;
                        //dock.Model = reader.GetString("Model");
                        //dock.OrderNumber = reader.GetInt32("OrderNumber");
                        //dock.SerialNumber = reader.GetString("SerialNumber");
                        //dock.StationId = reader.GetInt32("StationId");
                        //dock.Status = reader.GetInt32("Status");
                        //dock.UpdatedDate = reader.GetDateTime(reader.GetOrdinal("UpdatedDate"));
                        //dock.UpdatedSystemUserId = reader.GetInt32("UpdatedSystemUserId");
                        //dock.BikeAlarmId = reader.GetInt32("BikeAlarmId");
                        //dock.ErrorStatus = reader.GetInt32("ErrorStatus") == 1;
                        //dock.Battery = reader.GetFloat(reader.GetOrdinal("Battery"));
                        //dock.Charging = reader.GetInt32("Charging") == 1;
                    }
                }

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_SqlGetDock", ex.ToString());
                OnCommunications($"error_GetDockInfo                  :{ex}", INK.CLR_PURPLE);

            }
            return dock;
        }

        private void UpdateDockInfoIntoDb(string IMEI, bool Charging, double PercentVoltage, bool LockStatus)
        {
            try
            {
                string sql = "UPDATE Dock SET LockStatus = @LockStatus, Battery = @Battery, Charging = @Charging WHERE IMEI = @IMEI";

                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
                {

                    var affectedRows = connection.Execute(sql, new { LockStatus = LockStatus, Battery = PercentVoltage, Charging = Charging, IMEI = IMEI });
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_UpdateDockInfoIntoDb", ex.ToString());
                OnCommunications($"error_UpdateDockInfoIntoDb                  :{ex}", INK.CLR_PURPLE);
            }

        }

        #endregion

        public static UInt16 Crc_bytes(byte[] data)
        {
            ushort crc = 0xFFFF;

            for (int i = 0; i < data.Length; i++)
            {
                crc ^= (ushort)(Reflect(data[i], 8) << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x8000) > 0)
                        crc = (ushort)((crc << 1) ^ 0x1021);
                    else
                        crc <<= 1;
                }
            }
            crc = Reflect(crc, 16);
            crc = (ushort)~crc;
            return crc;
        }
        public static ushort Reflect(ushort data, int size)
        {
            ushort output = 0;
            for (int i = 0; i < size; i++)
            {
                int lsb = data & 0x01;
                output = (ushort)((output << 1) | lsb);
                data >>= 1;
            }
            return output;
        }
        static byte[] TransformBytes(int num, int byteLength)
        {
            byte[] res = new byte[byteLength];

            byte[] temp = BitConverter.GetBytes(num);

            Array.Copy(temp, res, byteLength);

            return res;
        }
        static string BytesToString(byte[] bytes)
        {
            return string.Join("", bytes.Select(x => x.ToString("X2")));
        }
        static void Send(Socket socket, byte[] data)
        {
            // Convert the string data to byte data using ASCII encoding.
            // byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            socket.BeginSend(data, 0, data.Length, 0,
                new AsyncCallback(SendCallback), socket);
        }
        static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = ar.AsyncState as Socket;
                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                // Console.WriteLine("Sent {0} bytes to client.", bytesSent);
            }
            catch (Exception ex)
            {
                // Console.WriteLine(e.ToString());
                Utilities.WriteErrorLog("frmServer_SendCallback", ex.ToString());

                int myerror = -1;
            }
        }

        #endregion
        #region CLIENT CONNECTION PROCESS
        private void NewClientConnected(int ConnectionID)
        {
            try
            {
                Debug.WriteLine($"(RT Client)NewClientConnected: {ConnectionID}");
                OnCommunications($"Incoming Connection {ConnectionID}", INK.CLR_PURPLE);
                if (svr.workerSockets.ContainsKey(ConnectionID))
                {
                    lock (dClientRawPacketList)//http://www.albahari.com/threading/part2.aspx#_Locking
                    {
                        //Add the raw Packet collector
                        if (!dClientRawPacketList.ContainsKey(ConnectionID))
                        {
                            dClientRawPacketList.Add(ConnectionID, new MotherOfRawPackets(ConnectionID));
                        }
                    }

                    SetNewConnectionData_FromThread(ConnectionID);
                }
                else
                {
                    Debug.WriteLine("UNKNOWN CONNECTIONID" + ConnectionID.ToString());
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_NewClientConnected", ex.ToString());
                OnCommunications($"EXCEPTION: NewClientConnected on client {ConnectionID}, exception: {ex.Message}", INK.CLR_RED);
            }
        }

        private delegate void SetNewConnectionDataDelegate(int clientNumber);
        private void SetNewConnectionData_FromThread(int clientNumber)
        {
            if (InvokeRequired)
            {
                this.Invoke(new SetNewConnectionDataDelegate(SetNewConnectionData_FromThread), new object[] { clientNumber });
                return;
            }

            try
            {
                lock (svr.workerSockets)
                {
                    /*********************************   Add the data to the Listview  *************************************/
                    ListViewItem li = new ListViewItem(svr.workerSockets[clientNumber].UserSocket.RemoteEndPoint.ToString());
                    li.Name = clientNumber.ToString();//Set the Key as a unique identifier
                    li.Tag = clientNumber;

                    listView1.Items.Add(li);                    //index 0 Clients IP address
                    li.SubItems.Add("Receiving...");            //index 1 Lock
                    li.SubItems.Add("Receiving...");            //index 2 Computer name
                    li.SubItems.Add("Receiving...");            //index 3 version
                    li.SubItems.Add(clientNumber.ToString());   //index 4 //Client's ID
                    li.SubItems.Add("Receiving...");            //index 5 Clients Name
                    li.SubItems.Add("...");                     //index 6 Ping time
                    li.SubItems.Add("Receiving...");            //index 7 Gps
                    li.SubItems.Add("Receiving...");            //index 8 Battery
                    li.SubItems.Add("Receiving...");            //index 9 Status
                    /*******************************************************************************************************/
                }
                if (svr.workerSockets[clientNumber].UserSocket.Connected)
                {
                    OnCommunications($"RequestNewConnectionCredentials from: {clientNumber}", INK.CLR_PURPLE);
                    RequestNewConnectionCredentials(clientNumber);
                }
                else
                {
                    Debug.WriteLine($"ISSUE!!!(RequestNewConnectionCredentials) UserSocket.Connected is FALSE from: {clientNumber}");
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_SetNewConnectionData", ex.ToString());
                OnCommunications($"EXCEPTION: SetNewConnectionData_FromThread on client {clientNumber}, exception: {ex.Message}", INK.CLR_RED);
            }
        }


        private delegate void PostUserCredentialsDelegate(int clientNumber, byte[] message);
        /// <summary>
        /// return bool, TRUE if its a FullClient Connection
        /// </summary>
        /// <param name="clientNumber"></param>
        /// <param name="message"></param>
        private void PostUserCredentials(int clientNumber, byte[] message)
        {
            if (InvokeRequired)
            {
                this.Invoke(new PostUserCredentialsDelegate(PostUserCredentials), new object[] { clientNumber, message });
                return;
            }

            try
            {
                PACKET_DATA IncomingData = new PACKET_DATA();
                IncomingData = (PACKET_DATA)PACKET_FUNCTIONS.ByteArrayToStructure(message, typeof(PACKET_DATA));

                lock (svr.workerSockets)
                {
                    string ComputerName = new string(IncomingData.szStringDataA).TrimEnd('\0');//Station/Computer's name
                    string VersionStr = new string(IncomingData.szStringDataB).TrimEnd('\0');//app version
                    string ClientsName = new string(IncomingData.szStringData150).TrimEnd('\0');//Client's Name

                    listView1.Items[clientNumber.ToString()].SubItems[2].Text = ComputerName;
                    listView1.Items[clientNumber.ToString()].SubItems[3].Text = VersionStr;
                    listView1.Items[clientNumber.ToString()].SubItems[5].Text = ClientsName;

                    if (svr.workerSockets.ContainsKey(clientNumber))
                    {
                        svr.workerSockets[clientNumber].szStationName = ComputerName;

                        svr.workerSockets[clientNumber].szClientName = ClientsName;

                        OnCommunications(string.Format("Registered Connection ({0}) for '{1}' on PC: {2}", clientNumber, ClientsName, ComputerName),
                            INK.CLR_GREEN);
                    }
                }//end lock

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_PostUserCredentials", ex.ToString());
                OnCommunications($"EXCEPTION: PostUserCredentials on client {clientNumber}, exception: {ex.Message}", INK.CLR_RED);
            }
        }


        private void ClientDisconnect(int clientNumber)
        {
            if (ServerIsExiting)
                return;

            /*******************************************************/
            lock (dClientRawPacketList)//Make sure we don't do this twice
            {
                if (!dClientRawPacketList.ContainsKey(clientNumber))
                {
                    lock (svr.workerSockets)
                    {
                        if (!svr.workerSockets.ContainsKey(clientNumber))
                        {
                            return;
                        }
                    }
                }
            }
            /*******************************************************/

            try
            {
                RemoveClient_FromThread(clientNumber);
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_ClientDisconnect", ex.ToString());
                OnCommunications($"EXCEPTION: ClientDisconnect on client {clientNumber}, exception: {ex.Message}", INK.CLR_RED);
            }

            CleanupDeadClient(clientNumber);


            Thread.Sleep(10);
        }

        private void RemoveClient_FromThread(int clientNumber)
        {
            try
            {
                SendMessageOfClientDisconnect(clientNumber);
                OnCommunications(string.Format("{0} has disconnected", clientNumber), INK.CLR_BLUE);
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_RemoveClient_FromThread", ex.ToString());
                OnCommunications($"EXCEPTION: RemoveClient_FromThread on client {clientNumber}, Exception: {ex.Message}", INK.CLR_RED);
            }
        }
        #endregion

        #region Packet factory Processing from clients
        private void NormalizeThePackets()
        {
            if (svr == null)
                return;

            while (svr.IsListening)
            {
                //Debug.WriteLine("Before AutoEvent");
                autoEvent.WaitOne(10000);//wait at mutex until signal, and drop through every 10 seconds if something strange happens
                //Debug.WriteLine("After AutoEvent");

                /**********************************************/
                lock (dClientRawPacketList)//http://www.albahari.com/threading/part2.aspx#_Locking
                {
                    foreach (MotherOfRawPackets MRP in dClientRawPacketList.Values)
                    {
                        if (MRP.GetItemCount.Equals(0))
                            continue;
                        try
                        {
                            byte[] packetplayground = new byte[11264];//good for 10 full packets(10240) + 1 remainder(1024)
                            RawPackets rp;

                            int actualPackets = 0;

                            while (true)
                            {
                                if (MRP.GetItemCount == 0)
                                    break;

                                int holdLen = 0;

                                if (MRP.bytesRemaining > 0)
                                    Copy(MRP.Remainder, 0, packetplayground, 0, MRP.bytesRemaining);

                                holdLen = MRP.bytesRemaining;

                                for (int i = 0; i < 10; i++)//only go through a max of 10 times so there will be room for any remainder
                                {
                                    rp = MRP.GetTopItem;//dequeue

                                    Copy(rp.dataChunk, 0, packetplayground, holdLen, rp.iChunkLen);

                                    holdLen += rp.iChunkLen;

                                    if (MRP.GetItemCount.Equals(0))//make sure there is more in the list befor continuing
                                        break;
                                }

                                actualPackets = 0;

                                #region PACKET_SIZE 1024
                                if (holdLen >= 1024)//make sure we have at least one packet in there
                                {
                                    actualPackets = holdLen / 1024;
                                    MRP.bytesRemaining = holdLen - (actualPackets * 1024);

                                    for (int i = 0; i < actualPackets; i++)
                                    {
                                        byte[] tmpByteArr = new byte[1024];
                                        Copy(packetplayground, i * 1024, tmpByteArr, 0, 1024);
                                        lock (FullPacketList)
                                            FullPacketList.Enqueue(new FullPacket(MRP.iListClientID, tmpByteArr));
                                    }
                                }
                                else
                                {
                                    MRP.bytesRemaining = holdLen;
                                }

                                //hang onto the remainder
                                Copy(packetplayground, actualPackets * 1024, MRP.Remainder, 0, MRP.bytesRemaining);
                                #endregion

                                if (FullPacketList.Count > 0)
                                    autoEvent2.Set();
                                //Call_ProcessRecievedData_FromThread();

                            }//end of while(true)
                        }
                        catch (Exception ex)
                        {
                            MRP.ClearList();//pe 03-20-2013
                            string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;

                            OnCommunications("EXCEPTION in  NormalizeThePackets - " + msg, INK.CLR_RED);
                        }
                    }//end of foreach (dClientRawPacketList)
                }//end of lock
                /**********************************************/
                if (ServerIsExiting)
                    break;
            }//Endof of while(svr.IsListening)

            Debug.WriteLine("Exiting the packet normalizer");
            OnCommunications("Exiting the packet normalizer", INK.CLR_RED);
        }

        private void ProcessRecievedData()
        {
            if (svr == null)
                return;

            while (svr.IsListening)
            {
                //Debug.WriteLine("Before AutoEvent");
                autoEvent2.WaitOne();//wait at mutex until signal
                //Debug.WriteLine("After AutoEvent");

                try
                {
                    while (FullPacketList.Count > 0)
                    {
                        FullPacket fp;
                        lock (FullPacketList)
                            fp = FullPacketList.Dequeue();
                        //Console.WriteLine(GetDateTimeFormatted +" - Full packet fromID: " + fp.iFromClient.ToString() + ", Type: " + ((PACKETTYPES)fp.ThePacket[0]).ToString());
                        UInt16 type = (ushort)(fp.ThePacket[1] << 8 | fp.ThePacket[0]);
                        switch (type)//Interrigate the first 2 Bytes to see what the packet TYPE is
                        {
                            case (UInt16)PACKETTYPES.TYPE_MyCredentials:
                                {
                                    PostUserCredentials(fp.iFromClient, fp.ThePacket);
                                    //SendRegisteredMessage(fp.iFromClient, fp.ThePacket);
                                }
                                break;
                            case (UInt16)PACKETTYPES.TYPE_CredentialsUpdate:
                                break;
                            case (UInt16)PACKETTYPES.TYPE_PingResponse:
                                //Debug.WriteLine(DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToLongTimeString() + " - Received Ping from: " + fp.iFromClient.ToString() + ", on " + DateTime.Now.ToShortDateString() + ", at: " + DateTime.Now.ToLongTimeString());
                                UpdateTheConnectionTimers(fp.iFromClient, fp.ThePacket);
                                break;
                            case (UInt16)PACKETTYPES.TYPE_Close:
                                ClientDisconnect(fp.iFromClient);
                                break;
                            case (UInt16)PACKETTYPES.TYPE_Message:
                                {
                                    AssembleMessage(fp.iFromClient, fp.ThePacket);
                                }
                                break;
                            default:
                                PassDataThru(type, fp.iFromClient, fp.ThePacket);
                                break;
                        }
                    }//END  while (FullPacketList.Count > 0)
                }//try
                catch (Exception ex)
                {
                    try
                    {
                        string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;
                        OnCommunications($"EXCEPTION in  ProcessRecievedData - {msg}", INK.CLR_RED);
                    }
                    catch { }
                }

                if (ServerIsExiting)
                    break;
            }//End while (svr.IsListening)

            string info2 = string.Format("AppIsExiting = {0}", ServerIsExiting.ToString());
            string info3 = string.Format("Past the ProcessRecievedData loop");

            Debug.WriteLine(info2);
            Debug.WriteLine(info3);

            try
            {
                OnCommunications(info3, INK.CLR_RED);// "Past the ProcessRecievedData loop" also is logged to InfoLog.log

            }
            catch { }

            if (!ServerIsExiting)
            {
                //if we got here then something went wrong, we need to shut down the service
                OnCommunications("SOMETHING CRASHED", INK.CLR_RED);
            }
        }

        private void PassDataThru(UInt16 type, int MessageFrom, byte[] message)
        {
            try
            {
                int ForwardTo = (int)message[11] << 24 | (int)message[10] << 16 | (int)message[9] << 8 | (int)message[8];

                //Stuff in who this packet is from so we know who sent it
                byte[] x = BitConverter.GetBytes(MessageFrom);
                message[12] = (byte)x[0];//idFrom
                message[13] = (byte)x[1];//idFrom
                message[14] = (byte)x[2];//idFrom
                message[15] = (byte)x[3];//idFrom

                if (ForwardTo > 0)
                    svr.SendMessage(ForwardTo, message);
                else
                    svr.SendMessage(message);
            }
            catch (Exception ex)
            {
                string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;
                OnCommunications($"EXCEPTION in  PassDataThru - {msg}", INK.CLR_RED);
            }
        }

        #endregion

        StringBuilder sb = null;
        private void AssembleMessage(int clientID, byte[] message)
        {
            try
            {
                PACKET_DATA IncomingData = new PACKET_DATA();
                IncomingData = (PACKET_DATA)PACKET_FUNCTIONS.ByteArrayToStructure(message, typeof(PACKET_DATA));

                switch (IncomingData.Data_Type)
                {
                    case (UInt16)PACKETTYPES_SUBMESSAGE.SUBMSG_MessageStart:
                        {
                            if (svr.workerSockets.ContainsKey(clientID))
                            {
                                OnCommunications(
                                    $"Client '{svr.workerSockets[clientID].szClientName}' sent some numbers and some text... num1= {IncomingData.Data16} and num2= {IncomingData.Data17}:",
                                    INK.CLR_BLUE);
                                OnCommunications($"Client also said:", INK.CLR_BLUE);

                                //sb = new StringBuilder(new string(IncomingData.szStringDataA).TrimEnd('\0'));
                                OnCommunications(new string(IncomingData.szStringDataA).TrimEnd('\0'), INK.CLR_GREEN);
                            }
                        }
                        break;
                    case (UInt16)PACKETTYPES_SUBMESSAGE.SUBMSG_MessageGuts:
                        {
                            //sb.Append(new string(IncomingData.szStringDataA).TrimEnd('\0'));
                            OnCommunications(new string(IncomingData.szStringDataA).TrimEnd('\0'), INK.CLR_GREEN);
                        }
                        break;
                    case (UInt16)PACKETTYPES_SUBMESSAGE.SUBMSG_MessageEnd:
                        {
                            //sb = new StringBuilder(new string(IncomingData.szStringDataA).TrimEnd('\0'));
                            OnCommunications("FINISHED GETTING MESSAGE", INK.CLR_BLUE);

                            /****************************************************************/
                            //Now tell the client teh message was received!
                            PACKET_DATA xdata = new PACKET_DATA();

                            xdata.Packet_Type = (UInt16)PACKETTYPES.TYPE_MessageReceived;

                            byte[] byData = PACKET_FUNCTIONS.StructureToByteArray(xdata);

                            svr.SendMessage(clientID, byData);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_AssembleMessage", ex.ToString());

                Console.WriteLine("ERROR Assembling message");
            }
        }


        private void UpdateTheConnectionTimers(int clientNumber, byte[] message)
        {
            lock (svr.workerSockets)
            {
                try
                {
                    if (svr.workerSockets.ContainsKey(clientNumber))
                    {
                        svr.workerSockets[clientNumber].dTimer = DateTime.Now;
                        Int64 elapsedTime = svr.workerSockets[clientNumber].PingStatClass.StopTheClock();
                        //Console.WriteLine("UpdateTheConnectionTimers: " + ConnectionID.ToString());
                        //Debug.WriteLine("Ping Time for " + ConnectionID.ToString() + ": " + elapsedTime.ToString() + "ms");

                        PACKET_DATA IncomingData = new PACKET_DATA();
                        IncomingData = (PACKET_DATA)PACKET_FUNCTIONS.ByteArrayToStructure(message, typeof(PACKET_DATA));

                        int ThisClientsMaxReturnDataSetValue = IncomingData.Data16;


                        Console.WriteLine($"Ping From Server to client: {elapsedTime}ms");

                        UpdateThePingTimeFromThread(clientNumber, elapsedTime);
                        /****************************************************************************************/

                    }
                }
                catch (Exception ex)
                {
                    string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;
                    Utilities.WriteErrorLog("frmServer_UpdateTheConnectionTimers", ex.ToString());
                    OnCommunications($"EXCEPTION in UpdateTheConnectionTimers - {msg}", INK.CLR_RED);
                }
            }
        }

        private delegate void UpdateThePingTimeFromThreadDelegate(int clientNumber, long elapsedTimeInMilliseconds);
        private void UpdateThePingTimeFromThread(int clientNumber, long elapsedTimeInMilliseconds)
        {
            if (InvokeRequired)
            {
                this.Invoke(new UpdateThePingTimeFromThreadDelegate(UpdateThePingTimeFromThread), new object[] { clientNumber, elapsedTimeInMilliseconds });
                return;
            }

            listView1.Items[clientNumber.ToString()].SubItems[6].Text = string.Format("{0:0.##}ms", elapsedTimeInMilliseconds);
        }

        private void SetHostNameAndAddress()
        {
            string strHostName = Dns.GetHostName();

            labelMyIP.Text = $"Host Name: {strHostName}, Listening on Port: {MyPort}";
        }

        private void CheckOnApplicationDirectory()
        {
            try
            {
                string AppPath = GeneralFunction.GetAppPath;

                if (!Directory.Exists(AppPath))
                {
                    Directory.CreateDirectory(AppPath);
                }
            }
            catch (Exception ex)
            {
                string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;
                Utilities.WriteErrorLog("frmServer_CheckOnApplicationDirectory", ex.ToString());
                OnCommunications($"EXCEPTION: ISSUE CREATING A DIRECTORY - {msg}", INK.CLR_RED);
            }
        }

        #region PACKET MESSAGES
        private void RequestNewConnectionCredentials(int ClientID)
        {
            try
            {
                PACKET_DATA xdata = new PACKET_DATA();

                xdata.Packet_Type = (UInt16)PACKETTYPES.TYPE_RequestCredentials;
                xdata.Data_Type = 0;
                xdata.Packet_Size = 16;
                xdata.maskTo = 0;
                xdata.idTo = (UInt16)ClientID;
                xdata.idFrom = 0;

                xdata.DataLong1 = DateTime.UtcNow.Ticks;

                if (!svr.workerSockets.ContainsKey(ClientID))
                    return;

                lock (svr.workerSockets)
                {
                    //ship back their address for reference to the client
                    string clientAddr = ((IPEndPoint)svr.workerSockets[ClientID].UserSocket.RemoteEndPoint).Address.ToString();
                    clientAddr.CopyTo(0, xdata.szStringDataA, 0, clientAddr.Length);

                    byte[] byData = PACKET_FUNCTIONS.StructureToByteArray(xdata);

                    if (svr.workerSockets[ClientID].UserSocket.Connected)
                    {
                        svr.SendMessage(ClientID, byData);
                        Debug.WriteLine(DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToLongTimeString() + " - from " + ClientID.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_RequestNewConnectionCredentials", ex.ToString());
            }
        }

        private void SendMessageOfClientDisconnect(int clientId)
        {
            try
            {
                PACKET_DATA xdata = new PACKET_DATA();

                xdata.Packet_Type = (UInt16)PACKETTYPES.TYPE_ClientDisconnecting;
                xdata.Data_Type = 0;
                xdata.Packet_Size = (UInt16)Marshal.SizeOf(typeof(PACKET_DATA));
                xdata.maskTo = 0;
                xdata.idTo = 0;
                xdata.idFrom = (UInt32)clientId;

                byte[] byData = PACKET_FUNCTIONS.StructureToByteArray(xdata);
                svr.SendMessage(byData);
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_SendMessageOfClientDisconnect", ex.ToString());
            }
        }

        private void SendRegisteredMessage(int clientId, byte[] message)
        {
            PACKET_DATA IncomingData = new PACKET_DATA();
            IncomingData = (PACKET_DATA)PACKET_FUNCTIONS.ByteArrayToStructure(message, typeof(PACKET_DATA));

            try
            {
                PACKET_DATA xdata = new PACKET_DATA();

                xdata.Packet_Type = (UInt16)PACKETTYPES.TYPE_Registered;
                xdata.Data_Type = 0;
                xdata.Packet_Size = (UInt16)Marshal.SizeOf(typeof(PACKET_DATA));
                xdata.maskTo = 0;
                xdata.idTo = 0;
                xdata.idFrom = (UInt32)clientId;

                xdata.Data6 = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.Major;
                xdata.Data7 = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.Minor;
                xdata.Data8 = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.Build;
                //xdata.Data9 = MySettings.CurrentServiceFeatureVer;

                byte[] byData = PACKET_FUNCTIONS.StructureToByteArray(xdata);
                svr.SendMessage(byData);
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_SendRegisteredMessage", ex.ToString());
            }

        }
        #endregion

        #region WEB CONTROL
        private bool BrowserVersion()
        {
            Debug.WriteLine("CommunicationsDisplay.Version: " + CommunicationsDisplay.Version.ToString());

            if (CommunicationsDisplay.Version.Major < 9)
            {
                MessageBox.Show(this, "You must update your web browser to Internet Explorer 9 or greater to see the service output information!", "Message", MessageBoxButtons.OK);
                return false;
            }

            return true;
        }

        private delegate void OnCommunicationsDelegate(string str, INK iNK);
        private void OnCommunications(string str, INK iNK)
        {
            if (ValidBrowser == false)
            {
                System.Diagnostics.Debug.WriteLine("INVALID BROWSER, must update Internet Explorer to version 8 or better!!");
                return;
            }
            Int32 line = 0;
            //System.Diagnostics.Debug.WriteLine("~~~~~~ OnCommunications 1:");
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new OnCommunicationsDelegate(OnCommunications), new object[] { str, iNK });
                    return;
                }

                //  System.Diagnostics.Debug.WriteLine("~~~~~~ OnCommunications 2");
                HtmlDocument doc = CommunicationsDisplay.Document;
                line = 1;
                //System.Diagnostics.Debug.WriteLine("~~~~~~ OnCommunications 3");
                string style = String.Empty;
                if (iNK.Equals(INK.CLR_GREEN))
                    style = "font-family:Lucida Console;font-size:10px;color:Green;";
                else if (iNK.Equals(INK.CLR_BLUE))
                    style = "font-family:Lucida Console;font-size:10px;color:Blue;";
                else if (iNK.Equals(INK.CLR_RED))
                    style = "font-family:Lucida Console;font-size:10px;color:Red;";
                else if (iNK.Equals(INK.CLR_PURPLE))
                    style = "font-family:Lucida Console;font-size:10px;color:Purple;";
                else
                    style = "font-family:Lucida Console;font-size:10px;color:Black;";
                line = 2;
                //System.Diagnostics.Debug.WriteLine("~~~~~~ OnCommunications 4");
                //doc.Write(String.Format("<div style=\"{0}\">{1}</div><br />", style, str));
                doc.Write(String.Format("<div style=\"{0}\">{1} {2}</div>", style, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), str));
                //doc.Body.ScrollTop = int.MaxValue;
                //CommunicationsDisplay.Document.Window.ScrollTo(0, int.MaxValue);
                line = 3;
                ScrollMessageIntoView();
                //System.Diagnostics.Debug.WriteLine("~~~~~~ OnCommunications 5");
                line = 4;
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"EXCEPTION IN OnCommunications @ Line: {line}, {ex.Message}");

            }
        }

        /// <summary>
        /// force the web control to the last item in the window... set to the bottom for the latest activity
        /// </summary>
        private void ScrollMessageIntoView()
        {
            // MOST IMP : processes all windows messages queue
            System.Windows.Forms.Application.DoEvents();

            if (CommunicationsDisplay.Document != null)
            {
                CommunicationsDisplay.Document.Window.ScrollTo(0, CommunicationsDisplay.Document.Body.ScrollRectangle.Height);
            }
        }

        private void ClearEventAndStatusDisplays()
        {
            // Clear communications
            displayReady = false;
            CommunicationsDisplay.Navigate("about:blank");
            while (!displayReady)
            {
                Application.DoEvents();
            }
        }

        private void CommunicationsDisplay_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            //Debug.WriteLine("CommunicationsDisplay_Navigated");
            //OnCommunications("........", INK.CLR_BLACK);
            displayReady = true;
        }
        #endregion

        #region UNSAFE CODE
        // The unsafe keyword allows pointers to be used within the following method:
        static unsafe void Copy(byte[] src, int srcIndex, byte[] dst, int dstIndex, int count)
        {
            try
            {
                if (src == null || srcIndex < 0 || dst == null || dstIndex < 0 || count < 0)
                {
                    Console.WriteLine("Serious Error in the Copy function 1");
                    throw new System.ArgumentException();
                }

                int srcLen = src.Length;
                int dstLen = dst.Length;
                if (srcLen - srcIndex < count || dstLen - dstIndex < count)
                {
                    Console.WriteLine("Serious Error in the Copy function 2");
                    throw new System.ArgumentException();
                }

                // The following fixed statement pins the location of the src and dst objects
                // in memory so that they will not be moved by garbage collection.
                fixed (byte* pSrc = src, pDst = dst)
                {
                    byte* ps = pSrc + srcIndex;
                    byte* pd = pDst + dstIndex;

                    // Loop over the count in blocks of 4 bytes, copying an integer (4 bytes) at a time:
                    for (int i = 0; i < count / 4; i++)
                    {
                        *((int*)pd) = *((int*)ps);
                        pd += 4;
                        ps += 4;
                    }

                    // Complete the copy by moving any bytes that weren't moved in blocks of 4:
                    for (int i = 0; i < count % 4; i++)
                    {
                        *pd = *ps;
                        pd++;
                        ps++;
                    }
                }
            }
            catch (Exception ex)
            {
                var exceptionMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                Utilities.WriteDebugLog("frmServer_Copy", ex.ToString());

                Debug.WriteLine("EXCEPTION IN: Copy - " + exceptionMessage);
            }

        }
        #endregion
        private HubConnection _connection;
        public void ProcessData(string data)
        {
            //messagesList.Items.Add(data);
            Utilities.WriteDebugLog("ProcessData", $"Nhận yêu cầu mở khóa => message = {data}");
            OnCommunications($"Nhận yêu cầu mở khóa => message = {data}", INK.CLR_BLUE);
            var device = Newtonsoft.Json.JsonConvert.DeserializeObject<Device>(data);
            lock (svr.workerSockets)
            {
                // Get List of values. [3]
                List<UserSock> valueList = new List<UserSock>(svr.workerSockets.Values);
                if (valueList.Count == 0)
                {
                    Utilities.WriteDebugLog("ProcessData", $"Không có khóa nào kết nối tới server");
                    OnCommunications("Không có khóa nào kết nối tới server", INK.CLR_BLUE);
                    return;
                }
                // Display them.
                bool isSuccess = false;
                foreach (var value in valueList)
                {
                    if (value.szIMEI == device.IMEI)
                    {
                        UserSock userSock = svr.workerSockets[value.iClientID];
                        byte[] sendUnlock = { 0x78, 0x78, 0x11, 0x80, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x55, 0x4E, 0x4C, 0x4F, 0x43, 0x4B, 0x23, 0x00, 0x01, 0x53, 0x54, 0x0D, 0x0A };
                        Send(userSock.UserSocket, sendUnlock);
                        OnCommunications($"Unlock: ClientID = {value.iClientID};NO = {device.No}; IMEI = {value.szIMEI}", INK.CLR_BLUE);
                        isSuccess = true;
                    }
                }

                if (!isSuccess)
                {
                    Utilities.WriteDebugLog("ProcessData", $"Khóa không kết nối tới server. IMEI = {device.IMEI}");
                    OnCommunications($"Khóa không kết nối tới server. IMEI = {device.IMEI}", INK.CLR_BLUE);
                }
            }
        }

        public class Device
        {
            public string No { get; set; }
            public string IMEI { get; set; }
            public string MAC { get; set; }
        }

        private void InitHub()
        {
            _connection = new HubConnectionBuilder()
             .WithUrl(hub.Text, transports: HttpTransportType.LongPolling)
             .Build();
            _connection.HandshakeTimeout = new TimeSpan(0, 1, 0);
            _connection.ServerTimeout = new TimeSpan(0, 1, 0);
        }
        private async void _btnCN_ClickAsync(object sender, EventArgs e)
        {
            _btnCN.Enabled = false;
            lbConnect.Text = "Connecting...";
            lbConnect.ForeColor = Color.Blue;
            _connection.On<string>("DeviceReceiveMessage", (data) =>
            {
                if (this.InvokeRequired)
                    this.Invoke((MethodInvoker)delegate
                    {
                        ProcessData(data);
                    });
                else
                    ProcessData(data);

            });

            try
            {
                await _connection.StartAsync();
                OnCommunications($"Connect hub successfull: {hub.Text}", INK.CLR_GREEN);
                isRetry = false;
                timer1.Start();
                _btnDCN.Enabled = true;
                _btnCN.Visible = false;
                _btnDCN.Visible = true;
                //hub.Enabled = false;
                //txtPort.Enabled = false;
                lbConnect.Text = "Connceted";
                lbConnect.ForeColor = Color.Green;
            }
            catch (Exception ex)
            {
                Utilities.WriteDebugLog("frmServer_btnCN_ClickAsync", $"Connect hub  failed: {ex.Message}");

                _btnCN.Enabled = true;
                OnCommunications($"Connect hub  failed: {hub.Text}, exception: {ex.Message}", INK.CLR_RED);
                lbConnect.Text = "Disconnected";
                lbConnect.ForeColor = Color.Red;
            }
        }


        private void _btnDCN_Click(object sender, EventArgs e)
        {
            try
            {
                _btnDCN.Enabled = false;
                lbConnect.Text = "Disconnecting";
                timer1.Enabled = false;

                _connection.StopAsync();


                timer1.Stop();

                _btnCN.Enabled = true;
                _btnCN.Visible = true;
                _btnDCN.Visible = false;
                lbConnect.Text = "Disconnected";
                lbConnect.ForeColor = Color.Red;
                //hub.Enabled = true;
                //txtPort.Enabled = true;



            }
            catch (Exception ex)
            {
                _btnDCN.Enabled = true;
                Utilities.WriteDebugLog("frmServer_btnDCN_Click", ex.ToString());

                OnCommunications($"Disconnect hub  failed: {hub.Text}", INK.CLR_RED);
            }


        }

        private async void _timer_TickAsync(object sender, EventArgs e)
        {

            if (isRetry)
            {
                try
                {
                    OnCommunications($"Retry connection to hub: {hub.Text}", INK.CLR_BLUE);
                    lbConnect.Text = "Connecting...";
                    lbConnect.ForeColor = Color.Blue;
                    await _connection.StartAsync();

                    isRetry = false;
                    OnCommunications($"Retry connection to hub successfull: {hub.Text}", INK.CLR_GREEN);
                }
                catch (Exception ex)
                {
                    Utilities.WriteDebugLog("frmServer_Reconnect_Hub", $"{ex}");

                    OnCommunications($"Retry connection to hub failed: {hub.Text}", INK.CLR_RED);
                }
            }
            else
            {
                lbConnect.Text = "Connceted";
                lbConnect.ForeColor = Color.Green;
            }
        }
    }
}
