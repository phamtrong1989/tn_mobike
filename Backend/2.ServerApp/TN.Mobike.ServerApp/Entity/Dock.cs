﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Mobike.ServerApp.Entity
{
   public class Dock
    {
        public int Id { get; set; }
        public bool BikeAlarm { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedSystemUserId { get; set; }
        public int CurrentBikeId { get; set; }
        public string IMEI { get; set; }
        public bool LockStatus { get; set; }
        public string Model { get; set; }
        public int OrderNumber { get; set; }
        public string SerialNumber { get; set; }
        public int StationId { get; set; }
        public int Status { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedSystemUserId { get; set; }
        public int? BikeAlarmId { get; set; }
        public bool ErrorStatus { get; set; }
        public float Battery { get; set; }
        public bool Charging { get; set; }
    }
}
