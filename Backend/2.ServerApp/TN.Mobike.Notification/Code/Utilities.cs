﻿using FirebaseAdmin;
using FirebaseAdmin.Auth;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using log4net;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TN.Mobike.Controls.Code;

namespace TN.Mobike.SMSBox
{

    public class Functions
    {
        public static void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isExeption = false)
        {
            try
            {
                var date = DateTime.Now;
                Task.Run(() => MongoAddAsync(new MongoLog
                {
                    AccountId = accountId,
                    FunctionName = functionName,
                    LogKey = logKey,
                    CreateTime = $"{date:yyyy-MM-dd HH:mm:ss}",
                    Content = content,
                    CreateTimeTicks = date.Ticks,
                    DeviceKey = "",
                    IsExeption = isExeption,
                    SystemType = EnumSystemType.ControlService,
                    Type = type,
                    Date = Convert.ToInt32($"{date:yyyyMMdd}")
                })).ConfigureAwait(false);
            }
            catch { }
        }

        private static async Task MongoAddAsync(MongoLog data)
        {
            try
            {
                string dbName = SyncService.MongoDBSettings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<MongoLog>(SyncService.MongoDBSettings.MongoCollectionLog);
                await collection.InsertOneAsync(data);
            }
            catch { }
        }

        public static MongoDBSettings InitMongoDBSettings()
        {
            var path = $"{Environment.CurrentDirectory}/Configs/MongoDB.Settings.json";
            if (!File.Exists(path))
            {
                return null;
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<MongoDBSettings>(File.ReadAllText(path));
        }


        public static bool SendSMSs(TNGSMsms tngsmsms, SMSBox smsBox)
        {
            
            //Console.ForegroundColor = ConsoleColor.Green;
            //Console.WriteLine($"| TN.GSM.SMS connect : {tngsmsms.IsConnected}");
            //Console.ForegroundColor = ConsoleColor.White;
            //Console.WriteLine("+----------------------------------------------------------------------------------------+");
            try
            {
                if (tngsmsms.IsConnected)
                {
                    smsBox.Content = ConvertToUnSign3(smsBox.Content);
                    tngsmsms.Read();
                    var phones = smsBox.PhoneNumber;
                    if(string.IsNullOrEmpty(phones))
                    {
                        return true;
                    }
                    var arrPhone = phones.Split(',').ToList();
                    foreach(var phone in arrPhone)
                    {
                        var response = tngsmsms.Send(phone, smsBox.Content);
                        Utilities.WriteOperationLog($"[Functions_SendSMSs] : ", $"Send {phone}, content {smsBox.Content}, status {response.Status}");
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteOperationLog($"[Functions_SendSMSs] : ", $"{ex.ToString()}");
                return false;
            }

        }
        public static string ConvertToUnSign3(string s)
        {
            try
            {
                var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                string temp = s.Normalize(NormalizationForm.FormD);
                return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            }
            catch
            {
                return s;
            }
        }

        public static Tuple<bool, string> SendEmailAsync(EmailManage email, string to, string addEmail, string ccEmail, string bcEmail, string Subject, string Body)
        {
            try
            {
                var fromAddress = new MailAddress(email.Email);
                var toAddress = new MailAddress(to);
                var fromPassword = email.Password;
                var subject = Subject;
                var body = Body;

                var smtp = new SmtpClient
                {
                    Host = email.Host,
                    Port = email.Port,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                })
                {
                    if (!string.IsNullOrEmpty(addEmail))
                    {
                        message.To.Add(addEmail);
                    }
                    if (!string.IsNullOrEmpty(ccEmail))
                    {
                        message.CC.Add(ccEmail);
                    }
                    if (!string.IsNullOrEmpty(bcEmail))
                    {
                        message.Bcc.Add(bcEmail);
                    }
                    smtp.Send(message);
                    return new Tuple<bool, string>(true, null);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.ToString());
            }
        }
        public static decimal ToTotalPrice(
            ETicketType ticketType,
            DateTime startTime,
            decimal ticketPrice_TicketValue,
            int ticketPrice_LimitMinutes,
            int ticketPrice_BlockPerMinute,
            int ticketPrice_BlockPerViolation,
            decimal ticketPrice_BlockViolationValue,
            DateTime prepaid_EndTime,
            int prepaid_MinutesSpent
            )
        {
            var totalMinutes = Convert.ToInt32((DateTime.Now - startTime).TotalMinutes);
            int soPhutTinhPhuPhi;

            if (ticketType == ETicketType.Block)
            {
                soPhutTinhPhuPhi = totalMinutes - ticketPrice_BlockPerMinute;
                if (soPhutTinhPhuPhi <= 0)
                {
                    return ticketPrice_TicketValue;
                }
                return ticketPrice_TicketValue + ((soPhutTinhPhuPhi % ticketPrice_BlockPerViolation > 0) ? ((soPhutTinhPhuPhi / ticketPrice_BlockPerViolation) + 1) : (soPhutTinhPhuPhi / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;
            }
            else if (ticketType == ETicketType.Day)
            {
                var tongSoPhutConLai = ticketPrice_LimitMinutes - prepaid_MinutesSpent;
                // Không vượt quá thời gian black (22h đến 06h sáng ngày hôm sau)
                if (prepaid_EndTime > DateTime.Now)
                {
                    soPhutTinhPhuPhi = (totalMinutes < tongSoPhutConLai) ? 0 : totalMinutes - ticketPrice_LimitMinutes;
                }
                else // Đi quá 22h đêm
                {
                    // Tổng số phút còn lại lớn hơn tổng số phút từ bắt đầu đến thời điểm black
                    soPhutTinhPhuPhi = Convert.ToInt32((tongSoPhutConLai > (prepaid_EndTime - startTime).TotalMinutes) ? (totalMinutes - (prepaid_EndTime - startTime).TotalMinutes) : (totalMinutes - tongSoPhutConLai));
                }

                if (soPhutTinhPhuPhi <= 0)
                {
                    return 0;
                }

                return ((soPhutTinhPhuPhi % ticketPrice_BlockPerViolation > 0) ? ((soPhutTinhPhuPhi / ticketPrice_BlockPerViolation) + 1) : (soPhutTinhPhuPhi / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;

            }
            else if (ticketType == ETicketType.Month)
            {

                if (prepaid_EndTime > DateTime.Now)
                {
                    if (ticketPrice_LimitMinutes >= totalMinutes)
                    {
                        return 0;
                    }
                    soPhutTinhPhuPhi = totalMinutes - ticketPrice_LimitMinutes;
                }
                else
                {
                    soPhutTinhPhuPhi = Convert.ToInt32((DateTime.Now - prepaid_EndTime).TotalMinutes);
                }
                return ((soPhutTinhPhuPhi % ticketPrice_BlockPerViolation > 0) ? ((soPhutTinhPhuPhi / ticketPrice_BlockPerViolation) + 1) : (soPhutTinhPhuPhi / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;
            }
            return 0;
        }

       

       

        public static bool InitSettingNotification()
        {
            try
            {
                if (FirebaseMessaging.DefaultInstance == null)
                {
                    var path = $"{Environment.CurrentDirectory}/Configs/Notification.Settings.json";
                    var defaultApp = FirebaseApp.Create(new AppOptions()
                    {
                        Credential = GoogleCredential.FromFile(path),
                    });
                    var defaultAuth = FirebaseAuth.GetAuth(defaultApp);
                    defaultAuth = FirebaseAuth.DefaultInstance;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    
    public class Utilities
    {
        private static readonly ILog log = LogManager.GetLogger("TN.Mobike.SMSBox");
        public static bool ActiveOperationLog = true;
        public static bool ActiveDebugLog = true;
        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return (long)(datetime - sTime).TotalSeconds;
        }

        public static void StartLog(bool activeOperationLog, bool activeDebugLog)
        {
            ActiveOperationLog = activeOperationLog;
            ActiveDebugLog = activeDebugLog;
        }

        public static void CloseLog()
        {
            foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
            {
                app.Close();
            }
        }

        public static void WriteErrorLog(string logtype, string logcontent)
        {
            try
            {
                log.Error($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        public static void WriteOperationLog(string logtype, string logcontent)
        {
            if (!ActiveOperationLog)
                return;

            try
            {
                log.Info($"{logtype} \t {logcontent}");
            }
            catch 
            {
                // ignored
            }
        }

        public static void WriteDebugLog(string logtype, string logcontent)
        {
            if (!ActiveDebugLog)
                return;

            try
            {
                log.Debug($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                try
                {
                    if (stream != null)
                        stream.Close();
                }
                catch
                {

                }
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string GetBitStr(byte[] data)
        {
            BitArray bits = new BitArray(data);

            string strByte = string.Empty;
            for (int i = 0; i <= bits.Count - 1; i++)
            {
                if (i % 8 == 0)
                {
                    strByte += " ";
                }
                strByte += (bits[i] ? "1" : "0");
            }

            return strByte;
        }

        public static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
    }

    public static class StaticExtensions
    {
        public static string GetDisplayName(this Enum value)
        {
            try
            {
                Type enumType = value.GetType();
                var enumValue = Enum.GetName(enumType, value);
                MemberInfo member = enumType.GetMember(enumValue)[0];

                var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
                var outString = ((DisplayAttribute)attrs[0]).Name;

                if (((DisplayAttribute)attrs[0]).ResourceType != null)
                {
                    outString = ((DisplayAttribute)attrs[0]).GetName();
                }
                return outString;
            }
            catch
            {
                return value.ToString();
            }

        }
    }
}
