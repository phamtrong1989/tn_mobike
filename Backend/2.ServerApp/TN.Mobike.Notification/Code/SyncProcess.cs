﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using TN.Mobike.Controls.Code;

namespace TN.Mobike.SMSBox
{
    public class SyncProcess
    {

        private static readonly Timer autoSendSMS = new Timer() { Interval = 1000 * 10, Enabled = true, AutoReset = true };
        private static readonly Timer autoSendEmail = new Timer() { Interval = 1000 * 10, Enabled = true, AutoReset = true };
        private static readonly Timer publicNotify = new Timer() { Interval = 1000 * 1, Enabled = true, AutoReset = true };


        private static bool IsAutoSmsJob = false;
        private static bool IsAutoEmailJob = false;
        private static bool IsPublicNotify = false;

        private static List<Flag> flags = new List<Flag>();
        public static void Stop()
        {

            autoSendSMS.Stop();
            autoSendSMS.Dispose();

            autoSendEmail.Stop();
            autoSendEmail.Dispose();

            publicNotify.Stop();
            publicNotify.Dispose();
        }

        public static void Start()
        {
            Utilities.WriteOperationLog($"[SyncProcess_Start] : ", $"Start");
            publicNotify.Elapsed += new ElapsedEventHandler((s, x) => PublicNotify());
            autoSendSMS.Elapsed += new ElapsedEventHandler((s, x) => AutoSendSms());
            autoSendEmail.Elapsed += new ElapsedEventHandler((s, x) => AutoSendEmail());
        }

        public static void PublicNotify()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            try
            {
                if (IsPublicNotify)
                {
                    return;
                }
                IsPublicNotify = true;
                var listData = Controller.NotificationJobSearch();
                if (listData.Any())
                {
                    Functions.AddLog(0, "SyncProcess.PublicNotify", requestId, EnumMongoLogType.Start, $"Tìm thấy {listData.Count} bản ghi notify");
                    foreach (var item in listData)
                    {
                        if (item.AccountId > 0)
                        {
                            Controller.SendNotificationByAccount("SyncProcess.PublicNotify", requestId, item.AccountId, item.Tile, item.Message, true, item.DataDockEventType, item.DataContent);
                        }
                        else
                        {
                            Controller.SendNotificationByTopicAll("SyncProcess.PublicNotify", requestId, item.Tile, item.Message, true, item.DataDockEventType, item.DataContent);
                        }
                        var isSet = Controller.NotificationJobSetIsSend(item.Id);
                        if (isSet)
                        {
                            Functions.AddLog(0, "SyncProcess.PublicNotify", requestId, EnumMongoLogType.Center, $"Gửi thành công bản tin #{item.Id}, tiêu đề '{item.Tile}', nội dung '{item.Message}', Tài khoản '{item.AccountId}'");
                        }
                    }
                    Functions.AddLog(0, "SyncProcess.PublicNotify", requestId, EnumMongoLogType.End, $"Kết thúc");
                }
                IsPublicNotify = false;
            }
            catch (Exception ex)
            {
                IsPublicNotify = false;
                Functions.AddLog(0, "SyncProcess.PublicNotify", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        private static void AutoSendSms()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            try
            {
                if (IsAutoSmsJob)
                {
                    return;
                }
                IsAutoSmsJob = true;

                var tngsMsms = new TNGSMsms();
                tngsMsms.Connect();

                var smsBoxes = Controller.SMSBoxGet();
                if (smsBoxes != null)
                {
                    foreach (var item in smsBoxes)
                    {
                        var kt = Functions.SendSMSs(tngsMsms, item);
                        if (kt)
                        {
                            item.SendDate = DateTime.Now;
                            item.SuccessfulSendDate = DateTime.Now;
                            item.IsSend = true;
                        }
                        else
                        {
                            item.SendDate = DateTime.Now;
                            //item.SuccessfulSendDate = DateTime.Now;
                            item.IsSend = false;
                        }
                        var ktUpdateDB = Controller.SMSBoxUpdateSend(item.Id, item);
                        System.Threading.Thread.Sleep(100);

                    }
                }
                tngsMsms.Disconnect();
                IsAutoSmsJob = false;
            }
            catch (Exception ex)
            {
                IsAutoSmsJob = false;
                Functions.AddLog(0, "SyncProcess.AutoSendSms", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        private static void AutoSendEmail()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            try
            {
                if (IsAutoEmailJob)
                {
                    return;
                }

                IsAutoEmailJob = true;
                var config = Controller.EmailManageGet();
                if (config != null)
                {
                    var list = Controller.EmailBoxSearchTop(300);

                    foreach (var item in list)
                    {
                        var ktUpdateDB = Controller.EmailBoxUpdateStatus(item.Id, true);
                        var kt = Functions.SendEmailAsync(config, item.To, item.Add, item.Cc, item.Bc, item.Subject, item.Body);
                        System.Threading.Thread.Sleep(100);
                    }
                }
                IsAutoEmailJob = false;
            }
            catch (Exception ex)
            {
                IsAutoEmailJob = false;
                Functions.AddLog(0, "SyncProcess.AutoSendEmail", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
    }
}