﻿using Dapper;
using FirebaseAdmin.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace TN.Mobike.SMSBox
{
    public class Controller
    {
        public async static void SendNotificationByTopicAll(string functionName, string requestId, string title, string message, bool isShow, byte dataType, string dataContent)
        {
            try
            {
                await NotificationAdd(new NotificationData
                {
                    AccountId = 0,
                    CreatedDate = DateTime.Now,
                    DeviceId = "",
                    Tile = title,
                    Message = message,
                    TransactionCode = "SV",
                    Type = 0,
                    Icon = ""
                });

                var data = await FirebaseMessaging.DefaultInstance.SendAsync(new Message()
                {
                    Notification = new Notification
                    {
                        Title = title,
                        Body = message
                    },
                    Data = new Dictionary<string, string>() {
                        { "isShow", isShow.ToString() },
                        { "type", dataType.ToString() },
                        { "time", DateTime.Now.Ticks.ToString() },
                        { "content", dataContent }
                    },
                    Topic = "all",
                    Apns = new ApnsConfig
                    {
                        Aps = new Aps { ContentAvailable = true },
                        Headers = new Dictionary<string, string>() {
                            { "apns-push-type","alert" },
                            { "apns-priority","5" },
                            { "apns-topic", "all" }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, ex.ToString(), true);
            }
        }

        public static bool NotificationJobSetIsSend(int id)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    db.Execute($"UPDATE [dbo].[NotificationJob] Set IsSend = 1, SendDate = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}' where id = {id}", commandType: CommandType.Text);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobSetIsSend", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return false;
            }
        }


        public static async Task NotificationAdd(NotificationData data)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        INSERT INTO [dbo].[Notification]
                                   ([AccountId]
                                   ,[DeviceId]
                                   ,[Icon]
                                   ,[TransactionCode]
                                   ,[Type]
                                   ,[Tile]
                                   ,[Message]
                                   ,[CreatedDate])
                             VALUES
                                   ({data.AccountId}
                                   ,'{data.DeviceId}'
                                   ,'{data.Icon}'
                                   ,'{data.TransactionCode}'
                                   ,0
                                   ,N'{data.Tile}'
                                   ,N'{data.Message}'
                                   ,'{DateTime.Now:yyyy-MM-dd HH:mm:ss}')
                        ";
                    var rs = await db.ExecuteAsync(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationAdd", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        public static List<AccountDevice> AccountDeviceSearch(int accountId)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[AccountDevice] where AcountId = {accountId} and CloudMessagingToken <> ''";
                    return db.Query<AccountDevice>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<AccountDevice>();
            }
        }

        public async static void SendNotificationByAccount(string functionName, string requestId, int accoutnId, string title, string message, bool isShow, byte dataType, string dataContent)
        {
            try
            {
                await NotificationAdd(new NotificationData
                {
                    AccountId = accoutnId,
                    CreatedDate = DateTime.Now,
                    DeviceId = "",
                    Tile = title,
                    Message = message,
                    TransactionCode = "SV",
                    Type = 0,
                    Icon = ""
                });

                var getDevices = AccountDeviceSearch(accoutnId);

                foreach (var item in getDevices)
                {
                    var data = await FirebaseMessaging.DefaultInstance.SendAsync(new Message()
                    {
                        Notification = new Notification
                        {
                            Title = title,
                            Body = message
                        },
                        Token = item.CloudMessagingToken,
                        Data = new Dictionary<string, string>() {
                            { "isShow", isShow.ToString() },
                            { "type", dataType.ToString() },
                            { "time", DateTime.Now.Ticks.ToString() },
                            { "content", dataContent }
                        },
                        Apns = new ApnsConfig
                        {
                            Aps = new Aps { ContentAvailable = true },
                            Headers = new Dictionary<string, string>() {
                            { "apns-push-type","alert" },
                            { "apns-priority","5" },
                            { "apns-topic", "" }
                        }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, ex.ToString(), true);
            }
        }

        public static List<NotificationJob> NotificationJobSearch()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT top 500 * FROM [dbo].[NotificationJob] where IsSend = 0 and PublicDate <= '{DateTime.Now:yyyy-MM-dd HH:mm}' order by PublicDate asc";
                    return db.Query<NotificationJob>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<NotificationJob>();
            }
        }

        public static bool SMSBoxUpdateSend(int id, SMSBox smsBox)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    db.Execute($"UPDATE [dbo].[SMSBox] Set IsSend = {(smsBox.IsSend ? 1:0)}, SendDate = '{smsBox.SendDate:yyyy-MM-dd HH:mm:ss}',  SuccessfulSendDate = '{smsBox.SuccessfulSendDate:yyyy-MM-dd HH:mm:ss}' where id = {id}", commandType: CommandType.Text);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog($"[Controller_SMSBoxUpdateSendSuccess] : ", $"{ex.ToString()}");

                return false;
            }
        }

        public static List<SMSBox> SMSBoxSearchTop(int top)
        {
            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                string sql = $@"
                        SELECT top {top} * 
                        FROM [dbo].[SMSBox]
                        WHERE IsSend = 0 and [To] <> '' and NotSend = 0 and SendDate <= '{DateTime.Now:yyyy-MM-dd HH:mm:ss}'
                ";
                return db.Query<SMSBox>(sql, commandType: CommandType.Text).ToList();
            }
        }

        public static List<SMSBox> SMSBoxGet()
        {
            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                string sql = $"SELECT * FROM [dbo].[SMSBox] where IsSend = 0 and SendDate = CreatedDate and CreatedDate = SuccessfulSendDate";
                return db.Query<SMSBox>(sql, commandType: CommandType.Text).ToList();
            }
        }


        


    

        public static void SMSBoxAdd(DateTime sendDate)
        {
            try
            {
                
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                            INSERT INTO [dbo].[SMSBox]
                                       ([PhoneNumber]
                                       ,[NotificationDataId]
                                       ,[Content]
                                       ,[IsSend]
                                       ,[ReSend]
                                       ,[SendDate]
                                       ,[CreatedDate]
                                       ,[SuccessfulSendDate])
                                 VALUES
                                       (<PhoneNumber, nvarchar(50),>
                                       ,<NotificationDataId, int,>
                                       ,<Content, nvarchar(200),>
                                       ,<IsSend, bit,>
                                       ,<ReSend, int,>
                                       ,<SendDate, datetime2(7),>
                                       ,<CreatedDate, datetime2(7),>
                                       ,<SuccessfulSendDate, datetime2(7),>)

                           
                    ";
                    db.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog($"[Controller_SMSBoxAdd] : ", $"{ex.ToString()}");

            }
        }


        #region [EmailJobDb]

        public static bool EmailBoxUpdateStatus(int id, bool status)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    db.Execute($"UPDATE [dbo].[EmailBox] Set IsSend = {(status ? 1 : 0)}, SuccessfulSendDate = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}' where id = {id}", commandType: CommandType.Text);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog($"[Controller_EmailBoxUpdateStatus] : ", $"{ex.ToString()}");
                return false;
            }
        }

        public static List<EmailBox> EmailBoxSearchTop(int top)
        {
            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                string sql = $@"
                        SELECT top {top} * 
                        FROM [dbo].[EmailBox]
                        WHERE IsSend = 0 and [To] <> '' and NotSend = 0 and SendDate <= '{DateTime.Now:yyyy-MM-dd HH:mm:ss}'
                ";
                return db.Query<EmailBox>(sql, commandType: CommandType.Text).ToList();
            }
        }

        public static EmailManage EmailManageGet()
        {
            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                string sql = $"SELECT top 1 * FROM [dbo].[EmailManage]";
                return db.Query<EmailManage>(sql, commandType: CommandType.Text).FirstOrDefault();
            }
        }

        #endregion

    }
}