﻿using System;
using System.Configuration;
using System.Globalization;

namespace TN.Mobike.SMSBox
{
    class AppSettings
    {
        public static string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        public static string ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        public static string ServiceDisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"];
        public static string ServiceDescription = ConfigurationManager.AppSettings["ServiceDescription"];
        public static string APIToken = ConfigurationManager.AppSettings["APIToken"];
    }
}
