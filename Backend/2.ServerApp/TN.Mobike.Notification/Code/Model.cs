﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TN.Mobike.SMSBox
{
    public class MongoDBSettings
    {
        public bool Is { get; set; }
        public string MongoClient { get; set; }
        public string MongoDataBase { get; set; }
        public string MongoCollection { get; set; }
        public bool IsUseMongo { get; set; }
        public string MongoDataBaseLog { get; set; }
        public string MongoCollectionLog { get; set; }
    }

    public class EmailManage
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string Host { get; set; }
        public string From { get; set; }
        public string CustomerDates { get; set; }
        public string CustomerTime { get; set; }
        public bool CustomerStatus { get; set; }
        public string EmployeeTime { get; set; }
        public bool EmployeeStatus { get; set; }
        public string AddEmail { get; set; }
        public string CCEmail { get; set; }
        public string BCEmail { get; set; }
    }

    public class Flag
    {
        public long Id { get; set; }
        public string Transaction { get; set; }
        public int Type { get; set; }
        public bool IsProcess { get; set; }
        public DateTime UpdatedTime { get; set; }
    }

    public class NotificationData
    {

        public long Id { get; set; }
        public int AccountId { get; set; }

        public string DeviceId { get; set; }

        public string Icon { get; set; }

        public string TransactionCode { get; set; }

        public int Type { get; set; }

        [MaxLength(200)]
        public string Tile { get; set; }

        [MaxLength(500)]
        public string Message { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    public enum EBookingStatus
    {
        [Display(Name = "Bắt đầu chuyến đi")]
        Start = 1,

        [Display(Name = "Kết thúc chuyến đi")]
        End = 2,

        [Display(Name = "Hủy chuyến")]
        Cancel = 3,

        [Display(Name = "Hủy chuyến bởi quản trị")]
        CancelByAdmin = 4,

        [Display(Name = "Kết thúc chuyến bởi quản trị")]
        EndByAdmin = 5,

        [Display(Name = "Nợ cước")]
        Debt = 6,
    }

    public enum ETicketType : byte
    {
        [Display(Name = "Vé block")]
        Block = 1,
        [Display(Name = "Vé ngày")]
        Day = 2,
        [Display(Name = "Vé tháng")]
        Month = 3
    }

    public class Booking
    {
        public int Id { get; set; }
        public string TicketPrepaidCode { get; set; }
        public int TicketPrepaidId { get; set; }
        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int StationIn { get; set; }

        public int? StationOut { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public byte Vote { get; set; }

        [MaxLength(1000)]
        public string Feedback { get; set; }

        public int TicketPriceId { get; set; }
        public ETicketType TicketPrice_TicketType { get; set; }
        [DataType("money")]
        public decimal TicketPrice_TicketValue { get; set; }
        // Cho phép trừ vào tài khoản phụ ko
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        // Là bảng giá mặc định
        public bool TicketPrice_IsDefault { get; set; }
        // Quá bao phút thì x tiền
        public int TicketPrice_BlockPerMinute { get; set; }
        // Số tiền x lên quá
        public int TicketPrice_BlockPerViolation { get; set; }
        public decimal TicketPrice_BlockViolationValue { get; set; }
        //Nếu là vé ngày: 1 ngày tối đi đa 12 tiếng = 720 phút,  Nếu vé tháng: Đi thoải mái các ngày trong tháng.Mỗi chuyến đi không quá 2 tiếng = 120 phút, Nếu vé quý: Tương tự tháng
        public int TicketPrice_LimitMinutes { get; set; }
        // Điểm trừ vào tài khoản chính
        public decimal ChargeInAccount { get; set; }
        // Điểm trừ vào tài khoản khuyến mãi
        public decimal ChargeInSubAccount { get; set; }
        // Total price
        public decimal TotalPrice { get; set; }
        // Thời gian giao dịch
        public DateTime? DateOfPayment { get; set; }
        public int TotalMinutes { get; set; }
        public DateTime CreatedDate { get; set; }
        public double? KCal { get; set; }
        public double? KG { get; set; }
        public double? KM { get; set; }
        public EBookingStatus Status { get; set; }
        public string Note { get; set; }
        public decimal TripPoint { get; set; }
        // Với trường hợp là vé trả trước dụng chung trường nhưng ý nghĩa khác nhau
        // Tổng số phút đã sử dụng trong ngày (vé ngày), chưa tính giao dịch hiện tại
        // Tổng số phút tối đa sử dụng 1 lần giao dịch (vé tháng)
        public int Prepaid_MinutesSpent { get; set; }
        // Ngày hiệu lực bắt đầu
        public DateTime? Prepaid_StartDate { get; set; }
        // Ngày hết hiệu lực của vé trả trước
        public DateTime? Prepaid_EndDate { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_StartTime { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_EndTime { get; set; }
        public decimal EstimateChargeInAccount { get; set; }
        public decimal EstimateChargeInSubAccount { get; set; }
    }

    public class Wallet
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public decimal Balance { get; set; }
        public decimal SubBalance { get; set; }
        public decimal TripPoint { get; set; }
        public string HashCode { get; set; }
        public byte Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }

    public class NotificationJob
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string DeviceId { get; set; }
        public string Icon { get; set; }
        public string Tile { get; set; }
        public string Message { get; set; }
        public DateTime PublicDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int LanguageId { get; set; }
        public bool IsSend { get; set; }
        public DateTime? SendDate { get; set; }
        public byte DataDockEventType { get; set; }
        public string DataTime { get; set; }
        public string DataContent { get; set; }
    }

    public class AccountDevice
    {
        public int Id { get; set; }
        public int? AcountId { get; set; }
        public string Token { get; set; }
        public string DeviceId { get; set; }
        public string IP { get; set; }
        public string CloudMessagingToken { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class WalletTransaction
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public int WalletId { get; set; }

        public int CampaignId { get; set; }

        public int Type { get; set; }

        public int TransactionId { get; set; }

        [MaxLength(50)]
        public string OrderIdRef { get; set; }

        [MaxLength(50)]
        public string TransactionCode { get; set; }

        public string HashCode { get; set; }

        [DataType("money")]
        public decimal Amount { get; set; }

        [DataType("money")]
        public decimal SubAmount { get; set; }

        [DataType("money")]
        public decimal? DebtAmount { get; set; }

        // Số point được + thêm theo sự kiện
        [DataType("money")]
        public decimal DepositEvent_SubAmount { get; set; }

        [DataType("money")]
        public decimal TotalAmount { get; set; }

        [DataType("money")]
        public decimal TripPoint { get; set; }

        public bool IsAsync { get; set; } = false;

        public byte Status { get; set; }

        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public byte PaymentGroup { get; set; }

        public DateTime? SetPenddingTime { get; set; }

        public DateTime? PadTime { get; set; }

        public string ReqestId { get; set; }

        [MaxLength(1000)]
        public string AdminNote { get; set; }

        public string WarningHistoryData { get; set; }

        public bool IsWarningHistory { get; set; }

        //Giá trị ví hiện tại trước khi giao dịch này +
        [DataType("money")]
        public decimal Wallet_Balance { get; set; }

        [DataType("money")]
        public decimal Wallet_SubBalance { get; set; }

        [DataType("money")]
        public string Wallet_HashCode { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public string Files { get; set; }

        public int GiftFromAccountId { get; set; }
        public int GiftToAccountId { get; set; }
    }

    public enum EnumMongoLogType
    {
        Start = 0,
        Center,
        End
    }

    public enum EnumSystemType
    {
        APIAPP = 0,
        APIBackend,
        DockService,
        ControlService
    }

    public class MongoLog
    {
        public ObjectId _id { get; set; }
        public EnumMongoLogType Type { get; set; }
        public string Content { get; set; }
        public int AccountId { get; set; }
        public string LogKey { get; set; }
        public string DeviceKey { get; set; }
        public string FunctionName { get; set; }
        public string CreateTime { get; set; }
        public EnumSystemType SystemType { get; set; }
        public long CreateTimeTicks { get; set; }
        public bool IsExeption { get; set; }
        public int Date { get; set; }
    }

    public class Bike
    {
        public enum EBikeStatus
        {
            Inactive = 0,
            Active = 1,
            Warehouse = 2,
            ErrorWarehouse = 3
        }

        public enum EBikeType
        {
            XeDap = 0,
            XeDapDien = 1,
            XeLai = 2
        }

        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int InvestorId { get; set; }
        public int ModelId { get; set; }
        public int ProducerId { get; set; }
        public int ColorId { get; set; }
        public int WarehouseId { get; set; }
        public string Images { get; set; }
        public string Plate { get; set; }
        public string SerialNumber { get; set; }
        public EBikeStatus Status { get; set; }
        public EBikeType Type { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
        public int StationId { get; set; }
        public int DockId { get; set; }
        public string Note { get; set; }
        public DateTime? ProductionDate { get; set; }
        public DateTime? StartDate { get; set; }
        public bool IsTrouble { get; set; } = false;
        public int MinuteRequiredMaintenance { get; set; } = 4800;
        public BikeInfo BikeInfo { get; set; }
        public int? BookingId { get; set; }
        public bool IsMaintenance { get; set; } = false;
        public DateTime? LastMaintenanceTime { get; set; }
        public int TotalMinutes { get; set; }
        public Station Station { get; set; }
    }

    public class BikeInfo
    {
        public int Id { get; set; }
        public int BikeId { get; set; }
        // Tổng số phút đi lũy kế
        public int TotalMinutes { get; set; } = 0;
        // Thời gian bảo dưỡng gần nhất
        public DateTime? LastMaintenanceTime { get; set; }
        //Tổng số giờ đã sử dụng cần bảo dưỡng
        public double HoursForMaintenance { get; set; }
        // Thời gian bảo dưỡng tiếp theo
        public DateTime? NextMaintenanceTime { get; set; }
        // Mô tả lỗi trước khi sửa chữa
        public string ErrorDescriptions { get; set; }
        // File đính kèm trước khi sửa chữa
        public string ErrorFiles { get; set; }
        public string SuppliesIds { get; set; }
        public string SuppliesNames { get; set; }
        // Mô tả lỗi trước khi sửa chữa
        public string RepairDescriptions { get; set; }
        // File đính kèm trước khi sửa chữa
        public string RepairFiles { get; set; }
        //Ngày sửa chữa
        public DateTime? RepairTime { get; set; }
        // Last repairId
        public int MaintenanceRepairId { get; set; }
        //
        public int MaintenanceRepairItemId { get; set; }
    }

    public class TotalMinutesModel
    {
        public int BikeId { get; set; }
        public int Total { get; set; }
    }
    public class SMSBox
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public int NotificationDataId { get; set; }
        public string Content { get; set; }
        public bool IsSend { get; set; }
        public int ReSend { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime SuccessfulSendDate { get; set; }

    }
    public class EmailBox
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Add { get; set; }
        public string Cc { get; set; }
        public string Bc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachments { get; set; }
        public bool IsSend { get; set; }
        public bool NotSend { get; set; } = false;
        public byte Type { get; set; }
        public int ObjectId { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? SuccessfulSendDate { get; set; }
    }

    public class BikeLowBatteryModel
    {
        public string Plate { get; set; }
        public string SerialNumber { get; set; }
        public string StationName1 { get; set; }
        public string StationName2 { get; set; }
        public string Address { get; set; }
        public double Battery { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int UserId { get; set; }
    }

    public enum EWarehouseType : byte
    {
        [Display(Name = "Kho tổng VTS")]
        Defaut,
        [Display(Name = "Kho điều phối bảo dưỡng VTS")]
        Virtual
    }

    public class Warehouse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public EWarehouseType Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int EmployeeId { get; set; }
        public List<Supplies> Suppliess { get; set; }
    }

    public enum RoleManagerType
    {
        Default = -1,
        [Display(Name = "IT trí Nam")]
        Admin = 0,

        [Display(Name = "Kế toán Trí Nam")]
        Accounting_TN = 1,

        [Display(Name = "Hội đồng quản trị VTS")]
        BOD_VTS = 2,

        [Display(Name = "Kế toán trưởng VTS")]
        KTT_VTS = 3,

        [Display(Name = "Chăm sóc khách hàng & kinh doanh VTS")]
        CSKH_VTS = 4,

        [Display(Name = "Cộng tác viên VTS")]
        CTV_VTS = 5,

        [Display(Name = "Điều phối bảo dưỡng VTS")]
        Coordinator_VTS = 6,

        [Display(Name = "Nhân viên cung ứng Trí Nam")]
        NVCU_TN = 9,

        [Display(Name = "Thủ kho VTS")]
        ThuKho_VTS = 10
    }

    public class ApplicationUser
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public string DisplayName { get; set; }
        public string FullName { get; set; }
        public bool IsLock { get; set; }
        public string NoteLock { get; set; }
        public bool IsReLogin { get; set; }
        public bool IsSuperAdmin { get; set; } = false;
        public string Note { get; set; }
        public int? CreatedUserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedUserId { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public DateTime? Birthday { get; set; }

        public byte Sex { get; set; }

        public int ManagerUserId { get; set; }

        public int RoleId { get; set; }

        public RoleManagerType RoleType { get; set; } = RoleManagerType.Default;

        // Nghỉ việc
        public bool IsRetired { get; set; }
        // Làm việc từ ngày
        public DateTime? WorkingDateFrom { get; set; }
        // Làm việc đến hết ngày
        public DateTime? WorkingDateTo { get; set; }
    }

    public enum ESuppliesType : byte
    {
        [Display(Name = "Vật tư mới")]
        New = 0,
        [Display(Name = "Vật tư cũ")]
        Old = 1
    }

    public enum ESuppliesStatus
    {
        Enabled,
        Disabled
    }

    public class Supplies
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public string Name { get; set; }

        public string Descriptions { get; set; }

        public int Quantity { get; set; }

        public string Unit { get; set; }

        public ESuppliesType Type { get; set; }

        public decimal Price { get; set; }

        public ESuppliesStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }
    }

    public class SuppliesWarehouse
    {
        public int Id { get; set; }
        public int SuppliesId { get; set; }
        public int WarehouseId { get; set; }
        public int Quantity { get; set; }
    }

    public class BikeHoursOperationModel
    {
        public int BikeId { get; set; }
        public double Total { get; set; }
    }

    public class Station
    {
        public int Id { get; set; }
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int Width { get; set; }
        public int Spaces { get; set; }
        public int Height { get; set; }

        [MaxLength(1000)]
        public string Address { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(200)]
        public string DisplayName { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public int TotalDock { get; set; } = 0;
        public byte Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int ManagerUserId { get; set; }
    }

    public class StationTotalBikeModel
    {
        public int StationId { get; set; }
        public int TotalBike { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Address { get; set; }
        public int Spaces { get; set; }
        public int ManagerUserId { get; set; }
        public string Note { get; set; }
    }

    public class TransactionsDebtChargesModel
    {
        public string TransactionCode { get; set; }
        public int AccountId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int ChargeDebt { get; set; }
        public int ManagerUserId { get; set; }
        public int SubManagerUserId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class BikeReportModel
    {
        public int Id { get; set; }
        public string TransactionCode { get; set; }
        public int BikeId { get; set; }
        public string Plate { get; set; }
        public int AccountId { get; set; }
        public DateTime CreatedDate { get; set; }
        public EBikeReportType Type { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public enum EBikeReportType
    {
        [Display(Name = "Lỗi khác")]
        Khac = 0,

        [Display(Name = "Lỗi mở khóa")]
        MoKhoa = 1,

        [Display(Name = "Lỗi QR code")]
        MaQR = 2,

        [Display(Name = "Xe bị phá hoại")]
        BiPhaHoai = 3,

        [Display(Name = "Trạm không xe")]
        TramTrongXe = 4,

        [Display(Name = "Khóa hỏng")]
        KhoaHong = 5,

        [Display(Name = "Không nạp được điểm")]
        KhongNapDuocDiem = 6,

        [Display(Name = "Không nhận được điểm")]
        KhongNhanDuocDiemKhiNap = 7
    }
}