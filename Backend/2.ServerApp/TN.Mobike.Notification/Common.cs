﻿namespace TN.GSTP.Client
{
    public static class Common
    {
        public static string ApplicationTransactionFolder = "";
        public static string ApplicationPrepayFolder = "";
        public static string ApplicationPriotyVehicleFolder = "";
        public static string ApplicationLaneImageFolder = "";
        public static string ApplicationLpnImageFolder = "";

        //WL_YYYYMMDD_HH24MISS_0001.CSV
        public const string WhiteVehicleListFileName = "WL_{0}_{1}.csv";
        //Etag_LPN_StartTime_EndTime_VehicleType_Type
        public const string WhiteVehicleListContent = "{0},{1},{2},{3},{4},{5};\r\n";

        //DL_YYYYMMDD_HH24MISS_0001.CSV
        public const string DiscountVehicleListFileName = "DL_{0}_{1}.csv";
        //DiscountId_Etag_LPN_StartTime_EndTime_VehicleType_Type
        public const string DiscountVehicleListContent = "{0},{1},{2},{3},{4},{5},{6};\r\n";

        //TL_YYYYMMDD_HH24MISS_0001.CSV
        public const string TicketCanceltListFileName = "TL_{0}_{1}.csv";
        //DiscountId_PaymentType_VehicleType_TicketCode_TicketValue_CancelDate_Reason
        public const string TicketCancelListContent = "{0},{1},{2},{3},{4},{5},{6};\r\n";

        //TRANS_YYYYMMDD_HH24MISS_0001.CSV
        public const string TransactionFileName = "TRANS_{0}_{1}.csv";
        //TransactionId_StationType_TollType_PaymentType_TransactionType_VehicleType_DiscountId_LaneNumber_LPN_LpnBackend_LaneImage_PlateImage_Etag_Code_Time_TicketValue_PointIn_LpnIn_LaneImagePoinIn_PlateImageIn_PointOut_LpnOut_LaneImagePoinOut_PlateImageOut
        public const string TransactionContent = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23};\r\n";

        //PRE_YYYYMMDD_HH24MISS_0001.CSV
        public const string PrepayFileName = "PRE_{0}_{1}.csv";
        //LPN_Etag_Code_TicketValue_PaymentType_VehicleType_ValidDate_ExpiredDate_SoldDate_Status_DiscountId_PointIn_PointOut
        public const string PrepayContent = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12};\r\n";

        //REPORT_YYYYMMDD_HH24MISS_0001.CSV
        public const string ReportFileName = "REPORT_{0}_{1}.csv";
        //DiscountId_StationType_TollType_PaymentType_TransactionType_VehicleType_TotalVehicle_TotalAmount_ReportDate
        public const string ReportContent = "{0},{1},{2},{3},{4},{5},{6},{7},{8};\r\n";

        #region "Enum"
        public enum WhiteListType
        {
            Unlimited,
            Limit
        }

        public enum DiscountType
        {
            Invalid,
            Valid
        }

        public enum PaymentType
        {
            TurnTicket = 1,
            MonthTicket,
            QuarterTicket,
            YearTicket
        }

        public enum StationType
        {
            ThuPhiHo = 1,
            ThuPhiKin
        }
        public enum TollType
        {
            MTC = 1,
            ETC
        }
        public enum TransactionType
        {
            MTCTurnTicket = 1,
            MTCMonthTicket,
            MTCQuarterTicket,
            MTCOneTicketFree,
            MTCGroupTicketFree,
            MTCNationwide,
            MTCSpecial,
            ETCTurnTicket,
            ETCMonthTicket,
            ETCQuarterTicket,
            ETCPriority,
            ETCOffline,
            ETCYearTicket,
            MTCYearTicket
        }
        #endregion
    }
}
