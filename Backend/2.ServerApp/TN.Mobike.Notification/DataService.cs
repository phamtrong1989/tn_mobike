﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace TN.GSTP.Client
{
    class DataService
    {
        public static List<CenterArchives> GetETCData(string procedureName, object model)
        {
            using (IDbConnection db = new SqlConnection(AppSettings.ETCConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();
                //Execute stored procedure
                return db.Query<CenterArchives>(procedureName, model, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static List<EtcArchives> GetMTCData(string procedureName, object  model)
        {
            using (IDbConnection db = new SqlConnection(AppSettings.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();
                //Execute stored procedure
                return db.Query<EtcArchives>(procedureName, model, commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
