﻿using Dapper;
using FirebaseAdmin.Messaging;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Mobike.Controls
{
    public class Controller
    {
        public static bool EmailBoxUpdateStatus(int id, bool status)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    db.Execute($"UPDATE [dbo].[EmailBox] Set IsSend = {(status ? 1 : 0)}, SuccessfulSendDate = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}' where id = {id}", commandType: CommandType.Text);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.EmailBoxUpdateStatus", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return false;
            }
        }

        public static List<EmailBox> EmailBoxSearchTop(int top)
        {
            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                string sql = $@"
                        SELECT top {top} * 
                        FROM [dbo].[EmailBox]
                        WHERE IsSend = 0 and [To] <> '' and NotSend = 0 and SendDate <= '{DateTime.Now:yyyy-MM-dd HH:mm:ss}'
                ";
                return db.Query<EmailBox>(sql, commandType: CommandType.Text).ToList();
            }
        }

        public static List<BookingFail> BookingFailSearchTop(int top)
        {
            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                string sql = $@"SELECT top {top} *  FROM [dbo].[BookingFail] WHERE Status = 0";
                return db.Query<BookingFail>(sql, commandType: CommandType.Text).ToList();
            }
        }

        public static void DataInfoUpdate(EDataInfoType type, string data)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                            declare @type as int = {((int)type)};
                            declare @data as nvarchar(4000) = '{data}';
                            declare @isEx as int = -1;
                            select @isEx = [Type] from [dbo].[DataInfo] where [Type] = @type;

                            if @isEx > -1
                            begin
	                            UPDATE [dbo].[DataInfo]
	                               SET 
		                              [Data] = @data
		                              ,[UpdatedTime] = GETDATE()
	                             WHERE  [Type] = @type
                            end
                            else
                            begin
                            INSERT INTO [dbo].[DataInfo]
                                       ([Type]
                                       ,[Data]
                                       ,[UpdatedTime])
                                 VALUES
                                       (@type
                                       ,@data
                                       ,GETDATE())
                            end
                    ";
                    db.ExecuteScalar(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.DataInfoUpdate", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void BookingFailSetStatus(int id, EBookingFailStatus status, string note)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"Update [dbo].[BookingFail] set UpdatedDate = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}', status = {(byte)status}, note = N'{note}' where [Id] = {id}";
                    db.ExecuteScalar(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingFailSetStatus", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void BookingForgotrReturnTheBikeSetStatus(int id, string note)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"Update [dbo].[Booking] set IsForgotrReturnTheBike = 1, note = N'{note}' where [Id] = {id}";
                    db.ExecuteScalar(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingForgotrReturnTheBikeSetStatus", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void BookingForgotrReturnTheBikeSetStatusFalse(int id, string note)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"Update [dbo].[Booking] set IsForgotrReturnTheBike = 0, note = N'{note}' where [Id] = {id}";
                    db.ExecuteScalar(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingForgotrReturnTheBikeSetStatus", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static EmailManage EmailManageGet()
        {
            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                string sql = $"SELECT top 1 * FROM [dbo].[EmailManage]";
                return db.Query<EmailManage>(sql, commandType: CommandType.Text).FirstOrDefault();
            }
        }

        public static List<EventSystemMongo> EventSystemGets()
        {
            string dbName = SyncService.MongoDBSettings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
            var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
            var db = dbClient.GetDatabase(dbName);
            string query = "{ IsJob: true, UserId: 0 , Date : { $gte: " + DateTime.Now.AddDays(-2).ToString("yyyyMMdd") + ", $lte: " + DateTime.Now.ToString("yyyyMMdd") + " }}";
            var data = db.GetCollection<EventSystemMongo>("EventSystem").Find(query).ToList();
            return data;
        }

        public static void EventSystemAdd(EventSystemMongo data)
        {
            try
            {
                data.Id = Convert.ToInt64($"{DateTime.Now:yyyyMMddHHmmssffff}");
                string dbName = SyncService.MongoDBSettings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<EventSystemMongo>("EventSystem");
                collection.InsertOne(data);
            }
            catch {
    
            }
        }

        public static SMSBox SMSBoxAdd(SMSBox data)
        {
            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                string sql = $@"
                              INSERT INTO [dbo].[SMSBox]
                                       ([PhoneNumber]
                                       ,[NotificationDataId]
                                       ,[Content]
                                       ,[IsSend]
                                       ,[ReSend]
                                       ,[SendDate]
                                       ,[CreatedDate])
                                 VALUES
                                       ('{data.PhoneNumber}'
                                       ,{data.NotificationDataId}
                                       ,N'{data.Content}'
                                       ,{(data.IsSend ? "1" : "0")}
                                       ,0
                                       ,'{DateTime.Now:yyyy-MM-dd HH:mm:ss}'
                                       ,'{DateTime.Now:yyyy-MM-dd HH:mm:ss}')
                                select SCOPE_IDENTITY()
                ";
                var id = db.ExecuteScalar<int>(sql, commandType: CommandType.Text);
                data.Id = id;
                return data;
            }
        }

        public static List<NotificationJob> NotificationJobSearch()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT top 500 * FROM [dbo].[NotificationJob] where IsSend = 0 and PublicDate <= '{DateTime.Now:yyyy-MM-dd HH:mm}' order by PublicDate asc";
                    return db.Query<NotificationJob>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<NotificationJob>();
            }
        }

        public static bool NotificationJobSetIsSend(int id)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                   db.Execute($"UPDATE [dbo].[NotificationJob] Set IsSend = 1, SendDate = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}' where id = {id}", commandType: CommandType.Text);
                   return true;
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobSetIsSend", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return false;
            }
        }

        public static List<WalletTransaction> WalletTransactionPendingZALOSearch()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    // call giao dịch trong vòng 48 tiếng
                    string sql = $"SELECT * FROM [dbo].[WalletTransaction] where PaymentGroup = 3 and Type = 0 and ((Status = 0 and CreatedDate <= '{DateTime.Now.AddSeconds(-60):yyyy-MM-dd HH:mm:ss}') or (Status = 3 and CreatedDate <= '{DateTime.Now.AddMinutes(-16).AddSeconds(-2):yyyy-MM-dd HH:mm:ss}')) and CreatedDate >= '{DateTime.Now.AddDays(-10):yyyy-MM-dd HH:mm:ss}' order by id asc";
                    return db.Query<WalletTransaction>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.WalletTransactionPendingZALOSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<WalletTransaction>();
            }
        }

        public static List<WalletTransaction> WalletTransactionPendingPayooPaySearch()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    // call giao dịch trong vòng 48 tiếng
                    string sql = $"SELECT * FROM [dbo].[WalletTransaction] where PaymentGroup = 5 and Type = 0 and ((Status = 0 and CreatedDate <= '{DateTime.Now.AddSeconds(-28):yyyy-MM-dd HH:mm:ss}') or (Status = 3 and CreatedDate <= '{DateTime.Now.AddMinutes(-35):yyyy-MM-dd HH:mm:ss}')) and CreatedDate >= '{DateTime.Now.AddDays(-10):yyyy-MM-dd HH:mm:ss}' order by id asc";
                    return db.Query<WalletTransaction>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.WalletTransactionPendingZALOSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<WalletTransaction>();
            }
        }

        public static List<WalletTransaction> WalletTransactionPendingMomoSearch()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    // call giao dịch trong vòng 48 tiếng
                    string sql = $"SELECT * FROM [dbo].[WalletTransaction] where PaymentGroup = 2 and Type = 0 and Status = 0 and CreatedDate <= '{DateTime.Now.AddSeconds(-60):yyyy-MM-dd HH:mm:ss}' and CreatedDate >= '{DateTime.Now.AddDays(-30):yyyy-MM-dd HH:mm:ss}' order by id asc";
                    return db.Query<WalletTransaction>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.WalletTransactionPendingZALOSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<WalletTransaction>();
            }
        }

        public static List<Booking> BookingSearch()
        {
            try
            {
                var data = new List<Booking>();

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    // call giao dịch trong vòng 48 tiếng
                    string sql = $"SELECT * FROM [dbo].[Booking] where status = 1";
                    data = db.Query<Booking>(sql, commandType: CommandType.Text).ToList();
                }

                foreach(var item in data)
                {
                    item.TotalMinutes = Functions.TotalMilutes(item.StartTime, DateTime.Now);
                    item.TotalPrice = Functions.ToTotalPrice(item.TicketPrice_TicketType, item.StartTime, DateTime.Now, item.TicketPrice_TicketValue, item.TicketPrice_LimitMinutes, item.TicketPrice_BlockPerMinute, item.TicketPrice_BlockPerViolation, item.TicketPrice_BlockViolationValue, item.Prepaid_EndTime ?? DateTime.Now, item.Prepaid_MinutesSpent);
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Booking>();
            }
        }

        public static List<Booking> BookingSearchForgotrReturnTheBike()
        {
            try
            {
                var data = new List<Booking>();

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[Booking] where Status = 1 and IsForgotrReturnTheBike = 0 and StartTime <= '{DateTime.Now.AddMinutes(-20):yyyy-MM-dd HH:mm:ss}'";
                    return db.Query<Booking>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Booking>();
            }
        }

        public static List<Booking> BookingSearchForgotrReturnTheBikeRecheck()
        {
            try
            {
                var data = new List<Booking>();

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[Booking] where Status = 1 and IsForgotrReturnTheBike = 1";
                    return db.Query<Booking>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Booking>();
            }
        }

        public static List<Booking> CloseDockByBookings()
        {
            try
            {
                var data = new List<Booking>();

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
;with tbla as (
	SELECT *, N'ECloseBike_'+b.TransactionCode+'_'+CAST(b.BikeId as nvarchar)+'_'+CAST(b.AccountId as nvarchar) as FlagTransaction FROM [dbo].[Booking] b
	where status = 1
		and exists (select 1 from Dock d where d.Id = b.DockId and d.LockStatus = 1)
		and exists (select 1 from Flag f where f.[Transaction] = N'ECloseBike_'+b.TransactionCode+'_'+CAST(b.DockId as nvarchar)+'_'+CAST(b.AccountId as nvarchar) and f.UpdatedTime < DATEADD(MINUTE, -30, GETDATE()))
)
select t.*, a.FullName, a.Email, a.Phone,b.Plate  from tbla t left join Account a on t.AccountId = a.Id left join Bike b on t.BikeId = b.Id
                    ";
                    data = db.Query<Booking>(sql, commandType: CommandType.Text).ToList();
                }

                foreach (var item in data)
                {
                    item.TotalMinutes = Functions.TotalMilutes(item.StartTime, DateTime.Now);
                    item.TotalPrice = Functions.ToTotalPrice(item.TicketPrice_TicketType, item.StartTime, DateTime.Now, item.TicketPrice_TicketValue, item.TicketPrice_LimitMinutes, item.TicketPrice_BlockPerMinute, item.TicketPrice_BlockPerViolation, item.TicketPrice_BlockViolationValue, item.Prepaid_EndTime ?? DateTime.Now, item.Prepaid_MinutesSpent);
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.CloseDockByBookings", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Booking>();
            }
        }

        public static List<Wallet> WalletByAccount(List<int> accounts)
        {
            try
            {
                if(!accounts.Any())
                {
                    return new List<Wallet>();
                }    
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[Wallet] where AccountId in ({string.Join(",", accounts)})";
                    return db.Query<Wallet>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Wallet>();
            }
        }

        public static List<AccountDevice> AccountDeviceSearch(int accountId)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[AccountDevice] where AcountId = {accountId} and CloudMessagingToken <> ''";
                    return db.Query<AccountDevice>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobSearch", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<AccountDevice>();
            }
        }

        public static bool FlagCheck(string transaction, int type)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT top 1 * FROM [dbo].[Flag] where [Transaction] = '{transaction}' and Type = {type}";
                    var data = db.Query<Flag>(sql, commandType: CommandType.Text).ToList();
                    return data.Any();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.FlagCheck", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return true;
            }
        }

        public static Booking BookingByDock(int dockId)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT top 1 * FROM [dbo].[Booking] where DockId = {dockId} and Status = 1";
                    return db.Query<Booking>(sql, commandType: CommandType.Text).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingByAccount", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return null;
            }
        }

        public static Account AccountGet(int accountId)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT top 1 * FROM [dbo].[Account] where Id = {accountId}";
                    return db.Query<Account>(sql, commandType: CommandType.Text).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingByAccount", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return null;
            }
        }

        public static Booking BookingByAccount(int accountId)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT top 1 * FROM [dbo].[Booking] where AccountId = {accountId} and Status = 1";
                    return db.Query<Booking>(sql, commandType: CommandType.Text).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BookingByAccount", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return null;
            }
        }

            public static bool FlagUpdateTime(string transaction)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"Update [dbo].[Flag] set UpdatedTime = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}' where [Transaction] = '{transaction}'";
                    var data = db.Query<Flag>(sql, commandType: CommandType.Text).ToList();
                    return data.Any();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.FlagUpdateTime", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return true;
            }
        }

        public static Dock DockGet(int id)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT top 1 * FROM [dbo].[Dock] where Id = {id} and Status = 1";
                    return db.Query<Dock>(sql, commandType: CommandType.Text).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.DockGet", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return null;
            }
        }


        public static List<Dock> DocksByIds(List<int> ids)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[Dock] where Id in ({string.Join(",", ids)}) and Status = 1";
                    return db.Query<Dock>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.DocksByIds", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return null;
            }
        }

        public static bool FlagUpdateTimeOrInsert(string transaction)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                                        declare @transaction as nvarchar(100) = ''
                                        select top 1 @transaction = [Transaction] from Flag where [Transaction] = '{transaction}'
                                        if @transaction is not null and @transaction <> ''
                                        begin
	                                        Update [dbo].[Flag] set UpdatedTime = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}' where [Transaction] = '{transaction}'
                                        end
                                        else
                                        begin
	                                        INSERT INTO [dbo].[Flag] ([Transaction], [Type], [IsProcess], [UpdatedTime])
                                            VALUES ('{transaction}', 0, 0, '{DateTime.Now:yyyy-MM-dd HH:mm:ss}')
                                        end
                                ";
                    var data = db.Query<Flag>(sql, commandType: CommandType.Text).ToList();
                    return data.Any();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.FlagUpdateTime", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return true;
            }
        }

        public static void FlagAdd(string transaction, int type)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"INSERT INTO [dbo].[Flag]  ([Transaction] ,[Type] ,[IsProcess],[UpdatedTime]) VALUES('{transaction}' ,{type},0,'{DateTime.Now:yyyy-MM-dd HH:mm:ss}')";
                    db.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.FlagAdd", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void FlagClear()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"DELETE FROM [dbo].[Flag] where [UpdatedTime] < '{DateTime.Now.AddDays(-7):yyyy-MM-dd HH:mm:ss}'";
                    db.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.FlagClear", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void ClearData()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"delete FROM [dbo].[EmailBox] where IsSend = 1";
                    db.Execute(sql, commandType: CommandType.Text);
                }

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"Delete SMSBox where IsSend = 1";
                    db.Execute(sql, commandType: CommandType.Text);
                }

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"delete [dbo].[Notification] where CreatedDate <= '{DateTime.Now.AddDays(-15):yyyy-MM-dd}'";
                    db.Execute(sql, commandType: CommandType.Text);
                }

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"delete BikeWarningLog where UpdatedDate <= '{DateTime.Now.AddDays(-3):yyyy-MM-dd}'";
                    db.Execute(sql, commandType: CommandType.Text);
                }

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"delete [dbo].[OpenLockHistory]  WHERE CreatedDate <= '{DateTime.Now.AddDays(-3):yyyy-MM-dd}'";
                    db.Execute(sql, commandType: CommandType.Text);
                }

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"delete [dbo].[AccountOTP]  WHERE CreatedDate <= '{DateTime.Now.AddDays(-7):yyyy-MM-dd}'";
                    db.Execute(sql, commandType: CommandType.Text);
                }

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"delete FROM [dbo].[Flag] where UpdatedTime <= '{DateTime.Now.AddDays(-2):yyyy-MM-dd}'";
                    db.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.FlagClear", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public async static void SendNotificationByAccount(string functionName, string requestId, int accoutnId, string title, string message, bool isShow, byte dataType, string dataContent)
        {
            try
            {
                await NotificationAdd(new NotificationData
                {
                    AccountId = accoutnId,
                    CreatedDate = DateTime.Now,
                    DeviceId = "",
                    Tile = title,
                    Message = message,
                    TransactionCode = "SV",
                    Type = 0,
                    Icon = ""
                });

                var getDevices = AccountDeviceSearch(accoutnId);

                foreach (var item in getDevices)
                {
                    var data = await FirebaseMessaging.DefaultInstance.SendAsync(new Message()
                    {
                        Notification = new Notification
                        {
                            Title = title,
                            Body = message
                        },
                        Token = item.CloudMessagingToken,
                        Data = new Dictionary<string, string>() { 
                            { "isShow", isShow.ToString() },
                            { "type", dataType.ToString() },
                            { "time", DateTime.Now.Ticks.ToString() },
                            { "content", dataContent }
                        },
                        Apns = new ApnsConfig
                        {
                            Aps = new Aps { ContentAvailable = true },
                            Headers = new Dictionary<string, string>() {
                            { "apns-push-type","background" },
                            { "apns-priority","5" },
                            { "apns-topic", "" }
                        }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, ex.ToString(), true);
            }
        }

        public async static void SendNotificationByTopicAll(string functionName, string requestId, string title, string message, bool isShow, byte dataType, string dataContent)
        {
            try
            {
                await NotificationAdd(new NotificationData
                {
                    AccountId = 0,
                    CreatedDate = DateTime.Now,
                    DeviceId = "",
                    Tile = title,
                    Message = message,
                    TransactionCode = "SV",
                    Type = 0,
                    Icon = ""
                });

                var data = await FirebaseMessaging.DefaultInstance.SendAsync(new Message()
                {
                    Notification = new Notification
                    {
                        Title = title,
                        Body = message
                    },
                    Data = new Dictionary<string, string>() { 
                        { "isShow", isShow.ToString() },
                        { "type", dataType.ToString() },
                        { "time", DateTime.Now.Ticks.ToString() },
                        { "content", dataContent }
                    },
                    Topic = "all",
                    Apns = new ApnsConfig
                    {
                        Aps = new Aps { ContentAvailable = true },
                        Headers = new Dictionary<string, string>() {
                            { "apns-push-type","background" },
                            { "apns-priority","5" },
                            { "apns-topic", "all" }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, ex.ToString(), true);
            }
        }

        public static async Task NotificationAdd(NotificationData data)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        INSERT INTO [dbo].[Notification]
                                   ([AccountId]
                                   ,[DeviceId]
                                   ,[Icon]
                                   ,[TransactionCode]
                                   ,[Type]
                                   ,[Tile]
                                   ,[Message]
                                   ,[CreatedDate])
                             VALUES
                                   ({data.AccountId}
                                   ,'{data.DeviceId}'
                                   ,'{data.Icon}'
                                   ,'{data.TransactionCode}'
                                   ,0
                                   ,N'{data.Tile}'
                                   ,N'{data.Message}'
                                   ,'{DateTime.Now:yyyy-MM-dd HH:mm:ss}')
                        ";
                    var rs = await db.ExecuteAsync(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationAdd", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void NotificationJobAdd(int accountId, string title, string message)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        INSERT INTO [dbo].[NotificationJob]
                                   ([AccountId]
                                   ,[DeviceId]
                                   ,[Icon]
                                   ,[Tile]
                                   ,[Message]
                                   ,[PublicDate]
                                   ,[CreatedDate]
                                   ,[CreatedUser]
                                   ,[UpdatedDate]
                                   ,[UpdatedUser]
                                   ,[IsSend]
                                   ,[SendDate]
                                   ,[LanguageId]
                                   ,[IsSystem])
                             VALUES
                                   ({accountId}
                                   ,''
                                   ,''
                                   ,N'{title}'
                                   ,N'{message}'
                                   ,'{DateTime.Now:yyyy-MM-dd HH:mm:ss}'
                                   ,'{DateTime.Now:yyyy-MM-dd HH:mm:ss}'
                                   ,0
                                   ,null
                                   ,null
                                   ,0
                                   ,null
                                   ,0
                                   ,1)
                        ";
                    var rs =  db.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobAdd", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static List<Bike> Bikes()
        {
            try
            {
                var data = new List<Bike>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[Bike] where status = 1";
                    data = db.Query<Bike>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.Bikes", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Bike>();
            }
        }

        public static Bike BikeGet(int id)
        {
            try
            {
                var data = new List<Bike>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[Bike] where id = {id} and  status = 1";
                    return db.Query<Bike>(sql, commandType: CommandType.Text).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.Bikes", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return null;
            }
        }

        public static List<Bike> BikeNotInBooking()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
            SELECT b.Id, b.Plate, d.Lat, d.Long , d.SerialNumber
            FROM [dbo].[Bike]  b inner join Dock d on b.DockId  = d.Id
            where b.[status] = 1 and DockId > 0 and not exists (select 1 from Booking where Status = 1 and BikeId = b.Id)
                    ";
                    return db.Query<Bike>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.Bikes", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Bike>();
            }
        }

        public static List<Station> Stations()
        {
            try
            {
                var data = new List<Station>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[Station] where status = 1";
                    data = db.Query<Station>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.Station", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Station>();
            }
        }

        public static List<Station> StationsSearchByDeta(double min_lat, double max_lat, double min_lng, double max_lng)
        {
            try
            {
                var data = new List<Station>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[Station] where status = 1 and Lat >= {min_lat} and Lat <= {max_lat} and Lng >= {min_lng} and Lng <= {max_lng}";
                    data = db.Query<Station>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.Station", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Station>();
            }
        }

        public static List<StationTotalBikeModel> StationTotalBike()
        {
            try
            {
                var data = new List<StationTotalBikeModel>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        ;with tbl as (
							select StationId, COUNT(1) as TotalBike
							from Bike b
							where Status = 1 and not exists (select 1 from Booking where BikeId = b.Id and Status = 1)
							group by StationId
                        )
                        select Station.Id, Station.Name, Station.DisplayName, Station.Address, Station.Spaces, Station.ManagerUserId, Station.ProjectId, tbl.TotalBike
                        from Station left join tbl on tbl.StationId = Station.id
						where Station.Status = 1
                    ";
                    data = db.Query<StationTotalBikeModel>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.StationTotalBikeModel", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<StationTotalBikeModel>();
            }
        }

        public static List<BikeInfo> BikesInfo()
        {
            try
            {
                var data = new List<BikeInfo>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $"SELECT * FROM [dbo].[BikeInfo]";
                    data = db.Query<BikeInfo>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BikeInfo", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<BikeInfo>();
            }
        }

        public static void BikesInfoAdd(int bikeId)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"INSERT INTO [dbo].[BikeInfo] ([BikeId],[MaintenanceRepairId], [MaintenanceRepairItemId], [TotalMinutes], [HoursForMaintenance]) VALUES ({bikeId},0,0,0,0)";
                    var rs = db.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobAdd", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static List<TotalMinutesModel> TotalMinutes()
        {
            try
            {
                var data = new List<TotalMinutesModel>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                                    SELECT  [BikeId], SUM(TotalMinutes) as Total
                                    FROM [dbo].[Transaction]
                                    group by BikeId
                                    ";
                    data = db.Query<TotalMinutesModel>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BikeInfo", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<TotalMinutesModel>();
            }
        }

        public static void BikesInfoUpdate(BikeInfo item)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                                        UPDATE [dbo].[BikeInfo]
                                           SET 
                                               [TotalMinutes] = {item.TotalMinutes}
                                              ,[LastMaintenanceTime] = '{item.LastMaintenanceTime:yyyy-MM-dd HH:mm:ss}'
                                              ,[HoursForMaintenance] = {item.HoursForMaintenance}
                                              ,[NextMaintenanceTime] = '{item.NextMaintenanceTime:yyyy-MM-dd HH:mm:ss}'
                                              ,[ErrorDescriptions] = '{item.ErrorDescriptions}'
                                              ,[ErrorFiles] = '{item.ErrorFiles}'
                                              ,[SuppliesIds] = '{item.SuppliesIds}'
                                              ,[SuppliesNames] = '{item.SuppliesNames}'
                                              ,[RepairDescriptions] = '{item.RepairDescriptions}'
                                              ,[RepairFiles] = '{item.RepairFiles}'
                                              ,[RepairTime] = '{item.RepairTime:yyyy-MM-dd HH:mm:ss}'
                                              ,[MaintenanceRepairId] = {item.MaintenanceRepairId}
                                              ,[MaintenanceRepairItemId] = {item.MaintenanceRepairItemId}
                                         WHERE [BikeId] = {item.BikeId}
                                    ";
                    var rs = db.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.NotificationJobAdd", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static List<BikeLowBatteryModel> BikeByLowBattery()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        SELECT b.Id as BikeId,b.Plate,d.SerialNumber, s.Name as [StationName1], s.DisplayName as [StationName2], s.Address, d.Battery, b.ProjectId, (IIF(s.ManagerUserId is null, 0, s.ManagerUserId)) as ManagerUserId 
                        FROM [dbo].[Bike] b inner join [dbo].[Dock] d on b.DockId = d.Id inner join [dbo].[Station] s on b.StationId = s.Id
                        where b.Status = 1 and exists (select 1 from Dock d where d.Id = b.DockId and d.Battery < 10 and StationId > 0)
                                   ";
                    return  db.Query<BikeLowBatteryModel>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BikeByLowBattery", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<BikeLowBatteryModel>();
            }
        }

        public static void EmailBoxAdd(string form, string to, DateTime sendDate, string subject, string body, long notificationDataId)
        {
            try
            {
                string fTo = "";
                string fAdd = "";
                if(to == null)
                {
                    return;
                }    
                var tos = to.Split(',');
                if(tos.Length == 0)
                {
                    return;
                }
                else if (tos.Length == 1)
                {
                    fTo = tos[0];
                    fAdd = "";
                }
                else if (tos.Length > 1)
                {
                    fTo = tos[0];
                    fAdd = to.Replace($"{fTo},","");
                }

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                            INSERT INTO [dbo].[EmailBox]
                                       ([From]
                                       ,[To]
                                       ,[Add]
                                       ,[Cc]
                                       ,[Bc]
                                       ,[Subject]
                                       ,[Body]
                                       ,[Attachments]
                                       ,[IsSend]
                                       ,[NotSend]
                                       ,[Type]
                                       ,[ObjectId]
                                       ,[SendDate]
                                       ,[CreatedDate],[NotificationDataId])
                                 VALUES
                                       ('{form}'
                                       ,'{fTo}'
                                       ,'{fAdd}'
                                       ,''
                                       ,''
                                       ,N'{subject}'
                                       ,N'{body}'
                                       ,''
                                       ,0
                                       ,0
                                       ,0
                                       ,0
                                       ,'{sendDate:yyyy-MM-dd HH:mm:ss}'
                                       ,'{DateTime.Now:yyyy-MM-dd HH:mm:ss}', {notificationDataId})
                    ";
                    db.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.EmailBoxAdd", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static List<Warehouse> Warehouses()
        {
            try
            {
                var data = new List<Warehouse>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"SELECT * FROM [dbo].[Warehouse]";
                    data = db.Query<Warehouse>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.Warehouses", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Warehouse>();
            }
        }

        public static List<Warehouse> WarehousesJoin()
        {
            try
            {
                var warehouses = Warehouses();
                var suppliesss = Suppliess();
                var suppliesWarehousess = SuppliesWarehouses();

                foreach (var item in warehouses)
                {
                    item.Suppliess = new List<Supplies>();
                    foreach (var supplies in suppliesss)
                    {
                        item.Suppliess.Add(new Supplies { 
                            Id = supplies.Id,
                            Name = supplies.Name,
                            Code = supplies.Code,
                            Quantity = 0,
                            Status = ESuppliesStatus.Enabled,
                            Unit = supplies.Unit
                        });
                    }  
                    
                    foreach(var supplies in item.Suppliess)
                    {
                        int count = suppliesWarehousess.FirstOrDefault(x => x.WarehouseId == item.Id && x.SuppliesId == supplies.Id && x.Quantity > 0)?.Quantity ?? 0;
                        supplies.Quantity = count;
                    }
                    item.Suppliess = item.Suppliess.Where(x => x.Quantity > 0).ToList();
                }
                return warehouses;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.WarehousesJoin", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Warehouse>();
            }
        }

        public static List<ApplicationUser> ApplicationUsers()
        {
            try
            {
                var data = new List<ApplicationUser>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"SELECT * FROM [adm].[User] where IsRetired = 0";
                    data = db.Query<ApplicationUser>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.ApplicationUsers", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<ApplicationUser>();
            }
        }

        public static List<SuppliesWarehouse> SuppliesWarehouses()
        {
            try
            {
                var data = new List<SuppliesWarehouse>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"SELECT * FROM [dbo].[SuppliesWarehouse]";
                    data = db.Query<SuppliesWarehouse>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.SuppliesWarehouses", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<SuppliesWarehouse>();
            }
        }

        public static List<Supplies> Suppliess()
        {
            try
            {
                var suppliess = new List<Supplies>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"SELECT * from Supplies where status = 1";
                    suppliess = db.Query<Supplies>(sql, commandType: CommandType.Text).ToList();
                }
                return suppliess;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.Suppliess", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Supplies>();
            }
        }

        public static List<BikeHoursOperationModel> BikeHoursOperation()
        {
            try
            {
                var suppliess = new List<BikeHoursOperationModel>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"SELECT BikeId, SUM(TotalMinutes) as Total FROM [dbo].[Transaction] group by BikeId";
                    suppliess = db.Query<BikeHoursOperationModel>(sql, commandType: CommandType.Text).ToList();
                }
                return suppliess;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BikeHoursOperation", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<BikeHoursOperationModel>();
            }
        }

        
        public static List<TransactionsDebtChargesModel> TransactionsDebtCharges()
        {
            try
            {
                var suppliess = new List<TransactionsDebtChargesModel>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        ;with tbl as 
                        (
	                        select TransactionCode, AccountId, StartTime, EndTime, ChargeDebt, ProjectId
	                        from  [Transaction]
	                        where Status = 6
                        )
                        select t.*, a.FullName, a.Phone, a.Email, a.ManagerUserId, a.SubManagerUserId from tbl t inner join Account a on t.AccountId = a.id
                    ";
                    suppliess = db.Query<TransactionsDebtChargesModel>(sql, commandType: CommandType.Text).ToList();
                }
                return suppliess;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.TransactionsDebtCharges", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<TransactionsDebtChargesModel>();
            }
        }

        public static List<BikeReportModel> BikeReport()
        {
            try
            {
                var suppliess = new List<BikeReportModel>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
with tbl as 
(
	select Id, BikeId, Type, CreatedDate, AccountId, TransactionCode
	from BikeReport
	where Status = 0
)
select t.*, b.Plate, b.StationId, s.ManagerUserId,s.Name as StationName1, s.DisplayName as StationName2, s.Address as Address, b.ProjectId, a.FullName, a.Phone, a.Code, a.Email
from tbl t inner join Bike b on t.BikeId = b.id left join [dbo].[Station] s on s.Id = b.StationId left join [dbo].Account a on a.Id = t.AccountId
                    ";
                    suppliess = db.Query<BikeReportModel>(sql, commandType: CommandType.Text).ToList();
                }
                return suppliess;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BikeReport", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<BikeReportModel>();
            }
        }

        public static bool BikeReportUpdateIsSendEmail(List<int> ids)
        {
            if(!ids.Any())
            {
                return false;
            }
            
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    var sql = $@"update BikeReport set IsSendEmailReport = 1 Where id in ({string.Join(",", ids)})";
                    db.Execute(sql, commandType: CommandType.Text);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BikeReportUpdateIsSendEmail", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return false;
            }
        }

        public static bool EventSystemUpdateStatus(ObjectId _id)
        {
            try
            {
                string dbName = SyncService.MongoDBSettings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                db.GetCollection<EventSystemMongo>("EventSystem").UpdateOne(Builders<EventSystemMongo>.Filter.Eq("_id", _id), Builders<EventSystemMongo>.Update.Set("IsJob", false));
                return true;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.EventSystemUpdateStatus", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return false;
            }
        }

        public static List<BikeDisconectModel> BikeDisconect()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        SELECT  
	                           b.[Id]
	                          ,b.Plate
                              ,b.[StationId]
                              ,d.[IMEI]
                              ,d.[SerialNumber]
                              ,[LastConnectionTime]
                              ,b.[InvestorId]
                              ,b.[ProjectId],
	                          d.Lat,
	                          d.Long,
	                          (select top 1 t.id from Booking t where t.BikeId = b.Id and t.Status = 1) as BookingId,
							  (IIF(s.ManagerUserId is null, 0, s.ManagerUserId)) as ManagerUserId,
							  s.Name as StationName1,
							  s.DisplayName as StationName2,
							  s.Address as Address
                        FROM [dbo].[Dock] d left join [dbo].[Bike] b on d.Id = b.DockId left join [dbo].[Station] s on s.Id = b.StationId
                        where d.ConnectionStatus = 0 and b.DockId > 0 and b.Status = 1
                    ";
                    return db.Query<BikeDisconectModel>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.BikeDisconect", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<BikeDisconectModel>();
            }
        }

        public static List<DockOpenNotInBookingModel> DockOpenNotInBookings()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
SELECT b.Id as BikeId,b.Plate,d.SerialNumber, d.IMEI, s.Name as [StationName1], s.DisplayName as [StationName2], s.Address, d.Battery, b.ProjectId, (IIF(s.ManagerUserId is null, 0, s.ManagerUserId)) as ManagerUserId 
FROM [dbo].[Bike] b inner join [dbo].[Dock] d on b.DockId = d.Id inner join [dbo].[Station] s on b.StationId = s.Id
where b.Status = 1 and b.DockId > 0 and exists (select 1 from Dock d where d.Id = b.DockId and d.LockStatus = 0) and not exists (select 1 from Booking bo where d.Id = bo.DockId and bo.Status = 1)
                                   ";
                    return db.Query<DockOpenNotInBookingModel>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.DockOpenNotInBookings", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<DockOpenNotInBookingModel>();
            }
        }

        public static List<BikeWarningModel> BikeWarnings()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        ;with tbl as (
	                        SELECT
		                           bw.Id
		                          ,bw.[IMEI]
		                          ,bw.[WarningType]
		                          ,bw.[Lat]
		                          ,bw.[Long]
		                          ,bw.[Status]
		                          ,bw.[UpdatedDate]
		                          ,bw.[Note]
		                          ,bw.[Transaction]
		                          ,b.Id as BikeId
		                          ,b.Plate as Plate
		                          ,b.StationId
		                          ,b.ProjectId
	                        FROM [dbo].[BikeWarning] bw left join Dock d on bw.IMEI = d.IMEI left join Bike b on b.DockId = d.Id
	                        where b.Status = 1 and bw.Status = 1 and bw.UpdatedDate > DATEADD(mi, -5, GETDATE())
                        )
                        select t.*, s.Name, s.DisplayName, s.Address, s.ManagerUserId 
                        from tbl t left join Station s on s.Id = t.StationId
                    ";
                    return db.Query<BikeWarningModel>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.DockOpenNotInBookings", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<BikeWarningModel>();
            }
        }

        public static List<AccountVerifyStatusModel> AccountVerifyStatus()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        SELECT VerifyStatus, COUNT(1) as [Count]
                        FROM [dbo].[Account]
                        where VerifyStatus in (0,1,3)
                        group by VerifyStatus
                     ";
                    return db.Query<AccountVerifyStatusModel>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.AccountVerifyStatus", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<AccountVerifyStatusModel>();
            }
        }

        public static List<Booking> LowBatteryByAccount()
        {
            try
            {
                var data = new List<Booking>();

                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
;with tbla as (
	SELECT b.Id, b.DockId, b.BikeId, b.AccountId, b.Status, d.Battery, b.TransactionCode, N'ECloseBikeLowBattery_'+b.TransactionCode+'_'+CAST(CEILING(d.Battery) as nvarchar) as [FlagTransaction]
	FROM [dbo].[Booking] b left join Dock d on b.DockId = d.Id
	where b.Status = 1 and exists (select 1 from Dock d where d.Id = b.DockId and d.LockStatus = 1 and d.Battery < 10)
)
select t.*
from tbla t
where not exists (select 1 from Flag f where f.[Transaction] = t.FlagTransaction)
                    ";
                    data = db.Query<Booking>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.LowBatteryByAccount", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Booking>();
            }
        }

        public static List<Transaction> TransactionByNotGPS()
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        SELECT top 200 [Id], DockId, TransactionCode, StartTime, EndTime, BikeId
                        FROM [dbo].[Transaction]
                        where KM = 0 and StartTime >= '2022-04-20' and Status in (2,5,6,7,8) order by Id desc
                    ";
                    return db.Query<Transaction>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.LowBatteryByAccount", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<Transaction>();
            }
        }

        private static void TransactionUpdateGPS(int id, double km)
        {
            try
            {
                double sKm = Convert.ToDouble(km) / 1000;
                km = Math.Round(km, 2);
                double kcal = Math.Round(sKm * 20.1, 2);
                double kg = Math.Round(sKm * 0.1, 2);
                if(km == 0)
                {
                    km = 1;
                }    
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                            Update [dbo].[Transaction] 
                            set [KCal] = {kcal}, [KG] = {kg}, [KM] = {km}
                            where [Id] = {id}";
                    db.ExecuteScalar(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.TransactionUpdateGPS", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void FilterGPS(string functionName, string requestId, int transactionId, string transactionCode, int dockId, DateTime startTime, DateTime endTime)
        {
            try
            {
                double totalKM = 0;
                // Lấy tọa độ để trả về
                var getDock = DockGet(dockId);
                if(getDock == null)
                {
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Dock Null ({dockId}), transactionId {transactionId}", false);
                    return;
                }

                var gps = Functions.GPSGetByIMEI(getDock?.IMEI, startTime, endTime);

                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Tìm thấy ({gps.Count}) items GPS, transactionId {transactionId}", false);

                Functions.GPSDataAddAsync(new TransactionGPS
                {
                    TransactionCode = transactionCode,
                    TransactionId = transactionId,
                    Data = Newtonsoft.Json.JsonConvert.SerializeObject(gps.Select(x=> new GPSDataMini { Lat = x.Lat, Long = x.Long, CreateTimeTicks = x.CreateTimeTicks  })),
                    StartTime = startTime,
                    EndTime = endTime,
                    Date = Convert.ToInt32($"{endTime:yyyyMMdd}")
                });

                if (gps.Any())
                {
                    for (int i = 0; i < gps.Count - 1; i++)
                    {
                        totalKM += Functions.DistanceInMeter(gps[i].Lat, gps[i].Long, gps[i + 1].Lat, gps[i + 1].Long);
                    }
                }
                TransactionUpdateGPS(transactionId, totalKM);
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }


        public static DashboardInfoModel DashboardInfo(DateTime start, DateTime end)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        Declare @startTime datetime = ''{start:yyyy/MM/dd HH:mm:ss}''
                        Declare @endTime datetime = ''{end:yyyy/MM/dd HH:mm:ss}''
                        Declare @projectId int = 1

                        DECLARE @tienNap money = 0
                        declare @diemThue money = 0
                        declare @DiemNoCuoc money = 0

                        declare @napMomo money = 0
                        declare @napZalo money = 0
                        declare @napVTCPay money = 0
                        declare @napTrucTiep money = 0

                        declare @chuyenDiDangThue int = 0
                        declare @chuyenDiHoanThanh int = 0
                        declare @chuyenDiNoCuoc int = 0

                        declare @KHDangKy int = 0
                        declare @KHChuaXacThuc int = 0
                        declare @KHChoDuyet int = 0
                        declare @KHDaXacThuc int = 0

                        declare @XeSuDung int = 0
                        declare @XeSanSangTaiTram int = 0
                        declare @XeLuuKho int = 0
                        declare @LoiVaBaoDuong int = 0

                        declare @XeDangKetNoi int = 0
                        declare @XeMatKetNoi int = 0
                        declare @XeSapHet int = 0

                        select @tienNap = SUM(Price) from WalletTransaction where CreatedDate >= @startTime and CreatedDate < @endTime and [Status] = 1 and [Type] in (0,5,8)
                        select @diemThue = SUM(ChargeInAccount + ChargeInSubAccount) from [Transaction] where  StartTime >= @startTime and StartTime < @endTime and Status in (2,5,7,6)
                        select @DiemNoCuoc = SUM(ChargeDebt) from [Transaction] where  StartTime >= @startTime and StartTime < @endTime and Status = 6

                        select @napMomo = SUM(Price) from WalletTransaction where CreatedDate >= @startTime and CreatedDate < @endTime and [Status] = 1 and [Type] = 0 and PaymentGroup = 2
                        select @napZalo = SUM(Price) from WalletTransaction where CreatedDate >= @startTime and CreatedDate < @endTime and [Status] = 1 and [Type] = 0 and PaymentGroup = 3
                        select @napVTCPay = SUM(Price) from WalletTransaction where CreatedDate >= @startTime and CreatedDate < @endTime and [Status] = 1 and [Type] = 0 and PaymentGroup = 1
                        select @napTrucTiep = SUM(Price) from WalletTransaction where CreatedDate >= @startTime and CreatedDate < @endTime and [Status] = 1  and [Type] in (5,8)

                        select @chuyenDiDangThue = Count(1) from [Booking] where  Status in (1)
                        select @chuyenDiHoanThanh = Count(1) from [Transaction] where  StartTime >= @startTime and StartTime < @endTime and Status in (2,5,7)
                        select @chuyenDiNoCuoc = Count(1) from [Transaction] where  StartTime >= @startTime and StartTime < @endTime and Status = 6

                        select @KHDangKy = count(1) from Account where CreatedDate >= @startTime and CreatedDate < @endTime
                        select @KHChuaXacThuc = count(1) from Account where CreatedDate >= @startTime and CreatedDate < @endTime and VerifyStatus in (0, 3)
                        select @KHChoDuyet = count(1) from Account where CreatedDate >= @startTime and CreatedDate < @endTime and VerifyStatus = 1
                        select @KHDaXacThuc = count(1) from Account where CreatedDate >= @startTime and CreatedDate < @endTime and VerifyStatus = 2

                        select @XeSuDung = count(1) from Bike where Status = 1 and ProjectId = @projectId
                        select @XeSanSangTaiTram = count(1) from Bike b where Status = 1 and b.ProjectId = @projectId and b.DockId > 0 and exists (select 1 from Dock d where d.Id = b.DockId and d.ConnectionStatus = 1 and d.Battery >= 10)
                        select @XeLuuKho = count(1) from Bike where Status = 2 and ProjectId = @projectId
                        select @LoiVaBaoDuong = count(1) from Bike where  ProjectId = @projectId and Status = 3

                        select @XeDangKetNoi = count(1) from Bike b where Status = 1 and b.ProjectId = @projectId and b.DockId > 0 and exists (select 1 from Dock d where d.Id = b.DockId and d.ConnectionStatus = 1 and d.Battery >= 10)
                        select @XeMatKetNoi = count(1) from Bike b where Status = 1 and b.ProjectId = @projectId and b.DockId > 0 and exists (select 1 from Dock d where d.Id = b.DockId and d.ConnectionStatus = 0)
                        select @XeSapHet = count(1) from Bike b where Status = 1 and b.ProjectId = @projectId and b.DockId > 0 and exists (select 1 from Dock d where d.Id = b.DockId and d.ConnectionStatus = 1 and d.Battery < 10)

                        select @tienNap TienNap, @diemThue DiemThue, @DiemNoCuoc DiemNoCuoc, @napMomo NapMomo, @napZalo NapZalo, @napVTCPay NapVTCPAY, @napTrucTiep NapTrucTiep,
	                           @chuyenDiDangThue ChuyenDiDangThue, @chuyenDiHoanThanh ChuyenDiHoanThanh, @chuyenDiNoCuoc ChuyenDiNoCuoc,
	                           @KHDangKy KHDangKy, @KHChuaXacThuc KHChuaXacThuc, @KHChoDuyet KHChoDuyet, @KHDaXacThuc KHDaXacThuc,
	                           @XeSuDung XeSuDung, @XeSanSangTaiTram XeSanSangTaiTram, @XeLuuKho XeLuuKho, @LoiVaBaoDuong LoiVaBaoDuong,
	                           @XeDangKetNoi XeDangKetNoi, @XeMatKetNoi XeMatKetNoi, @XeSapHet XeSapHet
                    ";
                    return db.Query<DashboardInfoModel>($"EXEC sp_executesql N'{sql}'", commandType: CommandType.Text).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.DashboardInfo", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return null;
            }
        }

        public static List<int> ProcessDiscountByDayGetUsers(DateTime time)
        {
            try
            {
                var data = new List<int>();
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        SELECT a.AccountId 
                        FROM [dbo].[Transaction] a
                        where a.EndTime >= '{time.Date:yyyy-MM-dd}' and a.EndTime < '{time.AddDays(1).Date:yyyy-MM-dd}' and (a.[Status] = 2 or a.[Status] = 5 or a.[Status] = 7) and a.TotalPrice > 0 and not exists (select a.AccountId from TransactionDiscount b where b.Date = '{time.Date:yyyy-MM-dd}' and b.AccountId = a.AccountId)
                        group by AccountId 
                    ";
                    data = db.Query<int>(sql, commandType: CommandType.Text).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.LowBatteryByAccount", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<int>();
            }
        }

        public static List<AccountRatingItem> ChartsNews(DateTime start, DateTime end)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        declare @start as nvarchar(50) = '{start:yyyy-MM-dd 00:00:00}'
                        declare @end as nvarchar(50) = '{end.AddDays(1):yyyy-MM-dd 00:00:00}'
                        SELECT Top 200 AccountId, SUM(KM) as [Value]
                        FROM [dbo].[Transaction]
                        Where 
                            EndTime >= @start 
                            and EndTime < @end 
                            and [Status] in (2,5,7)
                            and (GroupTransactionOrder = 1 or GroupTransactionOrder is null or GroupTransactionOrder = 0)
                        group by AccountId
                        order by [Value] desc
                    ";
                    return db.Query<AccountRatingItem>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.ChartsNews", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
                return new List<AccountRatingItem>();
            }
        }

        public static void UpdateTop(List<AccountRatingItem> list, string code)
        {
            try
            {
                var qString = new StringBuilder();
                qString.Append($@"Delete [dbo].[AccountRatingItem] Where code = '{code}'");
                foreach (var item in list)
                {
                    qString.Append($@"
                    INSERT INTO [dbo].[AccountRatingItem]
                           ([Code]
                           ,[ProjectId]
                           ,[Order]
                           ,[AccountId]
                           ,[Value]
                           ,[UpdatedDate]
                           ,[Status])
                         VALUES
                               ('{item.Code}'
                               ,{item.ProjectId}
                               ,{item.Order}
                               ,{item.AccountId}
                               ,{item.Value}
                               ,'{item.UpdatedDate:yyyy-MM-dd HH:mm:ss}'
                               , 0)
                    ");
                }
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    db.Execute(qString.ToString(), commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, "Controller.UpdateTop", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void AccountRatingAutoAdd(AccountRating obj)
        {
            var sql = $@"
            declare @check as int = 0
            select @check = Id from AccountRating where Code = '{obj.Code}'
            if @check <= 0 
            INSERT INTO [dbo].[AccountRating]
                       ([Code]
                       ,[Name]
                       ,[Note]
                       ,[Reward]
                       ,[Type]
                       ,[Group]
                       ,[StartDate]
                       ,[EndDate]
                       ,[CreatedDate]
                       ,[CreatedUser]
                       ,[UpdatedDate]
                       ,[UpdatedUser]
                       ,[ProjectId])
                 VALUES
                       (N'{obj.Code }'
                       ,N'{obj.Name }'
                       ,N'{obj.Note }'
                       ,N'{obj.Reward}'
                       ,{(int)obj.Type}
                       ,{(int)obj.Group}
                       ,'{obj.StartDate:yyyy-MM-dd HH:mm:ss}'
                       ,'{obj.EndDate:yyyy-MM-dd HH:mm:ss}'
                       ,'{obj.CreatedDate:yyyy-MM-dd HH:mm:ss}'
                       ,0
                       ,null
                       ,null
                       ,{obj.ProjectId })";

            using (var db = new SqlConnection(AppSettings.ConnectionString))
            {
                db.Execute(sql, commandType: CommandType.Text);
            }
        }
    }
}