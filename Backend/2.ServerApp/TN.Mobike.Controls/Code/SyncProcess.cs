﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TN.Mobike.Controls
{
    public class SyncProcess
    {
        private static readonly Timer recheckOrderZALOPAY = new Timer() { Interval = 1000 * 60 * 5, Enabled = true, AutoReset = true };
        private static readonly Timer recheckOrderPAYOOPAY = new Timer() { Interval = 1000 * 60 * 5, Enabled = true, AutoReset = true };

        private static readonly Timer checkDebtTransaction = new Timer() { Interval = 1000 * 10, Enabled = true, AutoReset = true };
        private static readonly Timer clearSubMoneyFist = new Timer() { Interval = 1000 * 20, Enabled = true, AutoReset = true };
        private static readonly Timer clearSubMoney = new Timer() { Interval = 1000 * 20, Enabled = true, AutoReset = true };
        private static readonly Timer autoCheckLowBattery = new Timer() { Interval = 1000 * 60 * 30, Enabled = true, AutoReset = true };
        private static readonly Timer autoCheckInverter = new Timer() { Interval = (1000 * 60 * 60 * 24) * 5, Enabled = true, AutoReset = true };
        private static readonly Timer autoCheckMaintenanceSchedule = new Timer() { Interval = 1000 * 60 * 60 * 6, Enabled = true, AutoReset = true };
        private static readonly Timer checkStationTotalBike = new Timer() { Interval = 1000 * 60 * 60 * 1, Enabled = true, AutoReset = true };
        private static readonly Timer checkDebtCharges = new Timer() { Interval = 1000 * 60 * 30, Enabled = true, AutoReset = true };
        private static readonly Timer checkBikeReport = new Timer() { Interval = 1000 * 60 * 30, Enabled = true, AutoReset = true };
        private static readonly Timer createIndexMongo = new Timer() { Interval = 1000 * 60 * 60, Enabled = true, AutoReset = true };
        private static readonly Timer eventSystemCheck = new Timer() { Interval = 1000 * 2, Enabled = true, AutoReset = true };

        private static readonly Timer checkBikeDisconect = new Timer() { Interval = 1000 * 60 * 30, Enabled = true, AutoReset = true };
        private static readonly Timer checkDockOpenNotInBooking = new Timer() { Interval = 1000 * 60 * 5, Enabled = true, AutoReset = true };
        private static readonly Timer checkBikeWarning = new Timer() { Interval = 1000 * 60 * 15, Enabled = true, AutoReset = true };
        private static readonly Timer checkAccountVerifyStatus = new Timer() { Interval = 1000 * 60 * 30, Enabled = true, AutoReset = true };
        private static readonly Timer checkDebtSystem = new Timer() { Interval = 1000 * 60 * 30, Enabled = true, AutoReset = true };
        private static readonly Timer checkCloseDockByBooking = new Timer() { Interval = 1000 * 60 * 30, Enabled = true, AutoReset = true };
        private static readonly Timer checkCloseDockByBookingSystem = new Timer() { Interval = 1000 * 60 * 30, Enabled = true, AutoReset = true };
        private static readonly Timer checkLowBatteryByAccount = new Timer() { Interval = 1000 * 60 * 20, Enabled = true, AutoReset = true };
        private static readonly Timer checkBookingFail = new Timer() { Interval = 1000 * 5, Enabled = true, AutoReset = true };
        private static readonly Timer recheckOrderMomo = new Timer() { Interval = 1000 * 5, Enabled = true, AutoReset = true };
        private static readonly Timer bookingForgotrReturnTheBike = new Timer() { Interval = 1000 * 60 * 10, Enabled = true, AutoReset = true };
        private static readonly Timer checkAccountOnline = new Timer() { Interval = 1000 * 60, Enabled = true, AutoReset = true };
        private static readonly Timer clearnData = new Timer() { Interval = 1000 * 60 * 60, Enabled = true, AutoReset = true };
        private static readonly Timer updateTransactionGPS = new Timer() { Interval = 1000 * 60 * 1, Enabled = true, AutoReset = true };
        private static readonly Timer processDiscountByDay = new Timer() { Interval = 1000 * 60 * 10, Enabled = true, AutoReset = true };
        private static readonly Timer addPointDiscountByDay = new Timer() { Interval = 1000 * 60 * 10, Enabled = true, AutoReset = true };
        private static readonly Timer autoChartsTop = new Timer() { Interval = 1000 * 60 * 5, Enabled = true, AutoReset = true };

        private static bool IsRecheckOrderZALOPAY = false;
        private static bool IsRecheckOrderPayooPay = false;

        private static bool IsCheckDebtTransaction = false;
        private static bool IsCheckDebtSystem = false;
        private static bool IsCheckAccountOnline = false;
        private static bool IsAutoChartsTop = false;
        private static bool IsClearFlagStatic = false;
        private static bool IsClearSubMoney = false;
        private static bool IsClearSubMoneyFirst = false;
        private static bool IsCheckLowBattery = false;
        private static bool IsAutoCheckInverter = false;
        private static bool IsAutoCheckMaintenanceSchedule = false;
        private static bool IsCheckStationTotalBike = false;
        private static bool IsCheckDebtCharges = false;
        public static bool IsCheckBikeReport = false;
        public static bool IsCreateIndexMongo = false;
        public static bool IsEventSystemCheck = false;
        public static bool IsCheckBikeDisconect = false;
        public static bool IsCheckDockOpenNotInBooking = false;
        public static bool IsBikeWarning = false;
        public static bool IsCheckAccountVerifyStatus = false;
        public static bool IsCheckCloseDockByBooking = false;
        public static bool IsCheckCloseDockByBookingSystem = false;
        public static bool IsCheckLowBatteryByAccount = false;
        public static bool IsCheckBookingFail = false;
        public static bool IsRecheckOrderMomo = false;
        public static bool IsBookingForgotrReturnTheBike = false;
        public static bool IsCheckVTCPayStatus = false;
        public static bool IsClearnData = false;
        public static bool IsUpdateTransactionGPS = false;
        public static bool IsBikeNotInStationNotInBooking = false;
        public static bool IsDashboardInfo = false;
        public static bool IsProcessDiscountByDay = false;
        public static bool IsAddPointDiscountByDay = false;
        
        public static DateTime? CreateIndexMongoDate = null;
        private static List<Flag> flags = new List<Flag>();

        private static HubConnection connection;

        public static async Task InitHub()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.InitHub";
            try
            {
                connection = new HubConnectionBuilder().WithUrl($"{AppSettings.HubPath}").Build();
                connection.Closed += async (error) =>
                {
                    await Task.Delay(new Random().Next(0, 5) * 1000);
                    await connection.StartAsync();
                };
                await connection.StartAsync();
            }
            catch (Exception ex)
            {
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static async Task HubSendData(int accountId, EventSystemMongo data, string requestId, string functionName)
        {
            try
            {
                await connection.InvokeAsync("SendMessage", accountId, data);
            }
            catch (Exception ex)
            {
                await Task.Delay(2000);
                await connection.StartAsync();
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void Stop()
        {

            recheckOrderZALOPAY.Stop();
            recheckOrderZALOPAY.Dispose();

            checkDebtTransaction.Stop();
            checkDebtTransaction.Dispose();

            clearSubMoney.Stop();
            clearSubMoney.Dispose();

            autoCheckLowBattery.Stop();
            autoCheckLowBattery.Dispose();

            autoCheckInverter.Stop();
            autoCheckInverter.Dispose();

            autoCheckMaintenanceSchedule.Stop();
            autoCheckMaintenanceSchedule.Dispose();

            checkStationTotalBike.Stop();
            checkStationTotalBike.Dispose();

            checkDebtCharges.Stop();
            checkDebtCharges.Dispose();

            checkBikeReport.Stop();
            checkBikeReport.Dispose();

            clearSubMoneyFist.Stop();
            clearSubMoneyFist.Dispose();

            createIndexMongo.Stop();
            createIndexMongo.Dispose();

            eventSystemCheck.Stop();
            eventSystemCheck.Dispose();

            checkBikeDisconect.Stop();
            checkBikeDisconect.Dispose();

            checkDockOpenNotInBooking.Stop();
            checkDockOpenNotInBooking.Dispose();

            checkBikeWarning.Stop();
            checkBikeWarning.Dispose();

            checkAccountVerifyStatus.Stop();
            checkAccountVerifyStatus.Dispose();

            checkDebtSystem.Stop();
            checkDebtSystem.Dispose();

            checkCloseDockByBooking.Stop();
            checkCloseDockByBooking.Dispose();

            checkCloseDockByBookingSystem.Stop();
            checkCloseDockByBookingSystem.Dispose();

            checkLowBatteryByAccount.Stop();
            checkLowBatteryByAccount.Dispose();

            checkBookingFail.Stop();
            checkBookingFail.Dispose();

            recheckOrderMomo.Stop();
            recheckOrderMomo.Dispose();

            bookingForgotrReturnTheBike.Stop();
            bookingForgotrReturnTheBike.Dispose();

            checkAccountOnline.Stop();
            checkAccountOnline.Dispose();

            clearnData.Stop();
            clearnData.Dispose();

            updateTransactionGPS.Stop();
            updateTransactionGPS.Dispose();

            processDiscountByDay.Stop();
            processDiscountByDay.Dispose();

            addPointDiscountByDay.Stop();
            addPointDiscountByDay.Dispose();

            recheckOrderPAYOOPAY.Stop();
            recheckOrderPAYOOPAY.Dispose();

            autoChartsTop.Stop();
            autoChartsTop.Dispose();
        }

        public static void Start()
        {
            InitHub().ConfigureAwait(false);

            Functions.AddLog(0, "SyncProcess.Start", $"{DateTime.Now:yyyyMMddHHmmssffff}", EnumMongoLogType.Start, $"Start");
            EventSystemCheck();
            CheckDockOpenNotInBooking();
            recheckOrderZALOPAY.Elapsed += new ElapsedEventHandler((s, x) => RecheckOrderZALOPAYRun());

            //checkDebtTransaction.Elapsed += new ElapsedEventHandler((s, x) => CheckDebtTransaction());
      
            clearSubMoneyFist.Elapsed += new ElapsedEventHandler((s, x) => ClearSubMoneyFistRun());

            clearSubMoney.Elapsed += new ElapsedEventHandler((s, x) => ClearSubMoneyRun());
     
            checkBikeDisconect.Elapsed += new ElapsedEventHandler((s, x) => CheckBikeDisconect());

            autoCheckLowBattery.Elapsed += new ElapsedEventHandler((s, x) => AutoCheckLowBattery());

            checkDockOpenNotInBooking.Elapsed += new ElapsedEventHandler((s, x) => CheckDockOpenNotInBooking());

            checkStationTotalBike.Elapsed += new ElapsedEventHandler((s, x) => CheckStationTotalBike());
        
            checkBikeWarning.Elapsed += new ElapsedEventHandler((s, x) => CheckBikeWarning());

            checkDebtCharges.Elapsed += new ElapsedEventHandler((s, x) => CheckDebtCharges());

            checkBikeReport.Elapsed += new ElapsedEventHandler((s, x) => CheckBikeReport());

            createIndexMongo.Elapsed += new ElapsedEventHandler((s, x) => MongoCreateIndex());

            eventSystemCheck.Elapsed += new ElapsedEventHandler((s, x) => EventSystemCheck());

            checkAccountVerifyStatus.Elapsed += new ElapsedEventHandler((s, x) => CheckAccountVerifyStatus());
           
            checkDebtSystem.Elapsed += new ElapsedEventHandler((s, x) => CheckDebtSystem());

            checkCloseDockByBooking.Elapsed += new ElapsedEventHandler((s, x) => CheckCloseDockByBooking());

            checkCloseDockByBookingSystem.Elapsed += new ElapsedEventHandler((s, x) => CheckCloseDockByBookingSystem());

           // checkLowBatteryByAccount.Elapsed += new ElapsedEventHandler((s, x) => CheckLowBatteryByAccount());

            recheckOrderMomo.Elapsed += new ElapsedEventHandler((s, x) => RecheckOrderMomoRun());
          
            bookingForgotrReturnTheBike.Elapsed += new ElapsedEventHandler((s, x) => BookingForgotrReturnTheBike());

            checkAccountOnline.Elapsed += new ElapsedEventHandler((s, x) => CheckAccountOnlineRun());
           
            clearnData.Elapsed += new ElapsedEventHandler((s, x) => ClearData());

            updateTransactionGPS.Elapsed += new ElapsedEventHandler((s, x) => UpdateTransactionGPS());

            //ProcessDiscountByDay().ConfigureAwait(false);

            processDiscountByDay.Elapsed += new ElapsedEventHandler((s, x) => {
                ProcessDiscountByDay().ConfigureAwait(false);
            });

           // AddPointDiscountByDay().ConfigureAwait(false);

            addPointDiscountByDay.Elapsed += new ElapsedEventHandler((s, x) => {
                AddPointDiscountByDay().ConfigureAwait(false);
            });

            RecheckOrderPayooPAY().ConfigureAwait(false);

            recheckOrderPAYOOPAY.Elapsed += new ElapsedEventHandler((s, x) => {
                RecheckOrderPayooPAY().ConfigureAwait(false);
            });

            AutoChartsTop();

            autoChartsTop.Elapsed += new ElapsedEventHandler((s, x) => AutoChartsTop());
        }

        #region [# Xe ko trong chuyến đi nào, ko trong trạm 100m]
        public static void BikeNotInStationNotInBooking()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.BikeNotInStationNotInBooking";
            try
            {
                if (IsBikeNotInStationNotInBooking)
                {
                    return;
                }
                IsBikeNotInStationNotInBooking = true;

                var stations = Controller.Stations();
                var bikes = Controller.BikeNotInBooking();

                var newBikes = new List<Bike>();

                foreach(var bike in bikes)
                {
                    var inStation = stations.Any(x => Functions.DistanceInMeter(bike.Lat ?? 0, bike.Long ?? 0, x.Lat, x.Lng) <= 100);
                    if(!inStation)
                    {
                        newBikes.Add(new Bike { Id = bike.Id, Plate = bike.Plate, Lat = bike.Lat, Long = bike.Long });
                    }    
                }

                var stringData = string.Join("\n", newBikes.Select(x => $"{x.Plate}"));
                string folder = "C:\\Log";
                if(!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                File.WriteAllText($"{folder}\\{DateTime.Now:yyyyMMddHHmmss}.csv", stringData);

                IsBikeNotInStationNotInBooking = false;
            }
            catch (Exception ex)
            {
                IsBikeNotInStationNotInBooking = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
            
        }
        #endregion

        #region [# Xử lý những giao dịch mở khóa nhưng đợi quá lâu ko phản hồi từ khóa]
        // Lất giao dịch trạng thái = 0 và cancel khí
        // + Khách hàng đó thuê 1 chuyến khác rồi
        // + 
        public static void CheckBookingFail()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.CheckBookingBookingFail";
            try
            {
                if (IsCheckBookingFail)
                {
                    return;
                }
                IsCheckBookingFail = true;
                var listData = Controller.BookingFailSearchTop(1000);
                if (listData.Any())
                {
                    foreach (var item in listData)
                    {
                        //  Nếu khách hàng đã có chuyến đi khác rồi thì hủy bỏ sự kiện này
                        var bookingByAccount = Controller.BookingByAccount(item.AccountId);
                        if (bookingByAccount != null)
                        {
                            Controller.BookingFailSetStatus(item.Id, EBookingFailStatus.Cancel, $"Hệ thống tự hủy do khách đã đặt chuyến đi khác, mã chuyến đi {bookingByAccount.TransactionCode}");
                            continue;
                        }
                        // kiểm tra xe này đã được thuê chưa, nếu rồi thì hủy
                        var bookingByDock = Controller.BookingByDock(item.DockId);
                        if (bookingByDock != null)
                        {
                            Controller.BookingFailSetStatus(item.Id, EBookingFailStatus.Cancel, $"Hệ thống tự hủy do xe này đã được thuê rồi, mã chuyến đi {bookingByDock.TransactionCode}");
                            continue;
                        }
                        // Trường hợp xe không được thuê
                        var dock = Controller.DockGet(item.DockId);
                        var bike = Controller.BikeGet(item.BikeId);
                        if (bike == null)
                        {
                            Controller.BookingFailSetStatus(item.Id, EBookingFailStatus.Cancel, $"Hệ thống tự hủy do không tìm thấy xe hoặc xe bị vô hiệu hóa");
                            continue;
                        }
                        else if (dock == null)
                        {
                            Controller.BookingFailSetStatus(item.Id, EBookingFailStatus.Cancel, $"Hệ thống tự hủy do không tìm thấy khóa hoặc khóa bị vô hiệu hóa");
                            continue;
                        }    
                        else if(dock.LockStatus && item.CreatedDate.AddMinutes(5) <= DateTime.Now)
                        {
                            Controller.BookingFailSetStatus(item.Id, EBookingFailStatus.Cancel, $"Hệ thống tự hủy do vẫn đóng sau 5 phút");
                            continue;
                        }
                        else if (!dock.LockStatus)
                        {
                            var account = Controller.AccountGet(item.AccountId);
                            Controller.BookingFailSetStatus(item.Id, EBookingFailStatus.ViewAdmin, $"Khóa đang mở, nhân viên gọi điện cho khách để xử lý nghi vấn này");
                            // Tạo 1 sự kiện
                            var addEvent = new EventSystemMongo
                            {
                                IsMultiple = false,
                                IsJob = true,
                                IsProcessed = false,
                                IsFlag = false,
                                RoleType = RoleManagerType.Default,
                                DockId = 0,
                                ProjectId = dock.ProjectId,
                                Type = EEventSystemType.M_3014_NghiVanMatGiaoDich,
                                WarningType = EEventSystemWarningType.Danger,
                                TransactionCode = null,
                                UserId = 0,
                                AccountId = item.AccountId,
                                BikeId = item.BikeId,
                                StationId = item.StationIn,
                                Datas = $"[{new { item.DockId, bike.Plate, item.BikeId, item.TransactionCode, item.Id, item.AccountId, FullName = account?.FullName, Phone = account?.Phone }.ToJson()}]",
                                Title = $"Nghi vấn khóa mở nhưng không ghi nhận giao dịch",
                                Content = $"Xe {bike.Plate} nghi vấn không ghi nhận giao dịch do khóa xe phản hồi chậm, mã nghi vấn {item.TransactionCode}, quản trị liên hệ khách {account?.FullName} {account?.Phone} để xác mình",
                                CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                                UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                                CreatedDateTicks = DateTime.Now.Ticks,
                                UpdatedDateTicks = DateTime.Now.Ticks,
                                Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                            };
                            Controller.EventSystemAdd(addEvent);
                            continue;
                        }
                    }
                }
                IsCheckBookingFail = false;
            }
            catch (Exception ex)
            {
                IsCheckBookingFail = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [Job xử lý]
        public static void EventSystemCheck()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.EventSystemCheck";
            try
            {
                if (IsEventSystemCheck)
                {
                    return;
                }
                IsEventSystemCheck = true;
                var listEvent = Controller.EventSystemGets();
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Tìm thấy {listEvent.Count()} system event", false);

                if (listEvent.Any())
                {

                    foreach (var item in listEvent)
                    {
                        Controller.EventSystemUpdateStatus(item._id);
                        try
                        {
                            HubSendData(item.UserId, item, requestId, functionName).ConfigureAwait(false);
                        }
                        catch
                        {
                        }
                    }
                }
                IsEventSystemCheck = false;
            }
            catch (Exception ex)
            {
                IsEventSystemCheck = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }

        }

        public static void RecheckOrderZALOPAYRun()
        {
            _ = RecheckOrderZALOPAY();
        }

        public static void RecheckOrderMomoRun()
        {
            _ = RecheckOrderMomo();
        }

        public static void CheckAccountOnlineRun()
        {
            _ = CheckAccountOnline();
        }

        public static void CheckVTCPayStatusRun()
        {
            _ = CheckVTCPayStatus();
        }

        public static async Task CheckAccountOnline()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.CheckAccountOnline";
            try
            {
                if (IsCheckAccountOnline)
                {
                    return;
                }

                IsCheckAccountOnline = true;
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var stringContent = new StringContent("{}", UnicodeEncoding.UTF8, "application/json");
                    var result = await httpClient.PostAsync($"{AppSettings.GetAccountOnlinePath}", stringContent);
                    string outPut = "";
                    try
                    {
                        outPut = await result.Content.ReadAsStringAsync();
                        Controller.DataInfoUpdate(EDataInfoType.SumAccountLive, outPut);
                        Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Serve trả về {outPut}", false);
                    }
                    catch
                    { }
                }
                IsCheckAccountOnline = false;
            }
            catch (Exception ex)
            {
                IsCheckAccountOnline = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static async Task CheckVTCPayStatus()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.CheckVTCPayStatus";
            try
            {
                if (IsCheckVTCPayStatus)
                {
                    return;
                }

                IsCheckVTCPayStatus = true;
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var stringContent = new StringContent("{}", UnicodeEncoding.UTF8, "application/json");
                    var result = await httpClient.PostAsync($"{AppSettings.RecheckOrderVTCPath}", stringContent);
                    string outPut = "";
                    try
                    {
                        outPut = await result.Content.ReadAsStringAsync();
                        Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Serve trả về {outPut}", false);
                    }
                    catch
                    { }
                }
                IsCheckVTCPayStatus = false;
            }
            catch (Exception ex)
            {
                IsCheckVTCPayStatus = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void MongoCreateIndex()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.MongoCreateIndex";

            try
            {
                if (IsCreateIndexMongo)
                {
                    IsCreateIndexMongo = false;
                    return;
                }
                IsCreateIndexMongo = true;
                if (CreateIndexMongoDate != null && CreateIndexMongoDate.Value.Date == DateTime.Now.Date)
                {
                    IsCreateIndexMongo = false;
                    return;
                }
                bool isTime = DateTime.Now.Hour >= 0 && DateTime.Now.Hour <= 4;
                if (!isTime)
                {
                    IsCreateIndexMongo = false;
                    return;
                }
                var status = Functions.MongoCreateIndexAsync(true);
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Trạng thái {status}", false);
                CreateIndexMongoDate = DateTime.Now;
                IsCreateIndexMongo = false;
            }
            catch (Exception ex)
            {
                IsCreateIndexMongo = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }

        }

        public static void ClearSubMoneyRun()
        {
            _ = ClearSubMoney();
        }

        public static void ClearSubMoneyFistRun()
        {
            _ = ClearSubMoneyFist();
        }

        public static async Task ClearSubMoneyFist()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.ClearSubMoneyFist";
            try
            {
                if (IsClearSubMoneyFirst)
                {
                    return;
                }
                IsClearSubMoneyFirst = true;
                if (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 6)
                {
                    using (var httpClient = new HttpClient())
                    {
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var stringContent = new StringContent("{}", UnicodeEncoding.UTF8, "application/json");
                        var result = await httpClient.PostAsync($"{AppSettings.ClearSubPointFistPath}", stringContent);
                        string outPut = "";
                        try
                        {
                            outPut = await result.Content.ReadAsStringAsync();
                            Functions.AddLog(0, "SyncProcess.ClearSubMoneyFist", requestId, EnumMongoLogType.Start, $"Serve trả về {outPut}", false);
                        }
                        catch
                        { }
                    }
                }    
                IsClearSubMoneyFirst = false;
            }
            catch (Exception ex)
            {
                IsClearSubMoneyFirst = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static async Task ClearSubMoney()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.ClearSubMoney";
            try
            {
                if (IsClearSubMoney)
                {
                    return;
                }
                IsClearSubMoney = true;
                if(DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 6)
                {
                    using (var httpClient = new HttpClient())
                    {
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var stringContent = new StringContent("{}", UnicodeEncoding.UTF8, "application/json");
                        var result = await httpClient.PostAsync($"{AppSettings.ClearSubPointPath}", stringContent);
                        string outPut = "";
                        try
                        {
                            outPut = await result.Content.ReadAsStringAsync();
                            Functions.AddLog(0, "SyncProcess.ClearSubMoney", requestId, EnumMongoLogType.Start, $"Serve trả về {outPut}", false);
                        }
                        catch
                        { }
                    }
                }    
                IsClearSubMoney = false;
            }
            catch (Exception ex)
            {
                IsClearSubMoney = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public static void ClearFlagStatic()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsClearFlagStatic)
                {
                    return;
                }
                IsClearFlagStatic = true;
                flags = new List<Flag>();
                // Xóa flag có ngày tạo < 7 ngày hiện tại
                Controller.FlagClear();
                IsClearFlagStatic = false;
            }
            catch (Exception ex)
            {
                IsClearFlagStatic = false;
                Functions.AddLog(0, "SyncProcess.ClearFlagStatic", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public async static Task RecheckOrderMomo()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.RecheckOrderMomo";
            try
            {
                if (IsRecheckOrderMomo)
                {
                    return;
                }
                IsRecheckOrderMomo = true;
                var listData = Controller.WalletTransactionPendingMomoSearch();
                if (listData.Any())
                {
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Tìm thấy {listData.Count} bản ghi WalletTransaction");
                    foreach (var item in listData)
                    {
                        using (var httpClient = new HttpClient())
                        {
                            httpClient.DefaultRequestHeaders.Accept.Clear();
                            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var stringContent = new StringContent("{}", UnicodeEncoding.UTF8, "application/json");
                            var result = await httpClient.PostAsync($"{AppSettings.RecheckOrderMOMOPath}/{item.Id}", stringContent);
                            string outPut = "";
                            try
                            {
                                outPut = await result.Content.ReadAsStringAsync();
                            }
                            catch
                            { }
                            Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Check giao dịch #{item.Id} {result.StatusCode} => {outPut}");
                        }
                    }
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Kết thúc");
                }
                IsRecheckOrderMomo = false;
            }
            catch (Exception ex)
            {
                IsRecheckOrderMomo = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public async static Task RecheckOrderZALOPAY()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.RecheckOrderZALOPAY";
            try
            {
                if (IsRecheckOrderZALOPAY)
                {
                    return;
                }
                IsRecheckOrderZALOPAY = true;
                var listData = Controller.WalletTransactionPendingZALOSearch();
                if (listData.Any())
                {
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Tìm thấy {listData.Count} bản ghi WalletTransaction");
                    foreach (var item in listData)
                    {
                        using (var httpClient = new HttpClient())
                        {
                            httpClient.DefaultRequestHeaders.Accept.Clear();
                            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var stringContent = new StringContent("{}", UnicodeEncoding.UTF8, "application/json");
                            var result = await httpClient.PostAsync($"{AppSettings.RecheckOrderZALOPath}/{item.Id}", stringContent);
                            string outPut = "";
                            try
                            {
                                outPut = await result.Content.ReadAsStringAsync();
                            }
                            catch
                            { }
                            Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Check giao dịch #{item.Id} {result.StatusCode} => {outPut}");
                        }
                    }
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Kết thúc");
                }
                IsRecheckOrderZALOPAY = false;
            }
            catch (Exception ex)
            {
                IsRecheckOrderZALOPAY = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }

        public async static Task RecheckOrderPayooPAY()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.RecheckOrderPayooPAY";
            try
            {
                if (IsRecheckOrderPayooPay)
                {
                    return;
                }
                IsRecheckOrderPayooPay = true;
                var listData = Controller.WalletTransactionPendingPayooPaySearch();
                if (listData.Any())
                {
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Tìm thấy {listData.Count} bản ghi WalletTransaction");
                    foreach (var item in listData)
                    {
                        using (var httpClient = new HttpClient())
                        {
                            httpClient.DefaultRequestHeaders.Accept.Clear();
                            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var stringContent = new StringContent("{}", UnicodeEncoding.UTF8, "application/json");
                            var result = await httpClient.PostAsync($"{AppSettings.RecheckOrderPayooPath}/{item.Id}", stringContent);
                            string outPut = "";
                            try
                            {
                                outPut = await result.Content.ReadAsStringAsync();
                            }
                            catch
                            { }
                            Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Check giao dịch #{item.Id} {result.StatusCode} => {outPut}");
                        }
                    }
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Kết thúc");
                }
                IsRecheckOrderPayooPay = false;
            }
            catch (Exception ex)
            {
                IsRecheckOrderPayooPay = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [Update TransactionGPS]
        public static void UpdateTransactionGPS()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.UpdateTransactionGPS";
            try
            {
                if (IsUpdateTransactionGPS)
                {
                    return;
                }
                IsUpdateTransactionGPS = true;
                var data = Controller.TransactionByNotGPS();
                //Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Tìm thấy {data.Count} bản ghi chưa có GPS", false);
                foreach (var item in data)
                {
                    Controller.FilterGPS(functionName, requestId, item.Id, item.TransactionCode, item.DockId, item.StartTime, item.EndTime?.AddSeconds(10)?? item.StartTime.AddMinutes(item.TotalMinutes));
                    Task.Delay(10).ConfigureAwait(false);
                }    
                IsUpdateTransactionGPS = false;
            }
            catch (Exception ex)
            {
                IsUpdateTransactionGPS = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [Nhắc admin nghi vấn khách quên trả xe]
        public static void BookingForgotrReturnTheBike()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.BookingForgotrReturnTheBike";
            try
            {
                if (IsBookingForgotrReturnTheBike)
                {
                    return;
                }
                IsBookingForgotrReturnTheBike = true;

                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Bắt đầu quét");
                
                var listData = Controller.BookingSearchForgotrReturnTheBike();
                var stations = Controller.Stations();
                if (listData.Any())
                {
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Bắt đầu check xe khách quên trả xe không, tìm thấy {listData.Count}");

                    var docks = Controller.DocksByIds(listData.Select(x => x.DockId).ToList());
                    foreach(var item in listData)
                    {
                        var dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                        if(dock == null)
                        {
                            continue;
                        }
                        // getTop30 bản ghi GPS khác 0
                        var gpss = Functions.GPSGetByIMEITop(dock.IMEI, 90);
                        Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Tìm thấy {gpss.Count} hợp lệ");
                        if (gpss.Count < 90)
                        {
                            continue;
                        }

                        var stationCount = new List<StationCountModel>();
                        
                        foreach (var gps in gpss)
                        {
                            var station = stations.Where(x => x.ReturnDistance >= Functions.DistanceInMeter(gps.Lat, gps.Long, x.Lat, x.Lng)).OrderBy(x=> Functions.DistanceInMeter(gps.Lat, gps.Long, x.Lat, x.Lng)).FirstOrDefault();
                            if(station != null)
                            {
                                var stationIn = stationCount.FirstOrDefault(x=>x.StationId == station.Id);
                                if(stationIn != null)
                                {
                                    stationIn.Count += 1;
                                }   
                                else
                                {
                                    stationCount.Add(new StationCountModel { StationId = station.Id, Count = 1, Station = station });
                                }
                            }   
                        }

                        // Nếu đếm ko có bản ghi, hoặc ko có bản ghi trong trạm 
                        if(!stationCount.Any())
                        {
                            continue;
                        }

                        var banGhiDuDK = stationCount.FirstOrDefault(x => x.Count == stationCount.Max(m => m.Count));
                        if(banGhiDuDK == null)
                        {
                            continue;
                        }    
                        // Số lượng trong trạm nhiều nhất >= 90% thì cảnh báo
                        if(banGhiDuDK.Count >= 80)
                        {
                            // Tất cả đều tìm thấy trạm
                            Functions.AddLog(item.AccountId, functionName, requestId, EnumMongoLogType.Center, $"{banGhiDuDK.Count} điểm GPS đều cùng trạm #{banGhiDuDK?.Station?.Name}, chuyến đi {item.TransactionCode}");
                            // Update booking là khách quên trả xe
                            Controller.BookingForgotrReturnTheBikeSetStatus(item.Id, $"Trong 15 phút xe vẫn ở nguyên vị trí trong trạm {banGhiDuDK?.Station?.Name}, hệ thống tự động cảnh báo nghi vấn khách quên chưa trả xe");
                            // Gửi event cho admin
                            Controller.EventSystemAdd(new EventSystemMongo
                            {
                                IsMultiple = false,
                                IsJob = true,
                                IsProcessed = false,
                                IsFlag = false,
                                RoleType = RoleManagerType.Default,
                                DockId = item.DockId,
                                ProjectId = item.ProjectId,
                                Type = EEventSystemType.M_3015_KhachQuenTraXe,
                                WarningType = EEventSystemWarningType.Danger,
                                TransactionCode = null,
                                UserId = 0,
                                AccountId = item.AccountId,
                                BikeId = item.BikeId,
                                StationId = item.StationIn,
                                Datas = $"[{new { BikeId = item.BikeId, DockId = item.DockId, item.TransactionCode }.ToJson()}]",
                                Title = $"Khách quên trả xe mã chuyến đi {item.TransactionCode}",
                                Content = $"Khách quên trả xe mã chuyến đi {item.TransactionCode}, thời gian bắt đầu chuyến đi {item.StartTime: HH:mm dd/MM/yyyy}, QR {dock.SerialNumber}",
                                ParentId = 0,
                                CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                                UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                                CreatedDateTicks = DateTime.Now.Ticks,
                                UpdatedDateTicks = DateTime.Now.Ticks,
                                Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                            }
                         );
                        }    
                    }    
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Kết thúc");
                }

                BookingSearchForgotrReturnTheBikeRecheck(stations, functionName, requestId);
                IsBookingForgotrReturnTheBike = false;
            }
            catch (Exception ex)
            {
                IsBookingForgotrReturnTheBike = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        public static void BookingSearchForgotrReturnTheBikeRecheck(List<Station> stations,string functionName, string requestId)
        {
            var listData = Controller.BookingSearchForgotrReturnTheBikeRecheck();
            Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Kiểm tra lại xe nào bị check warning {listData.Count}");
            var docks = Controller.DocksByIds(listData.Select(x => x.DockId).ToList());
            foreach (var item in listData)
            {
                var dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                if (dock == null)
                {
                    continue;
                }
                // getTop30 bản ghi GPS khác 0
                var gpss = Functions.GPSGetByIMEITop(dock.IMEI, 90);
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Tìm thấy {gpss.Count} hợp lệ");
                if (gpss.Count < 90)
                {
                    continue;
                }

                var stationCount = new List<StationCountModel>();

                foreach (var gps in gpss)
                {
                    var station = stations.Where(x => x.ReturnDistance >= Functions.DistanceInMeter(gps.Lat, gps.Long, x.Lat, x.Lng)).OrderBy(x => Functions.DistanceInMeter(gps.Lat, gps.Long, x.Lat, x.Lng)).FirstOrDefault();
                    if (station != null)
                    {
                        var stationIn = stationCount.FirstOrDefault(x => x.StationId == station.Id);
                        if (stationIn != null)
                        {
                            stationIn.Count += 1;
                        }
                        else
                        {
                            stationCount.Add(new StationCountModel { StationId = station.Id, Count = 1, Station = station });
                        }
                    }
                }

                // Nếu đếm ko có bản ghi, hoặc ko có bản ghi trong trạm 
                if (!stationCount.Any())
                {
                    continue;
                }

                var banGhiDuDK = stationCount.FirstOrDefault(x => x.Count == stationCount.Max(m => m.Count));
                if (banGhiDuDK == null)
                {
                    continue;
                }
                // Số lượng trong trạm nhiều nhất >= 90% thì cảnh báo
                if (banGhiDuDK.Count < 80)
                {
                    // Gỡ bỏ nghi vấn
                    Functions.AddLog(item.AccountId, functionName, requestId, EnumMongoLogType.Center, $"{banGhiDuDK.Count} điểm GPS đều cùng trạm #{banGhiDuDK?.Station?.Name}, chuyến đi {item.TransactionCode}, gỡ bỏ nghi vẫn");
                    // Update booking là khách quên trả xe
                    Controller.BookingForgotrReturnTheBikeSetStatusFalse(item.Id, $"Hệ thống gỡ bỏ nghi vấn quên trả, {banGhiDuDK.Count} điểm trong trạm " );
                    // Gửi event cho admin
                    Controller.EventSystemAdd(new EventSystemMongo
                    {
                        IsMultiple = false,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.Default,
                        DockId = item.DockId,
                        ProjectId = item.ProjectId,
                        Type = EEventSystemType.M_3015_KhachQuenTraXe,
                        WarningType = EEventSystemWarningType.Warning,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = item.AccountId,
                        BikeId = item.BikeId,
                        StationId = item.StationIn,
                        Datas = $"[{new { BikeId = item.BikeId, DockId = item.DockId, item.TransactionCode }.ToJson()}]",
                        Title = $"Gỡ bỏ nghi vấn khách quên trả xe mã chuyến đi {item.TransactionCode}",
                        Content = $"Giỡ bỏ nghi vấn khách quên trả xe mã chuyến đi {item.TransactionCode}, thời gian bắt đầu chuyến đi {item.StartTime: HH:mm dd/MM/yyyy}, QR {dock.SerialNumber}",
                        ParentId = 0,
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    }
                 );
                }
            }
        }

        #region [#1001 (SMS,email,notify) Nhắc xe mất kết nối 30p/ lần]
        private static void CheckBikeDisconect()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsCheckBikeDisconect)
                {
                    return;
                }
                IsCheckBikeDisconect = true;
                var config = Controller.EmailManageGet();
                var datas = Controller.BikeDisconect();
                var users = Controller.ApplicationUsers().Where(x=> x.RoleType == RoleManagerType.Coordinator_VTS || x.RoleType == RoleManagerType.Admin);
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();

                foreach (var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId);
                    if(!data.Any())
                    {
                        continue;
                    }
                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.Default,
                        DockId = 0,
                        ProjectId = projectId,
                        Type = EEventSystemType.M_1001_XeMatKetNoiBE,
                        WarningType = EEventSystemWarningType.Danger,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = $"Có {data.Count()} xe đang mất kết nối",
                        Content = $"Có {data.Count()} xe đang mất kết nối {string.Join(", ", data.Select(x => x.Plate))}",
                        ParentId = 0,
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };
                    Controller.EventSystemAdd(addEvent);

                    foreach (var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent);
                    }

                    var phones = string.Join(",", users.Select(x => x.PhoneNumber));
                    var emails = string.Join(",", users.Select(x => x.Email));

                    // Giửi sms
                    Controller.SMSBoxAdd(new SMSBox
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        PhoneNumber = phones,
                        SendDate = DateTime.Now,
                        ReSend = 0,
                        SuccessfulSendDate = null,
                        Content = $"Có {data.Count()} xe đang mất kết nối"
                    });
                    // Giửi sms
                    Controller.SMSBoxAdd(new SMSBox
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        PhoneNumber = phones,
                        SendDate = DateTime.Now,
                        ReSend = 0,
                        SuccessfulSendDate = null,
                        Content = $"Có {data.Count()} xe đang mất kết nối"
                    });
                    // Gửi email
                    string styletd = "border: 1px solid silver; padding: 1px 10px;";
                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Biển số</td><td style=\"{styletd}\">Tên trạm</td><td style=\"{styletd}\">Tên trạm khác</td><td style=\"{styletd}\">Địa chỉ</td></tr>");
                    int stt = 1;
                    foreach (var item in data)
                    {
                        str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{item.Plate}</td><td style=\"{styletd}\">{item.StationName1}</td><td style=\"{styletd}\">{item.StationName2}</td><td style=\"{styletd}\">{item.Address}</td></tr>");
                        stt++;
                    }
                    str.Append("</table>");
                    Controller.EmailBoxAdd(config?.From, emails, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_1001_XeMatKetNoiBE} {DateTime.Now:dd-MM-yyyy}] Có {data.Count()} xe đang mất kết nối", str.ToString(), 0);
                }

                IsCheckBikeDisconect = false;
            }
            catch (Exception ex)
            {
                IsCheckBikeDisconect = false;
                Functions.AddLog(0, "SyncProcess.CheckBikeDisconect", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#1004 (SMS,email,notify) Xe hết PIN/sắp hết pin]
        private static void AutoCheckLowBattery()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsCheckLowBattery)
                {
                    return;
                }

                IsCheckLowBattery = true;
                var config = Controller.EmailManageGet();
                var datas = Controller.BikeByLowBattery();
                var users = Controller.ApplicationUsers().Where(x => x.RoleType == RoleManagerType.Coordinator_VTS);
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();

                string styletd = "border: 1px solid silver; padding: 1px 10px;";
                foreach (var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId).ToList();
                    if (!data.Any())
                    {
                        continue;
                    }

                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.Default,
                        DockId = 0,
                        ProjectId = projectId,
                        Type = EEventSystemType.M_1004_XeHetPinSapHetPin,
                        WarningType = EEventSystemWarningType.Danger,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = $"Có {data.Where(x => x.Battery == 0).Count()} xe hết pin và {data.Where(x => x.Battery != 0).Count()} gần hết pin",
                        Content = $"Có {data.Where(x => x.Battery == 0).Count()} xe hết pin và {data.Where(x => x.Battery != 0).Count()} gần hết pin {string.Join(", ", data.Select(x => x.Plate))}",
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };
                    Controller.EventSystemAdd(addEvent);
                    var phones = string.Join(",", users.Select(x => x.PhoneNumber));
                    var emails = string.Join(",", users.Select(x => x.Email));
                    foreach (var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent);
                    }
                    // Giửi sms
                    Controller.SMSBoxAdd(new SMSBox
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        NotificationDataId = 0,
                        PhoneNumber = phones,
                        SendDate = DateTime.Now,
                        ReSend = 0,
                        SuccessfulSendDate = null,
                        Content = $"Có {data.Where(x => x.Battery == 0).Count()} xe hết pin và {data.Where(x => x.Battery != 0).Count()} gần hết pin"
                    });

                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Biển số</td><td style=\"{styletd}\">% pin</td><td style=\"{styletd}\">Tên trạm</td><td style=\"{styletd}\">Tên trạm khác</td><td style=\"{styletd}\">Địa chỉ</td></tr>");
                    int stt = 1;
                    foreach (var item in data)
                    {
                        str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{item.Plate}</td><td style=\"{styletd}\">{item.Battery}</td><td style=\"{styletd}\">{item.StationName1}</td><td style=\"{styletd}\">{item.StationName2}</td><td style=\"{styletd}\">{item.Address}</td></tr>");
                    }
                    str.Append("</table>");
                    Controller.EmailBoxAdd(config.From, emails, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_1004_XeHetPinSapHetPin} {DateTime.Now:dd-MM-yyyy}] Có {data.Where(x => x.Battery == 0).Count()} xe hết pin và {data.Where(x => x.Battery != 0).Count()} gần hết pin", str.ToString(), 0);
                    stt++;
                }
                IsCheckLowBattery = false;
            }
            catch (Exception ex)
            {
                IsCheckLowBattery = false;
                Functions.AddLog(0, "SyncProcess.AutoCheckLowBattery", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#(1002 Đổ xe, #1007 di chuyển bất hợp pháp) #(1003 xe không trong trạm) #(1005 Xe mất GPS) #(X xe đi quá xa) (email,notify) ]
        private static void CheckBikeWarning()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsBikeWarning)
                {
                    return;
                }
                IsBikeWarning = true;

                var config = Controller.EmailManageGet();
                var datas = Controller.BikeWarnings();
                var users = Controller.ApplicationUsers().Where(x => x.RoleType == RoleManagerType.Coordinator_VTS);
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();

                string styletd = "border: 1px solid silver; padding: 1px 10px;";
                foreach (var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId).ToList();

                    if (!data.Any())
                    {
                        continue;
                    }

                    var warningTypes = data.GroupBy(x => x.WarningType).Select(x => x.Key).Distinct().ToList();

                    foreach (var warningType in warningTypes)
                    {
                        var dataByWarningType = data.Where(x => x.WarningType == warningType).ToList();
                        if (!dataByWarningType.Any())
                        {
                            continue;
                        }

                        string title = "";
                        var eventType = EEventSystemType.M_1005_XeMatGPS;

                        if (warningType == EBikeWarningType.DoXe)
                        {
                            eventType = EEventSystemType.M_1002_XeDo;
                        }
                        else if (warningType == EBikeWarningType.DiChuyenBatHopPhap)
                        {
                            eventType = EEventSystemType.M_1007_DiChuyenBatHopPhap;
                        }
                        else if (warningType == EBikeWarningType.XeKhongTrongTram)
                        {
                            eventType = EEventSystemType.M_1003_XeKhongLamTrongTram;
                        }
                        else if (warningType == EBikeWarningType.XeLoiGPS)
                        {
                            eventType = EEventSystemType.M_1005_XeMatGPS;
                        }
                        else
                        {
                            continue;
                        }

                        title = $"Có {dataByWarningType.Count()} {eventType.GetDisplayName().ToLower()}";

                        var addEvent = new EventSystemMongo
                        {
                            IsMultiple = true,
                            IsJob = true,
                            IsProcessed = false,
                            IsFlag = false,
                            RoleType = RoleManagerType.Default,
                            DockId = 0,
                            ProjectId = projectId,
                            Type = eventType,
                            WarningType = EEventSystemWarningType.Danger,
                            TransactionCode = null,
                            UserId = 0,
                            AccountId = 0,
                            BikeId = 0,
                            StationId = 0,
                            Datas = dataByWarningType.ToJson(),
                            Title = title,
                            Content = $"{title} {string.Join(",", dataByWarningType.Select(x => x.Plate))}",
                            CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                            UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                            CreatedDateTicks = DateTime.Now.Ticks,
                            UpdatedDateTicks = DateTime.Now.Ticks,
                            Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                        };

                        Controller.EventSystemAdd(addEvent);

                        var emails = string.Join(",", users.Select(x => x.Email));

                        foreach (var user in users)
                        {
                            addEvent.UserId = user.Id;
                            addEvent.RoleType = user.RoleType;
                            Controller.EventSystemAdd(addEvent);
                        }

                        var str = new StringBuilder();
                        str.Append($"<table style=\"border-collapse: collapse;\">");
                        str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Biển số</td><td style=\"{styletd}\">Thời gian cảnh báo</td><td style=\"{styletd}\">Tên trạm</td><td style=\"{styletd}\">Tên trạm khác</td><td style=\"{styletd}\">Địa chỉ</td></tr>");
                        int stt = 1;
                        foreach (var item in dataByWarningType)
                        {
                            str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{item.Plate}</td><td style=\"{styletd}\">{item.UpdatedDate:HH:mm:ss dd/MM/yyyy}</td><td style=\"{styletd}\">{item.StationName1}</td><td style=\"{styletd}\">{item.StationName2}</td><td style=\"{styletd}\">{item.Address}</td></tr>");
                            stt++;
                        }
                        str.Append("</table>");
                        Controller.EmailBoxAdd(config.From, emails, DateTime.Now, $"[TNGO {(int)eventType} {DateTime.Now:dd-MM-yyyy}] {title}", str.ToString(), 0);
                    }
                }
                IsBikeWarning = false;
            }
            catch (Exception ex)
            {
                IsBikeWarning = false;
                Functions.AddLog(0, "SyncProcess.CheckBikeWarning", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#1006 (SMS,email,notify) Xe mở mà không trong chuyến đi nào]
        private static void CheckDockOpenNotInBooking()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsCheckDockOpenNotInBooking)
                {
                    return;
                }
                IsCheckDockOpenNotInBooking = true;
                var config = Controller.EmailManageGet();
                var datas = Controller.DockOpenNotInBookings();
                var users = Controller.ApplicationUsers().Where(x => x.RoleType == RoleManagerType.Coordinator_VTS);
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();

                string styletd = "border: 1px solid silver; padding: 1px 10px;";
                foreach (var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId).ToList();

                    if (!data.Any())
                    {
                        continue;
                    }

                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.Default,
                        DockId = 0,
                        ProjectId = projectId,
                        Type = EEventSystemType.M_1006_XeMoNhungKhongTrongChuyenDiNao,
                        WarningType = EEventSystemWarningType.Danger,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = $"Có {data.Count()} xe mở khóa mà không trong chuyến đi nào",
                        Content = $"Có {data.Count()} xe mở khóa mà không trong chuyến đi nào {string.Join(", ", data.Select(x => x.Plate))}",
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };
                    Controller.EventSystemAdd(addEvent);
                    var phones = string.Join(",", users.Select(x => x.PhoneNumber));
                    var emails = string.Join(",", users.Select(x => x.Email));
                    foreach (var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent);
                    }
                    // Giửi sms
                    Controller.SMSBoxAdd(new SMSBox
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        PhoneNumber = phones,
                        SendDate = DateTime.Now,
                        ReSend = 0,
                        SuccessfulSendDate = null,
                        Content = $"Có {data.Count()} xe mở khóa mà không trong chuyến đi nào"
                    });

                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Biển số</td><td style=\"{styletd}\">Tên trạm</td><td style=\"{styletd}\">Tên trạm khác</td><td style=\"{styletd}\">Địa chỉ</td></tr>");
                    int stt = 1;
                    foreach (var item in data)
                    {
                        str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{item.Plate}</td><td style=\"{styletd}\">{item.StationName1}</td><td style=\"{styletd}\">{item.StationName2}</td><td style=\"{styletd}\">{item.Address}</td></tr>");
                    }
                    str.Append("</table>");
                    Controller.EmailBoxAdd(config.From, emails, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_1006_XeMoNhungKhongTrongChuyenDiNao} {DateTime.Now:dd-MM-yyyy}] Có {data.Count()} xe mở khóa mà không trong chuyến đi nào", str.ToString(), 0);
                    stt++;
                }
                IsCheckDockOpenNotInBooking = false;
            }
            catch (Exception ex)
            {
                IsCheckDockOpenNotInBooking = false;
                Functions.AddLog(0, "SyncProcess.CheckDockOpenNotInBooking", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#2001 (SMS,email,notify) Nhắc thừa xe thiếu xe]
        // <=2 xe là thiếu xe, còn 1 xe nữa là đầy xe
        private static void CheckStationTotalBike()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsCheckStationTotalBike)
                {
                    return;
                }
                IsCheckStationTotalBike = true;
                string styletd = "border: 1px solid silver; padding: 1px 10px;";
                var config = Controller.EmailManageGet();
                var datas = Controller.StationTotalBike();
                var users = Controller.ApplicationUsers().Where(x => x.RoleType == RoleManagerType.Coordinator_VTS);
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();

                foreach (var item in datas)
                {
                    if (item.TotalBike >= (item.Spaces - 3))
                    {
                        item.Note = "Thừa xe";
                    }
                    else if (item.TotalBike <= 3)
                    {
                        item.Note = "Thiếu xe";
                    }
                    else
                    {
                        item.Note = "Thiếu xe";
                    }
                }

                foreach (var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId).ToList();

                    if (!data.Any())
                    {
                        continue;
                    }

                    var content = 
                        $@"
                            Có {data.Where(x => x.Note == "Thiếu xe").Count()} trạm thiếu xe
                            => {string.Join(",", data.Where(x => x.Note == "Thiếu xe").Select(x => x.Name))}
                            Có {data.Where(x => x.Note == "Thừa xe").Count()} trạm thừa xe
                            => {string.Join(",", data.Where(x => x.Note == "Thừa xe").Select(x => x.Name))}
                        ";

                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.Default,
                        DockId = 0,
                        ProjectId = projectId,
                        Type = EEventSystemType.M_2001_ThuaXeThieuXe,
                        WarningType = EEventSystemWarningType.Warning,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = $"Có {data.Where(x => x.Note == "Thiếu xe").Count()} trạm thiếu xe, {data.Where(x => x.Note == "Thừa xe").Count()} trạm thừa xe",
                        Content = content,
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };
                    Controller.EventSystemAdd(addEvent);
                    var phones = string.Join(",", users.Select(x => x.PhoneNumber));
                    var emails = string.Join(",", users.Select(x => x.Email));

                    foreach (var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent);
                    }

                    Controller.SMSBoxAdd(new SMSBox
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        PhoneNumber = phones,
                        SendDate = DateTime.Now,
                        ReSend = 0,
                        SuccessfulSendDate = null,
                        Content = $"Có {data.Where(x => x.Note == "Thiếu xe").Count()} trạm thiếu xe, {data.Where(x => x.Note == "Thừa xe").Count()} trạm thừa xe"
                    });

                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Tên trạm</td><td style=\"{styletd}\">Tên trạm khác</td><td style=\"{styletd}\">Địa chỉ</td><td style=\"{styletd}\">Số xe / số chỗ</td><td style=\"{styletd}\">Ghi chú</td></tr>");
                    int stt = 1;
                    foreach (var station in data)
                    {
                        str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{station.Name}</td><td style=\"{styletd}\">{station.DisplayName}</td><td style=\"{styletd}\">{station.Address}</td><td style=\"{styletd}\">{station.TotalBike}/{station.Spaces}</td><td style=\"{styletd}\">{station.Note}</td></tr>");
                        stt++;
                    }
                    str.Append("</table>");
                    Controller.EmailBoxAdd(config.From, emails, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_2001_ThuaXeThieuXe} {DateTime.Now:dd-MM-yyyy}] Có {data.Where(x => x.Note == "Thiếu xe").Count()} trạm thiếu xe, {data.Where(x => x.Note == "Thừa xe").Count()} trạm thừa xe", str.ToString(), 0);
                }

                IsCheckStationTotalBike = false;
            }
            catch (Exception ex)
            {
                IsCheckStationTotalBike = false;
                Functions.AddLog(0, "SyncProcess.CheckStationTotalBike", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#4003 (sms, email, notify) Nhắc KH nợ cước]
        private static void CheckDebtCharges()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsCheckDebtCharges)
                {
                    return;
                }
                IsCheckDebtCharges = true;
                string styletd = "border: 1px solid silver; padding: 1px 10px;";
                var config = Controller.EmailManageGet();
                var users = Controller.ApplicationUsers().Where(x => (x.RoleType == RoleManagerType.BOD_VTS || x.RoleType == RoleManagerType.KTT_VTS || x.RoleType == RoleManagerType.CSKH_VTS)).ToList();
                var datas = Controller.TransactionsDebtCharges();
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();
                foreach(var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId).ToList();
                    if (!datas.Any())
                    {
                        continue;
                    }
                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.Default,
                        DockId = 0,
                        ProjectId = 0,
                        Type = EEventSystemType.M_4003_ChuyenDiNoCuoc,
                        WarningType = EEventSystemWarningType.Danger,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = $"Có {datas.Count()} chuyến đi nợ cước, tổng số điểm nợ cước {data.Sum(x => x.ChargeDebt)}",
                        Content = $"Có {datas.Count()} chuyến đi nợ cước, tổng số điểm nợ cước {data.Sum(x => x.ChargeDebt)} mã chuyến đi {string.Join(", ", data.Select(x => x.TransactionCode))}",
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };
                    Controller.EventSystemAdd(addEvent);
                    var phones = string.Join(",", users.Select(x => x.PhoneNumber));
                    var emails = string.Join(",", users.Select(x => x.Email));
                    foreach (var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent);
                    }

                    Controller.SMSBoxAdd(new SMSBox
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        PhoneNumber = phones,
                        SendDate = DateTime.Now,
                        ReSend = 0,
                        SuccessfulSendDate = null,
                        Content = $"Có {data.Count()} chuyến đi nợ cước, tổng số điểm nợ cước {data.Sum(x => x.ChargeDebt)}"
                    });

                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr>" +
                        $"<td style=\"{styletd}\">Stt</td>" +
                        $"<td style=\"{styletd}\">Mã giao dịch</td>" +
                        $"<td style=\"{styletd}\">Thời gian đi</td>" +
                        $"<td style=\"{styletd}\">Tên khách hàng</td>" +
                        $"<td style=\"{styletd}\">Email khách hàng</td>" +
                        $"<td style=\"{styletd}\">SĐT khách hàng</td>" +
                        $"<td style=\"{styletd}\">Số điểm nợ</td>" +
                        $"</tr>");
                    int stt = 1;
                    foreach (var station in data)
                    {
                        str.Append($"<tr>" +
                            $"<td style=\"{styletd}\">{stt}</td>" +
                            $"<td style=\"{styletd}\">{station.TransactionCode}</td>" +
                            $"<td style=\"{styletd}\">{station.StartTime:dd/MM/yyyy HH:mm:ss} - {station.EndTime:dd/MM/yyyy HH:mm:ss}</td>" +
                            $"<td style=\"{styletd}\">{station.FullName}</td>" +
                            $"<td style=\"{styletd}\">{station.Email}</td>" +
                            $"<td style=\"{styletd}\">{station.Phone}</td>" +
                            $"<td style=\"{styletd}\">{station.ChargeDebt}</td>" +
                            $"</tr>");
                        stt++;
                    }
                    str.Append("</table>");
                    Controller.EmailBoxAdd(config.From, emails, DateTime.Now, $"[TNGO-thông báo khách hàng nợ cước ngày {DateTime.Now:dd-MM-yyyy}] yêu cầu kiểm tra để xử lý", str.ToString(), 0);
                }    
                IsCheckDebtCharges = false;
            }
            catch (Exception ex)
            {
                IsCheckDebtCharges = false;
                Functions.AddLog(0, "SyncProcess.CheckDebtCharges", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#3011 (SMS,email,notify) Khách hàng báo hỏng xe]
        private static void CheckBikeReport()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsCheckBikeReport)
                {
                    return;
                }

                IsCheckBikeReport = true;

                var config = Controller.EmailManageGet();
                var users = Controller.ApplicationUsers().Where(x => x.RoleType == RoleManagerType.Coordinator_VTS || x.RoleType == RoleManagerType.CSKH_VTS);
                var datas = Controller.BikeReport();
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();

                foreach (var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId).ToList();
                    if(!data.Any())
                    {
                        continue;
                    }
                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.Default,
                        DockId = 0,
                        ProjectId = projectId,
                        Type = EEventSystemType.M_3011_KhachBaoHongXe,
                        WarningType = EEventSystemWarningType.Danger,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = $"Có {data.Count()} thông báo hỏng, lỗi xe đang cần xử lý",
                        Content = $"Có {data.Count()} thông báo hỏng, lỗi xe đang cần xử lý  {string.Join(", ", data.Select(x => x.Plate).Distinct())}",
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };
                    Controller.EventSystemAdd(addEvent);
                    var phones = string.Join(",", users.Select(x => x.PhoneNumber));
                    var emails = string.Join(",", users.Select(x => x.Email));
                    foreach (var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent);
                    }
                    // Giửi sms
                    Controller.SMSBoxAdd(new SMSBox
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        PhoneNumber = phones,
                        SendDate = DateTime.Now,
                        ReSend = 0,
                        SuccessfulSendDate = null,
                        Content = $"Có {data.Count()} thông báo hỏng, lỗi xe đang cần xử lý"
                    });

                    string styletd = "border: 1px solid silver; padding: 1px 10px;";
                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr>" +
                        $"<td style=\"{styletd}\">Stt</td>" +
                        $"<td style=\"{styletd}\">Xe</td>" +
                        $"<td style=\"{styletd}\">Tên khách hàng</td>" +
                        $"<td style=\"{styletd}\">Email khách hàng</td>" +
                        $"<td style=\"{styletd}\">SĐT khách hàng</td>" +
                        $"<td style=\"{styletd}\">Loại lỗi</td>" +
                        $"<td style=\"{styletd}\">Tên trạm</td><td style=\"{styletd}\">Tên trạm khác</td><td style=\"{styletd}\">Địa chỉ</td>" +
                        $"</tr>");

                    int stt = 1;
                    foreach (var item in data)
                    {
                        str.Append($"<tr>" +
                            $"<td style=\"{styletd}\">{stt}</td>" +
                            $"<td style=\"{styletd}\">{item.Plate}</td>" +
                            $"<td style=\"{styletd}\">{item.CreatedDate:dd/MM/yyyy HH:mm:ss}</td>" +
                            $"<td style=\"{styletd}\">{item.FullName}</td>" +
                            $"<td style=\"{styletd}\">{item.Email}</td>" +
                            $"<td style=\"{styletd}\">{item.Phone}</td>" +
                            $"<td style=\"{styletd}\">{item.Type.GetDisplayName()}</td>" +
                            $"<td style=\"{styletd}\">{item.StationName1}</td><td style=\"{styletd}\">{item.StationName2}</td><td style=\"{styletd}\">{item.Address}</td>" +
                            $"</tr>");
                        stt++;
                    }
                    str.Append("</table>");
                    Controller.EmailBoxAdd(config.From, emails, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_3011_KhachBaoHongXe} {DateTime.Now:dd-MM-yyyy}] Có {data.Count()} thông báo hỏng, lỗi xe đang cần xử lý", str.ToString(), 0);
                }    
                
                IsCheckBikeReport = false;
            }
            catch (Exception ex)
            {
                IsCheckBikeReport = false;
                Functions.AddLog(0, "SyncProcess.CheckBikeReport", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#4001 (sms, email, notify) Có yêu cầu xác minh thông tin khác hàng]
        private static void CheckAccountVerifyStatus()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsCheckAccountVerifyStatus)
                {
                    return;
                }
                IsCheckAccountVerifyStatus = true;
                var config = Controller.EmailManageGet();
                var users = Controller.ApplicationUsers().Where(x => x.RoleType == RoleManagerType.CSKH_VTS).ToList();
                var data = Controller.AccountVerifyStatus();

                if (data.Any())
                {
                    string text = $"Có {data.Where(x => x.VerifyStatus == EAccountVerifyStatus.Waiting).Sum(x => x.Count)} khách hàng gửi yêu cầu xác minh, {data.Where(x => x.VerifyStatus == EAccountVerifyStatus.Not).Sum(x => x.Count)} chưa gửi xác minh, {data.Where(x => x.VerifyStatus == EAccountVerifyStatus.Refuse).Sum(x => x.Count)} yêu cầu xác minh lại";
                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.Default,
                        DockId = 0,
                        ProjectId = 0,
                        Type = EEventSystemType.M_4001_CoYeuCauXacMinHThongTinKhachHang,
                        WarningType = EEventSystemWarningType.Danger,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = text,
                        Content = text,
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };
                    Controller.EventSystemAdd(addEvent);

                    foreach (var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent);
                        Controller.EmailBoxAdd(config.From, user.Email, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_4001_CoYeuCauXacMinHThongTinKhachHang} {DateTime.Now:dd-MM-yyyy}] {text}", text, 0);
                    }
                }
                IsCheckAccountVerifyStatus = false;
            }
            catch (Exception ex)
            {
                IsCheckAccountVerifyStatus = false;
                Functions.AddLog(0, "SyncProcess.CheckAccountVerifyStatus", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#3009 Cước chuyến đi vượt quá số dư]
        public static void CheckDebtSystem()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.CheckDebtSystem";
            try
            {
                if (IsCheckDebtSystem)
                {
                    return;
                }

                IsCheckDebtSystem = true;

                var config = Controller.EmailManageGet();
                var users = Controller.ApplicationUsers().Where(x => x.RoleType == RoleManagerType.CSKH_VTS).ToList();
                var datas = Controller.BookingSearch();
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();
                var listVi = Controller.WalletByAccount(datas.Select(x => x.AccountId).Distinct().ToList());

                foreach (var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId && listVi.Any(m => m.AccountId == x.AccountId && (m.Balance + m.SubBalance) - x.TotalPrice < 0)).ToList();
                    if (!data.Any())
                    {
                        continue;
                    }
                    string text = $"Có {data.Count()} chuyến đi của khách hàng đang vượt quá số điểm còn trong tài khoản";
                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.CSKH_VTS,
                        DockId = 0,
                        ProjectId = 0,
                        Type = EEventSystemType.M_3009_CuocChuyenVuotQuaSoDu,
                        WarningType = EEventSystemWarningType.Danger,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = text,
                        Content = text,
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };

                    Controller.EventSystemAdd(addEvent);

                    foreach(var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent);

                        string styletd = "border: 1px solid silver; padding: 1px 10px;";
                        var str = new StringBuilder();
                        str.Append($"<table style=\"border-collapse: collapse;\">");
                        str.Append($"<tr>" +
                            $"<td style=\"{styletd}\">Stt</td>" +
                            $"<td style=\"{styletd}\">Xe</td>" +
                            $"<td style=\"{styletd}\">Mã chuyến đi</td>" +
                            $"<td style=\"{styletd}\">Bất đầu chuyến</td>" +
                            $"<td style=\"{styletd}\">Cước thiếu</td>" +
                            $"<td style=\"{styletd}\">Tên khách hàng</td>" +
                            $"<td style=\"{styletd}\">Email khách hàng</td>" +
                            $"<td style=\"{styletd}\">SĐT khách hàng</td>" +
                            $"</tr>");

                        int stt = 1;
                        foreach (var item in data)
                        {
                            var getVi = listVi.FirstOrDefault(x => x.AccountId == item.AccountId);
                            if (getVi == null)
                            {
                                continue;
                            }
                            str.Append($"<tr>" +
                                $"<td style=\"{styletd}\">{stt}</td>" +
                                $"<td style=\"{styletd}\">{item.Plate}</td>" +
                                $"<td style=\"{styletd}\">{item.TransactionCode}</td>" +
                                $"<td style=\"{styletd}\">{item.CreatedDate:dd/MM/yyyy HH:mm:ss}</td>" +
                                $"<td style=\"{styletd}\">{ (getVi.Balance + getVi.SubBalance - item.TotalPrice)}</td>" +
                                $"<td style=\"{styletd}\">{item.FullName}</td>" +
                                $"<td style=\"{styletd}\">{item.Email}</td>" +
                                $"<td style=\"{styletd}\">{item.Phone}</td>" +
                                $"</tr>");
                            stt++;
                        }
                        str.Append($"</table>");
                        Controller.EmailBoxAdd(config.From, user.Email, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_3009_CuocChuyenVuotQuaSoDu} {DateTime.Now:dd-MM-yyyy}] {text}", str.ToString(), 0);
                    }    
                }
                IsCheckDebtSystem = false;
            }
            catch (Exception ex)
            {
                IsCheckDebtSystem = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#3013 (SMS,Email,Event) Tự động thông báo cho khách khi khách đóng khóa trong chuyến đi > 30p, #3013]
        public static void CheckCloseDockByBookingSystem()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.CheckCloseDockByBooking";
            try
            {
                if (IsCheckCloseDockByBookingSystem)
                {
                    return;
                }
                IsCheckCloseDockByBookingSystem = true;
                var config = Controller.EmailManageGet();
                var datas = Controller.CloseDockByBookings();
                var projectIds = datas.GroupBy(x => x.ProjectId).Select(x => x.Key).Distinct().ToList();
                var users = Controller.ApplicationUsers().Where(x => x.RoleType == RoleManagerType.CSKH_VTS || x.RoleType == RoleManagerType.Coordinator_VTS).ToList();
                foreach(var projectId in projectIds)
                {
                    var data = datas.Where(x => x.ProjectId == projectId).ToList();
                    if (!data.Any())
                    {
                        continue;
                    }
                    string text = $"Có {data.Count()} chuyến đi của khách hàng đang khóa xe > 30p";

                    var addEvent = new EventSystemMongo
                    {
                        IsMultiple = true,
                        IsJob = true,
                        IsProcessed = false,
                        IsFlag = false,
                        RoleType = RoleManagerType.CSKH_VTS,
                        DockId = 0,
                        ProjectId = 0,
                        Type = EEventSystemType.M_3013_KhachHangDungXeQuaLau,
                        WarningType = EEventSystemWarningType.Danger,
                        TransactionCode = null,
                        UserId = 0,
                        AccountId = 0,
                        BikeId = 0,
                        StationId = 0,
                        Datas = data.ToJson(),
                        Title = text,
                        Content = text,
                        CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                        CreatedDateTicks = DateTime.Now.Ticks,
                        UpdatedDateTicks = DateTime.Now.Ticks,
                        Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
                    };
                    Controller.EventSystemAdd(addEvent);
                    var phones = string.Join(",", users.Select(x => x.PhoneNumber));
                    var emails = string.Join(",", users.Select(x => x.Email));

                    foreach (var user in users)
                    {
                        addEvent.UserId = user.Id;
                        addEvent.RoleType = user.RoleType;
                        Controller.EventSystemAdd(addEvent); 
                    }

                    Controller.SMSBoxAdd(new SMSBox
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        PhoneNumber = phones,
                        SendDate = DateTime.Now,
                        ReSend = 0,
                        SuccessfulSendDate = null,
                        Content = text
                    });

                    string styletd = "border: 1px solid silver; padding: 1px 10px;";
                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr>" +
                        $"<td style=\"{styletd}\">Stt</td>" +
                        $"<td style=\"{styletd}\">Xe</td>" +
                        $"<td style=\"{styletd}\">Mã chuyến đi</td>" +
                        $"<td style=\"{styletd}\">Bất đầu chuyến</td>" +
                        $"<td style=\"{styletd}\">Chi phí</td>" +
                        $"<td style=\"{styletd}\">Tổng số phút</td>" +
                        $"<td style=\"{styletd}\">Tên khách hàng</td>" +
                        $"<td style=\"{styletd}\">Email khách hàng</td>" +
                        $"<td style=\"{styletd}\">SĐT khách hàng</td>" +
                        $"</tr>");

                    int stt = 1;
                    foreach (var item in data)
                    {
                        str.Append($"<tr>" +
                            $"<td style=\"{styletd}\">{stt}</td>" +
                            $"<td style=\"{styletd}\">{item.Plate}</td>" +
                            $"<td style=\"{styletd}\">{item.TransactionCode}</td>" +
                            $"<td style=\"{styletd}\">{item.CreatedDate:dd/MM/yyyy HH:mm:ss}</td>" +
                            $"<td style=\"{styletd}\">{(item.TotalPrice)}</td>" +
                            $"<td style=\"{styletd}\">{(item.TotalMinutes)}</td>" +
                            $"<td style=\"{styletd}\">{item.FullName}</td>" +
                            $"<td style=\"{styletd}\">{item.Email}</td>" +
                            $"<td style=\"{styletd}\">{item.Phone}</td>" +
                            $"</tr>");
                        stt++;
                    }
                    str.Append($"</table>");
                    Controller.EmailBoxAdd(config.From, emails, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_3013_KhachHangDungXeQuaLau} {DateTime.Now:dd-MM-yyyy}] {text}", str.ToString(), 0);
                }
                IsCheckCloseDockByBookingSystem = false;
            }
            catch (Exception ex)
            {
                IsCheckCloseDockByBookingSystem = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#auto Tự động thông báo cho khách khi khách đóng khóa trong chuyến đi > 30p, #3013]
        public static void CheckCloseDockByBooking()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.CheckCloseDockByBooking";
            try
            {
                if (IsCheckCloseDockByBooking)
                {
                    return;
                }
                IsCheckCloseDockByBooking = true;
                var listData = Controller.CloseDockByBookings();
                if (listData.Any())
                {
                    foreach (var item in listData)
                    {
                        Controller.NotificationJobAdd(item.AccountId, "TNGO thông báo", "TNGO thấy bạn trong chuyến đi nhưng đóng khóa xe khá lâu, nếu không phải quên trả xe thì bỏ qua tin nhắn này nhé");
                        Controller.FlagUpdateTime(item.FlagTransaction);
                    }
                }
                IsCheckCloseDockByBooking = false;
            }
            catch (Exception ex)
            {
                IsCheckCloseDockByBooking = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#auto Tự động thông báo cho khách khi trong chuyến đi nợ cước]
        public static void CheckDebtTransaction()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.CheckDebtTransaction";
            try
            {
                if (IsCheckDebtTransaction)
                {
                    return;
                }
                IsCheckDebtTransaction = true;
                var listData = Controller.BookingSearch();
                if (listData.Any())
                {
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Tìm thấy {listData.Count} bản ghi giao dịch hiện tại");
                    var listVi = Controller.WalletByAccount(listData.Select(x => x.AccountId).ToList());
                    foreach (var item in listData)
                    {
                        var vi = listVi.FirstOrDefault(x => x.AccountId == item.AccountId);
                        if (vi == null)
                        {
                            continue;
                        }
                        //Xử lý logic nếu tài khoản khách hàng còn < 5000 khi kết thúc giao dịch thì sẽ cảnh bao
                        // Khi khách hàng tài khoản <= 0 cảnh báo
                        var soDuVi = (vi.Balance + vi.SubBalance) - item.TotalPrice;
                        if (soDuVi < AppSettings.MinMoney && soDuVi >= 0)
                        {
                            if (!flags.Any(x => x.Transaction == item.TransactionCode && x.Type == 1))
                            {
                                if (!Controller.FlagCheck($"{item.TransactionCode}", 1))
                                {
                                    Controller.FlagAdd($"{item.TransactionCode}", 1);
                                    flags.Add(new Flag { Transaction = item.TransactionCode, Type = 1 });
                                    Controller.NotificationJobAdd(item.AccountId, "TNGO thông báo", "Tài khoản của bạn gần hết điểm, vui lòng nạp thêm điểm để tránh khỏi nợ cước chuến đi, trân trọng");
                                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Gửi cảnh báo gần hết điểm cho tài khoản {item.AccountId} chuyến đi {item.TransactionCode}");
                                }
                            }
                        }
                        else if (soDuVi < 0)
                        {
                            if (!flags.Any(x => x.Transaction == item.TransactionCode && x.Type == 2))
                            {
                                if (!Controller.FlagCheck($"{item.TransactionCode}", 2))
                                {
                                    Controller.FlagAdd($"{item.TransactionCode}", 2);
                                    flags.Add(new Flag { Transaction = item.TransactionCode, Type = 2 });
                                    Controller.NotificationJobAdd(item.AccountId, "TNGO thông báo", "Chuyến đi của bạn đang sử dụng vượt quá số dư trong ví, vui lòng nạp thêm điểm để tránh khỏi nợ cước, trân trọng");
                                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Gửi cảnh báo gần hết điểm cho tài khoản {item.AccountId} chuyến đi {item.TransactionCode}");
                                }
                            }
                        }
                    }
                    Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Kết thúc");
                }
                IsCheckDebtTransaction = false;
            }
            catch (Exception ex)
            {
                IsCheckDebtTransaction = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [#auto Tự động thông báo cho khách khi pin < 10% mà đang khóa, cứ mỗi 1 tiếng tb 1 lần]
        public static void CheckLowBatteryByAccount()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.CheckCheckLowBatteryByAccount";
            try
            {
                if (IsCheckLowBatteryByAccount)
                {
                    return;
                }
                IsCheckLowBatteryByAccount = true;
                var listData = Controller.LowBatteryByAccount();
                if (listData.Any())
                {
                    foreach (var item in listData)
                    {
                        Controller.NotificationJobAdd(item.AccountId, "TNGO thông báo", "TNGO thấy bạn đang tạm khóa xe nhưng pin còn yếu. Hãy sớm mở lại khóa để không ảnh hưởng tới chuyến đi");
                        Controller.FlagUpdateTimeOrInsert(item.FlagTransaction);
                    }
                }
                IsCheckLowBatteryByAccount = false;
            }
            catch (Exception ex)
            {
                IsCheckLowBatteryByAccount = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [Tự động tính toán bảng xếp hạng]
        public static void AutoChartsTop()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.AutoChartsTop";
            try
            {
                if (IsAutoChartsTop)
                {
                    return;
                }
                IsAutoChartsTop = true;
                var dateTimeNow = DateTime.Now;
                // Chạy lại tuần cũ
                if(dateTimeNow.Hour == 0 && dateTimeNow.Minute <= 15)
                {
                    dateTimeNow = dateTimeNow.AddMinutes(-16);
                }

                var thisWeek = Functions.GetIso8601WeekOfYear(dateTimeNow.Date);

                var code = $"W_{thisWeek.WeekNumber:D2}_{thisWeek.Year}";

                Controller.AccountRatingAutoAdd(new AccountRating
                {
                    Group = EAccountRatingGroup.KM,
                    ProjectId = 0,
                    Code = code,
                    CreatedDate = dateTimeNow,
                    CreatedUser = 0,
                    Name = $"Bảng xếp hàng tuần {thisWeek.WeekNumber:D2}/{thisWeek.Year:D4}",
                    Reward = "Đang cập nhật",
                    Type = EAccountRatingType.Week,
                    StartDate = thisWeek.StartDate,
                    EndDate = thisWeek.EndDate
                });

                var newTop = Controller.ChartsNews(thisWeek.StartDate, thisWeek.EndDate);
                if(newTop.Any())
                {
                    int order = 1;
                    foreach (var item in newTop)
                    {
                        item.Code = code;
                        item.UpdatedDate = dateTimeNow;
                        item.Order = order;
                        item.Value = Math.Round(item.Value / 1000, 3);
                        order++;
                    }
                    Controller.UpdateTop(newTop, code);
                }    
                IsAutoChartsTop = false;
            }
            catch (Exception ex)
            {
                IsAutoChartsTop = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [ẩn]
        #region [13. Nhắc kiểm tra tồn kho]
        private static void AutoCheckInverter()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsAutoCheckInverter)
                {
                    return;
                }
                IsAutoCheckInverter = true;
                var config = Controller.EmailManageGet();
                var users = Controller.ApplicationUsers();
                var warehouses = Controller.WarehousesJoin();
                // danh sách user liên quan
                string styletd = "border: 1px solid silver; padding: 1px 10px;";
                // Send email với điều phối viên
                foreach (var warehouse in warehouses)
                {
                    string sendEmail = "";
                    if (warehouse.Type == EWarehouseType.Virtual)
                    {
                        var ktUser = users.FirstOrDefault(x => x.Id == warehouse.EmployeeId);
                        if (ktUser == null)
                        {
                            continue;
                        }
                        sendEmail = ktUser.Email;
                    }
                    else
                    {
                        // Gửi cho thủ kho, sếp VTS, kế toán VTS
                        var listU = users.Where(x => x.RoleType == RoleManagerType.CSKH_VTS);
                        sendEmail = string.Join(",", listU.Select(x => x.Email).ToList());
                    }

                    var str = new StringBuilder();
                    if (!warehouse.Suppliess.Any())
                    {
                        str.Append($"Kho không còn vật tư nào");
                    }
                    else
                    {
                        str.Append($"<table style=\"border-collapse: collapse;\">");
                        str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Mã vật tư</td><td style=\"{styletd}\">Tên vật tư</td><td style=\"{styletd}\">Số lượng tồn</td></tr>");
                        int stt = 1;
                        foreach (var item in warehouse.Suppliess)
                        {
                            str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{item.Code}</td><td style=\"{styletd}\">{item.Name}</td><td style=\"{styletd}\">{item.Quantity}</td></tr>");
                            stt++;
                        }
                        str.Append("</table>");
                    }
                    Controller.EmailBoxAdd(config.From, sendEmail, DateTime.Now, $"[TNGO-thông báo tồn kho ngày {DateTime.Now:dd-MM-yyyy}] số lượng vật tư trong kho \"{warehouse.Name}\"", str.ToString(), 0);
                }
                IsAutoCheckInverter = false;
            }
            catch (Exception ex)
            {
                IsAutoCheckInverter = false;
                Functions.AddLog(0, "SyncProcess.AutoCheckInverter", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [11. Nhắc đến hạn bảo dưỡng]
        private static void AutoCheckMaintenanceSchedule()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsAutoCheckMaintenanceSchedule)
                {
                    return;
                }
                IsAutoCheckMaintenanceSchedule = true;
                var config = Controller.EmailManageGet();
                var users = Controller.ApplicationUsers();
                var bikeInfo = Controller.BikesInfo();
                var bikes = Controller.Bikes();
                var stations = Controller.Stations();

                foreach (var item in bikes)
                {
                    item.Station = stations.FirstOrDefault(x => x.Id == item.StationId);
                    var lastMaintenance = new DateTime();
                    var ktInfo = bikeInfo.FirstOrDefault(x => x.BikeId == item.Id);
                    if (item.StartDate == null)
                    {
                        continue;
                    }
                    lastMaintenance = ktInfo == null ? item.StartDate.Value : (ktInfo.LastMaintenanceTime == null ? DateTime.Now : ktInfo.LastMaintenanceTime.Value);
                    if ((DateTime.Now - lastMaintenance).TotalDays >= 90 || ktInfo.TotalMinutes >= 10800)
                    {
                        item.IsMaintenance = true;
                        item.TotalMinutes = ktInfo?.TotalMinutes ?? 0;
                        item.LastMaintenanceTime = lastMaintenance;
                    }
                }

                // Gửi email cho tài khoản nvđp quản lý trạm đó và những xe ko có trạm
                var listDP = users.Where(x => x.RoleType == RoleManagerType.Coordinator_VTS).ToList();
                var listBike = bikes.Where(x => x.IsMaintenance).ToList();
                string styletd = "border: 1px solid silver; padding: 1px 10px;";
                foreach (var item in listDP)
                {
                    var bikeW = listBike.Where(x => (x.Station == null || x.Station.ManagerUserId == item.Id)).ToList();
                    if (!bikeW.Any())
                    {
                        continue;
                    }
                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Xe</td><td style=\"{styletd}\">Tên trạm</td><td style=\"{styletd}\">Tên trạm khác</td><td style=\"{styletd}\">TG bảo dưỡng gần nhất</td><td style=\"{styletd}\">Số phút tích lũy từ bảo dưỡng gần nhất</td></tr>");
                    int stt = 1;
                    foreach (var bike in bikeW)
                    {
                        str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{bike.Plate}</td><td style=\"{styletd}\">{bike.Station?.Name}</td><td style=\"{styletd}\">{bike.Station?.DisplayName}</td><td style=\"{styletd}\">{bike.LastMaintenanceTime:dd/MM/yyyy}</td><td style=\"{styletd}\">{bike.TotalMinutes}</td></tr>");
                        stt++;
                    }
                    str.Append("</table>");
                    Controller.EmailBoxAdd(config.From, item.Email, DateTime.Now, $"[TNGO-thông báo xe đến hạn bảo dưỡng ngày {DateTime.Now:dd-MM-yyyy}] yêu cầu kiểm tra và bảo dưỡng xe", str.ToString(), 0);
                }
                // Gửi cho nhân viên CSKH & VNKD
                var nvCSKH = users.Where(x => x.RoleType == RoleManagerType.CSKH_VTS).ToList();
                if (listBike.Any())
                {
                    var str = new StringBuilder();
                    str.Append($"<table style=\"border-collapse: collapse;\">");
                    str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Xe</td><td style=\"{styletd}\">Tên trạm</td><td style=\"{styletd}\">Tên trạm khác</td><td style=\"{styletd}\">TG bảo dưỡng gần nhất</td><td style=\"{styletd}\">Số phút tích lũy từ bảo dưỡng gần nhất</td></tr>");
                    int stt = 1;
                    foreach (var bike in listBike)
                    {
                        str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{bike.Plate}</td><td style=\"{styletd}\">{bike.Station?.Name}</td><td style=\"{styletd}\">{bike.Station?.DisplayName}</td><td style=\"{styletd}\">{bike.LastMaintenanceTime:dd/MM/yyyy}</td><td style=\"{styletd}\">{bike.TotalMinutes}</td></tr>");
                        stt++;
                    }
                    str.Append("</table>");
                    Controller.EmailBoxAdd(config.From, string.Join(",", nvCSKH.Select(x => x.Email).ToList()), DateTime.Now, $"[TNGO-thông báo xe đến hạn bảo dưỡng ngày {DateTime.Now:dd-MM-yyyy}] yêu cầu kiểm tra và bảo dưỡng xe", str.ToString(), 0);
                }
                IsAutoCheckMaintenanceSchedule = false;
            }
            catch (Exception ex)
            {
                IsAutoCheckMaintenanceSchedule = false;
                Functions.AddLog(0, "SyncProcess.AutoCheckMaintenanceSchedule", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region Gửi email tính doanh thu định kỳ theo thời gian
        private static void DashboardInfo()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsDashboardInfo)
                {
                    return;
                }
                var config = Controller.EmailManageGet();
                var arrayTime = AppSettings.DashboardInfoSendTime.Split(',').Select(x => int.Parse(x)).ToList();
                if (arrayTime.Any(x => x == DateTime.Now.Hour))
                {
                    var data = Controller.DashboardInfo(DateTime.Now.Date, DateTime.Now.AddDays(1).Date);
                    if (data != null)
                    {
                        string styletd = "border: 1px solid silver; padding: 1px 10px;";
                        var str = new StringBuilder();
                        str.Append($"<table style=\"border-collapse: collapse;\">");
                        str.Append($"<tr><td colspan='2' style=\"{styletd}\">Thông tin vào ra</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">Tiền nạp</td><td style=\"{styletd}\">{Functions.FormatMoney(data.TienNap)}</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">Điểm thuê xe</td><td style=\"{styletd}\">{Functions.FormatMoney(data.DiemThue)}</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">Điểm nợ cước</td><td style=\"{styletd}\">{Functions.FormatMoney(data.DiemNoCuoc)}</td></tr>");
                        str.Append($"<tr><td colspan='2' style=\"{styletd}\">Nguồn nạp</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">MOMO</td><td style=\"{styletd}\">{Functions.FormatMoney(data.NapMomo)}</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">ZALOPAY</td><td style=\"{styletd}\">{Functions.FormatMoney(data.NapZalo)}</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">VTCPAY</td><td style=\"{styletd}\">{Functions.FormatMoney(data.NapVTCPAY)}</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">Thực tiếp</td><td style=\"{styletd}\">{Functions.FormatMoney(data.NapTrucTiep)}</td></tr>");
                        str.Append($"<tr><td colspan='2' style=\"{styletd}\">Thuê xe</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">Đang thuê</td><td style=\"{styletd}\">{Functions.FormatMoney(data.ChuyenDiDangThue)}</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">Hoàn thành</td><td style=\"{styletd}\">{Functions.FormatMoney(data.ChuyenDiHoanThanh)}</td></tr>");
                        str.Append($"<tr><td style=\"{styletd}\">Nợ cước</td><td style=\"{styletd}\">{Functions.FormatMoney(data.ChuyenDiNoCuoc)}</td></tr>");
                        str.Append($"<tr><td colspan='2' style=\"{styletd}\">Thuê xe</td></tr>");
                        str.Append("</table>");
                        Controller.EmailBoxAdd(config?.From, AppSettings.DashboardInfoSendSendEmails, DateTime.Now, $"[TNGO {(int)EEventSystemType.M_1001_XeMatKetNoiBE} {DateTime.Now:dd-MM-yyyy}] Có", str.ToString(), 0);
                    }
                }
                IsDashboardInfo = true;
            }
            catch (Exception ex)
            {
                IsDashboardInfo = false;
                Functions.AddLog(0, "SyncProcess.DashboardInfo", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }

        }
        #endregion
        #endregion

        #region [11. Clear Data sub]
        private static void ClearData()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            try
            {
                if (IsClearnData)
                {
                    return;
                }
                IsClearnData = true;
                if (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 4)
                {
                    Functions.AddLog(0, "SyncProcess.ClearData", requestId, EnumMongoLogType.End, "Bắt đầu clear", true);
                    Controller.ClearData();
                }
                IsClearnData = false;
            }
            catch (Exception ex)
            {
                IsClearnData = false;
                Functions.AddLog(0, "SyncProcess.ClearData", requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [X. Lấy ra những tài khoản đi xe ngày hôm trước]
        private async static Task ProcessDiscountByDay()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.ProcessDiscountByDay";
            try
            {
                if (IsProcessDiscountByDay)
                {
                    return;
                }
                IsProcessDiscountByDay = true;
                var checkTime = DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 6;
               // var checkTime = true;
                if (checkTime)
                {
                    var dateR = DateTime.Now.Date.AddDays(-1);
                    var processData = Controller.ProcessDiscountByDayGetUsers(dateR);
                    foreach (var item in processData)
                    {
                        using (var httpClient = new HttpClient())
                        {
                            httpClient.DefaultRequestHeaders.Accept.Clear();
                            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var stringContent = new StringContent("{}", Encoding.UTF8, "application/json");
                            var result = await httpClient.PostAsync($"{AppSettings.DiscountTransactionPath}/{item}/{dateR:yyyy-MM-dd}", stringContent);
                            string outPut = "";
                            try
                            {
                                outPut = await result.Content.ReadAsStringAsync();
                            }
                            catch
                            { }
                            Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Check discount transaction #{item} #{dateR:yyyy-MM-dd} {result.StatusCode} => {outPut}");
                        }

                    }
                }    
                IsProcessDiscountByDay = false;
            }
            catch (Exception ex)
            {
                IsProcessDiscountByDay = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion

        #region [X. 7h + điểm refun cho khách hàng]
        private async static Task AddPointDiscountByDay()
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffff}";
            var functionName = "SyncProcess.AddPointDiscountByDay";
            try
            {
                if (IsAddPointDiscountByDay)
                {
                    return;
                }
                IsAddPointDiscountByDay = true;
                var checkTime = DateTime.Now.Hour >= 6 && DateTime.Now.Hour < 8;
                //var checkTime = true;
                if (checkTime)
                {
                    var dateR = DateTime.Now.Date.AddDays(-1);
                    using (var httpClient = new HttpClient())
                    {
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{AppSettings.APIToken}");
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var stringContent = new StringContent("{}", Encoding.UTF8, "application/json");
                        var result = await httpClient.PostAsync($"{AppSettings.AddDiscountTransactionPath}/{dateR:yyyy-MM-dd}", stringContent);
                        string outPut = "";
                        try
                        {
                            outPut = await result.Content.ReadAsStringAsync();
                        }
                        catch
                        { }
                        Functions.AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Check add discount transaction #{dateR:yyyy-MM-dd} {result.StatusCode} => {outPut}");
                    }
                }
                IsAddPointDiscountByDay = false;
            }
            catch (Exception ex)
            {
                IsAddPointDiscountByDay = false;
                Functions.AddLog(0, functionName, requestId, EnumMongoLogType.End, ex.ToString(), true);
            }
        }
        #endregion
    }
}