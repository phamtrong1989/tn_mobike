﻿using FirebaseAdmin;
using FirebaseAdmin.Auth;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using log4net;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Threading.Tasks;
using TN.Domain.Model;
namespace TN.Mobike.Controls
{
    public class WeekYearModel
    {
        public int WeekNumber { get; set; }
        public int Year { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public static class Functions
    {
        public static WeekYearModel GetIso8601WeekOfYear(DateTime time)
        {
            var modelObj = new WeekYearModel();
            int year = time.Year;
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            var day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            modelObj.StartDate = time.AddDays(-((day == DayOfWeek.Sunday ? 7 : (int)day) - 1));
            modelObj.Year = modelObj.StartDate.Year;
            modelObj.EndDate = modelObj.StartDate.AddDays(6);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            // Return the week of our adjusted day
            var weekNumber = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            modelObj.WeekNumber = weekNumber;
            return modelObj;
        }

        public static string ToJson(this object obj)
        {
            try
            {
                var serializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
                return JsonConvert.SerializeObject(obj, serializerSettings);
            }
            catch
            {
                return "[]";
            }
        }   

        public static string FormatMoney(decimal v)
        {
            return string.Format("{0:00,0}", v);
        }
        
        public static Tuple<bool, string> SendEmailAsync(EmailManage email, string to, string addEmail, string ccEmail, string bcEmail, string Subject, string Body)
        {
            try
            {
                var fromAddress = new MailAddress(email.Email);
                var toAddress = new MailAddress(to);
                var fromPassword = email.Password;
                var subject = Subject;
                var body = Body;

                var smtp = new SmtpClient
                {
                    Host = email.Host,
                    Port = email.Port,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                })
                {
                    if (!string.IsNullOrEmpty(addEmail))
                    {
                        message.To.Add(addEmail);
                    }
                    if (!string.IsNullOrEmpty(ccEmail))
                    {
                        message.CC.Add(ccEmail);
                    }
                    if (!string.IsNullOrEmpty(bcEmail))
                    {
                        message.Bcc.Add(bcEmail);
                    }
                    smtp.Send(message);
                    return new Tuple<bool, string>(true, null);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.ToString());
            }
        }

        public static int TotalMilutes(DateTime startTime, DateTime endTime)
        {
            return Convert.ToInt32(Math.Ceiling((endTime - startTime).TotalSeconds / 60));
        }

        public static decimal ToTotalPrice(
            ETicketType ticketType,
            DateTime startTime,
            DateTime endTime,
            decimal ticketPrice_TicketValue,
            int ticketPrice_LimitMinutes,
            int ticketPrice_BlockPerMinute,
            int ticketPrice_BlockPerViolation,
            decimal ticketPrice_BlockViolationValue,
            DateTime prepaid_EndTime,
            int prepaid_MinutesSpent
            )
        {
            var totalMinutes = TotalMilutes(startTime, endTime);
            int soPhutTinhPhuPhi;
            if (ticketType == ETicketType.Block)
            {
                soPhutTinhPhuPhi = totalMinutes - ticketPrice_BlockPerMinute;
                if (soPhutTinhPhuPhi <= 0)
                {
                    return ticketPrice_TicketValue;
                }
                return ticketPrice_TicketValue + ((soPhutTinhPhuPhi % ticketPrice_BlockPerViolation > 0) ? ((soPhutTinhPhuPhi / ticketPrice_BlockPerViolation) + 1) : (soPhutTinhPhuPhi / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;
            }
            else if (ticketType == ETicketType.Day || ticketType == ETicketType.Month)
            {
                var tongSoPhutConLai = ticketPrice_LimitMinutes - prepaid_MinutesSpent;
                // trường hợp đi quá hạn vé trả trước
                if (prepaid_EndTime > endTime)
                {
                    soPhutTinhPhuPhi = (totalMinutes < tongSoPhutConLai) ? 0 : totalMinutes - ticketPrice_LimitMinutes;
                }
                // trường hợp đi hết hạn vé trả trước
                else
                {
                    // Tính theo end time là mốc hết hạn vé trả trước
                    // thời hạn vé trả trước - thời gian bắt đầu = số phút đi được theo hết mốc
                    // Tổng số phút còn lại > số thời gian theo hết mốc
                    double tongSoPhutHetMoc = (prepaid_EndTime - startTime).TotalMinutes;
                    if (tongSoPhutConLai > tongSoPhutHetMoc)
                    {
                        soPhutTinhPhuPhi = Convert.ToInt32(Math.Ceiling(totalMinutes - tongSoPhutHetMoc));
                    }    
                    else
                    {
                        soPhutTinhPhuPhi = totalMinutes - tongSoPhutConLai;
                    }
                   // soPhutTinhPhuPhi = Convert.ToInt32(Math.Ceiling((tongSoPhutConLai > (prepaid_EndTime - startTime).TotalMinutes) ? (totalMinutes - (prepaid_EndTime - startTime).TotalMinutes) : (totalMinutes - tongSoPhutConLai)));
                }

                if (soPhutTinhPhuPhi <= 0)
                {
                    return 0;
                }
                return ((soPhutTinhPhuPhi % ticketPrice_BlockPerViolation > 0) ? ((soPhutTinhPhuPhi / ticketPrice_BlockPerViolation) + 1) : (soPhutTinhPhuPhi / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;
            }
            return 0;
        }

        public static void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isExeption = false)
        {
            try
            {
                var date = DateTime.Now;
                Task.Run(() => MongoAddAsync(new MongoLog
                {
                    AccountId = accountId,
                    FunctionName = functionName,
                    LogKey = logKey,
                    CreateTime = $"{date:yyyy-MM-dd HH:mm:ss}",
                    Content = content,
                    CreateTimeTicks = date.Ticks,
                    DeviceKey = "",
                    IsExeption = isExeption,
                    SystemType = EnumSystemType.ControlService,
                    Type = type,
                    Date = Convert.ToInt64($"{date:yyyyMMdd}")
                })).ConfigureAwait(false);
            }
            catch { }
        }

        public static  bool MongoCreateIndexAsync(bool isDrop)
        {
            try
            {
                var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
                // MongoLog
                string dbName = SyncService.MongoDBSettings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<MongoLog>(SyncService.MongoDBSettings.MongoCollectionLog);
                if(isDrop)
                {
                    try
                    {
                        collection.Indexes.DropOne("Date_-1_SystemType_-1");
                    }
                    catch {}
                }
                collection.Indexes.CreateOne(new CreateIndexModel<MongoLog>(Builders<MongoLog>.IndexKeys.Descending(hamster => hamster.Date).Descending(x => x.SystemType)));
                // MongoGPS
                string dbNameGPS = SyncService.MongoDBSettings.MongoDataBase.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbGPS = dbClient.GetDatabase(dbNameGPS);
                var collectionGPS = dbGPS.GetCollection<GPSData>(SyncService.MongoDBSettings.MongoCollection);
                if (isDrop)
                {
                    try
                    {
                        collectionGPS.Indexes.DropOne("Date_-1_IMEI_1");
                    }
                    catch { }
                }    
                collectionGPS.Indexes.CreateOne(new CreateIndexModel<GPSData>(Builders<GPSData>.IndexKeys.Descending(hamster => hamster.Date).Ascending(x => x.IMEI)));
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static double DistanceInMeter(double lat1, double lon1, double lat2, double lon2)
        {
            try
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(Deg2rad(lat1)) * Math.Sin(Deg2rad(lat2)) + Math.Cos(Deg2rad(lat1)) * Math.Cos(Deg2rad(lat2)) * Math.Cos(Deg2rad(theta));
                dist = Math.Acos(dist);
                dist = Rad2deg(dist);
                dist = dist * 60 * 1.1515;
                // meter
                dist = dist * 1.609344 * 1000;
                if (Double.IsNaN(dist))
                {
                    return 0;
                }
                return Math.Round(dist, 1);
            }
            catch
            {
                return 99999;
            }
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double Deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double Rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        public static List<GPSData> GPSGetByIMEITop(string imei, int top = 30)
        {
            try
            {
                string dbName = SyncService.MongoDBSettings.MongoDataBase.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                string query = "{ IMEI : \"" + imei + "\", Date : " + DateTime.Now.ToString("yyyyMMdd") + ", Lat : { $gte: 1 }}";
                return db.GetCollection<GPSData>("GPS").Find(query).SortByDescending(x=>x.CreateTimeTicks).Limit(top).ToList();
            }
            catch
            {
                return new List<GPSData>();
            }
        }

        public static List<GPSData> GPSGetByIMEI(string imei, DateTime startTime, DateTime endTime)
        {
            try
            {
                string dbName = SyncService.MongoDBSettings.MongoDataBase.Replace("{TimeDB}", $"{startTime:yyyyMM}");
                var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                string query = "{ IMEI : \"" + imei + "\", Date : { $gte: " + startTime.ToString("yyyyMMdd") + ", $lte: " + endTime.ToString("yyyyMMdd") + " }, CreateTimeTicks: { $gte: " + startTime.Ticks.ToString() + ", $lte: " + endTime.Ticks.ToString() + " }, Lat: { $gte: 1} }";
                var _context = db.GetCollection<GPSData>("GPS").Find(query).ToList();
                _context = _context.Where(x => x.Lat > 0 && x.Long > 0).OrderBy(x => x.CreateTimeTicks).ToList();
                return _context;
            }
            catch
            {
                return new List<GPSData>();
            }
        }

        private static async Task MongoAddAsync(MongoLog data)
        {
            try
            {
                string dbName = SyncService.MongoDBSettings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<MongoLog>(SyncService.MongoDBSettings.MongoCollectionLog);
                await collection.InsertOneAsync(data);
            }
            catch { }
        }

        public static void GPSDataAddAsync(TransactionGPS data)
        {
            try
            {
                string dbName = "TNGO-Data";
                var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<TransactionGPS>("TransactionGPS");
                collection.InsertOne(data);
            }
            catch { }
        }

        public static MongoDBSettings InitMongoDBSettings()
        {
            var path = $"{Environment.CurrentDirectory}/Configs/MongoDB.Settings.json";
            if(!File.Exists(path))
            {
                return null;
            }
            return JsonConvert.DeserializeObject<MongoDBSettings>(File.ReadAllText(path));
        }

        public static bool InitSettingNotification()
        {
            try
            {
                if (FirebaseMessaging.DefaultInstance == null)
                {
                    var path = $"{Environment.CurrentDirectory}/Configs/Notification.Settings.json";
                    var defaultApp = FirebaseApp.Create(new AppOptions()
                    {
                        Credential = GoogleCredential.FromFile(path),
                    });
                    var defaultAuth = FirebaseAuth.GetAuth(defaultApp);
                    defaultAuth = FirebaseAuth.DefaultInstance;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public class Utilities
    {
        private static readonly ILog log = LogManager.GetLogger("TN.Mobike.Controls");
        public static bool ActiveOperationLog = true;
        public static bool ActiveDebugLog = true;
        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return (long)(datetime - sTime).TotalSeconds;
        }

        public static void StartLog(bool activeOperationLog, bool activeDebugLog)
        {
            ActiveOperationLog = activeOperationLog;
            ActiveDebugLog = activeDebugLog;
        }

        public static void CloseLog()
        {
            foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
            {
                app.Close();
            }
        }

        public static void WriteErrorLog(string logtype, string logcontent)
        {
            try
            {
                log.Error($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        public static void WriteOperationLog(string logtype, string logcontent)
        {
            if (!ActiveOperationLog)
                return;

            try
            {
                log.Info($"{logtype} \t {logcontent}");
            }
            catch 
            {
                // ignored
            }
        }

        public static void WriteDebugLog(string logtype, string logcontent)
        {
            if (!ActiveDebugLog)
                return;

            try
            {
                log.Debug($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                try
                {
                    if (stream != null)
                        stream.Close();
                }
                catch
                {

                }
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string GetBitStr(byte[] data)
        {
            BitArray bits = new BitArray(data);

            string strByte = string.Empty;
            for (int i = 0; i <= bits.Count - 1; i++)
            {
                if (i % 8 == 0)
                {
                    strByte += " ";
                }
                strByte += (bits[i] ? "1" : "0");
            }

            return strByte;
        }

        public static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
    }

    public static class StaticExtensions
    {
        public static string GetDisplayName(this Enum value)
        {
            try
            {
                Type enumType = value.GetType();
                var enumValue = Enum.GetName(enumType, value);
                MemberInfo member = enumType.GetMember(enumValue)[0];

                var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
                var outString = ((DisplayAttribute)attrs[0]).Name;

                if (((DisplayAttribute)attrs[0]).ResourceType != null)
                {
                    outString = ((DisplayAttribute)attrs[0]).GetName();
                }
                return outString;
            }
            catch
            {
                return value.ToString();
            }

        }
    }
}
