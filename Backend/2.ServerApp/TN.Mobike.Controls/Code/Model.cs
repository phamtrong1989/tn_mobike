﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static TN.Mobike.Controls.Bike;

namespace TN.Mobike.Controls
{
    public enum EDataInfoType
    {
        SumAccountLive = 0
    }

    public class DataInfo
    {
        public int Id { get; set; }
        public EDataInfoType Type { get; set; }
        [MaxLength(4000)]
        public string Data { get; set; }
        public DateTime UpdatedTime { get; set; }
    }

    public class StationCountModel
    {
        public int StationId { get; set; }
        public int Count { get; set; }
        public Station Station { get;  set; }
    }

    public enum EEventSystemType : int
    {
        //A
        [Display(Name = "Xe mất kết nối BE")]
        M_1001_XeMatKetNoiBE = 1001,
        [Display(Name = "Xe đổ")]
        M_1002_XeDo = 1002,
        [Display(Name = "Xe không nằm trong trạm")]
        M_1003_XeKhongLamTrongTram = 1003,
        [Display(Name = "Xe hết PIN/sắp hết pin")]
        M_1004_XeHetPinSapHetPin = 1004,
        [Display(Name = "Xe mất GPS")]
        M_1005_XeMatGPS = 1005,
        [Display(Name = "Xe đang mở khóa mà không trong chuyến đi nào")]
        M_1006_XeMoNhungKhongTrongChuyenDiNao = 1006,
        [Display(Name = "Xe di chuyển bất hợp pháp")]
        M_1007_DiChuyenBatHopPhap = 1007,
        //B
        [Display(Name = "Thừa xe/thiếu xe")]
        M_2001_ThuaXeThieuXe = 2001,

        [Display(Name = "Khách hàng mở xe không thành công")]
        M_3001_KhachHangMoXeKhongThanhCong = 3001,
        [Display(Name = "Có giao dịch mới")]
        M_3002_CoGiaoDichMoi = 3002,
        [Display(Name = "Kết thúc giao dịch thành công")]
        M_3003_KetThucGiaoDichThanhCong = 3003,
        [Display(Name = "Hủy chuyến")]
        M_3004_HuyChuyen = 3004,
        [Display(Name = "Kết thúc giao dịch cưỡng bức")]
        M_3005_KetThucGiaoDichCuongBuc = 3005,
        [Display(Name = "Yêu cầu thu hồi xe")]
        M_3006_YeuCauThuHoiXe = 3006,
        [Display(Name = "Khách hàng cố kết thúc giao dịch không thành công")]
        M_3007_KhachHangCoKetThucGiaoDichKhongThanhCong = 3007,
        [Display(Name = "Khách Sắp hết điểm khi đang đi")]
        M_3008_KhachSapHetTienKhiDangDi = 3008,
        [Display(Name = "Cước chuyến đi vượt quá số dư")]
        M_3009_CuocChuyenVuotQuaSoDu = 3009,
        [Display(Name = "Khách hàng đi quá xa phạm vi")]
        M_3010_KhachHangDiQuaXaPhamVi = 3010,
        [Display(Name = "Khách hàng báo hỏng xe")]
        M_3011_KhachBaoHongXe = 3011,
        [Display(Name = "Khách hàng báo lỗi trả xe")]
        M_3012_KhachBaoLoiTraXe = 3012,
        [Display(Name = "Khách hàng dừng xe quá lâu")]
        M_3013_KhachHangDungXeQuaLau = 3013,
        [Display(Name = "Nghi vấn khóa mở nhưng không ghi nhận giao dịch")]
        M_3014_NghiVanMatGiaoDich = 3014,
        [Display(Name = "Khách quên trả xe")]
        M_3015_KhachQuenTraXe = 3015,
        //D
        [Display(Name = "Có yêu cầu xác minh thông tin khác hàng")]
        M_4001_CoYeuCauXacMinHThongTinKhachHang = 4001,
        [Display(Name = "Phản hồi chuyến đi")]
        M_4002_PhanHoiChuyenDi = 4002,
        [Display(Name = "Chuyến đi nợ cước")]
        M_4003_ChuyenDiNoCuoc = 4003,
        [Display(Name = "Có tài khoản mới tạo")]
        M_4004_CoTaiKhoanMoiTao = 4004,
        [Display(Name = "Có tài khoản nạp tiền")]
        M_4005_CoTaiKhoanNapTien = 4005
    }

    public enum EEventSystemGroup : byte
    {
        CanhBaoXeKhoa = 1,
        CanhBaoTram = 2,
        CanhBaoGiaoDich = 3,
        CanhBaoCSKH = 4
    }

    public enum EEventSystemWarningType : byte
    {
        Info,
        Warning,
        Danger
    }

    

    public class SMSBox
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public long NotificationDataId { get; set; }
        public string Content { get; set; }
        public bool IsSend { get; set; }
        public int ReSend { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? SuccessfulSendDate { get; set; }
    }

    public class EmailManage
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string Host { get; set; }
        public string From { get; set; }
        public string CustomerDates { get; set; }
        public string CustomerTime { get; set; }
        public bool CustomerStatus { get; set; }
        public string EmployeeTime { get; set; }
        public bool EmployeeStatus { get; set; }
        public string AddEmail { get; set; }
        public string CCEmail { get; set; }
        public string BCEmail { get; set; }
    }

    public class Flag
    {
        public long Id { get; set; }
        public string Transaction { get; set; }
        public int Type { get; set; }
        public bool IsProcess { get; set; }
        public DateTime UpdatedTime { get; set; }
    }

    public class NotificationData
    {

        public long Id { get; set; }
        public int AccountId { get; set; }

        public string DeviceId { get; set; }

        public string Icon { get; set; }

        public string TransactionCode { get; set; }

        public int Type { get; set; }

        [MaxLength(200)]
        public string Tile { get; set; }

        [MaxLength(500)]
        public string Message { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    public enum EBookingStatus
    {
        [Display(Name = "Bắt đầu chuyến đi")]
        Start = 1,

        [Display(Name = "Kết thúc chuyến đi")]
        End = 2,

        [Display(Name = "Hủy chuyến")]
        Cancel = 3,

        [Display(Name = "Hủy chuyến bởi quản trị")]
        CancelByAdmin = 4,

        [Display(Name = "Kết thúc chuyến bởi quản trị")]
        EndByAdmin = 5,

        [Display(Name = "Nợ cước")]
        Debt = 6,
    }

    public enum ETicketType : byte
    {
        [Display(Name = "Vé block")]
        Block = 1,
        [Display(Name = "Vé ngày")]
        Day = 2,
        [Display(Name = "Vé tháng")]
        Month = 3
    }

    public class Booking
    {
        public int Id { get; set; }
        public string TicketPrepaidCode { get; set; }
        public int TicketPrepaidId { get; set; }
        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int StationIn { get; set; }

        public int? StationOut { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public byte Vote { get; set; }

        [MaxLength(1000)]
        public string Feedback { get; set; }

        public int TicketPriceId { get; set; }
        public ETicketType TicketPrice_TicketType { get; set; }
        [DataType("money")]
        public decimal TicketPrice_TicketValue { get; set; }
        // Cho phép trừ vào tài khoản phụ ko
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        // Là bảng giá mặc định
        public bool TicketPrice_IsDefault { get; set; }
        // Quá bao phút thì x tiền
        public int TicketPrice_BlockPerMinute { get; set; }
        // Số tiền x lên quá
        public int TicketPrice_BlockPerViolation { get; set; }
        public decimal TicketPrice_BlockViolationValue { get; set; }
        //Nếu là vé ngày: 1 ngày tối đi đa 12 tiếng = 720 phút,  Nếu vé tháng: Đi thoải mái các ngày trong tháng.Mỗi chuyến đi không quá 2 tiếng = 120 phút, Nếu vé quý: Tương tự tháng
        public int TicketPrice_LimitMinutes { get; set; }
        // Điểm trừ vào tài khoản chính
        public decimal ChargeInAccount { get; set; }
        // Điểm trừ vào tài khoản khuyến mãi
        public decimal ChargeInSubAccount { get; set; }
        // Total price
        public decimal TotalPrice { get; set; }
        // Thời gian giao dịch
        public DateTime? DateOfPayment { get; set; }
        public int TotalMinutes { get; set; }
        public DateTime CreatedDate { get; set; }
        public double? KCal { get; set; }
        public double? KG { get; set; }
        public double? KM { get; set; }
        public EBookingStatus Status { get; set; }
        public string Note { get; set; }
        public decimal TripPoint { get; set; }
        // Với trường hợp là vé trả trước dụng chung trường nhưng ý nghĩa khác nhau
        // Tổng số phút đã sử dụng trong ngày (vé ngày), chưa tính giao dịch hiện tại
        // Tổng số phút tối đa sử dụng 1 lần giao dịch (vé tháng)
        public int Prepaid_MinutesSpent { get; set; }
        // Ngày hiệu lực bắt đầu
        public DateTime? Prepaid_StartDate { get; set; }
        // Ngày hết hiệu lực của vé trả trước
        public DateTime? Prepaid_EndDate { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_StartTime { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_EndTime { get; set; }
        public decimal EstimateChargeInAccount { get; set; }
        public decimal EstimateChargeInSubAccount { get; set; }

        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Plate { get; set; }
        public string FlagTransaction { get; set; }

        public bool IsForgotrReturnTheBike { get; set; } = false;
    }

    public class Wallet
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public decimal Balance { get; set; }
        public decimal SubBalance { get; set; }
        public decimal TripPoint { get; set; }
        public string HashCode { get; set; }
        public byte Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }

    public class NotificationJob
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string DeviceId { get; set; }
        public string Icon { get; set; }
        public string Tile { get; set; }
        public string Message { get; set; }
        public DateTime PublicDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int LanguageId { get; set; }
        public bool IsSend { get; set; }
        public DateTime? SendDate { get; set; }
        public byte DataDockEventType { get; set; }
        public string DataTime { get; set; }
        public string DataContent { get; set; }
    }

    public class AccountDevice 
    {
        public int Id { get; set; }
        public int? AcountId { get; set; }
        public string Token { get; set; }
        public string DeviceId { get; set; }
        public string IP { get; set; }
        public string CloudMessagingToken { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class WalletTransaction
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public int WalletId { get; set; }

        public int CampaignId { get; set; }

        public int Type { get; set; }

        public int TransactionId { get; set; }

        [MaxLength(50)]
        public string OrderIdRef { get; set; }

        [MaxLength(50)]
        public string TransactionCode { get; set; }

        public string HashCode { get; set; }

        [DataType("money")]
        public decimal Amount { get; set; }

        [DataType("money")]
        public decimal SubAmount { get; set; }

        [DataType("money")]
        public decimal? DebtAmount { get; set; }

        // Số point được + thêm theo sự kiện
        [DataType("money")]
        public decimal DepositEvent_SubAmount { get; set; }

        [DataType("money")]
        public decimal TotalAmount { get; set; }

        [DataType("money")]
        public decimal TripPoint { get; set; }

        public bool IsAsync { get; set; } = false;

        public byte Status { get; set; }

        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public byte PaymentGroup { get; set; }

        public DateTime? SetPenddingTime { get; set; }

        public DateTime? PadTime { get; set; }

        public string ReqestId { get; set; }

        [MaxLength(1000)]
        public string AdminNote { get; set; }

        public string WarningHistoryData { get; set; }

        public bool IsWarningHistory { get; set; }

        //Giá trị ví hiện tại trước khi giao dịch này +
        [DataType("money")]
        public decimal Wallet_Balance { get; set; }

        [DataType("money")]
        public decimal Wallet_SubBalance { get; set; }

        [DataType("money")]
        public string Wallet_HashCode { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public string Files { get; set; }

        public int GiftFromAccountId { get; set; }
        public int GiftToAccountId { get; set; }
    }

    public enum EnumMongoLogType
    {
        Start = 0,
        Center,
        End
    }

    public enum EnumSystemType
    {
        APIAPP = 0,
        APIBackend,
        DockService,
        ControlService
    }

    public class TransactionGPS
    {
        public int TransactionId { get; set; }
        public int Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string TransactionCode { get; set; }
        public string Data { get; set; }
    }

    public class GPSDataMini
    {
        public double Lat { get; set; }
        public double Long { get; set; }
        public long CreateTimeTicks { get; set; }
    }

    public class GPSData
    {
        public ObjectId _id { get; set; }
        public string IMEI { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public string CreateTime { get; set; }
        public long CreateTimeTicks { get; set; }
        public long Date { get; set; }
    }

    public class MongoLog
    {
        public ObjectId _id { get; set; }
        public EnumMongoLogType Type { get; set; }
        public string Content { get; set; }
        public int AccountId { get; set; }
        public string LogKey { get; set; }
        public string DeviceKey { get; set; }
        public string FunctionName { get; set; }
        public string CreateTime { get; set; }
        public EnumSystemType SystemType { get; set; }
        public long CreateTimeTicks { get; set; }
        public bool IsExeption { get; set; }
        public long Date { get; set; }
    }
    public class EventSystemMongo
    {
        public EventSystemMongo()
        {
            int type = (int)Type;
            if (type >= 1000 && type < 2000)
            {
                Group = EEventSystemGroup.CanhBaoXeKhoa;
            }
            else if (type >= 2000 && type < 3000)
            {
                Group = EEventSystemGroup.CanhBaoTram;
            }
            else if (type >= 3000 && type < 4000)
            {
                Group = EEventSystemGroup.CanhBaoGiaoDich;
            }
            else if (type >= 4000 && type < 5000)
            {
                Group = EEventSystemGroup.CanhBaoCSKH;
            }
        }

        [BsonId]
        public ObjectId _id { get; set; }

        public long Id { get; set; }
        public int ProjectId { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public long ParentId { get; set; } = 0;

        public int UserId { get; set; }

        public int AccountId { get; set; }

        public int StationId { get; set; }

        public string TransactionCode { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public string Content { get; set; }

        public string Datas { get; set; }

        public bool IsMultiple { get; set; }

        public bool IsFlag { get; set; } = false;

        public bool IsProcessed { get; set; } = false;

        public EEventSystemType Type { get; set; }

        public EEventSystemGroup Group { get; set; }

        public EEventSystemWarningType WarningType { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public RoleManagerType RoleType { get; set; } = RoleManagerType.Default;
        public bool IsJob { get; set; } = false;
        public int Date { get; set; }
        public long CreatedDateTicks { get; set; }
        public long UpdatedDateTicks { get; set; }
    }

    public class Bike
    {
        public enum EBikeStatus
        {
            Inactive = 0,
            Active = 1,
            Warehouse = 2,
            ErrorWarehouse = 3
        }

        public enum EBikeType
        {
            XeDap = 0,
            XeDapDien = 1,
            XeLai = 2
        }

        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int InvestorId { get; set; }
        public int ModelId { get; set; }
        public int ProducerId { get; set; }
        public int ColorId { get; set; }
        public int WarehouseId { get; set; }
        public string Images { get; set; }
        public string Plate { get; set; }
        public string SerialNumber { get; set; }
        public EBikeStatus Status { get; set; }
        public EBikeType Type { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
        public int StationId { get; set; }
        public int DockId { get; set; }
        public string Note { get; set; }
        public DateTime? ProductionDate { get; set; }
        public DateTime? StartDate { get; set; }
        public bool IsTrouble { get; set; } = false;
        public int MinuteRequiredMaintenance { get; set; } = 4800;
        public BikeInfo BikeInfo { get; set; }
        public int? BookingId { get; set; }
        public bool IsMaintenance { get; set; } = false;
        public DateTime? LastMaintenanceTime { get; set; }
        public int TotalMinutes { get; set; }
        public Station Station { get; set; }
    }

    public class BikeInfo 
    {
        public int Id { get; set; }
        public int BikeId { get; set; }
        // Tổng số phút đi lũy kế
        public int TotalMinutes { get; set; } = 0;
        // Thời gian bảo dưỡng gần nhất
        public DateTime? LastMaintenanceTime { get; set; }
        //Tổng số giờ đã sử dụng cần bảo dưỡng
        public double HoursForMaintenance { get; set; }
        // Thời gian bảo dưỡng tiếp theo
        public DateTime? NextMaintenanceTime { get; set; }
        // Mô tả lỗi trước khi sửa chữa
        public string ErrorDescriptions { get; set; }
        // File đính kèm trước khi sửa chữa
        public string ErrorFiles { get; set; }
        public string SuppliesIds { get; set; }
        public string SuppliesNames { get; set; }
        // Mô tả lỗi trước khi sửa chữa
        public string RepairDescriptions { get; set; }
        // File đính kèm trước khi sửa chữa
        public string RepairFiles { get; set; }
        //Ngày sửa chữa
        public DateTime? RepairTime { get; set; }
        // Last repairId
        public int MaintenanceRepairId { get; set; }
        //
        public int MaintenanceRepairItemId { get; set; }
    }

    public class TotalMinutesModel
    {
        public int BikeId { get; set; }
        public int Total { get; set; }
    }

    public class EmailBox
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Add { get; set; }
        public string Cc { get; set; }
        public string Bc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachments { get; set; }
        public bool IsSend { get; set; }
        public bool NotSend { get; set; } = false;
        public byte Type { get; set; }
        public int ObjectId { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? SuccessfulSendDate { get; set; }
    }

    public class BikeLowBatteryModel
    {
        public int BikeId { get; set; }
        public int ProjectId { get; set; }
        public string Plate { get; set; }
        public string SerialNumber { get; set; }
        public string StationName1 { get; set; }
        public string StationName2 { get; set; }
        public string Address { get; set; }
        public double Battery { get; set; }
        public string PhoneNumber { get; set; }
        public int ManagerUserId { get; set; }
    }

    public enum EWarehouseType : byte
    {
        [Display(Name = "Kho tổng VTS")]
        Defaut,
        [Display(Name = "Kho điều phối bảo dưỡng VTS")]
        Virtual
    }

    public class Warehouse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public EWarehouseType Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int EmployeeId { get; set; }
        public List<Supplies> Suppliess { get; set; }
    }

    public enum RoleManagerType
    {
        Default = -1,
        [Display(Name = "IT trí Nam")]
        Admin = 0,

        [Display(Name = "Kế toán Trí Nam")]
        Accounting_TN = 1,

        [Display(Name = "Hội đồng quản trị VTS")]
        BOD_VTS = 2,

        [Display(Name = "Kế toán trưởng VTS")]
        KTT_VTS = 3,

        [Display(Name = "Chăm sóc khách hàng & kinh doanh VTS")]
        CSKH_VTS = 4,

        [Display(Name = "Cộng tác viên VTS")]
        CTV_VTS = 5,

        [Display(Name = "Điều phối bảo dưỡng VTS")]
        Coordinator_VTS = 6,

        [Display(Name = "Nhân viên cung ứng Trí Nam")]
        NVCU_TN = 9,

        [Display(Name = "Thủ kho VTS")]
        ThuKho_VTS = 10
    }

    public class ApplicationUser
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public string DisplayName { get; set; }
        public string FullName { get; set; }
        public bool IsLock { get; set; }
        public string NoteLock { get; set; }
        public bool IsReLogin { get; set; }
        public bool IsSuperAdmin { get; set; } = false;
        public string Note { get; set; }
        public int? CreatedUserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedUserId { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public DateTime? Birthday { get; set; }

        public byte Sex { get; set; }

        public int ManagerUserId { get; set; }

        public int RoleId { get; set; }

        public RoleManagerType RoleType { get; set; } = RoleManagerType.Default;

        // Nghỉ việc
        public bool IsRetired { get; set; }
        // Làm việc từ ngày
        public DateTime? WorkingDateFrom { get; set; }
        // Làm việc đến hết ngày
        public DateTime? WorkingDateTo { get; set; }
    }

    public enum ESuppliesType : byte
    {
        [Display(Name = "Vật tư mới")]
        New = 0,
        [Display(Name = "Vật tư cũ")]
        Old = 1
    }

    public enum ESuppliesStatus
    {
        Enabled,
        Disabled
    }

    public class Supplies
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public string Name { get; set; }

        public string Descriptions { get; set; }

        public int Quantity { get; set; }

        public string Unit { get; set; }

        public ESuppliesType Type { get; set; }

        public decimal Price { get; set; }

        public ESuppliesStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }
    }

    public class SuppliesWarehouse
    {
        public int Id { get; set; }
        public int SuppliesId { get; set; }
        public int WarehouseId { get; set; }
        public int Quantity { get; set; }
    }

    public class BikeHoursOperationModel
    {
        public int BikeId { get; set; }
        public double Total { get; set; }
    }

    public class Station
    {
        public int Id { get; set; }
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int Width { get; set; }
        public int Spaces { get; set; }
        public int Height { get; set; }

        [MaxLength(1000)]
        public string Address { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(200)]
        public string DisplayName { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public int TotalDock { get; set; } = 0;
        public byte Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int ManagerUserId { get; set; }
        public double Distance { get; set; }
        public double ReturnDistance { get; set; }
    }

    public class StationTotalBikeModel
    {
        public int ProjectId { get; set; }
        public int StationId { get; set; }
        public int TotalBike { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Address { get; set; }
        public int Spaces { get; set; }
        public int ManagerUserId { get; set; }
        public string Note { get; set; }
    }

    public class TransactionsDebtChargesModel
    {
        public string TransactionCode { get; set; }
        public int AccountId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int ChargeDebt { get; set; }
        public int ManagerUserId { get; set; }
        public int SubManagerUserId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int ProjectId { get; set; }
    }

    public enum EBikeReportType
    {
        [Display(Name = "Lỗi khác")]
        Khac = 0,

        [Display(Name = "Lỗi mở khóa")]
        MoKhoa = 1,

        [Display(Name = "Lỗi QR code")]
        MaQR = 2,

        [Display(Name = "Xe bị phá hoại")]
        BiPhaHoai = 3,

        [Display(Name = "Trạm không xe")]
        TramTrongXe = 4,

        [Display(Name = "Khóa hỏng")]
        KhoaHong = 5,

        [Display(Name = "Không nạp được điểm")]
        KhongNapDuocDiem = 6,

        [Display(Name = "Không nhận được điểm")]
        KhongNhanDuocDiemKhiNap = 7
    }

    public class BikeDisconectModel
    {
        public int Id { get; set; }
        public string Plate { get; set; }
        public int StationId { get; set; }
        public string IMEI { get; set; }
        public string SerialNumber { get; set; }
        public DateTime LastConnectionTime { get; set; }
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        public string Email { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public string Path { get; set; }
        public int BookingId { get; set; }
        public int ManagerUserId { get; set; }
        public string StationName1 { get; set; }
        public string StationName2 { get; set; }
        public string Address { get; set; }
    }

    public class UserModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public string Code { get; set; }
    }

    public class DockOpenNotInBookingModel
    {
        public int BikeId { get; set; }
        public int ProjectId { get; set; }
        public string Plate { get; set; }
        public string SerialNumber { get; set; }
        public string StationName1 { get; set; }
        public string StationName2 { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int ManagerUserId { get; set; }
    }

    public class BikeWarningModel
    {
        public int Id { get; set; }
        public string IMEI { get; set; }
        public string Transaction { get; set; }
        public EBikeWarningType WarningType { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public bool Status { get; set; }
        public string Note { get; set; }
        public DateTime UpdatedDate { get; set; }

        public int BikeId { get; set; }
        public int ManagerUserId { get; set; }
        public string StationName1 { get; set; }
        public string StationName2 { get; set; }
        public string Address { get; set; }
        public string Plate { get; set; }
        public int ProjectId { get; set; }
    }

    public class BikeReportModel
    {
        public int Id { get; set; }
        public string TransactionCode { get; set; }
        public int BikeId { get; set; }
        public string Plate { get; set; }
        public int AccountId { get; set; }
        public DateTime CreatedDate { get; set; }
        public EBikeReportType Type { get; set; }
        public int ManagerUserId { get; set; }
        public string StationName1 { get; set; }
        public string StationName2 { get; set; }
        public string Address { get; set; }
        public int ProjectId { get; set; }

        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Code { get; set; }
        public string Email { get; set; }
    }

    public enum EBikeWarningType
    {
        DoXe = 0,
        DiChuyenBatHopPhap = 1,
        XeDiQuaXa = 2,
        XeKhongTrongTram = 3,
        XeLoiGPS = 4
    }

    public class BikeWarning
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string IMEI { get; set; }
        public string Transaction { get; set; }
        public EBikeWarningType WarningType { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public bool Status { get; set; }
        [MaxLength(200)]
        public string Note { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public enum EAccountVerifyStatus
    {
        [Display(Name = "Chưa thực hiện")]
        Not = 0,
        [Display(Name = "Gửi chờ duyệt")]
        Waiting = 1,
        [Display(Name = "Xác nhận thông tin")]
        Ok = 2,
        [Display(Name = "Trả lại yêu cầu hoàn thiện")]
        Refuse = 3
    }

    public class AccountVerifyStatusModel
    {
        public EAccountVerifyStatus VerifyStatus { get; set; }
        public int Count { get; set; }
    }

    public enum EBookingFailStatus : byte
    {
        Default = 0,
        ViewAdmin = 1,
        AddBooking = 2,
        Cancel = 3
    }

    public class BookingFail
    {
        public int Id { get; set; }

        public string TicketPrepaidCode { get; set; }

        public int TicketPrepaidId { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int StationIn { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public int TicketPriceId { get; set; }
        public ETicketType TicketPrice_TicketType { get; set; }
        public decimal TicketPrice_TicketValue { get; set; }
        // Cho phép trừ vào tài khoản phụ ko
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        // Là bảng giá mặc định
        public bool TicketPrice_IsDefault { get; set; }
        // Quá bao phút thì x tiền
        public int TicketPrice_BlockPerMinute { get; set; }
        // Số tiền x lên quá
        public int TicketPrice_BlockPerViolation { get; set; }
        public decimal TicketPrice_BlockViolationValue { get; set; }
        //Nếu là vé ngày: 1 ngày tối đi đa 12 tiếng = 720 phút,  Nếu vé tháng: Đi thoải mái các ngày trong tháng.Mỗi chuyến đi không quá 2 tiếng = 120 phút, Nếu vé quý: Tương tự tháng
        public int TicketPrice_LimitMinutes { get; set; }
        // Điểm trừ vào tài khoản chính
        public decimal ChargeInAccount { get; set; }
        // Điểm trừ vào tài khoản khuyến mãi
        public decimal ChargeInSubAccount { get; set; }
        // Total price
        public decimal TotalPrice { get; set; }
        // Thời gian giao dịch
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int UpdatedUser { get; set; }

        [MaxLength(500)]
        public string Note { get; set; }

        // Với trường hợp là vé trả trước dụng chung trường nhưng ý nghĩa khác nhau
        // Tổng số phút đã sử dụng trong ngày (vé ngày), chưa tính giao dịch hiện tại
        // Tổng số phút tối đa sử dụng 1 lần giao dịch (vé tháng)
        public int Prepaid_MinutesSpent { get; set; }
        // Ngày hiệu lực bắt đầu
        public DateTime? Prepaid_StartDate { get; set; }
        // Ngày hết hiệu lực của vé trả trước
        public DateTime? Prepaid_EndDate { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_StartTime { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_EndTime { get; set; }

        public EBookingFailStatus Status { get; set; }
    }

    public class Dock
    {
        private double battery;

        public int Id { get; set; }

        public int InvestorId { get; set; }

        public int StationId { get; set; }

        public int ProjectId { get; set; }

        public int ModelId { get; set; }

        public int WarehouseId { get; set; }

        public int ProducerId { get; set; }

        [MaxLength(50)]
        public string IMEI { get; set; }

        [MaxLength(50)]
        public string SerialNumber { get; set; }

        [MaxLength(20)]
        public string SIM { get; set; }

        public EBikeStatus Status { get; set; }

        public bool LockStatus { get; set; }

        public double Battery { get => Math.Round(battery, 1); set => battery = value; }

        public bool Charging { get; set; }

        public bool ConnectionStatus { get; set; }

        public DateTime? LastConnectionTime { get; set; }
        public DateTime? LastGPSTime { get; set; }

        public double? Lat { get; set; }

        public double? Long { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public string Note { get; set; }
    }

    public enum ESex : byte
    {
        [Display(Name = "Khác")]
        Khac = 2,
        [Display(Name = "Nam")]
        Nam = 1,
        [Display(Name = "Nữ")]
        Nu = 0,
    }

    public enum EAccountType : byte
    {
        [Display(Name = "App mobile")]
        Normal = 0,
        [Display(Name = "RFID")]
        RFID = 1,
        [Display(Name = "App mobile và RFID")]
        NormalAndRFID = 2
    }

    public enum EAccountStatus : byte
    {
        [Display(Name = "Khóa")]
        Lock = 0,
        [Display(Name = "Kích hoạt")]
        Active = 1
    }

    public enum EAccountCreateType : byte
    {
        Customer = 0,
        Admin = 1
    }

    public class Account
    {
        public int Id { get; set; }

        public EAccountCreateType CreateType { get; set; }

        [MaxLength(30)]
        public string Username { get; set; }

        [MaxLength(50)]
        public string RFID { get; set; }

        [MaxLength(200)]
        public string Password { get; set; }

        [MaxLength(200)]
        public string PasswordSalt { get; set; }

        [MaxLength(50)]
        public string Etag { get; set; }

        [MaxLength(500)]
        public string Avatar { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        public ESex Sex { get; set; }

        [MaxLength(30)]
        public string FirstName { get; set; }

        [MaxLength(30)]
        public string LastName { get; set; }

        [MaxLength(200)]
        public string FullName { get; set; }

        public DateTime? Birthday { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(200)]
        public string Address { get; set; }

        [MaxLength(2000)]
        public string MobileToken { get; set; }

        public EAccountStatus Status { get; set; }

        [MaxLength(1000)]
        public string Note { get; set; }


        [MaxLength(1000)]
        public string NoteLock { get; set; }

        public EAccountType Type { get; set; }

        public int LanguageId { get; set; }

        [MaxLength(10)]
        public string Language { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        public string ShareCode { get; set; }

        public EAccountIdentificationType IdentificationType { get; set; } = EAccountIdentificationType.CMTND;

        [MaxLength(50)]
        public string IdentificationID { get; set; }

        [MaxLength(500)]
        public string IdentificationPhotoFront { get; set; }

        [MaxLength(500)]
        public string IdentificationPhotoBackside { get; set; }

        public EAccountVerifyStatus VerifyStatus { get; set; }

        [MaxLength(1000)]
        public string VerifyNote { get; set; }

        public DateTime? VerifyDate { get; set; }

        public int VerifyUserId { get; set; }

        public DateTime? CreatedSystemDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int CreatedUserId { get; set; }

        public int CreatedSystemUserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public DateTime? UpdatedSystemDate { get; set; }

        public int UpdatedUserId { get; set; }

        public int UpdatedSystemUserId { get; set; }

        public int SubManagerUserId { get; set; }

        public int ManagerUserId { get; set; }

        public bool IsLock { get; set; }
        public bool IsReLogin { get; set; }
        public int? NumberWrongPasswords { get; set; }
        public DateTime? ExpirationWrongPassword { get; set; }
    }
    public enum EAccountIdentificationType
    {
        CMTND = 0,
        CCCD = 1,
        Passport = 2
    }

    public class Transaction
    {
        public int Id { get; set; }

        public string TicketPrepaidCode { get; set; }

        public int TicketPrepaidId { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int? StationIn { get; set; }

        public int? StationOut { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public byte Vote { get; set; }

        [MaxLength(1000)]
        public string Feedback { get; set; }

        public int TicketPriceId { get; set; }

        public ETicketType TicketPrice_TicketType { get; set; }

        public decimal TicketPrice_TicketValue { get; set; }
        // Cho phép trừ vào tài khoản phụ ko
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        // Là bảng giá mặc định
        public bool TicketPrice_IsDefault { get; set; }
        // Quá bao phút thì x tiền
        public int TicketPrice_BlockPerMinute { get; set; }
        // Số tiền x lên quá
        public int TicketPrice_BlockPerViolation { get; set; }

        public decimal TicketPrice_BlockViolationValue { get; set; }
        //Nếu là vé ngày: 1 ngày tối đi đa 12 tiếng = 720 phút,  Nếu vé tháng: Đi thoải mái các ngày trong tháng.Mỗi chuyến đi không quá 2 tiếng = 120 phút, Nếu vé quý: Tương tự tháng
        public int TicketPrice_LimitMinutes { get; set; }
        // Điểm trừ vào tài khoản chính
        public decimal ChargeInAccount { get; set; }
        // Điểm trừ vào tài khoản khuyến mãi
        public decimal ChargeInSubAccount { get; set; }

        public decimal? ChargeDebt { get; set; }

        // Total price
        public decimal TotalPrice { get; set; }
        // Thời gian giao dịch
        public DateTime? DateOfPayment { get; set; }
        public int TotalMinutes { get; set; }
        public DateTime CreatedDate { get; set; }
        public double? KCal { get; set; }
        public double? KG { get; set; }
        public double? KM { get; set; }
        public EBookingStatus Status { get; set; }

        [MaxLength(500)]
        public string Note { get; set; }

        public decimal TripPoint { get; set; }
        // Với trường hợp là vé trả trước dụng chung trường nhưng ý nghĩa khác nhau
        // Tổng số phút đã sử dụng trong ngày (vé ngày), chưa tính giao dịch hiện tại
        // Tổng số phút tối đa sử dụng 1 lần giao dịch (vé tháng)
        public int Prepaid_MinutesSpent { get; set; }
        // Ngày hiệu lực bắt đầu
        public DateTime? Prepaid_StartDate { get; set; }
        // Ngày hết hiệu lực của vé trả trước
        public DateTime? Prepaid_EndDate { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_StartTime { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_EndTime { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public decimal EstimateChargeInAccount { get; set; }

        public decimal EstimateChargeInSubAccount { get; set; }

        public EVehicleRecallStatus VehicleRecallStatus { get; set; } = EVehicleRecallStatus.Default;

        public decimal VehicleRecallPrice { get; set; } = 0;

        public double ReCallM { get; set; } = 0;

        public bool IsDockNotOpen { get; set; } = false;
        public bool IsForgotrReturnTheBike { get; set; } = false;
    }

    public enum EVehicleRecallStatus : byte
    {
        Default = 0,
        Unfulfilled = 1,
        Processed = 2
    }

    public class DashboardInfoModel
    {
        public decimal TienNap { get; set; }
        public decimal DiemThue { get; set; }
        public decimal DiemNoCuoc { get; set; }
        public decimal NapMomo { get; set; }
        public decimal NapZalo { get; set; }
        public decimal NapVTCPAY { get; set; }
        public decimal NapTrucTiep { get; set; }
        public int ChuyenDiDangThue { get; set; }
        public int ChuyenDiHoanThanh { get; set; }
        public int ChuyenDiNoCuoc { get; set; }
        public int KHDangKy { get; set; }
        public int KHChuaXacThuc { get; set; }
        public int KHChoDuyet { get; set; }
        public int KHDaXacThuc { get; set; }
        public int XeSuDung { get; set; }
        public int XeSanSangTaiTram { get; set; }
        public int XeLuuKho { get; set; }
        public int LoiVaBaoDuong { get; set; }
        public int XeDangKetNoi { get; set; }
        public int XeMatKetNoi { get; set; }
        public int XeSapHet { get; set; }
    }

    public class AccountRatingItem
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        public int ProjectId { get; set; }

        public int Order { get; set; }

        public int AccountId { get; set; }

        public decimal Value { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool Status { get; set; }
    }

    public enum EAccountRatingType : byte
    {
        Week = 0,
        Month = 1
    }

    public enum EAccountRatingGroup : byte
    {
        KM = 0
    }

    public class AccountRating
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }

        [MaxLength(2000)]
        public string Reward { get; set; }

        public EAccountRatingType Type { get; set; }

        public EAccountRatingGroup Group { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
    }
}