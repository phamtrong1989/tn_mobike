﻿using System;
using System.Configuration;
using System.Globalization;

namespace TN.Mobike.Controls
{
    class AppSettings
    {
        public static string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        public static string ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        public static string ServiceDisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"];
        public static string ServiceDescription = ConfigurationManager.AppSettings["ServiceDescription"];
        public static string RecheckOrderZALOPath = ConfigurationManager.AppSettings["RecheckOrderZALOPath"];
        public static string RecheckOrderMOMOPath = ConfigurationManager.AppSettings["RecheckOrderMOMOPath"];
        public static string APIToken = ConfigurationManager.AppSettings["APIToken"];
        public static string ClearSubPointPath = ConfigurationManager.AppSettings["ClearSubPointPath"];
        public static decimal MinMoney = Convert.ToDecimal(ConfigurationManager.AppSettings["MinMoney"]);
        public static string ClearSubPointFistPath = ConfigurationManager.AppSettings["ClearSubPointFistPath"];
        public static string HubPath = ConfigurationManager.AppSettings["HubPath"];
        public static string GetAccountOnlinePath = ConfigurationManager.AppSettings["GetAccountOnlinePath"];
        public static string RecheckOrderVTCPath = ConfigurationManager.AppSettings["RecheckOrderVTCPath"];
        public static string DashboardInfoSendTime = ConfigurationManager.AppSettings["DashboardInfoSendTime"];
        public static string DashboardInfoSendSendEmails = ConfigurationManager.AppSettings["DashboardInfoSendSendEmails"];

        public static string DiscountTransactionPath = ConfigurationManager.AppSettings["DiscountTransactionPath"];
        public static string AddDiscountTransactionPath = ConfigurationManager.AppSettings["AddDiscountTransactionPath"];

        public static string RecheckOrderPayooPath = ConfigurationManager.AppSettings["RecheckOrderPayooPath"];

        public static string SendNotificationByAccountPath = ConfigurationManager.AppSettings["SendNotificationByAccountPath"];
        public static string SendNotificationByTopicAllPath = ConfigurationManager.AppSettings["SendNotificationByTopicAllPath"];

    }
}
