﻿using System;
using TN.Domain.Model;

namespace TN.Mobike.Controls
{
    public class SyncService
    {
        public static MongoDBSettings MongoDBSettings;
        public  void Start()
        {
            try
            {
                Utilities.WriteDebugLog("ProgramInit_Start", "Service Start");
                MongoDBSettings = Functions.InitMongoDBSettings();
                Functions.InitSettingNotification();
                SyncProcess.Start();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Start", ex.ToString());
            }
            
        }
        public  void Stop()
        {
            try
            {
                SyncProcess.Stop();
                Utilities.WriteDebugLog("ProgramInit_Stop", "Service Stop");
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Stop", ex.ToString());
            }
        }
    }
}
