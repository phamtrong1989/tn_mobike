﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Mobike.LockServices.Job;
using TN.Mobike.LockServices.Model.Data;
using TN.Mobike.LockServices.Settings;
using Topshelf;

namespace TN.Mobike.LockServices
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.OutputEncoding = Encoding.UTF8;
                TopshelfExitCode exitCode = HostFactory.Run(x =>
                {
                    x.Service<SyncServices>(s =>
                    {
                        s.ConstructUsing(name => new SyncServices());
                        s.WhenStarted(cb => cb.Start());
                        s.WhenStopped(cb => cb.Stop());
                        s.WhenShutdown(cb => cb.Stop());
                    });
                    x.RunAsLocalSystem();

                    x.SetServiceName(AppSettings.ServiceName);
                    x.SetDisplayName(AppSettings.ServiceDisplayName);
                    x.SetDescription(AppSettings.ServiceDescription);

                });
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("Program_Main", ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
