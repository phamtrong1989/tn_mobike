﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Connections;
using TN.Mobike.LockServices.Settings;
using Microsoft.AspNetCore.SignalR.Client;

namespace TN.Mobike.LockServices.Core
{
    class SignalRHub
    {
        public static HubConnection _connection;
        public static string _hubStringConnection = AppSettings.SignalRHub;

        public static void InitHub()
        {
            _connection = new HubConnectionBuilder()
                .WithUrl(_hubStringConnection, HttpTransportType.WebSockets | HttpTransportType.LongPolling)
                .WithAutomaticReconnect()
                //.ConfigureLogging(logging =>
                //{
                //    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                //    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                //})
                //.ConfigureLogging(logging =>
                //{
                //    // Log to the Console
                //    logging.AddConsole();

                //    // This will set ALL logging to Debug level
                //    logging.SetMinimumLevel(LogLevel.Information);
                //    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                //    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                //})
                .Build();
            _connection.HandshakeTimeout = new TimeSpan(0, 1, 0);
            _connection.ServerTimeout = new TimeSpan(0, 1, 0);

            if (AppSettings.ModeRun)
            {
                Console.WriteLine($"INITIAL SUCCESSFUL SIGNAL HUB : {_hubStringConnection}");
            }
        }

        public static void DisconnectHub()
        {
            _connection?.StopAsync();
        }
    }
}
