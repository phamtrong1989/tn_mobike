﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TN.Mobike.LockServices.Settings;

namespace TN.Mobike.LockServices.Core
{

    class Packet
    {
        public System.Net.Sockets.Socket CurrentSocket;
        public int ClientNumber;
        public byte[] DataBuffer = new byte[1024];
        //public byte[] DataBuffer = new byte[4096];

        /// <summary>
        /// Construct a Packet Object
        /// </summary>
        /// <param name="sock">The socket this Packet is being used on.</param>
        /// <param name="client">The client number that this packet is from.</param>
        public Packet(System.Net.Sockets.Socket sock, int client)
        {
            CurrentSocket = sock;
            ClientNumber = client;
        }
    }

    class Server
    {
        /// <summary>
        /// A delegate type called when a client initially connects to the server.  Void return type.
        /// </summary>
        /// <param name="clientNumber">A unique identifier of the client that has connected to the server.</param>
        public delegate void ClientConnectCallback(int clientNumber);

        /// <summary>
        /// A delegate type called when a client disconnects from the server.  Void return type.
        /// </summary>
        /// <param name="clientNumber">A unique identifier of the client that has disconnected from the server.</param>
        public delegate void ClientDisconnectCallback(int clientNumber);

        /// <summary>
        /// A delegate type called when the server receives data from a client.
        /// </summary>
        /// <param name="clientNumber">A unique identifier of the client that has disconnected from the server.</param>
        /// <param name="message">A byte array representing the message sent.</param>
        /// <param name="messageSize">The size in bytes of the message.</param>
        public delegate void ReceiveDataCallback(int clientNumber, byte[] message, int messageSize);

        private ClientConnectCallback _clientConnect = null;
        private ClientDisconnectCallback _clientDisconnect = null;
        private ReceiveDataCallback _receive = null;

        private System.Net.Sockets.Socket _mainSocket;
        private System.Threading.Timer _broadcastTimer;
        private int _currentClientNumber = 0;

        public class UserSock
        {
            public int iClientID { get; }

            public System.Net.Sockets.Socket UserSocket { get; }

            public DateTime dTimer { get; set; }

            public string szClientName { get; set; }

            public string szIMEI { get; set; }

            public string szStationName { get; set; }

            public UInt16 UserListentingPort { get; set; }

            public string szAlternateIP { get; set; }

            public PingStatsClass PingStatClass { get; set; }

            public int ZeroDataCount { get; internal set; }

            public UserSock(int nClientID, System.Net.Sockets.Socket s)
            {
                iClientID = nClientID;
                UserSocket = s;
                dTimer = DateTime.Now; //Initialize the ping timer to the current time
                szStationName = string.Empty;
                szClientName = string.Empty;
                szIMEI = string.Empty;
                UserListentingPort = 9998; //default
                szAlternateIP = string.Empty;
                PingStatClass = new PingStatsClass();
            }

            public UserSock()
            {

            }

            public override string ToString()
            {
                return JsonConvert.SerializeObject(this);
            }
        }

        // public Dictionary<int, Socket > workerSockets = new Dictionary<int, Socket>();
        public Dictionary<int, UserSock> workerSockets = new Dictionary<int, UserSock>();


        /// <summary>
        /// Modify the callback function used when a client initially connects to the server.
        /// </summary>
        public ClientConnectCallback OnClientConnect
        {
            get { return _clientConnect; }

            set { _clientConnect = value; }
        }

        /// <summary>
        /// Modify the callback function used when a client disconnects from the server.
        /// </summary>
        public ClientDisconnectCallback OnClientDisconnect
        {
            get { return _clientDisconnect; }

            set { _clientDisconnect = value; }
        }

        /// <summary>
        /// Modify the callback function used when the server receives a message from a client.
        /// </summary>
        public ReceiveDataCallback OnReceiveData
        {
            get { return _receive; }

            set { _receive = value; }
        }

        /// <summary>
        /// Whether or not the server is currently listening for new client connections.
        /// </summary>
        public bool IsListening
        {
            get
            {
                if (_mainSocket == null)
                    return false;
                else
                    return _mainSocket.IsBound;
            }
        }

        /// <summary>
        /// Make the server listen for client connections on a specific port.
        /// </summary>
        /// <param name="listenPort">The number of the port to listen for connections on.</param>
        public void Listen(int listenPort)
        {
            try
            {
                Stop();

                _mainSocket = new System.Net.Sockets.Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _mainSocket.Bind(new IPEndPoint(IPAddress.Any, listenPort));
                _mainSocket.Listen(100);
                _mainSocket.BeginAccept(new AsyncCallback(OnReceiveConnection), null);
            }

            catch (SocketException se)
            {
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine(se.Message);
                }
            }
        }

        /// <summary>
        /// Stop listening for new connections and close all currently open connections.
        /// </summary>
        public void Stop()
        {
            lock (workerSockets)
            {
                foreach (UserSock s in workerSockets.Values)
                {
                    if (s.UserSocket.Connected)
                        s.UserSocket.Close();
                }

                workerSockets.Clear();
            }

            if (IsListening)
                _mainSocket.Close();
        }

        /// <summary>
        /// Send a message to all connected clients.
        /// </summary>
        /// <param name="message">A byte array representing the message to send.</param>
        public void SendMessage(byte[] message, bool testConnections = false)
        {
            if (testConnections)
            {
                List<int> ClientsToRemove = new List<int>();
                foreach (int clientId in workerSockets.Keys)
                {
                    if (workerSockets[clientId].UserSocket.Connected)
                    {
                        try
                        {
                            workerSockets[clientId].UserSocket.Send(message);
                        }
                        catch
                        {
                            ClientsToRemove.Add(clientId);
                        }

                        Thread.Sleep(10); // this is for a client Ping so stagger the send messages
                    }
                    else
                        ClientsToRemove.Add(clientId);
                }

                //lock (workerSockets)//Already locked from the caller
                {
                    if (ClientsToRemove.Count > 0)
                    {
                        foreach (int cID in ClientsToRemove)
                        {
                            //Socket gets closed and removed from OnClientDisconnect
                            if (OnClientDisconnect != null)
                            {
                                OnClientDisconnect(cID);
                            }
                        }
                    }
                }
                ClientsToRemove.Clear();
                ClientsToRemove = null;
            }
            else
            {
                foreach (UserSock s in workerSockets.Values)
                {
                    try
                    {
                        if (s.UserSocket.Connected)
                            s.UserSocket.Send(message);
                    }
                    catch (SocketException se)
                    {
                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine(se.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Send a message to a specific client.
        /// </summary>
        /// <param name="clientNumber">A unique identifier of the client that has connected to the server.</param>
        /// <param name="message">A byte array representing the message to send.</param>
        public void SendMessage(int clientNumber, byte[] message)
        {
            if (!workerSockets.ContainsKey(clientNumber))
            {
                //throw new ArgumentException("Invalid Client Number", "clientNumber");
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine("Invalid Client Number");
                }
                return;
            }

            try
            {
                //workerSockets[clientNumber].Send(message);
                ((UserSock)workerSockets[clientNumber]).UserSocket.Send(message);
            }
            catch (SocketException se)
            {
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine(se.Message);
                }
            }
        }

        /// <summary>
        /// Begin broadcasting a message over UDP every several seconds.
        /// </summary>
        /// <param name="message">A byte array representing the message to send.</param>
        /// <param name="port">The port over which to send the message.</param>
        /// <param name="frequency">Frequency to send the message in seconds.</param>
        public void BeginBroadcast(byte[] message, int port, int frequency)
        {
            System.Net.Sockets.Socket sock = new System.Net.Sockets.Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sock.EnableBroadcast = true;

            Packet pack = new Packet(sock, port);
            pack.DataBuffer = message;

            if (_broadcastTimer != null)
                _broadcastTimer.Dispose();

            _broadcastTimer =
                new System.Threading.Timer(new TimerCallback(BroadcastTimerCallback), pack, 0, frequency * 1000);
        }

        /// <summary>
        /// Stop broadcasting UDP messages.
        /// </summary>
        public void EndBroadcast()
        {
            if (_broadcastTimer != null)
                _broadcastTimer.Dispose();
        }

        /// <summary>
        /// A callback called by the broadcast timer.  Broadcasts a message.
        /// </summary>
        /// <param name="state">An object representing the byte[] message to be broadcast.</param>
        private void BroadcastTimerCallback(object state)
        {
            ((Packet)state).CurrentSocket.SendTo(((Packet)state).DataBuffer,
                new IPEndPoint(IPAddress.Broadcast, ((Packet)state).ClientNumber));
        }

        /// <summary>
        /// An internal callback triggered when a client connects to the server.
        /// </summary>
        /// <param name="asyn"></param>
        private void OnReceiveConnection(IAsyncResult asyn)
        {
            try
            {
                lock (workerSockets)
                {
                    Interlocked.Increment(ref _currentClientNumber); // Thread Safe
                    UserSock us = new UserSock(_currentClientNumber, _mainSocket.EndAccept(asyn));
                    workerSockets.Add(_currentClientNumber, us);
                }

                if (_clientConnect != null)
                    _clientConnect(_currentClientNumber);

                WaitForData(_currentClientNumber);
                _mainSocket.BeginAccept(new AsyncCallback(OnReceiveConnection), null);
            }
            catch (ObjectDisposedException)
            {
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine("OnClientConnection: Socket has been closed");
                }
            }
            catch (SocketException se)
            {
                //Console.WriteLine("SERVER EXCEPTION in OnReceiveConnection: " + se.Message);
                System.Diagnostics.Debug.WriteLine("SERVER EXCEPTION in OnReceiveConnection: " +
                                                   se.Message); //pe 4-22-2015

                if (workerSockets.ContainsKey(_currentClientNumber) && AppSettings.ModeRun)
                {
                    Console.WriteLine("RemoteEndPoint: " +
                                      workerSockets[_currentClientNumber].UserSocket.RemoteEndPoint.ToString());
                    Console.WriteLine("LocalEndPoint: " +
                                      workerSockets[_currentClientNumber].UserSocket.LocalEndPoint.ToString());

                    Console.WriteLine("Closing socket from OnReceiveConnection");
                }

                //Socket gets closed and removed from OnClientDisconnect
                if (OnClientDisconnect != null)
                    OnClientDisconnect(_currentClientNumber);
            }
        }

        /// <summary>
        /// Begins an asynchronous wait for data for a particular client.
        /// </summary>
        /// <param name="clientNumber">A unique identifier of the client that has connected to the server.</param>
        private void WaitForData(int clientNumber)
        {
            if (!workerSockets.ContainsKey(clientNumber))
            {
                //Console.WriteLine("NO KEY: " + clientNumber.ToString());
                return;
            }

            try
            {
                Packet pack = new Packet(workerSockets[clientNumber].UserSocket, clientNumber);
                workerSockets[clientNumber].UserSocket.BeginReceive(pack.DataBuffer, 0, pack.DataBuffer.Length,
                    SocketFlags.None, new AsyncCallback(OnDataReceived), pack);
            }
            catch (SocketException se)
            {
                try
                {
                    //Socket gets closed and removed from OnClientDisconnect
                    OnClientDisconnect?.Invoke(clientNumber);

                    //Console.WriteLine("SERVER EXCEPTION in WaitForClientData: " + se.Message);
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine(
                            $"SERVER EXCEPTION in WaitForClientData: {se.Message}"); //pe 4-22-2015
                    }
                }
                catch
                {
                }
            }
            catch (Exception ex)
            {
                //Socket gets closed and removed from OnClientDisconnect
                OnClientDisconnect?.Invoke(clientNumber);

                string msg = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"SERVER EXCEPTION in WaitForClientData2: {msg}"); //pe 5-3-2017
                }
            }
        }

        /// <summary>
        /// An internal callback triggered when the server recieves data from a client.
        /// </summary>
        /// <param name="asyn"></param>
        private void OnDataReceived(IAsyncResult asyn)
        {
            Packet socketData = (Packet)asyn.AsyncState;

            try
            {
                int dataSize = socketData.CurrentSocket.EndReceive(asyn);

                if (dataSize.Equals(0))
                {
                    //System.Diagnostics.Debug.WriteLine($"OnDataReceived datasize is 0, zerocount = {((UserSock)workerSockets[socketData.ClientNumber]).ZeroDataCount}");//pe 5-3-2017

                    if (workerSockets.ContainsKey(socketData.ClientNumber))
                    {
                        if (((UserSock)workerSockets[socketData.ClientNumber]).ZeroDataCount++ == 10)
                        {
                            if (OnClientDisconnect != null)
                                OnClientDisconnect(socketData.ClientNumber);
                        }
                    }
                }
                else
                {
                    //if (_receive != null)
                    _receive(socketData.ClientNumber, socketData.DataBuffer, dataSize);

                    ((UserSock)workerSockets[socketData.ClientNumber]).ZeroDataCount = 0;
                }

                WaitForData(socketData.ClientNumber);
            }
            catch (ObjectDisposedException)
            {
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine("OnDataReceived: Socket has been closed");
                }
                //Socket gets closed and removed from OnClientDisconnect
                OnClientDisconnect?.Invoke(socketData.ClientNumber);
            }
            catch (SocketException se)
            {
                //10060 - A connection attempt failed because the connected party did not properly respond after a period of time,
                //or established connection failed because connected host has failed to respond.
                if (se.ErrorCode == 10054 || se.ErrorCode == 10060) //10054 - Error code for Connection reset by peer
                {
                    try
                    {
                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine(
                                "SERVER EXCEPTION in OnClientDataReceived, ServerObject removed:(" +
                                se.ErrorCode.ToString() + ") " + socketData.ClientNumber +
                                ", (happens during a normal client exit)");
                            Console.WriteLine("RemoteEndPoint: " +
                                              workerSockets[socketData.ClientNumber].UserSocket
                                                  .RemoteEndPoint.ToString());
                            Console.WriteLine("LocalEndPoint: " +
                                              workerSockets[socketData.ClientNumber].UserSocket
                                                  .LocalEndPoint.ToString());
                        }
                    }
                    catch
                    {
                        //
                    }

                    //Socket gets closed and removed from OnClientDisconnect
                    OnClientDisconnect?.Invoke(socketData.ClientNumber);

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine("Closing socket from OnDataReceived");
                    }
                }
                else
                {
                    string mess = "CONNECTION BOOTED for reason other than 10054: code = " + se.ErrorCode.ToString() +
                                  ",   " + se.Message;

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine("Closing socket from OnDataReceived");
                    }

                    ToFile(mess);
                }
            }
        }

        private void ToFile(string message)
        {
            string AppPath = CommonClassLibs.GeneralFunction.GetAppPath;//Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);//System.Windows.Forms.Application.StartupPath;

            StreamWriter sw = null;
            try
            {
                sw = File.AppendText(Path.Combine(AppPath, "ServerSocketIssue.txt"));
                string logLine = $"{DateTime.Now:G}: {message}.";
                sw.WriteLine(logLine);
            }
            catch// (Exception ex)
            {
                //Console.WriteLine("\n\nError in ToFile:\n" + message + "\n" + ex.Message + "\n\n");
                // System.Windows.Forms.MessageBox.Show("ERROR:\n\n" + ex.Message, "Possible Permissions Issue!");
            }
            finally
            {
                try
                {
                    if (sw != null)
                    {
                        sw.Close();
                        sw.Dispose();
                    }
                }
                catch
                {
                    // ignored
                }
            }
        }

    }

    public class PingStatsClass
    {
        public PingStatsClass()//Int32 ClientID)
        {
            //clientID = ClientID;
            sw = new System.Diagnostics.Stopwatch();
            PingCounter = 0;
            PingTimeTotal = 0;
            LongestPing = 0;
            LongestPingDateTimeStamp = DateTime.Now;
        }

        private System.Diagnostics.Stopwatch sw = null;

        public Int32 PingCounter;

        /// <summary>
        /// Time is in milliseconds
        /// </summary>
        public Int64 PingTimeTotal;

        /// <summary>
        /// Time is in milliseconds
        /// </summary>
        public Int64 LongestPing;
        public DateTime LongestPingDateTimeStamp;

        /// <summary>
        /// returns the elapsed ping time in miliseconds
        /// </summary>
        /// <returns></returns>
        public Int64 StopTheClock()
        {
            if (sw.IsRunning)
            {
                sw.Stop();

                PingCounter++;

                if (sw.ElapsedMilliseconds > LongestPing)
                {
                    LongestPing = sw.ElapsedMilliseconds;
                    LongestPingDateTimeStamp = DateTime.Now;
                }

                PingTimeTotal += sw.ElapsedMilliseconds;
            }

            return sw.ElapsedMilliseconds;
        }

        public void StartTheClock()
        {
            sw.Reset();

            if (!sw.IsRunning)
                sw.Start();
            else
                sw.Restart();
        }

        public Int64 GetElapsedTime => sw.ElapsedMilliseconds;
    }
}
