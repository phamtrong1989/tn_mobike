﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using TN.Mobike.LockServices.Entity;
using TN.Mobike.LockServices.MinaCore;
using TN.Mobike.LockServices.Model.Data;
using TN.Mobike.LockServices.Model.Helper;
using TN.Mobike.LockServices.Settings;

namespace TN.Mobike.LockServices.Core
{
    class UnLock
    {
        public static void SocketForStatusLock()
        {
            Socket socket = new Socket();
            socket.Start();
        }

        public static void QueryDbToUnlock()
        {

            try
            {
                var listOpenRequest = OpenLockModel.GetAllOpenLockRequests(DateTime.Now);

                if (listOpenRequest == null || listOpenRequest.Count <= 0)
                {
                    //Console.WriteLine($"NO DATA IN {DateTime.Now:yyyy/MM/dd HH:mm:ss}");
                    return;
                }

                foreach (var item in listOpenRequest)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("===============================================================");
                        Console.WriteLine(JsonConvert.SerializeObject(item));
                        Console.WriteLine("===============================================================");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    //var check = Socket.Unlock(item.IMEI);
                    var check = MinaControl.UnLock(item.IMEI);

                    if (!check) continue;

                    var openLock = new OpenLockHistory(item);
                    openLock.Status = 0;

                    OpenLockModel.InsertOpenLockHistory(openLock);

                    OpenLockModel.DeleteOpenLockRequest(item.Id);

                    var dock = OpenLockModel.GetOneDock(item.IMEI);

                    if (dock != null)
                    {
                        dock.LockStatus = false;
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"OPEN SUCCESSFUL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");
                    Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"INSERT TO OpenLockHistory SUCCESSFUL : {JsonConvert.SerializeObject(item)}");
                    Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"DELETE SUCCESSFUL FROM OpenLockRequest : {JsonConvert.SerializeObject(item)}");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ControlServices.QueryDbToUnlock]", $"[ERROR: {e}]");
            }

        }

        public static async void ReceiverToUnlock()
        {
            SignalRHub.InitHub();

            await SignalRHub._connection.StartAsync();

            if (AppSettings.ModeRun)
            {
                Console.WriteLine("CONNECT SUCCESSFUL TO SIGNAL HUB !");
            }

            SignalRHub._connection.On<string>("DeviceReceiveMessage", (data) =>
            {
                if (AppSettings.ModeRun)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("===============================================================");
                    Console.WriteLine(data);
                    Console.WriteLine("===============================================================");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Socket.ProcessData(data);
            });

        }
    }
}
