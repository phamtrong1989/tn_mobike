﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Buffer;
using Mina.Core.Session;

namespace TN.Mobike.LockServices.MinaCore
{
    class SessionMap
    {
        private static SessionMap sessionMap = null;
        public static readonly int STATUS_SEND_SESSION_NULL = 0;
        public static readonly int STATUS_SEND_SUCCESSFULLY = 1;
        public static readonly int STATUS_SEND_FAILING = 2;

        private Dictionary<long, IoSession> map = new Dictionary<long, IoSession>();

        public SessionMap()
        {
            
        }

        public static SessionMap NewInstance()
        {
            return sessionMap ?? (sessionMap = new SessionMap());
        }

        public void AddSession(long key, IoSession session)
        {
            try
            {
                Console.WriteLine($"KEY ADD SESSION = {key}");
                if (!map.TryGetValue(key, out var value))
                {
                    map.Add(key, session);
                }
                else
                {
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IoSession GetSession(long key)
        {
            map.TryGetValue(key, out var session);

            return session;
        }

        public bool ConstantKey(long key)
        {
            return map.ContainsKey(key);
        }

        public void RemoveSession(IoSession session)
        {
           
        }

        public int SendMessage(long key, string message, bool isDebug = false)
        {
            IoSession session = GetSession(key);

            if (session == null)
            {
                Console.WriteLine($"IMEI: {key} has not connected the service");
                return STATUS_SEND_SESSION_NULL;
            }

            if (isDebug)
            {
                Console.WriteLine($"DEBUG write IP = {session.RemoteEndPoint.Serialize().Family}");
                Console.WriteLine($"DEBUG write IP = {session.RemoteEndPoint.AddressFamily}");
            }

            session.Write(message);
            return STATUS_SEND_SUCCESSFULLY;
        }

        public int SendMessageArray(long key, byte[] messBytes, bool isDebug = false)
        {
            IoSession session = GetSession(key);
            if (session == null)
            {
                Console.WriteLine($"IMEI: {key} has not connected the service");
                return STATUS_SEND_SESSION_NULL;
            }

            if (isDebug)
            {
                Console.WriteLine($"DEBUG write IP = {session.RemoteEndPoint.Serialize().Family}");
                Console.WriteLine($"DEBUG write IP = {session.RemoteEndPoint.AddressFamily}");
            }

            session.Write(IoBuffer.Wrap(messBytes));
            return STATUS_SEND_SUCCESSFULLY;
        }

        public int SendMessage(long key, byte[] messBytes, bool isDebug = false)
        {
            return SendMessageArray(key, messBytes, isDebug);
        }
    }
}
