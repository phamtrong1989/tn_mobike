﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Mina.Core.Service;
using Mina.Core.Session;
using TN.Mobike.LockServices.Entity;
using TN.Mobike.LockServices.Model.Data;
using TN.Mobike.LockServices.Settings;

namespace TN.Mobike.LockServices.MinaCore
{
    class MinaSocket : IoHandlerAdapter
    {
        public override void ExceptionCaught(IoSession session, Exception cause)
        {
            Console.WriteLine(cause.ToString());
        }

        public override void MessageReceived(IoSession session, object message)
        {
            try
            {
                string msg = message.ToString();
                Console.WriteLine($"Message : {msg}");

                if (msg != null && msg.Length > 10)
                {
                    if (msg.Contains("*CMDR") && msg.Contains("#"))
                    {
                        int firstIndex = msg.IndexOf("#", StringComparison.Ordinal);
                        int lastIndex = msg.LastIndexOf("#", StringComparison.Ordinal);

                        if (firstIndex == lastIndex)
                        {
                            int startIndex = msg.IndexOf("*CMDR", StringComparison.Ordinal);
                            string order = msg.Substring(startIndex, lastIndex + 1);
                            HandCommand(session, order);
                        }
                        else
                        {
                            string[] strMsg = msg.Split('#');
                            foreach (var m in strMsg)
                            {
                                int startIndex = m.IndexOf("*CMDR", StringComparison.Ordinal);
                                string oneComm = m.Substring(startIndex, m.Length);
                                HandCommand(session, $"{oneComm}#");
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public override void SessionIdle(IoSession session, IdleStatus status)
        {
            try
            {
                Console.WriteLine($"IDLE : {session.GetIdleCount(status)}");
                if (status == IdleStatus.ReaderIdle)
                {
                    Console.WriteLine("READER IDLE");

                }
                else if (status == IdleStatus.WriterIdle)
                {
                    Console.WriteLine("WRITER IDLE");
                }
                else
                {
                    Console.WriteLine("BOTH IDLE");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public new void InputClosed(IoSession session)
        {
            SessionMap.NewInstance().RemoveSession(session);
            InputClosed(session);
        }

        public override void SessionClosed(IoSession session)
        {
            session.CloseOnFlush();
        }

        private void HandCommand(IoSession session, string command)
        {
            if(command.Length <= 1) return;

            var comm = command.Split(',').ToList();
            var comCode = comm[4];
            var imei = comm[2];
            var imeiL = Convert.ToInt64(imei);

            Console.WriteLine($"MESSAGE FROM IMEI: {imeiL}");

            SessionMap.NewInstance().AddSession(imeiL, session);

            var message = "";
            int status = 0;

            Dock dock = new Dock();

            switch (comCode.ToUpper())
            {
                //
                case "Q0":

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Q0 - SIGN IN COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Q0 - IMEI = {imei}");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER Q0 : {command}");


                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Thời gian : {comm[3]}");
                    Console.WriteLine($"Pin       : {comm[5].Replace("#<LF>", "")}");
                    Console.ForegroundColor = ConsoleColor.White;

                    break;
                case "H0":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : H0 - HEARTBEAT COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER H0 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Thời gian : {comm[3]}");
                    Console.WriteLine(comm[5] == "0" ? $"Trạng thái : Đã mở khóa" : $"Trạng thái : Đã khóa");
                    Console.WriteLine($"Pin : {comm[6]}");
                    Console.WriteLine($"GMS : {comm[7].Replace("#<LF>", "")}");
                    Console.ForegroundColor = ConsoleColor.White;

                    if (dock != null)
                    {
                        if (comm[5] == "0")
                        {
                            dock.LockStatus = false;
                        }
                        else
                        {
                            dock.LockStatus = true;
                        }
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }


                    break;
                case "L1":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : L1 -  LOCK COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER L1 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Thời gian : {comm[3]}");
                    Console.WriteLine($"USER ID   : {comm[5]}");
                    Console.WriteLine($"Thời gian mở khóa : {comm[6]}");
                    Console.WriteLine($"Thời gian lên xe : {comm[7].Replace("#<LF>", "")}");
                    Console.ForegroundColor = ConsoleColor.White;

                    message = $"*CMDS,OM,{imei},000000000000,Re,L1#<LF>";

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        dock.LockStatus = true;
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    //status = SessionMap.NewInstance().SendMessage(imeiL, message, false);

                    break;
                case "L0":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : L0 - UNLOCK COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER L0 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($" Thời gian : {comm[3]}");
                    Console.WriteLine(comm[5] == "0" ? $"Trạng thái : Đã mở khóa" : $"Trạng thái : Đã khóa");
                    Console.WriteLine($" Thời gian mở khóa : {comm[6]}");
                    Console.WriteLine($" Thời gian lên xe : {comm[7].Replace("#<LF>", "")}");
                    Console.ForegroundColor = ConsoleColor.White;

                    message = $"*CMDS,OM,{imei},001497689816,Re,L0#<LF>";

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        if (comm[5] == "0")
                        {
                            dock.LockStatus = false;
                        }
                        else
                        {
                            dock.LockStatus = true;
                        }
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    //status = SessionMap.NewInstance().SendMessage(imeiL, message, false);

                    break;
                case "D0":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : D0 - POSITIONING COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER D0 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;

                    var d12 = comm.IndexOf(comm.LastOrDefault());

                    var data12 = comm[d12];
                    var data11 = comm[d12 - 1];
                    var data10 = comm[d12 - 2];
                    var data9 = comm[d12 - 3];
                    var data8 = comm[d12 - 4];
                    var data7 = comm[d12 - 5];
                    var data6 = comm[d12 - 6];
                    var data5 = comm[d12 - 7];
                    var data4 = comm[d12 - 8];
                    var data3 = comm[d12 - 9];
                    var data2 = comm[d12 - 10];
                    var data1 = comm[d12 - 11];

                    Console.WriteLine($"<1> : {data1} (hhmmss / UTC)");


                    if (data2 == "V")
                    {
                        Console.WriteLine($"<2> : {data2} = Định vị không hoạt động");
                    }
                    else if(data2 == "A")
                    {
                        Console.WriteLine($"<2> : {data2} = Định vị hoạt động");
                    }

                    Console.WriteLine($"<3> : {data3} | Vĩ độ ddmm.mmmm");
                    Console.WriteLine($"<4> : {data4} | N (Bán cầu Bắc) | S (Bán cầu Nam)");
                    Console.WriteLine($"<5> : {data5} | Kinh độ dddmm.mmmm");
                    Console.WriteLine($"<6> : {data6} | E (Đông) | W (Tây)");
                    Console.WriteLine($"<7> : {data7} | Vận tốc");
                    Console.WriteLine($"<8> : {data8} | Ground heading");
                    Console.WriteLine($"<9> : {data9} | Ngày UTC | ddmmyy");
                    Console.WriteLine($"<10> : {data10} | Độ nghiêng từ tính");
                    Console.WriteLine($"<11> : {data11} | Hướng nghiêng từ tính | E (Đông) | W (Tây)");
                    if (data12 == "N#")
                    {
                        Console.WriteLine("<12> : N = Dữ liệu định vị không hợp lệ");
                    }
                    else if (data12 == "A#")
                    {
                        Console.WriteLine("<12> : A = Định vị tự động");
                    }
                    else if (data12 == "D#")
                    {
                        Console.WriteLine("<12> : D = Khác");
                    }



                    Console.ForegroundColor = ConsoleColor.White;

                    message = $"*CMDS,OM,{imei},000000000000,Re,D0#<LF>";

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }
                    //status = SessionMap.NewInstance().SendMessage(imeiL, message, false);

                    break;
                case "S5":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : S5 - LOCK STATUS COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER S5 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Pin : {comm[5]}");
                    Console.WriteLine($"GMS : {comm[6]}");
                    Console.WriteLine(comm[8] == "0" ? $"Trạng thái : Đã mở khóa" : $"Trạng thái : Đã khóa");
                    Console.ForegroundColor = ConsoleColor.White;

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        if (comm[8] == "0")
                        {
                            dock.LockStatus = false;
                        }
                        else
                        {
                            dock.LockStatus = true;
                        }
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    break;
                case "S8":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : S8 - RINGING FOR FINDING A BIKE COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER S8 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Thời gian : {comm[3]}");
                    Console.WriteLine($"Đổ chuông : {comm[5]} giây");
                    Console.ForegroundColor = ConsoleColor.White;

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    break;
                case "G0":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : G0 - QUERY FIRMWARE VERSION COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER G0 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Thời gian : {comm[3]}");
                    Console.WriteLine($"Phiên bản : {comm[5]}");
                    Console.WriteLine($"Thời gian cập nhật : {comm[6].Replace("#<LF>","")}");
                    Console.ForegroundColor = ConsoleColor.White;

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    break;
                case "W0":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : W0 - ALARMING COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER W0 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Thời gian : {comm[3]}");
                    Console.WriteLine(comm[5].Replace("#<LF>","") == "1" ? $"Báo động  : ON" : $"Báo động  : OFF");
                    Console.ForegroundColor = ConsoleColor.White;

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    break;
                case "I0":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : I0 - OBTAIN SIM CARD ICCID CODE COMMAND");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER I0 : {command}");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Thời gian : {comm[3]}");
                    Console.WriteLine($"Mã ICCID  : {comm[5].Replace("#<LF>", "")}");
                    Console.ForegroundColor = ConsoleColor.White;

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }

                    break;
                case "M0":
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : M0 - NOTHING");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER M0 : {command}");

                    dock = OpenLockModel.GetOneDock(imei);

                    if (dock != null)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        Utilities.WriteOperationLog("DOCK", $"{dock.ToString()}");

                        OpenLockModel.UpdateDock(dock);
                    }
                    break;
                default:
                    break;
            }
            status = SessionMap.NewInstance().SendMessage(imeiL, message, false);
            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message??"No Message"}");
            Console.WriteLine(status == 1 ? $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Success" : $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Fail");
            Console.WriteLine("====================================================================================================");
        }

        public static byte[] AddBytes(byte[] b1, byte[] b2)
        {
            byte[] b = new byte[b1.Length + b2.Length];
            Array.Copy(b1, 0, b, 0, b1.Length);
            Array.Copy(b2, 0, b, b1.Length, b2.Length);

            return b;
        }

    }
}
