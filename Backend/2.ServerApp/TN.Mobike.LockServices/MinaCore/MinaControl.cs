﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Service;
using Mina.Core.Session;
using Mina.Filter.Codec;
using Mina.Filter.Codec.TextLine;
using Mina.Filter.Executor;
using Mina.Filter.Logging;
using Mina.Transport.Socket;
using TN.Mobike.LockServices.Settings;

namespace TN.Mobike.LockServices.MinaCore
{
    class MinaControl
    {
        private static readonly int PORT = AppSettings.PortService;
        private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static void InitialMina()
        {
            try
            {
                //IoAcceptor acceptor = new AsyncSocketAcceptor();
                //acceptor.FilterChain.AddLast("logger", new LoggingFilter());
                //acceptor.FilterChain.AddLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Encoding.UTF8)));
                ////acceptor.FilterChain.AddLast("exceutor", new ExecutorFilter());
                ////acceptor.Handler = new MinaSocket();
                ////acceptor.SessionConfig.ReadBufferSize = 2048;
                ////acceptor.SessionConfig.SetIdleTime(IdleStatus.ReaderIdle, 8*60);
                ////acceptor.ExceptionCaught += (o, e) => Console.WriteLine(e.Exception);
                ////acceptor.ExceptionCaught += (o, e) => Console.WriteLine(e.Exception);

                //acceptor.ExceptionCaught +=
                //    (o, e) => Console.WriteLine(e.Exception);

                //acceptor.SessionIdle +=
                //    (o, e) => Console.WriteLine("IDLE " + e.Session.GetIdleCount(e.IdleStatus));

                //acceptor.MessageReceived += (o, e) =>
                //{
                //    String str = e.Message.ToString();
                //    Console.WriteLine(str);

                //    // "Quit" ? let's get out ...
                //    if (str.Trim().Equals("quit", StringComparison.OrdinalIgnoreCase))
                //    {
                //        e.Session.Close(true);
                //        return;
                //    }

                //    // Send the current date back to the client
                //    e.Session.Write(DateTime.Now.ToString());
                //    Console.WriteLine("Message written...");
                //};

                IoAcceptor acceptor = new AsyncSocketAcceptor();

                acceptor.FilterChain.AddLast("logger", new LoggingFilter());
                acceptor.FilterChain.AddLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Encoding.UTF8)));

                acceptor.ExceptionCaught +=
                    (o, e) => Console.WriteLine(e.Exception);

                acceptor.SessionIdle +=
                    (o, e) => Console.WriteLine("IDLE " + e.Session.GetIdleCount(e.IdleStatus));

                acceptor.FilterChain.AddLast("exceutor", new ExecutorFilter());
                acceptor.Handler = new MinaSocket();
                acceptor.SessionConfig.ReadBufferSize = 2048;
                acceptor.SessionConfig.SetIdleTime(IdleStatus.ReaderIdle, 8 * 60);

                //acceptor.MessageReceived += (o, e) =>
                //{
                //    String str = e.Message.ToString();

                //    Console.WriteLine(str);
                //    // "Quit" ? let's get out ...
                //    if (str.Trim().Equals("quit", StringComparison.OrdinalIgnoreCase))
                //    {
                //        e.Session.Close(true);
                //        return;
                //    }

                //    // Send the current date back to the client
                //    e.Session.Write(DateTime.Now.ToString());
                //    Console.WriteLine("Message written...");
                //};

                var ipAddress = IPAddress.Parse(AppSettings.HostService);

                acceptor.Bind(new IPEndPoint(ipAddress, PORT));
                Console.WriteLine($"LISTENING ON PORT : {PORT}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static bool UnLock(string imei)
        {
            var key = "L0";
            int uid = 0;
            long timestamp = (long) (DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
            var message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},{key},0,{uid},{timestamp}#";

            //var message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},D0#<LF>";
            //var message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},S5#<LF>";
            //var message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},S8,5,0#<LF>";
            //var message = $"*CMDS,XX,{imei},{DateTime.Now:yyMMddHHmmss},G0#<LF>";
            //var message = $"*CMDS,XX,{imei},{DateTime.Now:yyMMddHHmmss},W0#<LF>";
            //var message = $"*CMDS,XX,{imei},{DateTime.Now:yyMMddHHmmss},I0#<LF>";x

            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Message : {message}");

            var command = MinaSocket.AddBytes(new byte[] {(byte) 0xFF, (byte) 0xFF}, Encoding.ASCII.GetBytes(message));

            var result = SessionMap.NewInstance().SendMessage(Convert.ToInt64(imei), command, false);

            Console.WriteLine(result == 1 ? $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Success" : $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Fail");
            Console.WriteLine("====================================================================================================");
            return Convert.ToBoolean(result);
        }
    }
}
