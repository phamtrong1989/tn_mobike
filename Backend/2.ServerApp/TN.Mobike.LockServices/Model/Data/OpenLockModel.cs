﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Mobike.LockServices.Entity;
using TN.Mobike.LockServices.Model.Helper;
using TN.Mobike.LockServices.Settings;

namespace TN.Mobike.LockServices.Model.Data
{
    class OpenLockModel
    {
        private static SqlHelper sqlHelper = new SqlHelper();
        public static List<OpenLockRequest> GetAllOpenLockRequests(DateTime nowTime)
        {
            List<OpenLockRequest> list = new List<OpenLockRequest>();
            list = sqlHelper.GetAll<OpenLockRequest>(
                $"Where CreatedDate >= '{DateTime.Now.AddSeconds(-AppSettings.LimitTimeOpen):yyyy-MM-dd HH:mm:ss.ffffff}' AND CreatedDate <= '{DateTime.Now:yyyy-MM-dd HH:mm:ss.ffffff}'");
            return list;
        }

        public static void InsertOpenLockHistory(OpenLockHistory openLock)
        {
            sqlHelper.InsertOne(openLock);
        }

        public static void DeleteOpenLockRequest(int id)
        {
            sqlHelper.DeleteRecord<OpenLockRequest>($"id = {id}");
        }

        public static Dock GetOneDock(string imei)
        {
            var dock = (Dock)sqlHelper.GetOne<Dock>($"IMEI = '{imei}'");

            return dock;
        }

        public static void UpdateDock(Dock dock)
        {
            sqlHelper.UpdateOne(dock, $"id = {dock.Id}");
        }
    }
}
