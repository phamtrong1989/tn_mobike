﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.Mobike.LockServices.Entity
{
    class Dock
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public int StationId { get; set; }
        public int BikeId { get; set; }
        public string IMEI { get; set; }
        public string SerialNumber { get; set; }
        public string Model { get; set; }
        public int Status { get; set; }
        public bool LockStatus { get; set; }
        public double Battery { get; set; }
        public bool Charging { get; set; }
        public bool ConnectionStatus { get; set; }
        public DateTime LastConnectionTime { get; set; }
        public int BookingStatus { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime UpdatedDate { get; set; } = DateTime.Now;

        public Dock()
        {
            
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
