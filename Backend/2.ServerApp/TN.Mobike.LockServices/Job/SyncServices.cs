﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TN.Mobike.LockServices.Core;
using TN.Mobike.LockServices.MinaCore;
using TN.Mobike.LockServices.Model.Helper;
using TN.Mobike.LockServices.Settings;

namespace TN.Mobike.LockServices.Job
{
    class SyncServices
    {
        public void Start()
        {
            try
            {
                Utilities.WriteDebugLog("Start", "App Started!");

                Console.WriteLine("1");
                SqlHelper.CheckConnection();
                Console.WriteLine("2");
                //SqlHelper.OpenConnectMongoDb();
                //Console.WriteLine("3");
                if (AppSettings.TurnOnSocket)
                {
                    //Thread threadSocketLock = new Thread(UnLock.SocketForStatusLock);
                    Thread threadSocketLock = new Thread(MinaControl.InitialMina);
                    threadSocketLock.Start();
                }

                if (AppSettings.TurnOnSignalR)
                {
                    Thread threadSignalR = new Thread(UnLock.ReceiverToUnlock);
                    threadSignalR.Start();
                }

                if (AppSettings.TurnOnQueryDb)
                {
                    Thread threadScanDb = new Thread(JobScheduler.Start);
                    threadScanDb.Start();
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Start", ex.ToString());
            }
        }
        public void Stop()
        {
            try
            {
                JobScheduler.Stop();
                SignalRHub.DisconnectHub();
                Utilities.WriteDebugLog("ProgramInit_Stop", "Try to stop service...");
                Utilities.CloseLog();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Stop", ex.ToString());
            }
        }
    }
}
