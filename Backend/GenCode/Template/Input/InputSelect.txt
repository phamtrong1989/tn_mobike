﻿<mat-form-field floatLabel="always" appearance="outline">
 <mat-label translate>{{table-name}}.{{colurmName}}</mat-label>
    <mat-select [(value)]="data.{{colurmName}}" formControlName="{{colurmName}}" placeholder="{{ColurmName}}">
      <!-- <mat-option *ngFor="let item of roletypes; index as i;" [value]="item.value">
            {{item.text}}
        </mat-option> -->
    </mat-select>
   	<mat-error *ngIf="form.controls.{{colurmName}}.errors?.required" translate>validate.required</mat-error>
</mat-form-field>