﻿<mat-form-field floatLabel="always" appearance="outline">
   <mat-label translate>{{table-name}}.{{colurmName}}</mat-label>
    <input matInput formControlName="{{colurmName}}" placeholder="#" type="number" min="0" max="999999">
	<mat-error *ngIf="form.controls.{{colurmName}}.errors?.required" translate>validate.required</mat-error>
</mat-form-field>