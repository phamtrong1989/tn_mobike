﻿using {{NamespacBase}}.Domain.Model;
using {{NamespacBase}}.Infrastructure;
using System.Threading.Tasks;
using {{NamespacBase}}.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace {{Namespace}}
{
    public class {{TableName}}Repository : EntityBaseRepository<{{TableName}}>, I{{TableName}}Repository
    {
        public {{TableName}}Repository(ApplicationContext context) : base(context)
        {
        }
    }
}

