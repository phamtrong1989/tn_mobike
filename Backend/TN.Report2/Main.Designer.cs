﻿
namespace TN.Report
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PickerStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PickerEnd = new System.Windows.Forms.DateTimePicker();
            this.BtnExport = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LblInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PickerStart
            // 
            this.PickerStart.CustomFormat = "dd/MM/yyyy";
            this.PickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PickerStart.Location = new System.Drawing.Point(64, 17);
            this.PickerStart.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.PickerStart.MinDate = new System.DateTime(2022, 1, 1, 0, 0, 0, 0);
            this.PickerStart.Name = "PickerStart";
            this.PickerStart.Size = new System.Drawing.Size(106, 20);
            this.PickerStart.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Từ ngày:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Đến hết ngày:";
            // 
            // PickerEnd
            // 
            this.PickerEnd.CustomFormat = "dd/MM/yyyy";
            this.PickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PickerEnd.Location = new System.Drawing.Point(274, 17);
            this.PickerEnd.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.PickerEnd.MinDate = new System.DateTime(2022, 1, 1, 0, 0, 0, 0);
            this.PickerEnd.Name = "PickerEnd";
            this.PickerEnd.Size = new System.Drawing.Size(106, 20);
            this.PickerEnd.TabIndex = 2;
            // 
            // BtnExport
            // 
            this.BtnExport.Location = new System.Drawing.Point(274, 56);
            this.BtnExport.Name = "BtnExport";
            this.BtnExport.Size = new System.Drawing.Size(105, 23);
            this.BtnExport.TabIndex = 4;
            this.BtnExport.Text = "Xuất dữ liệu";
            this.BtnExport.UseVisualStyleBackColor = true;
            this.BtnExport.Click += new System.EventHandler(this.BtnExport_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Firebrick;
            this.label3.Location = new System.Drawing.Point(12, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "- Lượt xe đi qua tuyến đường Pasteur";
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.Firebrick;
            this.label4.Location = new System.Drawing.Point(12, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(368, 50);
            this.label4.TabIndex = 6;
            this.label4.Text = "- Trạm liên quan  Trung tâm thương mại Saigon Centre, Bảo tàng HCM, Tổng công ty " +
    "xây dựng số 1, Công viên 30/4, Hàm Nghi, Công xã Paris, Thương xá Tax";
            // 
            // LblInfo
            // 
            this.LblInfo.AutoSize = true;
            this.LblInfo.Location = new System.Drawing.Point(12, 45);
            this.LblInfo.Name = "LblInfo";
            this.LblInfo.Size = new System.Drawing.Size(0, 13);
            this.LblInfo.TabIndex = 7;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 136);
            this.Controls.Add(this.LblInfo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BtnExport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PickerEnd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PickerStart);
            this.Name = "Main";
            this.Text = "Lượt xe đi qua tuyến đường Pasteur";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker PickerStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker PickerEnd;
        private System.Windows.Forms.Button BtnExport;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LblInfo;
    }
}

