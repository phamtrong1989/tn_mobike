import { Investor } from "../category/investors/investors.model";
import { Project } from "../category/projects/projects.model";
import { Bike } from "../infrastructure/bikes/bikes.model";
import { Dock } from "../infrastructure/docks/docks.model";

export class BikeReport {
  id: number;
  dockId: number;
  bikeId: number;
  type: number;
  description: string;
  createdDate: Date;
  updatedDate: Date;
  accountId: number;
  updatedUser : number;
  bookingId: number;
  transactionCode: string;
  note: string;
  status: boolean;
  file: string;
  investorId: number;
  projectId: number;
  bike: Bike;
  dock: Dock;
  files: string;
}

export class BikeReportCommand {
  id: number;
  type: number;
  description: string;
  note: string;
  status: boolean;
  file: string;
  bike: Bike;
  dock: Dock;
  files: string;

  deserialize(input: BikeReport): BikeReportCommand {
    if (input == null) {
      input = new BikeReport();
    }
    this.id = input.id;
    this.type = input.type;
    this.description = input.description;
    this.note = input.note;
    this.status = input.status;
    this.file = input.file;
    this.bike = input.bike;
    this.dock = input.dock;
    this.files = input.files;
    return this;
  }
}