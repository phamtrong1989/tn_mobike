import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BikeReport, BikeReportCommand } from '../bike-reports.model';
import { BikeReportsService } from '../bike-reports.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-bike-reports-edit',
  templateUrl: './bike-reports-edit.component.html',
  providers: [
    BikeReportsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class BikeReportsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  uploadFileOut: FileDataDTO[];

  constructor(
    public dialogRef: MatDialogRef<BikeReportsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: BikeReportCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public bikeReportsService: BikeReportsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      note: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(1000)])],
      type: [0],
      description: [0],
      file: [null],
      status: [0],
      bike: [null],
      dock: [null],
      files: [null]
    });
  }

  ngOnInit() {
    if (this.data.files != null) {
      this.uploadFileOut = JSON.parse(this.data.files)
    }

    if (this.data.id > 0) {
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }

  getData() {
    this.isProcess.initData = true;
    this.bikeReportsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new BikeReportCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        if (this.data.files != null) {
          this.uploadFileOut = JSON.parse(this.data.files)
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save(status: any) {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
      this.bikeReportsService.edit(this.form.value, status).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
          this.close(res)
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
