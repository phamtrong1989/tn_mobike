import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../app.settings';
import { Settings, ApiResponseData } from '../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { BikeReportsService } from './bike-reports.service';
import { BikeReport, BikeReportCommand } from './bike-reports.model';
import { BikeReportsEditComponent } from './edit/bike-reports-edit.component';
import { BikeReportsDetailComponent } from './detail/bike-reports-detail.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../app.common'
import { AppStorage } from '../../app.storage';
import { FormControl } from '@angular/forms';
import { Account } from '../customer/accounts/accounts.model';

@Component({
  selector: 'app-bike-reports',
  templateUrl: './bike-reports.component.html',
  providers: [BikeReportsService, AppCommon]
})

export class BikeReportsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: BikeReport[];
  isProcess = { dialogOpen: false, listDataTable: true, accountSearching: false};
  params: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "transactionCode", "type", "accountId", "bikeId", "investorId", "description", "updatedDate", "note", "status", "img", "action"];
  public settings: Settings;
  public categoryData: any;
  accountKey: FormControl = new FormControl();
  accounts: Account[];
  constructor(
    public appSettings: AppSettings,
    public bikeReportsService: BikeReportsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.initSearchAccount();
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.appCommon.changeUrl(location.pathname, this.params);
    this.bikeReportsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;

      if(res.outData != null)
      {
        this.accounts = [];
        this.accounts.push(res.outData);
        this.params.accountId = res.outData?.id;
      }
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.params.from = this.appCommon.formatDateTime(this.params.from, "yyyy-MM-dd");
    this.params.to = this.appCommon.formatDateTime(this.params.to, "yyyy-MM-dd");
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: BikeReport) {
    var model = new BikeReportCommand();
    if (data == null) {
      model.id = 0;
    }
    else {
      model = new BikeReportCommand().deserialize(data);
    }

    this.dialog.open(BikeReportsEditComponent, {
      width: '800px',
      data: model,
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  public openImageDialog(data: BikeReport) {
    var model = new BikeReportCommand().deserialize(data);

    this.dialog.open(BikeReportsDetailComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  
  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
        this.isProcess.accountSearching = false;
        if (rs != "") {
          this.isProcess.accountSearching = true;
          this.bikeReportsService.searchByAccount({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.params.accountId = "";
            }
            this.accounts = rs.data;
            this.isProcess.accountSearching = false;
          });
        }
      },
        error => {
          this.isProcess.accountSearching = false;
      });
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}