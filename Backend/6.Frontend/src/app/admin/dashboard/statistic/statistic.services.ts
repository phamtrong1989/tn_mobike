import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ApiResponseData, ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AppCommon } from 'src/app/app.common';
import { AlertBike, Analytic, Log } from '../../troubleshooting-center/troubleshooting-center.model';
import { Dock } from '../../infrastructure/docks/docks.model';
import { BikeReport } from '../../bike-reports/bike-reports.model';
import { CustomerComplaint } from '../../customer/customer-complaints/customer-complaints.model';

@Injectable()
export class StatisticService {
  public url = "/api/work/troubleshootingCenter";
  _injector: Injector;
  public clientSettings: ClientSettings;
  constructor
    (
      public http: HttpClient,
      appClientSettings: AppClientSettings,
      public appCommon: AppCommon,
      injector: Injector
    ) {
    this.clientSettings = appClientSettings.settings
    this.url = this.clientSettings.serverAPI + this.url
    this._injector = injector;
  }

  alertBikeSearchPaged(params: any) {
    return this.http.get<ApiResponseData<AlertBike[]>>(this.url + "/alertBikeSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  getAnalyticBike(params: any) {
    return this.http.get<ApiResponseData<Analytic[]>>(this.url + "/getAnalyticBike", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  getAnalyticCashFlow(params: any) {
    return this.http.get<ApiResponseData<Analytic[]>>(this.url + "/getAnalyticCashFlow", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  getAnalyticCustomer(params: any) {
    return this.http.get<ApiResponseData<Analytic[]>>(this.url + "/getAnalyticCustomer", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  getAnalyticDock(params: any) {
    return this.http.get<ApiResponseData<Analytic[]>>(this.url + "/getAnalyticDock", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  getAnalyticRevenue(params: any) {
    return this.http.get<ApiResponseData<Analytic[]>>(this.url + "/getAnalyticRevenue", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  getAnalyticTransaction(params: any) {
    return this.http.get<ApiResponseData<any>>(this.url + "/getAnalyticTransaction", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  bikeControlSearchPaged(params: any) {
    return this.http.get<ApiResponseData<Dock[]>>(this.url + "/bikeControlSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  BikeReportSearchPaged(params: any) {
    return this.http.get<ApiResponseData<BikeReport[]>>(this.url + "/bikeReportSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  apiUrl() {
    return this.clientSettings.serverAPI;
  }

  customerComplaintSearchPaged(params: any) {
    return this.http.get<ApiResponseData<CustomerComplaint[]>>(this.url + "/customerComplaintSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  logAdminSearchPaged(params: any) {
    return this.http.get<ApiResponseData<Log[]>>(this.url + "/logAdminSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  handleError(error: any, injector: Injector) {
    if (error.status === 401) {
    } else {
    }
    return Promise.reject(error);
  }
}