import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { AccountData } from '../../account/account.model';
import { TroubleShootingService } from '../../troubleshooting-center/troubleshooting-center.services';

@Component({
  selector: 'app-dashboard-statistic',
  templateUrl: './statistic.component.html',
  providers: [TroubleShootingService, AppCommon]
})

export class StatisticComponent implements OnChanges {
  @Input() typeStatistic: string;
  @Input() projectId: number;
  @Input() statisticDate: Date;
  isAdmin: boolean = false;

  analyticRevenueData: any = [{ item1: "Tiền nạp tổng", item2: 0 }, { item1: "Tiền nạp dự án", item2: 0 }, { item1: "Đã thuê xe", item2: 0 }, { item1: "Nợ cước", item2: 0 }];
  analyticTransactionData: any = [{ item1: "Đang thuê", item2: 0 }, { item1: "Hoàn thành", item2: 0 }, { item1: "Nợ cước", item2: 0 }];
  analyticCustomerData: any = [{ item1: "Đăng ký mới", item2: 0 }, { item1: "Chưa xác thực", item2: 0 }, { item1: "Chờ duyệt", item2: 0 }, { item1: "Đã xác thực", item2: 0 }];
  analyticCashFlowData: any = [
    { item1: "MOMO", item2: 0, item3: 0 },
    { item1: "ZALOPay", item2: 0, item3: 0 },
    { item1: "VTCPay", item2: 0, item3: 0 },
    { item1: "Payoo", item2: 0, item3: 0 },
    { item1: "Trực tiếp", item2: 0, item3: 0 }
  ];
  analyticBikeData: any = [{ item1: "Sử dụng", item2: 0 }, { item1: "Sẵn sàng tại trạm", item2: 0 }, { item1: "Lưu kho", item2: 0 }, { item1: "Lỗi | Bảo dưỡng", item2: 0 }];
  analyticDockData: any = [{ item1: "Đang kết nối", item2: 0 }, { item1: "Mất kết nối", item2: 0 }, { item1: "Sắp hết pin", item2: 0 }];

  intervalGetRevenueData: any;
  intervalGetTransactionData: any;
  intervalGetCustomerData: any;
  intervalGetCashFlowData: any;
  intervalGetBikeData: any;
  intervalGetDockData: any;
  accountInfo: AccountData;

  constructor(
    public troubleShootingService: TroubleShootingService,
    private appCommon: AppCommon,
    public appStorage: AppStorage
  ) {
    this.accountInfo = this.appStorage.getAccountInfo();
    if (this.accountInfo.isSuperAdmin || this.accountInfo.roleType == 0 || this.accountInfo.roleType == 1 || this.accountInfo.roleType == 2 || this.accountInfo.roleType == 3 || this.accountInfo.roleType == 11) {
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
  }

  ngOnChanges() {
    let params = {};
    if (this.typeStatistic === "day") {
      params = { type: this.typeStatistic, projectId: this.projectId, statisticDate: this.appCommon.formatDateTime(this.statisticDate, "yyyy-MM-dd") };
    } else {
      params = { type: this.typeStatistic, projectId: this.projectId };
    }

    if (this.isAdmin) {
      this.getRevenueData(params);
      this.getCashFlowData(params);
    }

    this.getTransactionData(params);
    this.getCustomerData(params);
    this.getBikeData();
    this.getDockData();
    clearInterval(this.intervalGetRevenueData);
    clearInterval(this.intervalGetTransactionData);
    clearInterval(this.intervalGetCustomerData);
    clearInterval(this.intervalGetCashFlowData);
    clearInterval(this.intervalGetBikeData);
    clearInterval(this.intervalGetDockData);
    this.setInterval();
  }

  getRevenueData(params): void {
    this.troubleShootingService.getAnalyticRevenue(params).then((res) => {
      this.analyticRevenueData = res;
    });
  }

  getTransactionData(params): void {
    this.troubleShootingService.getAnalyticTransaction(params).then((res) => {
      this.analyticTransactionData = res;
    });
  }

  getCustomerData(params): void {
    this.troubleShootingService.getAnalyticCustomer(params).then((res) => {
      this.analyticCustomerData = res;
    });
  }

  getCashFlowData(params): void {
    this.troubleShootingService.getAnalyticCashFlow(params).then((res) => {
      this.analyticCashFlowData = res;
    });
  }

  getDockData(): void {
    let params = { projectId: this.projectId };

    this.troubleShootingService.getAnalyticDock(params).then((res) => {
      this.analyticDockData = res;
    });
  }

  getBikeData(): void {
    let params = { projectId: this.projectId };

    this.troubleShootingService.getAnalyticBike(params).then((res) => {
      this.analyticBikeData = res;
    });
  }

  setInterval() {
    let params = {};
    if (this.typeStatistic === "day") {
      params = { type: this.typeStatistic, projectId: this.projectId, statisticDate: this.appCommon.formatDateTime(this.statisticDate, "yyyy-MM-dd") };
    } else {
      params = { type: this.typeStatistic, projectId: this.projectId };
    }

    this.intervalGetRevenueData = setInterval(() => this.getRevenueData(params), 20000);
    this.intervalGetTransactionData = setInterval(() => this.getTransactionData(params), 20000);
    this.intervalGetCustomerData = setInterval(() => this.getCustomerData(params), 20000);
    this.intervalGetCashFlowData = setInterval(() => this.getCashFlowData(params), 20000);
    this.intervalGetBikeData = setInterval(() => this.getBikeData(), 20000);
    this.intervalGetDockData = setInterval(() => this.getDockData(), 20000);
  }

  ngOnDestroy(): void {
    if (this.intervalGetRevenueData) {
      clearInterval(this.intervalGetRevenueData);
    }

    if (this.intervalGetTransactionData) {
      clearInterval(this.intervalGetTransactionData);
    }

    if (this.intervalGetCustomerData) {
      clearInterval(this.intervalGetCustomerData);
    }

    if (this.intervalGetCashFlowData) {
      clearInterval(this.intervalGetCashFlowData);
    }

    if (this.intervalGetBikeData) {
      clearInterval(this.intervalGetBikeData);
    }

    if (this.intervalGetCashFlowData) {
      clearInterval(this.intervalGetDockData);
    }
  }
}