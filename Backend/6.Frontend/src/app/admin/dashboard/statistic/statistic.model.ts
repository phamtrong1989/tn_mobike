import { User } from 'src/app/admin/user/users/users.model';

export class Analytic {

  key: string;
  value: number;
}

export class AlertBike {
  id: number;
  type: number
  tile: string;
  message: string;
}

export class Log {
  id: number;
  action: string;
  object: string;
  objectId: number;
  objectType: number;
  createdDate: Date;
  systemUserId: number;
  type: number;

  systemUserObject: User;
}