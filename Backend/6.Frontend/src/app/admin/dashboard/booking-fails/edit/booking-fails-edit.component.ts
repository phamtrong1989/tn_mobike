import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { DashboardService } from '../../dashboard.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BookingFail, BookingFailCommand } from '../../dashboard.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-booking-fails-edit',
  templateUrl: './booking-fails-edit.component.html',
  providers: [
    DashboardService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class BookingFailsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<BookingFailsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: BookingFail,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public dashboardService: DashboardService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      note: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(4000)])],
    });
  }

  ngOnInit() {
    this.form.get('id').setValue(this.data.id);
    this.form.get('note').setValue(this.data.note);
    this.getData();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.dashboardService.bookingFailGetById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = rs.data;
        this.form.get('id').setValue(this.data.id);
        this.form.get('note').setValue(this.data.note);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save(status: number) {
    if (this.form.valid) {
      this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: "Bạn có muốn thực hiện hành động này không?",
        panelClass: 'dialog-confirmation',
      }).afterClosed().subscribe(result => {
        if (result) {
        this.isProcess.saveProcess = true;

         this.dashboardService.bookingAdd(this.form.value, status).then((res) => {
          if (res.status > 0) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
        }
      });
    }
  }


  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
