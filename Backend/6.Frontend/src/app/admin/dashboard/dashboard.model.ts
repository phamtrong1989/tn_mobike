import { Account } from "../customer/accounts/accounts.model";
import { Bike } from "../infrastructure/bikes/bikes.model";
import { Dock } from "../infrastructure/docks/docks.model";

export class EventSystem {

    id: bigint;
    name: string;
    title: string;
    projectId: number;
    accountId: number;
    stationId: number;
    transactionCode: string;
    bikeId: number;
    dockId: number;
    content: string;
    datas: string;
    isMultiple: boolean;
    type: number;
    group: number;
    warningType: number;
    createdDate: Date;
    updatedDate: Date;
    roleType: number;
    userId: number;
    isFlag: boolean;
    isProcessed: boolean;
    isShow: boolean;
    objectDatas: any[];
    isShowLess: boolean;
}

export class BookingFail {
    id: number;
    ticketPrepaidCode: string;
    ticketPrepaidId: number;
    projectId: number;
    investorId: number;
    updatedUser: number;
    bikeId: number;
    dockId: number;
    accountId: number;
    customerGroupId: number;
    transactionCode: string;
    createdDate: Date;
    status: number;
    chargeInAccount: number;
    chargeInSubAccount: number;
    note: string;
    prepaid_EndDate: Date;
    prepaid_EndTime: Date;
    prepaid_MinutesSpent: number;
    prepaid_StartDate: Date;
    prepaid_StartTime: Date;
    stationIn: number;
    ticketPriceId: number;
    ticketPrice_AllowChargeInSubAccount: boolean;
    ticketPrice_BlockPerMinute: number;
    ticketPrice_BlockPerViolation: number;
    ticketPrice_BlockViolationValue: number;
    ticketPrice_IsDefault: boolean;
    ticketPrice_LimitMinutes: number;
    ticketPrice_TicketType: number;
    ticketPrice_TicketValue: number;
    totalPrice: number;
    updatedDate: Date;

    dock: Dock;
    bike: Bike;
    account: Account;
}

export class BookingFailCommand {

    id: number;
    ticketPrepaidCode: string;
    ticketPrepaidId: number;
    projectId: number;
    investorId: number;
    updatedUser: number;
    bikeId: number;
    dockId: number;
    accountId: number;
    customerGroupId: number;
    transactionCode: string;
    createdDate: Date;
    status: number;
    chargeInAccount: number;
    chargeInSubAccount: number;
    note: string;
    prepaid_EndDate: Date;
    prepaid_EndTime: Date;
    prepaid_MinutesSpent: number;
    prepaid_StartDate: Date;
    prepaid_StartTime: Date;
    stationIn: number;
    ticketPriceId: number;
    ticketPrice_AllowChargeInSubAccount: boolean;
    ticketPrice_BlockPerMinute: number;
    ticketPrice_BlockPerViolation: number;
    ticketPrice_BlockViolationValue: number;
    ticketPrice_IsDefault: boolean;
    ticketPrice_LimitMinutes: number;
    ticketPrice_TicketType: number;
    ticketPrice_TicketValue: number;
    totalPrice: number;
    updatedDate: Date;


    deserialize(input: BookingFail): BookingFailCommand {
        if (input == null) {
            input = new BookingFail();
        }
        this.id = input.id;
        this.ticketPrepaidCode = input.ticketPrepaidCode;
        this.ticketPrepaidId = input.ticketPrepaidId;
        this.projectId = input.projectId;
        this.investorId = input.investorId;
        this.updatedUser = input.updatedUser;
        this.bikeId = input.bikeId;
        this.dockId = input.dockId;
        this.accountId = input.accountId;
        this.customerGroupId = input.customerGroupId;
        this.transactionCode = input.transactionCode;
        this.createdDate = input.createdDate;
        this.status = input.status;
        this.chargeInAccount = input.chargeInAccount;
        this.chargeInSubAccount = input.chargeInSubAccount;
        this.note = input.note;
        this.prepaid_EndDate = input.prepaid_EndDate;
        this.prepaid_EndTime = input.prepaid_EndTime;
        this.prepaid_MinutesSpent = input.prepaid_MinutesSpent;
        this.prepaid_StartDate = input.prepaid_StartDate;
        this.prepaid_StartTime = input.prepaid_StartTime;
        this.stationIn = input.stationIn;
        this.ticketPriceId = input.ticketPriceId;
        this.ticketPrice_AllowChargeInSubAccount = input.ticketPrice_AllowChargeInSubAccount;
        this.ticketPrice_BlockPerMinute = input.ticketPrice_BlockPerMinute;
        this.ticketPrice_BlockPerViolation = input.ticketPrice_BlockPerViolation;
        this.ticketPrice_BlockViolationValue = input.ticketPrice_BlockViolationValue;
        this.ticketPrice_IsDefault = input.ticketPrice_IsDefault;
        this.ticketPrice_LimitMinutes = input.ticketPrice_LimitMinutes;
        this.ticketPrice_TicketType = input.ticketPrice_TicketType;
        this.ticketPrice_TicketValue = input.ticketPrice_TicketValue;
        this.totalPrice = input.totalPrice;
        this.updatedDate = input.updatedDate;

        return this;
    }
}