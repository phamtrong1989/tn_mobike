import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ApiResponseData, ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AppCommon } from 'src/app/app.common';
import { GpsData, Station } from '../infrastructure/stations/stations.model';
import { Bike } from '../infrastructure/bikes/bikes.model';
import { Booking, BookingCancelCommand, BookingEndCommand, BookingOpenLockCommand } from '../transaction/bookings/bookings.model';
import { BookingFail, EventSystem } from './dashboard.model';
import { Account } from '../customer/accounts/accounts.model';
import { LogAdmin } from '../category/projects/projects.model';
import { User } from '../user/users/users.model';
@Injectable()
export class DashboardService {

    public url = "/api/work/Dashboard";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    stations(projectId: number) {
        return this.http.get<ApiResponseData<Station[]>>(this.url + "/Stations/" + projectId, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    bookings(projectId: number) {
        return this.http.get<ApiResponseData<Booking[]>>(this.url + "/bookings/" + projectId, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    bikeNotBookings(projectId: number) {
        return this.http.get<ApiResponseData<Bike[]>>(this.url + "/BikeNotBookings/" + projectId, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    bookingFails(projectId: number) {
        return this.http.get<ApiResponseData<BookingFail[]>>(this.url + "/BookingFails/" + projectId, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    bookingCalculate(id: number) {
        return this.http.get<ApiResponseData<Booking>>(this.url + "/bookingCalculate/" + id, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    getByIMEI(imei: string, time: string) {
        return this.http.get<ApiResponseData<GpsData[]>>(this.url + "/getByIMEI?imei=" + imei + "&start=" + time, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    bookingCheckOut(cm: BookingEndCommand) {
        return this.http.post<ApiResponseData<object>>(this.url + "/bookingCheckOut", cm).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    reCall(cm: any) {
        return this.http.post<ApiResponseData<object>>(this.url + "/reCall", cm).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bookingCancel(cm: BookingCancelCommand) {
        return this.http.post<ApiResponseData<object>>(this.url + "/BookingCancel", cm).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    openLock(cm: BookingOpenLockCommand) {
        return this.http.post<ApiResponseData<object>>(this.url + "/OpenLock", cm).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    tnGOHub() {
        return this.clientSettings.severMobile + '/hubs/tngohub';
    }

    eventSystemSearchPaged(params: any) {
        return this.http.get<ApiResponseData<EventSystem[]>>(this.url + "/EventSystemSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByAccount(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchByAccount", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    
    bikeSearchPaged(params: any) {
        return this.http.get<ApiResponseData<Bike[]>>(this.url + "/bikeSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bikeGetById(id: number) {
        return this.http.get<ApiResponseData<Bike>>(this.url + "/bikeGetById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    gpsByDock(params: any) {
        return this.http.get<ApiResponseData<GpsData[]>>(this.url + "/gpsByDock", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    logSearchPaged(params: any) {
        return this.http.get<ApiResponseData<LogAdmin[]>>(this.url + "/LogSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByUser(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchByUser", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    eventSystemChangeIsFlag(id: number, isFlag: boolean) {
        return this.http.post<ApiResponseData<object>>(this.url + "/EventSystemChangeIsFlag/"+id+"/"+isFlag, {}).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bookingFailSearchPaged(params: any) {
        return this.http.get<ApiResponseData<BookingFail[]>>(this.url + "/bookingFailSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bookingFailGetById(id: number) {
        return this.http.get<ApiResponseData<BookingFail>>(this.url + "/BookingFailGetById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bookingAdd(data: any, status: number) {
        return this.http.post<ApiResponseData<object>>(this.url + "/BookingAdd/"+status, data).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    
    dataInfoAccountOnline() {
        return this.http.get<ApiResponseData<any>>(this.url + "/DataInfoAccountOnline" ).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}