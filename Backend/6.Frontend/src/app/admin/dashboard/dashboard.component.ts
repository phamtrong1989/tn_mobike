import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AppClientSettings, AppSettings } from '../../app.settings';
import { ClientSettings, Settings } from '../../app.settings.model';
import { TranslateService } from '@ngx-translate/core';
import { AppStorage } from 'src/app/app.storage';
import { AccountData } from '../account/account.model';
import { AppCommon } from 'src/app/app.common';
import { DashboardService } from './dashboard.service';
import { GpsData, Station } from '../infrastructure/stations/stations.model';
import { Booking } from '../transaction/bookings/bookings.model';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { OpenLockComponent } from './open-lock/open-lock.component';
import { CloseComponent } from './close/close.component';
import { EndComponent } from './end/end.component';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { ReCallComponent } from './recall/recall.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BookingFail, EventSystem } from './dashboard.model';
import { Account } from '../customer/accounts/accounts.model';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { Bike } from '../infrastructure/bikes/bikes.model';
import { MatTableDataSource } from '@angular/material/table';
import { DashboardBikesMapComponent } from './bikes/map/bikes-map.component';
import { LogAdmin } from '../category/projects/projects.model';
import { RoleController } from '../user/role-controllers/role-controllers.model';
import { User } from '../user/users/users.model';
import { BookingFailsEditComponent } from './booking-fails/edit/booking-fails-edit.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [AppCommon, DashboardService]
})
export class DashboardComponent implements OnInit, OnDestroy {
  location: Location
  public settings: Settings;
  accountInfo: AccountData;
  viewType: number = 0;
  isMobile: boolean = false;

  isProcess = { dialogOpen: false, listDataTable: true, cancelBooking: false, endBooking: false, openLock: false, accountSearching: false, userSearching: false };
  heightTable: number = 900;
  heightTabTable: number = 210;
  iconSize: number = 20;
  stations: Station[] = [];
  searchStations: Station[] = [];
  bookings: Booking[] = [];
  searchBooking: Booking[] = [];
  searchBookingFail: BookingFail[] = [];

  currentStationMarker: StationMarker;
  currentBookingMarker: BookingMarker;
  currentBikeNotBooking: BikeNotBookingMarker;

  iconStation_DuXe: any = { url: '../assets/img/vendor/leaflet/station-success.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_ThuaXe: any = { url: '../assets/img/vendor/leaflet/station-duxe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_ThieuXe: any = { url: '../assets/img/vendor/leaflet/station-thieuXe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_HetXe: any = { url: '../assets/img/vendor/leaflet/station-hetxe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconBike: any = { url: '../assets/img/vendor/leaflet/bike-icon.png', scaledSize: { height: 20, width: 20 }, anchor: { x: 10, y: 10 } };
  iconBike_black: any = { url: '../assets/img/vendor/leaflet/bike-icon-black.png', scaledSize: { height: 20, width: 20 }, anchor: { x: 10, y: 10 } };


  categoryData: any;
  heightListBikeRight: number = 514;
  accountKey: FormControl = new FormControl("");
  isShowBooking: FormControl = new FormControl(true); 
  isShowStation: FormControl = new FormControl(true); 
  isBikeNotBooking: FormControl = new FormControl(false); 

  mapStyle: any = [
    { featureType: "road", stylers: [{ visibility: "on" }] },
    { featureType: "poi", stylers: [{ visibility: "off" }] },
    { featureType: "transit", stylers: [{ visibility: "on" }] },
    { featureType: "administrative", stylers: [{ visibility: "on" }] },
    { featureType: "administrative.locality", stylers: [{ visibility: "off" }] }
  ];
  displayedStationColumns = ["name", "cityId", "address", "info", "totalBike", "totalBikeGood", "totalBikeNotGood"];
  displayedEventSystemColumns = ["warningType", "createdDate", "type", "title", "isFlag"]
  displayedBookingColumns = ["isForgotrReturnTheBike", "isDockNotOpen", "transactionCode", "accountInfo", "stationIn", "bikeId", "ticketPriceId", "ticketPrice_TicketValue", "bikeCount", "totalMinutes", "totalPrice","totalPriceUT", "debtTotal", "action"];
  bikeDisplayedColumns = ["stt", "serialNumber", "dockId", "battery", "lockStatus", "connectionStatus", "type", "stationId", "bookingStatus", "action"];
  logDisplayedColumns = ["stt", "createdDate", "action", "object", "systemUserId"];
  //displayeBookingFailsColumns = ["transactionCode", "accountInfo", "stationIn", "bikeId", "ticketPriceId", "ticketPrice_Note", "ticketPrice_TicketValue", "totalMinutes","totalPrice", "status", "action"];

  stationParams: any = { key: "", investorId: "", cityId: "", districtId: "", status: "", projectId: "" };
  bikeParams: any = { pageIndex: 1, pageSize: 30, key: "", sortby: "", sorttype: "" };
  bookingParams: any = { key: "", investorId: "", plate: "" };
  eventSystemParams: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  logSystemParams: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  showMarkerParams: any = { booking: true, station: true }
  bikeNotBookingParams: any = { key: "", projectId: "", plate: "" };
  intevalStation: any;
  intevalBooking: any;
  intevalBookingFail: any;
  intevalPolylines: any;
  polylines: GpsData[];
  bikeNotBookings: Bike[] = [];
  listDataEventSystem: EventSystem[] = [];
  oldCurrentBookingMarker: BookingMarker;
  intevalAccountOnline: any;
  ctrlProjectId = new FormControl();
  systemUserKey = new FormControl();
  users: User[];
  private hubConnection: HubConnection;
  public clientSettings: ClientSettings;
  typeStatistic: string = 'day';
  accounts: Account[];
  bikeList: Bike[];
  logAdminList: LogAdmin[];
  stopEvent: boolean = false;
  roleControllers: RoleController[];
  statisticDate: Date = new Date();
  accountOnline: any;
  constructor(
    public appSettings: AppSettings,
    public appStorage: AppStorage,
    public dashboardService: DashboardService,
    public appCommon: AppCommon,
    public dialog: MatDialog,
    appClientSettings: AppClientSettings,
    private _notifications: NotificationsService

  ) {
    this.settings = this.appSettings.settings;
    this.accountInfo = this.appStorage.getAccountInfo();
    this.categoryData = appStorage.getCategoryData();
    this.searchBooking = [];
    this.searchStations = [];
    this.clientSettings = appClientSettings.settings
    this.roleControllers = this.categoryData.roleControllers.filter(x => x.id != 'Account' && x.id != 'Base`2' && x.id != 'Feedbacks' && x.id != 'Home' && x.id != 'InitData' && x.id != 'TicketPrices' && x.id != 'RoleGroups' && x.id != 'RoleAreas' && x.id != 'Public' && x.id != 'ReportInfrastructure')
    if (this.appStorage.getProjectId() == null || this.appStorage.getProjectId() == undefined) {
      this.appStorage.setProjectId(appStorage.getCategoryData().projects[0].id);
    }

    this.ctrlProjectId.setValue(this.appStorage.getProjectId());

    this.ctrlProjectId.valueChanges.subscribe(res => {
      this.appStorage.setProjectId(res);
      this.initData();
    });

    this.isShowBooking.valueChanges.subscribe(res => {
        this.location.bookingMarkers.forEach(x=>{
          x.isShow = res;
        });
    });

    this.isShowStation.valueChanges.subscribe(res => {
      console.log(res);
      this.location.stationMarkers.forEach(x=>{
        x.isShow = res;
      });
    });

    this.isBikeNotBooking.valueChanges.subscribe(res => {
      this.location.bikeNotBookings.forEach(x=>{
        x.isShow = res;
      });
    });
  }

  eventSystemSource: MatTableDataSource<EventSystem[]>;
  bikeSource: MatTableDataSource<Bike[]>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('eventSystemPaginator') eventSystemPaginator: MatPaginator;
  @ViewChild('bikePaginator') bikePaginator: MatPaginator;
  @ViewChild('logPaginator') logPaginator: MatPaginator;

  ngOnDestroy(): void {
    this.stopHub();
    if (this.intevalStation) {
      clearInterval(this.intevalStation);
    }
    if (this.intevalPolylines) {
      clearInterval(this.intevalPolylines);
    }
    if (this.intevalBooking) {
      clearInterval(this.intevalBooking);
    }
    if (this.intevalAccountOnline) {
      clearInterval(this.intevalAccountOnline);
    }
    // if (this.intevalBookingFail) {
    //   clearInterval(this.intevalBookingFail);
    // }
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
      this.isProcess.accountSearching = false;
      if (rs != "") {
        this.isProcess.accountSearching = true;
        this.dashboardService.searchByAccount({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.eventSystemParams.accountId = "";
          }
          this.accounts = rs.data;
          this.isProcess.accountSearching = false;
        });
      }
    },
      error => {
        this.isProcess.accountSearching = false;
      });
  }

  initData() {
    this.eventSystemParams.start = this.appCommon.dateAddDay(new Date(), -1);
    this.eventSystemParams.end = new Date();

    this.logSystemParams.start = this.appCommon.dateAddDay(new Date(), -1);
    this.logSystemParams.end = new Date()

    this.initSearchUser();

    this.initSearchAccount();



    var projectObject = this.appStorage.getCategoryData().projects.find(x => x.id == this.ctrlProjectId.value);
    if (projectObject != null) {
      this.location = {
        latitude: projectObject.lat,
        longitude: projectObject.lng,
        mapType: "street view",
        zoom: projectObject.defaultZoom,
        stationMarkers: [],
        bookingMarkers: [],
        bookingFailMarkers: [],
        bikeNotBookings: []
      }
    }
    else {
      this.location = {
        latitude: 21.0332535,
        longitude: 105.8345708,
        mapType: "street view",
        zoom: 15,
        stationMarkers: [],
        bookingMarkers: [],
        bookingFailMarkers: [],
        bikeNotBookings: []
      }
    }

    this.eventSystemSearch();
    this.logSystemSearch();
    this.bikeSearch();

    setTimeout(() => {
      this.getStations(true);
      this.getBookings(true);
    }, 2000);
  }

  ngOnInit() {
    this.initData();
    this.initHubData();
    this.setInteval();
  }

  gotoTop() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }

  getPolyline() {
    if(this.isMobile)
    {
      return;
    }

    if (this.currentBookingMarker != null) {
      this.dashboardService.getByIMEI(this.currentBookingMarker.booking.bike?.dock?.imei, this.appCommon.formatDateTime(this.currentBookingMarker.booking.startTime, 'yyyy-MM-dd HH:mm:ss')).then((res) => {
        if (this.oldCurrentBookingMarker?.booking?.id != this.currentBookingMarker?.booking?.id) {
          this.polylines = res.data;
          this.oldCurrentBookingMarker = this.currentBookingMarker;
        }
        else if (this.oldCurrentBookingMarker?.booking?.id == this.currentBookingMarker?.booking?.id && this.polylines.length < res.data.length) {
          this.polylines = res.data;
        }
        else {
          console.log("Không có tọa độ mới")
        }
      });
    }
    // else if (this.currentBookingFailMarker != null)
    // {
    //   this.dashboardService.getByIMEI(this.currentBookingFailMarker.bookingFail.bike?.dock?.imei, this.appCommon.formatDateTime(this.currentBookingFailMarker.bookingFail.createdDate, 'yyyy-MM-dd HH:mm:ss')).then((res) => {
    //     if (this.oldCurrentBookingFailMarker?.bookingFail?.id != this.currentBookingFailMarker?.bookingFail?.id) {
    //       this.polylines = res.data;
    //       this.oldCurrentBookingFailMarker = this.currentBookingFailMarker;
    //     }
    //     else if (this.oldCurrentBookingFailMarker?.bookingFail?.id == this.currentBookingFailMarker?.bookingFail?.id && this.polylines.length < res.data.length) {
    //       this.polylines = res.data;
    //     }
    //     else {
    //       console.log("Không có tọa độ mới")
    //     }
    //   });
    // }
    else {
      this.polylines = [];
    }
  }

  getStations(isFirst: boolean = false) {

    if(this.isMobile)
    {
      return;
    }

    this.dashboardService.stations(this.ctrlProjectId.value).then((res) => {
      this.stations = res.data;
      this.stationSearch();
      // Kiểm tra marker nào dữ liệu trả về không có thì xóa bỏ đi
      this.location.stationMarkers.forEach(element => {
        var ktTonTai = this.stations.find(x => x.id == element.station?.id);
        if (ktTonTai == null) {
          element.isEnable = false;
        }
      });
      // Kiểm tra nếu curent ko tồn tại nữa thì đóng set = null
      if (this.currentStationMarker != null && this.location.stationMarkers.find(x => this.currentStationMarker?.station?.id == x.station?.id && x.isEnable == true) == null) {
        this.currentStationMarker.isOpen = false;
        this.currentStationMarker = null;
      }
      // Cập nhật marker
      this.initIconSize();
      this.stations.forEach(station => {
        this.addStationMarker(station.lat, station.lng, station.name, false, station, true, true);
      });
    });
  }

  setInteval() {
    this.intevalStation = setInterval(() => this.getStations(), 10000);
    this.intevalBooking = setInterval(() => this.getBookings(), 10000);
    this.intevalPolylines = setInterval(() => this.getPolyline(), 10000);
    this.intevalAccountOnline = setInterval(() => this.getAccountOnline(), 10000);
  }

  getAccountOnline() {
    this.dashboardService.dataInfoAccountOnline().then((res) => {
      this.accountOnline = JSON.parse(res.data.data);
    });
  }

  getBookings(isFirst: boolean = false) {

    if(this.isMobile)
    {
      return;
    }
   
    this.dashboardService.bookings(this.ctrlProjectId.value).then((res) => {
      this.bookings = res.data;
      this.bookingSearch();
      // Kiểm tra marker nào dữ liệu trả về không có thì xóa bỏ đi
      this.location.bookingMarkers.forEach(element => {
        var ktTonTai = this.bookings.find(x => x.id == element.booking?.id);
        if (ktTonTai == null) {
          element.isEnable = false;
        }
      });

      if (this.currentBookingMarker != null && this.location.bookingMarkers.find(x => this.currentBookingMarker?.booking?.id == x.booking?.id && x.isEnable == true) == null) {
        this.currentBookingMarker.isOpen = false;
        this.currentBookingMarker = null;
        this.polylines = [];
      }
      // Cập nhật marker
      this.bookings.forEach(item => {
        this.addBookingMarker(item.bike?.dock?.lat, item.bike?.dock?.long, item.bike?.plate, this.iconBike, false, item, true, true);
      });

    });
  }

  removeMarkerBikeNotBooking() {
    this.location.bikeNotBookings.forEach(element => {
      element.isEnable = false;
    });
  }

  getBikeNotBooking(isFirst: boolean = false) {
    if (isFirst) {
      this.isProcess.listDataTable = true;
    }

    this.dashboardService.bikeNotBookings(this.ctrlProjectId.value).then((res) => {
      this.bikeNotBookings = res.data;
      setTimeout(() => {
        //this.bookingSearch();
        // Kiểm tra marker nào dữ liệu trả về không có thì xóa bỏ đi
        this.location.bikeNotBookings.forEach(element => {
          var ktTonTai = this.bikeNotBookings.find(x => x.id == element.bike.dock?.id);
          if (ktTonTai == null) {
            element.isEnable = false;
          }
        });

        if (this.currentBikeNotBooking != null && this.location.bikeNotBookings.find(x => this.currentBikeNotBooking?.bike.dock?.id == x.bike.dock?.id && x.isEnable == true) == null) {
          this.currentBikeNotBooking.isOpen = false;
          this.currentBikeNotBooking = null;
          this.polylines = [];
        }
        // Cập nhật marker
        this.bikeNotBookings.forEach(item => {
          this.addBikeNotBookingMarker(item?.dock?.lat, item?.dock?.long, item?.plate, this.iconBike_black, false, item, true, true);
        });
      }, 300);

      this.isProcess.listDataTable = false;
    });
  }

  selectStationMarker(marker: StationMarker) {
    if (!marker.isEnable) {
      marker.isOpen = true;
      return;
    }
    this.viewType = 3;
    if (this.currentBookingMarker != null) {
      this.currentBookingMarker.isOpen = false;
    }
    // if (this.currentBookingFailMarker != null) {
    //   this.currentBookingFailMarker.isOpen = false;
    // }
    // this.currentBookingFailMarker = null;

    this.currentBookingMarker = null;

    this.polylines = [];
    if (this.currentStationMarker != null) {
      this.currentStationMarker.isOpen = false;
    }
    this.currentStationMarker = marker;
    this.location.latitude = marker.lat;
    this.location.longitude = marker.lng;
    marker.isOpen = true;
  }

  selectBookingMarker(marker: BookingMarker) {
    this.viewType = 0;
    if (this.currentStationMarker != null) {
      this.currentStationMarker.isOpen = false;
    }

    // if (this.currentBookingFailMarker != null) {
    //   this.currentBookingFailMarker.isOpen = false;
    // }
    // this.currentBookingFailMarker = null;

    if (this.currentBookingMarker != null) {
      this.currentBookingMarker.isOpen = false;
    }

    this.currentStationMarker = null;

    this.currentBookingMarker = marker;
    this.location.latitude = marker.lat;
    this.location.longitude = marker.lng;
    marker.isOpen = true;
    this.getPolyline();
  }

  selectStation(station: Station) {
    var getMarker = this.location.stationMarkers.find(x => x.station.id == station.id);
    if (getMarker != null) {
      this.selectStationMarker(getMarker);
    }
  }

  openViewmapDialog(data: Bike) {
    this.dialog.open(DashboardBikesMapComponent, {
      data: data,
      panelClass: 'dialog-full',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.bikeSearchPage();
    });
  }

  openViewmapDialogById(bikeId: number) {
    if (bikeId > 0) {
      this.dialog.open(DashboardBikesMapComponent, {
        data: { id: bikeId },
        panelClass: 'dialog-full',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
      });
    }
  }

  selectBooking(booking: Booking) {
    if (booking != null) {
      var getMarker = this.location.bookingMarkers.find(x => x.booking?.id == booking?.id);
      if (getMarker != null) {
        this.selectBookingMarker(getMarker);
      }
    }
  }

  addStationMarker(lat: number, lng: number, label: string, isOpen: boolean, station: Station, isEnable: boolean, isShow: boolean) {
    var isSelect = false;
    var ktTT = this.location.stationMarkers.find(x => x.station?.id == station.id);
    var iconUrl = this.setupStationIcon(station);
    if (ktTT == null) {
      this.location.stationMarkers.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        station,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.station = station;
      ktTT.isEnable = true;
      if (this.currentStationMarker != null && this.currentStationMarker.station?.id == station.id) {
        this.currentStationMarker = ktTT;
      }
    }
  }

  changeIconMarker() {
    this.location.stationMarkers.forEach(element => {
      var newX = {
        url: element.iconUrl.url,
        scaledSize: {
          width: this.iconSize,
          height: this.iconSize
        }
        , anchor: { x: 10, y: 10 }
      };
      element.iconUrl = newX;
    });

    this.location.bookingMarkers.forEach(element => {
      var newX = {
        url: element.iconUrl.url,
        scaledSize: {
          width: this.iconSize,
          height: this.iconSize
        }, anchor: { x: 10, y: 10 }
      };
      element.iconUrl = newX;
    });
  }

  setupStationIcon(station: Station) {
    var data = this.iconStation_DuXe;
    if (station.totalBike <= 3 && station.totalBike > 0) {
      data = this.iconStation_ThieuXe;
    }
    else if (station.totalBike == 0) {
      data = this.iconStation_HetXe;
    }
    else if ((station.totalBike - 3) >= station.spaces) {
      data = this.iconStation_ThuaXe;
    }
    return data;
  }

  addBookingMarker(lat: number, lng: number, label: string, iconUrl: string, isOpen: boolean, booking: Booking, isEnable: boolean, isShow: boolean) {
    var isSelect = false;
    var ktTT = this.location.bookingMarkers.find(x => x.booking?.id == booking.id);
    if (ktTT == null) {

      this.location.bookingMarkers.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        booking,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.booking = booking;
      ktTT.isEnable = true;
      if (this.currentBookingMarker != null && this.currentBookingMarker.booking?.id == booking.id) {
        this.currentBookingMarker = ktTT;
      }
    }
  }

  addBikeNotBookingMarker(lat: number, lng: number, label: string, iconUrl: string, isOpen: boolean, bike: Bike, isEnable: boolean, isShow: boolean) {
    var isSelect = false;
    if (lat <= 0 || lng <= 0) {
      return;
    }
    var ktTT = this.location.bikeNotBookings.find(x => x.bike.dock?.id == bike.dock?.id);
    if (ktTT == null) {
      this.location.bikeNotBookings.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        bike,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.bike = bike;
      ktTT.isEnable = true;
      if (this.currentBikeNotBooking != null && this.currentBikeNotBooking.bike?.dock?.id == bike?.dock?.id) {
        this.currentBikeNotBooking = ktTT;
      }
    }
  }

  centerChange(event: any) {
  }

  zoomChange(event: any) {
    this.location.zoom = event;
    this.initIconSize();
    this.changeIconMarker();
  }

  initIconSize() {
    if (this.location.zoom >= 15) {
      this.iconSize = 20;
    }
    else if (this.location.zoom == 14) {
      this.iconSize = 15;
    }
    else if (this.location.zoom == 13) {
      this.iconSize = 14;
    }
    else if (this.location.zoom == 12) {
      this.iconSize = 13;
    }
    else if (this.location.zoom == 11) {
      this.iconSize = 12;
    }
    else if (this.location.zoom == 10) {
      this.iconSize = 11;
    }
    else if (this.location.zoom == 9) {
      this.iconSize = 10;
    }
    else if (this.location.zoom == 8) {
      this.iconSize = 8;
    }
    else if (this.location.zoom == 7) {
      this.iconSize = 7;
    }
    else if (this.location.zoom == 6) {
      this.iconSize = 6;
    }
    else {
      this.iconSize = 5;
    }
  }

  boundsChange(event: any) {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    this.isMobile = window.innerWidth <= 500;

    if (this.fix_scroll_table_rec.nativeElement != null) {
      this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - 410;
      this.heightListBikeRight = this.heightTable - 74;
    }

    if (this.fix_scroll_table_station && this.fix_scroll_table_station.nativeElement != null) {
      this.heightTabTable = 306;
    }

  }

  matTabGroupChange() {

  }

  public initHubData() {
    var data = this.appStorage.getAccountInfo();
    var strToken = "";
    if (data != null) {
      try {
        strToken = data.tokenHub;
        let builder = new HubConnectionBuilder();
        this.hubConnection = builder.withUrl(this.clientSettings.serverAPI + '/hubs/eventSystem', { accessTokenFactory: async () => strToken }).build();
        this.hubConnection.start();
        this.hubConnection.on('ReceiveMessageEventSystemSendMessage', (userId, message: EventSystem) => {
          message.objectDatas = JSON.parse(message.datas);
          if(message.projectId == 0 || (message.projectId > 0 && message.projectId == this.ctrlProjectId.value))
          {
            if (this.listDataEventSystem.find(x => x.id == message.id) == null) {
              if (message.warningType == 0) {
                // this.sendNotify(NotificationType.Info, message.content);
              }
              else if (message.warningType == 1) {
                this.sendNotify(NotificationType.Warn, message.content);
              }
              else if (message.warningType == 2) {
                this.sendNotify(NotificationType.Error, message.content);
              }
              if (this.eventSystemParams.pageIndex == 1 || this.stopEvent) {
                this.listDataEventSystem = [message].concat(this.listDataEventSystem);
              }
            }
          }
        });
      }
      catch {
      }
    }
  }

  stopHub() {
    if (this.hubConnection) {
      this.hubConnection.stop();
      this.hubConnection = undefined;
    }
  }

  sendNotify(type: NotificationType, content: string) {
    var data = {
      type: type,
      title: "Thông báo",
      content: content,
      timeOut: 15000,
      showProgressBar: true,
      pauseOnHover: true,
      clickToClose: true,
      animate: 'fromRight'
    };
    this._notifications.create("Thông báo", content, type, data)
  }

  viewChange($event: boolean, type: number) {
    if (type == 0) {
      this.location.stationMarkers.forEach(element => {
        element.isShow = $event;
      });
    }
    else if (type == 1) {
      this.location.bookingMarkers.forEach(element => {
        element.isShow = $event;
      });
    }
  }

  stationSearchModelChange($event: number, type: number) {
    if (type == 0) {
      this.stationParams.investorId = $event;
    }
    else if (type == 1) {
      this.stationParams.cityId = $event;
    }
    else if (type == 2) {
      this.stationParams.projectId = $event;
    }
    this.stationSearch();
  }

  stationSearch() {
    var list = this.stations;
    if (this.stationParams.projectId != "" && this.stationParams.projectId != null) {
      list = this.stations.filter(x => x.projectId == this.stationParams.projectId);
    }
    if (this.stationParams.investorId != "" && this.stationParams.investorId != null) {
      list = this.stations.filter(x => x.investorId == this.stationParams.investorId);
    }
    if (this.stationParams.cityId != "" && this.stationParams.cityId != null) {
      list = this.stations.filter(x => x.cityId == this.stationParams.cityId);
    }
    if (this.stationParams.districtId != "" && this.stationParams.districtId != null) {
      list = this.stations.filter(x => x.districtId == this.stationParams.districtId);
    }
    if (this.stationParams.key != "" && this.stationParams.key != null) {
      list = this.stations.filter(x => x.name.toLowerCase().indexOf(this.stationParams.key.toLowerCase()) !== -1)
    }
    this.searchStations = list;
  }

  bookingSearchModelChange($event: number, type: number) {
    if (type == 0) {
      this.stationParams.investorId = $event;
    }
    else if (type == 1) {
      this.stationParams.plate = $event;
    }
    this.bookingSearch();
  }

  bookingSearch() {
    var list = this.bookings;
    if (this.bookingParams.investorId != "" && this.bookingParams.investorId != null) {
      list = this.bookings.filter(x => x.investorId == this.bookingParams.investorId);
    }
    if (this.bookingParams.plate != "" && this.bookingParams.plate != null) {
      list = this.bookings.filter(x => (x.bike.plate.replace("-","").replace(".","").replace(" ","").toLowerCase().indexOf(this.bookingParams.plate.replace("-","").replace(".","").replace(" ","").toLowerCase()) !== -1) || (x.bike?.dock?.imei.toLowerCase().indexOf(this.bookingParams.plate.toLowerCase()) !== -1));
    }
    if (this.bookingParams.key != "" && this.bookingParams.key != null) {
      list = this.bookings.filter(x => x.transactionCode.toLowerCase().indexOf(this.bookingParams.key.toLowerCase()) !== -1);
    }
    if (this.bookingParams.accountKey != "" && this.bookingParams.accountKey != null) {
      var k = this.bookingParams.accountKey.toLowerCase();
      list = this.bookings.filter(x => x.account?.fullName?.toLowerCase().indexOf(k) !== -1 || x.account?.phone?.toLowerCase().indexOf(k) !== -1);
    }
    if (this.bookingParams.isDockNotOpen != "" && this.bookingParams.isDockNotOpen != null) {
      list = this.bookings.filter(x => x.isDockNotOpen.toString() == this.bookingParams.isDockNotOpen);
    }

    this.searchBooking = list;
  }

  eventSystemSearchPage(): void {
    this.isProcess.listDataTable = true;
    if (this.eventSystemParams.start) {
      this.eventSystemParams.start = this.appCommon.formatDateTime(this.eventSystemParams.start, "yyyy-MM-dd");
    }
    if (this.eventSystemParams.end) {
      this.eventSystemParams.end = this.appCommon.formatDateTime(this.eventSystemParams.end, "yyyy-MM-dd");
    }
    this.dashboardService.eventSystemSearchPaged(this.eventSystemParams).then((res) => {
      res.data.forEach(x => {
        try {
          x.objectDatas = JSON.parse(x.datas);
        }
        catch
        {
          x.objectDatas = [];
        }

      });
      if( this.eventSystemPaginator)
      {
        this.listDataEventSystem = res.data;

        this.eventSystemPaginator.length = res.count;
        this.eventSystemPaginator.pageIndex = res.pageIndex - 1;
        this.eventSystemPaginator.pageSize = this.eventSystemParams.pageSize;
        this.isProcess.listDataTable = false;
      }
    });
  }

  eventSystemHandlePage(e: any) {
    if (e.pageSize != this.eventSystemParams.pageSize) {
      this.eventSystemParams.pageIndex = 1;
    }
    else {
      this.eventSystemParams.pageIndex = e.pageIndex + 1;
    }
    this.eventSystemParams.pageSize = e.pageSize;
    this.eventSystemSearchPage();
  }

  eventSystemSearch() {
    this.eventSystemParams.pageIndex = 1;
    console.log(this.eventSystemParams);
    this.eventSystemSearchPage();
  }

  openLock() {
    if (this.currentBookingMarker?.booking != null) {
      this.dialog.open(OpenLockComponent, {
        data: this.currentBookingMarker.booking,
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
      });
    }
  }

  cancelBooking() {
    if (this.currentBookingMarker?.booking != null) {
      this.dialog.open(CloseComponent, {
        data: this.currentBookingMarker.booking,
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
      });
    }
  }

  endBooking() {
    if (this.currentBookingMarker?.booking != null) {
      this.dialog.open(EndComponent, {
        data: this.currentBookingMarker.booking,
        disableClose: true,
        autoFocus: false,
      }).afterClosed().subscribe(outData => {
      });
    }
  }

  reCall() {
    if (this.currentBookingMarker?.booking != null) {
      this.dialog.open(ReCallComponent, {
        data: this.currentBookingMarker.booking,
        disableClose: true,
        autoFocus: false,
        panelClass: 'dialog-600'
      }).afterClosed().subscribe(outData => {
      });
    }
  }

  getToday() {
    return new Date();
  }

  getMonday() {
    var d = new Date();
    var first = d.getDate() - d.getDay() + 1;
    return new Date(d.setDate(first));
  }

  getFirstDayOfMonth() {
    return new Date(new Date().getFullYear(), new Date().getMonth(), 1)
  }

  getFirstDayOfQuarter() {
    var currentMonth = new Date().getMonth();
    var currentQuarter = Math.ceil(currentMonth / 3);
    var firstMonthOfQuarter = currentQuarter * 3 - 3;
    return new Date(new Date().getFullYear(), firstMonthOfQuarter, 1)
  }

  getFirstDayOfYear() {
    return new Date(new Date().getFullYear(), 0, 1)
  }

  bikeSearchPage(): void {
    this.isProcess.listDataTable = true;
    this.dashboardService.bikeSearchPaged(this.bikeParams).then((res) => {
      if(this.bikePaginator)
      {
        this.bikeList = res.data;
        this.bikePaginator.length = res.count;
        this.bikePaginator.pageIndex = res.pageIndex - 1;
        this.bikePaginator.pageSize = this.bikeParams.pageSize;
        this.isProcess.listDataTable = false;
      }
    });
  }

  bikeSearch() {
    this.bikeParams.pageIndex = 1;
    this.bikeSearchPage();
  }

  bikeHandleSortData(e: any) {
    this.bikeParams.pageIndex = 1;
    this.bikeParams.sortby = e.direction ? e.active : '';
    this.bikeParams.sorttype = e.direction;
    this.bikeSearchPage();
  }

  bikeHandlePage(e: any) {
    if (e.pageSize != this.bikeParams.pageSize) {
      this.bikeParams.pageIndex = 1;
    }
    else {
      this.bikeParams.pageIndex = e.pageIndex + 1;
    }
    this.bikeParams.pageSize = e.pageSize;
    this.bikeSearchPage();
  }

  logSearchPage(): void {
    this.isProcess.listDataTable = true;
    if (this.logSystemParams.start) {
      this.logSystemParams.start = this.appCommon.formatDateTime(this.logSystemParams.start, "yyyy-MM-dd");
    }
    if (this.logSystemParams.end) {
      this.logSystemParams.end = this.appCommon.formatDateTime(this.logSystemParams.end, "yyyy-MM-dd");
    }
    this.dashboardService.logSearchPaged(this.logSystemParams).then((res) => {
      if(this.logPaginator)
      {
        this.logAdminList = res.data;
        this.logPaginator.length = res.count;
        this.logPaginator.pageIndex = res.pageIndex - 1;
        this.logPaginator.pageSize = this.logSystemParams.pageSize;
        this.isProcess.listDataTable = false;
      }
    });
  }

  initSearchUser() {
    this.systemUserKey.valueChanges.subscribe(rs => {
      this.isProcess.userSearching = false;
      if (rs != "") {
        this.isProcess.userSearching = true;
        this.dashboardService.searchByUser({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.logSystemParams.systemUserId = "";
          }
          this.users = rs.data;
          this.isProcess.userSearching = false;
        });
      }
    },
      error => {
        this.isProcess.userSearching = false;
      });
  }

  logHandlePage(e: any) {
    if (e.pageSize != this.logSystemParams.pageSize) {
      this.logSystemParams.pageIndex = 1;
    }
    else {
      this.logSystemParams.pageIndex = e.pageIndex + 1;
    }
    this.logSystemParams.pageSize = e.pageSize;
    this.logSearchPage();
  }

  logSystemSearch() {
    this.logSystemParams.pageIndex = 1;
    this.logSearchPage();
  }

  eventOnShow(element: EventSystem) {
    this.listDataEventSystem.forEach(x => {
      x.isShow = false;
    });
    element.isShow = true;
  }

  eventSystemChangeIsFlag(id: number, val: boolean) {
    this.dashboardService.eventSystemChangeIsFlag(id, val).then(rs => { });
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('fix_scroll_table_station', { static: false }) fix_scroll_table_station: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}

interface BookingMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  isSelect: boolean;
  booking: Booking;
  isEnable: boolean;
  isShow: boolean;
}

interface StationMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  isSelect: boolean;
  station: Station;
  isEnable: boolean;
  isShow: boolean;
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  stationMarkers: StationMarker[];
  bookingMarkers: BookingMarker[];
  bookingFailMarkers: BookingFailMarker[],
  bikeNotBookings: BikeNotBookingMarker[]
}


interface BookingFailMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  isSelect: boolean;
  bookingFail: BookingFail;
  isEnable: boolean;
  isShow: boolean;
}

interface BikeNotBookingMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  isSelect: boolean;
  bike: Bike;
  isEnable: boolean;
  isShow: boolean;
}
