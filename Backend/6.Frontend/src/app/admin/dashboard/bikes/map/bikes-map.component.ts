import { Component, OnInit, Inject, HostListener, AfterViewInit, OnDestroy } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { Subject, ReplaySubject } from 'rxjs';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { ClientSettings, PullData } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AnyNaptrRecord } from 'dns';
import { CustomerGroup } from 'src/app/admin/category/projects/projects.model';
import { TranslateService } from '@ngx-translate/core';
import { GpsData, Station } from 'src/app/admin/infrastructure/stations/stations.model';
import { DashboardService } from '../../dashboard.service';
import { Bike } from 'src/app/admin/infrastructure/bikes/bikes.model';

@Component({
  selector: 'app-dashboard-bikes-map',
  templateUrl: './bikes-map.component.html',
  providers: [
    DashboardService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class DashboardBikesMapComponent implements OnInit, AfterViewInit, OnDestroy {
  location: Location;
  mapStyle: any = [
    { featureType: "road", stylers: [{ visibility: "on" }] },
    { featureType: "poi", stylers: [{ visibility: "off" }] },
    { featureType: "transit", stylers: [{ visibility: "on" }] },
    { featureType: "administrative", stylers: [{ visibility: "on" }] },
    { featureType: "administrative.locality", stylers: [{ visibility: "off" }] }
  ];
  polylines: GpsData[];
  dataItems: GpsData[];
  iconStart: any = { url: '../assets/img/vendor/leaflet/icon-start.png', scaledSize: { height: 20, width: 20 } };
  iconEnd: any = { url: '../assets/img/vendor/leaflet/icon-end.png', scaledSize: { height: 20, width: 20 } };
  iconStation: any = { url: '../assets/img/vendor/leaflet/station-success.png', scaledSize: { height: 20, width: 20 }, anchor: { x: 10, y: 10 } };
  iconBike: any = { url: '../assets/img/vendor/leaflet/bike-icon.png', scaledSize: { height: 20, width: 20 } };
  iconSize: number = 20;
  iconStation_DuXe: any = { url: '../assets/img/vendor/leaflet/station-success.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_ThuaXe: any = { url: '../assets/img/vendor/leaflet/station-duxe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_ThieuXe: any = { url: '../assets/img/vendor/leaflet/station-thieuXe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_HetXe: any = { url: '../assets/img/vendor/leaflet/station-hetxe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };

  public clientSettings: ClientSettings;
  protected _onDestroy = new Subject<void>();
  public form: FormGroup;
  public categoryData: PullData;
  public customerGroups: CustomerGroup[];
  public heightScroll: number = 828;
  public heightMap: number = 828;
  public heightTable: number = 828;
  currentMarker: Marker;
  stations: Station[];
  transactionTranslate: any;
  displayedColumns = ["time", "location", "station"];
  params: any = { date: "", startTime: "", endTime: "", dateX: "", id: 0 };
  viewType: number;
  intevalCurrentBike: any;
  intevalStation: any;

  currentLocation: Marker;
  isProcess = { saveProcess: false, deleteAppointments: false, nationalitySearching: false, dialogOpen: false, initData: false, listDataTable: false };
  constructor(
    public dialogRef: MatDialogRef<DashboardBikesMapComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: Bike,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public dashboardService: DashboardService,
    public appCommon: AppCommon,
    private translate: TranslateService,
    appClientSettings: AppClientSettings,
  ) {
    this.clientSettings = appClientSettings.settings
    this.categoryData = this.appStorage.getCategoryData();
    this.initTransactionTranslate();
  }


  ngOnInit() {
    this.params.id = this.data.id;
    this.params.dateX = new Date();
    this.params.startTime = "05:00";
    this.params.endTime = this.appCommon.formatDateTime(new Date(), "HH:mm");

    this.location = {
      latitude: 0,
      longitude: 0,
      mapType: "street view",
      zoom: 18,
      markers: [],
      currentLocation: this.currentLocation,
      stationMarkers: []
    }

    this.getData(true, x => {
      this.getStations(true);
    });

    this.intevalCurrentBike = setInterval(() => this.getData(false, null), 10000);
    this.intevalStation = setInterval(() => this.getStations(false), 10000);
  }

  getData(isFirst: boolean = true, callback: any) {
    if (isFirst) {
      this.isProcess.initData = true;
    }

    this.dashboardService.bikeGetById(this.data.id).then(rs => {
      if (rs.data) {
         this.data = rs.data;
        if (this.location.currentLocation != null) {
          this.location.currentLocation.lat = this.data.dock?.lat;
          this.location.currentLocation.lng = this.data.dock?.long;
          this.location.currentLocation.data = this.data;
        }
        else {
          this.location.currentLocation = { lat: this.data.dock?.lat, lng: this.data.dock?.long, iconUrl: this.iconBike, isOpen: true, data: this.data, label: "Không label" }
        }

        if (this.data.lat > 0 && this.data.long > 0 && isFirst) {
          this.location.latitude = this.data.dock?.lat;
          this.location.longitude = this.data.dock?.long;
        }

        this.currentLocation = this.location.currentLocation;

        if (callback != null) {
          callback();
        }
      }


      this.isProcess.initData = false;
    });
  }

  openViewmapDialog() {

  }

  getStations(isFirst: boolean = false) {
    if(this.data.projectId)
    {
      this.dashboardService.stations(this.data.projectId).then((res) => {
        this.stations = res.data;
        this.initIconSize();
        this.stations.forEach(station => {
          this.addStationMarker(station.lat, station.lng, station.name, false, station, true, true);
        });
      });
    }
  }

  addStationMarker(lat: number, lng: number, label: string, isOpen: boolean, station: Station, isEnable: boolean, isShow: boolean) {
    var ktTT = this.location.stationMarkers.find(x => x.station?.id == station.id);
    var iconUrl = this.setupStationIcon(station);
    if (ktTT == null) {
      this.location.stationMarkers.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        station
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.station = station;
    }
  }

  setupStationIcon(station: Station) {
    var data = this.iconStation_DuXe;
    if (station.totalBike <= 3 && station.totalBike > 0) {
      data = this.iconStation_ThieuXe;
    }
    else if (station.totalBike == 0) {
      data = this.iconStation_HetXe;
    }
    else if ((station.totalBike - 3) >= station.spaces) {
      data = this.iconStation_ThuaXe;
    }
    return data;
  }

  initIconSize() {
    if (this.location.zoom >= 15) {
      this.iconSize = 20;
    }
    else if (this.location.zoom == 14) {
      this.iconSize = 15;
    }
    else if (this.location.zoom == 13) {
      this.iconSize = 14;
    }
    else if (this.location.zoom == 12) {
      this.iconSize = 13;
    }
    else if (this.location.zoom == 11) {
      this.iconSize = 12;
    }
    else if (this.location.zoom == 10) {
      this.iconSize = 11;
    }
    else if (this.location.zoom == 9) {
      this.iconSize = 10;
    }
    else if (this.location.zoom == 8) {
      this.iconSize = 8;
    }
    else if (this.location.zoom == 7) {
      this.iconSize = 7;
    }
    else if (this.location.zoom == 6) {
      this.iconSize = 6;
    }
    else {
      this.iconSize = 5;
    }
  }

  search() {
    this.isProcess.listDataTable = true;
    this.params.date = this.appCommon.formatDateTime(this.params.dateX, "yyyy-MM-dd");
    this.dashboardService.gpsByDock(this.params).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.dataItems = rs.data;
        this.polylines = rs.data.filter(x => x.lat > 0 && x.long > 0);

        if (this.polylines != null && this.polylines.length > 0) {
          this.location = {
            latitude: this.polylines[0].lat,
            longitude: this.polylines[0].long,
            mapType: "street view",
            zoom: 18,
            markers: [],
            currentLocation: this.location.currentLocation,
            stationMarkers: this.location.stationMarkers
          }

          for (var i = 0; i < this.polylines.length; i++) {
            if (i == 0) {
              this.addMarker(this.polylines[0].lat, this.polylines[0].long, this.iconEnd, true, this.polylines[i]);
            }
            else if (i == this.polylines.length - 1) {
              this.addMarker(this.polylines[this.polylines.length - 1].lat, this.polylines[this.polylines.length - 1].long, this.iconStart, true, this.polylines[i]);
            }
            else {
              this.addMarker(this.polylines[i].lat, this.polylines[i].long, this.iconStation, false, this.polylines[i]);
            }
          }

        }
        this.isProcess.listDataTable = false;
      }
      else {
        this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
      }
    });
  }

  selectRow(gps: GpsData) {
    this.currentMarker = null;
    var kt = this.location.markers.find(x => x.data.id == gps.id);
    if (kt == null) {
      return;
    }
    this.currentMarker = kt;
    this.location.latitude = kt.lat;
    this.location.longitude = kt.lng;
    this.currentMarker.isOpen = true;
  }

  selectMarker(marker: Marker) {
    this.location.latitude = marker.lat;
    this.location.longitude = marker.lng;
    this.currentMarker = marker;
    marker.isOpen = true;
  }

  zoomChange(event: any) {
    this.location.zoom = event;
  }

  getFullUrl(url: string) {
    return this.clientSettings.serverAPI + url;
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close(1);
    }
  }

  ngOnDestroy() {
    if (this.intevalCurrentBike) {
      clearInterval(this.intevalCurrentBike);
    }
    if (this.intevalStation) {
      clearInterval(this.intevalStation);
    }
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  public openCustomersEditDialog() {

  }

  matTabGroupChange(e: AnyNaptrRecord) {

  }

  fixHeightScroll() {
    let getTop = document.getElementById("fix_detail").getBoundingClientRect().top;
    this.heightScroll = window.innerHeight - getTop - 30;
    this.heightMap = this.heightScroll - 130;
    this.heightTable = this.heightMap;
    this.viewType = 0;
  }

  addMarker(lat: number, lng: number, iconUrl: string, isOpen: boolean, data: any) {
    this.location.markers.push({
      lat,
      lng,
      iconUrl,
      isOpen,
      data,
      label: "Không label"
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.fixHeightScroll();
    }, 200);
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightScroll();
  }

  initTransactionTranslate() {
    this.translate.get(['transaction'])
      .subscribe(translations => {
        this.transactionTranslate = translations['transaction'];
      });
  }
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  markers: Marker[];
  currentLocation: Marker;
  stationMarkers: StationMarker[];
}

interface Marker {
  lat: number;
  lng: number;
  iconUrl: string;
  isOpen: boolean;
  data: any;
  label: string;
}

interface StationMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  station: Station;
}