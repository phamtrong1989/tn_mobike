import { StatisticComponent } from './statistic/statistic.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SharedModule } from '../../shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxCurrencyModule } from 'ngx-currency';
import { CalendarModule } from 'angular-calendar';
import { AgmCoreModule } from '@agm/core';
import { OpenLockComponent } from './open-lock/open-lock.component';
import { CloseComponent } from './close/close.component';
import { EndComponent } from './end/end.component';
import { CheckRolesDirective } from 'src/app/app.data';
import { ReCallComponent } from './recall/recall.component';
import { DashboardBikesMapComponent } from './bikes/map/bikes-map.component';
import { MatNativeDateModule } from '@angular/material/core';
import { BookingFailsEditComponent } from './booking-fails/edit/booking-fails-edit.component';

export const routes = [
  { path: '', component: DashboardComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    NgxCurrencyModule,
    CalendarModule,
    AgmCoreModule,
    MatNativeDateModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    })
  ],
  declarations: [
    DashboardComponent,
    OpenLockComponent,
    CloseComponent,
    EndComponent,
    ReCallComponent,
    StatisticComponent,
    DashboardBikesMapComponent,
    BookingFailsEditComponent
  ],
  entryComponents: [
    OpenLockComponent,
    CloseComponent,
    EndComponent,
    ReCallComponent,
    StatisticComponent,
    DashboardBikesMapComponent,
    BookingFailsEditComponent
  ]
})
export class DashboardModule { }