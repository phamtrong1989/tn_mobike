
export class LoginCommand {
  username: string;
  password: string;  
}
export class DataRoleAction {
  controllerName: string;
  areaName: string;  
  actionName: string;  
  id: number;  
}
export class AccountData {
  id: number;
  token: string;  
  tokenHub: string;  
  displayName: string;  
  email: string;  
  phoneNumber: string;  
  roleName: string;  
  avartar: string;
  roleType: number;
  roleActions: DataRoleAction[];
  isSuperAdmin: boolean;
  loginProvider: string;
}

export class AccountProfilesCommand {
  displayName: string;  
  email: string;  
  phoneNumber: string;  
  avatar: string;
  Education: string;
  birthday: Date;
  gender: number;
  nationalityId: number;
  address: string;
  color: string;
}

export class AccountProfilesDTO {
  displayName: string;  
  email: string;  
  phoneNumber: string; 
  avatar: string;  
}

export class ChangePasswordCommand {
  newPassword: string;
  oldPassword: string;  
  confirmPassword: string;  
}