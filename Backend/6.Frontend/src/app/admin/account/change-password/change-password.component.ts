import { Component, OnInit, Inject, AfterViewInit, ChangeDetectorRef, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl, ValidationErrors } from '@angular/forms';

import { AccountService } from '../account.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { CustomValidator } from 'src/app/theme/utils/app-validators';
import { AccountProfilesDTO } from '../account.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  providers: [AccountService, AppCommon]
})
export class ChangePasswordComponent implements OnInit, AfterViewInit {

  public form: FormGroup;
  public formCalendars: FormGroup;
  public appStogate: any;
  roles = [];
  companyBranchs = [];
  nationalitys = []
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  data : AccountProfilesDTO = new AccountProfilesDTO();
  public passwordHide: boolean = true;
  baseTranslate: any;
  public isPasswordSame : boolean = false;
  constructor(
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountService: AccountService,
    private translate: TranslateService

  ) {
    this.initBaseTranslate();
    this.appStogate = this.appStorage.getCategoryData();

    this.form = this.fb.group({
      oldPassword: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15)])],
      newPassword: [null, Validators.compose([Validators.maxLength(15), CustomValidator.passwordStrongValidator])],
      confirmPassword: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15)])]
    });

  }

  ngOnInit() {
  
  }

  ngAfterViewInit(): void {

  }

  save() {
 
     this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if(this.form.controls["newPassword"].value != this.form.controls["confirmPassword"].value)
      {
          this.snackBar.open(this.baseTranslate["msg-compare-password"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          this.isProcess.saveProcess = false;
      }
      else
      {
        this.accountService.changePassword(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      
    }
   }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }
}
