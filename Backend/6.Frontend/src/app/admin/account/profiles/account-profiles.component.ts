import { Component, OnInit, Inject, AfterViewInit, ChangeDetectorRef, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AccountService } from '../account.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { CustomValidator } from 'src/app/theme/utils/app-validators';
import { AccountProfilesDTO } from '../account.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-profiles',
  templateUrl: './account-profiles.component.html',
  providers: [AccountService, AppCommon]
})
export class AccountProfilesComponent implements OnInit, AfterViewInit {

  public form: FormGroup;
  public formCalendars: FormGroup;
  public appStogate: any;
  roles = [];
  companyBranchs = [];
  nationalitys = []
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  data : AccountProfilesDTO = new AccountProfilesDTO();
  public passwordHide: boolean = true;
  baseTranslate: any;

  constructor(
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountService: AccountService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService

  ) {
    this.initBaseTranslate();
    this.appStogate = this.appStorage.getCategoryData();
    this.roles = this.appStogate.roles;
    this.companyBranchs = this.appStogate.companyBranchs;
    this.nationalitys = this.appStogate.nationalitys;
    this.form = this.fb.group({
      email: [null, Validators.compose([Validators.required, Validators.email ,Validators.minLength(10), Validators.maxLength(50)])],
      phoneNumber: [null, Validators.compose([Validators.maxLength(20)])],
      displayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      avatar: [null]
    });
  }

  ngOnInit() {
    this.accountService.getProfiles().then(rs=>{
      if(rs!=null && rs.data!=null)
      {
        this.data = rs.data;
        console.log(this.data);
        this.form.setValue(rs.data);
      }
     });
  }

  ngAfterViewInit(): void {

  }

  save() {
     this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.accountService.updateProfiles(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
   }

   
  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }
}
