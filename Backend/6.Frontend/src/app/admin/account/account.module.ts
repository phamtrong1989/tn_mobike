import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../shared/shared.module';
import { PipesModule } from '../../theme/pipes/pipes.module';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { AccountProfilesComponent } from './profiles/account-profiles.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxCurrencyModule } from 'ngx-currency';
import { ChangePasswordComponent } from './change-password/change-password.component';

export const routes = [
  { path: '', component: AccountProfilesComponent, data: { breadcrumb: '' } },
  { path: 'change-password', component: ChangePasswordComponent, data: { breadcrumb: '' } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule  ,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      }
    }),
    NgxCurrencyModule
  ],
  declarations: [
    AccountProfilesComponent,
    ChangePasswordComponent
  ],
  entryComponents:[
    AccountProfilesComponent,
    ChangePasswordComponent
  ]
})
export class AccountModule { }
