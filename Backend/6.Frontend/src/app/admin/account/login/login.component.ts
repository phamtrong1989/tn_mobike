import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { AccountService } from '../account.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppStorage } from '../../../app.storage'
import { AppService } from 'src/app/app.service';
import { MatDialog } from '@angular/material/dialog';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [AccountService, AppService]
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  public settings: Settings;
  isProcess = { login: false };
  private hubConnection :HubConnection;
  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    public snackBar: MatSnackBar,
    public accountService: AccountService,
    public appStorage: AppStorage,
    public appService: AppService,
    public dialog: MatDialog,
  ) {
    // this.appStorage.getAccountInfo()
    this.settings = this.appSettings.settings;
    this.form = this.fb.group({
      'username': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])]
    });
  }
  ngOnInit() {
    this.appStorage.removeAccountInfo();
    this.appStorage.removeCategoryData();
  }

  onSubmit(values: any): void {
    if (this.form.valid) {
      this.isProcess.login = true;
      this.accountService.login(values).subscribe((res) => {
        if (res && res.status == 1) {
          this.appStorage.setAccountInfo(res.data);
          this.snackBar.open("Đăng nhập thành công", "Chúc mừng!", { duration: 5000 });
          this.appService.pullData("x").subscribe(outData => {
            if (outData.status == 1) {
              this.appStorage.setCategoryData(outData.data);
            }
            this.router.navigate(['/']);
          });
        }
        else if (res && res.status == 2) {
          this.snackBar.open("Tài khoản hoặc mật khẩu không chính xác", "Lỗi", { duration: 5000 });
        }
        else if (res && res.status == 3) {
          this.snackBar.open("Tài khoản đang bị khóa, vui lòng liên hệ admin để mở khóa", "Lỗi", { duration: 5000 });
        }
        else if (res && res.status == 4) {
          // this.snackBar.open("Số lượng sai mật khẩu trong ngày đã vượt quá 10 lần trong ngày, vui lòng liên hệ admin để mở khóa", "danger", { duration: 5000 });
          this.snackBar.open("Tài khoản hoặc mật khẩu không chính xác", "Lỗi", { duration: 5000 });
        }
        else if (res && res.status == 5) {
          // this.snackBar.open("Số lượng sai mật khẩu trong ngày đã vượt quá 10 lần trong ngày", "danger", { duration: 5000 });
          this.snackBar.open("Tài khoản hoặc mật khẩu không chính xác", "Lỗi", { duration: 5000 });
        }
        else {
          this.snackBar.open("Đăng nhập thất bại", "Lỗi", { duration: 5000 });
        }
        this.isProcess.login = false;
      });
    }
  }

  onExternalLogin(provider: string) {
    document.location.href = this.accountService.externalLogin(provider);
    // this.dialog.open(ExternalLoginComponent, {
    //   data: provider,
    //   panelClass: 'dialog-default',
    //   disableClose: true,
    //   autoFocus: false
    // }).afterClosed().subscribe(outData => {
    //   if (outData) {
    //   }
    // });
  }

  // initData() {
  //   if (this.appStorage.getAccountInfo() != null) {
  //     this.appService.categoryVersion().subscribe(rs => {
  //       if (rs.data != this.appStorage.getCategoryVersion()) {
  //         this.appService.pullData().subscribe(outData => {
  //           if (outData) {
  //             this.appStorage.setCategoryData(outData.data);
  //             this.appStorage.setCategoryVersion(rs.data);
  //           }
  //         });
  //       }
  //     });
  //   }
  // }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }
}