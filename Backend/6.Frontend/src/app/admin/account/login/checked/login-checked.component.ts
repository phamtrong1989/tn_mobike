import { Component, OnInit, Inject, AfterViewInit, ChangeDetectorRef, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material/snack-bar';

import { TranslateService } from '@ngx-translate/core';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { AccountService } from '../../account.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountData } from '../../account.model';
import { ApiResponseData } from 'src/app/app.settings.model';

@Component({
  selector: 'app-login-checked',
  templateUrl: './login-checked.component.html',
  providers: [AppCommon, AccountService]
})
export class LoginCheckedComponent implements OnInit, AfterViewInit {

  public form: FormGroup;
  public appStogate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  baseTranslate: any;
  public baseData: string;
  public baseType: number = 0;
  public accountData: AccountData;
  public msg: string;
  constructor(
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountService: AccountService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router

  ) {
    this.initBaseTranslate();
  }

  ngOnInit() {

    this.baseData = this.activatedRoute.snapshot.queryParamMap.get('data');
    this.baseType = Number.parseInt(this.activatedRoute.snapshot.queryParamMap.get('type'));
    this.appStorage.removeAccountInfo();

    if (this.baseType == 1 && this.baseData != null && this.baseData != "") {
      this.accountData = JSON.parse(this.decodeUnicode(this.baseData));
      this.appStorage.setAccountInfo(this.accountData);
      this.router.navigateByUrl('/');
    }
  }

  decodeUnicode(str: string) {
    return decodeURIComponent(atob(str).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  }

  ngAfterViewInit(): void {

  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }
}
