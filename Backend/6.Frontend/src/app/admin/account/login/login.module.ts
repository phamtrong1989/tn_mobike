import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { LoginComponent } from './login.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { LoginCheckedComponent } from './checked/login-checked.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgxCurrencyModule } from 'ngx-currency';
export const routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'checked', component: LoginCheckedComponent, data: { breadcrumb: '' } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxMaterialTimepickerModule,
    NgxCurrencyModule,
	  TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    })
  ],
  declarations: [
    LoginComponent,
    LoginCheckedComponent
  ],
  entryComponents:[
    LoginCheckedComponent
  ]
})
export class LoginModule { }
