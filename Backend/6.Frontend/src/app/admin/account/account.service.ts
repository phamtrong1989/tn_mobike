import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginCommand , AccountData, AccountProfilesCommand, AccountProfilesDTO, ChangePasswordCommand} from './account.model';
import { ApiResponseData, Settings, ClientSettings } from './../../app.settings.model';
import { AppSettings, AppClientSettings } from '../../app.settings';
import { retry, catchError, shareReplay } from 'rxjs/operators';

@Injectable()
export class AccountService {
    public url = "/api/admin/account";
    public clientSettings: ClientSettings;
    _injector: Injector
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
           injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector =injector;
    }

    login(model: LoginCommand) {
        return this.http.post<ApiResponseData<AccountData>>(this.url + "/login", model);
    }

    updateProfiles(model: AccountProfilesCommand) {
        return this.http.put<ApiResponseData<AccountData>>(this.url + "/UpdateProfiles", model).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();;
    }

    changePassword(model: ChangePasswordCommand) {
        return this.http.put<ApiResponseData<object>>(this.url + "/changePassword", model).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();;
    }
    
    getProfiles() {
        return this.http.get<ApiResponseData<AccountProfilesDTO>>(this.url + "/getProfiles").pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();;
    }

    externalLogin(provider: string) {
        return this.url + "/externalLogin?provider=" + provider;
    }

    externalRemove(provider: string) {
        return this.url + "/externalRemove?provider=" + provider;
    }

    externalAdd(provider: string, token : string) {
        return this.url + "/ExternalAdd?provider=" + provider +"&token=" +token;
    }

    uploadPath()
    {
        return this.url + "/upload";
    }

    logout()
    {

    }
    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
} 