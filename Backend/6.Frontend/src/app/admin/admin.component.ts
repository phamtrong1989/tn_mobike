import { Component, OnInit, ViewChild, HostListener, ViewChildren, QueryList } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { AppSettings, AppClientSettings } from '../app.settings';
import { Settings, ClientSettings, PullData } from '../app.settings.model';
import { MenuService } from '../theme/components/menu/menu.service';
import { AppService } from '../app.service';
import { AppStorage } from '../app.storage';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import * as signalR from '@aspnet/signalr';
import { Compiler } from '@angular/core';
import { filter } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [MenuService, AppService]
})
export class AdminComponent implements OnInit {
  @ViewChild('sidenav', { static: false }) sidenav: any;
  @ViewChild('backToTop', { static: false }) backToTop: any;
  @ViewChildren(PerfectScrollbarDirective) pss: QueryList<PerfectScrollbarDirective>;
  public settings: Settings;
  public menus = ['vertical', 'horizontal'];
  public menuOption: string;
  public menuTypes = ['default', 'compact', 'mini'];
  public menuTypeOption: string;
  public lastScrollTop: number = 0;
  public showBackToTop: boolean = false;
  public toggleSearchBar: boolean = false;
  private defaultMenu: string;
  public hubConnection: HubConnection;
  public clientSettings: ClientSettings;
  categoryData: any;

  constructor(
    public appSettings: AppSettings,
    private router: Router,
    public appStorage: AppStorage,
    public appService: AppService,
    appClientSettings: AppClientSettings,
    private menuService: MenuService,
    private _compiler: Compiler,
     private titleService: Title
    ) {
    this.settings = this.appSettings.settings;
    this.clientSettings = appClientSettings.settings
    this.categoryData = this.appStorage.getCategoryData();

    router.events.subscribe((val) => {
      var rt = this.categoryData.roleControllers.find(x=>x.routerLink == location.pathname);
      if(rt != null)
      {
        this.titleService.setTitle(rt.name.toUpperCase() +" | HỆ THỐNG QUẢN LÝ DICH VỤ XE ĐẠP ĐÔ THỊ TN.GO");
      }
    });
  }

  ngOnInit() {
    if (window.innerWidth <= 768) {
      this.settings.menu = 'vertical';
      this.settings.sidenavIsOpened = false;
      this.settings.sidenavIsPinned = false;
    }
    this.menuOption = this.settings.menu;
    this.menuTypeOption = this.settings.menuType;
    this.defaultMenu = this.settings.menu;
    this.initHubData();
  }

  public initData() {
    var cvs = this.appStorage.getCategoryData();
    this.appService.pullData((cvs != null ? cvs.categoryVersion : "x")).subscribe(outData => {
      if (outData.status == 1) {
        this.appStorage.setCategoryData(outData.data);
      }
    });
  }

checkAppVersion()
{
  this.appService.currentVersion().subscribe(outData => {
    if (outData.status == 1) {
      if(this.appStorage.getAppVersion() != outData.data.name)
      {
        this.appStorage.setAppVersion(outData.data.name);
        this._compiler.clearCache();
        window.location.reload();
      }
    }
  });
}

  public initHubData() {
    var data = this.appStorage.getAccountInfo();
    var strToken = "";
    if (data != null) {
      try {
        strToken = data.tokenHub;
        let builder = new HubConnectionBuilder();
        this.hubConnection = builder.withUrl(this.clientSettings.serverAPI + '/hubs/category', { accessTokenFactory: async () => strToken }).build();
        this.hubConnection.start();
        this.hubConnection.on('ReceiveMessageUpdateCategory', (message) => {
          this.initData();
        });
      }
      catch {
      }
    }
  }

  getTitle(state, parent) {
    const data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.title) {
      data.push(parent.snapshot.data.title);
    }

    if (state && parent) {
      data.push(... this.getTitle(state, state.firstChild(parent)));
    }
    return data;
  }

  ngAfterViewInit() {

    setTimeout(() => { this.settings.loadingSpinner = false }, 300);

    this.backToTop.nativeElement.style.display = 'none';
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (!this.settings.sidenavIsPinned) {
          this.sidenav.close();
        }
        if (window.innerWidth <= 768) {
          this.sidenav.close();
        }
      }
    });
    if (this.settings.menu == "vertical")
      this.menuService.expandActiveSubMenu(this.menuService.getVerticalMenuItems());

  }

  public chooseMenu() {
    this.settings.menu = this.menuOption;
    this.defaultMenu = this.menuOption;
    this.router.navigate(['/']);
  }

  public chooseMenuType() {
    this.settings.menuType = this.menuTypeOption;
  }

  public changeTheme(theme) {
    this.settings.theme = theme;
  }

  onHelp() {
    this.router.navigate(['/admin/profiles/helps']);
  }

  public toggleSidenav() {
    if (this.sidenav != undefined) {
      this.sidenav.toggle();
    }
  }

  public onPsScrollY(event) {
    (event.target.scrollTop > 300) ? this.backToTop.nativeElement.style.display = 'flex' : this.backToTop.nativeElement.style.display = 'none';
    if (this.settings.menu == 'horizontal') {
      if (this.settings.fixedHeader) {
        var currentScrollTop = (event.target.scrollTop > 56) ? event.target.scrollTop : 0;
        if (currentScrollTop > this.lastScrollTop) {
          document.querySelector('#horizontal-menu').classList.add('sticky');
          event.target.classList.add('horizontal-menu-hidden');
        }
        else {
          document.querySelector('#horizontal-menu').classList.remove('sticky');
          event.target.classList.remove('horizontal-menu-hidden');
        }
        this.lastScrollTop = currentScrollTop;
      }
      else {
        if (event.target.scrollTop > 56) {
          document.querySelector('#horizontal-menu').classList.add('sticky');
          event.target.classList.add('horizontal-menu-hidden');
        }
        else {
          document.querySelector('#horizontal-menu').classList.remove('sticky');
          event.target.classList.remove('horizontal-menu-hidden');
        }
      }
    }
  }

  public scrollToTop() {
    this.pss.forEach(ps => {
      if (ps.elementRef.nativeElement.id == 'main' || ps.elementRef.nativeElement.id == 'main-content') {
        ps.scrollToTop(0, 250);
      }
    });
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    if (window.innerWidth <= 768) {
      this.settings.sidenavIsOpened = false;
      this.settings.sidenavIsPinned = false;
      this.settings.menu = 'vertical'
    }
    else {
      (this.defaultMenu == 'horizontal') ? this.settings.menu = 'horizontal' : this.settings.menu = 'vertical'
      this.settings.sidenavIsOpened = true;
      this.settings.sidenavIsPinned = true;
    }
  }

  public closeSubMenus() {
    let menu = document.querySelector(".sidenav-menu-outer");
    if (menu) {
      for (let i = 0; i < menu.children[0].children.length; i++) {
        let child = menu.children[0].children[i];
        if (child) {
          if (child.children[0].classList.contains('expanded')) {
            child.children[0].classList.remove('expanded');
            child.children[1].classList.remove('show');
          }
        }
      }
    }
  }

}