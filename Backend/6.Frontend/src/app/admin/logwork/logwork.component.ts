import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { HttpEventType } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppCommon } from '../../app.common';
import { AppStorage } from '../../app.storage';
import { Settings } from '../../app.settings.model';
import { AppSettings } from '../../app.settings';

import { LogWorkService } from './logwork.service';
import { LogWork, LogWorkCommand } from './logwork.model';
import { LogWorkEditComponent } from './edit/logwork-edit.component';
import { User } from './../user/users/users.model';

@Component({
  selector: 'app-logwork',
  templateUrl: './logwork.component.html',
  providers: [LogWorkService, AppCommon]
})

export class LogWorkComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: LogWork[];
  isProcess = { dialogOpen: false, listDataTable: true, userSearching: false };
  params: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  displayedColumns = ["stt", "userId", "logDate", "title", "description", "createdDate", "updatedDate", "action"];

  public settings: Settings;
  public categoryData: any;
  userKey: FormControl = new FormControl();
  users: User[];

  constructor(
    public dialog: MatDialog,
    public appSettings: AppSettings,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public LogWorkService: LogWorkService
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.initSearchUser();
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.from = this.appCommon.formatDateTime(this.params.from, 'yyyy-MM-dd');
    this.params.to = this.appCommon.formatDateTime(this.params.to, 'yyyy-MM-dd');
    this.appCommon.changeUrl(location.pathname, this.params);
    this.LogWorkService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
      if (res.outData != null) {
        this.users = [];
        this.users.push(res.outData);
        this.params.accountId = res.outData?.id;
      }
    });
  }

  export() {
    this.isProcess['export'] = true;

    var report_name = "BC_LogWork.xlsx";
    var params = this.params;

    params.from = this.params.start ? this.appCommon.formatDateTime(this.params.start, "yyyy-MM-dd") : null;
    params.to = this.params.end ? this.appCommon.formatDateTime(this.params.end, "yyyy-MM-dd") : null;

    this.LogWorkService.export(params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess['export'] = true;
          break;
        case HttpEventType.Response:
          this.isProcess['export'] = false;
          const downloadedFile = new Blob([data.body], { type: data.body.type });
          const a = document.createElement('a');
          a.setAttribute('style', 'display:none;');
          document.body.appendChild(a);
          a.download = report_name;
          a.href = URL.createObjectURL(downloadedFile);
          a.target = '_blank';
          a.click();
          document.body.removeChild(a);
          this.isProcess['export'] = false;
          break;
      }
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  getFilesCount(files: string) {
    try {
      if (files == null) {
        return 0;
      }
      var objects = JSON.parse(files);
      return objects.length;
    }
    catch
    {
      return 0;
    }
  }

  openEditDialog(data: LogWork) {
    var model = new LogWorkCommand();

    if (data == null) {
      model.id = 0;
    }
    else {
      model = new LogWorkCommand().deserialize(data);
    }

    this.dialog.open(LogWorkEditComponent, {
      width: "500px",
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  initSearchUser() {
    this.userKey.valueChanges.subscribe(rs => {
      this.isProcess.userSearching = false;
      if (rs != "") {
        this.isProcess.userSearching = true;
        this.LogWorkService.searchByUser({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.params.accountId = "";
          }
          this.users = rs.data;
          this.isProcess.userSearching = false;
        });
      }
    },
      error => {
        this.isProcess.userSearching = false;
      });
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}