import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LogWorkCommand } from '../logwork.model';
import { LogWorkService } from '../logwork.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-logwork-edit',
  templateUrl: './logwork-edit.component.html',
  providers: [
    LogWorkService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class LogWorkEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  uploadFileOut: FileDataDTO[];

  constructor(
    public dialogRef: MatDialogRef<LogWorkEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: LogWorkCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public logworkService: LogWorkService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      shift: [null, Validators.compose([Validators.required])],
      title: [null, Validators.compose([Validators.required])],
      description: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(2000)])],
      files: [null]
    });
  }

  ngOnInit() {
    if (this.data.files != null) {
      this.uploadFileOut = JSON.parse(this.data.files)
    }

    if (this.data.id > 0) {
      if (this.data.files != null) {
        this.uploadFileOut = JSON.parse(this.data.files)
      }
      this.getData();
    }
  }

  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.logworkService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new LogWorkCommand().deserialize(rs.data);
        this.form.setValue(this.data);

        if (this.data.files != null) {
          this.uploadFileOut = JSON.parse(this.data.files)
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
      if (this.data.id > 0) {
        this.logworkService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res);
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.logworkService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.close(res);
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
    else {
      console.log(this.form.validator)
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
