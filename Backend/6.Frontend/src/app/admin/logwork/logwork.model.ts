import { User } from './../user/users/users.model';

export class LogWork {
  id: number;
  shift: number;
  role: number;
  title: string;
  description: string;
  files: string;

  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  logDate: Date;
  isLate: boolean;

  CreatedUserObject: User;
  UpdatedUserObject: User;
}

export class LogWorkCommand {
  id: number;
  shift: number;
  title: string;
  description: string;
  files: string;

  deserialize(input: LogWork): LogWorkCommand {
    if (input == null) {
      input = new LogWork();
    }

    this.id = input.id;
    this.shift = input.shift;
    this.title = input.title;
    this.description = input.description;
    this.files = input.files;

    return this;
  }
}