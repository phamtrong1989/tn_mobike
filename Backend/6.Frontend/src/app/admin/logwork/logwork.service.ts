import { User } from './../user/users/users.model';
import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpParams, HttpRequest } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { LogWork, LogWorkCommand } from './logwork.model';
import { ApiResponseData, ClientSettings } from './../../app.settings.model';
import { AppClientSettings } from '../../app.settings';
import { AppCommon } from '../../app.common'
import { Observable } from 'rxjs';

@Injectable()
export class LogWorkService {
    public url = "/api/work/LogWork";
    public clientSettings: ClientSettings;
    _injector: Injector;

    constructor(
        public http: HttpClient,
        appClientSettings: AppClientSettings,
        public appCommon: AppCommon,
        injector: Injector
    ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<LogWork[]>>(this.url + "/searchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(params: LogWorkCommand) {
        return this.http.post<ApiResponseData<LogWork>>(this.url + "/create", params).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(params: LogWorkCommand) {
        return this.http.put<ApiResponseData<LogWork>>(this.url + "/edit", params).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<LogWork>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    delete(id: number) {
        return this.http.delete<ApiResponseData<LogWork>>(this.url + "/delete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByUser(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchByUser", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    uploadPath() {
        return this.url + "/Upload";
    }

    apiUrl() {
        return this.clientSettings.serverAPI;
    }

    export(params: any): Observable<HttpEvent<Blob>> {
        let obj = {};
        for (const prop in params) {
            if (params[prop] != null) {
                obj[prop] = params[prop];
            }
        }

        return this.http.request(new HttpRequest(
            'POST',
            this.url + "/report",
            null,
            {
                reportProgress: true,
                responseType: 'blob',
                params: new HttpParams({ fromObject: obj })
            }));
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {
        } else {
        }
        return Promise.reject(error);
    }
}