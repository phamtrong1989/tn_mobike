import { Account } from 'src/app/admin/customer/accounts/accounts.model';
import { Bike } from "../../infrastructure/bikes/bikes.model";
import { Dock } from '../../infrastructure/docks/docks.model';
import { GpsData } from '../../infrastructure/stations/stations.model';
import { User } from '../../user/users/users.model';

export class Transaction {

  id: number;
  projectId: number;
  stationIN: number;
  stationOut: number;
  bikeId: number;
  dockId: number;
  accountId: number;
  customerGroupId: number;
  transactionCode: string;
  endDate: Date;
  startDate: Date;
  vote: number;
  feedback: string;
  voucherCode: string;
  ticketType: number;
  ticketValue: number;
  status: number;
  accountBlance: number;
  subAccountBlance: number;
  chargeInAccount: number;
  allowChargeInSubAccount: boolean;
  chargeInSubAccount: number;
  ticketPriceId: number;
  blockPerMinute: number;
  blockPerViolation: number;
  blockViolationValue: number;
  limitMinutes: number;
  dateOfPayment: Date;
  createdDate: Date;
  sourceType: number;
  isDefault: boolean;
  totalMinutes: number;
  totalPrice: number;
  bike: Bike;
  dock: Dock;
  account: Account;
  updatedUserObject: User;
  gps: GpsData[];
  isDockNotOpen: boolean;
}

export class TransactionCommand {

  id: number;
  projectId: number;
  stationIN: number;
  stationOut: number;
  bikeId: number;
  dockId: number;
  accountId: number;
  customerGroupId: number;
  transactionCode: string;
  endDate: Date;
  startDate: Date;
  vote: number;
  feedback: string;
  voucherCode: string;
  ticketType: number;
  ticketValue: number;
  status: number;
  accountBlance: number;
  subAccountBlance: number;
  chargeInAccount: number;
  allowChargeInSubAccount: boolean;
  chargeInSubAccount: number;
  ticketPriceId: number;
  blockPerMinute: number;
  blockPerViolation: number;
  blockViolationValue: number;
  limitMinutes: number;
  dateOfPayment: Date;
  createdDate: Date;
  sourceType: number;
  isDefault: boolean;
  totalMinutes: number;
  totalPrice: number;
  updatedUserObject: User;
  gps: GpsData[];

  deserialize(input: Transaction): TransactionCommand {
    if (input == null) {
      input = new Transaction();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.stationIN = input.stationIN;
    this.stationOut = input.stationOut;
    this.bikeId = input.bikeId;
    this.dockId = input.dockId;
    this.accountId = input.accountId;
    this.customerGroupId = input.customerGroupId;
    this.transactionCode = input.transactionCode;
    this.endDate = input.endDate;
    this.startDate = input.startDate;
    this.vote = input.vote;
    this.feedback = input.feedback;
    this.voucherCode = input.voucherCode;
    this.ticketType = input.ticketType;
    this.ticketValue = input.ticketValue;
    this.status = input.status;
    this.accountBlance = input.accountBlance;
    this.subAccountBlance = input.subAccountBlance;
    this.chargeInAccount = input.chargeInAccount;
    this.allowChargeInSubAccount = input.allowChargeInSubAccount;
    this.chargeInSubAccount = input.chargeInSubAccount;
    this.ticketPriceId = input.ticketPriceId;
    this.blockPerMinute = input.blockPerMinute;
    this.blockPerViolation = input.blockPerViolation;
    this.blockViolationValue = input.blockViolationValue;
    this.limitMinutes = input.limitMinutes;
    this.dateOfPayment = input.dateOfPayment;
    this.createdDate = input.createdDate;
    this.sourceType = input.sourceType;
    this.isDefault = input.isDefault;
    this.updatedUserObject = input.updatedUserObject;
    this.gps = input.gps;
    return this;
  }
}