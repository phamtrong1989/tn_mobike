import { Component, OnInit, Inject, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { Subject, ReplaySubject } from 'rxjs';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { ClientSettings, PullData } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AnyNaptrRecord } from 'dns';
import { TransactionsService } from '../transactions.service';
import { Transaction, TransactionCommand } from '../transactions.model';
import { CustomerGroup } from 'src/app/admin/category/projects/projects.model';
import { TranslateService } from '@ngx-translate/core';
import { GpsData } from 'src/app/admin/infrastructure/stations/stations.model';
import { isThisSecond } from 'date-fns';

@Component({
  selector: 'app-transaction-details',
  templateUrl: './transaction-details.component.html',
  providers: [
    TransactionsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class TransactionDetailsComponent implements OnInit, AfterViewInit {
  location: Location;
  mapStyle: any = [
    { featureType: "road", stylers: [{ visibility: "on" }] },
    { featureType: "poi", stylers: [{ visibility: "off" }] },
    { featureType: "transit", stylers: [{ visibility: "on" }] },
    { featureType: "administrative", stylers: [{ visibility: "on" }] },
    { featureType: "administrative.locality", stylers: [{ visibility: "off" }] }
  ];
  polylines: GpsData[];
  iconStart: string = '../assets/img/vendor/leaflet/icon-start.png';
  iconEnd: string = '../assets/img/vendor/leaflet/icon-end.png';
  public clientSettings: ClientSettings;
  protected _onDestroy = new Subject<void>();
  public form: FormGroup;
  public categoryData: PullData;
  public customerGroups: CustomerGroup[];
  public heightScroll: number = 828;
  transactionTranslate: any;

  viewType: number;
  isProcess = { saveProcess: false, deleteAppointments: false, nationalitySearching: false, dialogOpen: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<TransactionDetailsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { transaction: Transaction, transactionCommand: TransactionCommand },
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public transactionService: TransactionsService,
    public appCommon: AppCommon,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    appClientSettings: AppClientSettings,
  ) {
    this.clientSettings = appClientSettings.settings
    this.categoryData = this.appStorage.getCategoryData();
    this.initTransactionTranslate();
    this.viewType = 0;
  }

  getData() {
    this.transactionService.getById(this.data.transaction.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data.transactionCommand = new TransactionCommand().deserialize(rs.data);
        this.data.transaction = rs.data;
        this.polylines = this.data.transaction.gps;
        this.isProcess.initData = false;
        if(this.polylines != null && this.polylines.length > 0)
        {
          this.location = {
            latitude: this.polylines[0].lat,
            longitude: this.polylines[0].long,
            mapType: "street view",
            zoom: 18,
            markers: []
          }
          this.addMarker(this.polylines[0].lat, this.polylines[0].long, this.iconStart, true);
          this.addMarker(this.polylines[this.polylines.length - 1].lat, this.polylines[this.polylines.length - 1].long, this.iconEnd, true);
        }
      }
      else {
        this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
      }
    });
  }

  ngOnInit() {
    var project = this.appStorage.getCategoryData().projects.find(x=>x.id == this.data.transaction.projectId);
    if(project == null)
    {
      this.location = {
        latitude: 21.0406353,
        longitude: 105.8250546,
        mapType: "street view",
        zoom: 18,
        markers: []
      }
    }
    else{
      this.location = {
        latitude: project.lat,
        longitude: project.lng,
        mapType: "street view",
        zoom: 18,
        markers: []
      }
    }
    this.getData();
  }

  zoomChange(event: any) {
    this.location.zoom = event;
  }

  getFullUrl(url: string) {
    return this.clientSettings.serverAPI + url;
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close(1);
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  public openCustomersEditDialog() {

  }

  matTabGroupChange(e: AnyNaptrRecord) {

  }

  fixHeightScroll() {
    let getTop = document.getElementById("fix_detail").getBoundingClientRect().top;
    this.heightScroll = window.innerHeight - getTop  -30;
  }

  
  addMarker(lat: number, lng: number,  iconUrl: string, isOpen: boolean) {
    this.location.markers.push({
      lat,
      lng,
      iconUrl,
      isOpen
    });
  }

  ngAfterViewInit(): void {
    this.fixHeightScroll();
    this.cdr.detectChanges();
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightScroll();
  }

  initTransactionTranslate() {
    this.translate.get(['transaction'])
      .subscribe(translations => {
        this.transactionTranslate = translations['transaction'];
      });
  }

  bindStatus(input: any = null) {
    var data: any
    if (input != null) {
      switch (input) {
        case 0:
          data = this.transactionTranslate["status-0"]
          break;
        case 1:
          data = this.transactionTranslate["status-1"]
          break;
        case 2:
          data = this.transactionTranslate["status-2"]
          break;
        case 3:
          data = this.transactionTranslate["status-3"]
          break;
        case 4:
          data = this.transactionTranslate["status-4"]
          break;
        case 5:
          data = this.transactionTranslate["status-5"]
          break;
        default:
          data = this.transactionTranslate["status-0"]
          break;
      }

      return data;

    } else {
      return null;
    }
  }
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  markers: Marker[];
}

interface Marker {
  lat: number;
  lng: number;
  iconUrl: string;
  isOpen: boolean;
}