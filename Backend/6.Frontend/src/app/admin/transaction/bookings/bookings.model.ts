import { Account, Transaction } from 'src/app/admin/customer/accounts/accounts.model';
import { CustomerGroup } from '../../category/projects/projects.model';
import { Bike } from "../../infrastructure/bikes/bikes.model";
import { GpsData } from '../../infrastructure/stations/stations.model';

export class Booking {
  id: number;
  stationIn: number;
  stationOut: number;
  bikeId: number;
  dockId: number;
  accountId: number;
  customerGroupId: number;
  transactionCode: string;
  vote: number;
  feedback: string;
  status: number;
  chargeInAccount: number;
  chargeInSubAccount: number;
  ticketPriceId: number;
  dateOfPayment: Date;
  createdDate: Date;
  totalMinutes: number;
  totalPrice: number;
  kCal: number;
  kG: number;
  kM: number;
  endTime: Date;
  investorId: number;
  projectId: number;
  startTime: Date;
  ticketPrice_AllowChargeInSubAccount: boolean;
  ticketPrice_BlockPerMinute: number;
  ticketPrice_BlockPerViolation: number;
  ticketPrice_BlockViolationValue: number;
  ticketPrice_IsDefault: boolean;
  ticketPrice_LimitMinutes: number;
  ticketPrice_TicketType: number;
  ticketPrice_TicketValue: number;
  wallet_AccountBlance: number;
  wallet_SubAccountBlance: number;
  note: string;
  ticketPrepaidCode: string;
  ticketPrepaidId: number;
  ticketPrice_Note: string;
  bike: Bike;
  customerGroup: CustomerGroup;
  tripPoint: number;
  gps: GpsData[];
  account: Account;
  isDockNotOpen: boolean;
  isForgotrReturnTheBike: boolean;
}

export class BookingCancelCommand
{
  bookingId: number;
  feedback: number;
}


export class BookingEndCommand
{
  bookingId: number;
  feedback: number;
}

export class BookingOpenLockCommand
{
  bookingId: number;
  dockId: number;
  feedback: number;
}

export class RFIDBookingEndCommand
{
  rfidBookingId: number;
  feedback: number;
  checkCloseDock: boolean;
  checkInStation: boolean;
}