import { Component, OnInit, Inject, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { Subject } from 'rxjs';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { ClientSettings, PullData } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AnyNaptrRecord } from 'dns';
import { Booking } from '../bookings.model';
import { BookingsService } from '../bookings.service';
import { CustomerGroup } from 'src/app/admin/category/projects/projects.model';
import { GpsData } from 'src/app/admin/infrastructure/stations/stations.model';
@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  providers: [
    BookingsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class BookingDetailsComponent implements OnInit, AfterViewInit {
  location: Location;
  mapStyle: any = [
    { featureType: "road", stylers: [{ visibility: "on" }] },
    { featureType: "poi", stylers: [{ visibility: "off" }] },
    { featureType: "transit", stylers: [{ visibility: "on" }] },
    { featureType: "administrative", stylers: [{ visibility: "on" }] },
    { featureType: "administrative.locality", stylers: [{ visibility: "off" }] }
  ];
  polylines: GpsData[];
  iconStart: string = '../assets/img/vendor/leaflet/icon-start.png';
  iconEnd: string = '../assets/img/vendor/leaflet/icon-end.png';
  public clientSettings: ClientSettings;
  protected _onDestroy = new Subject<void>();
  public form: FormGroup;
  public categoryData: PullData;
  public customerGroups : CustomerGroup[];
  public heightScroll: number = 828;
  viewType: number;
  isProcess = { saveProcess: false, deleteAppointments: false, nationalitySearching: false, dialogOpen: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<BookingDetailsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { booking: Booking},
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public bookingService: BookingsService,
    public appCommon: AppCommon,
    appClientSettings: AppClientSettings,
    private cdr: ChangeDetectorRef
  ) {
    this.clientSettings = appClientSettings.settings
    this.categoryData = this.appStorage.getCategoryData();
    this.isProcess.initData = true;
    this.getData();
  }

  getData() {
    this.bookingService.getById(this.data.booking.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data.booking = rs.data;

        this.polylines = this.data.booking.gps;
        this.isProcess.initData = false;
        if(this.polylines != null && this.polylines.length > 0)
        {
          this.location = {
            latitude: this.polylines[0].lat,
            longitude: this.polylines[0].long,
            mapType: "street view",
            zoom: 18,
            markers: []
          }
          this.addMarker(this.polylines[0].lat, this.polylines[0].long, this.iconStart, true);
          this.addMarker(this.polylines[this.polylines.length - 1].lat, this.polylines[this.polylines.length - 1].long, this.iconEnd, true);
        }

        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
      }
    });
  }

  
  addMarker(lat: number, lng: number,  iconUrl: string, isOpen: boolean) {
    this.location.markers.push({
      lat,
      lng,
      iconUrl,
      isOpen
    });
  }


  ngOnInit() {
    var project = this.appStorage.getCategoryData().projects.find(x=>x.id == this.data.booking.projectId);
    if(project == null)
    {
      this.location = {
        latitude: 21.0406353,
        longitude: 105.8250546,
        mapType: "street view",
        zoom: 18,
        markers: []
      }
    }
    else{
      this.location = {
        latitude: project.lat,
        longitude: project.lng,
        mapType: "street view",
        zoom: 18,
        markers: []
      }
    }
    this.getData();
  }

  getFullUrl(url:string) {
    return this.clientSettings.serverAPI + url;
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close(1);
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  public openCustomersEditDialog() {
    
  }

  matTabGroupChange(e: AnyNaptrRecord) {
   
  }

  fixHeightScroll() {
    let getTop = document.getElementById("fix_detail").getBoundingClientRect().top;
    this.heightScroll = window.innerHeight - getTop- 30;
    this.viewType = 0;
 
  }

  ngAfterViewInit(): void {
    this.fixHeightScroll();
    this.cdr.detectChanges();
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightScroll();
  }
  
}


interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  markers: Marker[];
}

interface Marker {
  lat: number;
  lng: number;
  iconUrl: string;
  isOpen: boolean;
}