import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { TransactionsDebtService } from '../transactions.service';
import { AppCommon } from 'src/app/app.common';
import { Transaction } from '../transactions.model';
import { AppStorage } from 'src/app/app.storage';

@Component({
  selector: 'app-refund-booking',
  templateUrl: './refund.component.html',
  providers: [
    TransactionsDebtService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class RefundComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  intevalData: any;
  constructor(
    public dialogRef: MatDialogRef<RefundComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: Transaction,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public transactionsDebtService: TransactionsDebtService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      note: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(1000)])]
    });
  }

  ngOnInit() {
    this.form.get('id').setValue(this.data.id);
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData(loading: boolean) {
    if(loading)
    {
      this.isProcess.initData = true;
    }
    this.transactionsDebtService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.isProcess.initData = false;
        this.data = rs.data;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.transactionsDebtService.refund(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
  }

  close(input: any = null): void {
    if (this.intevalData) {
      clearInterval(this.intevalData);
    }
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
