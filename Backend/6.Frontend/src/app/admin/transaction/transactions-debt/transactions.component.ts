import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppClientSettings, AppSettings } from '../../../app.settings';
import { Settings, ApiResponseData } from '../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { TransactionsDebtService } from './transactions.service';
import { Transaction, TransactionCommand } from './transactions.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';
import { TransactionDetailsDebtComponent } from './details/transaction-details.component';
import { FormControl } from '@angular/forms';
import { Account } from '../../customer/accounts/accounts.model';
import { ReportService } from '../../report/report.service';
import { HttpEventType } from '@angular/common/http';
import { RefundComponent } from './refund/refund.component';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  providers: [TransactionsDebtService, ReportService, AppCommon]
})

export class TransactionsDebtComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: Transaction[];
  accountKey: FormControl = new FormControl("");
  accounts: Account[];
  date: Date;
  isProcess = { dialogOpen: false, listDataTable: true, accountSearching: false, export: false };
  params: any = { pageIndex: 1, pageSize: 20, projectId: "", customerGroupId:"", investorId: "" , stationIn: "", stationOut: "", key: "",  sortby: "", status: "", accountId: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "transactionCode", "accountId", "bikeId" , "time", "station",  "estimate","totalPrice", "chargeDebt", "vote", "sk",  "projectId", "action"];
  baseURL;

  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public transactionsService: TransactionsDebtService,
    public reportService: ReportService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    appClientSettings: AppClientSettings

  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.baseURL = appClientSettings.settings.serverAPI;
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.params.from = !this.params.from ? this.appCommon.formatDateTime(new Date(), 'yyyy-MM-dd') : this.params.from;
    this.params.to = !this.params.to ? this.appCommon.formatDateTime(new Date(), 'yyyy-MM-dd') : this.params.to;
    this.initSearchAccount();
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.appCommon.changeUrl(location.pathname, this.params);
    this.transactionsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;

      if(res.outData!=null && (this.accounts == null || this.accounts.length == 0))
      {
        this.accounts= [];
        this.accounts.push(res.outData);
        this.params.accountId = res.outData.id;
      }
    });
  }

  
  openRefundDialog(data: Transaction) {
    this.dialog.open(RefundComponent, {
      data: data,
      disableClose: true,
      autoFocus: false,
      panelClass: 'dialog-600'
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
      }
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.params.from = this.appCommon.formatDateTime(this.params.from, "yyyy-MM-dd");
    this.params.to = this.appCommon.formatDateTime(this.params.to, "yyyy-MM-dd");
    this.searchPage();
  }

  export() {
    this.isProcess['export'] = true;

    var report_name = "BC_ChuyenDiNoCuoc.xlsx";
    var params = this.params;

    params.from = this.params.from ? this.appCommon.formatDateTime(this.params.from, "yyyy-MM-dd") : null;
    params.to = this.params.to ? this.appCommon.formatDateTime(this.params.to, "yyyy-MM-dd") : null;

    this.reportService.export("reportTransaction", "transactionDebt", params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess['export'] = true;
          break;
        case HttpEventType.Response:
          this.isProcess['export'] = false;

          window.open(this.baseURL + "/" + data.body);
          // const downloadedFile = new Blob([data.body], { type: data.body.type });
          // const a = document.createElement('a');
          // a.setAttribute('style', 'display:none;');
          // document.body.appendChild(a);
          // a.download = report_name;
          // a.href = URL.createObjectURL(downloadedFile);
          // a.target = '_blank';
          // a.click();
          // document.body.removeChild(a);

          this.isProcess['export'] = false;
          break;
      }
    });
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
        this.isProcess.accountSearching = false;
        if (rs != "") {
          this.isProcess.accountSearching = true;
          this.transactionsService.searchByAccount({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.params.accountId = "";
            }
            this.accounts = rs.data;
            this.isProcess.accountSearching = false;
          });
        }
      },
        error => {
          this.isProcess.accountSearching = false;
      });
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  public openDetailsDialog(data: Transaction) {
    var dataModel = new TransactionCommand().deserialize(data);
    this.dialog.open(TransactionDetailsDebtComponent, {
      data: { transaction: data, transactionCommand: dataModel },
      panelClass: 'dialog-full',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
      }
    });
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}