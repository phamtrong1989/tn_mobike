import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { TransactionsDebtComponent } from './transactions.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { TransactionDetailsDebtComponent } from './details/transaction-details.component';
import { AgmCoreModule } from '@agm/core';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { RefundComponent } from './refund/refund.component';

export const routes = [
  { path: '', component: TransactionsDebtComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    AgmCoreModule,
    TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }) 
  ],
  declarations: [
    TransactionsDebtComponent,
    TransactionDetailsDebtComponent,
    RefundComponent
  ],
  entryComponents:[
    TransactionDetailsDebtComponent,
    RefundComponent
  ]
})
export class TransactionsDebtModule { }
