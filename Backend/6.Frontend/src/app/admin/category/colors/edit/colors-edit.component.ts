import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Color, ColorCommand } from '../colors.model';
import { ColorsService } from '../colors.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-colors-edit',
  templateUrl: './colors-edit.component.html',
  providers: [
    ColorsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class ColorsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<ColorsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ColorCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public colorsService: ColorsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      name: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(99999)])],
      code: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(99999)])],
      colorpicker: [0]

    });

  }

  ngOnInit() {

    if (this.data.id > 0) {
      this.form.setValue(this.data);
      this.getData();
    }

  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.colorsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new ColorCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.colorsService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.colorsService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new ColorCommand().deserialize(res.data);
            this.form.setValue(this.data);
            this.close(res)
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.colorsService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  changeColor(input: any = null): void {
    var value = input.target.value;
    var id = input.target.id;
    if (id == "colorpicker") {
      this.form.get('code').setValue(value)
    }
    else if (id == "colorstring"){
      if(value.length > 6){
        this.form.get('colorpicker').setValue(value)
      }
    }
  }

}
