export class Color {

id: number;
name: string;
code: string;
createdDate: Date;
createdUser: number;
updatedDate: Date;
updatedUser: number;


}

export class ColorCommand {

 id: number;
name: string;
code: string;
colorpicker: string;


  deserialize(input: Color): ColorCommand {
    if(input==null)
    {
      input  = new Color();
    }
	this.id = input.id;
this.name = input.name;
this.code = input.code;
this.colorpicker = input.code;

   
    return this;
  }
}