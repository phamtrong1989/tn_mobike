import { User } from "../../user/users/users.model";

export class Investor {
  id: number;
  name: string;
  description: string;
  code: string;
  status: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  createdUserObject: User;
  updatedUserObject: User;
}

export class InvestorCommand {
  id: number;
  name: string;
  description: string;
  code: string;
  status: number;

  deserialize(input: Investor): InvestorCommand {
    if (input == null) {
      input = new Investor();
    }
    this.id = input.id;
    this.name = input.name;
    this.description = input.description;
    this.code = input.code;
    this.status = input.status;
    return this;
  }
}
