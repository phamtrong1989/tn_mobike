export class Language {

id: number;
name: string;
code1: string;
code2: string;
createdDate: Date;
createdUser: number;
updatedDate: Date;
updatedUser: number;
flag1: string;
flag2: string;


}

export class LanguageCommand {

 id: number;
name: string;
code1: string;
code2: string;

flag1: string;
flag2: string;


  deserialize(input: Language): LanguageCommand {
    if(input==null)
    {
      input  = new Language();
    }
	this.id = input.id;
this.name = input.name;
this.code1 = input.code1;
this.code2 = input.code2;

this.flag1 = input.flag1;
this.flag2 = input.flag2;

   
    return this;
  }
}