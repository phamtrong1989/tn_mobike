import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Output, Input, EventEmitter } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../../app.settings';
import { Settings, ApiResponseData } from '../../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage';
import { ProjectsService } from '../projects.service';
import { CustomerGroup, Project, ProjectCommand, TicketPrice, TicketPriceCommand } from '../projects.model';
import { ProjectsTicketPricesEditComponent } from './edit/projects-ticket-prices-edit.component';
@Component({
  selector: 'app-projects-ticket-prices',
  templateUrl: './projects-ticket-prices.component.html',
  providers: [ProjectsService, AppCommon]
})

export class ProjectsTicketPricesComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  @Input() project: Project;
  @Input() customerGroup: CustomerGroup;
  @Output() isCommit: EventEmitter<boolean> = new EventEmitter<boolean>();

  heightTable: number = 200;
  listData: TicketPrice[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, customerGroupId: 0, projectId: 0, sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "name",  "ticketType", "ticketValue", "info" , "isDefault",  "isActive", "action"];
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public projectsService: ProjectsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params.customerGroupId = this.customerGroup.id;
    this.params.projectId = this.project.id;
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.projectsService.ticketPriceSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: TicketPrice) {
    var model = new TicketPriceCommand();

    if (data == null) {
      model.id = 0;
      model.customerGroupId = this.customerGroup.id;
      model.projectId = this.project.id;
    }
    else {
      model = new TicketPriceCommand().deserialize(data);
    }

    this.dialog.open(ProjectsTicketPricesEditComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }
}