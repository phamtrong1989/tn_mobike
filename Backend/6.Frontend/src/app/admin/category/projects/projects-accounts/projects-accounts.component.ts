import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../../app.settings';
import { Settings, ApiResponseData } from '../../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage';
import { Project, ProjectAccount, ProjectAccountCommand } from '../projects.model';
import { ProjectsService } from '../projects.service';
import { ProjectsAccountsEditComponent } from './edit/projects-accounts-edit.component';

@Component({
  selector: 'app-projects-accounts',
  templateUrl: './projects-accounts.component.html',
  providers: [ProjectsService, AppCommon]
})

export class ProjectsAccountsComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  @Input() project: Project;
  heightTable: number = 700;
  listData: ProjectAccount[];
  isProcess = { dialogOpen: false, listDataTable: true };

  params: any = {
    pageIndex: 1,
    pageSize: 20,
    key: "",
    sortby: "",
    sorttype: "",
    projectId: "",
    customerGroupId: "",
    sex: "",
    type: "",
    status: "",
    id: null
  };

  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "id", "fullName", "phone", "rfid", "birthday", "email", "sex", "type", "customerGroupId", "language", "status", "action"];
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public projectsService: ProjectsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  ngOnInit(): void {

  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public firsLoad() {
    this.fixHeightTable();
    this.search();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.projectId = this.project.id;
    this.projectsService.projectAccountSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: ProjectAccount) {
    var model = new ProjectAccountCommand();

    if (data == null) {
      model.id = 0;
      model.projectId = this.project.id;
    }
    else {
      model = new ProjectAccountCommand().deserialize(data);
    }

    this.dialog.open(ProjectsAccountsEditComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null && this.fix_scroll_table_rec != undefined) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 30;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;
  @ViewChild('headTable_rec', { static: false }) headTable_rec: ElementRef;
  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable();
  }
}