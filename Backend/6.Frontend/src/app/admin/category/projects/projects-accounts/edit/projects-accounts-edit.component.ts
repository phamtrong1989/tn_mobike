import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../../app.common'
import { AppStorage } from '../../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { Account } from 'src/app/admin/customer/accounts/accounts.model';
import { PullData } from 'src/app/app.settings.model';
import { ProjectsService } from '../../projects.service';
import { CustomerGroup, ProjectAccountCommand } from '../../projects.model';

@Component({
  selector: 'app-projects-accounts-edit',
  templateUrl: './projects-accounts-edit.component.html',
  providers: [
    ProjectsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class ProjectsAccountsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: PullData;
  customerGroups : CustomerGroup[];
  baseTranslate: any;
  account: Account;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, searchProcess: false };
  constructor(
    public dialogRef: MatDialogRef<ProjectsAccountsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ProjectAccountCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public tenantsService: ProjectsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.customerGroups = this.categoryData.customerGroups.filter(x=>x.projectId == this.data.projectId);
    this.form = this.fb.group({
      id: [0],
      projectId: [0],
      accountId: [null, Validators.compose([Validators.required])],
      phone: [null, Validators.compose([Validators.required])],
      customerGroupId: [null, Validators.compose([Validators.required])],
      sourceType: [1],
      dateImport: [new Date()],
      createDate: [new Date()],
      updateDate: [new Date()]
    });
  }

  ngOnInit() {
    if (this.data.id > 0) {
      this.setForm();
      this.getData();
      this.setDefaultAccount();
    }
    else
    {
      
      this.form.get('projectId').setValue(this.data.projectId);
      this.setDefaultAccount();
    }
  }

  setForm()
  {
    this.form.get('id').setValue(this.data.id);
    this.form.get('customerGroupId').setValue(this.data.customerGroupId);
    this.form.get('projectId').setValue(this.data.projectId);
    this.form.get('accountId').setValue(this.data.accountId);
    this.form.get('phone').setValue("0987265582");
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  setDefaultAccount()
  {
    this.account = new Account();
    this.account.id = 0;
    this.account.firstName = "";
    this.account.lastName = "";
    this.account.birthday = null;
    this.account.sex = null;
    this.account.phone = "";
    this.account.email = "";
  }

  getData() {
    this.isProcess.initData = true;
    this.tenantsService.projectAccountGetById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new ProjectAccountCommand().deserialize(rs.data);
        this.account =rs.data.account;
        this.setForm();
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  search()
  {
    this.isProcess.searchProcess = true;
    this.tenantsService.accountGetByPhone(this.form.get('phone').value).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.account = rs.data;
        this.isProcess.searchProcess = false;
        this.form.get('accountId').setValue(this.account.id);
      }
      else {
        this.snackBar.open("Không tồn tại tài khoản nào có số điện thoại trên", this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.searchProcess = false;
        this.form.get('accountId').setValue(null);
        this.setDefaultAccount();
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.tenantsService.projectAccountEdit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.tenantsService.projectAccountCreate(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.close(res)
          }
          else  if (res.status == 2) {
            this.snackBar.open("Khách hàng này đã tồn tại trong khách hàng nội bộ, vui lòng kiểm tra lại", this.baseTranslate["msg-success-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.tenantsService.projectAccountDelete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });
      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
