import { Component, OnInit, Inject, Input } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../../app.common'
import { AppStorage } from '../../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { CustomerGroupCommand } from '../../projects.model';
import { ProjectsService } from '../../projects.service';

@Component({
  selector: 'app--projects-customer-groups-edit',
  templateUrl: './customer-groups-edit.component.html',
  providers: [
    ProjectsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class ProjectsCustomerGroupsEditComponent implements OnInit {

  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;

  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<ProjectsCustomerGroupsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: CustomerGroupCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public projectsService: ProjectsService,
    public appCommon: AppCommon
  ) {
    this.categoryData = this.appStorage.getCategoryData();

    this.form = this.fb.group({
      id: [0],
      name: [null, Validators.compose([Validators.required,  Validators.maxLength(100)])],
      note: [null, Validators.compose([ Validators.maxLength(1000)])],
      order: [0],
      projectId: [0],
      status: [1],
      isDefault: [false],
      createdDate: [new Date()],
      updatedDate: [new Date()]
    });
  }

  ngOnInit() {
    if(this.data.id >0)
    {
      this.form.setValue(this.data);
      this.getData();
    }
    else
    {
      this.form.get('projectId').setValue(this.data.projectId);
    }
  }

  getData() {
    this.isProcess.initData = true;
    this.projectsService.customerGroupGetById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new CustomerGroupCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "warning", { duration: 5000, });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if(this.data.id > 0)
      {
        this.projectsService.customerGroupEdit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open("Cập nhật thành công", "Thành công", { duration: 5000 });
            this.close(res)
          }
          else  if (res.status == 2) {
            this.snackBar.open("Chỉ được chọn 1 nhóm khách hàng là mặc định", "Thành công", { duration: 5000 });
          }
          else {
            this.snackBar.open("Có lỗi hoặc phiên làm việc đã kết thúc, vui lòng F5 refesh lại trình duyệt và thử lại", "danger", { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else
      {
        this.projectsService.customerGroupCreate(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open("Thêm mới thành công", "Thành công", { duration: 5000 });
            this.close(res)
          }
          else  if (res.status == 2) {
            this.snackBar.open("Chỉ được chọn 1 nhóm khách hàng là mặc định", "Thành công", { duration: 5000 });
          }
          else {
            this.snackBar.open("Có lỗi hoặc phiên làm việc đã kết thúc, vui lòng F5 refesh lại trình duyệt và thử lại", "Cảnh báo", { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Bạn có muốn thực hiện thao tác này không, dữ liệu xóa đi sẽ không thể phục hồi?",
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        if (result) {
          this.isProcess.deleteProcess = true;
          this.projectsService.customerGroupDelete(this.data.id).then((res) => {
            if (res.status == 1) {
              this.snackBar.open("Xóa thành công", "Thành công", { duration: 5000, });
              this.isProcess.deleteProcess = false;
              this.close(this.data)
            }
            else  if (res.status == 2) {
              this.snackBar.open("Nhóm khách hàng này đang được sử dụng trong dữ liệu vé, vui lòng xóa hết dữ liệu vé trước khi thực hiện thao tác này", "Cảnh báo", { duration: 5000, });
              this.isProcess.deleteProcess = false;
            }
            else if (res.status == 0) {
              this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
              this.close(this.data)
            }
          });
        }
      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
