import { Component, ViewChild, HostListener, OnInit, AfterViewInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../../app.settings';
import { Settings, ApiResponseData } from '../../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ProjectsCustomerGroupsEditComponent } from './edit/customer-groups-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage';
import { ProjectsService } from '../projects.service';
import { CustomerGroup, CustomerGroupCommand, Project } from '../projects.model';
import { CustomerGroupsResourceEditComponent } from './resource-edit/customer-groups-resource-edit.component';
@Component({
  selector: 'app-projects-customer-groups',
  templateUrl: './customer-groups.component.html',
  providers: [ProjectsService, AppCommon]
})

export class ProjectCustomerGroupsComponent implements OnInit, AfterViewInit {
  @Input() project: Project;
  @Output() isCommit: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  heightTable: number = 600;
  listData: CustomerGroup[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, projectId: 0, sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;

  displayedColumns = ["stt", "name", "note", "isDefault", "action"];

  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public projectsService: ProjectsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  ngOnInit() {
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.projectId = this.project.id;
    this.projectsService.customerGroupSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  firsLoad() {
    this.fixHeightTable();
    this.search();
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: CustomerGroup) {
    var model = new CustomerGroupCommand();
    if (data == null) {
      model.id = 0;
      model.projectId = this.params.projectId;
    }
    else {
      model = new CustomerGroupCommand().deserialize(data);
    }
    this.dialog.open(ProjectsCustomerGroupsEditComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
        this.isCommit.emit(true);
      }
    });
  }

  openEditLanguageDialog(customerGroupId: number, language: string)
  {
    this.dialog.open(CustomerGroupsResourceEditComponent, {
      data: { customerGroupId: customerGroupId, language: language },
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
      }
    });
  }

  ngAfterViewInit() {
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null && this.fix_scroll_table_rec != undefined) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 30;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;
  @ViewChild('headTable_rec', { static: false }) headTable_rec: ElementRef;
  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable();
  }
}