import { Component, OnInit, Inject, ViewChild, ElementRef, NgZone } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { MapsAPILoader } from '@agm/core';
import { ProjectsService } from '../../projects.service';
import { AppCommon } from 'src/app/app.common';
import { CustomerGroupResourceCommand, ProjectResourceCommand } from '../../projects.model';
import { AppStorage } from 'src/app/app.storage';

@Component({
  selector: 'app-customer-groups-resource-edit',
  templateUrl: './customer-groups-resource-edit.component.html',
  providers: [
    ProjectsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class CustomerGroupsResourceEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<CustomerGroupsResourceEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: CustomerGroupResourceCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public projectsService: ProjectsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      name: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(200)])],
      customerGroupId: [0],
      language: ["en"],
      languageId: [0]
    });
  }

  ngOnInit() {
    this.form.get('customerGroupId').setValue(this.data.customerGroupId);
    this.form.get('language').setValue(this.data.language);
    this.getData();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.projectsService.customerGroupResourceGetById(this.data.customerGroupId, this.data.language).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new CustomerGroupResourceCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.projectsService.customerGroupResourceEdit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
        } 
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.close(res)
        this.isProcess.saveProcess = false;
      });
    }
  }
  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
