import { Account } from "../../customer/accounts/accounts.model";
import { User } from "../../user/users/users.model";

export class Project {
  id: number;
  name: string;
  description: string;
  code: string;
  isPrivate: boolean;
  status: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  lat: number;
  lng: number;
  defaultZoom: number;
  createdUserObject: User;
  updatedUserObject: User;
  headquartersLat: number;
  headquartersLng: number;
}

export class ProjectCommand {
  id: number;
  name: string;
  description: string;
  code: string;
  isPrivate: boolean;
  status: number;
  lat: number;
  lng: number;
  defaultZoom: number;
  headquartersLat: number;
  headquartersLng: number;

  deserialize(input: Project): ProjectCommand {
    if (input == null) {
      input = new Project();
    }
    this.id = input.id;
    this.name = input.name;
    this.description = input.description;
    this.code = input.code;
    this.isPrivate = input.isPrivate;
    this.status = input.status;
    this.lat = input.lat;
    this.lng = input.lng;
    this.defaultZoom = input.defaultZoom;
    this.headquartersLat = input.headquartersLat;
    this.headquartersLng = input.headquartersLng;
    return this;
  }
}

export class CustomerGroup {
  id: number;
  projectId: number;
  name: string;
  note: string;
  order: number;
  createdDate: Date;
  updatedDate: Date;
  status: boolean;
  isDefault: boolean;
  ticketPrices: TicketPrice[];
  isAccountActive: boolean;
}

export class CustomerGroupCommand {
  id: number;
  name: string;
  order: number;
  note: string;
  projectId: number;
  createdDate: Date;
  updatedDate: Date;
  status: boolean;
  isDefault: boolean;

  deserialize(input: CustomerGroup): CustomerGroupCommand {
    if (input == null) {
      input = new CustomerGroup();
    }
    this.id = input.id;
    this.name = input.name;
    this.order = input.order;
    this.createdDate = input.createdDate;
    this.updatedDate = input.updatedDate;
    this.isDefault = input.isDefault;
    this.status = input.status;
    this.note = input.note;
    this.projectId = input.projectId;
    return this;
  }
}

export class TicketPrice {

  id: number;
  projectId: number;
  customerGroupId: number;
  ticketType: number;
  allowChargeInSubAccount: boolean;
  ticketValue: number;
  blockPerMinute: number;
  blockPerViolation: number;
  blockViolationValue: number;
  limitMinutes: number;
  isDefault: boolean;
  isActive: boolean;
  createDate: Date;
  updateDate: Date;
  name: string;
  note: string;
}

export class TicketPriceCommand {

  id: number;
  projectId: number;
  customerGroupId: number;
  ticketType: number;
  allowChargeInSubAccount: boolean;
  ticketValue: number;
  blockPerMinute: number;
  blockPerViolation: number;
  blockViolationValue: number;
  limitMinutes: number;
  isDefault: boolean;
  isActive: boolean;
  createDate: Date;
  updateDate: Date;
  name: string;

  deserialize(input: TicketPrice): TicketPriceCommand {
    if (input == null) {
      input = new TicketPrice();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.customerGroupId = input.customerGroupId;
    this.ticketType = input.ticketType;
    this.allowChargeInSubAccount = input.allowChargeInSubAccount;
    this.ticketValue = input.ticketValue;
    this.blockPerMinute = input.blockPerMinute;
    this.blockPerViolation = input.blockPerViolation;
    this.blockViolationValue = input.blockViolationValue;
    this.limitMinutes = input.limitMinutes;
    this.isDefault = input.isDefault;
    this.isActive = input.isActive;
    this.createDate = input.createDate;
    this.updateDate = input.updateDate;
    this.name = input.name;
    return this;
  }
}

export class ProjectAccount {
  id: number;
  projectId: number;
  investorId: number;
  accountId: number;
  customerGroupId: number;
  sourceType: number;
  dateImport: Date;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updateUser: number;
  account: Account;
}

export class ProjectAccountCommand {
  id: number;
  projectId: number;
  investorId: number;
  accountId: number;
  customerGroupId: number;
  sourceType: number;

  deserialize(input: ProjectAccount): ProjectAccountCommand {
    if (input == null) {
      input = new ProjectAccount();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.accountId = input.accountId;
    this.customerGroupId = input.customerGroupId;
    this.sourceType = input.sourceType;
    return this;
  }
}

export class LogAdmin {
  id: number;
  action: string;
  object: object;
  objectId: number;
  objectType: string;
  createdDate: Date;
  systemUserId: number;
  type: number;
  systemUserObject: User
}


export class ProjectAccountEdit {
  projectId: number;
  customerGroupId: number;
  accountId: number;
}

export class ProjectResource {
  id: number;
  projectId: number;
  name: string;
  languageId: string;
  language: string;

}

export class ProjectResourceCommand  {
  id: number;
  projectId: number;
  name: string;
  languageId: string;
  language: string;

  deserialize(input: ProjectResource): ProjectResourceCommand {
    if (input == null) {
      input = new ProjectResource();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.name = input.name;
    this.languageId = input.languageId;
    this.language = input.language;
    return this;
  }
}


export class CustomerGroupResource {
  id: number;
  customerGroupId: number;
  name: string;
  languageId: string;
  language: string;

}

export class CustomerGroupResourceCommand  {
  id: number;
  customerGroupId: number;
  name: string;
  languageId: string;
  language: string;

  deserialize(input: CustomerGroupResource): CustomerGroupResourceCommand {
    if (input == null) {
      input = new CustomerGroupResource();
    }
    this.id = input.id;
    this.customerGroupId = input.customerGroupId;
    this.name = input.name;
    this.languageId = input.languageId;
    this.language = input.language;
    return this;
  }
}