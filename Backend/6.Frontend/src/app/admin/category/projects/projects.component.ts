import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ProjectsEditComponent } from './edit/projects-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';
import { ProjectsService } from './projects.service';
import { Project, ProjectCommand } from './projects.model';
import { ProjectsDetailsComponent } from './details/projects-details.component';
import { ProjectsHeadquartersComponent } from './headquarters/projects-headquarters.component';
import { HttpEventType } from '@angular/common/http';
import { ReportService } from '../../report/report.service';
import { ProjectsResourceEditComponent } from './resource-edit/projects-resource-edit.component';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  providers: [ProjectsService, ReportService, AppCommon]
})

export class ProjectsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: Project[];
  isProcess = { dialogOpen: false, listDataTable: true, export: false };
  params: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "", status: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "id", "code", "name", "createdUser", "createdDate", "updatedUser", "updatedDate", "status", "action"];
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public projectsService: ProjectsService,
    public reportService: ReportService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.appCommon.changeUrl(location.pathname, this.params);
    this.projectsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  export() {
    this.isProcess['export'] = true;

    var report_name = "BC_DuAn.xlsx";
    var params = this.params;

    this.reportService.export("reportCategory", "project", params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess['export'] = true;
          break;
        case HttpEventType.Response:
          this.isProcess['export'] = false;
          const downloadedFile = new Blob([data.body], { type: data.body.type });
          const a = document.createElement('a');
          a.setAttribute('style', 'display:none;');
          document.body.appendChild(a);
          a.download = report_name;
          a.href = URL.createObjectURL(downloadedFile);
          a.target = '_blank';
          a.click();
          document.body.removeChild(a);
          this.isProcess['export'] = false;
          break;
      }
    });
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: Project) {
    var model = new ProjectCommand();
    if (data == null) {
      model.id = 0;
    }
    else {
      model = new ProjectCommand().deserialize(data);
    }
    this.dialog.open(ProjectsEditComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  public openDetailsDialog(data: Project) {
    var dataModel = new ProjectCommand().deserialize(data);
    this.dialog.open(ProjectsDetailsComponent, {
      data: { project: data, projectCommand: dataModel },
      panelClass: 'dialog-full',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
      }
    });
  }

  openEditLanguageDialog(projectId: number, language: string)
  {
    this.dialog.open(ProjectsResourceEditComponent, {
      data: { projectId: projectId, language: language },
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
      }
    });
  }

  public openHeadquarterDialog(data: Project) {
    var model = new ProjectCommand().deserialize(data);
    this.dialog.open(ProjectsHeadquartersComponent, {
      data: model,
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}