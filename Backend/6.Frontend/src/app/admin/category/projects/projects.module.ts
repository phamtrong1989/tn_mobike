import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { ProjectsEditComponent } from './edit/projects-edit.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { ProjectsCustomerGroupsEditComponent } from './customer-groups/edit/customer-groups-edit.component';
import { ProjectCustomerGroupsComponent } from './customer-groups/customer-groups.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { ProjectsComponent } from './projects.component';
import { ProjectsDetailsComponent } from './details/projects-details.component';
import { ProjectsAccountsComponent } from './projects-accounts/projects-accounts.component';
import { ProjectsAccountsEditComponent } from './projects-accounts/edit/projects-accounts-edit.component';
import { ProjectsTicketPricesEditComponent } from './ticket-prices/edit/projects-ticket-prices-edit.component';
import { ProjectsTicketPricesComponent } from './ticket-prices/projects-ticket-prices.component';
import { ProjectsLogComponent } from './log/projects-log.component';
import { AgmCoreModule } from '@agm/core';
import { ProjectsHeadquartersComponent } from './headquarters/projects-headquarters.component';
import { ProjectsResourceEditComponent } from './resource-edit/projects-resource-edit.component';
import { CustomerGroupsResourceEditComponent } from './customer-groups/resource-edit/customer-groups-resource-edit.component';

export const routes = [
  { path: '', component: ProjectsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule,
    NgxCurrencyModule,
    AgmCoreModule,
	 TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    }) 
  ],
  declarations: [
    ProjectsComponent,
    ProjectsEditComponent,
    ProjectsDetailsComponent,
    ProjectCustomerGroupsComponent,
    ProjectsCustomerGroupsEditComponent,
    ProjectsTicketPricesComponent,
    ProjectsTicketPricesEditComponent,
    ProjectsAccountsComponent,
    ProjectsAccountsEditComponent,
    ProjectsLogComponent,
    ProjectsHeadquartersComponent,
    ProjectsResourceEditComponent,
    CustomerGroupsResourceEditComponent
  ],
  entryComponents:[
    ProjectsEditComponent,
    ProjectsDetailsComponent,
    ProjectCustomerGroupsComponent,
    ProjectsCustomerGroupsEditComponent,
    ProjectsTicketPricesComponent,
    ProjectsTicketPricesEditComponent,
    ProjectsAccountsComponent,
    ProjectsAccountsEditComponent,
    ProjectsLogComponent,
    ProjectsHeadquartersComponent,
    ProjectsResourceEditComponent,
    CustomerGroupsResourceEditComponent
  ]
})
export class ProjectsModule { }
