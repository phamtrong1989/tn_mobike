import { Component, OnInit, Inject, ViewChild, ElementRef, NgZone } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { ProjectsService } from '../projects.service';
import { ProjectCommand } from '../projects.model';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-projects-headquarters',
  templateUrl: './projects-headquarters.component.html',
  providers: [
    ProjectsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class ProjectsHeadquartersComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  private geoCoder: any;
  selectedMarker: Marker;
  location: Location
  @ViewChild('search')
  public searchElementRef: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<ProjectsHeadquartersComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ProjectCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public projectsService: ProjectsService,
    public appCommon: AppCommon,
    private translate: TranslateService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone

  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [this.data.id],
      lat: [0],
      lng: [0]
    });
  }

  ngOnInit() {
    this.form.get('id').setValue(this.data.id);
    this.form.get('lat').setValue(this.data.headquartersLat);
    this.form.get('lng').setValue(this.data.headquartersLng);
    this.initMap();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.projectsService.headquartersEdit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
          } 
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.close(res)
        });
      }
    }
  }
  
  initMap() {
    this.location = {
      latitude: 10.912368,
      longitude: 106.7656889,
      mapType: "street view",
      zoom: 15,
      markers: []
    }
  
    if (this.data.id > 0 && this.data.headquartersLat != 0 && this.data.headquartersLng != 0 && this.data.headquartersLat != null && this.data.headquartersLng != null) 
    {
      this.addMarker(this.data.headquartersLat, this.data.headquartersLng)
    }

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.addMarkerSerch(place.geometry.location.lat(), place.geometry.location.lng());
        });
      });
    });
  }

  addMarkerSerch(lat: number, lng: number) {
    this.addMarker(lat, lng);
  }

  
  zoomChange(event: any) {
    this.location.zoom = event;
  }

  addMarker(lat: number, lng: number) {
    this.location.markers = [];
    this.location.markers.push({
      lat,
      lng
    });
    this.location.latitude = lat;
    this.location.longitude = lng;
    this.form.get('lat').setValue(lat);
    this.form.get('lng').setValue(lng);
  }

  selectMarker(event: any) {
    this.selectedMarker = {
      lat: event.latitude,
      lng: event.longitude
    };
    this.form.get('lat').setValue(event.latitude);
    this.form.get('lng').setValue(event.longitude);
  }

  markerDragEnd($event: any) {
    this.form.get('lat').setValue($event.coords.lat);
    this.form.get('lng').setValue($event.coords.lng);
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
interface Marker {
  lat: number;
  lng: number;
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  markers: Marker[];
}