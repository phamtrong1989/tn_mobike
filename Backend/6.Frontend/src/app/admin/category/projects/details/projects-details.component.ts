import { Component, OnInit, Inject, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { Subject, ReplaySubject } from 'rxjs';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { ClientSettings, PullData } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AnyNaptrRecord } from 'dns';
import { ProjectsEditComponent } from '../edit/projects-edit.component';
import { ProjectCustomerGroupsComponent } from '../customer-groups/customer-groups.component';
import { CustomerGroup, Project, ProjectCommand } from '../projects.model';
import { ProjectsService } from '../projects.service';
import { ProjectsAccountsComponent } from '../projects-accounts/projects-accounts.component';
import { ProjectsLogComponent } from '../log/projects-log.component';
import { ProjectsHeadquartersComponent } from '../headquarters/projects-headquarters.component';
@Component({
  selector: 'app-projects-details',
  templateUrl: './projects-details.component.html',
  providers: [
    ProjectsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ProjectsDetailsComponent implements OnInit, AfterViewInit {

  public clientSettings: ClientSettings;
  protected _onDestroy = new Subject<void>();
  public form: FormGroup;
  public categoryData: PullData;
  public heightScroll: number = 828;
  viewType: number;
  customerGroups: CustomerGroup[];
  isProcess = { saveProcess: false, dialogOpen: false, initData: false, listCustomerGroup: false };
  constructor(
    public dialogRef: MatDialogRef<ProjectsDetailsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { project: Project, projectCommand: ProjectCommand },
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public projectsService: ProjectsService,
    public appCommon: AppCommon,
    appClientSettings: AppClientSettings,
    private cdr: ChangeDetectorRef
  ) {
    this.clientSettings = appClientSettings.settings
    this.categoryData = this.appStorage.getCategoryData();
    this.isProcess.initData = true;
    this.getData();
  }

  getData() {
    this.projectsService.getById(this.data.project.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data.projectCommand = new ProjectCommand().deserialize(rs.data);
        this.data.project = rs.data;
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
      }
    });
  }

  ngOnInit() {
  }

  refeshData()
  {

  }

  getFullUrl(url: string) {
    return this.clientSettings.serverAPI + url;
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.projectsService.edit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open("Cập nhật tài khoản thành công", "Thành công", { duration: 5000 });
          this.close(res)
        }
        else {
          this.snackBar.open("Có lỗi hoặc phiên làm việc đã kết thúc, vui lòng F5 refesh lại trình duyệt và thử lại", "Cảnh báo", { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  public openHeadquarterDialog() {
    this.dialog.open(ProjectsHeadquartersComponent, {
      data: this.data.projectCommand,
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close(1);
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  public openCustomersEditDialog() {
    var data = this.data.project;
    var model = new ProjectCommand();

    if (data == null) {
      model.id = 0;
    }
    else {
      model = new ProjectCommand().deserialize(data);
    }

    this.dialog.open(ProjectsEditComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  getCustomerGroups(): void {
    this.isProcess.initData = true;
    this.projectsService.customerGroupSearchPaged({ projectId: this.data.project.id, pageIndex: 1, pageSize: 100}).then((res) => {
      this.customerGroups = res.data;
      this.isProcess.initData = false;
    });
  }

  matTabGroupChange(e: any, cg: ProjectCustomerGroupsComponent, te : ProjectsAccountsComponent, log: ProjectsLogComponent) {
     if(e.index == 1)
    {
      cg.firsLoad();
    }
    else if(e.index == 2)
    {
      te.firsLoad();
    }
    else if(e.index == 3)
    {
      this.getCustomerGroups();
    }
    else if(e.index == 4)
    {
      log.firsLoad();
    }
  }

  fixHeightScroll() {
    let getTop = document.getElementById("fix_detail").getBoundingClientRect().top;
    this.heightScroll = window.innerHeight - getTop - 30;
    this.viewType = 0;
  }

  ngAfterViewInit(): void {
    this.fixHeightScroll();
    this.cdr.detectChanges();
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
  
    this.fixHeightScroll();
  }
}
