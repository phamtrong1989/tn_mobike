import { Component, OnInit, Inject, ViewChild, ElementRef, NgZone } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { ProjectsService } from '../projects.service';
import { ProjectCommand } from '../projects.model';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-projects-edit',
  templateUrl: './projects-edit.component.html',
  providers: [
    ProjectsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class ProjectsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  private geoCoder: any;
  selectedMarker: Marker;
  location: Location
  @ViewChild('search')
  public searchElementRef: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<ProjectsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ProjectCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public projectsService: ProjectsService,
    public appCommon: AppCommon,
    private translate: TranslateService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone

  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      name: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(200)])],
      description: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(1000)])],
      code: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(20)])],
      isPrivate: [false],
      defaultZoom: [0],
      lat: [0],
      lng: [0],
      status: [1]
    });
  }

  ngOnInit() {
    this.initMap();
    if (this.data.id > 0) {
      this.form.setValue(this.data);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.projectsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new ProjectCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.projectsService.edit(this.form.value).then((res) => {
          console.log(res.status);

          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          } else if (res.status == 2) {
            this.snackBar.open("Mã đơn vị đã tồn tại, vui lòng thử lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.projectsService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new ProjectCommand().deserialize(res.data);
            this.form.setValue(this.data);
            this.close(res)

          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.projectsService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });
      }
    });
  }

  
  initMap() {
    this.location = {
      latitude: 10.912368,
      longitude: 106.7656889,
      mapType: "street view",
      zoom: 15,
      markers: []
    }
  
    if (this.data.id > 0 && this.data.lat != 0 && this.data.lng != 0 && this.data.lat != null && this.data.lng != null) {
    
      this.addMarker(this.data.lat, this.data.lng)
    }

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.addMarkerSerch(place.geometry.location.lat(), place.geometry.location.lng());
        });
      });
    });
  }

  addMarkerSerch(lat: number, lng: number) {
    this.addMarker(lat, lng);
  }

  
  zoomChange(event: any) {
    this.location.zoom = event;
    this.form.get('defaultZoom').setValue(this. location.zoom);
  }

  addMarker(lat: number, lng: number) {
    this.location.markers = [];
    this.location.markers.push({
      lat,
      lng
    });

    this.location.latitude = lat;
    this.location.longitude = lng;
    this.location.zoom = this.data.defaultZoom <= 0 ? 15 : this.data.defaultZoom;

    this.form.get('lat').setValue(lat);
    this.form.get('lng').setValue(lng);
    this.form.get('defaultZoom').setValue(this. location.zoom);
  }

  selectMarker(event: any) {
    this.selectedMarker = {
      lat: event.latitude,
      lng: event.longitude
    };
    this.form.get('lat').setValue(event.latitude);
    this.form.get('lng').setValue(event.longitude);
  }

  markerDragEnd($event: any) {
    this.form.get('lat').setValue($event.coords.lat);
    this.form.get('lng').setValue($event.coords.lng);
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
interface Marker {
  lat: number;
  lng: number;
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  markers: Marker[];
}