import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { CustomerGroup, CustomerGroupCommand, CustomerGroupResource, CustomerGroupResourceCommand, LogAdmin, Project, ProjectAccount, ProjectAccountCommand, ProjectCommand, ProjectResource, ProjectResourceCommand, TicketPrice, TicketPriceCommand } from './projects.model';
import { ApiResponseData,  ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { Account } from '../../customer/accounts/accounts.model';

@Injectable()
export class ProjectsService {
    public url = "/api/work/Projects";
     _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon :AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<Project[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
   
    create(user: ProjectCommand) {
        return this.http.post<ApiResponseData<Project>>(this.url + "/create", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: ProjectCommand) {
        return this.http.put<ApiResponseData<Project>>(this.url + "/edit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    getById(id: number) {
        return this.http.get<ApiResponseData<Project>>(this.url + "/getById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    delete(id: number) {
        return this.http.delete<ApiResponseData<Project>>(this.url + "/delete/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    // CUSTOMER-GROUP
    customerGroupSearchPaged(params: any) {
        return this.http.get<ApiResponseData<CustomerGroup[]>>(this.url + "/customerGroupSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    headquartersEdit(user: any) {
        return this.http.put<ApiResponseData<Project>>(this.url + "/headquartersEdit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    customerGroupCreate(user: CustomerGroupCommand) {
        return this.http.post<ApiResponseData<CustomerGroup>>(this.url + "/customerGroupCreate", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    customerGroupEdit(user: CustomerGroupCommand) {
        return this.http.put<ApiResponseData<CustomerGroup>>(this.url + "/customerGroupEdit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    customerGroupGetById(id: number) {
        return this.http.get<ApiResponseData<CustomerGroup>>(this.url + "/customerGroupGetById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    customerGroupDelete(id: number) {
        return this.http.delete<ApiResponseData<CustomerGroup>>(this.url + "/customerGroupDelete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    // TICKET-PRICE
    ticketPriceSearchPaged(params: any) {
        return this.http.get<ApiResponseData<TicketPrice[]>>(this.url + "/ticketPriceSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
   
    ticketPriceCreate(user: TicketPriceCommand) {
        return this.http.post<ApiResponseData<TicketPrice>>(this.url + "/ticketPriceCreate", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    ticketPriceEdit(user: TicketPriceCommand) {
        return this.http.put<ApiResponseData<TicketPrice>>(this.url + "/ticketPriceEdit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    ticketPriceGetById(id: number) {
        return this.http.get<ApiResponseData<TicketPrice>>(this.url + "/ticketPriceGetById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    ticketPriceDelete(id: number) {
        return this.http.delete<ApiResponseData<TicketPrice>>(this.url + "/ticketPriceDelete/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }


    // Project-ACCOUNT
    projectAccountSearchPaged(params: any) {
        return this.http.get<ApiResponseData<ProjectAccount[]>>(this.url + "/ProjectAccountSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
   
    projectAccountCreate(user: ProjectAccountCommand) {
        return this.http.post<ApiResponseData<ProjectAccount>>(this.url + "/ProjectAccountCreate", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    projectAccountEdit(user: ProjectAccountCommand) {
        return this.http.put<ApiResponseData<ProjectAccount>>(this.url + "/ProjectAccountEdit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    projectAccountGetById(id: number) {
        return this.http.get<ApiResponseData<ProjectAccount>>(this.url + "/ProjectAccountGetById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    projectAccountDelete(id: number) {
        return this.http.delete<ApiResponseData<ProjectAccount>>(this.url + "/ProjectAccountDelete/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    accountGetByPhone(phone: string) {
        return this.http.get<ApiResponseData<Account>>(this.url + "/AccountGetByPhone/" + phone).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    logSearchPaged(params: any) {
        return this.http.get<ApiResponseData<LogAdmin[]>>(this.url + "/LogSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    projectResourceEdit(user: ProjectResourceCommand) {
        return this.http.put<ApiResponseData<ProjectResource>>(this.url + "/ProjectResourceEdit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    projectResourceGetById(projectId: number, language: string) {
        return this.http.get<ApiResponseData<ProjectResource>>(this.url + "/ProjectResourceGetById/" + projectId+"/"+language).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    customerGroupResourceEdit(user: CustomerGroupResourceCommand) {
        return this.http.put<ApiResponseData<CustomerGroupResource>>(this.url + "/customerGroupResourceEdit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    customerGroupResourceGetById(customerGroupId: number, language: string) {
        return this.http.get<ApiResponseData<CustomerGroupResource>>(this.url + "/CustomerGroupResourceGetById/" + customerGroupId+"/"+language).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    
	 handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
} 