export class City {

id: number;
privateId: number;
name: string;
createdDate: Date;
createdUser: number;
updatedDate: Date;
updatedUser: number;


}

export class CityCommand {

 id: number;
privateId: number;
name: string;

  deserialize(input: City): CityCommand {
    if(input==null)
    {
      input  = new City();
    }
	this.id = input.id;
this.privateId = input.privateId;
this.name = input.name;
    return this;
  }
}