export class RedeemPoint {
  id: number;
  name: string;
  tripPoint: number;
  toPoint: number;
  note: string;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  createdDate: Date;
}

export class RedeemPointCommand {
  id: number;
  name: string;
  tripPoint: number;
  toPoint: number;
  note: string;

  deserialize(input: RedeemPoint): RedeemPointCommand {
    if (input == null) {
      input = new RedeemPoint();
    }
    this.id = input.id;
    this.name = input.name;
    this.tripPoint = input.tripPoint;
    this.toPoint = input.toPoint;
    this.note = input.note;

    return this;
  }
}