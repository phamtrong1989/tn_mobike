export class District {

id: number;
privateId: number;
cityId: number;
name: string;
createdDate: Date;
createdUser: number;
updatedDate: Date;
updatedUser: number;


}

export class DistrictCommand {

 id: number;
privateId: number;
cityId: number;
name: string;



  deserialize(input: District): DistrictCommand {
    if(input==null)
    {
      input  = new District();
    }
	this.id = input.id;
this.privateId = input.privateId;
this.cityId = input.cityId;
this.name = input.name;


   
    return this;
  }
}