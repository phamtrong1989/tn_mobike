export class Model {

id: number;
producerId: number;
name: string;
createdDate: Date;
createdUser: number;
updatedDate: Date;
updatedUser: number;


}

export class ModelCommand {

 id: number;
producerId: number;
name: string;



  deserialize(input: Model): ModelCommand {
    if(input==null)
    {
      input  = new Model();
    }
	this.id = input.id;
this.producerId = input.producerId;
this.name = input.name;


   
    return this;
  }
}