import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ApiResponseData, ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AppCommon } from 'src/app/app.common';
import { GpsData, Station } from '../infrastructure/stations/stations.model';
import { Bike } from '../infrastructure/bikes/bikes.model';
import {  BookingOpenLockCommand } from '../transaction/bookings/bookings.model';
import { Account } from '../customer/accounts/accounts.model';
import { User } from '../user/users/users.model';
@Injectable()
export class BikeNotInBookingService {

    public url = "/api/work/BikeNotInBooking";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    stations(projectId: number) {
        return this.http.get<ApiResponseData<Station[]>>(this.url + "/Stations/" + projectId, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    bikeNotBookings(projectId: number) {
        return this.http.get<ApiResponseData<Bike[]>>(this.url + "/BikeNotBookings/" + projectId, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    getByIMEI(imei: string, time: string) {
        return this.http.get<ApiResponseData<GpsData[]>>(this.url + "/getByIMEI?imei=" + imei + "&start=" + time, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

  
    openLock(cm: BookingOpenLockCommand) {
        return this.http.post<ApiResponseData<object>>(this.url + "/OpenLock", cm).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByAccount(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchByAccount", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bikeGetById(id: number) {
        return this.http.get<ApiResponseData<Bike>>(this.url + "/bikeGetById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    gpsByDock(params: any) {
        return this.http.get<ApiResponseData<GpsData[]>>(this.url + "/gpsByDock", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByUser(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchByUser", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
 
    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}