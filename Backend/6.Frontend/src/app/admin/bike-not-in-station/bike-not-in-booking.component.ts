import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AppClientSettings, AppSettings } from '../../app.settings';
import { ClientSettings, Settings } from '../../app.settings.model';
import { TranslateService } from '@ngx-translate/core';
import { AppStorage } from 'src/app/app.storage';
import { AccountData } from '../account/account.model';
import { AppCommon } from 'src/app/app.common';
import { GpsData, Station } from '../infrastructure/stations/stations.model';
import { Booking } from '../transaction/bookings/bookings.model';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { OpenLockComponent } from './open-lock/open-lock.component';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Account } from '../customer/accounts/accounts.model';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { Bike } from '../infrastructure/bikes/bikes.model';
import { MatTableDataSource } from '@angular/material/table';
import { DashboardBikesMapComponent } from './bikes/map/bikes-map.component';
import { LogAdmin } from '../category/projects/projects.model';
import { RoleController } from '../user/role-controllers/role-controllers.model';
import { User } from '../user/users/users.model';
import { BikeNotInBookingService } from './bike-not-in-booking.service';
import { BookingFail } from '../dashboard/dashboard.model';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-bike-not-in-booking',
  templateUrl: './bike-not-in-booking.component.html',
  providers: [AppCommon, BikeNotInBookingService]
})
export class BikeNotInBookingComponent implements OnInit, OnDestroy {
  location: Location
  public settings: Settings;
  accountInfo: AccountData;
  viewType: number = 0;
  isProcess = { listDataTable: false, cancelBooking: false, endBooking: false, openLock: false, accountSearching: false, userSearching: false, dialogOpen: false };
  heightTable: number = 900;
  heightTabTable: number = 210;
  iconSize: number = 20;
  stations: Station[] = [];
  searchStations: Station[] = [];
  searchBookingFail: BookingFail[] = [];

  currentStationMarker: StationMarker;
  currentBikeNotBooking: BikeNotBookingMarker;

  iconStation_DuXe: any = { url: '../assets/img/vendor/leaflet/station-success.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_ThuaXe: any = { url: '../assets/img/vendor/leaflet/station-duxe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_ThieuXe: any = { url: '../assets/img/vendor/leaflet/station-thieuXe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_HetXe: any = { url: '../assets/img/vendor/leaflet/station-hetxe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconBike: any = { url: '../assets/img/vendor/leaflet/bike-icon.png', scaledSize: { height: 20, width: 20 }, anchor: { x: 10, y: 10 } };
  iconBike_black: any = { url: '../assets/img/vendor/leaflet/bike-icon-black.png', scaledSize: { height: 20, width: 20 }, anchor: { x: 10, y: 10 } };

  categoryData: any;
  heightListBikeRight: number = 514;
  accountKey: FormControl = new FormControl("");
  isShowStation: FormControl = new FormControl(true); 
  isBikeNotBooking: FormControl = new FormControl(true); 

  mapStyle: any = [
    { featureType: "road", stylers: [{ visibility: "on" }] },
    { featureType: "poi", stylers: [{ visibility: "off" }] },
    { featureType: "transit", stylers: [{ visibility: "off" }] },
    { featureType: "administrative", stylers: [{ visibility: "off" }] },
    { featureType: "administrative.locality", stylers: [{ visibility: "off" }] }
  ];
  displayedStationColumns = ["name", "cityId", "address", "info", "totalBike", "totalBikeGood", "totalBikeNotGood"];
  bikeDisplayedColumns = ["serialNumber", "dockId", "battery", "lockStatus", "connectionStatus", "type", "stationId", "bookingStatus", "action"];
  stationParams: any = { key: "", investorId: "", cityId: "", districtId: "", status: "", projectId: "" };
  bikeParams: any = { pageIndex: 1, pageSize: 30, key: "", sortby: "", sorttype: "" };
  bookingParams: any = { key: "", investorId: "", plate: "" };
  eventSystemParams: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  logSystemParams: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  showMarkerParams: any = { booking: true, station: true }
  bikeNotBookingParams: any = { key: "", projectId: "", plate: "" };
  intevalStation: any;
  intevalBooking: any;
  intevalBookingFail: any;
  intevalPolylines: any;
  polylines: GpsData[];
  bikeNotBookings: Bike[] = [];
  intevalAccountOnline: any;
  ctrlProjectId = new FormControl();
  systemUserKey = new FormControl();
  users: User[];
  private hubConnection: HubConnection;
  public clientSettings: ClientSettings;
  typeStatistic: string = 'day';
  accounts: Account[];
  bikeList: Bike[];
  logAdminList: LogAdmin[];
  stopEvent: boolean = false;
  roleControllers: RoleController[];
  statisticDate: Date = new Date();
  accountOnline: any;
  isFirst: boolean = false;

  constructor(
    public appSettings: AppSettings,
    public appStorage: AppStorage,
    public bikeNotInBookingService: BikeNotInBookingService,
    public appCommon: AppCommon,
    public dialog: MatDialog,
    appClientSettings: AppClientSettings,
    private _notifications: NotificationsService

  ) {
    this.settings = this.appSettings.settings;
    this.accountInfo = this.appStorage.getAccountInfo();
    this.categoryData = appStorage.getCategoryData();
    this.searchStations = [];
    this.clientSettings = appClientSettings.settings
    this.roleControllers = this.categoryData.roleControllers.filter(x => x.id != 'Account' && x.id != 'Base`2' && x.id != 'Feedbacks' && x.id != 'Home' && x.id != 'InitData' && x.id != 'TicketPrices' && x.id != 'RoleGroups' && x.id != 'RoleAreas' && x.id != 'Public' && x.id != 'ReportInfrastructure')
    if (this.appStorage.getProjectId() == null || this.appStorage.getProjectId() == undefined) {
      this.appStorage.setProjectId(appStorage.getCategoryData().projects[0].id);
    }

    this.ctrlProjectId.setValue(this.appStorage.getProjectId());

    this.ctrlProjectId.valueChanges.subscribe(res => {
      this.appStorage.setProjectId(res);
      this.initData();
    });

    this.isShowStation.valueChanges.subscribe(res => {
      this.location.stationMarkers.forEach(x=>{
        x.isShow = res;
      });
    });

    this.isBikeNotBooking.valueChanges.subscribe(res => {
      this.location.bikeNotBookings.forEach(x=>{
        x.isShow = res;
      });
    });
  }

  bikeSource: MatTableDataSource<Bike[]>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('eventSystemPaginator') eventSystemPaginator: MatPaginator;
  @ViewChild('bikePaginator') bikePaginator: MatPaginator;
  @ViewChild('logPaginator') logPaginator: MatPaginator;

  ngOnDestroy(): void {
    this.stopHub();
    if (this.intevalStation) {
      clearInterval(this.intevalStation);
    }
    if (this.intevalPolylines) {
      clearInterval(this.intevalPolylines);
    }
    if (this.intevalBooking) {
      clearInterval(this.intevalBooking);
    }
    if (this.intevalAccountOnline) {
      clearInterval(this.intevalAccountOnline);
    }
  }

  initData() {
    var projectObject = this.appStorage.getCategoryData().projects.find(x => x.id == this.ctrlProjectId.value);
    if (projectObject != null) {
      this.location = {
        latitude: projectObject.lat,
        longitude: projectObject.lng,
        mapType: "street view",
        zoom: projectObject.defaultZoom,
        stationMarkers: [],
        bikeNotBookings: []
      }
    }
    else {
      this.location = {
        latitude: 21.0332535,
        longitude: 105.8345708,
        mapType: "street view",
        zoom: 15,
        stationMarkers: [],
        bikeNotBookings: []
      }
    }
    setTimeout(() => {
      this.getStations(true);
    }, 2000);
  }

  ngOnInit() {
    this.initData();
    this.setInteval();
  }

   getStations(isFirst: boolean = false) {

    if (isFirst) {

    }

    this.bikeNotInBookingService.stations(this.ctrlProjectId.value).then((res) => {
      this.stations = res.data;
      this.stationSearch();
      // Kiểm tra marker nào dữ liệu trả về không có thì xóa bỏ đi
      this.location.stationMarkers.forEach(element => {
        var ktTonTai = this.stations.find(x => x.id == element.station?.id);
        if (ktTonTai == null) {
          element.isEnable = false;
        }
      });
      // Kiểm tra nếu curent ko tồn tại nữa thì đóng set = null
      if (this.currentStationMarker != null && this.location.stationMarkers.find(x => this.currentStationMarker?.station?.id == x.station?.id && x.isEnable == true) == null) {
        this.currentStationMarker.isOpen = false;
        this.currentStationMarker = null;
      }
      // Cập nhật marker
      this.initIconSize();
      this.stations.forEach(station => {
        this.addStationMarker(station.lat, station.lng, station.name, false, station, true, true);
      });
    });
  }

  setInteval() {
    this.intevalStation = setInterval(() => this.getStations(), 10000);
  }

  getBikeNotBooking() {
 
    this.bikeNotInBookingService.bikeNotBookings(this.ctrlProjectId.value).then((res) => {
      this.bikeNotBookings = res.data;
      setTimeout(() => {
        //this.bookingSearch();
        // Kiểm tra marker nào dữ liệu trả về không có thì xóa bỏ đi
        this.location.bikeNotBookings.forEach(element => {
          var ktTonTai = this.bikeNotBookings.find(x => x.id == element.bike.dock?.id);
          if (ktTonTai == null) {
            element.isEnable = false;
          }
        });

        if (this.currentBikeNotBooking != null && this.location.bikeNotBookings.find(x => this.currentBikeNotBooking?.bike.dock?.id == x.bike.dock?.id && x.isEnable == true) == null) {
          this.currentBikeNotBooking.isOpen = false;
          this.currentBikeNotBooking = null;
          this.polylines = [];
        }
        // Cập nhật marker
        this.bikeNotBookings.forEach(item => {
          this.addBikeNotBookingMarker(item?.dock?.lat, item?.dock?.long, item?.plate, this.iconBike_black, false, item, true, true);
        });
        this.isProcess.dialogOpen = false;
      }, 300);
      this.isProcess.listDataTable = false;
    });
  }

  selectStationMarker(marker: StationMarker) {
    if (!marker.isEnable) {
      marker.isOpen = true;
      return;
    }
    this.viewType = 1;
    this.polylines = [];
    if (this.currentStationMarker != null) {
      this.currentStationMarker.isOpen = false;
    }
    this.currentStationMarker = marker;
    this.location.latitude = marker.lat;
    this.location.longitude = marker.lng;
    marker.isOpen = true;
  }

  selectStation(station: Station) {
    var getMarker = this.location.stationMarkers.find(x => x.station.id == station.id);
    if (getMarker != null) {
      this.selectStationMarker(getMarker);
    }
  }

  openViewmapDialog(data: Bike) {
    this.dialog.open(DashboardBikesMapComponent, {
      data: data,
      panelClass: 'dialog-full',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
    });
  }

  openViewmapDialogById(bikeId: number) {
    if (bikeId > 0) {
      this.dialog.open(DashboardBikesMapComponent, {
        data: { id: bikeId },
        panelClass: 'dialog-full',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
      });
    }
  }

  addStationMarker(lat: number, lng: number, label: string, isOpen: boolean, station: Station, isEnable: boolean, isShow: boolean) {
    var isSelect = false;
    var ktTT = this.location.stationMarkers.find(x => x.station?.id == station.id);
    var iconUrl = this.setupStationIcon(station);
    if (ktTT == null) {
      this.location.stationMarkers.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        station,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.station = station;
      ktTT.isEnable = true;
      if (this.currentStationMarker != null && this.currentStationMarker.station?.id == station.id) {
        this.currentStationMarker = ktTT;
      }
    }
  }

  changeIconMarker() {
    this.location.stationMarkers.forEach(element => {
      var newX = {
        url: element.iconUrl.url,
        scaledSize: {
          width: this.iconSize,
          height: this.iconSize
        }
        , anchor: { x: 10, y: 10 }
      };
      element.iconUrl = newX;
    });
  }

  setupStationIcon(station: Station) {
    var data = this.iconStation_DuXe;
    if (station.totalBike <= 3 && station.totalBike > 0) {
      data = this.iconStation_ThieuXe;
    }
    else if (station.totalBike == 0) {
      data = this.iconStation_HetXe;
    }
    else if ((station.totalBike - 3) >= station.spaces) {
      data = this.iconStation_ThuaXe;
    }
    return data;
  }

  addBikeNotBookingMarker(lat: number, lng: number, label: string, iconUrl: string, isOpen: boolean, bike: Bike, isEnable: boolean, isShow: boolean) {
    var isSelect = false;
    if (lat <= 0 || lng <= 0) {
      return;
    }
    var ktTT = this.location.bikeNotBookings.find(x => x.bike.dock?.id == bike.dock?.id);
    if (ktTT == null) {
      this.location.bikeNotBookings.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        bike,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.bike = bike;
      ktTT.isEnable = true;
      if (this.currentBikeNotBooking != null && this.currentBikeNotBooking.bike?.dock?.id == bike?.dock?.id) {
        this.currentBikeNotBooking = ktTT;
      }
    }
  }

  centerChange(event: any) {
  }

  zoomChange(event: any) {
    this.location.zoom = event;
    this.initIconSize();
    this.changeIconMarker();
  }

  initIconSize() {
    if (this.location.zoom >= 15) {
      this.iconSize = 20;
    }
    else if (this.location.zoom == 14) {
      this.iconSize = 15;
    }
    else if (this.location.zoom == 13) {
      this.iconSize = 14;
    }
    else if (this.location.zoom == 12) {
      this.iconSize = 13;
    }
    else if (this.location.zoom == 11) {
      this.iconSize = 12;
    }
    else if (this.location.zoom == 10) {
      this.iconSize = 11;
    }
    else if (this.location.zoom == 9) {
      this.iconSize = 10;
    }
    else if (this.location.zoom == 8) {
      this.iconSize = 8;
    }
    else if (this.location.zoom == 7) {
      this.iconSize = 7;
    }
    else if (this.location.zoom == 6) {
      this.iconSize = 6;
    }
    else {
      this.iconSize = 5;
    }
  }

  boundsChange(event: any) {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - 410;
      this.heightListBikeRight = this.heightTable - 74;
    }

    if (this.fix_scroll_table_station.nativeElement != null) {
      this.heightTabTable = 406;
    }
  }

  matTabGroupChange() {
  }

  stopHub() {
    if (this.hubConnection) {
      this.hubConnection.stop();
      this.hubConnection = undefined;
    }
  }

  sendNotify(type: NotificationType, content: string) {
    var data = {
      type: type,
      title: "Thông báo",
      content: content,
      timeOut: 15000,
      showProgressBar: true,
      pauseOnHover: true,
      clickToClose: true,
      animate: 'fromRight'
    };
    this._notifications.create("Thông báo", content, type, data)
  }

  viewChange($event: boolean, type: number) {
    if (type == 0) {
      this.location.stationMarkers.forEach(element => {
        element.isShow = $event;
      });
    }
  }

  stationSearchModelChange($event: number, type: number) {
    if (type == 0) {
      this.stationParams.investorId = $event;
    }
    else if (type == 1) {
      this.stationParams.cityId = $event;
    }
    else if (type == 2) {
      this.stationParams.projectId = $event;
    }
    this.stationSearch();
  }

  stationSearch() {
    var list = this.stations;
    if (this.stationParams.projectId != "" && this.stationParams.projectId != null) {
      list = this.stations.filter(x => x.projectId == this.stationParams.projectId);
    }
    if (this.stationParams.investorId != "" && this.stationParams.investorId != null) {
      list = this.stations.filter(x => x.investorId == this.stationParams.investorId);
    }
    if (this.stationParams.cityId != "" && this.stationParams.cityId != null) {
      list = this.stations.filter(x => x.cityId == this.stationParams.cityId);
    }
    if (this.stationParams.districtId != "" && this.stationParams.districtId != null) {
      list = this.stations.filter(x => x.districtId == this.stationParams.districtId);
    }
    if (this.stationParams.key != "" && this.stationParams.key != null) {
      list = this.stations.filter(x => x.name.toLowerCase().indexOf(this.stationParams.key.toLowerCase()) !== -1)
    }
    this.searchStations = list;
  }

  openLock() {
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('fix_scroll_table_station', { static: false }) fix_scroll_table_station: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}

interface StationMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  isSelect: boolean;
  station: Station;
  isEnable: boolean;
  isShow: boolean;
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  stationMarkers: StationMarker[];
  bikeNotBookings: BikeNotBookingMarker[]
}

interface BikeNotBookingMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  isSelect: boolean;
  bike: Bike;
  isEnable: boolean;
  isShow: boolean;
}
