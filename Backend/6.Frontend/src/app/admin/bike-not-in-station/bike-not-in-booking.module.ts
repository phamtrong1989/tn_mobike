import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxCurrencyModule } from 'ngx-currency';
import { CalendarModule } from 'angular-calendar';
import { AgmCoreModule } from '@agm/core';
import { MatNativeDateModule } from '@angular/material/core';
import { BikeNotInBookingComponent } from './bike-not-in-booking.component';
import { DashboardBikesMapComponent } from './bikes/map/bikes-map.component';
import { OpenLockComponent } from './open-lock/open-lock.component';

export const routes = [
  { path: '', component: BikeNotInBookingComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    NgxCurrencyModule,
    CalendarModule,
    AgmCoreModule,
    MatNativeDateModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    })
  ],
  declarations: [
    BikeNotInBookingComponent,
    DashboardBikesMapComponent,
    OpenLockComponent
  ],
  entryComponents: [
    DashboardBikesMapComponent,
    OpenLockComponent
  ]
})
export class BikeNotInBookingModule { }