import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NotificationJob, NotificationJobCommand } from '../notification-jobs.model';
import { NotificationJobsService } from '../notification-jobs.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { Account } from 'src/app/admin/customer/accounts/accounts.model';

@Component({
  selector: 'app-notification-jobs-edit',
  templateUrl: './notification-jobs-edit.component.html',
  providers: [
    NotificationJobsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class NotificationJobsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  accountKey: FormControl = new FormControl("");
  accounts: Account[];
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountSearching: false };
  constructor(
    public dialogRef: MatDialogRef<NotificationJobsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: NotificationJobCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public notificationJobsService: NotificationJobsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      accountId: [0],
      tile: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(100)])],
      message: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(500)])],
      publicDate: [new Date(), Validators.compose([Validators.required])],
      publicTime: ["00:00", Validators.compose([Validators.required])],
      languageId: [1],
      account: [null]
    });
    this.accounts = [];
  }

  ngOnInit() {
    this.initSearchAccount();
    this.accounts = [];
    if(this.data.account != null)
    {
        this.accounts.push(this.data.account);
    }
    if (this.data.id > 0) {
      this.form.setValue(this.data);
      this.getData();
    }
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
        this.isProcess.accountSearching = false;
        if (rs != "") {
          this.isProcess.accountSearching = true;
          this.notificationJobsService.searchByAccount({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.form.get('accountId').setValue(0);
            }
            this.accounts = rs.data;
            this.isProcess.accountSearching = false;
          });
        }
      },
        error => {
          this.isProcess.accountSearching = false;
      });
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.notificationJobsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new NotificationJobCommand().deserialize(rs.data);
        this.data.publicTime = this.appCommon.formatDateTime(this.data.publicDate, "HH:mm");
        this.form.setValue(this.data);
        this.accounts = [];
        if(this.data.account != null)
        {
            this.accounts.push(this.data.account);
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.notificationJobsService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else if (res.status == 2) {
            this.snackBar.open("Thông báo này đã gửi rồi thì không được cập nhật", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 3) {
            this.snackBar.open("Ngày gửi phải lớn hơn hoặc bằng ngày hiện tại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.notificationJobsService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.close(res);
          }
          else if (res.status == 3) {
            this.snackBar.open("Ngày gửi phải lớn hơn hoặc bằng ngày hiện tại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.notificationJobsService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
