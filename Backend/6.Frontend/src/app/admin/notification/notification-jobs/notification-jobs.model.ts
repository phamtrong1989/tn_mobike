import { Account } from "../../customer/accounts/accounts.model";

export class NotificationJob {
  id: number;
  accountId: number;
  deviceId: string;
  icon: string;
  type: number;
  tile: string;
  message: string;
  publicDate: Date;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  isSend: boolean;
  sendDate: Date;
  languageId: number;
  account: Account;
}

export class NotificationJobCommand {

  id: number;
  accountId: number;
  tile: string;
  message: string;
  publicDate: Date;
  publicTime: string;
  languageId: number;
  account: Account;

  deserialize(input: NotificationJob): NotificationJobCommand {
    if (input == null) {
      input = new NotificationJob();
    }
    this.id = input.id;
    this.accountId = input.accountId;
    this.tile = input.tile;
    this.message = input.message;
    this.publicDate = input.publicDate;
    this.languageId = input.languageId;
    this.publicTime = "00:00";
    this.account = input.account;
    return this;
  }
}