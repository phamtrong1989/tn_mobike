import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';

import { ApiResponseData, ClientSettings } from './../../app.settings.model';
import { AppClientSettings } from '../../app.settings';
import { AppCommon } from '../../app.common'

@Injectable()
export class ChartService {
  public url = "/api/work/chartReport/";
  _injector: Injector;
  public clientSettings: ClientSettings;
  constructor
    (
      public http: HttpClient,
      appClientSettings: AppClientSettings,
      public appCommon: AppCommon,
      injector: Injector
    ) {
    this.clientSettings = appClientSettings.settings
    this.url = this.clientSettings.serverAPI + this.url
    this._injector = injector;
  }

  apiUrl() {
    return this.clientSettings.serverAPI;
  }

  GetJsonData(method: string, params: any) {
    return this.http.get<any>(this.url + method, this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
  }

  getStations(params: any) {
    return this.http.get<any>(this.url + "getStations", this.appCommon.getHeaders(params)).pipe(
        shareReplay(1),
        retry(1),
        catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
    ).toPromise();
}

  handleError(error: any, injector: Injector) {
    if (error.status === 401) {
    } else {
    }
    return Promise.reject(error);
  }
}