import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { ChartService } from '../chart.services';

@Component({
    selector: 'app-chart-bike',
    templateUrl: './chart-bike.component.html',
    styleUrls: ['./../chart.component.scss'],
    providers: [ChartService, AppCommon]
})

export class ChartBikeComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    @Input() typeTimeView: string;

    // max rent bike chart config
    maxChart: any = {
        view: [1500, 500],
        results: [],
        legend: false,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showYAxisLabel: true,
        showXAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#155799']
        }
    }

    minChart: any = {
        view: [1500, 500],
        results: [],
        legend: false,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showYAxisLabel: true,
        showXAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#155799']
        }
    }

    constructor(
        public chartService: ChartService,
        public appCommon: AppCommon
    ) { }

    ngOnChanges(changes: SimpleChanges): void {
        this.getData();
    }

    getData(): void {
        let params = {
            projectId: this.projectId,
            typeTimeView: this.typeTimeView,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };

        this.chartService.GetJsonData("BikeChartData", params).then((res) => {
            this.maxChart.results = res.chart_data_best_20;
            this.minChart.results = res.chart_data_worst_20;
        });
    }
}