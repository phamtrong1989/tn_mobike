import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { HttpLoaderFactory } from 'src/app/app.module';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { SharedModule } from '../../shared/shared.module';

import { ChartComponent } from './chart.component';
import { ChartGrowthComponent } from './chart-growth/chart-growth.component';
import { ChartCustomerComponent } from './chart-customer/chart-customer.component';
import { ChartFinanceComponent } from './chart-finance/chart-finance.component';
import { ChartTransactionComponent } from './chart-transaction/chart-transaction.component';
import { ChartBikeComponent } from './chart-bike/chart-bike.component';
import { ChartStationComponent } from './chart-station/chart-station.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

export const routes = [
  { path: '', component: ChartComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    PerfectScrollbarModule,
    SharedModule,
    PipesModule,
    AgmCoreModule,
    NgMultiSelectDropDownModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    })
  ],
  declarations: [
    ChartComponent,
    ChartCustomerComponent,
    ChartFinanceComponent,
    ChartTransactionComponent,
    ChartGrowthComponent,
    ChartBikeComponent,
    ChartStationComponent
  ],
  entryComponents: [
  ]
})

export class ChartModule { }
