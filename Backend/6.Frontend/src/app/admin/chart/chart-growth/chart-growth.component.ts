import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { ChartService } from '../chart.services';

@Component({
    selector: 'app-chart-growth',
    templateUrl: './chart-growth.component.html',
    styleUrls: ['./../chart.component.scss'],
    providers: [ChartService, AppCommon]
})

export class ChartGrowthComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    @Input() typeTimeView: string;

    // options config chart
    view: any[] = [1500, 600];
    legend: boolean = true;
    legendTitle: string = "Đề mục";
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'Thời gian';
    yAxisLabel: string = 'Số lượng';
    timeline: boolean = false;

    colorScheme = {
        //domain: ['#155799', '#ef32d9', '#cb2d3e', '#ffd200', '#159957', '#95726c']
        domain: ['#155799', '#cb2d3e', '#159957']
    };
    results: any;

    constructor(
        public chartService: ChartService,
        public appCommon: AppCommon
    ) { }

    ngOnChanges() {
        this.getData();
    }

    getData(): void {
        var params = {
            projectId: this.projectId,
            typeTimeView: this.typeTimeView,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };

        this.chartService.GetJsonData("GrowthChartData", params).then((res) => {
            this.results = res;
        });
    }

    onSelect(data: any): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data: any): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data: any): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }
}