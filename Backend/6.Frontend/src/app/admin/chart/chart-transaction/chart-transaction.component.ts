import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { ChartService } from '../chart.services';

@Component({
    selector: 'app-chart-transaction',
    templateUrl: './chart-transaction.component.html',
    styleUrls: ['./../chart.component.scss'],
    providers: [ChartService, AppCommon]
})

export class ChartTransactionComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    @Input() typeTimeView: string;

    commonInfo: any = {
        new_customer_quantity: 0,
        new_customer_quantity_avg: 0,
        recharge_sum: 0,
        recharge_avg: 0,
        recharge_transaction_quantity: 0,
        recharge_transaction_quantity_avg: 0,
        rent_sum: 0,
        rent_sum_avg: 0,
        debt_sum: 0,
        transaction_quantity: 0,
        transaction_quantity_avg: 0,
        transaction_time_sum: 0,
        transaction_time_sum_avg: 0,
        transaction_km_sum: 0,
        transaction_km_sum_avg: 0,
        transaction_debt_quantity: 0,
        transaction_recall_quantity: 0
    };

    // total chart config
    totalChartConfig: any = {
        view: [1500, 500],
        results: [],
        showXAxis: true,
        showYAxis: true,
        gradient: false,
        showLegend: false,
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        showDataLabel: true,
        colorScheme: {
            domain: ['#155799']
        }
    };

    // avg chart config
    avgChartConfig: any = {
        view: [1500, 500],
        results: [],
        showXAxis: true,
        showYAxis: true,
        gradient: false,
        showLegend: false,
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        showDataLabel: true,
        colorScheme: {
            domain: ['#155799']
        }
    };

    // type trend chart config
    typeTrendChartConfig: any = {
        results: [],
        view: [1500, 500],
        legend: true,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0']
        }
    }

    // hour trend chart config
    hourTrendChartConfig: any = {
        results: [],
        view: [1500, 500],
        legend: true,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0', '#C7B42C']
        }
    }

    // channel trend chart config
    timeTrendChartConfig: any = {
        results: [],
        view: [1500, 500],
        legend: true,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0', '#C7B42C']
        }
    }

    // channel trend chart config
    distanceTrendChartConfig: any = {
        results: [],
        view: [1500, 500],
        legend: true,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0']
        }
    }

    // age trend chart config
    ageTrendChartConfig: any = {
        results: [],
        view: [1500, 500],
        legend: true,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0']
        }
    }

    // day trend chart config
    dayTrendChartConfig: any = {
        results: [],
        view: [1500, 500],
        showXAxis: true,
        showYAxis: true,
        gradient: true,
        showLegend: true,
        legendTitle: "Đề mục",
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        colorScheme: {
            domain: ['#5AA454', '#ff5722', '#C7B42C', '#AAAAAA']
        }
    }

    constructor(
        public chartService: ChartService,
        public appCommon: AppCommon
    ) { }

    ngOnChanges() {
        this.getData();
        this.totalChartConfig.showDataLabel = (this.typeTimeView != "day");
    }

    getData(): void {
        var params = {
            projectId: this.projectId,
            typeTimeView: this.typeTimeView,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };

        this.chartService.GetJsonData("CommonInfo", params).then((res) => {
            this.commonInfo = res;
        });

        this.chartService.GetJsonData("TransactionChartQuantity", params).then((res) => {
            this.totalChartConfig.results = res.transaction_quantity;
        });

        this.chartService.GetJsonData("TransactionChartQuantityAverage", params).then((res) => {
            this.avgChartConfig.results = res.transaction_quantity_avg;
        });

        this.chartService.GetJsonData("TransactionChartTypeTicketTrend", params).then((res) => {
            this.typeTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("TransactionChartRangeHourTrend", params).then((res) => {
            this.hourTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("TransactionChartTripTimeTrend", params).then((res) => {
            this.timeTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("TransactionChartTripDistanceTrend", params).then((res) => {
            this.distanceTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("TransactionChartRangeAgeTrend", params).then((res) => {
            this.ageTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("TransactionChartDayOfWeekTrend", params).then((res) => {
            this.dayTrendChartConfig.results = res;
        });
    }
}