import { Component, Injectable, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { ChartService } from './chart.services';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  providers: [ChartService, AppCommon]
})

export class ChartComponent implements OnInit {
  readonly _SYSTEM_START_DATE = new Date(2021, 10, 1);

  ctrlProjectId = new FormControl();
  fromDate: Date;
  toDate: Date;

  categoryData: any;

  typeTimeView: string = "day";
  minFromDate: Date = new Date(2021, 10, 1);
  maxFromDate: Date = new Date();
  minToDate: Date = new Date(2021, 10, 1);
  maxToDate: Date = new Date();
  boxInfo: any = {};

  constructor(
    public appCommon: AppCommon,
    public appStorage: AppStorage,
    public chartService: ChartService,
  ) {
    this.categoryData = appStorage.getCategoryData();

    if (this.appStorage.getProjectId() == null || this.appStorage.getProjectId() == undefined) {
      this.appStorage.setProjectId(appStorage.getCategoryData().projects[0].id);
    }

    this.ctrlProjectId.setValue(this.appStorage.getProjectId());
    this.ctrlProjectId.valueChanges.subscribe(res => {
      this.appStorage.setProjectId(res);
    });

    // set default date
    var today = new Date();
    var date = today.getDate(), month = today.getMonth(), year = today.getFullYear();

    if (month == 0) {
      year--;
      month = 11;
    } else {
      month--;
    }

    this.fromDate = new Date(year, month, date);
    this.toDate = new Date();
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.chartService.GetJsonData("GetInfoBoxData", {}).then((res) => {
      this.boxInfo = res;
    });
  }

  typeTimeViewChange(event) {
    this.minFromDate = new Date(2021, 10, 1);
    this.maxFromDate = new Date();
    this.minToDate = new Date(2021, 10, 1);
    this.maxToDate = new Date();
    //
    var today = new Date();
    var date = today.getDate(), month = today.getMonth(), year = today.getFullYear();

    switch (event.value) {
      case "day": {
        if (month == 0) {
          year--;
          month = 11;
        } else {
          month--;
        }

        this.fromDate = new Date(year, month, date);
        this.toDate = new Date();
      }
        break;
      case "month": {
        year = year - 1;
        this.fromDate = new Date(year, month, date);
        if (this.fromDate < this._SYSTEM_START_DATE) {
          this.fromDate = this._SYSTEM_START_DATE;
        }
        this.toDate = new Date();
      }
        break;
      default: {
        this.fromDate = new Date(today.setDate(today.getDate() - 7));
        this.toDate = new Date();
      }
    }
  }

  getMonday() {
    var today = new Date();
    var first = today.getDate() - today.getDay() + 1;
    return new Date(today.setDate(first));
  }

  getFirstDayOfMonth() {
    return new Date(new Date().getFullYear(), new Date().getMonth(), 1)
  }

  getFirstDayOfQuarter() {
    var currentMonth = new Date().getMonth();
    var currentQuarter = Math.ceil(currentMonth / 3);
    var firstMonthOfQuarter = currentQuarter * 3 - 3;
    return new Date(new Date().getFullYear(), firstMonthOfQuarter, 1)
  }

  getFirstDayOfYear() {
    return new Date(new Date().getFullYear(), 0, 1)
  }
}