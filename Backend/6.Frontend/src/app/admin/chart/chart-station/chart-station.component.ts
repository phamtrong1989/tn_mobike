import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { ChartService } from '../chart.services';

@Component({
    selector: 'app-chart-station',
    templateUrl: './chart-station.component.html',
    styleUrls: ['./../chart.component.scss'],
    providers: [ChartService, AppCommon]
})

export class ChartStationComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    @Input() typeTimeView: string;

    typeView: string = "bikeOut";

    // ng multi select config
    ngSelect: any = {
        data: [],
        selectedItems: [],
        settings: {
            singleSelection: false,
            idField: 'id',
            textField: 'text',
            enableCheckAll: false,
            selectAllText: 'Chọn tất cả',
            unSelectAllText: 'Bỏ chọn tất cả',
            allowSearchFilter: true,
            searchPlaceholderText: 'Tìm kiếm',
            clearSearchFilter: true,
            maxHeight: 200,
            noDataAvailablePlaceholderText: 'Không có dữ liệu',
            noFilteredDataAvailablePlaceholderText: 'Không tìm thấy dữ liệu phù hợp',
            showSelectedItemsAtTop: true
        }
    }

    // chart config
    view: any[] = [1500, 550];
    legend: boolean = false;
    legendTitle: string = "Đề mục";
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = '';
    yAxisLabel: string = '';
    timeline: boolean = false;

    colorScheme = {
        domain: ['#155799', '#ef32d9', '#cb2d3e', '#ffd200', '#159957', '#95726c']
    };
    results: any;

    constructor(
        public chartService: ChartService,
        public appCommon: AppCommon
    ) {
        this.getStations();
        this.results = [];
    }

    ngOnChanges() {
        this.getStations();
        this._resetData();
    }

    getStations() {
        let params = {
            projectId: this.projectId
        };

        this.chartService.getStations(params).then((res) => {
            this.ngSelect.data = res;
        });
    }

    onNgSelect(item: any) {
        let params = {
            projectId: this.projectId,
            typeTimeView: this.typeTimeView,
            typeView: this.typeView,
            stationId: item.id,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };
        //
        this.chartService.GetJsonData("StationChartData", params).then((res) => {
            let temp = this.results;
            this.results = [];
            this.results = [...temp, res];
        });
    }

    onNgDeSelect(item: any) {
        this.results = this.results.filter(function (obj) {
            return obj.name != item.text;
        });
    }

    onNgSelectAll(items: any) {
        console.log(items);
    }

    typeViewChange(event) {
        this._resetData();
    }

    _resetData() {
        this.results = [];
        this.ngSelect.selectedItems = [];
    }
}