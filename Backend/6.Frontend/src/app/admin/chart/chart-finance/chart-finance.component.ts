import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { ChartService } from '../chart.services';

@Component({
    selector: 'app-chart-finance',
    templateUrl: './chart-finance.component.html',
    styleUrls: ['./../chart.component.scss'],
    providers: [ChartService, AppCommon]
})

export class ChartFinanceComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    @Input() typeTimeView: string;

    commonInfo: any = {
        new_customer_quantity: 0,
        new_customer_quantity_avg: 0,
        recharge_sum: 0,
        recharge_avg: 0,
        recharge_transaction_quantity: 0,
        recharge_transaction_quantity_avg: 0,
        rent_sum: 0,
        rent_sum_avg: 0,
        debt_sum: 0,
        transaction_quantity: 0,
        transaction_quantity_avg: 0,
        transaction_time_sum: 0,
        transaction_time_sum_avg: 0,
        transaction_km_sum: 0,
        transaction_km_sum_avg: 0,
        transaction_debt_quantity: 0,
        transaction_recall_quantity: 0
    };

    // total chart config
    totalChartConfig: any = {
        view: [1500, 500],
        results: [],
        showXAxis: true,
        showYAxis: true,
        gradient: false,
        showLegend: false,
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        showDataLabel: true,
        colorScheme: {
            domain: ['#155799']
        },
        myYAxisTickFormatting(val) {
            if (val >= 1000000000) {
                return val / 1000000000 + ' B';
            } else if (val >= 1000000) {
                return val / 1000000 + ' M';
            } else if (val >= 1000) {
                return val / 1000 + ' K';
            } else {
                return val;
            }
        }
    };

    // avg chart config
    avgChartConfig: any = {
        view: [1500, 500],
        results: [],
        showXAxis: true,
        showYAxis: true,
        gradient: false,
        showLegend: false,
        showYAxisLabel: true,
        showXAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        showDataLabel: true,
        colorScheme: {
            domain: ['#155799']
        },
        myYAxisTickFormatting(val) {
            if (val >= 1000000000) {
                return val / 1000000000 + ' B';
            } else if (val >= 1000000) {
                return val / 1000000 + ' M';
            } else if (val >= 1000) {
                return val / 1000 + ' K';
            } else {
                return val;
            }
        }
    };

    // age trend chart config
    ageTrendChartConfig: any = {
        results: [],
        view: [1500, 400],
        legend: true,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showYAxisLabel: true,
        showXAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0']
        },
        myYAxisTickFormatting(val) {
            if (val >= 1000000000) {
                return val / 1000000000 + ' B';
            } else if (val >= 1000000) {
                return val / 1000000 + ' M';
            } else if (val >= 1000) {
                return val / 1000 + ' K';
            } else {
                return val;
            }
        }
    }

    // hour trend chart config
    hourTrendChartConfig: any = {
        results: [],
        view: [1500, 400],
        legend: true,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showYAxisLabel: true,
        showXAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0', '#C7B42C']
        },
        myYAxisTickFormatting(val) {
            if (val >= 1000000000) {
                return val / 1000000000 + ' B';
            } else if (val >= 1000000) {
                return val / 1000000 + ' M';
            } else if (val >= 1000) {
                return val / 1000 + ' K';
            } else {
                return val;
            }
        }
    }

    // channel trend chart config
    chanelTrendChartConfig: any = {
        results: [],
        view: [1500, 400],
        legend: true,
        legendTitle: "Đề mục",
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showYAxisLabel: true,
        showXAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0']
        },
        myYAxisTickFormatting(val) {
            if (val >= 1000000000) {
                return val / 1000000000 + ' B';
            } else if (val >= 1000000) {
                return val / 1000000 + ' M';
            } else if (val >= 1000) {
                return val / 1000 + ' K';
            } else {
                return val;
            }
        }
    }

    // level trend chart config
    levelTrendChartConfig: any = {
        results: [],
        view: [1500, 400],
        legend: true,
        legendTitle: "Đề mục",
        showDataLabel: true,
        showLabels: true,
        animations: true,
        xAxis: true,
        yAxis: true,
        showYAxisLabel: true,
        showXAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        timeline: true,
        colorScheme: {
            domain: ['#795548', '#009688', '#ff5722', '#03a9f4', '#ffc107', '#3f51b5', '#9c27b0']
        },
        myYAxisTickFormatting(val) {
            if (val >= 1000000000) {
                return val / 1000000000 + ' B';
            } else if (val >= 1000000) {
                return val / 1000000 + ' M';
            } else if (val >= 1000) {
                return val / 1000 + ' K';
            } else {
                return val;
            }
        }
    }

    // day trend chart config
    dayTrendChartConfig: any = {
        results: [],
        view: [1500, 500],
        showXAxis: true,
        showYAxis: true,
        gradient: true,
        showLegend: true,
        legendTitle: "Đề mục",
        showXAxisLabel: true,
        showYAxisLabel: true,
        xAxisLabel: '',
        yAxisLabel: '',
        colorScheme: {
            domain: ['#5AA454', '#C7B42C', '#AAAAAA']
        }
    }

    constructor(
        public chartService: ChartService,
        public appCommon: AppCommon
    ) { }

    ngOnChanges() {
        this.getData();
        this.totalChartConfig.showDataLabel = (this.typeTimeView != "day");
    }

    getData(): void {
        var params = {
            projectId: this.projectId,
            typeTimeView: this.typeTimeView,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };

        this.chartService.GetJsonData("CommonInfo", params).then((res) => {
            this.commonInfo = res;
        });
        //
        this.chartService.GetJsonData("FinanceChartRechargeSum", params).then((res) => {
            this.totalChartConfig.results = res.recharge_sum;
        });

        this.chartService.GetJsonData("FinanceChartRechargeSumAverage", params).then((res) => {
            this.avgChartConfig.results = res.recharge_sum_avg;
        });

        this.chartService.GetJsonData("FinanceChartRangeAgeTrend", params).then((res) => {
            this.ageTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("FinanceChartRangeHourTrend", params).then((res) => {
            this.hourTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("FinanceChartRechargeChanelTrend", params).then((res) => {
            this.chanelTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("FinanceChartRechargeLevelTrend", params).then((res) => {
            this.levelTrendChartConfig.results = res;
        });

        this.chartService.GetJsonData("FinanceChartDayOfWeekTrend", params).then((res) => {
            this.dayTrendChartConfig.results = res;
        });
    }
}