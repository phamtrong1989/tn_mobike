export class News {
  id: number;
  campaignId: number;
  author: string;
  description: string;
  content: string;
  publicDate: Date;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  type: number;
  accountId: number;
  name: string;
  languageId: number;
  projectId: number;
}

export class NewsCommand {
  id: number;
  campaignId: number;
  author: string;
  description: string;
  content: string;
  publicDate: Date;
  type: number;
  name: string;
  languageId: number;
  projectId: number;

  deserialize(input: News): NewsCommand {
    if (input == null) {
      input = new News();
    }
    this.id = input.id;
    this.campaignId = input.campaignId;
    this.author = input.author;
    this.description = input.description;
    this.content = input.content;
    this.publicDate = input.publicDate;
    this.type = input.type;
    this.name = input.name;
    this.languageId = input.languageId;
    this.projectId = input.projectId;

    return this;
  }
}