import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { News, NewsCommand } from './newss.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'

@Injectable()
export class NewssService {
    public url = "/api/work/Newss";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<News[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: NewsCommand) {
        return this.http.post<ApiResponseData<News>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: NewsCommand) {
        return this.http.put<ApiResponseData<News>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    getById(id: number) {
        return this.http.get<ApiResponseData<News>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    delete(id: number) {
        return this.http.delete<ApiResponseData<News>>(this.url + "/delete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}