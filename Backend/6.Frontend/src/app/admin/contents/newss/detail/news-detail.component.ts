import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { News, NewsCommand } from '../newss.model';
import { NewssService } from '../newss.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-news-detail',
    templateUrl: './news-detail.component.html',
    styleUrls: ['../../content-pages/detail/content-styles.css'],
    providers: [
        NewssService,
        AppCommon,
        { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})

export class NewsDetailComponent implements OnInit {
    public content;
    public formCalendars: FormGroup;
    public categoryData: any;
    baseTranslate: any;
    isProcess = { saveProcess: false, deleteProcess: false, initData: false };

    constructor(
        public dialogRef: MatDialogRef<NewsDetailComponent>,
        @Inject(MAT_DIALOG_DATA)
        public data: NewsCommand,
        public fb: FormBuilder,

        public dialog: MatDialog,
        public snackBar: MatSnackBar,
        public appStorage: AppStorage,
        public contentPagesService: NewssService,
        public appCommon: AppCommon,
        private translate: TranslateService
    ) {
        this.categoryData = this.appStorage.getCategoryData();
        this.initBaseTranslate();
    }

    ngOnInit() {
        if (this.data.id > 0) {
            this.getData();
        }
    }

    initBaseTranslate() {
        this.translate.get(['base'])
            .subscribe(translations => {
                this.baseTranslate = translations['base'];
            });
    }

    getData() {
        this.isProcess.initData = true;
        this.contentPagesService.getById(this.data.id).then(rs => {
            if (rs.status == 1 && rs.data) {
                this.content = rs.data.content;
                this.isProcess.initData = false;
            }
            else {
                this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
                this.isProcess.initData = false;
            }
        });
    }

    close(input: any = null): void {
        if (input != null) {
            this.dialogRef.close(input);
        }
        else {
            this.dialogRef.close();
        }
    }
}