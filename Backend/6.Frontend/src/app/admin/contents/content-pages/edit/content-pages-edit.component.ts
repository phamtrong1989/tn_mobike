import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContentPage, ContentPageCommand } from '../content-pages.model';
import { ContentPagesService } from '../content-pages.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import Editor from '@app-libs/ckeditor5';

@Component({
  selector: 'app-content-pages-edit',
  templateUrl: './content-pages-edit.component.html',
  providers: [
    ContentPagesService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ContentPagesEditComponent implements OnInit {
  public model = {
    editorData: ''
  };
  public Editor = Editor;
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<ContentPagesEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ContentPageCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public contentPagesService: ContentPagesService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      name: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(99999)])],
      type: [null, Validators.compose([Validators.required])],
      languageId: [null, Validators.compose([Validators.required])],
      content: ['']
    });
  }

  ngOnInit() {
    if (this.data.id > 0) {
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.contentPagesService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new ContentPageCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.model.editorData = rs.data.content;
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.form.get('content').setValue(this.model.editorData);
      if (this.data.id > 0) {
        this.contentPagesService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          } else if (res.status == 2) {
            this.snackBar.open("Loại nội dung này đã tồn tại 1 bản ghi khác. Vui lòng sửa lại nội dung đã tồn tại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.contentPagesService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new ContentPageCommand().deserialize(res.data);
            this.form.setValue(this.data);
            this.close(res);
          } else if (res.status == 2) {
            this.snackBar.open("Loại nội dung này đã tồn tại 1 bản ghi khác. Vui lòng sửa lại nội dung đã tồn tại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.contentPagesService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
