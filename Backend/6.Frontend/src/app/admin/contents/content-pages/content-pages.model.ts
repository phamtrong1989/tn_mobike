export class ContentPage {

  id: number;
  name: string;
  content: string;
  type: number;
  languageId: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
}

export class ContentPageCommand {

  id: number;
  name: string;
  type: number;
  languageId: number;
  content: string;

  deserialize(input: ContentPage): ContentPageCommand {
    if (input == null) {
      input = new ContentPage();
    }
    this.id = input.id;
    this.name = input.name;
    this.type = input.type;
    this.languageId = input.languageId;
    this.content = input.content;

    return this;
  }
}