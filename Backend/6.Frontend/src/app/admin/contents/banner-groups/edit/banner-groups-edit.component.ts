import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BannerGroup, BannerGroupCommand } from '../banner-groups.model';
import { BannerGroupsService } from '../banner-groups.service'
import { AppCommon } from '../../../../app.common';
import { AppStorage } from '../../../../app.storage';
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-banner-groups-edit',
  templateUrl: './banner-groups-edit.component.html',
  providers: [
    BannerGroupsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class BannerGroupsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<BannerGroupsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: BannerGroupCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public bannerGroupsService: BannerGroupsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(99999)])],
      type: [null, Validators.compose([Validators.required])],
      status: [null, Validators.compose([Validators.required])],
      description: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(99999)])],
      createdDate: [null, Validators.compose([Validators.required])],
      createdUser: [null, Validators.compose([Validators.required])],
      updatedDate: [null, Validators.compose([Validators.required])],
      updatedUser: [null, Validators.compose([Validators.required])],
      languageId: [null, Validators.compose([Validators.required])],
      language: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(99999)])],

    });
  }

  ngOnInit() {

    if (this.data.id > 0) {
      this.form.setValue(this.data);
      this.getData();
    }

  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.bannerGroupsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new BannerGroupCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }
  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
