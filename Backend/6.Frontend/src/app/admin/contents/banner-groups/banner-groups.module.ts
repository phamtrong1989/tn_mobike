import { BannersComponent } from './banners/banners.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { BannerGroupsComponent } from './banner-groups.component';
import { BannerGroupsEditComponent } from './edit/banner-groups-edit.component';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { BannersEditComponent } from './banners/edit/banners-edit.component';

export const routes = [
  { path: '', component: BannerGroupsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxMaterialTimepickerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      }
    })
  ],
  declarations: [
    BannerGroupsComponent,
    BannerGroupsEditComponent,
    BannersComponent,
    BannersEditComponent
  ],
  entryComponents: [
    BannerGroupsEditComponent,
    BannersComponent,
    BannersEditComponent
  ]
})

export class BannerGroupsModule { }
