import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppCommon } from '../../../../app.common';
import { AppStorage } from '../../../../app.storage';
import { AppSettings } from '../../../../app.settings';
import { Settings } from '../../../../app.settings.model';
import { BannerGroup } from '../banner-groups.model';
import { Banner, BannerCommand } from './banners.model';
import { BannerGroupsService } from '../banner-groups.service';
import { BannersEditComponent } from './edit/banners-edit.component';

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  providers: [BannerGroupsService, AppCommon]
})

export class BannersComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: Banner[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["name", "image", "data", "status", "rangeTime", "createdDate", "updatedDate", "action"];
  public settings: Settings;
  public categoryData: any;

  constructor(
    public dialogRef: MatDialogRef<BannersEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public bannerGroup: BannerGroup,
    public appSettings: AppSettings,
    public bannerGroupsService: BannerGroupsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  ngOnInit() {
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;

    this.params.bannerGroupId = this.bannerGroup.id;
    this.bannerGroupsService.bannerSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: Banner) {
    var bannerData = new BannerCommand();

    if (data == null) {
      bannerData.id = 0;
      bannerData.bannerGroupId = this.bannerGroup.id;
    }
    else {
      bannerData = new BannerCommand().deserialize(data);
    }

    this.dialog.open(BannersEditComponent, {
      data: bannerData,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}