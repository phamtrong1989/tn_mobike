import { BannerGroup } from './../banner-groups.model';

export class Banner {
  id: number;
  bannerGroupId: number;
  name: string;
  image: string;
  data: string;
  type: number;
  status: number;
  startTime: Date;
  endTime: Date;
  description: string;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  languageId: number;
  language: string;

  bannerGroup: BannerGroup;
}

export class BannerCommand {

  id: number;
  bannerGroupId: number;
  name: string;
  image: string;
  data: string;
  type: number;
  status: number;
  startTime: Date;
  endTime: Date;
  description: string;
  languageId: number;

  deserialize(input: Banner): BannerCommand {
    if (input == null) {
      input = new Banner();
    }
    this.id = input.id;
    this.bannerGroupId = input.bannerGroupId;
    this.name = input.name;
    this.image = input.image;
    this.data = input.data;
    this.type = input.type;
    this.status = input.status;
    this.startTime = input.startTime;
    this.endTime = input.endTime;
    this.description = input.description;
    this.languageId = input.languageId;

    return this;
  }
}