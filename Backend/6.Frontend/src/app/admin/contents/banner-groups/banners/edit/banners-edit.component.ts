import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppStorage } from '../../../../../app.storage';
import { AppCommon } from '../../../../../app.common';
import { BannerCommand } from '../banners.model';
import { BannerGroupsService } from '../../banner-groups.service';
import { BannerGroup } from '../../banner-groups.model';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-banners-edit',
  templateUrl: './banners-edit.component.html',
  providers: [
    BannerGroupsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class BannersEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  bannerGroups: Iterable<BannerGroup>;
  uploadFileOut: FileDataDTO[];
  currentFiles: FileDataDTO[];

  constructor(
    public dialogRef: MatDialogRef<BannersEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: BannerCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public bannerGroupsService: BannerGroupsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      bannerGroupId: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(99999)])],
      image: [null],
      data: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(99999)])],
      type: [null, Validators.compose([Validators.required])],
      status: [null, Validators.compose([Validators.required])],
      startTime: [null, Validators.compose([Validators.required])],
      endTime: [null, Validators.compose([Validators.required])],
      description: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(99999)])],
      languageId: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.getBannerGroups();

    if (this.data.id > 0) {
      this.getData();
    } else {
      this.form.get('bannerGroupId').setValue(this.data.bannerGroupId);
    }
  }

  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.bannerGroupsService.bannerGetById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new BannerCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        if (this.data.image != null) {
          let fileAdded: FileDataDTO = {
            path: rs.data.image,
            fileName: rs.data.image.substring(rs.data.image.lastIndexOf('/') + 1),
            createdDate: new Date(),
            createdUser: new Date(),
            fileSize: 0
          };
          this.uploadFileOut = [fileAdded]
          this.currentFiles = [fileAdded];
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  getBannerGroups() {
    this.bannerGroupsService.getBannerGroups().then(rs => {
      this.bannerGroups = rs;
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.form.get('image').setValue(this.uploadFileOut[0].path);
      if (this.data.id > 0) {
        this.bannerGroupsService.bannerEdit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.bannerGroupsService.bannerCreate(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new BannerCommand().deserialize(res.data);
            this.form.setValue(this.data);
            this.close(res);
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.bannerGroupsService.bannerDelete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
