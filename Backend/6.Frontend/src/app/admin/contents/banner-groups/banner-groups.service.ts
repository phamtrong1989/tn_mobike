import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { BannerGroup } from './banner-groups.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common';
import { Banner, BannerCommand } from './banners/banners.model';

@Injectable()
export class BannerGroupsService {
    public url = "/api/work/BannerGroups";
    _injector: Injector;
    public clientSettings: ClientSettings;

    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    // BANNER GROUP
    searchPaged(params: any) {
        return this.http.get<ApiResponseData<BannerGroup[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<BannerGroup>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getBannerGroups() {
        return this.http.get<Iterable<BannerGroup>>(this.url + "/getBannerGroups").pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    //

    // BANNER
    bannerSearchPaged(params: any) {
        return this.http.get<ApiResponseData<Banner[]>>(this.url + "/bannerSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bannerCreate(user: BannerCommand) {
        return this.http.post<ApiResponseData<Banner>>(this.url + "/bannerCreate", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bannerEdit(user: BannerCommand) {
        return this.http.put<ApiResponseData<Banner>>(this.url + "/bannerEdit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();

    }

    bannerGetById(id: number) {
        return this.http.get<ApiResponseData<Banner>>(this.url + "/bannerGetById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bannerDelete(id: number) {
        return this.http.delete<ApiResponseData<Banner>>(this.url + "/bannerDelete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    uploadPath() {
        return this.url + "/Upload";
    }

    apiUrl() {
        return this.clientSettings.serverAPI;
    }

    //
    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
} 