export class BannerGroup {
  id: number;
  name: string;
  type: number;
  status: number;
  description: string;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  languageId: number;
  language: string;
}

export class BannerGroupCommand {

  id: number;
  name: string;
  type: number;
  status: number;
  description: string;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  languageId: number;
  language: string;

  deserialize(input: BannerGroup): BannerGroupCommand {
    if (input == null) {
      input = new BannerGroup();
    }
    this.id = input.id;
    this.name = input.name;
    this.type = input.type;
    this.status = input.status;
    this.description = input.description;
    this.createdDate = input.createdDate;
    this.createdUser = input.createdUser;
    this.updatedDate = input.updatedDate;
    this.updatedUser = input.updatedUser;
    this.languageId = input.languageId;
    this.language = input.language;

    return this;
  }
}