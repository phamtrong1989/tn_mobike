import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { CampaignsService } from '../../campaigns.service';
import { VoucherCodeCommand } from '../../campaigns.model';
import { PullData } from 'src/app/app.settings.model';
import { CustomerGroup } from 'src/app/admin/category/projects/projects.model';
import { CustomValidator } from 'src/app/theme/utils/app-validators';
import { AppCommon } from 'src/app/app.common';

@Component({
  selector: 'app-voucher-codes-edit',
  templateUrl: './voucher-codes-edit.component.html',
  providers: [
    CampaignsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class VoucherCodesEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: PullData;
  customerGroups: CustomerGroup[];
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<VoucherCodesEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: VoucherCodeCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public campaignsService: CampaignsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.customerGroups = this.categoryData.customerGroups.filter(x => x.projectId == this.data.projectId);
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      projectId: [0],
      customerGroupId: [null, Validators.compose([Validators.required])],
      provisoType: [null, Validators.compose([Validators.required])],
      campaignId: [0],
      code: ['0000000000', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(20)])],
      type: [1],
      limit: [1, Validators.compose([Validators.required, Validators.min(1), Validators.max(10000)])],
      point: [1000, Validators.compose([Validators.required, Validators.min(1000), Validators.max(10000000)])],
      stacked: [true],
      curentLimit: [0],
      public: [false]
    });
  }

  initEvent() {
    this.form.get('stacked').valueChanges.subscribe(rs => {
      if (this.form.get('stacked').value) {
        this.form.get('code').setValue(this.getRandomString(16));
      }
      else {
        this.form.get('code').setValue("0000000000")
      }
    });

    this.form.get('type').valueChanges.subscribe(rs => {
      if (this.form.get('type').value == 1) {
        this.form.get('point').setValue(0)
      }
      else {
        this.form.get('code').setValue(1000)
      }
    });
  }

  ngOnInit() {

    if (this.data.id > 0) {
      this.form.get('code').disable();
      this.form.get('type').disable();
      this.form.get('stacked').disable();
      this.form.setValue(this.data);
      this.getData();
    }
    else {
      this.form.get('projectId').setValue(this.data.projectId);
      this.form.get('campaignId').setValue(this.data.campaignId);
      this.form.get('code').setValue(this.getRandomString(10));
      this.initEvent();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;

    this.campaignsService.voucherCodeGetById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new VoucherCodeCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
        this.initEvent();
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  getRandomString(length: number) {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = '';
    for (var i = 0; i < length; i++) {
      result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result.toUpperCase();
  }

  save() {
    this.isProcess.saveProcess = true;

    if (this.form.valid) {

      if (this.data.id > 0) {
        this.campaignsService.voucherCodeEdit (this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.campaignsService.voucherCodeCreate(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.close(1);
          }
          else if (res.status == 2) {
            this.snackBar.open("Mã voucher đã tồn tại vui lòng chọn mã khác", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.campaignsService.voucherCodeDelete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 2) {
            this.snackBar.open("Không thể xóa, chiến dịch này đã phát sinh giao dịch rồi", this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
