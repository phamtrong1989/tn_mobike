import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Campaign, CampaignCommand } from '../campaigns.model';
import { CampaignsService } from '../campaigns.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { CampaignsLogComponent } from '../log/campaigns-log.component';
import { VoucherCodesComponent } from '../voucher-codes/voucher-codes.component';
import { DiscountCodesComponent } from '../discount-codes/discount-codes.component';

@Component({
  selector: 'app-campaigns-edit',
  templateUrl: './campaigns-edit.component.html',
  providers: [
    CampaignsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class CampaignsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<CampaignsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: CampaignCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public campaignsService: CampaignsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      projectId: [0, Validators.compose([Validators.required])],
      investorId: [0],
      name: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(200)])],
      status: [1],
      description: [null, Validators.compose([Validators.maxLength(4000)])],
      type: [0],
      startDate: [new Date()],
      endDate: [new Date()]
    });
  }

  ngOnInit() {
    if (this.data.id > 0) {
      this.form.get('type').disable();
      this.form.get('projectId').disable();
      this.form.get('startDate').disable();
      this.form.get('endDate').disable();
      this.form.setValue(this.data);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  matTabGroupChange(e: any, list: VoucherCodesComponent, deposit: any, log:CampaignsLogComponent, discount: DiscountCodesComponent) {

    if(e.index == 1)
    {
      if(this.data.type == 0)
      {
        list.firsLoad();
      }
      else if(this.data.type == 1)   
      {
        deposit.firsLoad();
      }
      else
      {
        discount.firsLoad();
      }
    }
    else if(e.index == 2)
    {
      log.firsLoad();
    }
  }

  getData() {
    this.isProcess.initData = true;
    this.campaignsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new CampaignCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      // if (this.form.get("startDate").value > this.form.get("endDate").value) {
      //   this.snackBar.open("Thời gian hiệu lực không thể lớn hơn thời gian hết hiệu lực", this.baseTranslate["msg-warning-type"], { duration: 5000 });
      //   this.isProcess.saveProcess = false;
      //   return;
      // }

      if (this.data.id > 0) {
        this.campaignsService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else if (res.status == 2) {
            this.snackBar.open("Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.campaignsService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new CampaignCommand().deserialize(res.data);
            this.form.setValue(this.data);
            this.form.get('type').disable();
            this.form.get('projectId').disable();
            this.form.get('startDate').disable();
            this.form.get('endDate').disable();
          }
          else if (res.status == 2) {
            this.snackBar.open("Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.campaignsService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 2) {
            this.snackBar.open("Không thể xóa, chiến dịch này đã phát sinh giao dịch rồi", this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
