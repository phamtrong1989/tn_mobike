import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { CampaignsComponent } from './campaigns.component';
import { CampaignsEditComponent } from './edit/campaigns-edit.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { VoucherCodesComponent } from './voucher-codes/voucher-codes.component';
import { VoucherCodesEditComponent } from './voucher-codes/edit/voucher-codes-edit.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { CampaignsLogComponent } from './log/campaigns-log.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DepositEventsComponent } from './deposit-events/deposit-events.component';
import { DepositEventsEditComponent } from './deposit-events/edit/deposit-events-edit.component';
import { DiscountCodesEditComponent } from './discount-codes/edit/discount-codes-edit.component';
import { DiscountCodesComponent } from './discount-codes/discount-codes.component';

export const routes = [
  { path: '', component: CampaignsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxCurrencyModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
	  TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }) 
  ],
  declarations: [
    CampaignsComponent,
    CampaignsEditComponent,
    VoucherCodesComponent,
    VoucherCodesEditComponent,
    CampaignsLogComponent,
    DepositEventsComponent,
    DepositEventsEditComponent,
    DiscountCodesComponent,
    DiscountCodesEditComponent
  ],
  entryComponents:[
    CampaignsEditComponent,
    VoucherCodesComponent,
    VoucherCodesEditComponent,
    CampaignsLogComponent,
    DepositEventsComponent,
    DepositEventsEditComponent,
    DiscountCodesComponent,
    DiscountCodesEditComponent
  ]
})
export class CampaignsModule { }
