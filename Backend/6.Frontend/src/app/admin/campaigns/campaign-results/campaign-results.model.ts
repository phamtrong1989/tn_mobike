import { CustomerGroup } from "src/app/admin/category/projects/projects.model";
import { Account, WalletTransaction } from "../../customer/accounts/accounts.model";
import { User } from "../../user/users/users.model";
import { DepositEvent, VoucherCode } from '../campaigns.model';

export class CampaignResult {
  projectName: string;
  CustomerGroupName: string;
  code: string;
  point: number;
  percent: number;
  startAmount: number;
  limit: number;

  campaign: Campaign;
  customer: Account;
  walletTransaction: WalletTransaction;
}

export class Campaign {
  id: number;
  projectId: number;
  investorId: number;
  name: string;
  status: number;
  type: number;
  description: string;
  createdDate: Date;
  updatedDate: Date;
  updatedUser: number;
  createdUser: number;
  totalLimit: number;
  totalCurentLimit: number;
  createdUserObject: User;
  updatedUserObject: User;
  startDate: Date;
  endDate: Date;
  // References
  customerGroup: CustomerGroup;
  depositEvents: Iterable<DepositEvent>;
  voucherCodes: Iterable<VoucherCode>;
}

export class CampaignCommand {
  id: number;
  projectId: number;
  investorId: number;
  type: number;
  name: string;
  status: number;
  description: string;
  startDate: Date;
  endDate: Date;

  deserialize(input: Campaign): CampaignCommand {
    if (input == null) {
      input = new Campaign();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.investorId = input.investorId;
    this.name = input.name;
    this.status = input.status;
    this.description = input.description;
    this.type = input.type;
    this.startDate = input.startDate;
    this.endDate = input.endDate;
    return this;
  }
}