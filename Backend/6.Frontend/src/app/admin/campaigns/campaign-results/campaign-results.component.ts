import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';

import { CampaignResultsService } from './campaign-results.service';
import { Campaign, CampaignResult } from './campaign-results.model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-campaign-results',
  templateUrl: './campaign-results.component.html',
  providers: [CampaignResultsService, AppCommon]
})

export class CampaignResultsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: CampaignResult[];
  isProcess = { dialogOpen: false, listDataTable: true, campaignSearching: false };
  params: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "", projectId: "", type: "", customerGroupId: "", campaignId: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "name", "projectId", "type", "code", "customer"];
  campaignKey: FormControl = new FormControl("");
  campaigns: Campaign[];

  public settings: Settings;
  public categoryData: any;

  constructor(
    public appSettings: AppSettings,
    public CampaignResultsService: CampaignResultsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.initSearchCampaign();
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;

    this.params.from = this.appCommon.formatDateTime(this.params.from, 'yyyy-MM-dd');
    this.params.to = this.appCommon.formatDateTime(this.params.to, 'yyyy-MM-dd');
    this.appCommon.changeUrl(location.pathname, this.params);
    this.CampaignResultsService.campaignResultSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  initSearchCampaign() {
    this.campaignKey.valueChanges.subscribe(rs => {
      this.isProcess.campaignSearching = false;
      if (rs != "") {
        this.isProcess.campaignSearching = true;
        this.CampaignResultsService.searchByCampaign({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.params.campaignId = "";
          }
          this.campaigns = rs.data;
          this.isProcess.campaignSearching = false;
        });
      }
    },
      error => {
        this.isProcess.campaignSearching = false;
      });
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}