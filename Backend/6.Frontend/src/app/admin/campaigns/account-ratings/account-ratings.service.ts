import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { AccountRating, AccountRatingCommand } from './account-ratings.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { AccountRatingItem, AccountRatingItemCommand } from './account-rating-items/account-rating-items.model';
import { Account, WalletTransaction, WalletTransactionCommand } from '../../customer/accounts/accounts.model';

@Injectable()
export class AccountRatingsService {
    public url = "/api/work/AccountRatings";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    // Account Rating
    searchPaged(params: any) {
        return this.http.get<ApiResponseData<AccountRating[]>>(this.url + "/searchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: AccountRatingCommand) {
        return this.http.post<ApiResponseData<AccountRating>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: AccountRatingCommand) {
        return this.http.put<ApiResponseData<AccountRating>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    getById(id: number) {
        return this.http.get<ApiResponseData<AccountRating>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    delete(id: number) {
        return this.http.delete<ApiResponseData<AccountRating>>(this.url + "/delete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    // Account Rating Item
    searchPagedRankItem(params: any) {
        return this.http.get<ApiResponseData<AccountRatingItem[]>>(this.url + "/searchPagedRankItem", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    createRankItem(user: AccountRatingItemCommand) {
        return this.http.post<ApiResponseData<AccountRatingItem>>(this.url + "/createRankItem", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    editRankItem(user: AccountRatingItemCommand) {
        return this.http.put<ApiResponseData<AccountRatingItem>>(this.url + "/editRankItem", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    getByIdRankItem(id: number) {
        return this.http.get<ApiResponseData<AccountRatingItem>>(this.url + "/getByIdRankItem/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    deleteRankItem(id: number) {
        return this.http.delete<ApiResponseData<AccountRatingItem>>(this.url + "/deleteRankItem/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByAccount(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchByAccount", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    walletTransactionAdd(user: WalletTransactionCommand) {
        return this.http.post<ApiResponseData<WalletTransaction>>(this.url + "/WalletTransactionAdd", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
} 