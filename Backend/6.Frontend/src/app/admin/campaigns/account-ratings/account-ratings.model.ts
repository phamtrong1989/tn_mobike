import { User } from "../../user/users/users.model";

export class AccountRating {
  id: number;
  code: string;
  name: string;
  note: string;
  reward: string;
  type: number;
  group: number;
  startDate: Date;
  endDate: Date;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  projectId: number;

  createdUserObject: User;
  updatedUserObject: User;
}

export class AccountRatingCommand {
  id: number;
  name: string;
  note: string;
  reward: string;
  type: number;
  group: number;
  startDate: Date;
  endDate: Date;

  deserialize(input: AccountRating): AccountRatingCommand {
    if (input == null) {
      input = new AccountRating();
    }
    this.id = input.id;
    this.name = input.name;
    this.note = input.note;
    this.reward = input.reward;
    this.type = input.type;
    this.group = input.group;
    this.startDate = input.startDate;
    this.endDate = input.endDate;

    return this;
  }
}