import { Account } from 'src/app/admin/customer/accounts/accounts.model';
export class AccountRatingItem {
  id: number;
  code: string;
  projectId: number;
  order: number;
  accountId: number;
  value: number;
  updatedDate: Date;
  status: boolean;

  account: Account;
}

export class AccountRatingItemCommand {
  id: number;
  order: number;
  accountId: number;

  deserialize(input: AccountRatingItem): AccountRatingItemCommand {
    if (input == null) {
      input = new AccountRatingItem();
    }
    this.id = input.id;
    this.order = input.order;
    this.accountId = input.accountId;

    return this;
  }
}

export class AccountRatingItemCreate {
  id: number;
  order: number;
  accountId: number;
  value: number;
  code: string;

  deserialize(input: AccountRatingItem): AccountRatingItemCreate {
    if (input == null) {
      input = new AccountRatingItem();
    }
    this.id = input.id;
    this.order = input.order;
    this.accountId = input.accountId;
    this.value = input.value;
    this.code = input.code;

    return this;
  }
}

export class AccountRatingItemEdit {
  id: number;
  order: number;

  deserialize(input: AccountRatingItem): AccountRatingItemEdit {
    if (input == null) {
      input = new AccountRatingItem();
    }
    this.id = input.id;
    this.order = input.order;

    return this;
  }
}