import { Account } from 'src/app/admin/customer/accounts/accounts.model';
import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AppCommon } from '../../../../../app.common';
import { AppStorage } from '../../../../../app.storage';
import { AccountRatingsService } from '../../account-ratings.service';
import { AccountRatingItemCreate } from '../account-rating-items.model';

@Component({
  selector: 'app-account-rating-items-create',
  templateUrl: './account-rating-items-create.component.html',
  providers: [
    AccountRatingsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class AccountRatingItemsCreateComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accSearching: false  };
  accounts: Account[];
  accKey: FormControl = new FormControl("");

  constructor(
    public dialogRef: MatDialogRef<AccountRatingItemsCreateComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: AccountRatingItemCreate,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountRatingsService: AccountRatingsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      order: [null, Validators.compose([Validators.required])],
      accountId: [0],
      value: [null, Validators.compose([Validators.required])],
      code: [null]
    });
  }

  ngOnInit() {
    this.initSearchUser();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.form.get('code').setValue(this.data.code);
      this.accountRatingsService.createRankItem(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
          this.data = new AccountRatingItemCreate().deserialize(res.data);
          this.form.setValue(this.data);
          this.close(res);
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });

    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }


  initSearchUser() {
    this.accKey.valueChanges.subscribe(rs => {
      this.isProcess.accSearching = false;
      if (rs != "") {
        this.accountRatingsService.searchByAccount({ key: rs, type: 5 }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('accountId').setValue(0)
          }
          this.accounts = rs.data;
          this.isProcess.accSearching = false;
        });
      }
    },
      error => {
        this.isProcess.accSearching = false;
      });
  }

}
