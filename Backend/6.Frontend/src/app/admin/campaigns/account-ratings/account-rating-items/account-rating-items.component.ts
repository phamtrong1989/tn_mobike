import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Inject } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../../app.settings';
import { Settings } from '../../../../app.settings.model';
import { MatPaginator } from '@angular/material/paginator';
import { AccountRatingItemsEditComponent } from './edit/account-rating-items-edit.component';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { AccountRatingsService } from '../account-ratings.service';
import { AccountRatingItemsCreateComponent } from './create/account-rating-items-create.component';
import { AccountRating } from './../account-ratings.model';
import { AccountRatingItem, AccountRatingItemCommand, AccountRatingItemCreate, AccountRatingItemEdit } from './account-rating-items.model';
import { TranslateService } from '@ngx-translate/core';
import { AddPointManualComponent } from './add-point-manual/add-point-manual.component';

@Component({
  selector: 'app-account-rating-items',
  templateUrl: './account-rating-items.component.html',
  providers: [AccountRatingsService, AppCommon]
})

export class AccountRatingItemsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: AccountRatingItem[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["order", "accountId", "value", "updatedDate", "action"];
  public settings: Settings;
  public categoryData: any;
  baseTranslate: any;

  constructor(
    public dialogRef: MatDialogRef<AccountRatingItemsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public accountRating: AccountRating,
    public appSettings: AppSettings,
    public accountRatingsService: AccountRatingsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    private translate: TranslateService
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;

    this.params.code = this.accountRating.code;

    this.accountRatingsService.searchPagedRankItem(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  openCreateDialog() {
    let monday = new Date(this.accountRating.startDate);
    let nextMonday = this.setToNextMonday(monday);

    if (new Date() > nextMonday) {
      let model = new AccountRatingItemCreate();
      model.id = 0;
      model.code = this.accountRating.code;

      this.dialog.open(AccountRatingItemsCreateComponent, {
        data: model,
        width: '500px',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.searchPage();
      });
    } else {
      this.snackBar.open("Thông báo!", "Chỉ có thể cập nhật sau 0h ngày thứ 2 cuả tuần kế tiếp!", { duration: 5000 });
    }
  }

  openEditDialog(data: AccountRatingItem) {
    let monday = new Date(this.accountRating.startDate);
    let nextMonday = this.setToNextMonday(monday);

    if (new Date() > nextMonday) {

      let model = new AccountRatingItemEdit();
      model = new AccountRatingItemEdit().deserialize(data);

      this.dialog.open(AccountRatingItemsEditComponent, {
        data: model,
        width: '500px',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.searchPage();
      });
    } else {
      this.snackBar.open("Thông báo!", "Chỉ có thể cập nhật sau 0h ngày thứ 2 cuả tuần kế tiếp!", { duration: 5000 });
    }
  }

  openAddMoneyDialog(data: AccountRatingItem) {
    let monday = new Date(this.accountRating.startDate);
    let nextMonday = this.setToNextMonday(monday);

    if (new Date() > nextMonday) {
      this.dialog.open(AddPointManualComponent, {
        data: data.account,
        panelClass: 'dialog-500',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.search();
      });
    } else {
      this.snackBar.open("Thông báo!", "Chỉ có thể cập nhật sau 0h ngày thứ 2 cuả tuần kế tiếp!", { duration: 5000 });
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  private setToNextMonday(date: Date) {
    date.setDate(date.getDate() + (((1 + 7 - date.getDay()) % 7) || 7));
    return date;
  }
}