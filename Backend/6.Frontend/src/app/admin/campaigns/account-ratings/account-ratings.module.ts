import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { AccountRatingsComponent } from './account-ratings.component';
import { AccountRatingsEditComponent } from './edit/account-ratings-edit.component';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { AccountRatingItemsEditComponent } from './account-rating-items/edit/account-rating-items-edit.component';
import { AccountRatingItemsComponent } from './account-rating-items/account-rating-items.component';
import { AccountRatingItemsCreateComponent } from './account-rating-items/create/account-rating-items-create.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AddPointManualComponent } from './account-rating-items/add-point-manual/add-point-manual.component';
import { NgxCurrencyModule } from 'ngx-currency';

export const routes = [
  { path: '', component: AccountRatingsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxCurrencyModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    })
  ],
  declarations: [
    AccountRatingsComponent,
    AccountRatingItemsComponent,
    AccountRatingsEditComponent,
    AccountRatingItemsCreateComponent,
    AccountRatingItemsEditComponent,
    AddPointManualComponent
  ],
  entryComponents: [
    AccountRatingItemsComponent,
    AccountRatingsEditComponent,
    AccountRatingItemsCreateComponent,
    AccountRatingItemsEditComponent,
    AddPointManualComponent
  ]
})

export class AccountRatingsModule { }
