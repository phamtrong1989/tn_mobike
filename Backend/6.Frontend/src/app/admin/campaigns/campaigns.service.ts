import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { Campaign, CampaignCommand, DepositEvent, DepositEventCommand, DiscountCode, DiscountCodeCommand, VoucherCode, VoucherCodeCommand } from './campaigns.model';
import { ApiResponseData, ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AppCommon } from 'src/app/app.common';
import { LogAdmin } from '../category/projects/projects.model';
import { Account } from '../customer/accounts/accounts.model';

@Injectable()
export class CampaignsService {
    public url = "/api/work/Campaigns";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<Campaign[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: CampaignCommand) {
        return this.http.post<ApiResponseData<Campaign>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: CampaignCommand) {
        return this.http.put<ApiResponseData<Campaign>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<Campaign>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    delete(id: number) {
        return this.http.delete<ApiResponseData<Campaign>>(this.url + "/delete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    voucherCodeSearchPaged(params: any) {
        return this.http.get<ApiResponseData<VoucherCode[]>>(this.url + "/voucherCodeSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    voucherCodeCreate(user: VoucherCodeCommand) {
        return this.http.post<ApiResponseData<VoucherCode>>(this.url + "/voucherCodeCreate", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    voucherCodeEdit(user: VoucherCodeCommand) {
        return this.http.put<ApiResponseData<VoucherCode>>(this.url + "/voucherCodeEdit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    voucherCodeGetById(id: number) {
        return this.http.get<ApiResponseData<VoucherCode>>(this.url + "/voucherCodeGetById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    voucherCodeDelete(id: number) {
        return this.http.delete<ApiResponseData<VoucherCode>>(this.url + "/voucherCodeDelete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    logSearchPaged(params: any) {
        return this.http.get<ApiResponseData<LogAdmin[]>>(this.url + "/LogSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    depositEventSearchPaged(params: any) {
        return this.http.get<ApiResponseData<DepositEvent[]>>(this.url + "/depositEventSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    depositEventCreate(user: DepositEventCommand) {
        return this.http.post<ApiResponseData<DepositEvent>>(this.url + "/depositEventCreate", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    depositEventEdit(user: DepositEventCommand) {
        return this.http.put<ApiResponseData<DepositEvent>>(this.url + "/depositEventEdit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    depositEventGetById(id: number) {
        return this.http.get<ApiResponseData<DepositEvent>>(this.url + "/depositEventGetById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    depositEventDelete(id: number) {
        return this.http.delete<ApiResponseData<DepositEvent>>(this.url + "/depositEventDelete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    discountCodeSearchPaged(params: any) {
        return this.http.get<ApiResponseData<DiscountCode[]>>(this.url + "/discountCodeSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    discountCodeCreate(user: DiscountCodeCommand) {
        return this.http.post<ApiResponseData<DiscountCode>>(this.url + "/discountCodeCreate", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    discountCodeEdit(user: DiscountCodeCommand) {
        return this.http.put<ApiResponseData<DiscountCode>>(this.url + "/discountCodeEdit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    discountCodeGetById(id: number) {
        return this.http.get<ApiResponseData<DiscountCode>>(this.url + "/discountCodeGetById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    discountCodeDelete(id: number) {
        return this.http.delete<ApiResponseData<DiscountCode>>(this.url + "/discountCodeDelete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByAccount(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchByAccount", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}