import { CustomerGroup, Project } from "../category/projects/projects.model";
import { Account } from "../customer/accounts/accounts.model";
import { User } from "../user/users/users.model";

export class Campaign {
  id: number;
  projectId: number;
  investorId: number;
  name: string;
  status: number;
  type: number;
  description: string;
  createdDate: Date;
  updatedDate: Date;
  updatedUser: number;
  createdUser: number;
  totalLimit: number;
  totalCurentLimit: number;
  createdUserObject: User;
  updatedUserObject: User;
  startDate: Date;
  endDate: Date;
}

export class CampaignCommand {

  id: number;
  projectId: number;
  investorId: number;
  type: number;
  name: string;
  status: number;
  description: string;
  startDate: Date;
  endDate: Date;

  deserialize(input: Campaign): CampaignCommand {
    if (input == null) {
      input = new Campaign();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.investorId = input.investorId;
    this.name = input.name;
    this.status = input.status;
    this.description = input.description;
    this.type = input.type;
    this.startDate = input.startDate;
    this.endDate = input.endDate;
    return this;
  }
}

export class VoucherCode {
  id: number;
  projectId: number;
  customerGroupId: number;
  campaignId: number;
  code: string;
  type: number;
  startDate: Date;
  endDate: Date;
  limit: number;
  createdDate: Date;
  point: number;
  stacked: boolean;
  curentLimit: number;
  updatedUser: number;
  createdUser: number;
  public: boolean;
  customerGroup: CustomerGroup;
  project: Project;
  provisoType: number;
}

export class VoucherCodeCommand {
  id: number;
  projectId: number;
  customerGroupId: number;
  campaignId: number;
  code: string;
  type: number;
  limit: number;
  point: number;
  stacked: boolean;
  curentLimit: number;
  public: boolean;
  provisoType: number;

  deserialize(input: VoucherCode): VoucherCodeCommand {
    if (input == null) {
      input = new VoucherCode();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.customerGroupId = input.customerGroupId;
    this.campaignId = input.campaignId;
    this.code = input.code;
    this.type = input.type;
    this.limit = input.limit;
    this.point = input.point;
    this.stacked = input.stacked;
    this.curentLimit = input.curentLimit;
    this.public = input.public;
    this.provisoType = input.provisoType;
    return this;
  }
}

export class DepositEvent {

  id: number;
  projectId: number;
  investorId: number;
  customerGroupId: number;
  campaignId: number;
  transactionCode: string;
  startAmount: number;
  startDate: Date;
  endDate: Date;
  point: number;
  type: number;
  limit: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  percent: number;
  provisoType: number;
  maxAmount: number;
  subPoint: number;
  subPercent: number;
  subPointExpiry: number;
}

export class DepositEventCommand {
  id: number;
  projectId: number;
  investorId: number;
  customerGroupId: number;
  campaignId: number;
  transactionCode: string;
  startAmount: number;
  point: number;
  type: number;
  limit: number;
  percent: number;
  provisoType: number;
  maxAmount: number;
  subPoint: number;
  subPercent: number;
  subPointExpiry: number;

  deserialize(input: DepositEvent): DepositEventCommand {
    if (input == null) {
      input = new DepositEvent();
    }

    this.id = input.id;
    this.projectId = input.projectId;
    this.investorId = input.investorId;
    this.customerGroupId = input.customerGroupId;
    this.campaignId = input.campaignId;
    this.transactionCode = input.transactionCode;
    this.startAmount = input.startAmount;
    this.point = input.point;
    this.type = input.type;
    this.limit = input.limit;
    this.percent = input.percent;
    this.provisoType = input.provisoType;
    this.maxAmount = input.maxAmount;
    this.subPoint = input.subPoint;
    this.subPercent = input.subPercent;
    this.subPointExpiry = input.subPointExpiry;

    return this;
  }
}

export class DiscountCode {

  id: number;
  projectId: number;
  investorId: number;
  customerGroupId: number;
  campaignId: number;
  code: string;
  type: number;
  startDate: Date;
  endDate: Date;
  limit: number;
  curentLimit: number;
  point: number;
  public: boolean;
  provisoType: number;
  percent: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  beneficiaryAccountId: number;
  beneficiaryAccount: Account;
}

export class DiscountCodeCommand {

  id: number;
  projectId: number;
  customerGroupId: number;
  campaignId: number;
  code: string;
  type: number;
  limit: number;
  point: number;
  public: boolean;
  provisoType: number;
  percent: number;
  beneficiaryAccountId: number;
  beneficiaryAccount: Account;

  deserialize(input: DiscountCode): DiscountCodeCommand {
    if (input == null) {
      input = new DiscountCode();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.customerGroupId = input.customerGroupId;
    this.campaignId = input.campaignId;
    this.code = input.code;
    this.type = input.type;
    this.limit = input.limit;
    this.point = input.point;
    this.public = input.public;
    this.provisoType = input.provisoType;
    this.percent = input.percent;
    this.beneficiaryAccount = input.beneficiaryAccount;
    this.beneficiaryAccountId = input.beneficiaryAccountId;
    return this;
  }
}