import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { DepositEventCommand } from '../../campaigns.model';
import { CampaignsService } from '../../campaigns.service';

@Component({
  selector: 'app-deposit-events-edit',
  templateUrl: './deposit-events-edit.component.html',
  providers: [
    CampaignsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class DepositEventsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  customerGroups: [];
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<DepositEventsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: DepositEventCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public campaignsService: CampaignsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.customerGroups = this.categoryData.customerGroups.filter(x => x.projectId == this.data.projectId);
    this.form = this.fb.group({
      id: [0],
      projectId: [0, Validators.compose([Validators.required])],
      investorId: [0],
      customerGroupId: [null, Validators.compose([Validators.required])],
      campaignId: [0],
      transactionCode: [null],
      startAmount: [10000, Validators.compose([Validators.required, Validators.min(10000)])],
      point: [0, Validators.compose([Validators.required])],
      subPoint: [0, Validators.compose([Validators.required])],
      type: [0, Validators.compose([Validators.required])],
      limit: [99999, Validators.compose([Validators.required])],
      percent: [0],
      subPercent: [20],
      provisoType: [null, Validators.compose([Validators.required])],
      maxAmount: [100000, Validators.compose([Validators.required])],
      subPointExpiry: [0, Validators.compose([Validators.required])]
    });
    this.form.get('projectId').setValue(this.data.projectId);
    this.form.get('investorId').setValue(this.data.investorId);
    this.form.get('campaignId').setValue(this.data.campaignId);
  }
  
  ngOnInit() {
    if (this.data.id > 0) {
      this.form.setValue(this.data);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.campaignsService.depositEventGetById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new DepositEventCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.campaignsService.depositEventEdit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.campaignsService.depositEventCreate(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.campaignsService.depositEventDelete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 2) {
            this.snackBar.open("Không thể xóa, chiến dịch này đã phát sinh giao dịch rồi", this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
