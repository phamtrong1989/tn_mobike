import { Warehouse } from './../../infrastructure/warehouses/warehouses.model';

export class Supplies {
  id: number;
  code: string;
  name: string;
  descriptions: string;
  quantity: number;
  unit: string;
  type: number;
  price: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  suppliesWarehouses: SuppliesWarehouse[];
}

export class SuppliesCommand {
  id: number;
  code: string;
  name: string;
  descriptions: string;
  quantity: number;
  unit: string;
  type: number;
  price: number;

  deserialize(input: Supplies): SuppliesCommand {
    if (input == null) {
      input = new Supplies();
    }

    this.id = input.id;
    this.code = input.code;
    this.name = input.name;
    this.descriptions = input.descriptions;
    this.quantity = input.quantity;
    this.unit = input.unit;
    this.type = input.type;
    this.price = input.price;
    return this;
  }
}

export class SuppliesWarehouse {
  id: number;
  suppliesId: number;
  warehouseId: number;
  quantity: number;
  warehouse: Warehouse;
  supplies: Supplies;
}