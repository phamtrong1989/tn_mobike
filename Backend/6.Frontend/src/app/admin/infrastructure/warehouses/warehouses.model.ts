import { User } from "../../user/users/users.model";

export class Warehouse {
  id: number;
  name: string;
  address: string;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  createdUserObject: User;
  updatedUserObject: User;
  type: number;
  employeeId: number;
  employee: User;
}

export class WarehouseCommand {
  id: number;
  name: string;
  address: string;
  type: number;
  employeeId: number;
  employee: User;

  deserialize(input: Warehouse): WarehouseCommand {
    if (input == null) {
      input = new Warehouse();
    }
    this.id = input.id;
    this.name = input.name;
    this.address = input.address;
    this.type = input.type;
    this.employeeId = input.employeeId;
    this.employee = input.employee;

    return this;
  }
}