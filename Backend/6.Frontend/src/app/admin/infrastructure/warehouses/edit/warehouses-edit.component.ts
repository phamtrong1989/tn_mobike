import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Warehouse, WarehouseCommand } from '../warehouses.model';
import { WarehousesService } from '../warehouses.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/admin/user/users/users.model';

@Component({
  selector: 'app-warehouses-edit',
  templateUrl: './warehouses-edit.component.html',
  providers: [
    WarehousesService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class WarehousesEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  employees: User[];
  employeeKey: FormControl = new FormControl("");
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, employeeSearching: false };

  constructor(
    public dialogRef: MatDialogRef<WarehousesEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: WarehouseCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public warehousesService: WarehousesService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      name: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(200)])],
      address: [null, Validators.compose([Validators.maxLength(500)])],
      type: [null, Validators.compose([Validators.required])],
      employeeId: [null, Validators.compose([Validators.required])],
      employee: [null]
    });
  }

  ngOnInit() {
    this.initSearchEmployee();
    if (this.data.id > 0) {
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.warehousesService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new WarehouseCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.warehousesService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else if (res.status == 2) {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.warehousesService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new WarehouseCommand().deserialize(res.data);
            this.form.setValue(this.data);
            this.close(res);
          }
          else if (res.status == 2) {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.warehousesService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
          else if (res.status == 2) {
            this.snackBar.open("Không thể xóa kho của nhân viên điều phối", this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  initSearchEmployee() {
    this.employeeKey.valueChanges.subscribe(rs => {
      this.isProcess.employeeSearching = false;
      if (rs != "") {
        this.isProcess.employeeSearching = true;
        this.warehousesService.searchByEmployeeFree({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('employeeId').setValue(this.data.employeeId);
          }
          this.employees = rs.data;
          this.isProcess.employeeSearching = false;
        });
      }
    },
      error => {
        this.isProcess.employeeSearching = false;
      });
  }

  onChangeWarehouseType(value) {
    if (this.data.id == 0) {
      if (value == 0) {
        this.form.get('employeeId').setValue(0);
      }
    } else {
      if (value == 0) {
        this.form.get('employeeId').setValue(0);
      } else {
        this.form.get('employeeId').setValue(this.data.employeeId);
      }
    }
    //
    this.data.type = value;
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
