import { User } from "../../user/users/users.model";
import { Bike } from "../bikes/bikes.model";


export class GpsData {
  id: string;
  imei: string;
  lat: number;
  long: number;
  createTime: Date;
  createTimeTicks: number;
}

export class Station {
  id: number;
  projectId: number;
  investorId: number;
  cityId: number;
  districtId: number;
  width: number;
  height: number;
  spaces: number;
  name: string;
  displayName: string;
  address: string;
  lat: number;
  lng: number;
  description: string;
  status: number;
  createdDate: Date;
  updatedDate: Date;
  createdUser: number;
  updatedUser: number;
  createdUserObject: User;
  updatedUserObject: User;
  returnDistance: number;
  totalBike: number;
  totalBikeGood: number;
  totalBikeNotGood: number;
  bikes: Bike[];
  hideSearch:boolean;
  managerUserId: number;
  managerUserObject: User;
}

export class StationCommand {

  id: number;
  projectId: number;
  investorId: number;
  cityId: number;
  districtId: number;
  width: number;
  height: number;
  name: string;
  address: string;
  lat: number;
  lng: number;
  description: string;
  status: number;
  spaces: number;
  displayName: string;
  managerUserId: number;
  managerUserObject: User;
  returnDistance: number;

  deserialize(input: Station): StationCommand {
    if(input==null)
    {
      input  = new Station();
    }
    this.id = input.id;
    this.projectId = input.projectId;
    this.investorId = input.investorId;
    this.cityId = input.cityId;
    this.districtId = input.districtId;
    this.width = input.width;
    this.height = input.height;
    this.name = input.name;
    this.address = input.address;
    this.lat = input.lat;
    this.lng = input.lng;
    this.spaces = input.spaces;
    this.description = input.description;
    this.status = input.status;
    this.displayName = input.displayName;
    this.managerUserId = input.managerUserId;
    this.managerUserObject = input.managerUserObject;
    this.returnDistance = input.returnDistance;
    return this;
  }
}


export class StationResource {
  id: number;
  stationId: number;
  name: string;
  address: string;
  language: string;
  displayName: string;
}

export class StationResourceCommand {

  id: number;
  stationId: number;
  name: string;
  address: string;
  language: string;
  displayName: string;

  deserialize(input: StationResource): StationResourceCommand {
    if(input==null)
    {
      input  = new StationResource();
    }
    this.id = input.id;
    this.stationId = input.stationId;
    this.name = input.name;
    this.address = input.address;
    this.language = input.language;
    this.displayName = input.displayName;
    return this;
  }
}