import { Component, OnInit, Inject, ViewChild, ElementRef, NgZone } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { MapsAPILoader } from '@agm/core';
import { StationsService } from '../stations.service';
import { StationResourceCommand } from '../stations.model';

@Component({
  selector: 'app-stations-resource-edit',
  templateUrl: './stations-resource-edit.component.html',
  providers: [
    StationsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class StationsResourceEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  selectedMarker: Marker;
  location: Location
  @ViewChild('search')
  public searchElementRef: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<StationsResourceEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: StationResourceCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public stationsService: StationsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      name: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(200)])],
      displayName: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(200)])],
      address: [""],
      stationId: [0],
      language: ["en"]
    });
  }

  ngOnInit() {
    this.form.get('stationId').setValue(this.data.stationId);
    this.form.get('language').setValue(this.data.language);
    this.getData();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.stationsService.stationResourceGetById(this.data.stationId, this.data.language).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new StationResourceCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.stationsService.stationResourceEdit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
        } 
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.close(res)
        this.isProcess.saveProcess = false;
      });
    }
  }
  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
interface Marker {
  lat: number;
  lng: number;
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  markers: Marker[];
}