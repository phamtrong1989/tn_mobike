import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../../app.settings';
import { Settings, ApiResponseData } from '../../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MaintenanceRepairItemsEditComponent } from './edit/maintenance-repair-items-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage';
import { MaintenanceRepair, MaintenanceRepairItem, MaintenanceRepairItemCommand } from '../maintenance-repairs.model';
import { MaintenanceRepairsService } from '../maintenance-repairs.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-maintenance-repair-items',
  templateUrl: './maintenance-repair-items.component.html',
  providers: [MaintenanceRepairsService, AppCommon]
})

export class MaintenanceRepairItemsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  @Input() maintenanceRepair: MaintenanceRepair;
  heightTable: number = 300;
  listData: MaintenanceRepairItem[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, maintenanceRepairId: 0 };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "bikeId", "type", "suppliesId", "suppliesQuantity", "completeDate", "action"];
  public settings: Settings;
  public categoryData: any;
  isEditer: boolean = true;
  outData: any;
  @Input() changing: Subject<boolean>;

  constructor(
    public appSettings: AppSettings,
    public maintenanceRepairsService: MaintenanceRepairsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }
  ngAfterViewInit(): void {
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.changing.subscribe((rs) => {
      this.isEditer = rs;
    });
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.maintenanceRepairId = this.maintenanceRepair.id;
    this.maintenanceRepairsService.maintenanceRepairItemSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.outData = res.outData;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: MaintenanceRepairItem) {
    var model = new MaintenanceRepairItemCommand();
    if (data == null) {
      model.id = 0;
      model.maintenanceRepairId = this.maintenanceRepair.id;
    }
    else {
      model = new MaintenanceRepairItemCommand().deserialize(data);
    }
    model.warehouseId = this.maintenanceRepair.warehouseId;

    this.dialog.open(MaintenanceRepairItemsEditComponent, {
      data: { maintenanceRepairItem: model , isEditer : this.isEditer },
      panelClass: 'dialog-600',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }
}