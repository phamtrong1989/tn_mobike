import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../../app.common'
import { AppStorage } from '../../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { MaintenanceRepairsService } from '../../maintenance-repairs.service';
import { MaintenanceRepairItem, MaintenanceRepairItemCommand } from '../../maintenance-repairs.model';
import { Supplies } from '../../../suppliess/suppliess.model';
import { Bike } from '../../../bikes/bikes.model';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-maintenance-repair-items-edit',
  templateUrl: './maintenance-repair-items-edit.component.html',
  providers: [
    MaintenanceRepairsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class MaintenanceRepairItemsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, suppliesSearching: false, bikeSearching: false, isEditer: false };
  suppliesKey: FormControl = new FormControl();
  bikeKey: FormControl = new FormControl();
  suppliess: Supplies[] = [];
  bikes: Bike[] = [];
  uploadFileOut: FileDataDTO[];
  whId: number;
  constructor(
    public dialogRef: MatDialogRef<MaintenanceRepairItemsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { maintenanceRepairItem: MaintenanceRepairItemCommand, isEditer: false },
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public maintenanceRepairsService: MaintenanceRepairsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      maintenanceRepairId: [this.data.maintenanceRepairItem.maintenanceRepairId],
      bikeId: [null, Validators.compose([Validators.required])],
      type: [0],
      status: [true],
      descriptions: [null, Validators.compose([Validators.maxLength(2000)])],
      files: [null],
      completeDate: [new Date(), Validators.compose([Validators.required])],
      suppliesId: [0],
      suppliesQuantity: [0, Validators.compose([Validators.required, Validators.min(0)])],
      bike: [null],
      supplies: [null],
      warehouseId: [0]
    });
  }

  ngOnInit() {
    this.initSearchSupplies();
    this.initSearchBike();
    this.whId = this.data.maintenanceRepairItem.warehouseId;

    if (this.data.maintenanceRepairItem.id > 0) {
      if(this.data.isEditer)
      {
        this.form.enable();
      }
      else
      {
        this.form.disable();
      }

      if (this.data.maintenanceRepairItem.files) {
        this.uploadFileOut = JSON.parse(this.data.maintenanceRepairItem.files)
      }
      if(this.data.maintenanceRepairItem.bike != null)
      {
        this.bikes = [];
        this.bikes.push(this.data.maintenanceRepairItem.bike);
      }
      if(this.data.maintenanceRepairItem.supplies != null)
      {
        this.suppliess = [];
        this.suppliess.push(this.data.maintenanceRepairItem.supplies);
      }
      this.form.setValue(this.data.maintenanceRepairItem);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.maintenanceRepairsService.maintenanceRepairItemGetById(this.data.maintenanceRepairItem.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data.maintenanceRepairItem = new MaintenanceRepairItemCommand().deserialize(rs.data);
        this.form.setValue(this.data.maintenanceRepairItem);
        if (this.data.maintenanceRepairItem.files) {
          this.uploadFileOut = JSON.parse(this.data.maintenanceRepairItem.files)
        }
        if(this.data.maintenanceRepairItem.bike != null)
        {
          this.bikes = [];
          this.bikes.push(this.data.maintenanceRepairItem.bike);
        }
        if(this.data.maintenanceRepairItem.supplies != null)
        {
          this.suppliess = [];
          this.suppliess.push(this.data.maintenanceRepairItem.supplies);
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }
  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));

      if (this.data.maintenanceRepairItem.id > 0) {
        this.maintenanceRepairsService.maintenanceRepairItemEdit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.close(1);
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.maintenanceRepairsService.maintenanceRepairItemCreate(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.close(1);
          this.isProcess.saveProcess = false;
        });
      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.maintenanceRepairsService.maintenanceRepairItemDelete(this.data.maintenanceRepairItem.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });
      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  initSearchSupplies() {
    this.suppliesKey.valueChanges.subscribe(rs => {
      this.isProcess.suppliesSearching = false;
      if (rs != "") {
        this.isProcess.suppliesSearching = true;
        this.maintenanceRepairsService.suppliesSearchKey({ key: rs, warehouseId: this.whId }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('suppliesId').setValue(null);
          }
          this.suppliess = rs.data;
          this.isProcess.suppliesSearching = false;
        });
      }
    },
      error => {
        this.isProcess.suppliesSearching = false;
      });
  }

  initSearchBike() {
    this.bikeKey.valueChanges.subscribe(rs => {
      this.isProcess.bikeSearching = false;
      if (rs != "") {
        this.isProcess.bikeSearching = true;
        this.maintenanceRepairsService.bikesSearchKey({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('bikeId').setValue(null);
          }
          this.bikes = rs.data;
          this.isProcess.bikeSearching = false;
        });
      }
    },
      error => {
        this.isProcess.bikeSearching = false;
      });
  }
}
