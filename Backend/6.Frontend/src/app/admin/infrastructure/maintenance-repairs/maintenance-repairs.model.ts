import { Bike } from "../bikes/bikes.model";
import { Supplies } from "../suppliess/suppliess.model";

export class MaintenanceRepair {
  id: number;
  name: string;
  descriptions: string;
  files: string;
  startDate: Date;
  endDate: Date;
  status: number;
  warehouseId: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  lastDescriptions: string;
  lastFiles: string; 
}

export class MaintenanceRepairCommand {
  id: number;
  name: string;
  descriptions: string;
  files: string;
  startDate: Date;
  endDate: Date;
  status: number;
  warehouseId: number;
  lastDescriptions: string;
  lastFiles: string; 

  deserialize(input: MaintenanceRepair): MaintenanceRepairCommand {
    if (input == null) {
      input = new MaintenanceRepair();
    }
    this.id = input.id;
    this.name = input.name;
    this.descriptions = input.descriptions;
    this.files = input.files;
    this.startDate = input.startDate;
    this.endDate = input.endDate;
    this.status = input.status;
    this.warehouseId = input.warehouseId;
    this.lastDescriptions = input.lastDescriptions;
    this.lastFiles = input.lastFiles;
    return this;
  }
}

export class MaintenanceRepairItem {

  id: number;
  maintenanceRepairId: number;
  bikeId: number;
  projectId: number;
  investorId: number;
  type: number;
  status: boolean;
  descriptions: string;
  files: string;
  completeDate: Date;
  suppliesId: number;
  suppliesCode: string;
  suppliesQuantity: number;
  employeeId: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  bike: Bike;
  supplies: Supplies;
}

export class MaintenanceRepairItemCommand {

  id: number;
  maintenanceRepairId: number;
  bikeId: number;
  type: number;
  status: boolean;
  descriptions: string;
  files: string;
  completeDate: Date;
  suppliesId: number;
  suppliesQuantity: number;
  warehouseId: number;
  bike: Bike;
  supplies: Supplies;

  deserialize(input: MaintenanceRepairItem): MaintenanceRepairItemCommand {
    if (input == null) {
      input = new MaintenanceRepairItem();
    }
    this.id = input.id;
    this.maintenanceRepairId = input.maintenanceRepairId;
    this.bikeId = input.bikeId;
    this.type = input.type;
    this.status = input.status;
    this.descriptions = input.descriptions;
    this.files = input.files;
    this.completeDate = input.completeDate;
    this.suppliesId = input.suppliesId;
    this.suppliesQuantity = input.suppliesQuantity;
    this.warehouseId = 0;
    this.bike = input.bike;
    this.supplies = input.supplies;
    return this;
  }
}