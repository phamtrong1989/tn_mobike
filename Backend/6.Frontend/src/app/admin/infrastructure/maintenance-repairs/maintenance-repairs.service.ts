import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEvent, HttpRequest, HttpParams } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { MaintenanceRepair, MaintenanceRepairCommand, MaintenanceRepairItem, MaintenanceRepairItemCommand } from './maintenance-repairs.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { Supplies } from '../suppliess/suppliess.model';
import { Bike } from '../bikes/bikes.model';
import { Observable } from 'rxjs';

@Injectable()
export class MaintenanceRepairsService {
    public url = "/api/work/MaintenanceRepairs";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<MaintenanceRepair[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: MaintenanceRepairCommand) {
        return this.http.post<ApiResponseData<MaintenanceRepair>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: MaintenanceRepairCommand) {
        return this.http.put<ApiResponseData<MaintenanceRepair>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<MaintenanceRepair>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    cancel(id: number) {
        return this.http.post<ApiResponseData<MaintenanceRepair>>(this.url + "/cancel/" + id, {}).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    
    approve(id: number, endDate: string) {
        return this.http.post<ApiResponseData<MaintenanceRepair>>(this.url + "/approve/" + id +"/"+endDate, {}).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    uploadPath(): string {
        return this.url + "/upload"
    }

    viewPath(): string
    {
        return this.url + "/view"
    }
    
    maintenanceRepairItemSearchPaged(params: any) {
        return this.http.get<ApiResponseData<MaintenanceRepairItem[]>>(this.url + "/maintenanceRepairItemSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
   
    maintenanceRepairItemCreate(user: MaintenanceRepairItemCommand) {
        return this.http.post<ApiResponseData<MaintenanceRepairItem>>(this.url + "/maintenanceRepairItemCreate", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    maintenanceRepairItemEdit(user: MaintenanceRepairItemCommand) {
        return this.http.put<ApiResponseData<MaintenanceRepairItem>>(this.url + "/maintenanceRepairItemEdit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    maintenanceRepairItemGetById(id: number) {
        return this.http.get<ApiResponseData<MaintenanceRepairItem>>(this.url + "/maintenanceRepairItemGetById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    maintenanceRepairItemDelete(id: number) {
        return this.http.delete<ApiResponseData<MaintenanceRepairItem>>(this.url + "/maintenanceRepairItemDelete/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    
    suppliesSearchKey(params: any) {
        return this.http.get<ApiResponseData<Supplies[]>>(this.url + "/SuppliesSearchKey", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bikesSearchKey(params: any) {
        return this.http.get<ApiResponseData<Bike[]>>(this.url + "/BikesSearchKey", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    export(params: any): Observable<HttpEvent<Blob>> {
        let obj = {};
        for (const prop in params) {
            if (params[prop] != null) {
                obj[prop] = params[prop];
            }
        }

        return this.http.request(new HttpRequest(
            'POST',
            this.url + "/report",
            null,
            {
                reportProgress: true,
                responseType: 'blob',
                params: new HttpParams({ fromObject: obj })
            }));
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}