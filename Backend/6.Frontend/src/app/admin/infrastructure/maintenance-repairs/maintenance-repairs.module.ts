import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { MaintenanceRepairsComponent } from './maintenance-repairs.component';
import { MaintenanceRepairsEditComponent } from './edit/maintenance-repairs-edit.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { MaintenanceRepairItemsEditComponent } from './maintenance-repair-items/edit/maintenance-repair-items-edit.component';
import { MaintenanceRepairItemsComponent } from './maintenance-repair-items/maintenance-repair-items.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

export const routes = [
  { path: '', component: MaintenanceRepairsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
	 TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    }) 
  ],
  declarations: [
    MaintenanceRepairsComponent,
    MaintenanceRepairsEditComponent,
    MaintenanceRepairItemsEditComponent,
    MaintenanceRepairItemsComponent
  ],
  entryComponents:[
    MaintenanceRepairsEditComponent,
    MaintenanceRepairItemsEditComponent,
    MaintenanceRepairItemsComponent
  ]
})
export class MaintenanceRepairsModule { }
