import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MaintenanceRepair, MaintenanceRepairCommand } from '../maintenance-repairs.model';
import { MaintenanceRepairsService } from '../maintenance-repairs.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { FileDataDTO } from 'src/app/app.settings.model';
import { Subject } from 'rxjs';
import { Warehouse } from 'src/app/admin/infrastructure/warehouses/warehouses.model';

@Component({
  selector: 'app-maintenance-repairs-edit',
  templateUrl: './maintenance-repairs-edit.component.html',
  providers: [
    MaintenanceRepairsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class MaintenanceRepairsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  uploadFileOut: FileDataDTO[];
  uploadLastFileOut: FileDataDTO[];
  changingValue: Subject<boolean> = new Subject();
  warehouses: Warehouse[];
  accountInfo: any;
  constructor(
    public dialogRef: MatDialogRef<MaintenanceRepairsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: MaintenanceRepairCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public maintenanceRepairsService: MaintenanceRepairsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.accountInfo = this.appStorage.getAccountInfo();
    this.form = this.fb.group({
      id: [0],
      name: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(200)])],
      descriptions: [null, Validators.compose([Validators.maxLength(2000)])],
      files: [null],
      startDate: [new Date(), Validators.compose([Validators.required])],
      endDate: [null],
      status: [0],
      warehouseId: [null, Validators.compose([Validators.required])],
      lastDescriptions: [null, Validators.compose([Validators.maxLength(2000)])],
      lastFiles: [null]
    });

    if(this.data.id > 0)
    {
      this.warehouses = this.categoryData.warehouses.filter(x=>x.type == 1);
    }
    else
    {
      this.warehouses= this.categoryData.warehouses.filter(x=>x.type == 1 && x.employeeId == this.accountInfo.id);
      if(this.warehouses.length > 0)
      {
        this.form.get('warehouseId').setValue(this.warehouses[0].id);
        this.form.get('warehouseId').disable();
      }
      else
      {
        this.form.get('warehouseId').setValue(null);
      }
    }
  }

  ngOnInit() {
    if (this.data.id > 0) {
      this.initEdit();
      if (this.data.files) {
        this.uploadFileOut = JSON.parse(this.data.files);
        this.uploadLastFileOut = JSON.parse(this.data.lastFiles);
      }
      this.form.setValue(this.data);
      this.getData();
    }
  }
  
  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }

  getOutPutLast(data: FileDataDTO[]) {
    this.uploadLastFileOut = data;
  }

  initEdit() {
    if (this.data.status == 0) {
      this.form.enable();
      this.changingValue.next(true);
    }
    else {
      this.form.disable();
      this.changingValue.next(false);
    }
    if(this.data.id > 0)
    {
      this.form.get('warehouseId').disable();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.maintenanceRepairsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new MaintenanceRepairCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        if (this.data.files) {
          this.uploadFileOut = JSON.parse(this.data.files);
          this.uploadLastFileOut = JSON.parse(this.data.lastFiles);
        }
        this.initEdit();
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;

    if (this.form.valid) {
      this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
      this.form.get('lastFiles').setValue(JSON.stringify(this.uploadLastFileOut));

      if (this.data.id > 0) {
        this.maintenanceRepairsService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
            this.close(res);
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.form.get('warehouseId').enable();
        setTimeout(() => {
          this.maintenanceRepairsService.create(this.form.value).then((res) => {
            if (res.status == 1) {
              this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
              this.data = new MaintenanceRepairCommand().deserialize(res.data);
              this.form.setValue(this.data);
              this.form.get('warehouseId').disable();
            }
            else {
              this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
            }
            this.isProcess.saveProcess = false;
          });
        }, 200);
      }
    }
  }

  cancel() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Bạn có muốn hủy lịch này không?",
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.maintenanceRepairsService.cancel(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });
      }
    });
  }

  approve() {
    if (this.form.get('endDate').value == null) {
      this.snackBar.open("Bạn muốn hoàn thành thì phải chọn ngày kết thúc", this.baseTranslate["msg-danger-type"], { duration: 5000 });
      this.isProcess.saveProcess = false;
      return;
    }

    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Bạn có muốn hoàn thành sửa chữa, bảo dưỡng này không? nếu hoàn thành thì dữ liệu sẽ không thể cập nhât và số lượng vật tư sẽ bị trừ theo số lượng sử dụng.",
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.maintenanceRepairsService.approve(this.data.id, this.appCommon.formatDateTime(this.form.get('endDate').value, "yyyy-MM-dd")).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(this.data)
            this.isProcess.deleteProcess = false;
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
          }
        });
      }
    });
  }


  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
