import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { ImportBillsComponent } from './import-bills.component';
import { ImportBillsEditComponent } from './edit/import-bills-edit.component';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ImportBillItemsComponent } from './import-bill-items/import-bill-items.component';
import { ImportBillItemEditComponent } from './import-bill-items/edit/import-bill-items-edit.component';

export const routes = [
  { path: '', component: ImportBillsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    NgxCurrencyModule,

    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    }),
  ],
  declarations: [
    ImportBillsComponent,
    ImportBillsEditComponent,
    ImportBillItemsComponent,
    ImportBillItemEditComponent
  ],
  entryComponents: [
    ImportBillsEditComponent,
    ImportBillItemsComponent,
    ImportBillItemEditComponent
  ]
})
export class ImportBillsModule { }