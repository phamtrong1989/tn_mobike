import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ImportBill, ImportBillCommand, ImportBillItem, ImportBillItemCommand } from './import-bills.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { Supplies } from '../suppliess/suppliess.model';

@Injectable()
export class ImportBillsService {
    public url = "/api/work/ImportBills";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<ImportBill[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: ImportBillCommand) {
        return this.http.post<ApiResponseData<ImportBill>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: ImportBillCommand) {
        return this.http.put<ApiResponseData<ImportBill>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    approve(user: ImportBillCommand) {
        return this.http.put<ApiResponseData<ImportBill>>(this.url + "/approve", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    cancel(user: ImportBillCommand) {
        return this.http.put<ApiResponseData<ImportBill>>(this.url + "/cancel", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    denied(user: ImportBillCommand) {
        return this.http.put<ApiResponseData<ImportBill>>(this.url + "/denied", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<ImportBill>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }

    uploadPath(): string {
        return this.url + "/upload"
    }

    viewPath(): string {
        return this.url + "/view"
    }

    // Import Bill Item
    importBillItemSearchPaged(params: any) {
        return this.http.get<ApiResponseData<ImportBillItem[]>>(this.url + "/importBillItemSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    importBillItemGetById(id: number) {
        return this.http.get<ApiResponseData<ImportBillItem>>(this.url + "/importBillItemGetById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    importBillItemCreate(model: ImportBillItemCommand) {
        return this.http.post<ApiResponseData<ImportBillItem>>(this.url + "/importBillItemCreate", model).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    importBillItemEdit(model: ImportBillItemCommand) {
        return this.http.put<ApiResponseData<ImportBillItem>>(this.url + "/importBillItemEdit", model).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    importBillItemDelete(id: number) {
        return this.http.delete<ApiResponseData<ImportBillItem>>(this.url + "/importBillItemDelete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchBySupplies(params: any) {
        return this.http.get<ApiResponseData<Supplies[]>>(this.url + "/searchBySupplies", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    //
}