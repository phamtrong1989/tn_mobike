import { Supplies } from './../../../suppliess/suppliess.model';
import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../../app.common'
import { AppStorage } from '../../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { ImportBillCommand, ImportBillItemCommand } from '../../import-bills.model';
import { ImportBillsService } from '../../import-bills.service';

@Component({
  selector: 'app-import-bill-items-edit',
  templateUrl: './import-bill-items-edit.component.html',
  providers: [
    ImportBillsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ImportBillItemEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  customerGroups: [];
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, suppliesSearching: false };
  suppliesKey: FormControl = new FormControl("");
  supplies: Supplies[];

  constructor(
    public dialogRef: MatDialogRef<ImportBillItemEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ImportBillItemCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public importBillsService: ImportBillsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();

    this.form = this.fb.group({
      id: [0],
      importBillId: [0, Validators.compose([Validators.required])],
      suppliesId: [null, Validators.compose([Validators.required])],
      count: [null, Validators.compose([Validators.required])],
      price: [null, Validators.compose([Validators.required])],
      total: [0],
      note: [null],
      files: [null],
      supplies: [null]
    });
    this.form.get('importBillId').setValue(this.data.importBillId);
    this.form.get('suppliesId').setValue(this.data.suppliesId);
  }

  ngOnInit() {
    this.initSearchSupplies();
    if (this.data.id > 0) {
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.importBillsService.importBillItemGetById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new ImportBillItemCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.importBillsService.importBillItemEdit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.importBillsService.importBillItemCreate(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.importBillsService.importBillItemDelete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(res.message, this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  initSearchSupplies() {
    this.suppliesKey.valueChanges.subscribe(rs => {
      this.isProcess.suppliesSearching = false;
      if (rs != "") {
        this.isProcess.suppliesSearching = true;
        this.importBillsService.searchBySupplies({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('suppliesId').setValue(this.data.suppliesId);
          }
          this.supplies = rs.data;
          this.isProcess.suppliesSearching = false;
        });
      }
    },
      error => {
        this.isProcess.suppliesSearching = false;
      });
  }
}
