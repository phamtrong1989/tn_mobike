import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../../app.settings';
import { Settings } from '../../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage';
import { ImportBillsService } from '../import-bills.service';
import { ImportBillCommand, ImportBill, ImportBillItemCommand, ImportBillItem } from '../import-bills.model';
import { ImportBillItemEditComponent } from './edit/import-bill-items-edit.component';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-import-bill-items',
  templateUrl: './import-bill-items.component.html',
  providers: [ImportBillsService, AppCommon]
})

export class ImportBillItemsComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 220;

  @Input() importBill: ImportBillCommand;
  listData: ImportBillItem[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, projectId: "", key: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "suppliesId", "price", "count", "total", "action"];
  baseTranslate: any;
  public settings: Settings;
  public categoryData: any;
  public totalBill: number;

  constructor(
    public appSettings: AppSettings,
    public importBillsService: ImportBillsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    private translate: TranslateService
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.importBillId = this.importBill.id;

    this.importBillsService.importBillItemSearchPaged(this.params).then((res) => {

      this.totalBill = res.data.reduce(function (prev, cur) {
        return prev + cur.total;
      }, 0);
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: ImportBillItem) {
    var model = new ImportBillItemCommand();
    if (data == null) {
      model.id = 0;
      model.importBillId = this.importBill.id;
    }
    else {
      model = new ImportBillItemCommand().deserialize(data);
    }
    this.dialog.open(ImportBillItemEditComponent, {
      data: model,
      panelClass: 'dialog-600',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }
  
  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
    }, 1);
  }
}