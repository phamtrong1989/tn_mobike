import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ImportBill, ImportBillCommand } from '../import-bills.model';
import { ImportBillsService } from '../import-bills.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { FileDataDTO } from 'src/app/app.settings.model';
import { Warehouse } from 'src/app/admin/infrastructure/warehouses/warehouses.model';

@Component({
  selector: 'app-import-bills-edit',
  templateUrl: './import-bills-edit.component.html',
  providers: [
    ImportBillsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ImportBillsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  public pathUpload: string;
  uploadFileOut: FileDataDTO[];
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, cancelProcess: false, approveProcess: false };
  public warehousesT: Warehouse[];

  constructor(
    public dialogRef: MatDialogRef<ImportBillsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ImportBillCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public importBillsService: ImportBillsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.warehousesT = this.categoryData.warehouses.filter(x => x.type == 0);

    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      warehouseId: [null, Validators.compose([Validators.required])],
      type: [null, Validators.compose([Validators.required])],
      status: [0],
      code: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(50)])],
      total: [0],
      dateBill: [null, Validators.compose([Validators.required])],
      receiverUser: [null, Validators.compose([Validators.maxLength(200)])],
      files: [""],
      note: [null, Validators.compose([Validators.maxLength(2000)])],
      approveBillPermission: [false],
      cancelBillPermission: [false],
      deniedBillPermission: [false],
      editItemPermission: [false],
    });
  }

  ngOnInit() {
    if (this.data.id > 0) {
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.importBillsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new ImportBillCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        if (this.data.files) {
          this.uploadFileOut = JSON.parse(this.data.files)
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));

      if (this.data.id > 0) {
        this.importBillsService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.data = new ImportBillCommand().deserialize(res.data);
            this.form.setValue(this.data);
          }
          else if (res.status == 2) {
            this.snackBar.open("Mã phiếu đã tồn tại!", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.importBillsService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new ImportBillCommand().deserialize(res.data);
            this.form.setValue(this.data);
          }
          else if (res.status == 2) {
            this.snackBar.open("Mã phiếu đã tồn tại!", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
  }

  approve() {
    this.isProcess.saveProcess = true;

    this.importBillsService.approve(this.data).then((res) => {
      if (res.status == 1) {
        this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
        this.close(res)
      }
      else {
        this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
      }
      this.isProcess.saveProcess = false;
    });
  }

  denied() {
    this.isProcess.saveProcess = true;

    this.importBillsService.denied(this.data).then((res) => {
      if (res.status == 1) {
        this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
        this.close(res)
      }
      else {
        this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
      }
      this.isProcess.saveProcess = false;
    });
  }

  cancel() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-cancel-bill-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        let importBill = this.data;

        this.isProcess.deleteProcess = true;
        this.importBillsService.cancel(importBill).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(res.message, this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
