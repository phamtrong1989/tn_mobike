import { User } from "../../user/users/users.model";
import { Supplies } from "../suppliess/suppliess.model";

export class ImportBill {

  id: number;
  code: string;
  dateBill: Date;
  total: number;
  receiverUser: string;
  warehouseId: number;
  note: string;
  files: string;
  type: number;
  status: number;
  pCUConfirm: Date;
  pCUUser: number;
  tKVTSConfirm: Date;
  tKVTSUser: number;
  bODVTSConfirm: Date;
  bODVTSUser: number;
  completeConfirm: Date;
  completeUser: number;
  createdDate: Date;
  createdUser: number;
  createdUserObject: User;
  approveBillPermission: boolean;
  cancelBillPermission: boolean;
  deniedBillPermission: boolean;
  editItemPermission: boolean;

}

export class ImportBillCommand {
  id: number;
  type: number;
  status: number;
  code: string;
  dateBill: Date;
  total: number;
  receiverUser: string;
  warehouseId: number;
  note: string;
  files: string;
  approveBillPermission: boolean;
  cancelBillPermission: boolean;
  deniedBillPermission: boolean;
  editItemPermission: boolean;

  deserialize(input: ImportBill): ImportBillCommand {
    if (input == null) {
      input = new ImportBill();
    }
    this.id = input.id;
    this.code = input.code;
    this.dateBill = input.dateBill;
    this.total = input.total;
    this.receiverUser = input.receiverUser;
    this.warehouseId = input.warehouseId;
    this.note = input.note;
    this.files = input.files;
    this.type = input.type;
    this.status = input.status;
    this.approveBillPermission = input.approveBillPermission;
    this.cancelBillPermission = input.cancelBillPermission;
    this.deniedBillPermission = input.deniedBillPermission;
    this.editItemPermission = input.editItemPermission;

    return this;
  }
}

export class ImportBillItem {
  id: number;
  importBillId: number;
  suppliesId: number;
  count: number;
  price: number;
  total: number;
  note: string;
  files: string;
  supplies: Supplies;
}

export class ImportBillItemCommand {
  id: number;
  importBillId: number;
  suppliesId: number;
  count: number;
  price: number;
  total: number;
  note: string;
  files: string;
  supplies: Supplies;

  deserialize(input: ImportBillItem): ImportBillItemCommand {
    if (input == null) {
      input = new ImportBillItem();
    }
    this.id = input.id;
    this.importBillId = input.importBillId;
    this.suppliesId = input.suppliesId;
    this.count = input.count;
    this.price = input.price;
    this.total = input.total;
    this.note = input.note;
    this.files = input.files;
    this.supplies = input.supplies;

    return this;
  }
}