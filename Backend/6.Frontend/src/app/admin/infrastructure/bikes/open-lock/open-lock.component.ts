import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { BikesService } from '../bikes.service';
import { Dock } from '../../docks/docks.model';

@Component({
  selector: 'app-dock-open-lock-bike',
  templateUrl: './open-lock.component.html',
  providers: [
    BikesService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class OpenLockComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  noteOut: string;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<OpenLockComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: Dock,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public bikesService: BikesService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      dockId: [0],
      commandType: [0],
      note: [null, Validators.compose([Validators.maxLength(1000)])]
    });
    this.noteOut = ""
  }

  ngOnInit() {
    if (this.data.id > 0) {
      this.form.get('dockId').setValue(this.data.id);
      this.form.get('commandType').setValue(0);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.bikesService.openLock(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open("Thao tác thành công, kết quả xem ghi chú", this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.noteOut = "Ghi chú: " + res.data;
          }
          else if(res.status == 2) {
            this.snackBar.open("Khóa không có kết nối với hệ thống", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if(res.status == 3) {
            this.snackBar.open("Khóa chưa lấy dc tọa độ, vui lòng để khóa ra ngoài trời", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if(res.status == 4) {
            this.snackBar.open("Khóa đang ở trạng thái mở, không cần gửi lệnh mở nữa", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open("Không thấy phản hồi từ service khóa", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
  }
  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
