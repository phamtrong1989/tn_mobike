import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Bike, BikeCommand } from '../bikes.model';
import { BikesService } from '../bikes.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { BikeLogComponent } from '../log/bikes-log.component';
import { Dock } from '../../docks/docks.model';
import { FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-bikes-edit',
  templateUrl: './bikes-edit.component.html',
  providers: [
    BikesService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class BikesEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  docks: Dock[];
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, dockSearching: false };
  dockKey: FormControl = new FormControl("");
  constructor(
    public dialogRef: MatDialogRef<BikesEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: BikeCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public bikesService: BikesService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      serialNumber: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(50)])],
      status: [0],
      type: [0],
      description: [null, Validators.compose([Validators.maxLength(1000)])],
      colorId: [0],
      images: [null],
      investorId: [0],
      modelId: [0],
      plate: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(50)])],
      producerId: [0],
      projectId: [0],
      warehouseId: [0],
      stationId: [0],
      dockId: [0],
      dock: [null],
      note: [null, Validators.compose([Validators.maxLength(1000)])],
      isSetLocation: [false],
      startDate: [null],
      productionDate: [null],
      minuteRequiredMaintenance: [4800]
    });

    this.form.get('status').valueChanges.subscribe((data) => {
      if (data == 3 || data == 2) {
        this.form.get('stationId').setValue(0);
        this.form.get('stationId').disable();
      }
      else {
        this.form.get('stationId').enable();
      }
    });
  }

  ngOnInit() {
    this.initSearchDock();
    if (this.data.id > 0) {
      this.form.setValue(this.data);
      if (this.data.dock != null) {
        this.docks = [];
        if (this.data.dock != null) {
          this.docks.push(this.data.dock);
        }
      }
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.bikesService.getById(this.data.id).then(rs => {
      if (rs.data) {
        this.data = new BikeCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        if (this.data.dock != null) {
          this.docks = [];
          if (this.data.dock != null) {
            this.docks.push(this.data.dock);
          }
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  initSearchDock() {

    this.form.get('projectId').valueChanges.subscribe(rs => {
      this.docks = [];
      this.form.get('dockId').setValue(0);
    });

    this.dockKey.valueChanges.subscribe(rs => {
      this.isProcess.dockSearching = false;
      if (rs != "") {
        this.isProcess.dockSearching = true;
        this.bikesService.dockSearch({ key: rs, projectId: this.form.get('projectId').value, investorId: this.form.get('investorId').value }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('dockId').setValue(0);
          }
          this.docks = rs.data;
          this.isProcess.dockSearching = false;
        });
      }
    },
      error => {
        this.isProcess.dockSearching = false;
      });
  }

  matTabGroupChange(e: any, log: BikeLogComponent) {
    if (e.index == 1) {
      log.firsLoad();
    }
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.bikesService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else if (res.status == 2) {
            this.snackBar.open("Biển đã tồn tại vui lòng kiểm tra lại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 3) {
            this.snackBar.open("Số Serial tồn tại vui lòng kiểm tra lại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 4) {
            this.snackBar.open("Khóa bạn muốn đính vào xe đang trong chuyến đi thì không thể cập nhật được", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 5) {
            this.snackBar.open("Xe và khóa không thuộc cùng dự án không thể ghép cho nhau", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 6) {
            this.snackBar.open("Khóa lắp trên xe phải cùng đơn vị đầu tư", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 7) {
            this.snackBar.open("Khóa này đã được gắn trên một xe khác", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 8) {
            this.snackBar.open("Khóa muốn gán đang trong chuyến đi, vui lòng kiểm tra lại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.bikesService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new BikeCommand().deserialize(res.data);
            this.form.setValue(this.data);
            this.close(res)
          }
          else if (res.status == 1) {
            this.snackBar.open("Khóa đã được lắp xe khác rồi, vui lòng kiểm tra lại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 2) {
            this.snackBar.open("Biển đã tồn tại vui lòng kiểm tra lại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 3) {
            this.snackBar.open("Số Serial tồn tại vui lòng kiểm tra lại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 4) {
            this.snackBar.open("Xe đang trong chuyến đi, không thể cập nhật", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 5) {
            this.snackBar.open("Xe và khóa không thuộc cùng dự án không thể ghép cho nhau", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 6) {
            this.snackBar.open("Khóa lắp trên xe phải cùng đơn vị đầu tư", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 7) {
            this.snackBar.open("Khóa này đã được gắn trên một xe khác", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 8) {
            this.snackBar.open("Khóa muốn gán đang trong chuyến đi, vui lòng kiểm tra lại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.bikesService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 4) {
            this.snackBar.open("Xe đang trong chuyến đi, không thể cập nhật", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
