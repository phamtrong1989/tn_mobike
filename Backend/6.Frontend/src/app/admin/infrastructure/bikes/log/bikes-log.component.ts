import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../../app.settings';
import { Settings } from '../../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage';
import { LogAdmin } from 'src/app/admin/category/projects/projects.model';
import { BikesService } from '../bikes.service';
import { Bike } from '../bikes.model';

@Component({
  selector: 'app-bikes-log',
  templateUrl: './bikes-log.component.html',
  providers: [BikesService, AppCommon]
})

export class BikeLogComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  @Input() bike: Bike;
  heightTable: number = 513;
  listData: LogAdmin[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = {
    pageIndex: 1,
    pageSize: 20,
    sortby: "",
    sorttype: "",
    objectId: 0
  };

  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "actionAd", "createdDate", "systemUserId", "type", "action"];
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public bikesService: BikesService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  ngOnInit(): void {
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public firsLoad() {
    this.fixHeightTable();
    this.search();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.objectId = this.bike.id;
    this.bikesService.logSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }
  
  fixHeightTable() {

  }

  @HostListener('window:resize')
  public onWindowResize(): void {
  }
}