import { Component, OnInit, Inject, ElementRef, NgZone, ViewChild } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { PullData } from 'src/app/app.settings.model';
import { MapsAPILoader } from '@agm/core';
import { BikesService } from '../bikes.service';
import { Bike } from '../bikes.model';
import { stat } from 'fs';

@Component({
  selector: 'app-bikes-location-edit',
  templateUrl: './location-edit.component.html',
  providers: [
    BikesService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class BikesLocationComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: PullData;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  private geoCoder: any;
  selectedMarker: Marker;
  location: Location

  @ViewChild('search')
  public searchElementRef: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<BikesLocationComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: Bike,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public bikesService: BikesService,
    public appCommon: AppCommon,
    private translate: TranslateService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();

    this.form = this.fb.group({
      id: [0],
      lng: [0],
      lat: [0],
      stationId: [0]
    });
    this.form.get('id').setValue(this.data.id);
    this.form.get('stationId').setValue(this.data.stationId);
    this.form.get('lng').setValue(this.data.long);
    this.form.get('lat').setValue(this.data.lat);
    console.log("(" + this.data.lat + "," + this.data.long+")");
  }

  ngOnInit() {
    this.form.get('stationId').valueChanges.subscribe(rs=>{
      var station = this.categoryData.stations.find(x=>x.id == rs);
      if(station!=null)
      {
        if(station.lat == null || station.lat == 0 || station.lng == null || station.lng == 0)
        {
          this.snackBar.open("Trạm này tọa độ không xác định", this.baseTranslate["msg-warning-type"], { duration: 5000 });
          return;
        }

        this.location.latitude = station.lat;
        this.location.longitude = station.lng;
        this.form.get('lat').setValue(station.lat);
        this.form.get('lng').setValue(station.lng);
        if(this.location.markers.length == 0)
        {
          this.addMarker(station.lat,station.lng);
        }
        else
        {
          this.selectedMarker = this.location.markers[0];
          this.selectedMarker.lat = station.lat;
          this.selectedMarker.lng = station.lng;
        }
      }
    });
    this.initMap();
  }

  initMap() {
    this.location = {
      latitude: 10.912368,
      longitude: 106.7656889,
      mapType: "street view",
      zoom: 18,
      markers: []
    }

    if (this.data.id > 0 && this.data.lat != 0 && this.data.long != 0 && this.data.lat != null && this.data.long != null) {
      this.addMarker(this.data.lat, this.data.long)
    }

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.addMarkerSerch(place.geometry.location.lat(), place.geometry.location.lng());
        });
      });
    });
  }

  addMarkerSerch(lat: number, lng: number) {
    this.addMarker(lat, lng);
    this.location.latitude = lat;
    this.location.longitude = lng;
    this.form.get('lat').setValue(lat);
    this.form.get('lng').setValue(lng);
  }

  addMarker(lat: number, lng: number) {
    this.location.markers = [];
    this.location.markers.push({
      lat,
      lng
    });
    this.location.latitude = lat;
    this.location.longitude = lng;

    this.form.get('lat').setValue(lat);
    this.form.get('lng').setValue(lng);
  }

  selectMarker(event: any) {
    this.selectedMarker = {
      lat: event.latitude,
      lng: event.longitude
    };
    this.form.get('lat').setValue(event.latitude);
    this.form.get('lng').setValue(event.longitude);
  }

  markerDragEnd($event: any) {
    this.form.get('lat').setValue($event.coords.lat);
    this.form.get('lng').setValue($event.coords.lng);
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.bikesService.editLocation(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
          this.close(res)
        }
        else if (res.status == 2) {
          this.snackBar.open("Xe này chưa gắn khóa, thao tác thất bại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}

interface Marker {
  lat: number;
  lng: number;
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  markers: Marker[];
}