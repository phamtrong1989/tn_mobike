import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { BikesComponent } from './bikes.component';
import { BikesEditComponent } from './edit/bikes-edit.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { BikeLogComponent } from './log/bikes-log.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { BikesLocationComponent } from './location/location-edit.component';
import { AgmCoreModule } from '@agm/core';
import { BikesMapComponent } from './map/bikes-map.component';
import { OpenLockComponent } from './open-lock/open-lock.component';

export const routes = [
  { path: '', component: BikesComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    AgmCoreModule,
	 TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    }) 
  ],
  declarations: [
    BikesComponent,
    BikesEditComponent,
    BikeLogComponent,
    BikesLocationComponent,
    BikesMapComponent,
    OpenLockComponent
  ],
  entryComponents:[
    BikesEditComponent,
    BikeLogComponent,
    BikesLocationComponent,
    BikesMapComponent,
    OpenLockComponent
  ]
})
export class BikesModule { }
