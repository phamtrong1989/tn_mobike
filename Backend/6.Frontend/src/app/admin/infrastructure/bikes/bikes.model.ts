import { User } from "../../user/users/users.model";
import { Dock } from "../docks/docks.model";
import { Warehouse } from "../warehouses/warehouses.model";

export class Bike {

  id: number;
  serialNumber: string;
  status: number;
  type: number;
  description: string;
  createdDate: Date;
  updatedDate: Date;
  colorId: number;
  createdUser: number;
  images: string;
  investorId: number;
  modelId: number;
  plate: string;
  producerId: number;
  projectId: number;
  updatedUser: number;
  warehouseId: number;
  dockId: number;
  stationId: number;
  lat: number;
  long: number;
  createdUserObject: User;
  updatedUserObject: User;
  dock: Dock;
  warehouse: Warehouse;
  note: string;
  startDate: Date;
  productionDate: Date;
  minuteRequiredMaintenance: number;
}

export class BikeEditLocationCommand
{
  Id: number;
  stationId: number;
  lat: number;
  lng: number;
}

export class BikeCommand {

  id: number;
  serialNumber: string;
  status: number;
  type: number;
  description: string;
  colorId: number;
  images: string;
  investorId: number;
  modelId: number;
  plate: string;
  producerId: number;
  projectId: number;
  warehouseId: number;
  dockId: number;
  stationId: number;
  dock: Dock;
  note: string;
  isSetLocation: boolean;
  startDate: Date;
  productionDate: Date;
  minuteRequiredMaintenance: number;

  deserialize(input: Bike): BikeCommand {
    if (input == null) {
      input = new Bike();
    }
    this.id = input.id;
    this.serialNumber = input.serialNumber;
    this.status = input.status;
    this.type = input.type;
    this.description = input.description;
    this.colorId = input.colorId;
    this.images = input.images;
    this.investorId = input.investorId;
    this.modelId = input.modelId;
    this.plate = input.plate;
    this.producerId = input.producerId;
    this.projectId = input.projectId;
    this.warehouseId = input.warehouseId;
    this.dockId = input.dockId;
    this.stationId = input.stationId;
    this.dock = input.dock;
    this.note = input.note;
    this.startDate = input.startDate;
    this.productionDate = input.productionDate;
    this.minuteRequiredMaintenance = input.minuteRequiredMaintenance;
    this.isSetLocation = false;
    return this;
  }
}