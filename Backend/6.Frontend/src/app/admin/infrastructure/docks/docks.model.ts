import { User } from "../../user/users/users.model";
import { Bike } from "../bikes/bikes.model";

export class Dock {
  id: number;
  stationId: number;
  imei: string;
  serialNumber: string;
  status: number;
  lockStatus: boolean;
  battery: number;
  charging: boolean;
  connectionStatus: boolean;
  lastConnectionTime: Date;
  bookingStatus: number;
  lat: number;
  long: number;
  createdDate: Date;
  updatedDate: Date;
  sim: string;
  createdUser: number;
  investorId: number;
  projectId: number;
  updatedUser: number;
  modelId: number;
  producerId: number;
  warehouseId: number;
  createdUserObject: User;
  updatedUserObject: User;
  bike: Bike;
  note: string;
}

export class DockCommand {

  id: number;
  stationId: number;
  imei: string;
  serialNumber: string;
  status: number;
  sim: string;
  investorId: number;
  projectId: number;
  modelId: number;
  producerId: number;
  warehouseId: number;
  bike: Bike;
  note: string;

  deserialize(input: Dock): DockCommand {
    if (input == null) {
      input = new Dock();
    }
    this.id = input.id;
    this.stationId = input.stationId;
    this.imei = input.imei;
    this.serialNumber = input.serialNumber;
    this.status = input.status;
    this.sim = input.sim;
    this.investorId = input.investorId;
    this.projectId = input.projectId;
    this.modelId = input.modelId;
    this.producerId = input.producerId;
    this.warehouseId = input.warehouseId;
    this.bike = input.bike;
    this.note = input.note;
    return this;
  }
}

export class DockOpenManualCommand {
  dockId: number;
  note: string;
}