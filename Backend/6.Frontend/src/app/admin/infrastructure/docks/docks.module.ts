import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { DocksComponent } from './docks.component';
import { DocksEditComponent } from './edit/docks-edit.component';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DocksLogComponent } from './log/docks-log.component';
import { OpenLockComponent } from './open-lock/open-lock.component';
import { AgmCoreModule } from '@agm/core';

export const routes = [
  { path: '', component: DocksComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxMatSelectSearchModule,
    NgxMaterialTimepickerModule,
    AgmCoreModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    })
  ],
  declarations: [
    DocksComponent,
    DocksEditComponent,
    DocksLogComponent,
    OpenLockComponent
  ],
  entryComponents: [
    DocksEditComponent,
    DocksLogComponent,
    OpenLockComponent
  ],
  exports: [
    OpenLockComponent
  ]
})

export class DocksModule { }

export { OpenLockComponent };