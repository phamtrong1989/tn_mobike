import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Dock, DockCommand } from '../docks.model';
import { DocksService } from '../docks.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { Bike } from '../../bikes/bikes.model';
import { DocksLogComponent } from '../log/docks-log.component';

@Component({
  selector: 'app-docks-edit',
  templateUrl: './docks-edit.component.html',
  providers: [
    DocksService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class DocksEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, bikeSearching: false };
  bikeKey: FormControl = new FormControl("");
  constructor(
    public dialogRef: MatDialogRef<DocksEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: DockCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public docksService: DocksService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      stationId: [0],
      imei: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(50)])],
      sim: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(20)])],
      serialNumber: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(50)])],
      status: [0],
      investorId: [0],
      projectId: [0],
      modelId: [0],
      producerId: [0],
      warehouseId: [0],
      bike: [null],
      note: [null, Validators.compose([Validators.maxLength(1000)])]
    });

    
  }

  ngOnInit() {
   // this.initEventStatusChanger();
    if (this.data.id > 0) {
      this.form.setValue(this.data);
      this.initStatus();
      this.getData();
    }
  }

  initEventStatusChanger()
  {
    this.form.get('status').valueChanges.subscribe((data) => {
      if(this.data.bike == null)
      {
        if (data == 3 || data == 2) {
          this.form.get('stationId').setValue(0);
          this.form.get('stationId').disable();
        }
        else {
          this.form.get('stationId').enable();
        }
      }
      else
      {
        this.form.get('stationId').disable();
      }
    });
  }

  initStatus()
  {
      if(this.data.bike != null)
      {
        this.form.get('stationId').setValue(this.data.bike.stationId);
        this.form.get('status').setValue(this.data.bike.status);
        this.form.get('warehouseId').setValue(this.data.bike.warehouseId);
        this.form.get('stationId').disable();
        this.form.get('status').disable();
        this.form.get('warehouseId').disable();
      }
      else
      {
        this.form.get('stationId').enable();
        this.form.get('status').enable();
        this.form.get('warehouseId').enable();
      }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.docksService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new DockCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.initStatus();
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.docksService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else if (res.status == 2)
          {
            this.snackBar.open("IMEI đã tồn tại, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 3)
          {
            this.snackBar.open("Serial đã tồn tại, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 4)
          {
            this.snackBar.open("SIM đã tồn tại, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 5)
          {
            this.snackBar.open("Xe đính khóa này đang trang chuyến đi, không thể cập nhật được", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 6)
          {
            this.snackBar.open("Dự án của xe không giống với dự án khóa, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 7)
          {
            this.snackBar.open("Đơn vị đầu tư của xe không giống với dự án khóa, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.docksService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new DockCommand().deserialize(res.data);
            this.form.setValue(this.data);
          }
          else if (res.status == 2)
          {
            this.snackBar.open("IMEI đã tồn tại, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 3)
          {
            this.snackBar.open("Serial đã tồn tại, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 4)
          {
            this.snackBar.open("SIM đã tồn tại, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 5)
          {
            this.snackBar.open("Xe đính khóa này đang trang chuyến đi, không thể cập nhật được", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 6)
          {
            this.snackBar.open("Khóa này đã được đặt trên xe khác, vui lòng kiểm tra lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {

        this.isProcess.deleteProcess = true;
        this.docksService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 5)
          {
            this.snackBar.open("Xe đính khóa này đang trang chuyến đi, không thể cập nhật được", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
  matTabGroupChange(e: any, log:DocksLogComponent) {
    if(e.index == 1)
    {
      log.firsLoad();
    }
  }
}
