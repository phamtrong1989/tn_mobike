import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEvent, HttpRequest, HttpParams } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { Dock, DockCommand, DockOpenManualCommand } from './docks.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { Bike } from '../bikes/bikes.model';
import { LogAdmin } from '../../category/projects/projects.model';
import { Observable } from 'rxjs';

@Injectable()
export class DocksService {
    public url = "/api/work/Docks";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<Dock[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: DockCommand) {
        return this.http.post<ApiResponseData<Dock>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: DockCommand) {
        return this.http.put<ApiResponseData<Dock>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<Dock>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    delete(id: number) {
        return this.http.delete<ApiResponseData<Dock>>(this.url + "/delete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    bikeSearch(key: string) {
        return this.http.get<ApiResponseData<Bike[]>>(this.url + "/bikeSearch?key=" + key, this.appCommon.getHeaders({})).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    logSearchPaged(params: any) {
        return this.http.get<ApiResponseData<LogAdmin[]>>(this.url + "/LogSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    openLock(user: DockOpenManualCommand) {
        return this.http.post<ApiResponseData<Dock>>(this.url + "/openLock", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    export(params: any): Observable<HttpEvent<Blob>> {
        let obj = {};
        for (const prop in params) {
            if (params[prop] != null) {
                obj[prop] = params[prop];
            }
        }

        return this.http.request(new HttpRequest(
            'POST',
            this.url + "/report",
            null,
            {
                reportProgress: true,
                responseType: 'blob',
                params: new HttpParams({ fromObject: obj })
            }));
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}