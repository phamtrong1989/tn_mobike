import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../app.settings';
import { Settings, ApiResponseData } from '../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ExportBillsService } from './export-bills.service';
import { ExportBill, ExportBillCommand } from './export-bills.model';
import { ExportBillsEditComponent } from './edit/export-bills-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';
import { AccountData } from '../../account/account.model';
import { Warehouse } from '../../infrastructure/warehouses/warehouses.model';

@Component({
  selector: 'app-export-bills',
  templateUrl: './export-bills.component.html',
  providers: [ExportBillsService, AppCommon]
})

export class ExportBillsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: ExportBill[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, key: "", warehouseId: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "dateBill", "code","warehouseId", "toWarehouseId", "count", "total", "status", "completeStatus", "createdDate", "action"];
  public settings: Settings;
  public categoryData: any;
  isEdit: boolean = false;
  accountInfo: AccountData;
  warehousesT: Warehouse[];
  warehousesDF: Warehouse[];
  constructor(
    public appSettings: AppSettings,
    public ExportBillsService: ExportBillsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.accountInfo = this.appStorage.getAccountInfo();
    this.warehousesT = this.categoryData.warehouses.filter(x=>x.type == 0);
    this.warehousesDF = this.categoryData.warehouses.filter(x=>x.type == 1);
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.start = this.appCommon.formatDateTime(this.params.start, 'yyyy-MM-dd');
    this.params.end = this.appCommon.formatDateTime(this.params.end, 'yyyy-MM-dd');
    this.appCommon.changeUrl(location.pathname, this.params);
    this.ExportBillsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  initEditer(data:any) {
    // Điều phối
    if (this.accountInfo.roleType == 6) {
      if (data.status != 1 && data.status != 7) {
        return false;
      }
      else if(data.status == 7 && data.completeStatus != 0)
      {
        return false;
      }
    }
    // Kế toán VTS
    else if(this.accountInfo.roleType == 3) {
      if (data.status != 2 && data.status != 4) {
        return false
      }
    }
    // Thủ kho
    else if(this.accountInfo.roleType == 10) {
      if(data.status == 7 && data.completeStatus == 2)
      {
        return true;
      }
      else if (data.status != 3 && data.status != 6) {
        return false;
      }
    }
    // GĐ VTS
    else if(this.accountInfo.roleType == 2) {
      if (data.status != 4) {
          return false
      }
  }
    return true;
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: ExportBill) {
    var model = new ExportBillCommand();

    if (data == null) {
      model.id = 0;
    }
    else {
      model = new ExportBillCommand().deserialize(data);
    }

    this.dialog.open(ExportBillsEditComponent, {
      data: model,
      panelClass: 'dialog-1100',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}