import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../../app.settings';
import { Settings, ApiResponseData } from '../../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ExportBillItemsEditComponent } from './edit/export-bill-items-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage';
import { ExportBill, ExportBillItem, ExportBillItemCommand } from '../export-bills.model';
import { ExportBillsService } from '../export-bills.service';
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { AccountData } from 'src/app/admin/account/account.model';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-export-bill-items',
  templateUrl: './export-bill-items.component.html',
  providers: [ExportBillsService, AppCommon]
})

export class ExportBillItemsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  @Input() exportBill: ExportBill;
  @Input() changing: Subject<boolean>;
  heightTable: number = 220;
  listData: ExportBillItem[];
  isProcess = { dialogOpen: false, listDataTable: true, deleteProcess: false };
  params: any = { pageIndex: 1, pageSize: 20, exportBillId: 0 };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "suppliesId", "unit", "count", "price", "total", "action"];
  public settings: Settings;
  public categoryData: any;
  baseTranslate: any;
  isEdit: boolean = true;
  outData: any;
  accountData: AccountData;
  constructor(
    public appSettings: AppSettings,
    public exportBillsService: ExportBillsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    private translate: TranslateService
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.accountData = this.appStorage.getAccountInfo();
    this.initBaseTranslate();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.searchPage();
    this.changing.subscribe((rs) => { 
      this.isEdit = rs
    });
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.exportBillId = this.exportBill.id;
    this.exportBillsService.exportBillItemSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
      this.outData = res.outData;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: ExportBillItem) {
    var model = new ExportBillItemCommand();
    if (data == null) {
      model.id = 0;
      model.exportBillId = this.exportBill.id;
    }
    else {
      model = new ExportBillItemCommand().deserialize(data);
    }
    this.dialog.open(ExportBillItemsEditComponent, {
      data:  { exportBillItem: model, exportBill: this.exportBill },
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  exportBillItemDelete(data: ExportBillItem) {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.exportBillsService.exportBillItemDelete(data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-warning-type"], { duration: 5000 });
          }
          this.isProcess.deleteProcess = false;
          this.search();
        });
      }
    });
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
    }, 1);
  }
}