import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../../app.common'
import { AppStorage } from '../../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { ExportBillsService } from '../../export-bills.service';
import { ExportBill, ExportBillItemCommand } from '../../export-bills.model';
import { Supplies } from '../../../suppliess/suppliess.model';

@Component({
  selector: 'app-export-bill-items-edit',
  templateUrl: './export-bill-items-edit.component.html',
  providers: [
    ExportBillsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class ExportBillItemsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  suppliesKey: FormControl = new FormControl();
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, suppliesSearching: false };
  suppliess: Supplies[];
  supplies: Supplies;
  constructor(
    public dialogRef: MatDialogRef<ExportBillItemsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { exportBillItem: ExportBillItemCommand, exportBill: ExportBill },
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public exportBillsService: ExportBillsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      exportBillId: [0],
      suppliesId: [null, Validators.compose([Validators.required])],
      count: [1, Validators.compose([Validators.required])],
      price: [0, Validators.compose([Validators.required])],
      total: [0],
      note: [null],
      supplies: [null]
    });
    this.suppliess = [];
    this.form.get('exportBillId').setValue(this.data.exportBillItem.exportBillId);
  }

  ngOnInit() {
    this.suppliess = [];
    if (this.data.exportBillItem.supplies) {
      this.suppliess.push(this.data.exportBillItem.supplies);
    }

    this.initSearchSupplies();
    this.form.get('suppliesId').valueChanges.subscribe(() => {
      this.supplies = this.suppliess.find(x => x.id == this.form.get('suppliesId').value);
      if (this.supplies) {
        this.form.get('price').setValue(this.supplies.price);
        this.form.get('total').setValue(this.form.get('count').value * this.form.get('price').value);
      }
      else {
        this.supplies = null;
      }
    });

    this.form.get('count').valueChanges.subscribe(() => {
      this.form.get('total').setValue(this.form.get('count').value * this.form.get('price').value);
    });

    this.form.get('price').valueChanges.subscribe(() => {
      this.form.get('total').setValue(this.form.get('count').value * this.form.get('price').value);
    });

    if (this.data.exportBillItem.id > 0) {
      this.form.get('suppliesId').disable();
      this.form.setValue(this.data.exportBillItem);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.exportBillsService.exportBillItemGetById(this.data.exportBillItem.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data.exportBillItem = new ExportBillItemCommand().deserialize(rs.data);
        this.form.setValue(this.data.exportBillItem);
        this.suppliess = [];
        if (this.data.exportBillItem.supplies) {
          this.suppliess.push(this.data.exportBillItem.supplies);
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(rs.message, this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  initSearchSupplies() {
    this.suppliesKey.valueChanges.subscribe(rs => {
      this.isProcess.suppliesSearching = false;
      if (rs != "") {
        this.isProcess.suppliesSearching = true;
        this.exportBillsService.suppliesSearchKey({ key: rs, warehouseId: this.data.exportBill.warehouseId }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('suppliesId').setValue(null);
          }
          this.suppliess = rs.data;
          this.isProcess.suppliesSearching = false;
        });
      }
    },
      error => {
        this.isProcess.suppliesSearching = false;
      });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.exportBillItem.id > 0) {
        this.exportBillsService.exportBillItemEdit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.exportBillsService.exportBillItemCreate(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.close(res);
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.exportBillsService.exportBillItemDelete(this.data.exportBillItem.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });
      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
