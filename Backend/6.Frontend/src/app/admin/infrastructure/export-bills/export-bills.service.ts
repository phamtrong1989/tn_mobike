import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ExportBill, ExportBillCommand, ExportBillItem, ExportBillItemCommand } from './export-bills.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { Supplies } from '../suppliess/suppliess.model';

@Injectable()
export class ExportBillsService {
    public url = "/api/work/ExportBills";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<ExportBill[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: ExportBillCommand) {
        return this.http.post<ApiResponseData<ExportBill>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: ExportBillCommand) {
        return this.http.put<ApiResponseData<ExportBill>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    approve(user: any) {
        return this.http.put<ApiResponseData<ExportBill>>(this.url + "/approve", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    cancel(id: number) {
        return this.http.put<ApiResponseData<ExportBill>>(this.url + "/cancel/" + id, {}).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    
    getById(id: number) {
        return this.http.get<ApiResponseData<ExportBill>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    exportBillItemSearchPaged(params: any) {
        return this.http.get<ApiResponseData<ExportBillItem[]>>(this.url + "/ExportBillItemSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
   
    exportBillItemCreate(user: ExportBillItemCommand) {
        return this.http.post<ApiResponseData<ExportBillItem>>(this.url + "/exportBillItemCreate", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    exportBillItemEdit(user: ExportBillItemCommand) {
        return this.http.put<ApiResponseData<ExportBillItem>>(this.url + "/exportBillItemEdit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    exportBillItemGetById(id: number) {
        return this.http.get<ApiResponseData<ExportBillItem>>(this.url + "/exportBillItemGetById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    exportBillItemDelete(id: number) {
        return this.http.delete<ApiResponseData<ExportBillItem>>(this.url + "/exportBillItemDelete/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    suppliesSearchKey(params: any) {
        return this.http.get<ApiResponseData<Supplies[]>>(this.url + "/SuppliesSearchKey", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    completeApprove(id: number, completeStatus: number) {
        return this.http.put<ApiResponseData<ExportBill>>(this.url + "/completeApprove/"+id+"/"+completeStatus, {}).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    refuse(id: number) {
        return this.http.put<ApiResponseData<ExportBill>>(this.url + "/Refuse/" + id, {}).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }

    uploadPath(): string {
        return this.url + "/upload"
    }

    viewPath(): string
    {
        return this.url + "/view"
    }
}