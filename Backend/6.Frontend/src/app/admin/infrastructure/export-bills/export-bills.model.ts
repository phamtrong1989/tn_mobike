import { User } from "../../user/users/users.model";
import { Supplies } from "../suppliess/suppliess.model";

export class ExportBill {
  id: number;
  code: string;
  dateBill: Date;
  total: number;
  warehouseId: number;
  note: string;
  files: string;
  status: number;
  tK2Confirm: Date;
  tK2User: number;
  tKConfirm: Date;
  tKUser: number;
  bODVTSConfirm: Date;
  bODVTSUser: number;
  completeConfirm: Date;
  completeUser: number;
  createdDate: Date;
  createdUser: number;
  kT2Confirm: Date;
  kT2User: number;
  kTConfirm: Date;
  kTUser: number;
  nVDPConfirm: Date;
  nVDPUser: number;
  toWarehouseId: number;
  createdUserObject: User;
  completeStatus: number;
}

export class ExportBillCommand {
  id: number;
  code: string;
  dateBill: Date;
  warehouseId: number;
  toWarehouseId: number;
  note: string;
  files: string;
  status: number;
  completeStatus: number;

  deserialize(input: ExportBill): ExportBillCommand {
    if (input == null) {
      input = new ExportBill();
    }
    this.id = input.id;
    this.code = input.code;
    this.dateBill = input.dateBill;
    this.warehouseId = input.warehouseId;
    this.note = input.note;
    this.files = input.files;
    this.status = input.status;
    this.toWarehouseId = input.toWarehouseId;
    this.completeStatus = input.completeStatus;
    return this;
  }
}

export class ExportBillItem {

  id: number;
  exportBillId: number;
  suppliesId: number;
  count: number;
  price: number;
  total: number;
  note: string;
  files: string;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  supplies: Supplies;
}

export class ExportBillItemCommand {

  id: number;
  exportBillId: number;
  suppliesId: number;
  count: number;
  price: number;
  total: number;
  note: string;
  supplies: Supplies;

  deserialize(input: ExportBillItem): ExportBillItemCommand {
    if (input == null) {
      input = new ExportBillItem();
    }
    this.id = input.id;
    this.exportBillId = input.exportBillId;
    this.suppliesId = input.suppliesId;
    this.count = input.count;
    this.price = input.price;
    this.total = input.total;
    this.note = input.note;
    this.supplies = input.supplies;
    return this;
  }
}