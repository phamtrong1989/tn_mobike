import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { ExportBillsComponent } from './export-bills.component';
import { ExportBillsEditComponent } from './edit/export-bills-edit.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { NgxCurrencyModule } from 'ngx-currency';
import { ExportBillItemsComponent } from './export-bill-items/export-bill-items.component';
import { ExportBillItemsEditComponent } from './export-bill-items/edit/export-bill-items-edit.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AgmCoreModule } from '@agm/core';

export const routes = [
  { path: '', component: ExportBillsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    AgmCoreModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    }),
    NgxCurrencyModule
  ],
  declarations: [
    ExportBillsComponent,
    ExportBillsEditComponent,
    ExportBillItemsComponent,
    ExportBillItemsEditComponent
  ],
  entryComponents: [
    ExportBillsEditComponent,
    ExportBillItemsComponent,
    ExportBillItemsEditComponent
  ]
})
export class ExportBillsModule { }