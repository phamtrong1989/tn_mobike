import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExportBill, ExportBillCommand } from '../export-bills.model';
import { ExportBillsService } from '../export-bills.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { FileDataDTO } from 'src/app/app.settings.model';
import { AccountData } from 'src/app/admin/account/account.model';
import { of, Subject } from 'rxjs';
import { ExportBillItemsComponent } from '../export-bill-items/export-bill-items.component';
import { Warehouse } from 'src/app/admin/infrastructure/warehouses/warehouses.model';

@Component({
  selector: 'app-export-bills-edit',
  templateUrl: './export-bills-edit.component.html',
  providers: [
    ExportBillsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ExportBillsEditComponent implements OnInit {
  @ViewChild('exportBillItems') exportBillItems:ExportBillItemsComponent;
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  public pathUpload: string;
  uploadFileOut: FileDataDTO[];
  baseTranslate: any;
  accountInfo: AccountData;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, cancelProcess: false, approveProcess : false };
  isSaveData: boolean = true;
  changingValue: Subject<boolean> = new Subject();
  warehousesT: Warehouse[];
  warehousesDF: Warehouse[];

  constructor(
    public dialogRef: MatDialogRef<ExportBillsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ExportBillCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public exportBillsService: ExportBillsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.accountInfo = this.appStorage.getAccountInfo();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      warehouseId: [null, Validators.compose([Validators.required])],
      toWarehouseId: [null, Validators.compose([Validators.required])],
      status: [0],
      code: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(50)])],
      dateBill: [new Date(), Validators.compose([Validators.required])],
      files: [null],
      note: [null, Validators.compose([Validators.maxLength(2000)])],
      completeStatus: [0]
    });

    this.warehousesT = this.categoryData.warehouses.filter(x=>x.type == 0);
    if(this.data.id > 0)
    {
      this.warehousesDF = this.categoryData.warehouses.filter(x=>x.type == 1);
    }
    else
    {
      this.warehousesDF = this.categoryData.warehouses.filter(x=>x.type == 1 && x.employeeId == this.accountInfo.id);
      if(this.warehousesDF.length > 0)
      {
        this.form.get('toWarehouseId').setValue(this.warehousesDF[0].id);
        this.form.get('toWarehouseId').disable();
      }
      else
      {
        this.form.get('toWarehouseId').setValue(null);
      }
    }
  }

  ngOnInit() {
    if (this.data.id > 0) {
      if (this.data.files) {
        this.uploadFileOut = JSON.parse(this.data.files)
      }
      this.initEditer();
      this.getData();
    }
  }

  initEditer() {
    this.form.get('toWarehouseId').disable();
    this.changingValue.next(true);
    if (this.accountInfo.roleType == 6) {
      if (this.data.status != 1) {
        this.form.disable();
        this.isSaveData = false;
        this.changingValue.next(false);
      }
    }
    else if(this.accountInfo.roleType == 3) {
      if (this.data.status != 2 && this.data.status != 4) {
        this.form.disable();
        this.isSaveData = false;
        this.changingValue.next(false);
      }
    }
    else if(this.accountInfo.roleType == 10) {
      if (this.data.status != 3 && this.data.status != 6) {
        this.form.disable();
        this.isSaveData = false;
        this.changingValue.next(false);
      }
    }
    else if(this.accountInfo.roleType == 2) {
      if (this.data.status != 4) {
        this.form.disable();
        this.isSaveData = false;
        this.changingValue.next(false);
      }
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.exportBillsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new ExportBillCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        if (this.data.files) {
          this.uploadFileOut = JSON.parse(this.data.files)
        }
        this.initEditer();
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  approve(status: number) {
    this.isProcess.approveProcess = true;
    this.exportBillsService.approve({ id: this.data.id, status: status }).then((res) => {
      if (res.status == 1) {
        this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
        this.data = new ExportBillCommand().deserialize(res.data);
        this.form.setValue(this.data);
      }
      else {
        this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
      }
      this.initEditer();
      this.isProcess.approveProcess = false;
    });
  }

  refuse() {
    this.isProcess.cancelProcess = true;
    this.exportBillsService.refuse(this.data.id).then((res) => {
      if (res.status == 1) {
        this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
      }
      else {
        this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
      }
      this.close(1);
      this.isProcess.cancelProcess = false;
    });
  }

  completeApprove(completeStatus: number) {
    this.isProcess.approveProcess = true;
    this.exportBillsService.completeApprove(this.data.id, completeStatus).then((res) => {
      if (res.status == 1) {
        this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
      }
      else {
        this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
      }
      this.close();
      this.isProcess.approveProcess = false;
    });
  }

  save() {
    if (this.form.valid) {
      this.isProcess.saveProcess = true;
      this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
      if (this.data.id > 0) {
        this.exportBillsService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.data = new ExportBillCommand().deserialize(res.data);
            this.form.setValue(this.data);
          }
          else {
            this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.form.get('toWarehouseId').enable();
        setTimeout(() => {
          this.exportBillsService.create(this.form.value).then((res) => {
            if (res.status == 1) {
              this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
              this.data = new ExportBillCommand().deserialize(res.data);
              this.form.setValue(this.data);
              this.form.get('toWarehouseId').disable();
            }
            else {
              this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
            }
            this.isProcess.saveProcess = false;
          });
        }, 200);
      }
    }
  }

  cancel() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-cancel-bill-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.exportBillsService.cancel(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(res.message, this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(res.message, this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  refund() {
    let ExportBill = this.data;
    ExportBill.status = 9; // Xác nhận sai lệch trong xuất - nhận hàng. Đề nghị hoàn trả lại kho
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-refund-bill-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.exportBillsService.edit(ExportBill).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-refund-bill-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });

      }
    });
  }

  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
