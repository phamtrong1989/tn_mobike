import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { TroubleShootingService } from '../troubleshooting-center.services';

@Component({
    selector: 'app-chart-age-rent',
    templateUrl: './chart-age-rent.component.html',
    providers: [TroubleShootingService, AppCommon]
})

export class ChartAgeRentComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    typeChart: string = "ageRent";
    intervalGetData: any;


    // options config chart
    view: any[] = [1500, 400];
    gradient: boolean = true;
    showLegend: boolean = true;
    showLabels: boolean = true;
    isDoughnut: boolean = false;
    label: string = "Tổng số phút thuê";
    percentageFormatting(value) {
        return Math.round(value * 100) / 100;
    }

    colorScheme = {
        domain: ['#155799', '#ef32d9', '#cb2d3e', '#ffd200', '#159957', '#95726c']
    };
    results: any = [
        { "name": "14 - 18", "value": 0 },
        { "name": "18 - 22", "value": 0 },
        { "name": "22 - 40", "value": 0 },
        { "name": "40 - 60", "value": 0 },
        { "name": "Trên 60", "value": 0 },
        { "name": "Chưa rõ", "value": 0 }]

    constructor(
        public troubleShootingService: TroubleShootingService,
        public appCommon: AppCommon
    ) { }

    ngOnChanges() {
        clearInterval(this.intervalGetData);
        this.getData();
        this.setInterval();
    }

    getData(): void {
        var params = {
            projectId: this.projectId,
            typeChart: this.typeChart,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };

        this.troubleShootingService.GetPieChartData(params).then((res) => {
            this.results = res;
        });
    }

    onSelect(data: any): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data: any): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data: any): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }

    setInterval() {
        this.intervalGetData = setInterval(() => this.getData(), 10000);
    }

    ngOnDestroy(): void {
        if (this.intervalGetData) {
            clearInterval(this.intervalGetData);
        }
    }
}