import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { TroubleShootingService } from '../troubleshooting-center.services';

@Component({
    selector: 'app-stacked-area-chart',
    templateUrl: './stacked-area-chart.component.html',
    providers: [TroubleShootingService, AppCommon]
})
export class StackedAreaChartComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    @Input() typeChart: string;
    intervalGetData: any;

    // config chart
    view: any[] = [1500, 400];
    legend: boolean = true;
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'Year';
    yAxisLabel: string = 'Population';
    timeline: boolean = true;
    colorScheme = {
        domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
    };
    results: any = [
        {
            "name": "Germany",
            "series": [
                {
                    "name": "1990",
                    "value": 62000000
                },
                {
                    "name": "2010",
                    "value": 73000000
                },
                {
                    "name": "2011",
                    "value": 89400000
                }
            ]
        },

        {
            "name": "USA",
            "series": [
                {
                    "name": "1990",
                    "value": 250000000
                },
                {
                    "name": "2010",
                    "value": 309000000
                },
                {
                    "name": "2011",
                    "value": 311000000
                }
            ]
        },

        {
            "name": "France",
            "series": [
                {
                    "name": "1990",
                    "value": 58000000
                },
                {
                    "name": "2010",
                    "value": 50000020
                },
                {
                    "name": "2011",
                    "value": 58000000
                }
            ]
        },
        {
            "name": "UK",
            "series": [
                {
                    "name": "1990",
                    "value": 57000000
                },
                {
                    "name": "2010",
                    "value": 62000000
                }
            ]
        }
    ];

    constructor(
        public troubleShootingService: TroubleShootingService,
        public appCommon: AppCommon
    ) {
    }

    ngOnChanges() {
        clearInterval(this.intervalGetData);
        this.getData();
        this.setInterval();
    }

    getData(): void {
        var params = {
            projectId: this.projectId,
            typeChart: this.typeChart,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };

        this.troubleShootingService.GetPieChartData(params).then((res) => {
            this.results = res;
        });
    }

    setInterval() {
        this.intervalGetData = setInterval(() => this.getData(), 10000);
    }

    ngOnDestroy(): void {
        if (this.intervalGetData) {
            clearInterval(this.intervalGetData);
        }
    }

    onSelect(event) {
        console.log(event);
    }
}