import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { HttpLoaderFactory } from 'src/app/app.module';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { SharedModule } from '../../shared/shared.module';

import { TroubleshootingCenterComponent } from './troubleshooting-center.component';
import { ChartLevelRechargeComponent } from './chart-level-recharge/chart-level-recharge.component';
import { ChartAgeRechargeComponent } from './chart-age-recharge/chart-age-recharge.component';
import { ChartTimeRechargeComponent } from './chart-time-recharge/chart-time-recharge.component';
import { ChartPayGateComponent } from './chart-paygate/chart-paygate.component';
import { ChartAgeRentComponent } from './chart-age-rent/chart-age-rent.component';
import { ChartTimeRentComponent } from './chart-time-rent/chart-time-rent.component';
import { StackedAreaChartComponent } from './stacked-area-chart/stacked-area-chart.component';

export const routes = [
  { path: '', component: TroubleshootingCenterComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    PerfectScrollbarModule,
    SharedModule,
    PipesModule,
    AgmCoreModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    })
  ],
  declarations: [
    TroubleshootingCenterComponent,
    ChartLevelRechargeComponent,
    ChartAgeRechargeComponent,
    ChartTimeRechargeComponent,
    ChartPayGateComponent,
    ChartAgeRentComponent,
    ChartTimeRentComponent,
    StackedAreaChartComponent
  ],
  entryComponents: [
  ]
})

export class TroubleshootingCenterModule { }
