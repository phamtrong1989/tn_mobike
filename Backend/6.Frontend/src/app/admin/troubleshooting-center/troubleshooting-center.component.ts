import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { TroubleShootingService } from './troubleshooting-center.services';

@Component({
  selector: 'app-troubleshooting-center',
  templateUrl: './troubleshooting-center.component.html',
  styleUrls: ['./troubleshooting-center.component.scss'],
  providers: [TroubleShootingService, AppCommon]
})

export class TroubleshootingCenterComponent implements OnInit {
  ctrlProjectId = new FormControl();
  fromDate: Date = new Date(new Date().getFullYear(), 0, 1);
  toDate: Date = new Date();

  categoryData: any;
  intervalGetData: any;
  public boxInfo: any = { item1: 0, item2: 0, item3: 0, item4: 0, item5: 0 };

  constructor(
    public appCommon: AppCommon,
    public appStorage: AppStorage,
    public troubleShootingService: TroubleShootingService,
  ) {
    this.categoryData = appStorage.getCategoryData();

    if (this.appStorage.getProjectId() == null || this.appStorage.getProjectId() == undefined) {
      this.appStorage.setProjectId(appStorage.getCategoryData().projects[0].id);
    }

    this.ctrlProjectId.setValue(this.appStorage.getProjectId());
    this.ctrlProjectId.valueChanges.subscribe(res => {
      this.appStorage.setProjectId(res);
    });
  }

  ngOnInit(): void {
    clearInterval(this.intervalGetData);
    this.getData();
    this.setInterval();
  }

  getData() {
    this.troubleShootingService.GetInfoBoxData().then((res) => {
      this.boxInfo = res;
    });
  }

  setInterval() {
    this.intervalGetData = setInterval(() => this.getData(), 10000);
}

  getToday() {
    return new Date();
  }

  getMonday() {
    var d = new Date();
    var first = d.getDate() - d.getDay() + 1;
    return new Date(d.setDate(first));
  }

  getFirstDayOfMonth() {
    return new Date(new Date().getFullYear(), new Date().getMonth(), 1)
  }

  getFirstDayOfQuarter() {
    var currentMonth = new Date().getMonth();
    var currentQuarter = Math.ceil(currentMonth / 3);
    var firstMonthOfQuarter = currentQuarter * 3 - 3;
    return new Date(new Date().getFullYear(), firstMonthOfQuarter, 1)
  }

  getFirstDayOfYear() {
    return new Date(new Date().getFullYear(), 0, 1)
  }
}