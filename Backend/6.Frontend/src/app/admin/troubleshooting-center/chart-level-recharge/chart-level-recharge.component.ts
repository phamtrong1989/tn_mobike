import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { TroubleShootingService } from '../troubleshooting-center.services';

@Component({
    selector: 'app-chart-level-recharge',
    templateUrl: './chart-level-recharge.component.html',
    providers: [TroubleShootingService, AppCommon]
})

export class ChartLevelRechargeComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    typeChart: string = "levelRecharge";
    intervalGetData: any;


    // options config chart
    view: any[] = [1500, 400];
    gradient: boolean = true;
    showLegend: boolean = true;
    showLabels: boolean = true;
    isDoughnut: boolean = false;
    label: string = "Giao dịch nạp tiền";

    colorScheme = {
        domain: ['#155799', '#5AA454', '#ffd200', '#A10A28']
    };
    results: any = [{ "name": "< 100k", "value": 0 }, { "name": "100k - 200k", "value": 0 }, { "name": "200k - 500k", "value": 0 }, { "name": "> 500k", "value": 0 }]
    percentageFormatting(value) {
        return Math.round(value * 100) / 100;
    }
    constructor(
        public troubleShootingService: TroubleShootingService,
        public appCommon: AppCommon
    ) { }

    ngOnChanges() {
        clearInterval(this.intervalGetData);
        this.getData();
        this.setInterval();
    }

    getData(): void {
        var params = {
            projectId: this.projectId,
            typeChart: this.typeChart,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };

        this.troubleShootingService.GetPieChartData(params).then((res) => {
            this.results = res;
        });
    }

    onSelect(data: any): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data: any): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data: any): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }

    setInterval() {
        this.intervalGetData = setInterval(() => this.getData(), 10000);
    }

    ngOnDestroy(): void {
        if (this.intervalGetData) {
            clearInterval(this.intervalGetData);
        }
    }
}