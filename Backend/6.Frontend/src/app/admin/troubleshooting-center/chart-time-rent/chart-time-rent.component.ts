import { Component, Input, OnChanges } from '@angular/core';
import { AppCommon } from 'src/app/app.common';
import { TroubleShootingService } from '../troubleshooting-center.services';

@Component({
    selector: 'app-chart-time-rent',
    templateUrl: './chart-time-rent.component.html',
    providers: [TroubleShootingService, AppCommon]
})

export class ChartTimeRentComponent implements OnChanges {
    @Input() projectId: string;
    @Input() fromDate: Date;
    @Input() toDate: Date;
    typeChart: string = "timeRent";
    intervalGetData: any;

    // options config chart
    view: any[] = [1500, 400];
    gradient: boolean = true;
    showLegend: boolean = true;
    showLabels: boolean = true;
    isDoughnut: boolean = false;
    label: string = "Tổng số chuyến đi";
    percentageFormatting(value) {
        return Math.round(value * 100) / 100;
    }

    colorScheme = {
        domain: ['#155799', '#A10A28', '#cb2d3e', '#d76b26', '#C7B42C', '#ffd200', '#159957', '#5AA454']
    };
    results: any = [
        { "name": "0H : 3H", "value": 0 },
        { "name": "3H : 6H", "value": 0 },
        { "name": "6H : 9H", "value": 0 },
        { "name": "9H : 12H", "value": 0 },
        { "name": "12H : 15H", "value": 0 },
        { "name": "15H : 18H", "value": 0 },
        { "name": "18H : 21H", "value": 0 },
        { "name": "21H : 24H", "value": 0 },
    ]

    constructor(
        public troubleShootingService: TroubleShootingService,
        public appCommon: AppCommon
    ) { }

    ngOnChanges() {
        clearInterval(this.intervalGetData);
        this.getData();
        this.setInterval();
    }

    getData(): void {
        var params = {
            projectId: this.projectId,
            typeChart: this.typeChart,
            fromDate: this.appCommon.formatDateTime(this.fromDate, "yyyy-MM-dd"),
            toDate: this.appCommon.formatDateTime(this.toDate, "yyyy-MM-dd")
        };

        this.troubleShootingService.GetPieChartData(params).then((res) => {
            this.results = res;
        });
    }

    onSelect(data: any): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data: any): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data: any): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }

    setInterval() {
        this.intervalGetData = setInterval(() => this.getData(), 10000);
    }

    ngOnDestroy(): void {
        if (this.intervalGetData) {
            clearInterval(this.intervalGetData);
        }
    }
}