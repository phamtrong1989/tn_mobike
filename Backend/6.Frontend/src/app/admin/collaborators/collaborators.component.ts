import { Component, OnInit, ViewEncapsulation, ViewChild, HostListener, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppCommon } from 'src/app/app.common';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';
import { AppStorage } from 'src/app/app.storage';
import { Role } from '../user/roles/roles.model';
import { ManagerRegisterCommand, User } from '../user/users/users.model';
import { CollaboratorsService } from './collaborators.service';
import { CollaboratorsCreateComponent } from './create/collaborators-create.component';
import { CollaboratorsEditComponent } from './edit/collaborators-edit.component';
import { CollaboratorsReportComponent } from './report/collaborators-report.component';

@Component({
  selector: 'app-collaborators',
  templateUrl: './collaborators.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [CollaboratorsService, AppCommon]
})
export class CollaboratorsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  heightTable: number;
  listData: User[];
  roles: Role[];
  public appStogate: any;
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, key: "", type: "", sortby: "", sorttype: "", roleId: null, isLock: null };
  loadingIndicator: boolean = true;
  displayedColumns = ['id', 'userName', 'email', 'fullName', 'identificationID', 'isRetired', 'roles', 'isLock', 'createdDate', 'updatedDate', 'action'];
  public settings: Settings;
  constructor(
    public appSettings: AppSettings,
    public collaboratorsService: CollaboratorsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;

  }

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  ngOnInit() {

    this.roles = this.appStorage.getCategoryData().roles;
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.searchPage();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.heightTable = this.appCommon.initHeightTable("scroll_table", 75);
    }, 1);
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.appCommon.changeUrl(location.pathname, this.params);
    this.collaboratorsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.paginator.pageSize = this.params.pageSize;
      this.isProcess.listDataTable = false;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openCreateDialog() {

    this.dialog.open(CollaboratorsCreateComponent, {
      data: null,
      disableClose: true,
      panelClass: 'dialog-default',
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  public openEditDialog(data: User) {
    this.dialog.open(CollaboratorsEditComponent, {
      data: new ManagerRegisterCommand().deserialize(data),
      disableClose: true,
      panelClass: 'dialog-default',
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  public openReportDialog() {
    this.dialog.open(CollaboratorsReportComponent, {
      disableClose: true,
      panelClass: 'dialog-default',
      autoFocus: false
    }).afterClosed().subscribe(outData => {
    });
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.heightTable = this.appCommon.initHeightTable("scroll_table", 75)
  }
}