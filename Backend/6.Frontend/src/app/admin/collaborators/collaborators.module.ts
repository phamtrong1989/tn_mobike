import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { CollaboratorsCreateComponent } from './create/collaborators-create.component';
import { CollaboratorsEditComponent } from './edit/collaborators-edit.component';
import { CollaboratorsComponent } from './collaborators.component';
import { CollaboratorsReportComponent } from './report/collaborators-report.component';

export const routes = [
  { path: '', component: CollaboratorsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    })
  ],
  declarations: [
    CollaboratorsComponent,
    CollaboratorsEditComponent,
    CollaboratorsCreateComponent,
    CollaboratorsReportComponent
  ],
  entryComponents: [
    CollaboratorsEditComponent,
    CollaboratorsCreateComponent,
    CollaboratorsReportComponent
  ]
})
export class CollaboratorsModule { }
