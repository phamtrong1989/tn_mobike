import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomValidator } from 'src/app/theme/utils/app-validators'
import { ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { CollaboratorsService } from '../collaborators.service';
import { AppCommon } from 'src/app/app.common';
import { ManagerRegisterCommand, User } from '../../user/users/users.model';
import { AppStorage } from 'src/app/app.storage';
@Component({
  selector: 'app-collaborators-create',
  templateUrl: './collaborators-create.component.html',
  providers: [
    CollaboratorsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class CollaboratorsCreateComponent implements OnInit {
  public form: FormGroup;

  public formCalendars: FormGroup;
  public appStogate: any;
  roles = [];
  isProcess = { saveProcess: false, userSearching: false };
  public passwordHide: boolean = true;
  userKey: FormControl = new FormControl("");
  dateTest: String;
  users: User[];
  public clientSettings: ClientSettings;
  constructor(
    public dialogRef: MatDialogRef<CollaboratorsCreateComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ManagerRegisterCommand,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public collaboratorsService: CollaboratorsService,
    appClientSettings: AppClientSettings
  ) {
    this.appStogate = this.appStorage.getCategoryData();
    this.roles = this.appStogate.roles;
    this.clientSettings = appClientSettings.settings
    this.form = this.fb.group({
      id: [0],
      username: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(15)])],
      email: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(10), Validators.maxLength(50)])],
      roleId: [1],
      password: [{ value: 'Matkhau@91234', disabled: true }, Validators.compose([Validators.maxLength(12), CustomValidator.passwordStrongValidator])],
      phoneNumber: [null, Validators.compose([Validators.maxLength(20)])],
      displayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      isLock: [false],
      isChangePassword: [false],
      identificationID: [null, Validators.compose([Validators.maxLength(50)])],
      identificationSupplyDate: [null],
      identificationSupplyAdress: [null, Validators.compose([Validators.maxLength(200)])],
      identificationAdress: [null, Validators.compose([Validators.maxLength(200)])],
      identificationPhotoBackside: [null, Validators.compose([Validators.maxLength(500)])],
      identificationPhotoFront: [null, Validators.compose([Validators.maxLength(500)])],
      sex: [2, Validators.compose([Validators.required])],
      fullName: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100)])],
      birthday: [null, Validators.compose([Validators.required])],
      address: [null, Validators.compose([Validators.maxLength(200)])],
      education: [null, Validators.compose([Validators.maxLength(200)])],
      isRetired: [false],
      workingDateFrom: [null],
      workingDateTo: [null],
      managerUserId: [69],
      roleType: [5],
      managerUser: [null],
      code: [null]
    });

    this.form.get('isChangePassword').valueChanges.subscribe(rs => {
      var isX = this.form.get('isChangePassword').value;
      if (isX == true) {
        this.form.get('password').setValue('');
        this.form.get('password').enable();
      }
      else {
        this.form.get('password').setValue('Matkhau@9');
        this.form.get('password').disable();
      }
    });
  }

  ngOnInit() {
    this.initSearchUser();
    this.data = new ManagerRegisterCommand();
    this.data.isChangePassword = false;
  }

  initSearchUser() {
    this.userKey.valueChanges.subscribe(rs => {
      this.isProcess.userSearching = false;
      if (rs != "") {
        this.isProcess.userSearching = true;
        this.collaboratorsService.searchByUser({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('managerUserId').setValue(null);
          }
          this.users = rs.data;
          this.isProcess.userSearching = false;
        });
      }
    },
      error => {
        this.isProcess.userSearching = false;
      });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.collaboratorsService.create(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open("Tạo mới tài khoản thành công", "success", { duration: 5000 });
          this.close(res)
        }
        else if (res.status == 2) {
          this.snackBar.open("Tài khoản này đã có người sử dụng, vui lòng thử lại", "danger", { duration: 5000 });
        }
        else if (res.status == 3) {
          this.snackBar.open("Email này đã có người sử dụng, vui lòng thử lại", "danger", { duration: 5000 });
        }
        else {
          this.snackBar.open("Có lỗi hoặc phiên làm việc đã kết thúc, vui lòng F5 refesh lại trình duyệt và thử lại", "danger", { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  showManager() {
    try {
      if (this.form.get('roleId').value == 0) {
        this.form.get("managerUserId").disable();
        return false;
      }
      else {
        var thisData = this.roles.find(x => x.id == this.form.get('roleId').value);
        if (thisData == null) {
          this.form.get("managerUserId").disable();
          return false;
        }

        if (thisData.type == 4) {
          this.form.get("managerUserId").enable();
          return true;
        }
        else {
          this.form.get("managerUserId").disable();
          return false;
        }
      }
    }
    catch
    {
      this.form.get("managerUserId").disable();
      return false;
    }
  }

  getFullUrl(url: string) {
    return this.clientSettings.serverAPI + url;
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
  resetInputPassword() {
    this.form.get('password').setValue('');
  }

  onGetRequired() {
    const invalid = [];
    const controls = this.form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    console.log(invalid);
    return invalid;
  }
}
