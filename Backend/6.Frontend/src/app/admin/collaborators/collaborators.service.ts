import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEvent, HttpRequest, HttpParams } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ApiResponseData, ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AppCommon } from 'src/app/app.common';
import { ManagerRegisterCommand, User } from '../user/users/users.model';
import { Observable } from 'rxjs';
@Injectable()
export class CollaboratorsService {

    public url = "/api/Work/Collaborators";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    create(user: ManagerRegisterCommand) {
        return this.http.post<ApiResponseData<ManagerRegisterCommand>>(this.url + "/create", user).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    edit(user: ManagerRegisterCommand) {
        return this.http.put<ApiResponseData<ManagerRegisterCommand>>(this.url + "/edit", user).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
    getById(id: number) {
        return this.http.get<ApiResponseData<User>>(this.url + "/getById/" + id).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
    delete(id: number) {
        return this.http.delete<ApiResponseData<User>>(this.url + "/delete/" + id).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    searchByUser(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchByUser", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    uploadImage() {
        return this.url + "/uploadImage";
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }

    export(params: any): Observable<HttpEvent<Blob>> {
        let obj = {};
        for (const prop in params) {
            if (params[prop] != null) {
                obj[prop] = params[prop];
            }
        }

        return this.http.request(new HttpRequest(
            'POST',
            this.url + "/report",
            null,
            {
                reportProgress: true,
                responseType: 'blob',
                params: new HttpParams({ fromObject: obj })
            }
        ));
    }
} 