import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { CollaboratorsService } from '../collaborators.service';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-collaborators-report',
  templateUrl: './collaborators-report.component.html',
  providers: [
    CollaboratorsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class CollaboratorsReportComponent {
  public clientSettings: ClientSettings;
  isProcess = { saveProcess: false, export: false };
  params: any = {
    type: "all",
    from: new Date(),
    to: new Date(),
  };

  constructor(
    public dialogRef: MatDialogRef<CollaboratorsReportComponent>,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    private appCommon: AppCommon,
    public collaboratorsService: CollaboratorsService,
    appClientSettings: AppClientSettings
  ) {
    this.clientSettings = appClientSettings.settings
  }


  export() {
    this.isProcess['export'] = true;

    var report_name = "BC_DoanhThu_CTV.xlsx";
    var params = this.params;

    params.from = this.params.from ? this.appCommon.formatDateTime(this.params.from, "yyyy-MM-dd") : null;
    params.to = this.params.to ? this.appCommon.formatDateTime(this.params.to, "yyyy-MM-dd") : null;

    this.collaboratorsService.export(params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess['export'] = true;
          break;
        case HttpEventType.Response:
          this.isProcess['export'] = false;
          const downloadedFile = new Blob([data.body], { type: data.body.type });
          const a = document.createElement('a');
          a.setAttribute('style', 'display:none;');
          document.body.appendChild(a);
          a.download = report_name;
          a.href = URL.createObjectURL(downloadedFile);
          a.target = '_blank';
          a.click();
          document.body.removeChild(a);
          this.isProcess['export'] = false;
          break;
      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
