export class RoleGroup {
  id: number;
  name: string;
  description: string;
  order: number;
  isShow: boolean;
  icon: string;
}

export class RoleGroupCommand {

  id: number;
  name: string;
  description: string;
  order: number;
  isShow: boolean;
  icon: string;

  deserialize(input: RoleGroup): RoleGroupCommand {
    if (input == null) {
      input = new RoleGroup();
    }
    this.id = input.id;
    this.name = input.name;
    this.description = input.description;
    this.order = input.order;
    this.isShow = input.isShow;
    this.icon = input.icon;
    return this;
  }
}