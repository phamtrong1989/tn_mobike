export class MongoLogDTO {
  type: number;
  content: string;
  accountId: number;
  logKey: string;
  functionName: string;
  createTime: string;
  createTimeTicks: number;
}