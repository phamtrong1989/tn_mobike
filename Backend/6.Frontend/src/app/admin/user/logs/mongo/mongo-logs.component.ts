import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from 'src/app/app.common';
import { LogsService } from './logs.service';
import { MongoLogDTO } from './mongo-logs.model';
import { AppSettings } from 'src/app/app.settings';
import { AppStorage } from 'src/app/app.storage';
import { Settings } from 'src/app/app.settings.model';
import { FormControl } from '@angular/forms';
import { Account } from 'src/app/admin/customer/accounts/accounts.model';

@Component({
  selector: 'app-mongo-logs',
  templateUrl: './mongo-logs.component.html',
  providers: [LogsService, AppCommon]
})

export class MongoLogsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: MongoLogDTO[];
  accountKey: FormControl = new FormControl("");
  accounts: Account[];
  isProcess = { dialogOpen: false, listDataTable: true, accountSearching: false  };
  params: any = { pageIndex: 1, pageSize: 20, accountId: "", logKey: "", type: "" , functionName: "", date: "", systemType: "0",  sortby: "",  sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "createTime", "content", "logKey", "functionName", "accountId", "type"];
  public settings: Settings;
   public categoryData :any;
  constructor(
    public appSettings: AppSettings,
    public logsService: LogsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
	  public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
	  this.categoryData =  this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    if(this.params.date == "" || this.params.date == null)
    {
      this.params.date = this.appCommon.formatDateTime(new Date(),"yyyy-MM-dd");
    }
    this.initSearchAccount();
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.date = this.appCommon.formatDateTime(this.params.date, 'yyyy-MM-dd');
    this.appCommon.changeUrl(location.pathname, this.params);
    this.logsService.mongoSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
        this.isProcess.accountSearching = false;
        if (rs != "") {
          this.isProcess.accountSearching = true;
          this.logsService.searchByAccount({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.params.accountId = "";
            }
            this.accounts = rs.data;
            this.isProcess.accountSearching = false;
          });
        }
      },
        error => {
          this.isProcess.accountSearching = false;
      });
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  ngAfterViewInit() {
     setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

 fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if(window.innerHeight > 550)
      {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else
      {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
      this.fixHeightTable()
  }
}