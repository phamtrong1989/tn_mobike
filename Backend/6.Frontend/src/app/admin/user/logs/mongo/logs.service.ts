import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ApiResponseData, ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AppCommon } from 'src/app/app.common';
import { MongoLogDTO } from './mongo-logs.model';
import { Account } from 'src/app/admin/customer/accounts/accounts.model';

@Injectable()
export class LogsService {
    public url = "/api/admin/Log";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    mongoSearchPaged(params: any) {
        return this.http.get<ApiResponseData<MongoLogDTO[]>>(this.url + "/MongoSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByAccount(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchByAccount", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}