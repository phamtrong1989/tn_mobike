import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../app.settings';
import { Settings, ApiResponseData } from '../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { RoleAreasService } from './role-areas.service';
import { RoleArea, RoleAreaCommand } from './role-areas.model';
import { RoleAreasEditComponent } from './edit/role-areas-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';
@Component({
  selector: 'app-role-areas',
  templateUrl: './role-areas.component.html',
  providers: [RoleAreasService, AppCommon]
})

export class RoleAreasComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: RoleArea[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "id", "name", "order", "isShow", "action"];
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public roleAreasService: RoleAreasService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {

    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.appCommon.changeUrl(location.pathname, this.params);
    this.roleAreasService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }


  public openEditDialog(data: RoleArea) {
    var model = new RoleAreaCommand();

    if (data == null) {
      model.id = "";
    }
    else {
      model = new RoleAreaCommand().deserialize(data);
    }

    this.dialog.open(RoleAreasEditComponent, {
      data: model,
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }


  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}