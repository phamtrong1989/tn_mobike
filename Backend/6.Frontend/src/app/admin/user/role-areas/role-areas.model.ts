export class RoleArea {

id: string;
name: string;
order: number;
isShow: boolean;


}

export class RoleAreaCommand {

 id: string;
name: string;
order: number;
isShow: boolean;


  deserialize(input: RoleArea): RoleAreaCommand {
    if(input==null)
    {
      input  = new RoleArea();
    }
	this.id = input.id;
this.name = input.name;
this.order = input.order;
this.isShow = input.isShow;

   
    return this;
  }
}