import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { RoleAreasComponent } from './role-areas.component';
import { RoleAreasEditComponent } from './edit/role-areas-edit.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';

export const routes = [
  { path: '', component: RoleAreasComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule,
	 TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    }) 
  ],
  declarations: [
    RoleAreasComponent,
    RoleAreasEditComponent
  ],
  entryComponents:[
    RoleAreasEditComponent
  ]
})
export class RoleAreasModule { }
