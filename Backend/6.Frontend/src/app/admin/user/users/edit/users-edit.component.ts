import { Component, OnInit, Inject, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ManagerRegisterCommand, User } from '../users.model';
import { UsersService } from '../users.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { CustomValidator } from 'src/app/theme/utils/app-validators';
import { ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  providers: [UsersService, AppCommon]
})
export class UsersEditComponent implements OnInit, AfterViewInit {

  public form: FormGroup;
  public formCalendars: FormGroup;
  public appStogate: any;
  roles = [];
  companyBranchs = [];
  nationalitys = []
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, userSearching: false };
  public passwordHide: boolean = true;
  public clientSettings: ClientSettings;
  userKey: FormControl = new FormControl("");
  users: User[];
  constructor(
    public dialogRef: MatDialogRef<UsersEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ManagerRegisterCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public usersService: UsersService,
    private cdr: ChangeDetectorRef,
    appClientSettings: AppClientSettings

  ) {
    this.appStogate = this.appStorage.getCategoryData();
    this.roles = this.appStogate.roles;
    this.companyBranchs = this.appStogate.companyBranchs;
    this.nationalitys = this.appStogate.nationalitys;
    this.clientSettings = appClientSettings.settings

    this.form = this.fb.group({
      id: [0],
      code: [null],
      username: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(15)])],
      email: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(100)])],
      roleId: [0, Validators.compose([Validators.required,Validators.min(1)])],
      password: [{value: 'Matkhau@91234', disabled: true}, Validators.compose([Validators.maxLength(12), CustomValidator.passwordStrongValidator])],
      phoneNumber: [null, Validators.compose([Validators.maxLength(20)])],
      displayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      isLock: [false],
      isChangePassword:[false],
      identificationID: [null, Validators.compose([Validators.maxLength(50)])],
      identificationSupplyDate: [null],
      identificationSupplyAdress: [null, Validators.compose([Validators.maxLength(200)])],
      identificationAdress: [null, Validators.compose([Validators.maxLength(200)])],
      identificationPhotoBackside: [null, Validators.compose([Validators.maxLength(500)])],
      identificationPhotoFront: [null, Validators.compose([Validators.maxLength(500)])],
      sex: [2, Validators.compose([Validators.required])],
      fullName: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100)])],
      birthday: [null, Validators.compose([Validators.required])],
      address: [null, Validators.compose([Validators.maxLength(200)])],
      isRetired: [false],
      workingDateFrom: [null],
      workingDateTo: [null],
      managerUserId: [0, Validators.compose([Validators.required,Validators.min(1)])],
      objectIds: ["", Validators.compose([Validators.required])],
      roleType: [0],
      managerUser: [null],
      education: [null, Validators.compose([Validators.maxLength(200)])],
      rfid: [null, Validators.compose([Validators.maxLength(50)])]
    });

    this.form.get('isChangePassword').valueChanges.subscribe(rs=>{
        var isX = this.form.get('isChangePassword').value;
        if(isX==true)
        {
          this.form.get('password').setValue('');
          this.form.get('password').enable();
        }
        else
        {
          this.form.get('password').setValue('Matkhau@9');
          this.form.get('password').disable();
        }
    });
  }

  initDefaulSelectUser()
  {
    this.users = [];
    if(this.data.managerUser != null)
    {
      this.users.push(this.data.managerUser);
    }
  }

  ngOnInit() {
    this.initDefaulSelectUser();
    this.initSearchUser();
    this.form.setValue(this.data);
    var start = new Date().getTime();
    this.isProcess.initData = true;
    this.usersService.getById(this.data.id).then(rs => {

      if (rs.status == 1 && rs.data) {
        this.data = new ManagerRegisterCommand().deserialize(rs.data);
        this.initDefaulSelectUser();
        this.form.setValue(this.data);
        this.isProcess.initData = false;
        var end = new Date().getTime();
        var dur = end - start;
      }
      else {
        this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
        this.isProcess.initData = false;
      }
    });
  }

  ngAfterViewInit(): void {
  }

  initSearchUser() {
    this.userKey.valueChanges.subscribe(rs => {
        this.isProcess.userSearching = false;
        if (rs != "") {
          this.isProcess.userSearching = true;
          this.usersService.searchByUser({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.form.get('managerUserId').setValue(null);
            }
            this.users = rs.data;
            this.isProcess.userSearching = false;
          });
        }
      },
        error => {
          this.isProcess.userSearching = false;
      });
  }


  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.usersService.edit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open("Cập nhật tài khoản thành công", "Thành công", { duration: 5000 });
          this.close(res)
        }
        else {
          this.snackBar.open("Có lỗi hoặc phiên làm việc đã kết thúc, vui lòng F5 refesh lại trình duyệt và thử lại", "danger", { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  getFullUrl(url:string) {
    return this.clientSettings.serverAPI + url;
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Bạn có muốn thực hiện thao tác này không, dữ liệu xóa đi sẽ không thể phục hồi?",
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        if (result) {
          this.isProcess.deleteProcess = true;
          this.usersService.delete(this.data.id).then((res) => {
            if (res.status == 1) {
              this.snackBar.open("Xóa thành công", "Thành công", { duration: 5000, });
              this.isProcess.deleteProcess = false;
              this.close(this.data)
            }
            else if (res.status == 2) {
              this.snackBar.open("Quyền này đang sử dụng, vui lòng xóa tài khoản liên quan đến quyền này rồi thử lại", "Cảnh báo", { duration: 5000 });
            }
            else if (res.status == 0) {
              this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
              this.close(this.data)
            }
          });
        }
      }
    });
  }

  showManager()
  {
    try
    {
        this.form.get("objectIds").disable();
        if(this.form.get('roleId').value == 0)
        {
          this.form.get("managerUserId").disable();
          return false;
        }
        else
        {
          var thisData = this.roles.find(x=>x.id == this.form.get('roleId').value);
          if(thisData == null)
          {
            this.form.get("managerUserId").disable();
            return false;
          }

          if(thisData.type == 5)
          {
            this.form.get("managerUserId").enable();
            return true;
          }
          else
          {
            this.form.get("managerUserId").disable();
            return false;
          }
        }
    } 
    catch
    {
      this.form.get("managerUserId").disable();
      return false;
    }
  }

  showInvester()
  {
    try
    {
        this.form.get("managerUserId").disable();
        if(this.form.get('roleId').value == 0)
        {
          this.form.get("objectIds").disable();
          return false;
        }
        else
        {
          var thisData = this.roles.find(x=>x.id == this.form.get('roleId').value);
          if(thisData == null)
          {
            this.form.get("objectIds").disable();
            return false;
          }

          if(thisData.type == 11)
          {
            this.form.get("objectIds").enable();
            return true;
          }
          else
          {
            this.form.get("objectIds").disable();
            return false;
          }
        }
    } 
    catch
    {
      this.form.get("objectIds").disable();
      return false;
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  resetInputPassword() {
    this.form.get('password').setValue('');
  }
}
