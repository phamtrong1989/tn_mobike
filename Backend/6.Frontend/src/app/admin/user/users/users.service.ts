import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { User, ManagerRegisterCommand } from './users.model';
import { ApiResponseData,  ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { shareReplay, retry, catchError } from 'rxjs/operators';
@Injectable()
export class UsersService {

    public url = "/api/admin/Users";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon :AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
   
    create(user: ManagerRegisterCommand) {
        return this.http.post<ApiResponseData<ManagerRegisterCommand>>(this.url + "/create", user).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    edit(user: ManagerRegisterCommand) {
        return this.http.put<ApiResponseData<ManagerRegisterCommand>>(this.url + "/edit", user).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
    getById(id: number) {
        return this.http.get<ApiResponseData<User>>(this.url + "/getById/" + id).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
    delete(id: number) {
        return this.http.delete<ApiResponseData<User>>(this.url + "/delete/" + id).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    searchByUser(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchByUser", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    uploadImage()
    {
        return this.url + "/uploadImage";
    }
    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
} 