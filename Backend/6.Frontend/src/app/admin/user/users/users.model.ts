import { DatatableRowDetailDirective } from "@swimlane/ngx-datatable";
import { Role } from "./../roles/roles.model";

export class User {
  id: number;
  accessFailedCount: number;
  avatar: string;
  concurrencyStamp: string;
  createdDate: Date;
  createdUserId: number;
  displayName: string;
  email: string;
  emailConfirmed: boolean;
  isLock: boolean;
  isReLogin: boolean;
  isSuperAdmin: boolean;
  lockoutEnabled: boolean;
  lockoutEnd: Date;
  normalizedEmail: string;
  normalizedUserName: string;
  note: string;
  noteLock: string;
  passwordHash: string;
  phoneNumber: string;
  phoneNumberConfirmed: boolean;
  securityStamp: string;
  twoFactorEnabled: boolean;
  updatedDate: Date;
  updatedUserId: number;
  userName: string;
  roles: Role[];

  address: string;
  identificationID: string;
  identificationSupplyDate: Date;
  identificationSupplyAdress: string;
  identificationAdress: string;
  identificationPhotoFront: string;
  identificationPhotoBackside: string;
  managerUserId: number;
  roleType: number;
  fullName: string;
  birthday: Date;
  sex: number;
  workingDateFrom: Date;
  workingDateTo: Date;
  isRetired: boolean;
  managerUser: User;
  code: string;
  education: string;
  rfid: string;

  objectIds: string;
}

export class ManagerRegisterCommand {

  id: number;
  username: string;
  email: string;
  phoneNumber: string;
  displayName: string;
  password: string;
  isLock: boolean;
  note: string;
  roleId: number;
  avatar: string;
  isChangePassword: boolean;

  address: string;
  identificationID: string;
  identificationSupplyDate: Date;
  identificationSupplyAdress: string;
  identificationAdress: string;
  identificationPhotoFront: string;
  identificationPhotoBackside: string;
  managerUserId: number;
  roleType: number;
  fullName: string;
  birthday: Date;
  sex: number;
  workingDateFrom: Date;
  workingDateTo: Date;
  isRetired: boolean;
  managerUser: User;
  code: string;
  education: string;
  rfid: string;
  objectIds: string;
  deserialize(input: User): ManagerRegisterCommand {
    let roleId = 0;
    if (input.roles != undefined && input.roles != null && input.roles.length > 0) {
      roleId = input.roles[0].id;
    }
    this.id = input.id;
    this.username = input.userName;
    this.email = input.email;
    this.phoneNumber = input.phoneNumber;
    this.displayName = input.displayName;
    this.password = "";
    this.isLock = input.isLock;
    this.roleId = roleId;
    this.isChangePassword = false;

    this.address = input.address;
    this.identificationID = input.identificationID;
    this.identificationSupplyDate = input.identificationSupplyDate;
    this.identificationSupplyAdress = input.identificationSupplyAdress;
    this.identificationAdress = input.identificationAdress;
    this.identificationPhotoFront = input.identificationPhotoFront;
    this.identificationPhotoBackside = input.identificationPhotoBackside;
    this.managerUserId = input.managerUserId;
    this.roleType = input.roleType;
    this.fullName = input.fullName;
    this.birthday = input.birthday;
    this.sex = input.sex;
    this.workingDateFrom = input.workingDateFrom;
    this.workingDateTo = input.workingDateTo;
    this.isRetired = input.isRetired;
    this.managerUser = input.managerUser;
    this.code = input.code;
    this.education = input.education;
    this.rfid = input.rfid;
    this.objectIds = input.objectIds;
    return this;
  }
}

export class CustomerUserEditCommand {
  id: number;
  customerId: number;
  isLock: boolean;
  userName: string;
  email: string;
  password: string;
  isChangePassword: boolean;
  education: string;

  address: string;
  identificationID: string;
  identificationSupplyDate: Date;
  identificationSupplyAdress: string;
  identificationAdress: string;
  identificationPhotoFront: string;
  identificationPhotoBackside: string;
  managerUserId: number;
  roleType: number;
  fullName: string;
  birthday: Date;
  sex: number;
  workingDateFrom: Date;
  workingDateTo: Date;
  isRetired: boolean;
  managerUser: User;
  code: string;
  rfid: string;

  deserialize(input: User): CustomerUserEditCommand {
    this.id = input.id;
    this.customerId = 0;
    this.isLock = input.isLock;
    this.userName = input.userName;
    this.password = "";
    this.email = input.email;
    this.isChangePassword = false;
    this.address = input.address;
    this.identificationID = input.identificationID;
    this.identificationSupplyDate = input.identificationSupplyDate;
    this.identificationSupplyAdress = input.identificationSupplyAdress;
    this.identificationAdress = input.identificationAdress;
    this.identificationPhotoFront = input.identificationPhotoFront;
    this.identificationPhotoBackside = input.identificationPhotoBackside;
    this.managerUserId = input.managerUserId;
    this.roleType = input.roleType;
    this.fullName = input.fullName;
    this.birthday = input.birthday;
    this.sex = input.sex;
    this.workingDateFrom = input.workingDateFrom;
    this.workingDateTo = input.workingDateTo;
    this.isRetired = input.isRetired;
    this.managerUser = input.managerUser;
    this.code = input.code;
    this.education = input.education;
    this.rfid = input.rfid;

    return this;
  }
}
