import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../../app.settings';
import { Settings, ApiResponseData } from '../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { RolesService } from './roles.service';
import { Role } from './roles.model';
import { RoleCreateComponent } from './create/role-create.component';
import { RoleEditComponent } from './edit/role-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../app.common'

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  providers: [RolesService, AppCommon]
})

export class RolesComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  heightTable: number = 400;
  listData: Role[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, key: "", type: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ['id', 'name', 'type', 'action'];
  public settings: Settings;
  constructor(
    public appSettings: AppSettings,
    public rolesService: RolesService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public cdRef:ChangeDetectorRef
  ) {
    this.settings = this.appSettings.settings;
  }

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.searchPage();
  }

  ngAfterViewInit() {
    this.heightTable = this.appCommon.initHeightTable("scroll_table", 75);
    this.cdRef.detectChanges();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.appCommon.changeUrl(location.pathname, this.params);
    this.rolesService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.active;
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openCreateDialog() {

    this.dialog.open(RoleCreateComponent, {
      data: null,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
      }
    });

  }

  public openEditDialog(data: Role) {

    this.dialog.open(RoleEditComponent, {
      data: new Role().deserialize(data),
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
      }
    });
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.heightTable = this.appCommon.initHeightTable("scroll_table", 75)
  }
}