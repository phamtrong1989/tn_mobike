import { RoleArea } from "../role-areas/role-areas.model";
import { RoleAction } from "../role-controllers/actions/role-actions.model";
import { RoleController } from "../role-controllers/role-controllers.model";
import { RoleGroup } from "../role-groups/role-groups.model";

export class Role {
  id: number;
  name: string;
  type: number;
  description: string;
  roleDetailObj: RoleDetailObj;
  roleActions: RoleAction[];

  deserialize(input: Role): RoleEditCommand {
    this.id = input.id;
    this.name = input.name;
    this.type = input.type;
    this.description = input.description;
    this.roleActions = [];
    return this;
  }
}

export class RoleEditCommand {
  id: number;
  name: string;
  type: number;
  description: string;
  roleActions: RoleAction[];
}

export class RoleTypeData {
  value: number;
  text: string;
}

export class RoleDetailObj {
  roleAreas: RoleArea[];
  roleControllers: RoleController[];
  roleDetails: RoleDetail[];
  roleActions: RoleAction[];
  roleGroups: RoleGroup[];
}

export class RoleDetail {
  actionId: number;
  roleId: number;
}
