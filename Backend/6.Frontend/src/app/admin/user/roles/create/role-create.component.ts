import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { RoleEditCommand } from '../roles.model';
import { RolesService } from '../roles.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  providers: [RolesService, AppCommon]
})
export class RoleCreateComponent implements OnInit {
  public form: FormGroup;
  roletypes = [];
  isProcess = { saveProcess: false };
  public passwordHide: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<RoleCreateComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: RoleEditCommand,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public rolesService: RolesService,
    public appStorage: AppStorage
  ) {

    this.roletypes = this.appStorage.getCategoryData().roleTypes;
    this.form = this.fb.group({
      id: 0,
      name: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100)])],
      description: [null, Validators.compose([Validators.maxLength(1000)])],
      type: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.data = new RoleEditCommand();
  }
  reset() {
    this.form.reset();
  }
  save() {
    if (this.form.valid) {
      this.isProcess.saveProcess = true;
      this.rolesService.create(this.form.value).then((res) => {
        if (res.status == 1 && res.data) {
          this.snackBar.open("Thêm mới thành công", "Thành công", { duration: 5000 });
          this.close(res)
        }
        else {
          this.snackBar.open("Thêm mới thất bại", "Cảnh báo", { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    } 

  }
  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close(1);
    }
  }
}
