import { Component, OnInit, Inject, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Role, RoleDetail, RoleDetailObj, RoleEditCommand } from '../roles.model';
import { RolesService } from '../roles.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from '../../../../theme/components/confirmation-dialog/confirmation-dialog.component';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { RoleController } from '../../role-controllers/role-controllers.model';
import { RoleAction } from '../../role-controllers/actions/role-actions.model';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  providers: [RolesService, AppCommon]
})
export class RoleEditComponent implements OnInit,AfterViewInit {
 
  public form: FormGroup;
  roletypes = [];
  roleDetailObj : RoleDetailObj;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
 
  public passwordHide: boolean = true;
  constructor(
    public dialogRef: MatDialogRef<RoleEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoleEditCommand,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public rolesService: RolesService,
    public dialog: MatDialog,
    public appStorage : AppStorage,
    private cdr: ChangeDetectorRef
  ) {
   
    this.form = this.fb.group({
      id: 0,
      name: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100)])],
      description: [null, Validators.compose([Validators.maxLength(1000)])],
      type: [null, Validators.compose([Validators.required])],
      roleActions: [null]
    });
    this.form.setValue(this.data);
  }

  ngOnInit() {
    this.roletypes = this.appStorage.getCategoryData().roleTypes;
    this.isProcess.initData = true;
    this.rolesService.getById(this.data.id, true).then(rs => {
      if (rs.status==1 && rs.data) {
        this.data = new Role().deserialize(rs.data);
        this.form.setValue(this.data);
        this.roleDetailObj = rs.data.roleDetailObj;

        for(var i = 0; i < this.roleDetailObj.roleActions.length;i ++)
        {
          var kt = this.roleDetailObj.roleDetails.find(x=>x.actionId == this.roleDetailObj.roleActions[i].id);
          if(kt!=null)
          {
            this.roleDetailObj.roleActions[i].checked = true;
          }
          else
          {
            this.roleDetailObj.roleActions[i].checked =  false;
          }
        }

        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "warning", { duration: 5000, });
      }
    });
  }
  
  getController(controllers: RoleController[], groupId: number)
  {
      return controllers.filter(x=>x.groupId == groupId);
  }

  getActionChecked(actionId:number)
  {
      var kt = this.roleDetailObj.roleDetails.find(x=>x.actionId == actionId);
      if(kt!=null)
      {
        return true;
      }
      else
      {
        return false;
      }
  }

  getAction(acctions: RoleAction[], controllerId: string)
  {
      return acctions.filter(x=>x.controllerId == controllerId);
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }

  save() {
    if (this.form.valid)
    {
      this.isProcess.saveProcess = true;
      this.form.controls["roleActions"].setValue(this.roleDetailObj.roleActions);
      this.rolesService.edit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open("Cập nhật thành công", "Thành công", { duration: 5000, });
          this.isProcess.saveProcess = false;
          this.close(res.data)
        }
        else if (res.status == 0) {
          this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
          this.close(this.data)
        }
      });
    }
    
  }
  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Bạn có muốn thực hiện thao tác này không, dữ liệu xóa đi sẽ không thể phục hồi?",
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        if(result)
        {
          this.isProcess.deleteProcess =true;
          this.rolesService.delete(this.data.id).then((res) => {
            if(res.status==1)
            {
              this.snackBar.open("Xóa thành công", "Thành công", { duration: 5000, });
              this.isProcess.deleteProcess =false;
              this.close(this.data)
            }
            else if(res.status==2)
            {
              this.snackBar.open("Xóa thất bại, dữ liệu đang được sử dung ở quản lý tài khoản quản trị", "Thất bại", { duration: 5000, });
              this.isProcess.deleteProcess =false;
            }
            else if(res.status==0)
            {
              this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
              this.close(this.data)
            }
          });
        }
      }
    });
  }
  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
