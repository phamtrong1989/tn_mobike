import { Injectable, Injector } from '@angular/core';
import { HttpClient,  HttpErrorResponse } from '@angular/common/http';
import { Role } from './roles.model';
import { ApiResponseData,  ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { shareReplay, retry, catchError } from 'rxjs/operators';
@Injectable()
export class RolesService {
    public url = "/api/admin/roles";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon :AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<Role[]>>(this.url + "/search", this.appCommon.getHeaders(params)).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
   
    create(user: Role) {
        return this.http.post<ApiResponseData<Role>>(this.url + "/create", user).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    edit(user: Role) {
        return this.http.put<ApiResponseData<Role>>(this.url + "/edit", user).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
    getById(id: number, importDetails: boolean = false) {
        return this.http.get<ApiResponseData<Role>>(this.url + "/getById/" + id +"/" + importDetails).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
    delete(id: number) {
        return this.http.delete<ApiResponseData<Role>>(this.url + "/delete/" + id).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
} 