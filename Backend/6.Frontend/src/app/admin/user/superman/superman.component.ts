import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from 'src/app/app.common';
import { AppSettings } from 'src/app/app.settings';
import { AppStorage } from 'src/app/app.storage';
import { Settings } from 'src/app/app.settings.model';
import { FormControl } from '@angular/forms';
import { Account } from 'src/app/admin/customer/accounts/accounts.model';
import { SupermanService } from './superman.service';
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-superman',
  templateUrl: './superman.component.html',
  providers: [SupermanService, AppCommon]
})

export class SupermanComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  isProcess = { saveProcess: false };
  projectId: FormControl = new FormControl("projectId");
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public supermanService: SupermanService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
  }
  
  save() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Chức năng này rất nguy hiểm, bạn có muốn thực hiện chức năng này",
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.saveProcess = true;
        this.supermanService.dockOpenAll(this.projectId.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open("Gửi lệnh mở khóa tất cả thành công", "Thành công", { duration: 5000 });
          }
          else {
            this.snackBar.open("Gửi lệnh mở khóa tất cả thất bại", "Thất bại", { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    });
  }
}