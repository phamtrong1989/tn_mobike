import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ApiResponseData, ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AppCommon } from 'src/app/app.common';
import { Account } from 'src/app/admin/customer/accounts/accounts.model';

@Injectable()
export class SupermanService {
    public url = "/api/admin/superman";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    dockOpenAll(projectId: number) {
        return this.http.put<ApiResponseData<any>>(this.url + "/DockOpenAll/" + projectId, {}).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    
    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}