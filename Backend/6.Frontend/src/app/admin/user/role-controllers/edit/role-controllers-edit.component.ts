import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RoleController, RoleControllerCommand } from '../role-controllers.model';
import { RoleControllersService } from '../role-controllers.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { RoleActionsComponent } from '../actions/role-actions.component';

@Component({
  selector: 'app-role-controllers-edit',
  templateUrl: './role-controllers-edit.component.html',
  providers: [
    RoleControllersService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class RoleControllersEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<RoleControllersEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: RoleControllerCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public roleControllersService: RoleControllersService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(200)])],
      areaId: [null, Validators.compose([Validators.required])],
      groupId: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(200)])],
      description: [null, Validators.compose([Validators.maxLength(1000)])],
      order: [0, Validators.compose([Validators.required])],
      isShow: [true],
      icon: [null, Validators.compose([Validators.maxLength(1000)])],
      routerLink: [null, Validators.compose([Validators.maxLength(1000)])],
      href: [null, Validators.compose([Validators.maxLength(1000)])]
    });
  }

  ngOnInit() {

    if (this.data.id != "") {
      this.form.get('id').disable();
      this.form.setValue(this.data);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  matTabGroupChange(e: any, roleAcction: RoleActionsComponent) {
    if (e.index == 1) {
      roleAcction.firsLoad();
    }
  }

  getData() {
    this.isProcess.initData = true;
    this.roleControllersService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new RoleControllerCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id != "") {
        this.form.get('id').enable();
        this.roleControllersService.edit(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.close(res)
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.roleControllersService.create(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new RoleControllerCommand().deserialize(res.data);
            this.form.setValue(this.data);
            this.close(res)
          }
          else if (res.status == 2) {
            this.snackBar.open("Quyền controller đã tồn tại, vui lòng kiểm tra lại", this.baseTranslate["msg-succes-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });

      }
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.roleControllersService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          } else if (res.status == 2) {
            this.snackBar.open("Quyền controller đang đự sử dụng, xóa thất bại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });
      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
