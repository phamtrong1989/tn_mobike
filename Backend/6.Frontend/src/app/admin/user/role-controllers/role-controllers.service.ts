import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { RoleController, RoleControllerCommand } from './role-controllers.model';
import { ApiResponseData,  ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { RoleAction, RoleActionCommand } from './actions/role-actions.model';

@Injectable()
export class RoleControllersService {
    public url = "/api/admin/RoleControllers";
     _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon :AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<RoleController[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
   
    create(user: RoleControllerCommand) {
        return this.http.post<ApiResponseData<RoleController>>(this.url + "/create", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: RoleControllerCommand) {
        return this.http.put<ApiResponseData<RoleController>>(this.url + "/edit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: string) {
        return this.http.get<ApiResponseData<RoleController>>(this.url + "/getById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    delete(id: string) {
        return this.http.delete<ApiResponseData<RoleController>>(this.url + "/delete/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    roleActionSearchPaged(params: any) {
        return this.http.get<ApiResponseData<RoleAction[]>>(this.url + "/roleActionSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
   
    roleActionCreate(user: RoleActionCommand) {
        return this.http.post<ApiResponseData<RoleAction>>(this.url + "/roleActionCreate", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    roleActionEdit(user: RoleActionCommand) {
        return this.http.put<ApiResponseData<RoleAction>>(this.url + "/roleActionEdit", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    roleActionGetById(id: number) {
        return this.http.get<ApiResponseData<RoleAction>>(this.url + "/roleActionGetById/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    roleActionDelete(id: number) {
        return this.http.delete<ApiResponseData<RoleAction>>(this.url + "/roleActionDelete/" + id).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    autoCreate() {
        return this.http.post<ApiResponseData<RoleController[]>>(this.url + "/AutoCreate", {}).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

	 handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
} 