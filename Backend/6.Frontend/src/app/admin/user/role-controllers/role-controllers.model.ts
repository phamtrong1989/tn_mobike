import { RoleArea } from "../role-areas/role-areas.model";
import { RoleGroup } from "../role-groups/role-groups.model";

export class RoleController {
  id: string;
  areaId: string;
  groupId: number;
  name: string;
  description: string;
  order: number;
  isShow: boolean;
  roleGroup: RoleGroup;
  roleArea: RoleArea;

  icon: string;
  routerLink: string;
  href: string;
}

export class RoleControllerCommand {

  id: string;
  areaId: string;
  groupId: number;
  name: string;
  description: string;
  order: number;
  isShow: boolean;

  icon: string;
  routerLink: string;
  href: string;

  deserialize(input: RoleController): RoleControllerCommand {
    if (input == null) {
      input = new RoleController();
    }
    this.id = input.id;
    this.areaId = input.areaId;
    this.groupId = input.groupId;
    this.name = input.name;
    this.description = input.description;
    this.order = input.order;
    this.isShow = input.isShow;

    this.icon = input.icon;
    this.routerLink = input.routerLink;
    this.href = input.href;
    return this;
  }
}