export class RoleAction {

id: number;
controllerId: string;
name: string;
actionName: string;
description: string;
order: number;
isShow: boolean;
checked: boolean;
}

export class RoleActionCommand {

 id: number;
controllerId: string;
name: string;
actionName: string;
description: string;
order: number;
isShow: boolean;


  deserialize(input: RoleAction): RoleActionCommand {
    if(input==null)
    {
      input  = new RoleAction();
    }
	this.id = input.id;
this.controllerId = input.controllerId;
this.name = input.name;
this.actionName = input.actionName;
this.description = input.description;
this.order = input.order;
this.isShow = input.isShow;

   
    return this;
  }
}