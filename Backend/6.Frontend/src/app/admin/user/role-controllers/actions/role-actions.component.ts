import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { RoleAction, RoleActionCommand } from './role-actions.model';
import { RoleActionsEditComponent } from './edit/role-actions-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { RoleControllersService } from '../role-controllers.service';
import { AppCommon } from 'src/app/app.common';
import { Settings } from 'src/app/app.settings.model';
import { AppSettings } from 'src/app/app.settings';
import { AppStorage } from 'src/app/app.storage';
import { RoleController } from '../role-controllers.model';
@Component({
  selector: 'app-role-actions',
  templateUrl: './role-actions.component.html',
  providers: [RoleControllersService, AppCommon]
})

export class RoleActionsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  @Input() roleController : RoleController;
  heightTable: number = 348;
  listData: RoleAction[];
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 20, controllerId: "", key: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "id", "actionName", "name", "order", "isShow", "action"];
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public roleControllersService: RoleControllersService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {

  }
  
  public firsLoad() {
    this.fixHeightTable();
    this.search();
  }

  searchPage(): void {
    this.params.controllerId = this.roleController.id;
    this.isProcess.listDataTable = true;
    this.roleControllersService.roleActionSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: RoleAction) {
    var model = new RoleActionCommand();
    if (data == null) {
      model.id = 0;
    }
    else {
      model = new RoleActionCommand().deserialize(data);
    }
    model.controllerId = this.roleController.id;
 
    this.dialog.open(RoleActionsEditComponent, {
      data: model,
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}