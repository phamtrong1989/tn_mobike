import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { RoleControllersComponent } from './role-controllers.component';
import { RoleControllersEditComponent } from './edit/role-controllers-edit.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { RoleActionsEditComponent } from './actions/edit/role-actions-edit.component';
import { RoleActionsComponent } from './actions/role-actions.component';

export const routes = [
  { path: '', component: RoleControllersComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule,
	 TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    }) 
  ],
  declarations: [
    RoleControllersComponent,
    RoleControllersEditComponent,
    RoleActionsEditComponent,
    RoleActionsComponent
  ],
  entryComponents:[
    RoleControllersEditComponent,
    RoleActionsEditComponent,
    RoleActionsComponent
  ]
})
export class RoleControllersModule { }
