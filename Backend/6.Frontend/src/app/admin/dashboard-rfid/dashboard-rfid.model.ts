import { Account } from "../customer/accounts/accounts.model";
import { Bike } from "../infrastructure/bikes/bikes.model";
import { User } from "../user/users/users.model";

export class RFIDBooking {
    id: number;
    rFID: string;
    transactionCode: string;
    projectId: number;
    investorId: number;
    stationIn: number;
    stationOut: number;
    bikeId: number;
    dockId: number;
    userId: number;
    accountId: number;
    startTime: Date;
    endTime: Date;
    note: string;
    status: number;
    type: number;
    startLat: number;
    startLng: number;
    endLat: number;
    endLng: number;
    updatedDate: Date;
    upatedUser: number;
    account: Account;
    user: User;
    bike: Bike;
}