import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { Booking } from '../../transaction/bookings/bookings.model';
import { DashboardRFIDService } from '../dashboard-rfid.service';
import { RFIDBooking } from '../dashboard-rfid.model';

@Component({
  selector: 'app-open-lock',
  templateUrl: './open-lock.component.html',
  providers: [
    DashboardRFIDService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class OpenLockComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<OpenLockComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: RFIDBooking,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public dashboardRFIDService: DashboardRFIDService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      dockId: [0],
      feedback: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(1000)])]
    });
  }

  ngOnInit() {
    if (this.data.id > 0) {
      this.form.get('dockId').setValue(this.data.dockId);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      if (this.data.id > 0) {
        this.dashboardRFIDService.openLock(this.form.value).then((res) => {
          if (res.status == 1) {
            this.snackBar.open("Mở khóa thành công", this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else if (res.status == 2) {
            this.snackBar.open("Giao dịch không tồn tại vui lòng thử lại", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          else {
            this.snackBar.open("Mở khóa thất bại, vượt quá thời gian không thấy phản hồi từ khóa", this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
