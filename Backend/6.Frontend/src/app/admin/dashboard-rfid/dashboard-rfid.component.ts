import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AppClientSettings, AppSettings } from '../../app.settings';
import { ClientSettings, Settings } from '../../app.settings.model';
import { TranslateService } from '@ngx-translate/core';
import { AppStorage } from 'src/app/app.storage';
import { AccountData } from '../account/account.model';
import { AppCommon } from 'src/app/app.common';
import { GpsData, Station } from '../infrastructure/stations/stations.model';
import { Booking } from '../transaction/bookings/bookings.model';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { MatSort } from '@angular/material/sort';
import { Account } from '../customer/accounts/accounts.model';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { Bike } from '../infrastructure/bikes/bikes.model';
import { LogAdmin } from '../category/projects/projects.model';
import { RoleController } from '../user/role-controllers/role-controllers.model';
import { User } from '../user/users/users.model';
import { DashboardRFIDService } from './dashboard-rfid.service';
import { RFIDBooking } from './dashboard-rfid.model';
import { DashboardBikesMapComponent } from './bikes/map/bikes-map.component';
import { OpenLockComponent } from './open-lock/open-lock.component';
import { EndComponent } from './end/end.component';

@Component({
  selector: 'app-dashboard-rfid',
  templateUrl: './dashboard-rfid.component.html',
  providers: [AppCommon, DashboardRFIDService]
})
export class DashboardRFIDComponent implements OnInit, OnDestroy {
  location: Location
  public settings: Settings;
  accountInfo: AccountData;
  viewType: number = 0;
  isProcess = { dialogOpen: false, listDataTable: true, cancelBooking: false, endBooking: false, openLock: false, accountSearching: false, userSearching: false };
  heightTable: number = 900;
  heightTabTable: number = 210;
  iconSize: number = 20;
  stations: Station[] = [];
  searchStations: Station[] = [];
  rfidBookings: RFIDBooking[] = [];
  searchRFIDBooking: RFIDBooking[] = [];
  currentStationMarker: StationMarker;
  currentRFIDBookingMarker: RFIDBookingMarker;

  iconStation_DuXe: any = { url: '../assets/img/vendor/leaflet/station-success.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_ThuaXe: any = { url: '../assets/img/vendor/leaflet/station-duxe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_ThieuXe: any = { url: '../assets/img/vendor/leaflet/station-thieuXe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconStation_HetXe: any = { url: '../assets/img/vendor/leaflet/station-hetxe.png', scaledSize: { height: this.iconSize, width: this.iconSize }, anchor: { x: 10, y: 10 } };
  iconBike: any = { url: '../assets/img/vendor/leaflet/bike-icon.png', scaledSize: { height: 20, width: 20 }, anchor: { x: 10, y: 10 } };
  iconBike_black: any = { url: '../assets/img/vendor/leaflet/bike-icon-black.png', scaledSize: { height: 20, width: 20 }, anchor: { x: 10, y: 10 } };

  categoryData: any;
  heightListBikeRight: number = 514;
  accountKey: FormControl = new FormControl("");
  isShowRFIDBooking: FormControl = new FormControl(true); 
  isShowStation: FormControl = new FormControl(true); 

  mapStyle: any = [
    { featureType: "road", stylers: [{ visibility: "on" }] },
    { featureType: "poi", stylers: [{ visibility: "off" }] },
    { featureType: "transit", stylers: [{ visibility: "off" }] },
    { featureType: "administrative", stylers: [{ visibility: "off" }] },
    { featureType: "administrative.locality", stylers: [{ visibility: "off" }] }
  ];
  displayedStationColumns = ["name", "cityId", "address", "info", "totalBike", "totalBikeGood", "totalBikeNotGood"];
  displayedRFIDBookingColumns = ["transactionCode", "type", "accountInfo", "maxBoooking", "stationIn", "bikeId",  "totalMinutes", "action"];

  stationParams: any = { key: "", investorId: "", cityId: "", districtId: "", status: "", projectId: "" };
  rfidBookingParams: any = { key: "", investorId: "", plate: "" };
  intevalStation: any;
  intevalBooking: any;
  intevalPolylines: any;
  polylines: GpsData[];
  bikeNotBookings: Bike[] = [];
  oldCurrentRFIDBookingMarker: RFIDBookingMarker;
  intevalAccountOnline: any;
  ctrlProjectId = new FormControl();
  systemUserKey = new FormControl();
  users: User[];
  private hubConnection: HubConnection;
  public clientSettings: ClientSettings;
  typeStatistic: string = 'day';
  accounts: Account[];
  bikeList: Bike[];
  logAdminList: LogAdmin[];
  stopEvent: boolean = false;
  roleControllers: RoleController[];
  statisticDate: Date = new Date();
  accountOnline: any;
  constructor(
    public appSettings: AppSettings,
    public appStorage: AppStorage,
    public dashboardRFIDService: DashboardRFIDService,
    public appCommon: AppCommon,
    public dialog: MatDialog,
    appClientSettings: AppClientSettings,
    private _notifications: NotificationsService

  ) {
    this.settings = this.appSettings.settings;
    this.accountInfo = this.appStorage.getAccountInfo();
    this.categoryData = appStorage.getCategoryData();
    this.searchRFIDBooking = [];
    this.searchStations = [];
    this.clientSettings = appClientSettings.settings
    this.roleControllers = this.categoryData.roleControllers.filter(x => x.id != 'Account' && x.id != 'Base`2' && x.id != 'Feedbacks' && x.id != 'Home' && x.id != 'InitData' && x.id != 'TicketPrices' && x.id != 'RoleGroups' && x.id != 'RoleAreas' && x.id != 'Public' && x.id != 'ReportInfrastructure')
    if (this.appStorage.getProjectId() == null || this.appStorage.getProjectId() == undefined) {
      this.appStorage.setProjectId(appStorage.getCategoryData().projects[0].id);
    }

    this.ctrlProjectId.setValue(this.appStorage.getProjectId());

    this.ctrlProjectId.valueChanges.subscribe(res => {
      this.appStorage.setProjectId(res);
      this.initData();
    });

    this.isShowRFIDBooking.valueChanges.subscribe(res => {
        this.location.rfidBookingMarkers.forEach(x=>{
          x.isShow = res;
        });
    });

    this.isShowStation.valueChanges.subscribe(res => {
      console.log(res);
      this.location.stationMarkers.forEach(x=>{
        x.isShow = res;
      });
    });
  }

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnDestroy(): void {
    this.stopHub();
    if (this.intevalStation) {
      clearInterval(this.intevalStation);
    }
    if (this.intevalPolylines) {
      clearInterval(this.intevalPolylines);
    }
    if (this.intevalBooking) {
      clearInterval(this.intevalBooking);
    }
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
      this.isProcess.accountSearching = false;
      if (rs != "") {
        this.isProcess.accountSearching = true;
        this.dashboardRFIDService.searchByAccount({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
           
          }
          this.accounts = rs.data;
          this.isProcess.accountSearching = false;
        });
      }
    },
      error => {
        this.isProcess.accountSearching = false;
      });
  }

  initData() {
    this.isProcess.dialogOpen = true;
    this.initSearchUser();
    this.initSearchAccount();

    var projectObject = this.appStorage.getCategoryData().projects.find(x => x.id == this.ctrlProjectId.value);
    if (projectObject != null) {
      this.location = {
        latitude: projectObject.lat,
        longitude: projectObject.lng,
        mapType: "street view",
        zoom: projectObject.defaultZoom,
        stationMarkers: [],
        rfidBookingMarkers: []
      }
    }
    else {
      this.location = {
        latitude: 21.0332535,
        longitude: 105.8345708,
        mapType: "street view",
        zoom: 15,
        stationMarkers: [],
        rfidBookingMarkers: []
      }
    }

    setTimeout(() => {
      this.getStations(true);
      this.getRFIDBookings(true);
    }, 2000);
  }

  ngOnInit() {
    this.initData();
    this.setInteval();
  }

  endRFIDBooking() {
    if (this.currentRFIDBookingMarker?.rfidBooking != null) {
      this.dialog.open(EndComponent, {
        data: this.currentRFIDBookingMarker.rfidBooking,
        disableClose: true,
        autoFocus: false,
      }).afterClosed().subscribe(outData => {
      });
    }
  }

  getPolyline() {
    if (this.currentRFIDBookingMarker != null) {
      this.dashboardRFIDService.getByIMEI(this.currentRFIDBookingMarker.rfidBooking.bike?.dock?.imei, this.appCommon.formatDateTime(this.currentRFIDBookingMarker.rfidBooking.startTime, 'yyyy-MM-dd HH:mm:ss')).then((res) => {
        if (this.oldCurrentRFIDBookingMarker?.rfidBooking?.id != this.currentRFIDBookingMarker?.rfidBooking?.id) {
          this.polylines = res.data;
          this.oldCurrentRFIDBookingMarker = this.currentRFIDBookingMarker;
        }
        else if (this.oldCurrentRFIDBookingMarker?.rfidBooking?.id == this.currentRFIDBookingMarker?.rfidBooking?.id && this.polylines.length < res.data.length) {
          this.polylines = res.data;
        }
        else {
          console.log("Không có tọa độ mới")
        }
      });
    }
    else {
      this.polylines = [];
    }
  }

  getStations(isFirst: boolean = false) {
    if(this.isShowStation.value == false)
    {
        return;
    }
    if (isFirst) {
      this.isProcess.dialogOpen = true;
    }

    this.dashboardRFIDService.stations(this.ctrlProjectId.value).then((res) => {
      this.stations = res.data;
      this.stationSearch();
      // Kiểm tra marker nào dữ liệu trả về không có thì xóa bỏ đi
      this.location.stationMarkers.forEach(element => {
        var ktTonTai = this.stations.find(x => x.id == element.station?.id);
        if (ktTonTai == null) {
          element.isEnable = false;
        }
      });
      // Kiểm tra nếu curent ko tồn tại nữa thì đóng set = null
      if (this.currentStationMarker != null && this.location.stationMarkers.find(x => this.currentStationMarker?.station?.id == x.station?.id && x.isEnable == true) == null) {
        this.currentStationMarker.isOpen = false;
        this.currentStationMarker = null;
      }
      // Cập nhật marker
      this.initIconSize();
      this.stations.forEach(station => {
        this.addStationMarker(station.lat, station.lng, station.name, false, station, true, true);
      });
      this.isProcess.dialogOpen = false;
    });
  }

  setInteval() {
    this.intevalStation = setInterval(() => this.getStations(), 10000);
    this.intevalBooking = setInterval(() => this.getRFIDBookings(), 5000);
    this.intevalPolylines = setInterval(() => this.getPolyline(), 10000);
  }

  getRFIDBookings(isFirst: boolean = false) {

    if (isFirst) {
      this.isProcess.dialogOpen = true;
    }

    this.dashboardRFIDService.rfidBookings(this.ctrlProjectId.value).then((res) => {
      this.rfidBookings = res.data;
      this.rfidBookingSearch();
      // Kiểm tra marker nào dữ liệu trả về không có thì xóa bỏ đi
      this.location.rfidBookingMarkers.forEach(element => {
        var ktTonTai = this.rfidBookings.find(x => x.id == element.rfidBooking?.id);
        if (ktTonTai == null) {
          element.isEnable = false;
        }
      });

      if (this.currentRFIDBookingMarker != null && this.location.rfidBookingMarkers.find(x => this.currentRFIDBookingMarker?.rfidBooking?.id == x.rfidBooking?.id && x.isEnable == true) == null) {
        this.currentRFIDBookingMarker.isOpen = false;
        this.currentRFIDBookingMarker = null;
        this.polylines = [];
      }
      // Cập nhật marker
      this.rfidBookings.forEach(item => {
        this.addRFIDBookingMarker(item.bike?.dock?.lat, item.bike?.dock?.long, item.bike?.plate, this.iconBike, false, item, true, true);
      });

      this.isProcess.dialogOpen = false;
    });
  }

  selectStationMarker(marker: StationMarker) {
    if (!marker.isEnable) {
      marker.isOpen = true;
      return;
    }
    this.viewType = 1;
    this.polylines = [];
    if (this.currentStationMarker != null) {
      this.currentStationMarker.isOpen = false;
    }

    if (this.currentRFIDBookingMarker != null) {
      this.currentRFIDBookingMarker.isOpen = false;
    }
    this.currentRFIDBookingMarker = null;
    this.currentStationMarker = marker;
    this.location.latitude = marker.lat;
    this.location.longitude = marker.lng;
    marker.isOpen = true;
  }

  selectRFIDBookingMarker(marker: RFIDBookingMarker) {
    this.viewType = 0;
    if (this.currentStationMarker != null) {
      this.currentStationMarker.isOpen = false;
    }
 
    if (this.currentRFIDBookingMarker != null) {
      this.currentRFIDBookingMarker.isOpen = false;
    }

    this.currentStationMarker = null;
    this.currentRFIDBookingMarker = marker;
    this.location.latitude = marker.lat;
    this.location.longitude = marker.lng;
    marker.isOpen = true;
    this.getPolyline();
  }

  selectStation(station: Station) {
    var getMarker = this.location.stationMarkers.find(x => x.station.id == station.id);
    if (getMarker != null) {
      this.selectStationMarker(getMarker);
    }
  }

  openViewmapDialog(data: Bike) {
    this.dialog.open(DashboardBikesMapComponent, {
      data: data,
      panelClass: 'dialog-full',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
    });
  }

  openViewmapDialogById(bikeId: number) {
    // if (bikeId > 0) {
    //   this.dialog.open(DashboardBikesMapComponent, {
    //     data: { id: bikeId },
    //     panelClass: 'dialog-full',
    //     disableClose: true,
    //     autoFocus: false
    //   }).afterClosed().subscribe(outData => {
    //   });
    // }
  }

  selectRFIDBooking(rfidBooking: RFIDBooking) {
    if (rfidBooking != null) {
      var getMarker = this.location.rfidBookingMarkers.find(x => x.rfidBooking?.id == rfidBooking?.id);
      if (getMarker != null) {
        this.selectRFIDBookingMarker(getMarker);
      }
    }
  }

  addStationMarker(lat: number, lng: number, label: string, isOpen: boolean, station: Station, isEnable: boolean, isShow: boolean) {
    var isSelect = false;
    var ktTT = this.location.stationMarkers.find(x => x.station?.id == station.id);
    var iconUrl = this.setupStationIcon(station);
    if (ktTT == null) {
      this.location.stationMarkers.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        station,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.station = station;
      ktTT.isEnable = true;
      if (this.currentStationMarker != null && this.currentStationMarker.station?.id == station.id) {
        this.currentStationMarker = ktTT;
      }
    }
  }

  changeIconMarker() {
    this.location.stationMarkers.forEach(element => {
      var newX = {
        url: element.iconUrl.url,
        scaledSize: {
          width: this.iconSize,
          height: this.iconSize
        }
        , anchor: { x: 10, y: 10 }
      };
      element.iconUrl = newX;
    });

    this.location.rfidBookingMarkers.forEach(element => {
      var newX = {
        url: element.iconUrl.url,
        scaledSize: {
          width: this.iconSize,
          height: this.iconSize
        }, anchor: { x: 10, y: 10 }
      };
      element.iconUrl = newX;
    });
  }

  setupStationIcon(station: Station) {
    var data = this.iconStation_DuXe;
    if (station.totalBike <= 3 && station.totalBike > 0) {
      data = this.iconStation_ThieuXe;
    }
    else if (station.totalBike == 0) {
      data = this.iconStation_HetXe;
    }
    else if ((station.totalBike - 3) >= station.spaces) {
      data = this.iconStation_ThuaXe;
    }
    return data;
  }

  addRFIDBookingMarker(lat: number, lng: number, label: string, iconUrl: string, isOpen: boolean, rfidBooking: RFIDBooking, isEnable: boolean, isShow: boolean) {
    var isSelect = false;
    var ktTT = this.location.rfidBookingMarkers.find(x => x.rfidBooking?.id == rfidBooking.id);
    if (ktTT == null) {

      this.location.rfidBookingMarkers.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        rfidBooking,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.rfidBooking = rfidBooking;
      ktTT.isEnable = true;
      if (this.currentRFIDBookingMarker != null && this.currentRFIDBookingMarker.rfidBooking?.id == rfidBooking.id) {
        this.currentRFIDBookingMarker = ktTT;
      }
    }
  }

  centerChange(event: any) {
  }

  zoomChange(event: any) {
    this.location.zoom = event;
    this.initIconSize();
    this.changeIconMarker();
  }

  initIconSize() {
    if (this.location.zoom >= 15) {
      this.iconSize = 20;
    }
    else if (this.location.zoom == 14) {
      this.iconSize = 15;
    }
    else if (this.location.zoom == 13) {
      this.iconSize = 14;
    }
    else if (this.location.zoom == 12) {
      this.iconSize = 13;
    }
    else if (this.location.zoom == 11) {
      this.iconSize = 12;
    }
    else if (this.location.zoom == 10) {
      this.iconSize = 11;
    }
    else if (this.location.zoom == 9) {
      this.iconSize = 10;
    }
    else if (this.location.zoom == 8) {
      this.iconSize = 8;
    }
    else if (this.location.zoom == 7) {
      this.iconSize = 7;
    }
    else if (this.location.zoom == 6) {
      this.iconSize = 6;
    }
    else {
      this.iconSize = 5;
    }
  }

  boundsChange(event: any) {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - 510;
      this.heightListBikeRight = this.heightTable - 74;
    }
    if (this.fix_scroll_table_station.nativeElement != null) {
      this.heightTabTable = 406;
    }
  }

  matTabGroupChange() {
  }

  stopHub() {
    if (this.hubConnection) {
      this.hubConnection.stop();
      this.hubConnection = undefined;
    }
  }

  sendNotify(type: NotificationType, content: string) {
    var data = {
      type: type,
      title: "Thông báo",
      content: content,
      timeOut: 15000,
      showProgressBar: true,
      pauseOnHover: true,
      clickToClose: true,
      animate: 'fromRight'
    };
    this._notifications.create("Thông báo", content, type, data)
  }

  viewChange($event: boolean, type: number) {
    // if (type == 0) {
    //   this.location.stationMarkers.forEach(element => {
    //     element.isShow = $event;
    //   });
    // }
    // else if (type == 1) {
    //   this.location.bookingMarkers.forEach(element => {
    //     element.isShow = $event;
    //   });
    // }
  }

  stationSearchModelChange($event: number, type: number) {
    if (type == 0) {
      this.stationParams.investorId = $event;
    }
    else if (type == 1) {
      this.stationParams.cityId = $event;
    }
    else if (type == 2) {
      this.stationParams.projectId = $event;
    }
    this.stationSearch();
  }

  stationSearch() {
    var list = this.stations;
    if (this.stationParams.projectId != "" && this.stationParams.projectId != null) {
      list = this.stations.filter(x => x.projectId == this.stationParams.projectId);
    }
    if (this.stationParams.investorId != "" && this.stationParams.investorId != null) {
      list = this.stations.filter(x => x.investorId == this.stationParams.investorId);
    }
    if (this.stationParams.cityId != "" && this.stationParams.cityId != null) {
      list = this.stations.filter(x => x.cityId == this.stationParams.cityId);
    }
    if (this.stationParams.districtId != "" && this.stationParams.districtId != null) {
      list = this.stations.filter(x => x.districtId == this.stationParams.districtId);
    }
    if (this.stationParams.key != "" && this.stationParams.key != null) {
      list = this.stations.filter(x => x.name.toLowerCase().indexOf(this.stationParams.key.toLowerCase()) !== -1)
    }
    this.searchStations = list;
  }

  bookingSearchModelChange($event: number, type: number) {
    if (type == 0) {
      this.stationParams.investorId = $event;
    }
    else if (type == 1) {
      this.stationParams.plate = $event;
    }
    this.rfidBookingSearch();
  }

  rfidBookingSearch() {
    var list = this.rfidBookings;
    if (this.rfidBookingParams.investorId != "" && this.rfidBookingParams.investorId != null) {
      list = this.rfidBookings.filter(x => x.investorId == this.rfidBookingParams.investorId);
    }
    if (this.rfidBookingParams.plate != "" && this.rfidBookingParams.plate != null) {
      list = this.rfidBookings.filter(x => (x.bike.plate.replace("-","").replace(".","").replace(" ","").toLowerCase().indexOf(this.rfidBookingParams.plate.replace("-","").replace(".","").replace(" ","").toLowerCase()) !== -1) || (x.bike?.dock?.imei.toLowerCase().indexOf(this.rfidBookingParams.plate.toLowerCase()) !== -1));
    }
    if (this.rfidBookingParams.key != "" && this.rfidBookingParams.key != null) {
      list = this.rfidBookings.filter(x => x.transactionCode.toLowerCase().indexOf(this.rfidBookingParams.key.toLowerCase()) !== -1);
    }
    if (this.rfidBookingParams.type != "" && this.rfidBookingParams.type != null) {
      list = this.rfidBookings.filter(x => x.type == this.rfidBookingParams.type);
    }
    if (this.rfidBookingParams.accountKey != "" && this.rfidBookingParams.accountKey != null) {
      var k = this.rfidBookingParams.accountKey.toLowerCase();
      list = this.rfidBookings.filter(x => x.account?.fullName.toLowerCase().indexOf(k) !== -1 || x.account?.phone.toLowerCase().indexOf(k) !== -1);
      list = this.rfidBookings.filter(x => x.user?.phoneNumber?.toLowerCase().indexOf(k) !== -1 || x.user?.fullName?.toLowerCase().indexOf(k) !== -1);
    }
    this.searchRFIDBooking = list;
  }

  openLock() {
    if (this.currentRFIDBookingMarker?.rfidBooking != null) {
      this.dialog.open(OpenLockComponent, {
        data: this.currentRFIDBookingMarker.rfidBooking,
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
      });
    }
  }

  initSearchUser() {
    this.systemUserKey.valueChanges.subscribe(rs => {
      this.isProcess.userSearching = false;
      if (rs != "") {
        this.isProcess.userSearching = true;
        this.dashboardRFIDService.searchByUser({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            // this.logSystemParams.systemUserId = "";
          }
          this.users = rs.data;
          this.isProcess.userSearching = false;
        });
      }
    },
      error => {
        this.isProcess.userSearching = false;
      });
  }
 
  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('fix_scroll_table_station', { static: false }) fix_scroll_table_station: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}

interface RFIDBookingMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  isSelect: boolean;
  rfidBooking: RFIDBooking;
  isEnable: boolean;
  isShow: boolean;
}

interface StationMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: any;
  isOpen: boolean;
  isSelect: boolean;
  station: Station;
  isEnable: boolean;
  isShow: boolean;
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  stationMarkers: StationMarker[];
  rfidBookingMarkers: RFIDBookingMarker[];
}