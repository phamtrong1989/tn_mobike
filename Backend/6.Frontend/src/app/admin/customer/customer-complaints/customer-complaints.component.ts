import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppClientSettings, AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { CustomerComplaintsService } from './customer-complaints.service';
import { CustomerComplaint, CustomerComplaintCommand } from './customer-complaints.model';
import { CustomerComplaintsEditComponent } from './edit/customer-complaints-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';
import { FormControl } from '@angular/forms';
import { Account } from '../accounts/accounts.model';
import { CustomerComplaintDetailComponent } from './detail/customer-complaints-detail.component';
import { HttpEventType } from '@angular/common/http';
@Component({
  selector: 'app-customer-complaints',
  templateUrl: './customer-complaints.component.html',
  providers: [CustomerComplaintsService, AppCommon]
})

export class CustomerComplaintsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: CustomerComplaint[];
  isProcess = { dialogOpen: false, listDataTable: true, accountSearching: false };
  params: any = { pageIndex: 1, pageSize: 20, key: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "complaintInfo", "accountId", "transaction1", "description", "status", "processTime", "note", "action"];
  public settings: Settings;
  public categoryData: any;
  accountKey: FormControl = new FormControl();
  accounts: Account[];
  baseURL;

  constructor(
    public appSettings: AppSettings,
    public customerComplaintsService: CustomerComplaintsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    appClientSettings: AppClientSettings
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.baseURL = appClientSettings.settings.serverAPI;
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.initSearchAccount();
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.start = this.appCommon.formatDateTime(this.params.start, 'yyyy-MM-dd');
    this.params.end = this.appCommon.formatDateTime(this.params.end, 'yyyy-MM-dd');
    this.appCommon.changeUrl(location.pathname, this.params);
    this.customerComplaintsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
      if (res.outData != null) {
        this.accounts = [];
        this.accounts.push(res.outData);
        this.params.accountId = res.outData?.id;
      }
    });
  }

  export() {
    this.isProcess['export'] = true;

    var report_name = "BC_GiaiQuyetKhieuNai.xlsx";
    var params = this.params;

    params.from = this.params.start ? this.appCommon.formatDateTime(this.params.start, "yyyy-MM-dd") : null;
    params.to = this.params.end ? this.appCommon.formatDateTime(this.params.end, "yyyy-MM-dd") : null;

    this.customerComplaintsService.export(params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess['export'] = true;
          break;
        case HttpEventType.Response:
          this.isProcess['export'] = false;

          window.open(this.baseURL + "/" + data.body);
          // const downloadedFile = new Blob([data.body], { type: data.body.type });
          // const a = document.createElement('a');
          // a.setAttribute('style', 'display:none;');
          // document.body.appendChild(a);
          // a.download = report_name;
          // a.href = URL.createObjectURL(downloadedFile);
          // a.target = '_blank';
          // a.click();
          // document.body.removeChild(a);

          this.isProcess['export'] = false;
          break;
      }
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  getFilesCount(files: string) {
    try {
      if (files == null) {
        return 0;
      }
      var objects = JSON.parse(files);
      return objects.length;
    }
    catch
    {
      return 0;
    }
  }

  public openEditDialog(data: CustomerComplaint) {
    var model = new CustomerComplaintCommand();

    if (data == null) {
      model.id = 0;
    }
    else {
      model = new CustomerComplaintCommand().deserialize(data);
    }

    this.dialog.open(CustomerComplaintsEditComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  public openImageDialog(data: CustomerComplaint) {

    this.dialog.open(CustomerComplaintDetailComponent, {
      data: data,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => { });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
      this.isProcess.accountSearching = false;
      if (rs != "") {
        this.isProcess.accountSearching = true;
        this.customerComplaintsService.searchByAccount({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.params.accountId = "";
          }
          this.accounts = rs.data;
          this.isProcess.accountSearching = false;
        });
      }
    },
      error => {
        this.isProcess.accountSearching = false;
      });
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}