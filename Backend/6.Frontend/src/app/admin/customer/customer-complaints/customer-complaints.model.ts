import { Transaction } from "../../transaction/transactions/transactions.model";
import { Account } from "../accounts/accounts.model";

export class CustomerComplaint {
  id: number;
  accountId: number;
  transactionCode: string;
  bikeId: number;
  bikeReportType: number;
  description: string;
  note: string;
  status: number;
  createdDate: Date;
  createdUser: number;
  updatedDate: Date;
  updatedUser: number;
  files: string;
  complaintDate: Date;
  startDate: Date;
  endDate: Date;
  pattern: number;

  account: Account;
  transaction: Transaction;
  bikeReportTypeText: string;
}

export class CustomerComplaintCommand {
  id: number;
  accountId: number;
  transactionCode: string;
  bikeId: number;
  bikeReportType: number;
  description: string;
  note: string;
  status: number;
  files: string;
  complaintDate: Date;
  startDate: Date;
  endDate: Date;
  pattern: number;

  account: Account;

  deserialize(input: CustomerComplaint): CustomerComplaintCommand {
    if (input == null) {
      input = new CustomerComplaint();
    }

    this.id = input.id;
    this.accountId = input.accountId;
    this.transactionCode = input.transactionCode;
    this.bikeId = input.bikeId;
    this.bikeReportType = input.bikeReportType;
    this.description = input.description;
    this.note = input.note;
    this.status = input.status;
    this.files = input.files;
    this.complaintDate = input.complaintDate;
    this.startDate = input.startDate;
    this.endDate = input.endDate;
    this.pattern = input.pattern;

    this.account = input.account;

    return this;
  }
}