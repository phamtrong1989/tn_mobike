import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomerComplaintCommand } from '../customer-complaints.model';
import { CustomerComplaintsService } from '../customer-complaints.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { Account } from '../../accounts/accounts.model';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-customer-complaints-edit',
  templateUrl: './customer-complaints-edit.component.html',
  providers: [
    CustomerComplaintsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class CustomerComplaintsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountSearching: false };
  accounts: Account[];
  accountKey: FormControl = new FormControl();
  uploadFileOut: FileDataDTO[];

  constructor(
    public dialogRef: MatDialogRef<CustomerComplaintsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: CustomerComplaintCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public customerComplaintsService: CustomerComplaintsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      accountId: [null, Validators.compose([Validators.required])],
      transactionCode: [null],
      pattern: [null, Validators.compose([Validators.required])],
      bikeId: [0],
      bikeReportType: [null, Validators.compose([Validators.required])],
      description: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(2000)])],
      note: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(2000)])],
      status: [0],
      files: [null],
      complaintDate: [new Date(), Validators.compose([Validators.required])],
      startDate: [new Date()],
      endDate: [new Date()],
      account: [null]
    });
    this.accounts = [];
  }

  ngOnInit() {
    if (this.data.account != null) {
      this.accounts.push(this.data.account);
    }

    if (this.data.files != null) {
      this.uploadFileOut = JSON.parse(this.data.files)
    }

    this.initSearchAccount();
    if (this.data.id > 0) {
      this.form.setValue(this.data);
      if (this.data.files != null) {
        this.uploadFileOut = JSON.parse(this.data.files)
      }
      this.getData();
    }
  }

  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
      this.isProcess.accountSearching = false;
      if (rs != "") {
        this.isProcess.accountSearching = true;
        this.customerComplaintsService.searchByAccount({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('accountId').setValue(null);
          }
          this.accounts = rs.data;
          this.isProcess.accountSearching = false;
        });
      }
    },
      error => {
        this.isProcess.accountSearching = false;
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.customerComplaintsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new CustomerComplaintCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        if (this.data.account != null) {
          this.accounts.push(this.data.account);
        }

        if (this.data.files != null) {
          this.uploadFileOut = JSON.parse(this.data.files)
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save(status: any) {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
      if (this.data.id > 0) {
        this.customerComplaintsService.edit(this.form.value, status).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.close(res)
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
      else {
        this.customerComplaintsService.create(this.form.value, 0).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
            this.data = new CustomerComplaintCommand().deserialize(res.data);
            this.form.setValue(this.data);
          }
          else {
            this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
          }
          this.isProcess.saveProcess = false;
        });
      }
    }
    else {
      console.log(this.form.validator)
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.customerComplaintsService.delete(this.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
          }
          else if (res.status == 2) {
            this.snackBar.open("Khiếu nại này đã xử lý không thể xóa", this.baseTranslate["msg-warning-type"], { duration: 5000 });
          }
          this.close(this.data)
        });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
