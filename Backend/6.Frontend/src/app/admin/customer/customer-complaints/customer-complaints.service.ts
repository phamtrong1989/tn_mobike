import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpParams, HttpRequest } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { CustomerComplaint, CustomerComplaintCommand } from './customer-complaints.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { Account } from '../accounts/accounts.model';
import { Observable } from 'rxjs';

@Injectable()
export class CustomerComplaintsService {
    public url = "/api/work/CustomerComplaints";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<CustomerComplaint[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: CustomerComplaintCommand, status: any) {
        return this.http.post<ApiResponseData<CustomerComplaint>>(this.url + "/create?status=" + status, user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: CustomerComplaintCommand, status: any) {
        return this.http.put<ApiResponseData<CustomerComplaint>>(this.url + "/edit?status=" + status, user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<CustomerComplaint>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    delete(id: number) {
        return this.http.delete<ApiResponseData<CustomerComplaint>>(this.url + "/delete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByAccount(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchByAccount", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    uploadPath() {
        return this.url + "/Upload";
    }

    apiUrl() {
        return this.clientSettings.serverAPI;
    }

    // export(params: any): Observable<HttpEvent<Blob>> {
    //     let obj = {};
    //     for (const prop in params) {
    //         if (params[prop] != null) {
    //             obj[prop] = params[prop];
    //         }
    //     }

    //     return this.http.request(new HttpRequest(
    //         'POST',
    //         this.url + "/report",
    //         null,
    //         {
    //             reportProgress: true,
    //             responseType: 'blob',
    //             params: new HttpParams({ fromObject: obj })
    //         }));
    // }

    export(params: any): any {
        let obj = {};
        for (const prop in params) {
            if (params[prop] != null) {
                obj[prop] = params[prop];
            }
        }

        return this.http.request(new HttpRequest(
            'POST',
            this.url + "/report",
            null,
            {
                reportProgress: true,
                responseType: 'text',
                params: new HttpParams({ fromObject: obj })
            }));
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {
        } else {
        }
        return Promise.reject(error);
    }
}