import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { CustomerComplaintsComponent } from './customer-complaints.component';
import { CustomerComplaintsEditComponent } from './edit/customer-complaints-edit.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { CustomerComplaintDetailComponent } from './detail/customer-complaints-detail.component';

export const routes = [
  { path: '', component: CustomerComplaintsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
	 TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],

      }
    }) 
  ],
  declarations: [
    CustomerComplaintsComponent,
    CustomerComplaintsEditComponent,
    CustomerComplaintDetailComponent
  ],
  entryComponents:[
    CustomerComplaintsEditComponent,
    CustomerComplaintDetailComponent
  ]
})
export class CustomerComplaintsModule { }
