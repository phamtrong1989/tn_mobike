import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { AppCommon } from '../../../../app.common';
import { CustomerComplaint } from '../customer-complaints.model';
import { CustomerComplaintsService } from '../customer-complaints.service';

@Component({
  selector: 'app-customer-complaints-detail-1',
  templateUrl: './customer-complaints-detail.component.html',
  providers: [
    CustomerComplaintsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class CustomerComplaintDetailComponent {
  public imageUrl: string;

  constructor(
    public dialogRef: MatDialogRef<CustomerComplaintDetailComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: CustomerComplaint,
    public customerComplaintsService: CustomerComplaintsService,
  ) {
    this.imageUrl = this.data.files;
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}