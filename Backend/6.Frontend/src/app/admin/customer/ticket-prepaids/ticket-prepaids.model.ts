import { CustomerGroup, TicketPrice } from "../../category/projects/projects.model";
import { User } from "../../user/users/users.model";
import { Account } from "../accounts/accounts.model";

export class TicketPrepaid {

  id: number;
  accountId: number;
  customerGroupId: number;
  startDate: Date;
  endDate: Date;
  ticketValue: number;
  ticketPriceId: number;
  dateOfPayment: Date;
  createdDate: Date;
  createdUser: number;
  projectId: number;
  updatedDate: Date;
  updatedUser: number;
  code: string;
  ticketPrice_AllowChargeInSubAccount: boolean;
  ticketPrice_BlockPerMinute: number;
  ticketPrice_BlockPerViolation: number;
  ticketPrice_BlockViolationValue: number;
  ticketPrice_IsDefault: boolean;
  ticketPrice_LimitMinutes: number;
  ticketPrice_TicketType: number;
  ticketPrice_TicketValue: number;
  wallet_AccountBlance: number;
  wallet_SubAccountBlance: number;
  chargeInAccount: number;
  chargeInSubAccount: number;
  note: string;
  ticketTypeNote: string;
  customerGroup: CustomerGroup;
  account: Account;
  createdUserObject: User;
  updatedUserObject: User;
  ticketPrice: TicketPrice;
}

export class TicketPrepaidCommand {

  id: number;
  accountId: number;
  customerGroupId: number;
  startDate: Date;
  ticketValue: number;
  ticketPriceId: number;
  dateOfPayment: Date;
  createdDate: Date;
  createdUser: number;
  projectId: number;
  updatedDate: Date;
  updatedUser: number;
  code: string;
  note: string;
  month: number;
  year: number;

  deserialize(input: TicketPrepaid): TicketPrepaidCommand {
    if (input == null) {
      input = new TicketPrepaid();
    }
    this.id = input.id;
    this.accountId = input.accountId;
    this.customerGroupId = input.customerGroupId;
    this.startDate = input.startDate;
    this.ticketValue = input.ticketValue;
    this.ticketPriceId = input.ticketPriceId;
    this.dateOfPayment = input.dateOfPayment;
    this.createdDate = input.createdDate;
    this.createdUser = input.createdUser;
    this.projectId = input.projectId;
    this.updatedDate = input.updatedDate;
    this.updatedUser = input.updatedUser;
    this.code = input.code;
    this.note = input.note;
    this.year = this.startDate.getFullYear();
    this.month = this.startDate.getMonth();
    return this;
  }
}

export class TicketPrepaidEditCommand {
  id: number;
  startDate: Date;
  dateOfPayment: Date;
  note: string;
  month: number;
  year: number;
  
  deserialize(input: TicketPrepaid): TicketPrepaidEditCommand {
    if (input == null) {
      input = new TicketPrepaid();
    }
    this.id = input.id;
    this.startDate = input.startDate;
    this.dateOfPayment = input.dateOfPayment;
    this.note = input.note;
    return this;
  }
}