import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEvent, HttpRequest, HttpParams } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { TicketPrepaid, TicketPrepaidCommand } from './ticket-prepaids.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { Account } from '../accounts/accounts.model';
import { CustomerGroup } from '../../category/projects/projects.model';
import { Observable } from 'rxjs';

@Injectable()
export class TicketPrepaidsService {
    public url = "/api/work/TicketPrepaids";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<TicketPrepaid[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: TicketPrepaidCommand) {
        return this.http.post<ApiResponseData<TicketPrepaid>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: TicketPrepaidCommand) {
        return this.http.put<ApiResponseData<TicketPrepaid>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<TicketPrepaid>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }
    
    delete(id: number) {
        return this.http.delete<ApiResponseData<TicketPrepaid>>(this.url + "/delete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByAccount(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchByAccount", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchTicketPriceByProject(params: any) {
        return this.http.get<ApiResponseData<CustomerGroup[]>>(this.url + "/SearchTicketPriceByProject", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    // export(params: any): Observable<HttpEvent<Blob>> {
    //     let obj = {};
    //     for (const prop in params) {
    //         if (params[prop] != null) {
    //             obj[prop] = params[prop];
    //         }
    //     }

    //     return this.http.request(new HttpRequest(
    //         'POST',
    //         this.url + "/report",
    //         null,
    //         {
    //             reportProgress: true,
    //             responseType: 'blob',
    //             params: new HttpParams({ fromObject: obj })
    //         }));
    // }

    export(params: any): any {
        let obj = {};
        for (const prop in params) {
            if (params[prop] != null) {
                obj[prop] = params[prop];
            }
        }

        return this.http.request(new HttpRequest(
            'POST',
            this.url + "/report",
            null,
            {
                reportProgress: true,
                responseType: 'text',
                params: new HttpParams({ fromObject: obj })
            }));
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}