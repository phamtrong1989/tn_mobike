import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TicketPrepaidCommand } from '../ticket-prepaids.model';
import { TicketPrepaidsService } from '../ticket-prepaids.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { Account } from '../../accounts/accounts.model';
import { CustomerGroup, TicketPrice } from 'src/app/admin/category/projects/projects.model';

@Component({
  selector: 'app-ticket-prepaids-create',
  templateUrl: './ticket-prepaids-create.component.html',
  providers: [
    TicketPrepaidsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class TicketPrepaidsCreateComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountSearching: false, ticketPriceSeaching: false };
  accounts: Account[];
  accountKey: FormControl = new FormControl("");
  customerGroups: CustomerGroup[];
  ticketPrices: TicketPrice[];
  currentTicketPrice: TicketPrice;
  expressNote: string;
  years: number[] = [];
  constructor(
    public dialogRef: MatDialogRef<TicketPrepaidsCreateComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: TicketPrepaidCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public ticketPrepaidsService: TicketPrepaidsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      accountId: [null, Validators.compose([Validators.required])],
      customerGroupId: [0],
      startDate: [new Date(), Validators.compose([Validators.required])],
      ticketValue: [null, Validators.compose([Validators.required])],
      ticketPriceId: [null, Validators.compose([Validators.required])],
      dateOfPayment: [new Date(), Validators.compose([Validators.required])],
      projectId: [null, Validators.compose([Validators.required])],
      code: ["Tự sinh khi thêm"],
      note: [null],
      noteTicket: [null],
      month: [1],
      year: [2021]
    });

    this.customerGroups = [];
    this.ticketPrices = [];
    this.currentTicketPrice = new TicketPrice();
    this.form.get("noteTicket").disable();
    this.form.get("code").disable();
    this.form.get("ticketValue").disable();
    this.form.get("month").setValue((new Date().getMonth()));
    this.form.get("year").setValue((new Date().getFullYear()));

    for (let index = 2021; index <= (new Date()).getFullYear(); index++) {
      this.years.push(index);
    }
  }

  ngOnInit() {
    this.initSearchAccount();
    this.initBindTicketType();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.ticketPrepaidsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {

        this.data = new TicketPrepaidCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.ticketPrepaidsService.create(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
          this.close(1);
        }
        else {
          this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
      this.isProcess.accountSearching = false;
      if (rs != "") {
        this.isProcess.accountSearching = true;
        this.ticketPrepaidsService.searchByAccount({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('accountId').setValue(null);
          }
          this.accounts = rs.data;
          this.isProcess.accountSearching = false;
        });
      }
    },
      error => {
        this.isProcess.accountSearching = false;
      });
  }

  fGetTicketPrice() {
    if (this.form.get("projectId").value > 0 && this.form.get("accountId").value > 0) {
      this.isProcess.ticketPriceSeaching = true;
      this.ticketPrepaidsService.searchTicketPriceByProject({ projectId: this.form.get("projectId").value, accountId: this.form.get("accountId").value }).then(rs => {
        if (rs.data.length == 0) {
          this.form.get('ticketPriceId').setValue(null);
        }
        this.customerGroups = rs.data;
        this.customerGroups.forEach(element => {
          element.ticketPrices.forEach(e => {
            this.ticketPrices.push(e)
          });
        });
        this.isProcess.ticketPriceSeaching = false;
      });
    }
  }

  
  initNoteExp()
  {
    if(this.currentTicketPrice)
    {
      if (this.currentTicketPrice.ticketType == 2) {
        this.expressNote = "Sử dụng trong ngày " +  this.appCommon.formatDateTime(this.form.get("startDate").value, 'dd/MM/yyyy');
      }
      else if (this.currentTicketPrice.ticketType == 3) {
        var startDay = new Date(this.form.get("year").value +"-" + this.form.get("month").value + "-1");
        var totalDay =this.appCommon.daysInMonth(this.form.get("year").value,this.form.get("month").value);
        var endDay = new Date(this.form.get("year").value +"-" + this.form.get("month").value + "-" +totalDay) ;
        this.expressNote = "Từ ngày " + this.appCommon.formatDateTime(startDay, 'dd/MM/yyyy') + " đến hết ngày " +  this.appCommon.formatDateTime(endDay, 'dd/MM/yyyy');
      }
    }
  }

  initBindTicketType() {
    this.form.get('ticketPriceId').valueChanges.subscribe(rs => {
      var ktNote = this.ticketPrices.find(x => x.id == rs);
      this.currentTicketPrice = ktNote;
      if (ktNote == null) {
        this.form.get('noteTicket').setValue("");
        this.form.get('ticketValue').setValue(0);
      }
      else {
        this.form.get('noteTicket').setValue(ktNote.note);
        this.form.get('ticketValue').setValue(ktNote.ticketValue);
      }
      this.initNoteExp();
    }, error => { });

    this.form.get('projectId').valueChanges.subscribe(rs => {
      this.isProcess.ticketPriceSeaching = true;
      this.fGetTicketPrice();
    },
      error => {
        this.isProcess.ticketPriceSeaching = false;
      });

    this.form.get('accountId').valueChanges.subscribe(rs => {
      this.isProcess.ticketPriceSeaching = true;
      this.fGetTicketPrice();
    },
      error => {
        this.isProcess.ticketPriceSeaching = false;
   });

   this.form.get('month').valueChanges.subscribe(rs => {
    this.initNoteExp();
  },
    error => {
      this.isProcess.ticketPriceSeaching = false;
    });

    this.form.get('year').valueChanges.subscribe(rs => {
      this.initNoteExp();
    },
      error => {
        this.isProcess.ticketPriceSeaching = false;
      });

      this.form.get('startDate').valueChanges.subscribe(rs => {
        this.initNoteExp();
      },
        error => {
          this.isProcess.ticketPriceSeaching = false;
        });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
