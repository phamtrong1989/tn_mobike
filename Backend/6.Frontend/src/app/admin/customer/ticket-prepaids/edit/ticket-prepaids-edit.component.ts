import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TicketPrepaid, TicketPrepaidCommand, TicketPrepaidEditCommand } from '../ticket-prepaids.model';
import { TicketPrepaidsService } from '../ticket-prepaids.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { Account } from '../../accounts/accounts.model';
import { TicketPrice } from 'src/app/admin/category/projects/projects.model';

@Component({
  selector: 'app-ticket-prepaids-edit',
  templateUrl: './ticket-prepaids-edit.component.html',
  providers: [
    TicketPrepaidsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class TicketPrepaidsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountSearching: false, ticketPriceSeaching: false };
  currentTicketPrice: TicketPrice;
  accounts: Account[];
  expressNote: string = "";
  constructor(
    public dialogRef: MatDialogRef<TicketPrepaidsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { dataEdit: TicketPrepaidEditCommand, data: TicketPrepaid },
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public ticketPrepaidsService: TicketPrepaidsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      dateOfPayment: [new Date(), Validators.compose([Validators.required])],
      note: [null],
      startDate: [null]
    });
  }

  ngOnInit() {
    this.accounts = [];
    if(this.data.data.account!=null)
    {
      this.accounts.push(this.data.data.account);
    }
    this.form.setValue(this.data.dataEdit);
    this.getData();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.ticketPrepaidsService.getById(this.data.dataEdit.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data.dataEdit = new TicketPrepaidEditCommand().deserialize(rs.data);
        this.data.data = rs.data;
        this.accounts = [];
        if(this.data.data.account!=null)
        {
          this.accounts.push(this.data.data.account);
        }
        this.form.setValue(this.data.dataEdit);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.ticketPrepaidsService.edit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
          this.close(res)
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
        this.isProcess.deleteProcess = true;
        this.ticketPrepaidsService.delete(this.data.data.id).then((res) => {
          if (res.status == 1) {
            this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
            this.isProcess.deleteProcess = false;
            this.close(this.data)
          }
          else if (res.status == 0) {
            this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
            this.close(this.data)
          }
        });
      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
