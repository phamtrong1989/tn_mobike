import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppClientSettings, AppSettings } from '../../../app.settings';
import { Settings, ApiResponseData } from '../../../app.settings.model';
import { MatPaginator } from '@angular/material/paginator';
import { TicketPrepaidsService } from './ticket-prepaids.service';
import { TicketPrepaid, TicketPrepaidCommand, TicketPrepaidEditCommand } from './ticket-prepaids.model';
import { TicketPrepaidsEditComponent } from './edit/ticket-prepaids-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';
import { TicketPrepaidsCreateComponent } from './create/ticket-prepaids-create.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { Account } from '../accounts/accounts.model';
import { FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatSort } from '@angular/material/sort';
import { HttpEventType } from '@angular/common/http';
@Component({
  selector: 'app-ticket-prepaids',
  templateUrl: './ticket-prepaids.component.html',
  providers: [
    TicketPrepaidsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class TicketPrepaidsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: TicketPrepaid[];
  isProcess = { dialogOpen: false, listDataTable: true, accountSearching: false, export: false  };
  params: any = { pageIndex: 1, pageSize: 20, key: "", projectId: "", ticketPrice_TicketType: "", sellStart: "", sellEnd: "", accountId: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "code", "accountId", "startDate", "ticketValue", "exp","dateOfPayment", "customerGroupId", "projectId", "createdDate"];
  public settings: Settings;
  public categoryData: any;
  accounts: Account[];
  accountKey: FormControl = new FormControl("");
  baseURL;

  constructor(
    public appSettings: AppSettings,
    public ticketPrepaidsService: TicketPrepaidsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    appClientSettings: AppClientSettings
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.baseURL = appClientSettings.settings.serverAPI;
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.initSearchAccount();
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;

    this.params.sellStart = this.appCommon.formatDateTime(this.params.sellStart, 'yyyy-MM-dd');
    this.params.sellEnd = this.appCommon.formatDateTime(this.params.sellEnd, 'yyyy-MM-dd');
   
    this.appCommon.changeUrl(location.pathname, this.params);
    this.ticketPrepaidsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
      if(res.outData != null)
      {
        this.accounts = [];
        this.accounts.push(res.outData);
        this.params.accountId = res.outData?.id;
      }
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }
  
  export() {
    this.isProcess['export'] = true;

    var report_name = "BC_Ve.xlsx";
    var params = this.params;

    params.sellStart = this.params.sellStart ? this.appCommon.formatDateTime(this.params.sellStart, "yyyy-MM-dd") : null;
    params.sellEnd = this.params.sellEnd ? this.appCommon.formatDateTime(this.params.sellEnd, "yyyy-MM-dd") : null;

    this.ticketPrepaidsService.export(params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess['export'] = true;
          break;
        case HttpEventType.Response:
          this.isProcess['export'] = false;

          window.open(this.baseURL + "/" + data.body);
          // const downloadedFile = new Blob([data.body], { type: data.body.type });
          // const a = document.createElement('a');
          // a.setAttribute('style', 'display:none;');
          // document.body.appendChild(a);
          // a.download = report_name;
          // a.href = URL.createObjectURL(downloadedFile);
          // a.target = '_blank';
          // a.click();
          // document.body.removeChild(a);

          this.isProcess['export'] = false;
          break;
      }
    });
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: TicketPrepaid) {
    var model = new TicketPrepaidCommand();
    if (data == null) {
      model.id = 0;
      this.dialog.open(TicketPrepaidsCreateComponent, {
        data: model,
        panelClass: 'dialog-default',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.searchPage();
      });
    }
    else {
      var dataEdit = new TicketPrepaidEditCommand().deserialize(data);
      this.dialog.open(TicketPrepaidsEditComponent, {
        data: { dataEdit, data },
        panelClass: 'dialog-default',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.searchPage();
      });
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
        this.isProcess.accountSearching = false;
        if (rs != "") {
          this.isProcess.accountSearching = true;
          this.ticketPrepaidsService.searchByAccount({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.params.accountId = "";
            }
            this.accounts = rs.data;
            this.isProcess.accountSearching = false;
          });
        }
      },
        error => {
          this.isProcess.accountSearching = false;
      });
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}