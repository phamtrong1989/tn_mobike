import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { AccountsService } from '../accounts.service';
import { Account, GiftCardGetDataModel, RedeemPoint } from '../accounts.model';

@Component({
  selector: 'app-account-add-gift-card',
  templateUrl: './add-gift-card.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class AccountAddGiftCardComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, redeemPoint: false };
  listData: GiftCardGetDataModel;
  currentVoucherCode: number;
  currentRedeemPoint: number;
  constructor(
    public dialogRef: MatDialogRef<AccountAddGiftCardComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: Account,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountsService: AccountsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {

    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      code: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1000)])],
      accountId: [0],
    });
    this.form.get('accountId').setValue(this.data.id);
    this.listData = new GiftCardGetDataModel();
    this.listData.redeemPoints = [];
    this.listData.totalTripPoint = 0;
    this.listData.voucherCodes = [];
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isProcess.initData = true;
    this.accountsService.giftCardGetData(this.data.id).then((res) => {
      if (res.status == 1) {
        this.listData = res.data;
      }
      else {
        this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
      }
      this.isProcess.initData = false;
    });
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  onSelectRedeemPoint(item: RedeemPoint) {
    item.isProcess = true;
    setTimeout(() => {
      this.accountsService.redeemPointAdd(item.id, this.form.get("accountId").value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        else {
          this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        item.isProcess = false;
        this.getData();
      });
    }, 1000);
  }

  save() {
    this.isProcess.saveProcess = true;
    setTimeout(() => {
      this.accountsService.addGiftCard(this.form.get("code").value, this.form.get("accountId").value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        else {
          this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
        this.getData();
      });
    }, 1000);
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
