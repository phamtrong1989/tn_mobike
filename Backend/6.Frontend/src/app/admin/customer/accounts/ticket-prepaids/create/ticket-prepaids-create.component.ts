import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { CustomerGroup, TicketPrice } from 'src/app/admin/category/projects/projects.model';
import { AppCommon } from 'src/app/app.common';
import { TicketPrepaidCommand } from '../../../ticket-prepaids/ticket-prepaids.model';
import { Account } from '../../accounts.model';
import { AppStorage } from 'src/app/app.storage';
import { AccountsService } from '../../accounts.service';

@Component({
  selector: 'app-account-ticket-prepaids-create',
  templateUrl: './ticket-prepaids-create.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class AccountTicketPrepaidsCreateComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountSearching: false, ticketPriceSeaching: false };
  accounts: Account[];
  accountKey: FormControl = new FormControl("");
  customerGroups: CustomerGroup[];
  ticketPrices: TicketPrice[];
  currentTicketPrice: TicketPrice;
  constructor(
    public dialogRef: MatDialogRef<AccountTicketPrepaidsCreateComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: TicketPrepaidCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountService: AccountsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      accountId: [null, Validators.compose([Validators.required])],
      customerGroupId: [0],
      startDate: [new Date(), Validators.compose([Validators.required])],
      endDate: [new Date(), Validators.compose([Validators.required])],
      ticketValue: [null, Validators.compose([Validators.required])],
      ticketPriceId: [null, Validators.compose([Validators.required])],
      dateOfPayment: [new Date(), Validators.compose([Validators.required])],
      projectId: [null, Validators.compose([Validators.required])],
      code: ["Tự sinh khi thêm"],
      note: [null],
      noteTicket: [null]
    });
    this.customerGroups = [];
    this.ticketPrices = [];
    this.currentTicketPrice = new TicketPrice();
    this.form.get("noteTicket").disable();
    this.form.get("code").disable();
    this.form.get("ticketValue").disable();
    this.form.get("endDate").disable();
    this.form.get("startDate").disable();
    this.form.get('accountId').setValue(this.data.accountId);
  }

  ngOnInit() {
    this.initBindTicketType();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.accountService.ticketPrepaidCreate(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
          this.close(1);
        }
        else {
          this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  fGetTicketPrice() {
    
    if (this.form.get("projectId").value > 0 && this.form.get("accountId").value > 0) {
      this.isProcess.ticketPriceSeaching = true;
 
      this.accountService.searchTicketPriceByProject({ projectId: this.form.get("projectId").value, accountId: this.form.get("accountId").value }).then(rs => {
        if (rs.data.length == 0) {
          this.form.get('ticketPriceId').setValue(null);
        }
        this.customerGroups = rs.data;
        this.customerGroups.forEach(element => {
          element.ticketPrices.forEach(e => {
            this.ticketPrices.push(e)
          });
        });
        this.isProcess.ticketPriceSeaching = false;
      });
    }
  }

  initBindTicketType() {

    this.form.get('startDate').valueChanges.subscribe(rs => {
      if (this.currentTicketPrice != null) {
        if (this.currentTicketPrice.ticketType == 2) {
          this.form.get("endDate").setValue(new Date(this.appCommon.formatDateTime(this.form.get("startDate").value, 'yyyy/MM/dd')));
        }
        else if (this.currentTicketPrice.ticketType == 3) {
          this.form.get("endDate").setValue(this.appCommon.dateAddDay(this.form.get("startDate").value, 30));
        }
      }
    }, error => { });

    this.form.get('ticketPriceId').valueChanges.subscribe(rs => {
      var ktNote = this.ticketPrices.find(x => x.id == rs);
      this.currentTicketPrice = ktNote;
      if (ktNote == null) {
        this.form.get('noteTicket').setValue("");
        this.form.get('ticketValue').setValue(0);
      }
      else {
        this.form.get('noteTicket').setValue(ktNote.note);
        this.form.get('ticketValue').setValue(ktNote.ticketValue);

        if (ktNote.ticketType == 2) {
          this.form.get("endDate").setValue(new Date(this.appCommon.formatDateTime(this.form.get("startDate").value, 'yyyy/MM/dd')));
        }
        else if (ktNote.ticketType == 3) {
          this.form.get("endDate").setValue(this.appCommon.dateAddDay(this.form.get("startDate").value, 30));
        }
      }
    }, error => { });

    this.form.get('projectId').valueChanges.subscribe(rs => {
      this.isProcess.ticketPriceSeaching = true;
     
      this.fGetTicketPrice();
      },
      error => {
        this.isProcess.ticketPriceSeaching = false;
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
