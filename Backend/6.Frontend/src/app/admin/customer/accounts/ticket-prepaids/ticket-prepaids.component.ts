import { Component, ViewChild, HostListener, OnInit, ElementRef, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { AccountsService } from '../accounts.service';
import { AppCommon } from 'src/app/app.common';
import { TicketPrepaid, TicketPrepaidCommand, TicketPrepaidEditCommand } from '../../ticket-prepaids/ticket-prepaids.model';
import { Account } from '../accounts.model';
import { AppSettings } from 'src/app/app.settings';
import { AppStorage } from 'src/app/app.storage';
import { Settings } from 'src/app/app.settings.model';
import { AccountTicketPrepaidsCreateComponent } from './create/ticket-prepaids-create.component';
import { AccountTicketPrepaidsEditComponent } from './edit/ticket-prepaids-edit.component';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatSort } from '@angular/material/sort';
@Component({
  selector: 'app-accounts-ticket-prepaids',
  templateUrl: './ticket-prepaids.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class AccountTicketPrepaidsComponent implements OnInit {
  @Input() account: Account;
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: TicketPrepaid[];
  isProcess = { dialogOpen: false, listDataTable: true, accountSearching: false };
  params: any = { pageIndex: 1, pageSize: 20, key: "", projectId: "", ticketPrice_TicketType: "", accountId: "", sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "code", "startDate", "ticketValue", "dateOfPayment", "ticketTypeNote", "customerGroupId", "projectId", "createdDate", "updatedDate", "action"];
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public accountsService: AccountsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params.accountId = this.account.id;
  }

  firstSearch() {
    this.fixHeightTable();
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.accountsService.ticketPrepaidSearchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
      if (res.outData != null) {
        this.params.accountId = res.outData?.id;
      }
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  openEditDialog(data: TicketPrepaid) {
    var model = new TicketPrepaidCommand();
    if (data == null) {
      model.id = 0;
      model.accountId = this.account.id;
      this.dialog.open(AccountTicketPrepaidsCreateComponent, {
        data: model,
        panelClass: 'dialog-default',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.search();
      });
    }
    else {
      var dataEdit = new TicketPrepaidEditCommand().deserialize(data);
      this.dialog.open(AccountTicketPrepaidsEditComponent, {
        data: { dataEdit, data },
        panelClass: 'dialog-default',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.search();
      });
    }
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null && this.fix_scroll_table_rec != undefined) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 30;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;
  @ViewChild('headTable_rec', { static: false }) headTable_rec: ElementRef;
  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable();
  }
}