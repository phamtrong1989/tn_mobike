import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AccountsService } from '../../accounts.service';
import { AppCommon } from 'src/app/app.common';
import { TicketPrepaid, TicketPrepaidEditCommand } from '../../../ticket-prepaids/ticket-prepaids.model';
import { AppStorage } from 'src/app/app.storage';
import { TicketPrice } from 'src/app/admin/category/projects/projects.model';

@Component({
  selector: 'app-account-ticket-prepaids-edit',
  templateUrl: './ticket-prepaids-edit.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class AccountTicketPrepaidsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountSearching: false, ticketPriceSeaching: false };
  currentTicketPrice: TicketPrice;
  constructor(
    public dialogRef: MatDialogRef<AccountTicketPrepaidsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { dataEdit: TicketPrepaidEditCommand, data: TicketPrepaid },
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountsService: AccountsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      startDate: [new Date(), Validators.compose([Validators.required])],
      endDate: [new Date(), Validators.compose([Validators.required])],
      dateOfPayment: [new Date(), Validators.compose([Validators.required])],
      note: [null]
    });
    this.form.get('endDate').disable();
    this.form.get("startDate").disable();
  }

  ngOnInit() {
    this.form.setValue(this.data.dataEdit);
    this.getData();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.accountsService.ticketPrepaidGetById(this.data.dataEdit.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data.dataEdit = new TicketPrepaidEditCommand().deserialize(rs.data);
        this.data.data = rs.data;
        this.form.setValue(this.data.dataEdit);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
      this.initBindTicketType();
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.accountsService.ticketPrepaidEdit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
          this.close(res)
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  initBindTicketType() {
    this.form.get('startDate').valueChanges.subscribe(rs => {
      if (this.data.data.ticketPrice != null) {
        if (this.data.data.ticketPrice.ticketType == 2) {
          this.form.get("endDate").setValue(new Date(this.appCommon.formatDateTime(this.form.get("startDate").value, 'yyyy/MM/dd')));
        }
        else if (this.data.data.ticketPrice.ticketType == 3) {
          this.form.get("endDate").setValue(this.appCommon.dateAddDay(this.form.get("startDate").value, 30));
        }
      }
    }, error => { });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
