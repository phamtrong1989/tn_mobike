import { Campaign, VoucherCode } from "../../campaigns/campaigns.model";
import { CustomerGroup, Project, ProjectAccount } from "../../category/projects/projects.model";
import { Bike } from "../../infrastructure/bikes/bikes.model";
import { User } from "../../user/users/users.model";
import { TicketPrepaid } from "../ticket-prepaids/ticket-prepaids.model";

export class Account {

  id: number;
  address: string;
  rfid: string;
  avatar: string;
  birthday: Date;
  email: string;
  etag: string;
  firstName: string;
  fullName: string;
  isConfirmedIdentity: boolean;
  isConfirmedPhone: boolean;
  lastName: string;
  mobileToken: string;
  note: string;
  noteLock: string;
  password: string;
  passwordSalt: string;
  phone: string;
  sex: number;
  status: number;
  type: number;
  createType: number;
  username: string;
  language: string;
  code: string;
  points: number;
  expTocken: Date;
  shareCode: string;
  identificationType: number;
  identificationID: string;
  identificationPhotoFront: string;
  identificationPhotoBackside: string;
  verifyStatus: number;
  verifyDate: Date;
  verifyUserId: number;
  verifyNote: string;
  createdDate: Date;
  createdSystemDate: Date;
  createdUserId: number;
  createdSystemUserId: number;
  updatedDate: Date;
  updatedSystemDate: Date;
  updatedSystemUserId: number;
  updatedUserId: number;
  languageId: number;
  transactions: Transaction[];
  walletTransactions: WalletTransaction[];
  ticketPrepaids: TicketPrepaid[];
  wallet : Wallet;
  projectAccounts: ProjectAccount[];
  managerUserId: number;
  subManagerUserId: number;
  managerUserObject: User;
  subManagerUserObject: User;
  maxBookingBike: number;
  project: Project[];
  customerGroups: CustomerGroup[]
}

export class Wallet {
  accountId: number;
  balance: number;
  subBalance: number;
  status: number;
  createDate: Date;
  updateDate: number;
  tripPoint: number;
}

export class AccountEditCommand {
  id: number;
  address: string;
  rfid: string;
  birthday: Date;
  email: string;
  fullName: string;
  note: string;
  password: string;
  phone: string;
  sex: number;
  status: number;
  type: number;
  createType: number;
  languageId: number;
  userId: number;
  user: User;
  maxBookingBike: number;

  deserialize(input: Account): AccountEditCommand {
    if (input == null) {
      input = new Account();
    }
    this.id = input.id;
    this.email = input.email;
    this.sex = input.sex;
    this.birthday = input.birthday;
    this.phone = input.phone;
    this.address = input.address;
    this.status = input.status;
    this.note = input.note;
    this.type = input.type;
    this.password = "Matk@123";
    this.rfid = input.rfid;
    this.fullName = input.fullName;
    this.languageId = input.languageId;
    this.maxBookingBike = input.maxBookingBike;

    if(input.subManagerUserId <= 0)
    {
      this.userId = input.managerUserId;
      this.user = input.managerUserObject;
    }
    else
    {
      this.userId = input.subManagerUserId;
      this.user = input.subManagerUserObject;
    }
    return this;
  }
}

export class AccountVerifyCommand {

  id: number;
  identificationID: string;
  identificationPhotoBackside: string;
  identificationPhotoFront: string;
  identificationType: number;
  verifyNote: string;
  verifyStatus: number;

  deserialize(input: Account): AccountVerifyCommand {
    if (input == null) {
      input = new Account();
    }
    this.id = input.id;
    this.identificationID = input.identificationID;
    this.identificationPhotoBackside = input.identificationPhotoBackside;
    this.identificationPhotoFront = input.identificationPhotoFront;
    this.identificationType = input.identificationType;
    this.verifyNote = input.verifyNote;
    this.verifyStatus = input.verifyStatus;
    return this;
  }
}

export class AccountChangePasswordCommand {
  newPassword: string;
  confirmPassword: string;
}

export class AccountCommand {

  id: number;
  address: string;
  avatar: string;
  birthday: Date;
  createdDate: Date;
  createdSystemUserId: number;
  email: string;
  etag: string;
  firstName: string;
  isConfirmedIdentity: boolean;
  isConfirmedPhone: boolean;
  lastName: string;
  mobileToken: string;
  note: string;
  noteLock: string;
  password: string;
  passwordSalt: string;
  phone: string;
  sex: number;
  status: number;
  type: number;
  updatedDate: Date;
  updatedSystemUserId: number;
  username: string;
  language: string;
  code: string;
  points: number;
  expTocken: Date;
  shareCode: string;
  identificationType: number;
  identificationID: string;
  identificationPhotoFront: string;
  identificationPhotoBackside: string;
  verifyStatus: number;
  verifyNote: string;

  deserialize(input: Account): AccountCommand {
    if (input == null) {
      input = new Account();
    }
    this.id = input.id;
    this.address = input.address;
    this.avatar = input.avatar;
    this.birthday = input.birthday;
    this.createdDate = input.createdDate;
    this.createdSystemUserId = input.createdSystemUserId;
    this.email = input.email;
    this.etag = input.etag;
    this.firstName = input.firstName;
    this.isConfirmedIdentity = input.isConfirmedIdentity;
    this.isConfirmedPhone = input.isConfirmedPhone;
    this.lastName = input.lastName;
    this.mobileToken = input.mobileToken;
    this.note = input.note;
    this.noteLock = input.noteLock;
    this.password = input.password;
    this.passwordSalt = input.passwordSalt;
    this.phone = input.phone;
    this.sex = input.sex;
    this.status = input.status;
    this.type = input.type;
    this.updatedDate = input.updatedDate;
    this.updatedSystemUserId = input.updatedSystemUserId;
    this.username = input.username;
    this.language = input.language;
    this.code = input.code;
    this.points = input.points;
    this.expTocken = input.expTocken;
    this.shareCode = input.shareCode;
    this.identificationType = input.identificationType;
    this.identificationID = input.identificationID;
    this.identificationPhotoFront = input.identificationPhotoFront;
    this.identificationPhotoBackside = input.identificationPhotoBackside;
    this.verifyStatus = input.verifyStatus;
    this.verifyNote = input.verifyNote;
    return this;
  }
}

export class WalletTransaction {
  id: number;
  accountId: number;
  walletId: number;
  walletType: number;
  type: number;
  transactionId: number;
  transactionCode: string;
  hashCode: string;
  amount: number;
  status: number;
  note: string;
  createdDate: Date;
  campaignId: number;
  campaign: Campaign;
  tripPoint: number;
  paymentGroup: number;
  setPenddingTime: Date;
  padTime: number;
  reqestId: number;
  adminNote: string;
  files: string;
  account: Account;
}

export class WalletTransactionCommand {

  id: number;
  accountId: number;
  walletId: number;
  walletType: number;
  type: number;
  transactionId: number;
  transactionCode: string;
  hashCode: string;
  amount: number;
  status: number;
  note: string;
  createdDate: Date;
  campaignId: number;
  adminNote: string;
  files: string;

  deserialize(input: WalletTransaction): WalletTransactionCommand {
    if (input == null) {
      input = new WalletTransaction();
    }
    this.id = input.id;
    this.accountId = input.accountId;
    this.walletId = input.walletId;
    this.walletType = input.walletType;
    this.type = input.type;
    this.transactionId = input.transactionId;
    this.transactionCode = input.transactionCode;
    this.hashCode = input.hashCode;
    this.amount = input.amount;
    this.status = input.status;
    this.note = input.note;
    this.createdDate = input.createdDate;
    this.campaignId = input.campaignId;
    this.adminNote = input.adminNote;
    this.files = input.files;
    return this;
  }
}

export class WalletTransactionEditCommand
{
  id: number;
  adminNote: string;
}


export class Transaction {

  id: number;
  stationIn: number;
  stationOut: number;
  bikeId: number;
  dockId: number;
  accountId: number;
  customerGroupId: number;
  transactionCode: string;
  vote: number;
  feedback: string;
  status: number;
  chargeInAccount: number;
  chargeInSubAccount: number;
  ticketPriceId: number;
  dateOfPayment: Date;
  createdDate: Date;
  totalMinutes: number;
  totalPrice: number;
  kCal: number;
  kG: number;
  kM: number;
  endTime: Date;
  investorId: number;
  projectId: number;
  startTime: Date;
  ticketPrice_AllowChargeInSubAccount: boolean;
  ticketPrice_BlockPerMinute: number;
  ticketPrice_BlockPerViolation: number;
  ticketPrice_BlockViolationValue: number;
  ticketPrice_IsDefault: boolean;
  ticketPrice_LimitMinutes: number;
  ticketPrice_TicketType: number;
  ticketPrice_TicketValue: number;
  wallet_AccountBlance: number;
  wallet_SubAccountBlance: number;
  note: string;
  ticketPrepaidCode: string;
  ticketPrepaidId: number;
  ticketPrice_Note : string;
  bike:Bike;
  customerGroup: CustomerGroup;
  tripPoint: number;
}

export class TransactionCommand {

  id: number;
  stationIn: number;
  stationOut: number;
  bikeId: number;
  dockId: number;
  accountId: number;
  customerGroupId: number;
  transactionCode: string;
  vote: number;
  feedback: string;
  status: number;
  chargeInAccount: number;
  chargeInSubAccount: number;
  ticketPriceId: number;
  dateOfPayment: Date;
  createdDate: Date;
  totalMinutes: number;
  totalPrice: number;
  kCal: number;
  kG: number;
  kM: number;
  endTime: Date;
  investorId: number;
  projectId: number;
  startTime: Date;
  ticketPrice_AllowChargeInSubAccount: boolean;
  ticketPrice_BlockPerMinute: number;
  ticketPrice_BlockPerViolation: number;
  ticketPrice_BlockViolationValue: number;
  ticketPrice_IsDefault: boolean;
  ticketPrice_LimitMinutes: number;
  ticketPrice_TicketType: number;
  ticketPrice_TicketValue: number;
  wallet_AccountBlance: number;
  wallet_SubAccountBlance: number;
  note: string;


  deserialize(input: Transaction): TransactionCommand {
    if (input == null) {
      input = new Transaction();
    }
    this.id = input.id;
    this.stationIn = input.stationIn;
    this.stationOut = input.stationOut;
    this.bikeId = input.bikeId;
    this.dockId = input.dockId;
    this.accountId = input.accountId;
    this.customerGroupId = input.customerGroupId;
    this.transactionCode = input.transactionCode;
    this.vote = input.vote;
    this.feedback = input.feedback;
    this.status = input.status;
    this.chargeInAccount = input.chargeInAccount;
    this.chargeInSubAccount = input.chargeInSubAccount;
    this.ticketPriceId = input.ticketPriceId;
    this.dateOfPayment = input.dateOfPayment;
    this.createdDate = input.createdDate;
    this.totalMinutes = input.totalMinutes;
    this.totalPrice = input.totalPrice;
    this.kCal = input.kCal;
    this.kG = input.kG;
    this.kM = input.kM;
    this.endTime = input.endTime;
    this.investorId = input.investorId;
    this.projectId = input.projectId;
    this.startTime = input.startTime;
    this.ticketPrice_AllowChargeInSubAccount = input.ticketPrice_AllowChargeInSubAccount;
    this.ticketPrice_BlockPerMinute = input.ticketPrice_BlockPerMinute;
    this.ticketPrice_BlockPerViolation = input.ticketPrice_BlockPerViolation;
    this.ticketPrice_BlockViolationValue = input.ticketPrice_BlockViolationValue;
    this.ticketPrice_IsDefault = input.ticketPrice_IsDefault;
    this.ticketPrice_LimitMinutes = input.ticketPrice_LimitMinutes;
    this.ticketPrice_TicketType = input.ticketPrice_TicketType;
    this.ticketPrice_TicketValue = input.ticketPrice_TicketValue;
    this.wallet_AccountBlance = input.wallet_AccountBlance;
    this.wallet_SubAccountBlance = input.wallet_SubAccountBlance;
    this.note = input.note;

    return this;
  }
}

export class Feedback {

  id: number;
  accountId: number;
  message: string;
  status: number;
  createdDate: Date;
  rating: number;
  transactionId: number;
  type: number;
  transactionCode: string;
}

export class FeedbackCommand {

  id: number;
  accountId: number;
  message: string;
  status: number;
  createdDate: Date;
  rating: number;
  transactionId: number;
  type: number;
  transactionCode: string;

  deserialize(input: Feedback): FeedbackCommand {
    if (input == null) {
      input = new Feedback();
    }
    this.id = input.id;
    this.accountId = input.accountId;
    this.message = input.message;
    this.status = input.status;
    this.createdDate = input.createdDate;
    this.rating = input.rating;
    this.transactionId = input.transactionId;
    this.type = input.type;
    this.transactionCode = input.transactionCode;
    return this;
  }
}

export class RedeemPoint
{
  id: number;
  name: string;
  tripPoint: number;
  toPoint: number;
  note: string;
  isProcess: boolean;
}

export class GiftCardGetDataModel
{
  redeemPoints: RedeemPoint[];
  voucherCodes: VoucherCode[];
  totalTripPoint: number;
}