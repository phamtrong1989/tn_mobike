import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { Account, WalletTransaction, WalletTransactionCommand, Transaction, TransactionCommand, FeedbackCommand, Feedback, AccountEditCommand, AccountChangePasswordCommand, AccountVerifyCommand, RedeemPoint, GiftCardGetDataModel } from './accounts.model';
import { ApiResponseData, ClientSettings } from './../../../app.settings.model';
import { AppClientSettings } from '../../../app.settings';
import { AppCommon } from '../../../app.common'
import { CustomerGroup, LogAdmin, ProjectAccountEdit, TicketPrice, TicketPriceCommand } from '../../category/projects/projects.model';
import { TicketPrepaid, TicketPrepaidCommand } from '../ticket-prepaids/ticket-prepaids.model';
import { WalletTransactionGiftCommand } from '../../wallet/wallet-transactions/wallet-transactions.model';
import { User } from '../../user/users/users.model';

@Injectable()
export class AccountsService {
    public url = "/api/work/Customers";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    searchPaged(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchpaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    create(user: AccountEditCommand) {
        return this.http.post<ApiResponseData<Account>>(this.url + "/create", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    edit(user: AccountEditCommand) {
        return this.http.put<ApiResponseData<Account>>(this.url + "/edit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    editCustomerGroup(accountId: number, models: ProjectAccountEdit[]) {
        return this.http.put<ApiResponseData<Account>>(this.url + "/editCustomerGroup/" + accountId, models).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    connectEmployee(user: AccountEditCommand) {
        return this.http.put<ApiResponseData<Account>>(this.url + "/ConnectEmployee", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    getById(id: number) {
        return this.http.get<ApiResponseData<Account>>(this.url + "/getById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    delete(id: number) {
        return this.http.delete<ApiResponseData<Account>>(this.url + "/delete/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchWalletTransactionPaged(params: any) {
        return this.http.get<ApiResponseData<WalletTransaction[]>>(this.url + "/WalletTransactionSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchTransactionsPaged(params: any) {
        return this.http.get<ApiResponseData<Transaction[]>>(this.url + "/TransactionSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchFeedbackPaged(params: any) {
        return this.http.get<ApiResponseData<Feedback[]>>(this.url + "/FeedbackSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    changePassword(user: AccountChangePasswordCommand) {
        return this.http.put<ApiResponseData<Account>>(this.url + "/changePassword", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    verifyEdit(user: AccountVerifyCommand) {
        return this.http.put<ApiResponseData<Account>>(this.url + "/verifyEdit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    walletTransactionAdd(user: WalletTransactionCommand) {
        return this.http.post<ApiResponseData<WalletTransaction>>(this.url + "/WalletTransactionAdd", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    
    walletTransactionArrears(user: WalletTransactionCommand) {
        return this.http.post<ApiResponseData<WalletTransaction>>(this.url + "/walletTransactionArrears", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    logSearchPaged(params: any) {
        return this.http.get<ApiResponseData<LogAdmin[]>>(this.url + "/LogSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    
    ticketPrepaidCreate(user: TicketPrepaidCommand) {
        return this.http.post<ApiResponseData<TicketPrepaid>>(this.url + "/TicketPrepaidCreate", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    ticketPrepaidEdit(user: TicketPrepaidCommand) {
        return this.http.put<ApiResponseData<TicketPrepaid>>(this.url + "/TicketPrepaidEdit", user).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    ticketPrepaidGetById(id: number) {
        return this.http.get<ApiResponseData<TicketPrepaid>>(this.url + "/TicketPrepaidGetById/" + id).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchTicketPriceByProject(params: any) {
        return this.http.get<ApiResponseData<CustomerGroup[]>>(this.url + "/SearchTicketPriceByProject", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    ticketPrepaidSearchPaged(params: any) {
        return this.http.get<ApiResponseData<TicketPrepaid[]>>(this.url + "/TicketPrepaidSearchPaged", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    addGiftCard(code: string, accountId: number) {
        return this.http.post<ApiResponseData<WalletTransaction>>(this.url + "/addGiftCard?code=" + code + "&accountId=" + accountId, null).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    redeemPointAdd(id: number, accountId: number) {
        return this.http.post<ApiResponseData<RedeemPoint>>(this.url + "/RedeemPointAdd/" + accountId + "/" + id, null).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    giftCardGetData(accountId: number) {
        return this.http.get<ApiResponseData<GiftCardGetDataModel>>(this.url + "/GiftCardGetData/" + accountId, this.appCommon.getHeaders({})).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    walletTransactionGift(user: WalletTransactionGiftCommand) {
        return this.http.post<ApiResponseData<WalletTransaction>>(this.url + "/walletTransactionGift", user).pipe(shareReplay(1),retry(1),catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByAccount(params: any) {
        return this.http.get<ApiResponseData<Account[]>>(this.url + "/searchByAccount", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    uploadPath(): string
    {
        return this.url + "/upload";
    }

    searchByUser(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchByUser", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    searchByManagerUser(params: any) {
        return this.http.get<ApiResponseData<User[]>>(this.url + "/searchByManagerUser", this.appCommon.getHeaders(params)).pipe(shareReplay(1), retry(1), catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))).toPromise();
    }

    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
}