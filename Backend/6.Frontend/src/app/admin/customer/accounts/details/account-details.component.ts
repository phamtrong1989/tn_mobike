import { Component, OnInit, Inject, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { Subject, ReplaySubject } from 'rxjs';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { ClientSettings, PullData } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AccountsService } from '../accounts.service';
import { Account, AccountCommand, AccountEditCommand, AccountVerifyCommand, Transaction, WalletTransaction } from '../accounts.model';
import { AnyNaptrRecord } from 'dns';
import { WalletTransactionsComponent } from '../wallet-transactions/wallet-transactions.component';
import { TransactionsComponent } from '../transactions/transactions.component';
import { FeedbacksComponent } from '../feedbacks/feedbacks.component';
import { CustomerGroup } from 'src/app/admin/category/projects/projects.model';
import { AccountsEditComponent } from '../edit/accounts-editcomponent';
import { AccountsChangePasswordComponent } from '../change-password/accounts-change-password.component';
import { AccountsVerifyComponent } from '../verify/accounts-verify.component';
import { WalletTransactionsEditComponent } from '../wallet-transactions/create/wallet-transactions-edit.component';
import { AccountsLogComponent } from '../log/accounts-log.component';
import { TicketPrepaid, TicketPrepaidCommand, TicketPrepaidEditCommand } from '../../ticket-prepaids/ticket-prepaids.model';
import { TicketPrepaidsCreateComponent } from '../../ticket-prepaids/create/ticket-prepaids-create.component';
import { TicketPrepaidsEditComponent } from '../../ticket-prepaids/edit/ticket-prepaids-edit.component';
import { AccountTicketPrepaidsCreateComponent } from '../ticket-prepaids/create/ticket-prepaids-create.component';
import { AccountTicketPrepaidsEditComponent } from '../ticket-prepaids/edit/ticket-prepaids-edit.component';
import { AccountTicketPrepaidsComponent } from '../ticket-prepaids/ticket-prepaids.component';
import { AccountAddGiftCardComponent } from '../add-gift-card/add-gift-card.component';
import { EditCustomerGroupComponent } from '../edit-customer-group/edit-customer-group.component';
import { AccountsGiftComponent } from '../wallet-transactions/gift/accounts-gift.component';
import { AccountConnectionEmployeeComponent } from '../connection-employee/connection-employee.component';
import { WalletTransactionsArrearsComponent } from '../wallet-transactions/arrears/wallet-transactions-arrears.component';
@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class AccountDetailsComponent implements OnInit, AfterViewInit {

  public clientSettings: ClientSettings;
  protected _onDestroy = new Subject<void>();
  public form: FormGroup;
  public categoryData: PullData;
  public customerGroups: CustomerGroup[];
  public listData: Transaction[];
  public listWT: WalletTransaction[];
  public listTicketPrepaid: TicketPrepaid[];

  displayedColumns = ["dateOfPayment", "stationIn", "stationOut", "ticketPrice_TicketType", "totalPrice", "tripPoint", "status"];
  displayedColumnsWT = ["createdDate", "type", "totalAmount", "wallet", "note", "status"];
  displayedColumnsPrepad = ["code", "startDate", "ticketValue", "dateOfPayment", "ticketTypeNote"];

  public heightScroll: number = 828;
  viewType: number;
  isProcess = { saveProcess: false, deleteAppointments: false, nationalitySearching: false, dialogOpen: false, initData: false };
  constructor(
    public dialogRef: MatDialogRef<AccountDetailsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { account: Account, accountCommand: AccountCommand },
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountService: AccountsService,
    public appCommon: AppCommon,
    appClientSettings: AppClientSettings,
    private cdr: ChangeDetectorRef
  ) {
    this.clientSettings = appClientSettings.settings
    this.categoryData = this.appStorage.getCategoryData();
    this.isProcess.initData = true;
    this.getData();
  }

  getData() {
    this.accountService.getById(this.data.account.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data.accountCommand = new AccountCommand().deserialize(rs.data);
        this.data.account = rs.data;
        this.isProcess.initData = false;
        this.listData = rs.data.transactions;
        this.listWT = rs.data.walletTransactions;
        this.listTicketPrepaid = rs.data.ticketPrepaids;
      }
      else {
        this.snackBar.open("Dữ liệu không tồn tại vui lòng thử lại", "Cảnh báo", { duration: 5000, });
      }
    });
  }

  ngOnInit() {

  }

  getImage() {
    return this.data.account?.avatar == null ? "./assets/img/app/no-image.png" : this.data.account?.avatar;
  }

  getFullUrl(url: string) {
    return this.clientSettings.serverAPI + url;
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.accountService.edit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open("Cập nhật tài khoản thành công", "Thành công", { duration: 5000 });
          this.close(res)
        }
        else {
          this.snackBar.open("Có lỗi hoặc phiên làm việc đã kết thúc, vui lòng F5 refesh lại trình duyệt và thử lại", "Cảnh báo", { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  getCustomerGroupName() {
    var text = "";
    try {
      this.data.account.projectAccounts.forEach(element => {
        text += ", " + this.categoryData.customerGroups.find(x => x.id == element.customerGroupId)?.name + " (" + this.categoryData.projects.find(x => x.id == element.projectId)?.name + ")";
      });
      if (text.startsWith(', ')) {
        text = text.substring(2, text.length);
      }
    }
    catch
    { }
    return text;
  }

  openChangePassword() {
    this.dialog.open(AccountsChangePasswordComponent, {
      data: this.data.account,
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  addGift() {
    this.dialog.open(AccountAddGiftCardComponent, {
      data: this.data.account,
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close(1);
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  openAddMoneyDialog() {
    this.dialog.open(WalletTransactionsEditComponent, {
      data: this.data.account,
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  openAddArrearsDialog() {
    this.dialog.open(WalletTransactionsArrearsComponent, {
      data: this.data.account,
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  openGift() {
    this.dialog.open(AccountsGiftComponent, {
      data: this.data.account,
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  openCustomersEditDialog() {
    this.dialog.open(AccountsEditComponent, {
      data: new AccountEditCommand().deserialize(this.data.account),
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  openEditCustomerGroupDialog() {
    this.dialog.open(EditCustomerGroupComponent, {
      data: this.data.account,
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  openCustomersVerifyDialog() {
    this.dialog.open(AccountsVerifyComponent, {
      data: new AccountVerifyCommand().deserialize(this.data.account),
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  matTabGroupChange(e: any, wt: WalletTransactionsComponent, tt: TransactionsComponent, fb: FeedbacksComponent, log: AccountsLogComponent, prepad: AccountTicketPrepaidsComponent) {
    console.log(e.index);
    if (e.index == 1) {
      prepad.firstSearch();
    }
    else if (e.index == 2) {
      wt.firstSearch();
    }
    else if (e.index == 3) {
      tt.firstSearch();
    }
    else if (e.index == 4) {
      fb.firstSearch();
    }
    else if (e.index == 5) {
      log.firsLoad();
    }
  }

  openTicketPrepaidEditDialog(data: TicketPrepaid) {
    var model = new TicketPrepaidCommand();
    if (data == null) {
      model.id = 0;
      model.accountId = this.data.account.id;
      this.dialog.open(AccountTicketPrepaidsCreateComponent, {
        data: model,
        panelClass: 'dialog-default',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.getData();
      });
    }
    else {
      var dataEdit = new TicketPrepaidEditCommand().deserialize(data);
      this.dialog.open(AccountTicketPrepaidsEditComponent, {
        data: { dataEdit, data },
        panelClass: 'dialog-default',
        disableClose: true,
        autoFocus: false
      }).afterClosed().subscribe(outData => {
        this.getData();
      });
    }
  }

  fixHeightScroll() {
    let getTop = document.getElementById("fix_detail").getBoundingClientRect().top;
    this.heightScroll = window.innerHeight - getTop - 30;
    this.viewType = 0;
  }

  public openSetConnectionEmployeeDialog() {
    this.dialog.open(AccountConnectionEmployeeComponent, {
      data: new AccountEditCommand().deserialize(this.data.account),
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.getData();
    });
  }

  ngAfterViewInit(): void {
    this.fixHeightScroll();
    this.cdr.detectChanges();
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightScroll();
  }
}
