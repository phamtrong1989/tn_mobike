import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AccountCommand, AccountEditCommand, AccountVerifyCommand } from '../accounts.model';
import { AccountsService } from '../accounts.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AccountsChangePasswordComponent } from '../change-password/accounts-change-password.component';

@Component({
  selector: 'app-accounts-verify',
  templateUrl: './accounts-verify.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class AccountsVerifyComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  public isChangePassword: FormControl;
  constructor(
    public dialogRef: MatDialogRef<AccountsVerifyComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: AccountVerifyCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountsService: AccountsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      identificationID: [null, Validators.compose([Validators.maxLength(50)])],
      identificationPhotoBackside: [null],
      identificationPhotoFront: [null],
      identificationType: [0],
      verifyNote: [null, Validators.compose([Validators.maxLength(1000)])],
      verifyStatus: [0]
    });
  }

  ngOnInit() {
    if (this.data.id > 0) {
      this.form.setValue(this.data);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.accountsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new AccountVerifyCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    this.accountsService.verifyEdit(this.form.value).then((res) => {
      if (res.status == 1) {
        this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
        this.close(res)
      }
      else if (res.status == 2) {
        this.snackBar.open("Số điện thoại đã tồn tại, vui lòng chọn số điện thoại khác", this.baseTranslate["msg-success-type"], { duration: 5000 });
      }
      else if (res.status == 3) {
        this.snackBar.open("Mã RFID đã tồn tại, vui lòng chọn mã khác", this.baseTranslate["msg-success-type"], { duration: 5000 });
      }
      else if (res.status == 4) {
        this.snackBar.open("Bạn chưa nhập mã RFID", this.baseTranslate["msg-success-type"], { duration: 5000 });
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
      }
      this.isProcess.saveProcess = false;
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
