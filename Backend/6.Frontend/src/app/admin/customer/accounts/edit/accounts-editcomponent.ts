import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AccountCommand, AccountEditCommand } from '../accounts.model';
import { AccountsService } from '../accounts.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { AccountsChangePasswordComponent } from '../change-password/accounts-change-password.component';
import { User } from 'src/app/admin/user/users/users.model';

@Component({
  selector: 'app-accounts-edit',
  templateUrl: './accounts-edit.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class AccountsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, managerUserSearching: false, usersearching: false };
  public isChangePassword: FormControl;
  users: User[]; 
  userKey: FormControl = new FormControl("");

  constructor(
    public dialogRef: MatDialogRef<AccountsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: AccountEditCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountsService: AccountsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12)])],
      email: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(100)])],
      sex: [0],
      fullName: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(100)])],
      birthday: [new Date()],
      phone: [null, Validators.compose([Validators.required, Validators.minLength(9), Validators.maxLength(16)])],
      address: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(100)])],
      status: [1],
      note: [null, Validators.compose([Validators.maxLength(2000)])],
      type: [0],
      rfid: [null, Validators.compose([Validators.minLength(1), Validators.maxLength(200)])],
      languageId: [2],
      maxBookingBike: [0],
      userId: [0],
      user: [null]
    });

    this.form.get('rfid').disable();

    this.form.get('type').valueChanges.subscribe(x => {
      if (x == 0) {
        this.form.get('rfid').disable();
        this.form.get('phone').enable();
        if(this.data.id == 0)
        {
          this.form.get('password').enable();
        }
      }
      else if (x == 1) {
        this.form.get('password').setValue("Ma@168");
        this.form.get('rfid').enable();
        this.form.get('password').disable();
        this.form.get('phone').enable();
      }
      else if (x == 2) {
        this.form.get('rfid').enable();
        this.form.get('phone').enable();
        if(this.data.id == 0)
        {
          this.form.get('password').enable();
        }
      }
    });
  }

  ngOnInit() {
    if(this.data.user!=null)
    {
      this.users = [];
      this.users.push(this.data.user);
    }

  //  this.initSearchUser();

    if (this.data.id > 0) {
      this.form.get('password').setValue("Ma@168");
      this.form.get('password').disable();
      this.form.setValue(this.data);
      this.getData();
    }
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.accountsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new AccountEditCommand().deserialize(rs.data);
        this.form.setValue(this.data);

        if(this.data.user!=null)
        {
          this.users = [];
          this.users.push(this.data.user);
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;

    if (this.data.id > 0) {
      this.accountsService.edit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
          this.close(res)
        }
        else if (res.status == 2) {
          this.snackBar.open("Số điện thoại đã tồn tại, vui lòng chọn số điện thoại khác", this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        else if (res.status == 3) {
          this.snackBar.open("Mã RFID đã tồn tại, vui lòng chọn mã khác", this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        else if (res.status == 4) {
          this.snackBar.open("Bạn chưa nhập mã RFID", this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
    else {
      this.accountsService.create(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open("Thêm tài khoản thành công", this.baseTranslate["msg-success-type"], { duration: 5000 });
          this.close(null);
        }
        else if (res.status == 2) {
          this.snackBar.open("Số điện thoại đã tồn tại, vui lòng chọn số điện thoại khác", this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        else if (res.status == 3) {
          this.snackBar.open("Mã RFID đã tồn tại, vui lòng chọn mã khác", this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        else if (res.status == 4) {
          this.snackBar.open("Bạn chưa nhập mã RFID", this.baseTranslate["msg-success-type"], { duration: 5000 });
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }

  }

  public openChangePassword() {
   this.dialog.open(AccountsChangePasswordComponent, {
         data: this.data,
         panelClass: 'dialog-500',
         disableClose: true,
         autoFocus:false
       }).afterClosed().subscribe(outData => {
    });
 }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  // initSearchUser() {
  //   this.userKey.valueChanges.subscribe(rs => {
  //       this.isProcess.usersearching = false;
  //       if (rs != "") {
  //         this.isProcess.managerUserSearching = true;
  //         this.accountsService.searchByUser({key: rs, type: 5}).then(rs => {
  //           if (rs.data.length == 0) {
  //             this.form.get('userId').setValue(0)
  //           }
  //           this.users = rs.data;
  //           this.isProcess.usersearching = false;
  //         });
  //       }
  //     },
  //       error => {
  //         this.isProcess.usersearching = false;
  //     });
  // }
}
