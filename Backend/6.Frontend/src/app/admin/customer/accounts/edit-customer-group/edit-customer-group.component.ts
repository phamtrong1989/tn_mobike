import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Account, AccountCommand, AccountEditCommand } from '../accounts.model';
import { AccountsService } from '../accounts.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AccountsChangePasswordComponent } from '../change-password/accounts-change-password.component';
import { ProjectAccount, ProjectAccountEdit } from 'src/app/admin/category/projects/projects.model';
import { isThisSecond } from 'date-fns';
import { PullData } from 'src/app/app.settings.model';

@Component({
  selector: 'app-accounts-edit-customer-group',
  templateUrl: './edit-customer-group.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class EditCustomerGroupComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: PullData;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  public isChangePassword: FormControl;
  public projectAccount: ProjectAccountEdit[];
  constructor(
    public dialogRef: MatDialogRef<EditCustomerGroupComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: Account,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountsService: AccountsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
  }

  ngOnInit() {
    this.initGroup();
    this.getData();
  }

  initGroup()
  {
    this.projectAccount = [];
    this.categoryData.projects.forEach(element => {
      this.projectAccount.push({
        projectId: element.id, 
        customerGroupId: 0, 
        accountId: this.data.id
      });
    });
  
    this.projectAccount.forEach(element => {
      var ktex = this.data.projectAccounts.find(x=>x.projectId == element.projectId && x.accountId == element.accountId);
      if(ktex != null)
      {
        element.customerGroupId = ktex.customerGroupId;
      }
      else
      {
        element.customerGroupId = 0;
      }
    });
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
    this.isProcess.initData = true;
    this.accountsService.getById(this.data.id).then(rs => {
      this.data = rs.data;
      this.initGroup();
     this.isProcess.initData = false;
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    this.accountsService.editCustomerGroup(this.data.id, this.projectAccount).then((res) => {
      if (res.status == 1) {
        this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
        this.close(res)
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
      }
      this.isProcess.saveProcess = false;
    });
  }

  public openChangePassword() {
   this.dialog.open(AccountsChangePasswordComponent, {
         data: this.data,
         panelClass: 'dialog-500',
         disableClose: true,
         autoFocus:false
       }).afterClosed().subscribe(outData => {
    });
 }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
}
