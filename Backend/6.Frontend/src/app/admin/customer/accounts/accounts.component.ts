import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings, AppClientSettings } from '../../../app.settings';
import { Settings, ApiResponseData, ClientSettings } from '../../../app.settings.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { AccountsService } from './accounts.service';
import { Account, AccountCommand, AccountEditCommand } from './accounts.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';
import { AccountDetailsComponent } from './details/account-details.component';
import { AccountsEditComponent } from './edit/accounts-editcomponent';
import { AccountsChangePasswordComponent } from './change-password/accounts-change-password.component';
import { ReportService } from '../../report/report.service';
import { HttpEventType } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { User } from '../../user/users/users.model';
import { AccountConnectionEmployeeComponent } from './connection-employee/connection-employee.component';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  providers: [AccountsService, ReportService, AppCommon]
})

export class AccountsComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;

  clientSettings: ClientSettings;
  heightTable: number = 700;
  listData: Account[];
  isProcess = { dialogOpen: false, listDataTable: true, export: false, userSearching: false };
  baseURL;

  params: any = {
    pageIndex: 1,
    pageSize: 20,
    key: "",
    sortby: "",
    sorttype: "",
    customerGroupId: "",
    sex: "",
    type: "",
    status: "",
    id: null
  };

  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "id", "fullName", "phone", "vi", "debt", "customerGroups", "verifyStatus", "status", "manager", "updatedDate", "action"];
  public settings: Settings;
  public categoryData: any;
  public exportUrl: string;
  userKey: FormControl = new FormControl("");
  users: User[];
  constructor(
    public appSettings: AppSettings,
    public accountsService: AccountsService,
    public reportService: ReportService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    appClientSettings: AppClientSettings,
    private cdr: ChangeDetectorRef
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.baseURL = appClientSettings.settings.serverAPI;
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);
    this.initSearchUser();
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.appCommon.changeUrl(location.pathname, this.params);
    this.accountsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      if (res.outData) {
        this.users = [];
        this.users.push(res.outData);
      }
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  initSearchUser() {
    this.userKey.valueChanges.subscribe(rs => {
      this.isProcess.userSearching = false;
      if (rs != "") {
        this.isProcess.userSearching = true;
        this.accountsService.searchByUser({ key: rs, type: 5 }).then(rs => {
          if (rs.data.length == 0) {
            this.params.userId = null;
          }
          this.users = rs.data;
          this.isProcess.userSearching = false;
        });
      }
    },
      error => {
        this.isProcess.userSearching = false;
      });
  }

  export() {
    this.isProcess['export'] = true;

    var report_name = "BC_ThongTinKH.xlsx";
    var params = this.params;

    params.from = this.params.from ? this.appCommon.formatDateTime(this.params.from, "yyyy-MM-dd") : null;
    params.to = this.params.to ? this.appCommon.formatDateTime(this.params.to, "yyyy-MM-dd") : null;

    this.reportService.export("reportCustomer", "customer", params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess['export'] = true;
          break;
        case HttpEventType.Response:
          this.isProcess['export'] = false;

          window.open(this.baseURL + "/" + data.body);
          //const downloadedFile = new Blob([data.body], { type: data.body.type });
          // const a = document.createElement('a');
          // a.setAttribute('style', 'display:none;');
          // document.body.appendChild(a);
          // a.download = report_name;
          // a.href = URL.createObjectURL(downloadedFile);
          // a.target = '_blank';
          // a.click();
          // document.body.removeChild(a);

          this.isProcess['export'] = false;
          break;
      }
    });
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  public openEditDialog(data: Account) {
    var model = new AccountEditCommand();

    if (data == null) {
      model.id = 0;
    }
    else {
      model = new AccountEditCommand().deserialize(data);
    }

    this.dialog.open(AccountsEditComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  public openSetConnectionEmployeeDialog(data: Account) {
    var model = new AccountEditCommand();

    if (data == null) {
      model.id = 0;
    }
    else {
      model = new AccountEditCommand().deserialize(data);
    }

    this.dialog.open(AccountConnectionEmployeeComponent, {
      data: model,
      panelClass: 'dialog-default',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  public openChangePassword(data: Account) {
    this.dialog.open(AccountsChangePasswordComponent, {
      data: data,
      panelClass: 'dialog-500',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
    });
  }

  public openDetailsDialog(data: Account) {
    var dataModel = new AccountCommand().deserialize(data);
    this.dialog.open(AccountDetailsComponent, {
      data: { account: data, appointment: dataModel },
      panelClass: 'dialog-full',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      if (outData) {
        this.searchPage();
      }
    });
  }

  ngAfterViewInit() {
    this.fixHeightTable();
    this.cdr.detectChanges();
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }
}