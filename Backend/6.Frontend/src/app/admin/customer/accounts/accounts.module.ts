import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { AccountsComponent } from './accounts.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { AccountDetailsComponent } from './details/account-details.component';
import { WalletTransactionsComponent } from './wallet-transactions/wallet-transactions.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { FeedbacksComponent } from './feedbacks/feedbacks.component';
import { AccountsEditComponent } from './edit/accounts-editcomponent';
import { AccountsChangePasswordComponent } from './change-password/accounts-change-password.component';
import { AccountsVerifyComponent } from './verify/accounts-verify.component';
import { WalletTransactionsEditComponent } from './wallet-transactions/create/wallet-transactions-edit.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { AccountsLogComponent } from './log/accounts-log.component';
import { AccountTicketPrepaidsEditComponent } from './ticket-prepaids/edit/ticket-prepaids-edit.component';
import { AccountTicketPrepaidsCreateComponent } from './ticket-prepaids/create/ticket-prepaids-create.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AccountTicketPrepaidsComponent } from './ticket-prepaids/ticket-prepaids.component';
import { AccountAddGiftCardComponent } from './add-gift-card/add-gift-card.component';
import { EditCustomerGroupComponent } from './edit-customer-group/edit-customer-group.component';
import { AccountsGiftComponent } from './wallet-transactions/gift/accounts-gift.component';
import { AccountConnectionEmployeeComponent } from './connection-employee/connection-employee.component';
import { WalletTransactionsArrearsComponent } from './wallet-transactions/arrears/wallet-transactions-arrears.component';

export const routes = [
  { path: '', component: AccountsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxCurrencyModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
	 TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }) 
  ],
  declarations: [
    AccountsComponent,
    AccountsEditComponent,
    AccountDetailsComponent,
    WalletTransactionsComponent,
    TransactionsComponent,
    FeedbacksComponent,
    AccountsChangePasswordComponent,
    AccountsVerifyComponent,
    WalletTransactionsEditComponent,
    AccountsLogComponent,
    AccountTicketPrepaidsCreateComponent,
    AccountTicketPrepaidsEditComponent,
    AccountTicketPrepaidsComponent,
    AccountAddGiftCardComponent,
    EditCustomerGroupComponent,
    AccountsGiftComponent,
    AccountConnectionEmployeeComponent,
    WalletTransactionsArrearsComponent
  ],
  entryComponents:[
    AccountsEditComponent,
    AccountDetailsComponent,
    WalletTransactionsComponent,
    TransactionsComponent,
    FeedbacksComponent,
    AccountsChangePasswordComponent,
    AccountsVerifyComponent,
    WalletTransactionsEditComponent,
    AccountsLogComponent,
    AccountTicketPrepaidsCreateComponent,
    AccountTicketPrepaidsEditComponent,
    AccountTicketPrepaidsComponent,
    AccountAddGiftCardComponent,
    EditCustomerGroupComponent,
    AccountsGiftComponent,
    AccountConnectionEmployeeComponent,
    WalletTransactionsArrearsComponent
  ]
})
export class AccountsModule { }
