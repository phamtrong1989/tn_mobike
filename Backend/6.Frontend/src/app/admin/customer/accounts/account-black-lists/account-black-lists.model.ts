export class AccountBlackList {

id: number;
accountId: number;
type: number;
note: string;
time: Date;
createdUser: number;
createdDate: Date;
updatedUser: number;
updatedDate: Date;


}

export class AccountBlackListCommand {

 id: number;
accountId: number;
type: number;
note: string;
time: Date;
createdUser: number;
createdDate: Date;
updatedUser: number;
updatedDate: Date;


  deserialize(input: AccountBlackList): AccountBlackListCommand {
    if(input==null)
    {
      input  = new AccountBlackList();
    }
	this.id = input.id;
this.accountId = input.accountId;
this.type = input.type;
this.note = input.note;
this.time = input.time;
this.createdUser = input.createdUser;
this.createdDate = input.createdDate;
this.updatedUser = input.updatedUser;
this.updatedDate = input.updatedDate;

   
    return this;
  }
}