import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccountBlackList , AccountBlackListCommand} from '../account-black-lists.model';
import { AccountBlackListsService } from '../account-black-lists.service'
import { MatSnackBar } from '@angular/material';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-black-lists-edit',
  templateUrl: './account-black-lists-edit.component.html',
  providers: [
  AccountBlackListsService, 
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class AccountBlackListsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData :any;
  	baseTranslate: any;
  isProcess = { saveProcess: false , deleteProcess : false, initData: false};
  constructor(
    public dialogRef: MatDialogRef<AccountBlackListsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: AccountBlackListCommand,
    public fb: FormBuilder,

    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountBlackListsService: AccountBlackListsService,
		public appCommon : AppCommon,
		private translate:TranslateService
  ) {
    this.categoryData =  this.appStorage.getCategoryData();
	this.initBaseTranslate();
    this.form = this.fb.group({
        id: [null, Validators.compose([Validators.required])],
 accountId: [null, Validators.compose([Validators.required])],
 type: [null, Validators.compose([Validators.required])],
 note: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(99999)])],
 time: [null, Validators.compose([Validators.required])],
 createdUser: [null, Validators.compose([Validators.required])],
 createdDate: [null, Validators.compose([Validators.required])],
 updatedUser: [null, Validators.compose([Validators.required])],
 updatedDate: [null, Validators.compose([Validators.required])],

    });

  }

  ngOnInit() {

	if (this.data.id > 0) {
     this.form.setValue(this.data);
	 this.getData();
    }
   
  }

   initBaseTranslate()
  {
    this.translate.get(['base'])
    .subscribe(translations => {
      this.baseTranslate = translations['base'];
    });
  }

   getData()
  {
    this.isProcess.initData = true;
    this.accountBlackListsService.getById(this.data.id).then(rs => {
      if (rs.status==1 && rs.data) {

        this.data = new AccountBlackListCommand().deserialize(rs.data);
        this.form.setValue(this.data);
        this.isProcess.initData = false;
      }
      else {
          this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  save() {
    this.isProcess.saveProcess = true;
    if(this.form.valid)
    {
			if(this.data.id > 0)
			{
				this.accountBlackListsService.edit(this.form.value).then((res) => {
					  if (res.status == 1) {
						  this.snackBar.open(this.baseTranslate["msg-edit-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
						this.close(res)
					  }
					  else{
						this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
					  }
					  this.isProcess.saveProcess = false;
					});
			}
			else
			{
				this.accountBlackListsService.create(this.form.value).then((res) => {
					   if (res.status == 1) {
						  this.snackBar.open(this.baseTranslate["msg-create-success"], this.baseTranslate["msg-success-type"], { duration: 5000 });
						  this.data = new AccountBlackListCommand().deserialize(res.data);
							this.form.setValue(this.data);
						  this.close(res);
						}
						else {
						  this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
						}
						this.isProcess.saveProcess = false;
					  });
					
			}
    }
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: this.baseTranslate["msg-delete-confirmation"],
      panelClass: 'dialog-confirmation',
    }).afterClosed().subscribe(result => {
      if (result) {
       
          this.isProcess.deleteProcess =true;
          this.accountBlackListsService.delete(this.data.id).then((res) => {
            if(res.status==1)
            {
             this.snackBar.open(this.baseTranslate["msg-delete-success"], this.baseTranslate["msg-succes-type"], { duration: 5000 });
              this.isProcess.deleteProcess =false;
              this.close(this.data)
            }
            else if(res.status==0)
            {
               this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
              this.close(this.data)
            }
          });

      }
    });
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }
  
}
