import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AccountsService } from '../../accounts.service';
import { AppCommon } from 'src/app/app.common';
import { Account } from '../../accounts.model';
import { AppStorage } from 'src/app/app.storage';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-wallet-transactions-arrears',
  templateUrl: './wallet-transactions-arrears.component.html',
  providers: [
    AccountsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class WalletTransactionsArrearsComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  isProcess = { saveProcess: false, deleteProcess: false, initData: false };
  uploadFileOut: FileDataDTO[];
  constructor(
    public dialogRef: MatDialogRef<WalletTransactionsArrearsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: Account,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public accountsService: AccountsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      accountId: [0],
      type: [5],
      transactionCode: [null, Validators.compose([Validators.maxLength(50)])],
      note: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1000)])],
      subAmount: [0, Validators.compose([Validators.required])],
      amount: [0, Validators.compose([Validators.required])],
      files: [""]
    });
    this.form.get('accountId').setValue(this.data.id);
  }

  ngOnInit() {

  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  getData() {
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {
      this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: "Khi đồng ý thì dữ liệu không thể cập nhật hay xóa, bạn có muốn thực hiện chức năng này?",
        panelClass: 'dialog-confirmation',
      }).afterClosed().subscribe(result => {
        if (result) {
          this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
          this.accountsService.walletTransactionArrears(this.form.value).then((res) => {
            if (res.status == 1) {
              this.snackBar.open("Truy thu điểm thành công", this.baseTranslate["msg-success-type"], { duration: 5000 });
              this.close();
            }
            else  if (res.status == 5) {
              this.snackBar.open("Bạn chưa nhập số điểm", this.baseTranslate["msg-warning-type"], { duration: 5000 });
            }
            else  if (res.status == 6) {
              this.snackBar.open("Số dư điểm của tài khoản không đủ để thực hiện truy thu", this.baseTranslate["msg-warning-type"], { duration: 5000 });
            }
            else {
              this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
            }
            this.isProcess.saveProcess = false;
          });
        }
        else {
          this.isProcess.saveProcess = false;
        }
      });
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

}
