import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';

import { AppClientSettings, AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';

import { ReportService } from './../report.service';

@Component({
  selector: 'app-infrastructure-report',
  templateUrl: './report.infrastructure.component.html',
  providers: [ReportService, AppCommon]
})

export class ReportInfrastructureComponent {
  baseURL;
  isProcess = { station: false, bike: false, maintainRepair: false, supplies: false, suppliesInWarehouse: false, dock: false };

  station = {};
  bike = {};
  maintainRepair = { from: new Date(new Date().getFullYear(), new Date().getMonth(), 1), to: new Date() };
  suppliesInWarehouse = {};
  supplies = {};
  dock = {};

  public settings: Settings;
  public categoryData: any;

  constructor(
    public appSettings: AppSettings,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public reportService: ReportService,
    appClientSettings: AppClientSettings
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.baseURL = appClientSettings.settings.serverAPI;
  }

  export(reportType: string) {
    this.isProcess[reportType] = true;

    var report_name = "";
    var params;
    switch (reportType) {
      case "station": {
        report_name = "BC_TramXe.xlsx";
        params = this.station;
      }
        break;
      case "bike": {
        report_name = "BC_XeDap.xlsx";
        params = this.bike;
      }
        break;
      case "maintainRepair": {
        report_name = "BC_BaoDuongSuaChua.xlsx";

        params = this.maintainRepair;
        params.from = this.appCommon.formatDateTime(params.from, "yyyy-MM-dd");
        params.to = this.appCommon.formatDateTime(params.to, "yyyy-MM-dd");
      }
        break;
      case "supplies": {
        report_name = "BC_VatTu.xlsx";
        params = this.supplies;
      }
        break;
      case "suppliesInWarehouse": {
        report_name = "BC_TonKhoVatTu.xlsx";
        params = this.station;
      }
        break;
      case "dock": {
        report_name = "BC_KhoaSim.xlsx";
        params = this.dock;
      }
        break;
      default: {
      }
        break;
    }

    this.reportService.export("reportInfrastructure", reportType, params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess[reportType] = true;
          break;
        case HttpEventType.Response:
          this.isProcess[reportType] = false;

          window.open(this.baseURL + "/" + data.body);
          // const downloadedFile = new Blob([data.body], { type: data.body.type });
          // const a = document.createElement('a');
          // a.setAttribute('style', 'display:none;');
          // document.body.appendChild(a);
          // a.download = report_name;
          // a.href = URL.createObjectURL(downloadedFile);
          // a.target = '_blank';
          // a.click();
          // document.body.removeChild(a);

          this.isProcess[reportType] = false;
          break;
      }
    });
  }
}