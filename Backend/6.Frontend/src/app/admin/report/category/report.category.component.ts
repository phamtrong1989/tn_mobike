import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';

import { AppClientSettings, AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';

import { ReportService } from './../report.service';

@Component({
  selector: 'app-category-report',
  templateUrl: './report.category.component.html',
  providers: [ReportService, AppCommon]
})

export class ReportCategoryComponent {
  baseURL;
  isProcess = { project: false, investor: false, color: false, model: false, producer: false, redeemPoint: false };

  project = {};
  investor = {};
  color = {};
  model = {};
  producer = {};
  redeemPoint = {};

  public settings: Settings;
  public categoryData: any;

  constructor(
    public appSettings: AppSettings,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public reportService: ReportService,
    appClientSettings: AppClientSettings,
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.baseURL = appClientSettings.settings.serverAPI;
  }

  export(reportType: string) {
    this.isProcess[reportType] = true;

    var report_name = "";
    var params;
    switch (reportType) {
      case "project": {
        report_name = "BC_DS_DuAn.xlsx";
        params = this.project;
      }
        break;
      case "investor": {
        report_name = "BC_DS_NhaDauTu.xlsx";
        params = this.investor;
      }
        break;
      case "color": {
        report_name = "BC_DS_MaMau.xlsx";
        params = this.color;
      }
        break;
      case "model": {
        report_name = "BC_MauThietBi.xlsx";
        params = this.model;
      }
        break;
      case "producer": {
        report_name = "BC_NhaSanXuatThietBi.xlsx";
        params = this.producer;
      }
        break;
      case "redeemPoint": {
        report_name = "BC_CauHinhDoiDiem.xlsx";
        params = this.redeemPoint;
      }
        break;
    }

    this.reportService.export("reportCategory", reportType, params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess[reportType] = true;
          break;
        case HttpEventType.Response:
          this.isProcess[reportType] = false;

          window.open(this.baseURL + "/" + data.body);
          // const downloadedFile = new Blob([data.body], { type: data.body.type });
          // const a = document.createElement('a');
          // a.setAttribute('style', 'display:none;');
          // document.body.appendChild(a);
          // a.download = report_name;
          // a.href = URL.createObjectURL(downloadedFile);
          // a.target = '_blank';
          // a.click();
          // document.body.removeChild(a);

          this.isProcess[reportType] = false;
          break;
      }
    });
  }
}