import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpParams } from '@angular/common/http';
import { ClientSettings } from './../../app.settings.model';
import { AppClientSettings } from '../../app.settings';
import { AppCommon } from '../../app.common'
import { Observable } from 'rxjs';

@Injectable()
export class ReportService {
    public url = "/api/work";
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon: AppCommon
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
    }

    // export(controller: string, report: string, params: any): Observable<HttpEvent<Blob>> {
    //     let obj = {};
    //     for (const prop in params) {
    //         if (params[prop] != null) {
    //             obj[prop] = params[prop];
    //         }
    //     }

    //     return this.http.request(new HttpRequest(
    //         'POST',
    //         this.url + "/" + controller + "/" + report,
    //         null,
    //         {
    //             reportProgress: true,
    //             responseType: 'blob',
    //             params: new HttpParams({ fromObject: obj })
    //         }));
    // }

    export(controller: string, report: string, params: any): any {
        let obj = {};
        for (const prop in params) {
            if (params[prop] != null) {
                obj[prop] = params[prop];
            }
        }

        return this.http.request(new HttpRequest(
            'POST',
            this.url + "/" + controller + "/" + report,
            null,
            {
                reportProgress: true,
                responseType: 'text',
                params: new HttpParams({ fromObject: obj })
            }));
    }
}