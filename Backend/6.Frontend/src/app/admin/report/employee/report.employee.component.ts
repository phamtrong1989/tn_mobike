import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';

import { AppClientSettings, AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { AppCommon } from '../../../app.common'
import { AppStorage } from '../../../app.storage';

import { ReportService } from './../report.service';

@Component({
  selector: 'app-report-employee',
  templateUrl: './report.employee.component.html',
  providers: [ReportService, AppCommon]
})

export class ReportEmployeeComponent {
  baseURL;
  isProcess = { nvvts: false, ctvvts: false };

  nvvts = { from: new Date(new Date().getFullYear(), 0, 1), to: new Date() };
  ctvvts = { from: new Date(new Date().getFullYear(), 0, 1), to: new Date() };

  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public reportService: ReportService,
    appClientSettings: AppClientSettings,
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
    this.baseURL = appClientSettings.settings.serverAPI;
  }

  export(reportType: string) {
    this.isProcess[reportType] = true;

    var report_name = "";
    var params;
    switch (reportType) {
      case "nvvts": {
        report_name = "BC_NhanVienVTS.xlsx";
        params = this.nvvts;
      }
        break;
      case "ctvvts": {
        report_name = "BC_CongTacVienVTS.xlsx";
        params = this.ctvvts;
      }
        break;
      default: {
      }
        break;
    }

    params.from = this.appCommon.formatDateTime(params.from, "yyyy-MM-dd");
    params.to = this.appCommon.formatDateTime(params.to, "yyyy-MM-dd");

    this.reportService.export("reportEmployee", reportType, params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess[reportType] = true;
          break;
        case HttpEventType.Response:
          this.isProcess[reportType] = false;

          window.open(this.baseURL + "/" + data.body);
          // const downloadedFile = new Blob([data.body], { type: data.body.type });
          // const a = document.createElement('a');
          // a.setAttribute('style', 'display:none;');
          // document.body.appendChild(a);
          // a.download = report_name;
          // a.href = URL.createObjectURL(downloadedFile);
          // a.target = '_blank';
          // a.click();
          // document.body.removeChild(a);

          this.isProcess[reportType] = false;
          break;
      }
    });
  }
}