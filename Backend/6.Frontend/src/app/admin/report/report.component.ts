import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';

import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { AppCommon } from '../../app.common'
import { AppStorage } from '../../app.storage';

import { ReportService } from './report.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  providers: [ReportService, AppCommon]
})

export class ReportComponent {
  isProcess = { revenue: false, campaign: false, customer: false, transaction: false };

  all = { from: new Date(new Date().getFullYear(), new Date().getMonth(), 1), to: new Date() };
  customer = { from: new Date(new Date().getFullYear(), 0, 1), to: new Date() };
  revenue = { from: new Date(new Date().getFullYear(), new Date().getMonth(), 1), to: new Date() };
  campaign = { from: new Date(new Date().getFullYear(), new Date().getMonth(), 1), to: new Date() };
  transaction = { type: 'transaction', from: new Date(new Date().getFullYear(), new Date().getMonth(), 1), to: new Date() };
  customerTransaction = { from: new Date(new Date().getFullYear(), new Date().getMonth(), 1), to: new Date() };

  public settings: Settings;
  public categoryData: any;

  constructor(
    public appSettings: AppSettings,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public reportService: ReportService,
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  export(reportType: string) {
    this.isProcess[reportType] = true;

    var report_name = "";
    var params;
    switch (reportType) {
      case "revenue": {
        report_name = "BC_DoanhThu.xlsx";
        params = this.revenue;
      }
        break;
      case "campaign": {
        report_name = "BC_KhuyenMai.xlsx";
        params = this.campaign;
      }
        break;
      case "customer": {
        report_name = "BC_ThongTinKH.xlsx";
        params = this.customer;
      }
        break;
      case "transaction": {
        report_name = "BC_ChuyenDiHoanThanh.xlsx";
        params = this.transaction;
      }
        break;
      case "booking": {
        report_name = "BC_ChuyenDiDangThue.xlsx";
        params = this.transaction;
      }
        break;
      case "transactionDebt": {
        report_name = "BC_ChuyenDiNoCuoc.xlsx";
        params = this.transaction;
      }
        break;
      case "customerTransaction": {
        report_name = "BC_TonTKOnline.xlsx";
        params = this.customerTransaction;
      }
        break;
      default: {
        report_name = "BaoCaoTongHop.xlsx";
        params = this.all;
      }
        break;
    }

    params.from = this.appCommon.formatDateTime(params.from, "yyyy-MM-dd");
    params.to = this.appCommon.formatDateTime(params.to, "yyyy-MM-dd");

    this.reportService.export("", reportType, params).subscribe(data => {
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          this.isProcess[reportType] = true;
          // this.downloadStatus.emit( {status: ProgressStatusEnum.IN_PROGRESS, percentage: Math.round((data.loaded / data.total) * 100)});
          break;
        case HttpEventType.Response:
          this.isProcess[reportType] = false;
          // this.downloadStatus.emit( {status: ProgressStatusEnum.COMPLETE});
          const downloadedFile = new Blob([data.body], { type: data.body.type });
          const a = document.createElement('a');
          a.setAttribute('style', 'display:none;');
          document.body.appendChild(a);
          a.download = report_name;
          a.href = URL.createObjectURL(downloadedFile);
          a.target = '_blank';
          a.click();
          document.body.removeChild(a);
          this.isProcess[reportType] = false;
          break;
      }
    });
  }
}