export class WalletTransactionGiftCommand
{
    giftFromAccountId: number;
    giftToAccountId: number;
    amount: number;
    Note: number;
    files: number;
}