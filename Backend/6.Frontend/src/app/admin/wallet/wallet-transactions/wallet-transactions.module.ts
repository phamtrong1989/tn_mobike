import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../../shared/shared.module';
import { PipesModule } from '../../../theme/pipes/pipes.module';
import { WalletTransactionsComponent } from './wallet-transactions.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { WalletTransactionsCreateComponent } from './create/wallet-transactions-create.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { WalletTransactionsEditComponent } from './edit/wallet-transactions-edit.component';
import { WalletTransactionsGiftComponent } from './gift/wallet-transactions-gift.component';
import { CheckRolesDirective } from 'src/app/app.data';
import { WalletTransactionsArrearsComponent } from './arrears/wallet-transactions-arrears.component';

export const routes = [
  { path: '', component: WalletTransactionsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule ,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,

    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      }
    }),
    NgxCurrencyModule
  ],
  declarations: [
    WalletTransactionsComponent,
    WalletTransactionsCreateComponent,
    WalletTransactionsEditComponent,
    WalletTransactionsGiftComponent,
    WalletTransactionsArrearsComponent
  ],
  entryComponents:[
    WalletTransactionsCreateComponent,
    WalletTransactionsEditComponent,
    WalletTransactionsGiftComponent,
    WalletTransactionsArrearsComponent
  ]
})
export class WalletTransactionsModule { }
