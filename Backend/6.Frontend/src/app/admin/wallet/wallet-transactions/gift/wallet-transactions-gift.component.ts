import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { WalletTransactionsService } from '../wallet-transactions.service';
import { Account } from 'src/app/admin/customer/accounts/accounts.model';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-wallet-transactions-gift',
  templateUrl: './wallet-transactions-gift.component.html',
  providers: [
    WalletTransactionsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class WalletTransactionsGiftComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  public pathUpload: string;
  baseTranslate: any;
  accountsFrom: Account[];
  accountsTo: Account[];

  uploadFileOut: FileDataDTO[];
  accountFromKey: FormControl = new FormControl("");
  accountToKey: FormControl = new FormControl("");

  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountFromSearching: false, accountToSearching: false };
  constructor(
    public dialogRef: MatDialogRef<WalletTransactionsGiftComponent>,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public walletTransactionsService: WalletTransactionsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      giftFromAccountId: [null, Validators.compose([Validators.required])],
      giftToAccountId: [null, Validators.compose([Validators.required])],
      pointType: [0, Validators.compose([Validators.required])],
      note: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1000)])],
      amount: [0, Validators.compose([Validators.required, Validators.min(5000), Validators.max(10000000)])],
      files:[""]
    });
    this.pathUpload = this.walletTransactionsService.uploadPath();
  }

  ngOnInit() {
    this.initSearchFromAccount();
    this.initSearchToAccount();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  save() {
    this.isProcess.saveProcess = true;
    if (this.form.valid) {

      this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: "Khi đồng ý thì dữ liệu không thể cập nhật hay xóa, bạn có muốn thực hiện chức năng này?",
        panelClass: 'dialog-confirmation',
      }).afterClosed().subscribe(result => {
        if (result) {
          this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
          this.walletTransactionsService.gift(this.form.value).then((res) => {
            if (res.status == 1) {
              this.snackBar.open(res.message, this.baseTranslate["msg-success-type"], { duration: 5000 });
            }
            else {
              this.snackBar.open(res.message, this.baseTranslate["msg-danger-type"], { duration: 5000 });
            }
            this.close();
            this.isProcess.saveProcess = false;
          });
        }
        else
        {
          this.isProcess.saveProcess = false;
        }
      });
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  getOutPut(data: FileDataDTO[])
  {
      this.uploadFileOut = data;
  }

  initSearchFromAccount() {
    
    this.accountFromKey.valueChanges.subscribe(rs => {
        this.isProcess.accountFromSearching = false;
        if (rs != "") {
          this.isProcess.accountFromSearching = true;
          this.walletTransactionsService.searchByAccount({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.form.get('giftFromAccountId').setValue(null);
            }
            this.accountsFrom = rs.data;
            this.isProcess.accountFromSearching = false;
          });
        }
      },
        error => {
          this.isProcess.accountFromSearching = false;
      });
  }

  initSearchToAccount() {
    
    this.accountToKey.valueChanges.subscribe(rs => {
        this.isProcess.accountToSearching = false;
        if (rs != "") {
          this.isProcess.accountToSearching = true;
          this.walletTransactionsService.searchByAccount({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.form.get('giftToAccountId').setValue(null);
            }
            this.accountsTo = rs.data;
            this.isProcess.accountToSearching = false;
          });
        }
      },
        error => {
          this.isProcess.accountToSearching = false;
      });
  }
}
