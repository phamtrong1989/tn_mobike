import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { WalletTransactionsService } from '../wallet-transactions.service';
import { Account } from 'src/app/admin/customer/accounts/accounts.model';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-wallet-transactions-create',
  templateUrl: './wallet-transactions-create.component.html',
  providers: [
    WalletTransactionsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class WalletTransactionsCreateComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  public pathUpload: string;
  baseTranslate: any;
  accounts: Account[];
  uploadFileOut: FileDataDTO[];
  accountKey: FormControl = new FormControl("");
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountSearching: false };
  constructor(
    public dialogRef: MatDialogRef<WalletTransactionsCreateComponent>,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public walletTransactionsService: WalletTransactionsService,
    public appCommon: AppCommon,
    private translate: TranslateService
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      accountId: [0],
      type: [15],
      transactionCode: [null, Validators.compose([Validators.maxLength(50)])],
      note: ["Hệ thống hoàn điểm sau đối soát", Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1000)])],
      subAmount: [0, Validators.compose([Validators.required])],
      amount: [0, Validators.compose([Validators.required])],
      files:[""]
    });
    this.pathUpload = this.walletTransactionsService.uploadPath();
  }

  ngOnInit() {
    this.initSearchAccount();
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  save() {
    this.isProcess.saveProcess = true;

    if (this.form.valid) {
      this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: "Khi đồng ý thì dữ liệu không thể cập nhật hay xóa, bạn có muốn thực hiện chức năng này?",
        panelClass: 'dialog-confirmation',
      }).afterClosed().subscribe(result => {
        if (result) {
          this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
          this.walletTransactionsService.create(this.form.value).then((res) => {
            if (res.status == 1) {
              this.snackBar.open("Thêm giao dịch thành công", this.baseTranslate["msg-success-type"], { duration: 5000 });
              this.close();
            }
            else  if (res.status == 5) {
              this.snackBar.open("Bạn chưa nhập số điểm", this.baseTranslate["msg-success-type"], { duration: 5000 });
            }
            else {
              this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
            }
            this.isProcess.saveProcess = false;
          });
        }
        else
        {
          this.isProcess.saveProcess = false;
        }
      });
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  getOutPut(data: FileDataDTO[])
  {
      this.uploadFileOut = data;
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
        this.isProcess.accountSearching = false;
        if (rs != "") {
          this.isProcess.accountSearching = true;
          this.walletTransactionsService.searchByAccount({key: rs}).then(rs => {
            if (rs.data.length == 0) {
              this.form.get('accountId').setValue(null);
            }
            this.accounts = rs.data;
            this.isProcess.accountSearching = false;
          });
        }
      },
        error => {
          this.isProcess.accountSearching = false;
      });
  }
}
