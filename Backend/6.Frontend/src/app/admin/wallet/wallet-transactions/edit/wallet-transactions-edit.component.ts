import { Component, OnInit, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { AppCommon } from 'src/app/app.common';
import { AppStorage } from 'src/app/app.storage';
import { WalletTransactionsService } from '../wallet-transactions.service';
import { Account, WalletTransactionCommand } from 'src/app/admin/customer/accounts/accounts.model';
import { FileDataDTO } from 'src/app/app.settings.model';

@Component({
  selector: 'app-wallet-transactions-edit',
  templateUrl: './wallet-transactions-edit.component.html',
  providers: [
    WalletTransactionsService,
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class WalletTransactionsEditComponent implements OnInit {
  public form: FormGroup;
  public formCalendars: FormGroup;
  public categoryData: any;
  baseTranslate: any;
  accounts: Account[];
  accountKey: FormControl = new FormControl("");
  isProcess = { saveProcess: false, deleteProcess: false, initData: false, accountSearching: false };
  uploadFileOut: FileDataDTO[];

  constructor(
    public dialogRef: MatDialogRef<WalletTransactionsEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: WalletTransactionCommand,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public walletTransactionsService: WalletTransactionsService,
    public appCommon: AppCommon,
    private translate: TranslateService,
  ) {
    this.categoryData = this.appStorage.getCategoryData();
    this.initBaseTranslate();
    this.form = this.fb.group({
      id: [0],
      transactionCode: [null, Validators.compose([Validators.maxLength(50)])],
      adminNote: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1000)])],
      files: [""]
    });
  }

  ngOnInit() {
    this.form.get('id').setValue(this.data.id);
    this.form.get('adminNote').setValue(this.data.adminNote);
    this.form.get('transactionCode').setValue(this.data.transactionCode);

    if (this.data.files != null) {
      this.uploadFileOut = JSON.parse(this.data.files)
    }
    this.getData();
  }

  getData() {
    this.isProcess.initData = true;
    this.walletTransactionsService.getById(this.data.id).then(rs => {
      if (rs.status == 1 && rs.data) {
        this.data = new WalletTransactionCommand().deserialize(rs.data);
        this.form.get('id').setValue(this.data.id);
        this.form.get('adminNote').setValue(this.data.adminNote);
        this.form.get('transactionCode').setValue(this.data.transactionCode);

        if (this.data.files != null) {
          this.uploadFileOut = JSON.parse(this.data.files)
        }
        this.isProcess.initData = false;
      }
      else {
        this.snackBar.open(this.baseTranslate["msg-not-data"], this.baseTranslate["msg-warning-type"], { duration: 5000 });
        this.isProcess.initData = false;
      }
    });
  }

  getOutPut(data: FileDataDTO[]) {
    this.uploadFileOut = data;
  }

  initBaseTranslate() {
    this.translate.get(['base'])
      .subscribe(translations => {
        this.baseTranslate = translations['base'];
      });
  }

  save() {
    if (this.form.valid) {
      this.isProcess.saveProcess = true;
      this.form.get('files').setValue(JSON.stringify(this.uploadFileOut));
      this.walletTransactionsService.edit(this.form.value).then((res) => {
        if (res.status == 1) {
          this.snackBar.open("Cập nhật ghi chú thành công", this.baseTranslate["msg-success-type"], { duration: 5000 });
          this.close();
        }
        else {
          this.snackBar.open(this.baseTranslate["msg-danger"], this.baseTranslate["msg-danger-type"], { duration: 5000 });
        }
        this.isProcess.saveProcess = false;
      });
    }
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
      this.isProcess.accountSearching = false;
      if (rs != "") {
        this.isProcess.accountSearching = true;
        this.walletTransactionsService.searchByAccount({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.form.get('accountId').setValue(null);
          }
          this.accounts = rs.data;
          this.isProcess.accountSearching = false;
        });
      }
    },
      error => {
        this.isProcess.accountSearching = false;
      });
  }
}
