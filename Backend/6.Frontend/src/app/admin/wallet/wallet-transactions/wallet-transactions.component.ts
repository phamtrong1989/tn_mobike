import { Component, ViewChild, HostListener, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { WalletTransactionsService } from './wallet-transactions.service';
import { AppCommon } from 'src/app/app.common';
import { Settings } from 'src/app/app.settings.model';
import { AppSettings } from 'src/app/app.settings';
import { AppStorage } from 'src/app/app.storage';
import { Account, WalletTransaction, WalletTransactionCommand } from '../../customer/accounts/accounts.model';
import { WalletTransactionsCreateComponent } from './create/wallet-transactions-create.component';
import { WalletTransactionsEditComponent } from './edit/wallet-transactions-edit.component';
import { FormControl } from '@angular/forms';
import { WalletTransactionsGiftComponent } from './gift/wallet-transactions-gift.component';
import { WalletTransactionsArrearsComponent } from './arrears/wallet-transactions-arrears.component';
@Component({
  selector: 'app-wallet-transactions',
  templateUrl: './wallet-transactions.component.html',
  providers: [WalletTransactionsService, AppCommon]
})

export class WalletTransactionsComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 700;
  listData: WalletTransaction[];
  accounts: Account[];
  accountKey: FormControl = new FormControl("");
  isProcess = { dialogOpen: false, listDataTable: true, accountSearching: false };
  params: any = { pageIndex: 1, pageSize: 20, sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "type", "account", "transactionCode", "totalAmount", "wallet", "note", "adminNote", "files", "status", "createdDate", "updatedDate", "action"];
  public settings: Settings;
  public categoryData: any;
  constructor(
    public appSettings: AppSettings,
    public walletTransactionsService: WalletTransactionsService,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage
  ) {
    this.settings = this.appSettings.settings;
    this.categoryData = this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.params = this.appCommon.urlToJson(this.params, location.search);

    this.params.start = !this.params.start ? this.appCommon.formatDateTime(new Date(), 'yyyy-MM-dd') : this.params.start;
    this.params.end = !this.params.end ? this.appCommon.formatDateTime(new Date(), 'yyyy-MM-dd') : this.params.end;

    this.initSearchAccount();
    this.searchPage();
  }

  searchPage(): void {
    this.isProcess.listDataTable = true;
    this.params.start = this.appCommon.formatDateTime(this.params.start, 'yyyy-MM-dd');
    this.params.end = this.appCommon.formatDateTime(this.params.end, 'yyyy-MM-dd');
    this.appCommon.changeUrl(location.pathname, this.params);
    this.walletTransactionsService.searchPaged(this.params).then((res) => {
      this.listData = res.data;
      this.paginator.length = res.count;
      this.paginator.pageIndex = res.pageIndex - 1;
      this.isProcess.listDataTable = false;
      this.paginator.pageSize = this.params.pageSize;
      if (res.outData != null && (this.accounts == null || this.accounts.length == 0)) {
        this.accounts = [];
        this.accounts.push(res.outData);
        this.params.accountId = res.outData.id;
      }
    });
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction ? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      if (window.innerHeight > 550) {
        this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top - this.paginator_rec.nativeElement.offsetHeight - 20;
      }
      else {
        this.heightTable = 600;
      }
    }
  }

  openEditDialog(data: WalletTransaction) {
    var model = new WalletTransactionCommand().deserialize(data);
    this.dialog.open(WalletTransactionsEditComponent, {
      data: model,
      panelClass: 'dialog-600',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  openCreate() {
    this.dialog.open(WalletTransactionsCreateComponent, {
      panelClass: 'dialog-600',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  
  openArrears() {
    this.dialog.open(WalletTransactionsArrearsComponent, {
      panelClass: 'dialog-600',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }


  openGift() {
    this.dialog.open(WalletTransactionsGiftComponent, {
      panelClass: 'dialog-600',
      disableClose: true,
      autoFocus: false
    }).afterClosed().subscribe(outData => {
      this.searchPage();
    });
  }

  getFilesCount(files: string) {
    try {
      if (files == null) {
        return 0;
      }
      var objects = JSON.parse(files);
      return objects.length;
    }
    catch
    {
      return 0;
    }
  }

  initSearchAccount() {
    this.accountKey.valueChanges.subscribe(rs => {
      this.isProcess.accountSearching = false;
      if (rs != "") {
        this.isProcess.accountSearching = true;
        this.walletTransactionsService.searchByAccount({ key: rs }).then(rs => {
          if (rs.data.length == 0) {
            this.params.accountId = "";
          }
          this.accounts = rs.data;
          this.isProcess.accountSearching = false;
        });
      }
    },
      error => {
        this.isProcess.accountSearching = false;
      });
  }

  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;
  @ViewChild('paginator_rec', { static: false }) paginator_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}