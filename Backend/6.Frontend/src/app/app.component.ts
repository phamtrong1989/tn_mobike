import { Component } from '@angular/core';
import { AppSettings, AppClientSettings } from './app.settings';
import { Settings } from './app.settings.model';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { AppStorage } from './app.storage'
import { AppService } from './app.service';
import { AppCommon } from './app.common'
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router
} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    AppService,
    AppCommon,
    AppClientSettings
    // ,
    // { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    // { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    // { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class AppComponent {
  public settings: Settings;

  constructor(
    public appSettings: AppSettings,
    public appStorage: AppStorage,
    public appService: AppService,
    private translate: TranslateService,
    private router: Router
  ) {
    this.settings = this.appSettings.settings;
    translate.setDefaultLang('vi');

    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.settings.loadingSpinner = true; 
          break;
        }
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.settings.loadingSpinner = false; 
          break;
        }
        default: {
          break;
        }
      }
    });


  }

  ngOnInit() {
    // this.initData();
  }

  // initData() {
  //   if (this.appStorage.getAccountInfo() != null) {
  //     this.appService.categoryVersion().subscribe(rs => {
  //       if (rs.data != this.appStorage.getCategoryVersion()) {
  //         this.appService.pullData().subscribe(outData => {
  //           if (outData) {
  //             this.appStorage.setCategoryData(outData.data);
  //             this.appStorage.setCategoryVersion(rs.data);
  //           }
  //         });
  //       }
  //     });
  //   }
  // }
}
