import { Language } from './admin/category/languages/languages.model';
import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from './admin/user/users/users.model';
import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Station } from './admin/infrastructure/stations/stations.model';
import { CustomerGroup, Project } from './admin/category/projects/projects.model';
import { District } from './admin/category/districts/districts.model';
import { Investor } from './admin/category/investors/investors.model';
import { City } from './admin/category/citys/citys.model';
import { Model } from './admin/category/models/models.model';
import { DatePipe, formatDate } from '@angular/common';
import { RoleController } from './admin/user/role-controllers/role-controllers.model';

@Injectable()
export class AppCommon {

  jsonToQueryString(json: object) {
    return '?' +
      Object.keys(json).map(function (key) {
        return encodeURIComponent(key) + '=' +
          encodeURIComponent(json[key]);
      }).join('&');
  }

  moneyInputFormat() {
    return {
      align: "left",
      allowNegative: true,
      allowZero: true,
      decimal: ",",
      precision: 0,
      prefix: "",
      suffix: "",
      thousands: ".",
      nullable: false
    };
  }

  changeUrl(url: string, json: object) {
    window.history.pushState('page2', 'Title', url + this.jsonToQueryString(json));
  }

  finterUser(users: User[], roleType: number) {
    return users.filter(x => x.roles != null && x.roles.length > 0 && x.roles.map(m => m.type).indexOf(roleType) >= 0);
  }

  formatDateTime(inDate: any, fm: string) {
    if (inDate == null || inDate == undefined || inDate == "null") {
      return "";
    }
    var datePipe = new DatePipe("vi-VN");
    return datePipe.transform(inDate, fm);
  }

  dateAddDay(inDate: Date, days: number): Date {
    var newdate = new Date(inDate);
    newdate.setDate(newdate.getDate() + days);
    return newdate;
  }

  daysInMonth(year: number, month: number) {
    return new Date(year, month, 0).getDate();
  }

  urlToJson(defaultObject: any, url: string): any {

    if (url == null || url == undefined || url == "") {
      return defaultObject;
    }
    try {
      var hash: any[];
      var myJson = {};
      var hashes = url.slice(url.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        myJson[hash[0]] = decodeURIComponent(hash[1]);
        // If you want to get in native datatypes
        // myJson[hash[0]] = JSON.parse(hash[1]); 
      }
      return myJson;
    }
    catch
    {
      return defaultObject;
    }
  }

  urlToBasePath(url: string) {

    var hashes = url.split('?');
    if (url.length > 1) {
      return hashes[0];
    }
    return url;
  }

  initHeightTable(tagid: string, trudi: number): number {
    let getTop = document.getElementById(tagid).getBoundingClientRect().top;
    return window.innerHeight - getTop - trudi;
  }

  getHeaders(params: any) {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    return {
      params: this.cleanOject(params), headers: myHeaders
    }
  }

  getHeadersText(params: any) {
    return {
      params: this.cleanOject(params),
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      responseType: 'text'
    }
  }

  getHeadersDowload() {
    return new HttpHeaders({
      'Content-Type': 'application/pdf',
      responseType: 'blob',
      Accept: 'application/xlxs',
      observe: 'response'
    })
  }

  cleanOject(obj: any) {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === "null" || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj;
  }

  pad(number: number, length: number) {
    var str = '' + number;
    while (str.length < length) {
      str = '0' + str;
    }
    return str;
  }

  toAfterMoney(input: number, discountAmount: number, discountUnitType: number) {
    if (discountUnitType == 0) {
      return parseFloat((input - discountAmount).toString()).toFixed(0)
    }
    else {
      return parseFloat((input - ((discountAmount * input) / 100)).toString()).toFixed(0)
    }
  }

  toAfterMoneyStr(input: number, discountAmount: number, discountUnitType: number) {
    var total = "";
    if (discountUnitType == 0) {
      total = parseFloat((input - discountAmount).toString()).toFixed(0);
    }
    else {
      total = parseFloat((input - ((discountAmount * input) / 100)).toString()).toFixed(0)
    }
    if (total == "") {
      return "";
    }
    return this.formatMoney(parseInt(total), 0, 3);
  }

  formatMoney(value: number, n, x) {
    if (value == null) {
      return "0";
    }
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\,' : '$') + ')';
    return value.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&.');
  }

  fMoney(value: number) {
    var n = 0;
    var x = 3;
    if (value == null) {
      return "0";
    }
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\,' : '$') + ')';
    return value.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&.');
  }

  getDiscount(items: CustomerGroup[], ids: string) {
    if (ids == "" || ids == null) {
      return "---";
    }
    var arrayId = ids.split(',').map(Number);
    var dl = items.filter(x => arrayId.indexOf(x.id) >= 0);
    return dl.map(x => x.name).join(', ');
  }

  getListAssistant(users: User[], ids: string) {
    if (ids == "" || ids == null || ids == undefined) {
      return [];
    }
    var arrayId = ids.split(',').map(Number);
    return users.filter(x => arrayId.indexOf(x.id) >= 0);
  }

  getUser(users: User[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = users.find(x => x.id == id);
    if (dl != null) {
      return dl.displayName;
    }
    else {
      return "---";
    }
  }

  getInvestor(items: Investor[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);
    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getLanguages(items: Language[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);
    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getCity(items: City[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);
    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getDistrict(items: District[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);
    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getDoctor(users: User[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = users.find(x => x.id == id);
    if (dl != null) {
      return dl.displayName;
    }
    else {
      return "---";
    }
  }

  getCustomerGroup(items: CustomerGroup[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);

    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getProject(items: Project[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);

    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getTenant(items: any, id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);

    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getStation(items: Station[], id: number) {
    if (id == null || id == 0) {
      return "---";
    }

    var dl = items.find(x => x.id == id);

    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  isDisabledControll(form: FormGroup, controlName: string, value: boolean) {
    if (value) {
      form.get(controlName).disable();
    } else {
      form.get(controlName).enable();
    }
  }

  getColor(items: any, id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);

    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getModel(items: any, id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);

    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }

  getObj(items: any, id: number) {
    if (id == null || id == 0) {
      return "---";
    }
    var dl = items.find(x => x.id == id);

    if (dl != null) {
      return dl.name;
    }
    else {
      return "---";
    }
  }
  
    getObjString(items: any, id: string) {
      if (id == null || id == "") {
        return "---";
      }
      var dl = items.find(x => x.id == parseInt(id));
      if (dl != null) {
        return dl.name;
      }
      else {
        return "---";
      }
    }
}


@Pipe({ name: 'userFilter' })
@Injectable()
export class UserFilter implements PipeTransform {
  transform(users: User[], key: string): any {
    if (key == null || key == undefined || key == "") {
      return users;
    }
    return users.filter(treatmentService => treatmentService.displayName.toLowerCase().indexOf(key.toLowerCase()) !== -1);
  }
}

@Pipe({
  name: 'power'
})
export class PowerPipe implements PipeTransform {
  transform(base: number, exponent: number): number {
    return Math.pow(base, exponent);
  }
}

@Pipe({ name: 'safeHtml' })
export class Safe {
  constructor(private sanitizer: DomSanitizer) { }
  transform(value: any, args?: any): any {
    return this.sanitizer.bypassSecurityTrustHtml(value);
    // return this.sanitizer.bypassSecurityTrustStyle(style);
    // return this.sanitizer.bypassSecurityTrustXxx(style); - see docs
  }
}


@Pipe({ name: 'districtFilter' })
@Injectable()
export class DistrictFilter implements PipeTransform {
  transform(districts: District[], filter: any): any {
    var newList = districts;
    if (filter.key != null && filter.key != undefined && filter.key != "") {
      newList = districts.filter(x => x.name.toLowerCase().indexOf(filter.key.toLowerCase()) !== -1);
    }
    if (filter.cityId > 0) {
      newList = districts.filter(x => x.cityId == filter.cityId);
    }
    return newList;
  }
}

@Pipe({ name: 'modelFilter' })
@Injectable()
export class ModelFilter implements PipeTransform {
  transform(districts: Model[], filter: any): any {
    var newList = districts;
    if (filter.key != null && filter.key != undefined && filter.key != "") {
      newList = districts.filter(x => x.name.toLowerCase().indexOf(filter.key.toLowerCase()) !== -1);
    }
    if (filter.producerId >= 0) {
      newList = districts.filter(x => x.producerId == filter.producerId);
    }
    return newList;
  }
}

@Pipe({ name: 'stationByProjectFilter' })
@Injectable()
export class StationByProjectFilter implements PipeTransform {
  transform(list: Station[], filter: any): any {
    var newList = list;
    if (filter.key != null && filter.key != undefined && filter.key != "") {
      newList = list.filter(x => x.projectId == filter.projectId);
    }
    newList = list.filter(x => x.projectId == filter.projectId);
    return newList;
  }
}

@Pipe({ name: 'customerGroupsByProjectFilter' })
@Injectable()
export class CustomerGroupsByProjectFilter implements PipeTransform {
  transform(list: CustomerGroup[], filter: any): any {
    var newList = list;
    if (filter.key != null && filter.key != undefined && filter.key != "") {
      newList = list.filter(x => x.projectId == filter.projectId);
    }
    newList = list.filter(x => x.projectId == filter.projectId);
    return newList;
  }
}



@Pipe({ name: 'roleControllersFilter' })
@Injectable()
export class RoleControllersFilter implements PipeTransform {
  transform(data: RoleController[], filter: any): any {
    var newList = data;
    if (filter.key != null && filter.key != undefined && filter.key != "") {
      newList = data.filter(x => x.name.toLowerCase().indexOf(filter.key.toLowerCase()) !== -1);
    }
    if (filter.groupId > -1) {
      newList = data.filter(x => x.groupId == filter.groupId);
    }
    return newList;
  }
}
