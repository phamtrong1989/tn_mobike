import { Menu } from './menu.model';

export const verticalMenuItems = [
    new Menu(1, 'Trang tổng hợp', '/', null, 'dashboard', null, false, 0, "10,131,132,134"),

    new Menu(300, 'Quản lý chuyến đi', null, null, 'directions_bike', null, true, 0, "14,16,18,127,135,136,137"),
    new Menu(301, 'Chuyến đi đang thuê', '/admin/transaction/bookings', null, '', null, false, 300, "14,135,136,137"),
    new Menu(304, 'Chuyến đi hoàn thành', '/admin/transaction/transactions', null, '', null, false, 300, "18"),
    new Menu(304, 'Chuyến đi nợ cước', '/admin/transaction/transactions-debt', null, '', null, false, 300, "127"),

    new Menu(2803, 'Quản lý giao dịch', '/admin/wallet/wallet-transactions', null, 'money', null, false, 0, "95,97,138,139"),

    new Menu(100, 'Khách hàng', null, null, 'supervisor_account', null, true, 0, "26,28,29,30,32,140,142,143,151,152,153,154,155,156"),
    new Menu(101, 'Tra cứu khách hàng', '/admin/customer/accounts', null, '', null, false, 100, "26,28,29,151,152,153,154,155,156"),
    new Menu(101, 'Quản lý & xử lý khiếu nại(not)', '/admin/customer/complaint-management', null, '', null, false, 100, "140,142,143"),
    new Menu(103, 'Vé trả trước', '/admin/customer/ticket-prepaids', null, '', null, false, 100, "30,32"),

    new Menu(500, 'Chương trình khuyến mại', null, null, 'redeem', null, true, 0, "22,24,25,144"),
    new Menu(501, 'Tra cứu chương trình khuyến mại', '/admin/project/campaigns', null, 'redeem', null, false, 500, "22,24,25"),
    new Menu(502, 'Kết quả chương trình khuyến mại', '/admin/project/campaign-results', null, 'redeem', null, false, 500, "144"),

    new Menu(3703, 'Gửi thông báo hệ thống', '/admin/notification/notification-jobs', null, 'message', null, false, 0, "115,117,118"),

    new Menu(1000, 'Lịch sử báo xe hỏng', '/admin/bike-reports', null, 'report_problem', null, false, 0, "178,179,180"),

    new Menu(400, 'Quản lý hạ tầng', null, null, 'apartment', null, true, 0, "38,40,41,42,44,45,46,48,49,50,128,130,131,157,159,160,170,172,173,181,182,183"),
    new Menu(402, 'Quản lý trạm', '/admin/infrastructure/stations', null, '', null, false, 400, "38,40,41"),
    new Menu(403, 'Quản lý Xe', '/admin/infrastructure/bikes', null, '', null, false, 400, "42,44,45"),
    new Menu(404, 'Quản lý Khóa/SIM', '/admin/infrastructure/docs', null, '', null, false, 400, "46,48,49,50"),
    new Menu(404, 'Quản lý Vật tư', '/admin/infrastructure/suppliess', null, '', null, false, 400, "170,172,173"),
    new Menu(405, 'Quản lý nhập kho', '/admin/infrastructure/import-bills', null, '', null, false, 400, "157,159,160"),
    new Menu(406, 'Quản lý xuất kho', '/admin/infrastructure/export-bills', null, '', null, false, 400, "181,182,183"),
    new Menu(407, 'Quản lý sửa chữa bảo dưỡng(not)', '/admin/infrastructure/maintenance-repair', null, '', null, false, 400, ""),

    new Menu(1100, 'Báo cáo thống kê', null, null, 'dns', null, true, 0, "175"),
    new Menu(1101, 'Báo cáo doanh thu tiền nạp', '/admin/report/revenue', null, '', null, false, 1100, "175"),
    new Menu(1102, 'Báo cáo quản lý chuyến đi', '/admin/report/transaction', null, '', null, false, 1100, ""),
    new Menu(1103, 'Báo cáo chương trình khuyến mại', '/admin/report/campaign', null, '', null, false, 1100, ""),
    new Menu(1104, 'Báo cáo chăm sóc khách hàng', '/admin/report/customer', null, '', null, false, 1100, ""),
    new Menu(1105, 'Báo cáo quản lý hạ tầng', '/admin/report/infrastructure', null, '', null, false, 1100, ""),
    new Menu(1106, 'Báo cáo quản lý danh mục', '/admin/report/category', null, '', null, false, 1100, ""),

    new Menu(600, 'Quản lý danh mục', null, null, 'archive', null, true, 0, "51,55,63,67,71,75,79,83,120,122,123,124,126,127"),
    new Menu(601, 'Dự án', '/admin/category/projects', null, '', null, false, 600, "51"),
    new Menu(602, 'Đơn vị đầu tư', '/admin/category/investors', null, '', null, false, 600, "55"),
    new Menu(603, 'Tỉnh/thành phố', '/admin/category/cities', null, '', null, false, 600, "63"),
    new Menu(604, 'Quận/huyện', '/admin/category/districts', null, '', null, false, 600, "67"),
    new Menu(605, 'Mã màu', '/admin/category/colors', null, '', null, false, 600, "71"),
    new Menu(606, 'Mẫu mã trang thiết bị', '/admin/category/models', null, '', null, false, 600, "75"),
    new Menu(607, 'Nhà sản xuất trang thiết bị', '/admin/category/producers', null, '', null, false, 600, "79"),
    new Menu(608, 'Ngôn ngữ', '/admin/category/languages', null, '', null, false, 600, "83"),
    new Menu(609, 'Kho', '/admin/category/warehouses', null, '', null, false, 600, "120,122,123"),
    new Menu(610, 'Cấu hình đổi điểm', '/admin/category/redeem-points', null, '', null, false, 600, "124,126,127"),

    new Menu(700, 'Quản lý nội dung', null, null, 'description', null, true, 0, "107,109,110,111,113,114"),
    new Menu(701, 'Nội dung tĩnh', '/admin/contents/content-pages', null, '', null, false, 700, "107,109,110"),
    new Menu(702, 'Tin tức', '/admin/contents/news', null, '', null, false, 700, "111,113,114"),

    new Menu(900, 'Quản trị hệ thống', null, null, 'account_circle', null, true, 0, "1,91,87"),
    new Menu(901, 'Quản lý nhân viên', '/admin/users', null, '', null, false, 900, "1"),
    new Menu(902, 'Xem log', '/admin/log/mongo', null, '', null, false, 900, "91"),
    new Menu(902, 'Quyền  tài khoản', '/admin/roles', null, '', null, false, 900, "87"),
    
    new Menu(906, 'Role Controllers', '/admin/role-controllers', null, '', null, false, 900, "8888"),
    new Menu(903, 'Role Group', '/admin/role-groups', null, '', null, false, 900, "8888"),
    new Menu(905, 'Role Areas', '/admin/role-areas', null, '', null, false, 900, "8888")
]
export const horizontalMenuItems = verticalMenuItems
