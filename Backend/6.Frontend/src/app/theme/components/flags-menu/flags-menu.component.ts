import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { TranslateService } from '@ngx-translate/core';
import { AppCommon } from 'src/app/app.common';

@Component({
  selector: 'app-flags-menu',
  templateUrl: './flags-menu.component.html',
  styleUrls: ['./flags-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlagsMenuComponent implements OnInit {

  public settings: Settings;
  time: string = "--:-- --/--/----";
  constructor(
    public appSettings:AppSettings,
    private translate: TranslateService,
    private appCommon: AppCommon
    ){
      this.settings = this.appSettings.settings; 
      this.time = this.appCommon.formatDateTime(new Date(),'HH:mm:ss dd/MM/yyyy');
  }

  ngOnInit() {
    setInterval(() => {
      this.time = this.appCommon.formatDateTime(new Date(),'HH:mm:ss dd/MM/yyyy');
    }, 1000);
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }
}
