import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { MessagesDetailsComponent } from './details/messages-details.component';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: []
})
export class MessagesComponent implements OnInit {  
  @ViewChild(MatMenuTrigger, { static: false }) trigger: MatMenuTrigger;
  public selectedTab:number=1;
  params: any = { customerId: null, key: null};
  public messages:Array<Object> = [];
  public files:Array<Object> = [];
  public meetings:Array<Object> = []; 
  public clientSettings: ClientSettings;
  totalData: number = 0;
  visibleAppend : boolean  = false;
  constructor(
    appClientSettings: AppClientSettings,
    public dialog: MatDialog,
    ) { 
    this.clientSettings = appClientSettings.settings
  }

  ngOnInit()  {

    this.getCountMSG();
    setInterval(() => {
     this.getCountMSG();
    }, 5000);
  }

  getCountMSG()
  {
  }

  getFullUrl(url:string) {
    return this.clientSettings.serverAPI + url;
  }
  
  openMessagesMenu() {
    this.trigger.openMenu();
    this.selectedTab = 0;
  }

  onMouseLeave(){
    this.trigger.closeMenu();
  }

  stopClickPropagate(event: any){
    event.stopPropagation();
    event.preventDefault();
  }

}
