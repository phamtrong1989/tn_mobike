import { Component, OnInit, Inject, ElementRef, ViewChild, HostListener } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material/snack-bar';
import { AppCommon } from '../../../../app.common'
import { AppStorage } from '../../../../app.storage'
import { ConfirmationDialogComponent } from 'src/app/theme/components/confirmation-dialog/confirmation-dialog.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { url } from 'inspector';
import { MatPaginator} from '@angular/material/paginator';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';

import { NumberCardModule } from '@swimlane/ngx-charts';
import {Location} from '@angular/common'; 
import { HttpParameterCodec } from "@angular/common/http";
import { MatSort } from '@angular/material/sort';
@Component({
  selector: 'app-messages-details',
  templateUrl: './messages-details.component.html',
  providers: [
    AppCommon,
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class MessagesDetailsComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  heightTable: number = 500;
  isProcess = { dialogOpen: false, listDataTable: true };
  params: any = { pageIndex: 1, pageSize: 100, key: "", customerId : null , sortby: "", sorttype: "" };
  loadingIndicator: boolean = true;
  displayedColumns = ["stt", "subject"];
  public settings: Settings;
  public categoryData :any;
  public router: Router
  constructor(
    public dialogRef: MatDialogRef<MessagesDetailsComponent>,
    public appSettings: AppSettings,
    public dialog: MatDialog,
    private appCommon: AppCommon,
    public snackBar: MatSnackBar,
    public appStorage: AppStorage,
    public _router: Router,
    private location: Location
  ) {
    this.router =_router;
    this.settings = this.appSettings.settings;
	  this.categoryData =  this.appStorage.getCategoryData();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.searchPage();
  }

  searchPage(): void {
    
  }

  close(input: any = null): void {
    if (input != null) {
      this.dialogRef.close(input);
    }
    else {
      this.dialogRef.close();
    }
  }

  search() {
    this.params.pageIndex = 1;
    this.searchPage();
  }

  handleSortData(e: any) {
    this.params.pageIndex = 1;
    this.params.sortby = e.direction? e.active : '';
    this.params.sorttype = e.direction;
    this.searchPage();
  }

  handlePage(e: any) {
    if (e.pageSize != this.params.pageSize) {
      this.params.pageIndex = 1;
    }
    else {
      this.params.pageIndex = e.pageIndex + 1;
    }
    this.params.pageSize = e.pageSize;
    this.searchPage();
  }


  ngAfterViewInit() {
     setTimeout(() => {
    }, 1);
  }

  goLink(url: string)
  {
    // this.location.replaceState('/');
    var newUrl ='/redirect?newredirect=' + encodeURIComponent(url);
    console.log(newUrl);
    this.router.navigateByUrl(newUrl);
  }
}
