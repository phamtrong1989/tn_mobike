import { FormGroup, FormControl, ValidationErrors, AbstractControl, ValidatorFn } from '@angular/forms';

export function emailValidator(control: FormControl): { [key: string]: any } {
   var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
   if (control.value && !emailRegexp.test(control.value)) {
      return { invalidEmail: true };
   }
}

export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
   return (group: FormGroup) => {
      let password = group.controls[passwordKey];
      let passwordConfirmation = group.controls[passwordConfirmationKey];
      if (password.value !== passwordConfirmation.value) {
         return passwordConfirmation.setErrors({ mismatchedPasswords: true })
      }
   }
}


export class CustomValidator {
   // Validates URL
   static urlValidator(url): any {
      if (url.pristine) {
         return null;
      }
      const URL_REGEXP = /^(http?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
      url.markAsTouched();
      if (URL_REGEXP.test(url.value)) {
         return null;
      }
      return {
         invalidUrl: true
      };
   }
   // Validates passwords
   static matchPassword(group): any {
      const password = group.controls.password;
      const confirm = group.controls.confirm;
      if (password.pristine || confirm.pristine) {
         return null;
      }
      group.markAsTouched();
      if (password.value === confirm.value) {
         return null;
      }
      return {
         invalidPassword: true
      };
   }
   // Validates numbers
   static numberValidator(number): any {
      if (number.pristine) {
         return null;
      }
      const NUMBER_REGEXP = /^-?[\d.]+(?:e-?\d+)?$/;
      number.markAsTouched();
      if (NUMBER_REGEXP.test(number.value)) {
         return null;
      }
      return {
         invalidNumber: true
      };
   }
   // Validates US SSN
   static ssnValidator(ssn): any {
      if (ssn.pristine) {
         return null;
      }
      const SSN_REGEXP = /^(?!219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4}$/;
      ssn.markAsTouched();
      if (SSN_REGEXP.test(ssn.value)) {
         return null;
      }
      return {
         invalidSsn: true
      };
   }
   // Validates US phone numbers
   static phoneValidator(number): any {
      if (number.pristine) {
         return null;
      }
      const PHONE_REGEXP = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
      number.markAsTouched();
      if (PHONE_REGEXP.test(number.value)) {
         return null;
      }
      return {
         invalidNumber: true
      };
   }
   // Validates zip codes
   static zipCodeValidator(zip): any {
      if (zip.pristine) {
         return null;
      }
      const ZIP_REGEXP = /^[0-9]{5}(?:-[0-9]{4})?$/;
      zip.markAsTouched();
      if (ZIP_REGEXP.test(zip.value)) {
         return null;
      }
      return {
         invalidZip: true
      };
   }

   static passwordStrongValidator(val: any): any {
      if (val.pristine) {
         return null;
      }
      const _REGEXP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{5,15}$/;
      val.markAsTouched();
      if (_REGEXP.test(val.value)) {
         return null;
      }
      return {
         invalidPasswordStrong: true
      };
   }

   static comparePasswordValidator(val: any, newPassword: any): any {
      console.log(newPassword);
      if (val.pristine) {
         return null;
      }

      if (val.value = newPassword.value) {
         return null;
      }
      return {
         invalidComparePassword: true
      };
   }

   public static matchValues(
      matchTo: string // name of the control to match to
   ): (AbstractControl) => ValidationErrors | null {
      return (control: AbstractControl): ValidationErrors | null => {
         return !!control.parent &&
            !!control.parent.value &&
            control.value === control.parent.controls[matchTo].value
            ? null
            : { isMatching: false };
      };
   }

   static fromToDate(fromDateField: string, toDateField: string, errorName: string = 'fromToDate'): ValidatorFn {
      return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
         try {
            const fromDate = formGroup.get(fromDateField).value;
            const toDate = formGroup.get(toDateField).value;
            if ((fromDate !== null && toDate !== null) && fromDate > toDate) {
               return { [errorName]: true };
            }
         }
         catch
         {
         }
         return null;
      };
   }
}