import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppStorage } from 'src/app/app.storage';
import { AppCommon } from 'src/app/app.common';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Settings } from '../../app.settings.model';
import { AccountData } from '../../admin/account/account.model';
import { Station } from '../../admin/infrastructure/stations/stations.model';
import { AppSettings } from '../../app.settings';
import { PublicService } from './map.service';
import { Bike } from 'src/app/admin/infrastructure/bikes/bikes.model';
@Component({
  selector: 'app-public-map',
  templateUrl: './map.component.html',
  providers: [AppCommon, PublicService]
})
export class MapComponent implements OnInit, OnDestroy {
  location: Location
  public settings: Settings;
  accountInfo: AccountData;
  viewType: number = 0;
  isProcess = { dialogOpen: false, listDataTable: true, cancelBooking: false, endBooking: false, openLock: false };
  heightTable: number = 900;
  heightTabTable: number = 210;
  stations: Station[];
  bikes: Bike[];

  currentStationMarker: StationMarker;
  currentBike: BikeMarker;

  iconStation: string = '../assets/img/vendor/leaflet/marker-icon.png';
  iconBike: string = '../assets/img/vendor/leaflet/bike-icon-3.png';
  categoryData: any;
  heightListBikeRight: number = 514;
  mapStyle: any = [
    { featureType: "road", stylers: [{ visibility: "on" }] },
    { featureType: "poi", stylers: [{ visibility: "off" }] },
    { featureType: "transit", stylers: [{ visibility: "on" }] },
    { featureType: "administrative", stylers: [{ visibility: "on" }] },
    { featureType: "administrative.locality", stylers: [{ visibility: "off" }] }
  ];
  showMarkerParams: any = { booking: true, station: true }
  intevalStation: any;
  intevalBike: any;

  constructor(
    public appSettings: AppSettings,
    public appStorage: AppStorage,
    public publicService: PublicService,
    public appCommon: AppCommon,
    public dialog: MatDialog
  ) {
    this.settings = this.appSettings.settings;
    this.accountInfo = this.appStorage.getAccountInfo();
  }

  ngOnDestroy(): void {
    if (this.intevalStation) {
      clearInterval(this.intevalStation);
    }
    
    if (this.intevalBike) {
      clearInterval(this.intevalBike);
    }
  }

  initData() {
    this.isProcess.dialogOpen = true;
    this.location = {
      latitude: 10.762819204178118,
      longitude: 106.69847308186436,
      mapType: "street view",
      zoom: 14,
      stationMarkers: [],
      bikeMarkers:[]
    }

    setTimeout(() => {
      this.getStations(true);
      //this.getBike(true);
    }, 500);
  }

  ngOnInit() {
    this.initData();
    this.setInteval();
  }

  getBike(isFirst: boolean = false) {
    this.publicService.bikes().then((res) => {
      this.bikes = res.data;
      this.location.bikeMarkers.forEach(element => {
        var ktTonTai = this.bikes.find(x => x.id == element.bike?.id);
        if (ktTonTai == null) {
          element.isEnable = false;
        }
      });
      // Kiểm tra nếu curent ko tồn tại nữa thì đóng set = null
      if (this.currentBike != null && this.location.bikeMarkers.find(x => this.currentBike?.bike?.id == x.bike?.id && x.isEnable == true) == null) {
        this.currentBike.isOpen = false;
        this.currentBike = null;
      }
      // Cập nhật marker
      this.bikes.forEach(item => {
        this.addBikeMarker(item.lat, item.long, item.plate, this.iconBike, false, item, true, true);
      });
      this.isProcess.dialogOpen = false;
    });
  }

  getStations(isFirst: boolean = false) {
    if (isFirst) {
      this.isProcess.dialogOpen = true;
    }

    this.publicService.stations(0).then((res) => {
      this.stations = res.data;
      // Kiểm tra marker nào dữ liệu trả về không có thì xóa bỏ đi
      this.location.stationMarkers.forEach(element => {
        var ktTonTai = this.stations.find(x => x.id == element.station?.id);
        if (ktTonTai == null) {
          element.isEnable = false;
        }
      });
      // Kiểm tra nếu curent ko tồn tại nữa thì đóng set = null
      if (this.currentStationMarker != null && this.location.stationMarkers.find(x => this.currentStationMarker?.station?.id == x.station?.id && x.isEnable == true) == null) {
        this.currentStationMarker.isOpen = false;
        this.currentStationMarker = null;
      }
      // Cập nhật marker
      this.stations.forEach(station => {
        this.addStationMarker(station.lat, station.lng, station.name, this.iconStation, false, station, true, true);
      });
      this.isProcess.dialogOpen = false;
    });
  }

  setInteval() {
    this.intevalStation = setInterval(() => this.getStations(), 30000);
    // this.intevalBike = setInterval(() => this.getBike(), 20000);
  }

  selectStationMarker(marker: StationMarker) {
    if (!marker.isEnable) {
      marker.isOpen = true;
      return;
    }

    this.viewType = 1;
    if (this.currentStationMarker != null) {
      this.currentStationMarker.isOpen = false;
    }
    this.currentStationMarker = marker;
    this.location.latitude = marker.lat;
    this.location.longitude = marker.lng;
    marker.isOpen = true;
  }

  selectStation(station: Station) {
    var getMarker = this.location.stationMarkers.find(x => x.station.id == station.id);
    if (getMarker != null) {
      this.selectStationMarker(getMarker);
    }
  }

  addStationMarker(lat: number, lng: number, label: string, iconUrl: string, isOpen: boolean, station: Station, isEnable: boolean, isShow: boolean) {
    var isSelect = false;
    var ktTT = this.location.stationMarkers.find(x => x.station?.id == station.id);
    if (ktTT == null) {
      this.location.stationMarkers.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        station,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.station = station;
      ktTT.isEnable = true;
      if (this.currentStationMarker != null && this.currentStationMarker.station?.id == station.id) {
        this.currentStationMarker = ktTT;
      }
    }
  }

  addBikeMarker(lat: number, lng: number, label: string, iconUrl: string, isOpen: boolean, bike: Bike, isEnable: boolean, isShow: boolean) {
    console.log(iconUrl);
    var isSelect = false;
    var ktTT = this.location.bikeMarkers.find(x => x.bike?.id == bike.id);
    if (ktTT == null) {
      this.location.bikeMarkers.push({
        lat,
        lng,
        label,
        iconUrl,
        isOpen,
        isSelect,
        bike,
        isEnable,
        isShow
      });
    }
    else {
      ktTT.lat = lat;
      ktTT.lng = lng;
      ktTT.label = label;
      ktTT.iconUrl = iconUrl;
      ktTT.bike = bike;
      ktTT.isEnable = true;
      if (this.currentStationMarker != null && this.currentStationMarker.station?.id == bike.id) {
        this.currentBike = ktTT;
      }
    }
  }

  centerChange(event: any) {
  }

  zoomChange(event: any) {
    this.location.zoom = event;
  }

  boundsChange(event: any) {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.fixHeightTable()
    }, 1);
  }

  fixHeightTable() {
    if (this.fix_scroll_table_rec.nativeElement != null) {
      this.heightTable = window.innerHeight - this.fix_scroll_table_rec.nativeElement.getBoundingClientRect().top;
      this.heightListBikeRight = this.heightTable - 74;
    }
  }

  matTabGroupChange() {

  }

  viewChange($event: boolean, type: number) {
    if (type == 0) {
      this.location.stationMarkers.forEach(element => {
        element.isShow = $event;
      });
    }
  }
  
  @ViewChild('fix_scroll_table_rec', { static: false }) fix_scroll_table_rec: ElementRef;

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.fixHeightTable()
  }
}

interface BikeMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: string;
  isOpen: boolean;
  isSelect: boolean;
  bike: Bike;
  isEnable: boolean;
  isShow: boolean;
}

interface StationMarker {
  lat: number;
  lng: number;
  label: string;
  iconUrl: string;
  isOpen: boolean;
  isSelect: boolean;
  station: Station;
  isEnable: boolean;
  isShow: boolean;
}

interface Location {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;
  stationMarkers: StationMarker[];
  bikeMarkers: BikeMarker[];
}
