import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { shareReplay, retry, catchError } from 'rxjs/operators';
import { ApiResponseData, ClientSettings } from 'src/app/app.settings.model';
import { AppClientSettings } from 'src/app/app.settings';
import { AppCommon } from 'src/app/app.common';
import { Station } from 'src/app/admin/infrastructure/stations/stations.model';
import { Bike } from 'src/app/admin/infrastructure/bikes/bikes.model';
@Injectable()
export class PublicService {

    public url = "/api/work/Public";
    _injector: Injector;
    public clientSettings: ClientSettings;
    constructor
        (
            public http: HttpClient,
            appClientSettings: AppClientSettings,
            public appCommon :AppCommon,
            injector: Injector
        ) {
        this.clientSettings = appClientSettings.settings
        this.url = this.clientSettings.serverAPI + this.url
        this._injector = injector;
    }

    stations(projectId: number) {
        return this.http.get<ApiResponseData<Station[]>>(this.url + "/Stations/"+projectId, this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }

    bikes() {
        return this.http.get<ApiResponseData<Bike[]>>(this.url + "/bikes", this.appCommon.getHeaders({})).pipe(
            shareReplay(1),
            retry(1),
            catchError((err: HttpErrorResponse) => this.handleError(err, this._injector))
        ).toPromise();
    }
   
    handleError(error: any, injector: Injector) {
        if (error.status === 401) {

        } else {

        }
        return Promise.reject(error);
    }
} 