import { Injectable } from '@angular/core';
import { Settings, ClientSettings } from './app.settings.model';
@Injectable()
export class AppSettings {
    public settings = new Settings(
        'Gradus',   //theme name
        true,       //loadingSpinner
        true,       //fixedHeader
        true,       //sidenavIsOpened
        true,       //sidenavIsPinned  
        true,       //sidenavUserBlock 
        'vertical', //horizontal , vertical
        'default',  //default, compact, mini
        'indigo-light',   //indigo-light, teal-light, red-light, blue-dark, green-dark, pink-dark
        false       // true = rtl, false = ltr
    )
}

@Injectable()
export class AppClientSettings {
  //public settings = new ClientSettings("http://localhost:6262", "http://localhost:8888")
  public settings = new ClientSettings( "https://api-backend.tngo.vn", "https://api-mobile.tngo.vn")
  //public settings = new ClientSettings( "http://api-backend-dev.tngo.vn:6063", "http://api-mobile.tngo.vn:6061")
}