import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import {TranslateModule, TranslateLoader, TranslateCompiler, TranslateParser, MissingTranslationHandler} from '@ngx-translate/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PowerPipe, UserFilter, Safe, DistrictFilter, ModelFilter, StationByProjectFilter, CustomerGroupsByProjectFilter, RoleControllersFilter } from '../app.common';
import { FileUploadComponent } from '../theme/components/file-upload/file-upload.component';
import { AgmCoreModule } from '@agm/core';
import { RedirectComponent } from '../pages/redirect/redirect.component';
import { FileUploadAttackedComponent } from '../theme/components/file-upload-attached/file-upload-attached.component';
import { FileUploadAttackedViewComponent } from '../theme/components/file-upload-attached/view/file-upload-attached-view.component';
import { CheckRolesDirective } from '../app.data';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    AgmCoreModule.forRoot({
      // apiKey: 'AIzaSyCmoeWpevIeP92HPk5r2EpuIhQVRw-gE70',
      apiKey: 'AIzaSyDwwqrcSxawUxiuymbokknvfjp9Q1ZnXWc',
      libraries: ["places", "geometry"],
      language: 'vi',
      region: 'VN'
    }),
  ],
  exports: [
    FlexLayoutModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    PowerPipe,
    UserFilter,
    FileUploadComponent,
    Safe,
    DistrictFilter,
    ModelFilter,
    StationByProjectFilter,
    CustomerGroupsByProjectFilter,
    FileUploadAttackedComponent,
    CheckRolesDirective,
    RoleControllersFilter
  ],
  declarations: [
    PowerPipe,
    UserFilter,
    FileUploadComponent,
    Safe,
    DistrictFilter,
    ModelFilter,
    StationByProjectFilter,
    CustomerGroupsByProjectFilter,
    RedirectComponent,
    FileUploadAttackedComponent,
    CheckRolesDirective,
    RoleControllersFilter
  ]
})
export class SharedModule { }
