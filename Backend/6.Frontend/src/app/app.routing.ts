import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { NotFoundComponent } from './pages/not-found/not-found.component';

import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth/auth.guard';
import { NotEnoughAccessComponent } from './pages/not-enough-access/not-enough-accessComponent';
import { RedirectComponent } from './pages/redirect/redirect.component';
import { PublicComponent } from './public/public.component';

export const routes: Routes = [
    {
        path: '',
        component: AdminComponent, canActivate: [AuthGuard], children: [
            { path: '', loadChildren: () => import('./admin/dashboard/dashboard.module').then(m => m.DashboardModule), data: { breadcrumb: 'Dashboard' } },
           
            { path: 'admin/dashboard/bike-not-in-booking', loadChildren: () => import('./admin/bike-not-in-station/bike-not-in-booking.module').then(m => m.BikeNotInBookingModule), data: { breadcrumb: 'Dashboard' } },
            
            { path: 'admin/dashboard/rfid', loadChildren: () => import('./admin/dashboard-rfid/dashboard-rfid.module').then(m => m.DashboardRFIDModule), data: { breadcrumb: 'Dashboard' } },
           
            { path: 'admin/users', loadChildren: () => import('./admin/user/users/users.module').then(m => m.UsersModule), data: { breadcrumb: 'Users' } },

            { path: 'admin/profiles', loadChildren: () => import('./admin/account/account.module').then(m => m.AccountModule) },

            { path: 'admin/log/mongo', loadChildren: () => import('./admin/user/logs/mongo/mongo-logs.module').then(m => m.MongoLogsModule), data: { breadcrumb: '' } },

            { path: 'admin/roles', loadChildren: () => import('./admin/user/roles/roles.module').then(m => m.RolesModule), data: { breadcrumb: 'Roles' } },
            { path: 'admin/role-groups', loadChildren: () => import('./admin/user/role-groups/role-groups.module').then(m => m.RoleGroupsModule), data: { breadcrumb: '' } },
            { path: 'admin/role-areas', loadChildren: () => import('./admin/user/role-areas/role-areas.module').then(m => m.RoleAreasModule), data: { breadcrumb: '' } },
            { path: 'admin/role-controllers', loadChildren: () => import('./admin/user/role-controllers/role-controllers.module').then(m => m.RoleControllersModule), data: { breadcrumb: '' } },
            { path: 'admin/superman', loadChildren: () => import('./admin/user/superman/superman.module').then(m => m.SupermanModule), data: { breadcrumb: '' } },

            { path: 'admin/customer/accounts', loadChildren: () => import('./admin/customer/accounts/accounts.module').then(m => m.AccountsModule), data: { breadcrumb: '' } },
            { path: 'admin/customer/ticket-prepaids', loadChildren: () => import('./admin/customer/ticket-prepaids/ticket-prepaids.module').then(m => m.TicketPrepaidsModule), data: { breadcrumb: '' } },
            { path: 'admin/customer/complaint-management', loadChildren: () => import('./admin/customer/customer-complaints/customer-complaints.module').then(m => m.CustomerComplaintsModule), data: { breadcrumb: '' } },

            { path: 'admin/infrastructure/stations', loadChildren: () => import('./admin/infrastructure/stations/stations.module').then(m => m.StationsModule), data: { breadcrumb: '' } },
            { path: 'admin/infrastructure/bikes', loadChildren: () => import('./admin/infrastructure/bikes/bikes.module').then(m => m.BikesModule), data: { breadcrumb: '' } },
            { path: 'admin/infrastructure/docs', loadChildren: () => import('./admin/infrastructure/docks/docks.module').then(m => m.DocksModule), data: { breadcrumb: '' } },
            { path: 'admin/infrastructure/suppliess', loadChildren: () => import('./admin/infrastructure/suppliess/suppliess.module').then(m => m.SuppliessModule), data: { breadcrumb: '' } },
            { path: 'admin/infrastructure/import-bills', loadChildren: () => import('./admin/infrastructure/import-bills/import-bills.module').then(m => m.ImportBillsModule), data: { breadcrumb: '' } },
            { path: 'admin/infrastructure/export-bills', loadChildren: () => import('./admin/infrastructure/export-bills/export-bills.module').then(m => m.ExportBillsModule), data: { breadcrumb: '' } },
            { path: 'admin/infrastructure/maintenance-repair', loadChildren: () => import('./admin/infrastructure/maintenance-repairs/maintenance-repairs.module').then(m => m.MaintenanceRepairsModule), data: { breadcrumb: '' } },
            { path: 'admin/infrastructure/warehouses', loadChildren: () => import('./admin/infrastructure/warehouses/warehouses.module').then(m => m.WarehousesModule), data: { breadcrumb: '' } },

            { path: 'admin/transaction/bookings', loadChildren: () => import('./admin/transaction/bookings/bookings.module').then(m => m.BookingsModule), data: { breadcrumb: '' } },
            { path: 'admin/transaction/transactions', loadChildren: () => import('./admin/transaction/transactions/transactions.module').then(m => m.TransactionsModule), data: { breadcrumb: '' } },
            { path: 'admin/transaction/transactions-debt', loadChildren: () => import('./admin/transaction/transactions-debt/transactions.module').then(m => m.TransactionsDebtModule), data: { breadcrumb: '' } },

            { path: 'admin/category/projects', loadChildren: () => import('./admin/category/projects/projects.module').then(m => m.ProjectsModule), data: { breadcrumb: '' } },
            { path: 'admin/category/investors', loadChildren: () => import('./admin/category/investors/investors.module').then(m => m.InvestorsModule), data: { breadcrumb: '' } },
            { path: 'admin/category/cities', loadChildren: () => import('./admin/category/citys/citys.module').then(m => m.CitysModule), data: { breadcrumb: '' } },
            { path: 'admin/category/districts', loadChildren: () => import('./admin/category/districts/districts.module').then(m => m.DistrictsModule), data: { breadcrumb: '' } },
            { path: 'admin/category/colors', loadChildren: () => import('./admin/category/colors/colors.module').then(m => m.ColorsModule), data: { breadcrumb: '' } },
            { path: 'admin/category/languages', loadChildren: () => import('./admin/category/languages/languages.module').then(m => m.LanguagesModule), data: { breadcrumb: '' } },
            { path: 'admin/category/models', loadChildren: () => import('./admin/category/models/models.module').then(m => m.ModelsModule), data: { breadcrumb: '' } },
            { path: 'admin/category/producers', loadChildren: () => import('./admin/category/producers/producers.module').then(m => m.ProducersModule), data: { breadcrumb: '' } },
            { path: 'admin/category/redeem-points', loadChildren: () => import('./admin/category/redeem-points/redeem-points.module').then(m => m.RedeemPointsModule), data: { breadcrumb: '' } },

            { path: 'admin/project/campaigns', loadChildren: () => import('./admin/campaigns/campaigns.module').then(m => m.CampaignsModule), data: { breadcrumb: '' } },
            { path: 'admin/project/campaign-results', loadChildren: () => import('./admin/campaigns/campaign-results/campaign-results.module').then(m => m.CampaignResultsModule), data: { breadcrumb: '' } },
            { path: 'admin/account-rating', loadChildren: () => import('./admin/campaigns/account-ratings/account-ratings.module').then(m => m.AccountRatingsModule), data: { breadcrumb: '' } },

            { path: 'admin/wallet/wallet-transactions', loadChildren: () => import('./admin/wallet/wallet-transactions/wallet-transactions.module').then(m => m.WalletTransactionsModule), data: { breadcrumb: '' } },

            { path: 'admin/contents/content-pages', loadChildren: () => import('./admin/contents/content-pages/content-pages.module').then(m => m.ContentPagesModule), data: { breadcrumb: '' } },
            { path: 'admin/contents/news', loadChildren: () => import('./admin/contents/newss/newss.module').then(m => m.NewssModule), data: { breadcrumb: '' } },
            { path: 'admin/contents/banner-groups', loadChildren: () => import('./admin/contents/banner-groups/banner-groups.module').then(m => m.BannerGroupsModule), data: { breadcrumb: '' } },

            { path: 'admin/notification/notification-jobs', loadChildren: () => import('./admin/notification/notification-jobs/notification-jobs.module').then(m => m.NotificationJobsModule), data: { breadcrumb: '' } },

            { path: 'admin/bike-reports', loadChildren: () => import('./admin/bike-reports/bike-reports.module').then(m => m.BikeReportsModule), data: { breadcrumb: '' } },
            { path: 'admin/collaborators', loadChildren: () => import('./admin/collaborators/collaborators.module').then(m => m.CollaboratorsModule), data: { breadcrumb: '' } },
            { path: 'admin/report/campaign', loadChildren: () => import('./admin/report/campaign/report.campaign.module').then(m => m.ReportCampaignModule), data: { breadcrumb: '' } },
            { path: 'admin/report/category', loadChildren: () => import('./admin/report/category/report.category.module').then(m => m.ReportCategoryModule), data: { breadcrumb: '' } },
            { path: 'admin/report/customer', loadChildren: () => import('./admin/report/customer/report.customer.module').then(m => m.ReportCustomerModule), data: { breadcrumb: '' } },
            { path: 'admin/report/infrastructure', loadChildren: () => import('./admin/report/infrastructure/report.infrastructure.module').then(m => m.ReportInfrastructureModule), data: { breadcrumb: '' } },
            { path: 'admin/report/revenue', loadChildren: () => import('./admin/report/revenue/report.revenue.module').then(m => m.ReportRevenueModule), data: { breadcrumb: '' } },
            { path: 'admin/report/transaction', loadChildren: () => import('./admin/report/transaction/report.transaction.module').then(m => m.ReportTransactionModule), data: { breadcrumb: '' } },
            { path: 'admin/report/employee', loadChildren: () => import('./admin/report/employee/report.employee.module').then(m => m.ReportEmployeeModule), data: { breadcrumb: '' } },

            { path: 'admin/troubleshooting-center', loadChildren: () => import('./admin/troubleshooting-center/troubleshooting-center.module').then(m => m.TroubleshootingCenterModule), data: { breadcrumb: '' } },

            { path: 'admin/logwork', loadChildren: () => import('./admin/logwork/logwork.module').then(m => m.LogWorkModule), data: { breadcrumb: '' } },

            { path: 'admin/chart', loadChildren: () => import('./admin/chart/chart.module').then(m => m.ChartModule), data: { breadcrumb: '' } }
        ]
    },
    {
        path: '',
        component: PublicComponent, canActivate: [], children: [
            { path: 'admin/public/map', loadChildren: () => import('./public/map/map.module').then(m => m.MapModule) },
        ]
    },

    { path: 'login', loadChildren: () => import('./admin/account/login/login.module').then(m => m.LoginModule) },
    { path: 'not-enough-access', component: NotEnoughAccessComponent, data: { breadcrumb: 'Error' } },
    { path: 'redirect', component: RedirectComponent, data: { breadcrumb: 'Error' } },
    { path: '**', component: NotFoundComponent }
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
});