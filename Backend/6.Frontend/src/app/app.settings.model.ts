import { City } from './admin/category/citys/citys.model';
import { Color } from './admin/category/colors/colors.model';
import { District } from './admin/category/districts/districts.model';
import { Investor } from './admin/category/investors/investors.model';
import { Language } from './admin/category/languages/languages.model';
import { Model } from './admin/category/models/models.model';
import { Producer } from './admin/category/producers/producers.model';
import { CustomerGroup, Project } from './admin/category/projects/projects.model';
import { Warehouse } from './admin/infrastructure/warehouses/warehouses.model';
import { Station } from './admin/infrastructure/stations/stations.model';
import { RoleArea } from './admin/user/role-areas/role-areas.model';
import { RoleController } from './admin/user/role-controllers/role-controllers.model';
import { RoleGroup } from './admin/user/role-groups/role-groups.model';
import { Role,  RoleTypeData } from './admin/user/roles/roles.model'
import { User } from './admin/user/users/users.model';
import { Menu } from './theme/components/menu/menu.model';

export class Settings {
  constructor(public name: string,
    public loadingSpinner: boolean,
    public fixedHeader: boolean,
    public sidenavIsOpened: boolean,
    public sidenavIsPinned: boolean,
    public sidenavUserBlock: boolean,
    public menu: string,
    public menuType: string,
    public theme: string,
    public rtl: boolean) { }
}

export class ClientSettings {
  constructor(public serverAPI: string, public severMobile) { }
}
export class ApiResponseData<T> {
  pageIndex: number;
  pageSize: number;
  count: number;
  data: T;
  status: number;
  outData: any;
  message: string;
}

export class PullData {
  roles: Role[];
  roleTypes: RoleTypeData[];
  customerGroups: CustomerGroup[];
  employees: User[];
  categoryVersion: string;
  projects: Project[];
  stations: Station[];
  roleControllers: RoleController[];
  cities: City[];
  districts: District[];
  investors: Investor[];
  colors: Color[];
  models: Model[];
  warehouses: Warehouse[];
  producers: Producer[];
  languages: Language[];
  roleGroups: RoleGroup[];
  roleAres: RoleArea[];
  imagesMaxSize: string;
  imagesType: string;
  adminMenus: Menu[];
}

export class FileDataDTO {
 path: string;
 fileName: string;
 createdDate: Date;
 createdUser: Date;
 fileSize: number;
}

export class AppVersionDTO {
  id: number;
 name: string;
 note: string;
 }
