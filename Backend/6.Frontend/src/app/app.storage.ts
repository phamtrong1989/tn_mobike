import { Injectable, Inject} from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AccountData} from "./admin/account/account.model"
import { PullData } from './app.settings.model';
@Injectable()
export class AppStorage {
    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

    setAccountInfo(data:AccountData){
        this.storage.set("AccountInfo", data);
    }
    
    removeAccountInfo(){
        this.storage.remove("AccountInfo");
    }

    getAccountInfo(): AccountData
    {
       return this.storage.get("AccountInfo");
    }
    
    setCategoryData(data:PullData){
        this.storage.set("PullData", data);
    }

    removeCategoryData(){
        this.storage.remove("PullData");
    }

    getCategoryData(): PullData
    {
       return this.storage.get("PullData");
    }

    getProjectId(): number
    {
       var outPut = this.storage.get("projectId");
       if(outPut == null || outPut == "" || outPut == "0"|| outPut == undefined)
       {
         this.setProjectId(0);
       }
       return this.storage.get("projectId");
    }

    setProjectId(data: number){
        this.storage.set("projectId", data);
    }

    getAppVersion(): string
    {
       var outPut = this.storage.get("appVersion");
       if(outPut == null || outPut == "" || outPut == "0"|| outPut == undefined)
       {
         this.setProjectId(null);
       }
       return this.storage.get("appVersion");
    }

    setAppVersion(data: string){
        this.storage.set("appVersion", data);
    }
}