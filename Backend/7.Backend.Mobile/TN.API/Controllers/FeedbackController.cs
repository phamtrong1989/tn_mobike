﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class FeedbackController : Controller<Feedback, IFeedbackService>
    {
        public FeedbackController(IFeedbackService service)
           : base(service)
        {
        }

        [HttpPost("Contact")]
        [SetOnlineAccount]
        public async Task<object> Contact([FromBody] FeedbackContactCommand request) => await Service.Contact(AccountId ,request );
    }
}
