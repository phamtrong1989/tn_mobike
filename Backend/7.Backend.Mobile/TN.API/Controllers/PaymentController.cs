﻿using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TN.API.Model;
using TN.API.Services;
using TN.Backend.Model.Command;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PaymentController : Controller<Account, IPaymentService>
    {
        private readonly IBookingService _iBookingService;
        public PaymentController(IPaymentService service, IBookingService iBookingService)
          : base(service)
        {
            _iBookingService = iBookingService;
        }

        #region [Khi nạp tiền xong link sẽ redirect về đây để xác nhận đã nạp tiền]
        [HttpGet("deposit-done")]
        [HttpGet("/")]
        [AllowAnonymous]
        public async Task<IActionResult> DepositDone(
            int accountid,
            int amount,
            string message,
            string payment_type,
            string reference_number,
            int status,
            int trans_ref_no,
            int website_id,
            string signature
        )
        {
            if(accountid <= 0)
            {
                return NotFound();
            }  
            
            var data = await Service.DepositDone(accountid, amount, message, payment_type, reference_number, status, trans_ref_no, website_id, signature, 1, status > 0);
            
            if (data.Status)
            {
                return Content("<html  lang='vi'><head><title></title><meta charshet='utf-8' /></head><body><div class='allPage' style='width: 100%; text-align: center; padding-top: 20px;'><h1>NẠP TIỀN THÀNH CÔNG</h1><div>Click quay lại để về màn hình chính</div></div></body></html>", "text/html", System.Text.Encoding.UTF8);
            } 
            else
            {
                return Content("<html lang='vi'><head><title></title><meta charshet='utf-8' /></head><body><div class='allPage' style='width: 100%; text-align: center; padding-top: 20px;'><h1>NẠP TIỀN THẤT BẠI</h1><div>Click quay lại và thử lại các bước</div></div></body></html>", "text/html", System.Text.Encoding.UTF8);
            }
        }
        #endregion

        #region [Lấy link nạp tiền]
        [Authorize]
        [HttpPost("deposit")]
        public async Task<object> Deposit([FromBody] PaymentDepositCommand model) => await Service.Deposit(AccountId, Phone, model, Request);
        #endregion

        #region [App TNGO gửi lên khi kh xác nhận nạp bằng momo]
        // Xác nhận treo tiền kh, tiếp theo sẽ đợi server momo call notifi xác định chuyern tiền cho kh https://business.momo.vn/merchant/integrate/generalInfo
        [Authorize]
        [HttpPost("UserMomoConfirm")]
        public async Task<object> UserMomoConfirm([FromBody] MomoConfirmCommand model) => await Service.UserMomoConfirm(AccountId, model);
        #endregion

        #region [Momo callback cho api để xác nhận chuyển tiền vào tk]
        // Sau khi treo tiền thành công, server MoMo sẽ tạo dữ liệu và gọi URL do đối tác cung cấp(notifyUrl) để thông báo kết quả giao dịch với nội dung mô tả bên dưới.
        //Cấu hình notifyUrl tại đây: https://business.momo.vn/merchant/integrate/generalInfo
        [AllowAnonymous]
        [HttpPost("MomoNotify")]
        [Produces("application/json")]
        public async Task<object> MomoCallbackNotify([FromBody]MomoNotifyDataDTO form) => await Service.MomoCallbackNotify(Request, form);
        #endregion

        #region Momo checkStatus
        [AllowAnonymous]
        [AllowOnyLocal]
        [AppVerificationControlDevice]
        [HttpPost("MomoCheckStatus/{id}")]
        public async Task<object> MomoCheckStatus(int id) => await Service.MomoCheckStatus(id);

        //[AllowAnonymous]
        //[HttpPost("MomoRefund/{id}")]
        //public async Task<object> MomoRefund(int id) => await Service.MomoRefund(id);
        #endregion

        #region [Zalo App TNGO gửi lên khi kh xác nhận nạp bằng Zalo]
        // Xác nhận treo tiền kh, tiếp theo sẽ đợi server momo call notifi xác định chuyern tiền cho kh https://business.momo.vn/merchant/integrate/generalInfo
        [Authorize]
        [HttpPost("UserZaloConfirm")]
        public async Task<object> UserZaloConfirm([FromBody] UserZaloConfirmCommand model) => await Service.UserZaloConfirm(AccountId, model);
        #endregion

        #region [Payoo]
        [Authorize]
        [HttpPost("UserPayooConfirm")]
        public async Task<object> UserPayooConfirm([FromBody] UserPayooConfirmCommand model) => await Service.UserPayooConfirm(AccountId, model);

        [AllowAnonymous]
        [HttpPost("PayooCallbackNotify")]
        public async Task<object> PayooCallbackNotify([FromBody] PayooNotifyCommand notifyData) => await Service.PayooCallbackNotify(notifyData);
        #endregion
        
        #region [Zalo callback cho api để xác nhận giao dịch thành công]
        // Sau khi treo tiền thành công, server MoMo sẽ tạo dữ liệu và gọi URL do đối tác cung cấp(notifyUrl) để thông báo kết quả giao dịch với nội dung mô tả bên dưới.
        //Cấu hình trong Zalo setting
        [AllowAnonymous]
        [HttpPost("ZaloCallbackNotify")]
        public async Task<object> ZaloCallbackNotify([FromBody] dynamic cbdata) => await Service.ZaloCallbackNotify(cbdata);

        [AllowAnonymous]
        [AllowOnyLocal]
        [AppVerificationControlDevice]
        [HttpPost("RecheckCallBackZalo/{id}")]
        public async Task<object> RecheckCallBackZalo(int id) => await Service.RecheckCallBackZalo(id);

        [AllowAnonymous]
        [AllowOnyLocal]
        [AppVerificationControlDevice]
        [HttpPost("RecheckCallBackVTCPay")]
        public async Task<object> RecheckCallBackVTC() => await Service.RecheckCallBackVTCPay();

        [AllowAnonymous]
        [AllowOnyLocal]
        [AppVerificationControlDevice]
        [HttpPost("RecheckCallBackPayoo/{id}")]
        public async Task<object> RecheckCallBackPayoo(int id) => await Service.RecheckCallBackPayoo(id);

        [AllowAnonymous]
        [HttpGet("ZaloCallbackNotify")]
        public IActionResult ZaloCallbackNotify(int status) => RedirectToAction("ZaloStatus", new { status = status });

        [AllowAnonymous]
        [HttpGet("ZaloStatus")]
        public IActionResult ZaloStatus(int status)
        {
            if (status == 1)
            {
                return Content("<html  lang='vi'><head><title></title><meta charshet='utf-8' /></head><body><div class='allPage' style='width: 100%; text-align: center; padding-top: 20px;'><h1>NẠP TIỀN THÀNH CÔNG</h1><div>Click quay lại để về màn hình chính</div></div></body></html>", "text/html", System.Text.Encoding.UTF8);
            }
            else
            {
                return Content("<html lang='vi'><head><title></title><meta charshet='utf-8' /></head><body><div class='allPage' style='width: 100%; text-align: center; padding-top: 20px;'><h1>NẠP TIỀN THẤT BẠI</h1><div>Click quay lại và thử lại các bước</div></div></body></html>", "text/html", System.Text.Encoding.UTF8);
            }
        }

        #endregion

        [Authorize]
        [HttpGet("WalletTransactions/{page}/{limit}")]
        public async Task<object> Transactions(int page, int limit) => await Service.WalletTransactionGets(AccountId, page, limit);

        [Authorize]
        [HttpGet("WalletGets")]
        public async Task<object> WalletGets() => await Service.WalletGets(AccountId);

        [Authorize]
        [HttpPost("WalletImportCode/{code}")]
        public async Task<object> WalletImportCode(string code) => await Service.WalletImportCode(AccountId, code, "0", "0");

        [Authorize]
        [HttpPost("WalletImportCode/{code}/{lat}/{lng}")]
        public async Task<object> WalletImportCode(string code, string lat, string lng) => await Service.WalletImportCode(AccountId, code, lat, lng);

        [Authorize]
        [HttpGet("GiftCardGetData")]
        public async Task<object> GiftCardGetData(string lat, string lng) => await Service.GiftCardGetData(AccountId, lat, lng);

        [Authorize]
        [HttpPost("RedeemPointAdd/{id}")]
        public async Task<object> RedeemPointAdd(int id) => await Service.RedeemPointAdd(AccountId, id);

        #region [Gift]
        [Authorize]
        [HttpGet("WalletTransactionGiftSearch/{page}/{limit}")]
        public async Task<object> WalletTransactionGiftSearch(int page, int limit) => await Service.WalletTransactionGiftSearch(AccountId, page, limit);

        [Authorize]
        [HttpPost("WalletTransactionGift")]
        public async Task<object> WalletTransactionGift([FromBody] WalletTransactionGiftCommand model) => await Service.WalletTransactionGiftPost(AccountId, model);

        [Authorize]
        [HttpGet("WalletTransactionGiftCheckAccount/{phone}")]
        public async Task<object> WalletTransactionGiftCheckAccount(string phone) => await Service.WalletTransactionGiftCheckAccount(AccountId, phone);
        #endregion

        [AllowAnonymous]
        [AllowOnyLocal]
        [AppVerificationControlDevice]
        [HttpPost("ClearSubPoint")]
        public async Task<object> ClearSubPoint() => await Service.ClearSubPoint();

        [AllowAnonymous]
        [AllowOnyLocal]
        [AppVerificationControlDevice]
        [HttpPost("SubPointSettlement")]
        public async Task<object> SubPointSettlement() => await Service.SubPointSettlement();

        [AllowAnonymous]
        [AllowOnyLocal]
        [AppVerificationControlDevice]
        [HttpPost("RefundPoint/{accountId}/{time}")]
        public async Task<object> RefundPoint(int accountId, DateTime time) => await Service.RefundPoint(accountId, time);

        [AllowAnonymous]
        [AllowOnyLocal]
        [AppVerificationControlDevice]
        [HttpPost("RefundAddPoint/{time}")]
        public async Task<object> RefundAddPoint(DateTime time) => await Service.RefundAddPoint(time);

        [Authorize]
        [HttpGet("DiscountCodes/{lat}/{lng}")]
        public async Task<object> DiscountCodes(string lat, string lng) => await Service.DiscountCodes(AccountId, lat, lng);

        [Authorize]
        [HttpGet("DiscountCode/{code}/{lat}/{lng}")]
        public async Task<object> DiscountCode(string code, string lat, string lng)
        {
            var outData = await _iBookingService.VerifyDiscountCode("PaymentService.DiscountCode", $"{DateTime.Now:yyyyMMddHHMMssfff}", AccountId, code, lat, lng);
            if(outData.Data == null)
            {
                return outData;
            }    
            return ApiResponse.WithData(new DiscountCodeDTO(outData.Data));
        }
    }
} 