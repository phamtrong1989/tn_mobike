﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    public class StationController : Controller<Station, IStationService>
    {
        public StationController(IStationService service)
           : base(service)
        {
        }

        #region [Lấy danh sách trạm gần đây]
        [HttpGet("{lat_lng}")]
        [SetOnlineAccount]
        public async Task<object> Get(string lat_lng) => await Service.FindByLatLng(lat_lng );
        #endregion
    }
}
