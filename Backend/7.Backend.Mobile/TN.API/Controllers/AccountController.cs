﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]   
    [Route("api/[controller]")]
    public class AccountController : Controller<Account, IAccountService>
    {
        public AccountController(IAccountService service)
           : base(service)
        {
        }

        #region [recovery-password]
        [AllowAnonymous]
        [HttpPost("otp-register-recovery-password")]
        public async Task<object> OTPRegisterRecoveryPasswordAsync([FromBody] AccountOTPRegisterCommand request) => await Service.OTPRegister(AccountOTP.OTPType.RecoveryPassword, request);

        [AllowAnonymous]
        [HttpPost("confirm-otp-register-recovery-password")]
        public async Task<object> ConfirmOTPRegisterRecoveryPasswordAsync([FromBody] AccountConfirmOTPRegisterCommand request) => await Service.ConfirmOTPRegister(AccountOTP.OTPType.RecoveryPassword, request);

        [AllowAnonymous]
        [HttpPost("recovery-password")]
        public async Task<object> RecoveryPasswordAsync([FromBody] AccountRecoveryPasswordCommand request) => await Service.RecoveryPassword(request);
        #endregion

        #region [Register]
        [AllowAnonymous]
        [HttpPost("otp-register")]
        public async Task<object> OTPRegisterAsync([FromBody] AccountOTPRegisterCommand request) => await Service.OTPRegister(AccountOTP.OTPType.Register, request);

        [AllowAnonymous]
        [HttpPost("confirm-otp-register")]
        public async Task<object> ConfirmOTPRegisterAsync([FromBody]AccountConfirmOTPRegisterCommand request) => await Service.ConfirmOTPRegister( AccountOTP.OTPType.Register, request);

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<object> RegisterAsync([FromBody]AccountRegisterCommand data) => await Service.Register(data, Request);
        #endregion

        #region [UpdateProfile]
        [Authorize]
        [HttpPost("update-profile")]
        [SetOnlineAccount]
        public async Task<object> UpdateProfileAsync([FromBody] AccountUpdateProfileCommand cm) => await Service.UpdateProfile(cm, AccountId, Request);

        [Authorize]
        [HttpPost("profile-verify")]
        [SetOnlineAccount]
        public async Task<object> ProfileVerify([FromBody] AccountUpdatProfileVerifyCommand cm) => await Service.ProfileVerify(cm, AccountId, Request);
        #endregion

        [Authorize]
        [HttpPost("check-old-password")]
        [SetOnlineAccount]
        public async Task<object> ChangePassword(string oldPassword) => await Service.CheckOldPassword(Phone, oldPassword);

        [Authorize]
        [HttpPost("change-password")]
        [SetOnlineAccount]
        public async Task<object> ChangePassword([FromBody] AccountChangePasswordCommand cm) => await Service.ChangePassword(Phone,cm);

        [Authorize]
        [HttpPost("upload-avatar")]
        public async Task<object> UploadAvatarAsync([FromBody] AccountUploadAvatarCommand request) => await Service.UploadAvatar(request, AccountId, Request);

        [HttpPost("logout")]
        public async Task<object> Logout([FromBody] AccountLogoutCommand request) => await Service.Logout(request);

        [Authorize]
        [HttpGet("profile")]
        [SetOnlineAccount]
        public async Task<object> Profile() => await Service.Profile(AccountId, Request);

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<object> Login([FromBody] LoginCommand request) => await Service.Login(request.Username, request.Password, Request);

        [Authorize]
        [HttpPost("UpdateDeviceToken")]
        public async Task<object> UpdateDeviceToken([FromBody] UpdateDeviceCommand request)
        {
            return await Service.UpdateDeviceToken(AccountId, request.Token, Request);
        }

        [AllowAnonymous]
        [HttpGet("checkemail/{email}")]
        public async Task<object> CheckEmail(string email) => await Service.CheckEmail(email);

        [AllowAnonymous]
        [HttpPost("check-share-code")]
        public async Task<object> CheckShareCode(string shareCode) => await Service.CheckShareCode(shareCode);

        [Authorize]
        [HttpPost("ChangeLanguage/{language}")]
        public async Task<object> ChangeLanguage(string language) => await Service.ChangeLanguage(AccountId, language);
    }
}
