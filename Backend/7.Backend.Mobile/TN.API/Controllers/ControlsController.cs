﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PT.Domain.Model;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{

    [Route("api/[controller]")]
    public class ControlsController : Controller<Account, IControlsService>
    {
        public ControlsController(IControlsService service)
           : base(service)
        {
        }

        [AppVerificationControlDevice]
        [HttpPost("Dock/{imei}/{type}")]
        public async Task<object> ControlDock(string imei, DockEventType type, string rfid = null) => await Service.ControlDock(imei, type, rfid);

        [HttpPost("SendLocation/{lat_lng}")]
        [Authorize]
        [CheckTimeOutRequest]
        public object SendLocation(string lat_lng)
        {
            //var run = await Service.SendLocation(AccountId, lat_lng);
            //if (run.Status == false)
            //{
            //    return Unauthorized();
            //}
            //else
            //{
            //    return run;
            //}
            return ApiResponse.WithStatusOk();
        }

        [HttpGet("CurrentVersion")]
        public async Task<object> CurrentVersion() => await Service.CurrentVersion();

        [HttpGet("CheckService/{lat_lng}")]
        [Authorize]
        [CheckTimeOutRequest]
        public object CheckService(string lat_lng) => ApiResponse.WithStatus(true, null, ResponseCode.Ok);

        [HttpGet("ShareCodeInfo")]
        [Authorize]
        public object ShareCodeInfo() =>  Service.ShareCodeInfo();

        [HttpGet("Settings")]
        public object Settings() => Service.Settings();

        [HttpGet("DefaultData")]
        public object DefaultData() => ApiResponse.WithStatus(false,  null, ResponseCode.ThaoTacQuaNhanh);

        [HttpPost("TotalAccountOnline/{seconds}")]
        [AppVerificationControlDevice]
        public object TotalAccountOnline(int seconds = 60) => Service.TotalAccountOnline(seconds);

        [AppVerificationControlDevice]
        [HttpPost("BikeWarningSend")]
        public async Task<object> BikeWarningSend([FromBody] BikeWarningSendCommand cm ) => await Service.BikeWarningSend(cm);

        [HttpGet("Resources")]
        public async Task<object> Resources() => await Service.Resources();
    }
}
