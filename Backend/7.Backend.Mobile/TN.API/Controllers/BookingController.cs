﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Model;
using TN.API.Services;
using TN.Backend.Model.Command;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BookingController : Controller<Booking, IBookingService>
    {
        public BookingController(IBookingService service): base(service)
        {
        }

        [HttpGet("DashboardTransactions")]
        [SetOnlineAccount]
        public object DashboardTransactions() => Service.DashboardTransactions(AccountId);

        [HttpGet("Transactions/{page}/{limit}")]
        [SetOnlineAccount]
        public async Task<object> Transactions(int page, int limit) => await Service.Transactions(AccountId, page, limit);

        [HttpGet("GetTransaction/{id}")]
        [SetOnlineAccount]
        public async Task<object> GetTransaction(int id) => await Service.GetTransaction(AccountId, id);

        [HttpGet("SelectStation/{id}/{lat_lng}")]
        [SetOnlineAccount]
        public async Task<object> SelectStation(int id, string lat_lng) => await Service.SelectStation(id, lat_lng, AccountId);

        //[HttpPost("BookingCancel")]
        //[SetOnlineAccount]
        //public async Task<object> BookingCancel([FromBody] BookingCancelCommand cm) => await Service.BookingCancel(cm, AccountId);

        [HttpGet("BookingScanView/{serialNumber}/{lat_lng}")]
        [SetOnlineAccount]
        public async Task<object> BookingScanView(string serialNumber, string lat_lng) => await Service.BookingScanView(serialNumber, AccountId, lat_lng);

        [HttpPost("BookingScanSubmit/{serialNumber}/{ticketPriceId}/{ticketPrepaidId}/{dockId}/{lat_lng}")]
        [CheckTimeOutRequest]
        public async Task<object> BookingScanSubmit(string serialNumber, int ticketPriceId, int ticketPrepaidId, int dockId, string lat_lng) => await Service.BookingScanSubmit(AccountId, serialNumber, ticketPriceId, ticketPrepaidId, dockId, lat_lng, null, null, null);

        [HttpPost("BookingCheckIn/{serialNumber}/{ticketPriceId}/{ticketPrepaidId}/{dockId}/{lat_lng}")]
        [CheckTimeOutRequest]
        public async Task<object> BookingCheckIn(string serialNumber, int ticketPriceId, int ticketPrepaidId, int dockId, string lat_lng, string discountCode) => await Service.BookingCheckIn(AccountId, serialNumber, ticketPriceId, ticketPrepaidId, dockId, lat_lng, null, null, null, false, discountCode);

        [HttpPost("BookingReOpen/{serialNumber}/{bookingId}")]
        [SetOnlineAccount]
        [CheckTimeOutRequest]
        public async Task<object> BookingReOpen(string serialNumber, int bookingId) => await Service.BookingReOpen(serialNumber, AccountId, bookingId);

        [HttpPost("BookingReOpen")]
        [SetOnlineAccount]
        [CheckTimeOutRequest]
        public async Task<object> BookingReOpen() => await Service.BookingReOpenAll(AccountId);

        [HttpPost("BookingCheckOut/{bookingId}")]
        [SetOnlineAccount]
        [CheckTimeOutRequest]
        public async Task<object> BookingCheckOut(int bookingId) => await Service.BookingCheckOut(bookingId, AccountId);

        [HttpPost("BookingCheckOut")]
        [SetOnlineAccount]
        [CheckTimeOutRequest]
        public async Task<object> BookingCheckOut() => await Service.BookingCheckOutAll(AccountId);

        [HttpGet("CheckBikeInStation/{bookingId}")]
        [SetOnlineAccount]
        public async Task<object> CheckBikeInStation(int bookingId) => await Service.CheckBikeInStation(bookingId, AccountId);

        [HttpPost("BookingCurrent")]
        [SetOnlineAccount]
        public async Task<object> BookingCurrent() => await Service.BookingCurrent(AccountId);

        [HttpPost("BookingsCurrent")]
        [SetOnlineAccount]
        public async Task<object> BookingsCurrent() => await Service.BookingsCurrent(AccountId);

        [HttpPost("BookingFeedback")]
        [SetOnlineAccount]
        public async Task<object> BookingFeedback([FromBody] BookingFeedbackCommand cm) => await Service.BookingFeedback(AccountId, cm);

        [HttpPost("BookingsFeedback")]
        [SetOnlineAccount]
        public async Task<object> BookingsFeedback([FromBody] BookingsFeedbackCommand cm) => await Service.BookingsFeedback(AccountId, cm);

        [HttpPost("BikeReportAddError")]
        [SetOnlineAccount]
        public async Task<object> BikeReportAddError([FromBody] BikeReportCommand cm) => await Service.BikeReportAddError(AccountId, cm);

        //[HttpPost("TestDiscount")]
        //[AllowAnonymous]
        //[AllowOnyLocal]
        //[AppVerificationControlDevice]
        //public async Task<object> TestDiscount(int accountId, DateTime endTime) => await Service.TestDiscount(accountId, endTime);
    }
}