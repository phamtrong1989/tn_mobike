﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PT.Domain.Model;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{

    [Route("api/[controller]")]
    public class DashboardController : Controller<Transaction, IDashboardService>
    {
        public DashboardController(IDashboardService service)
           : base(service)
        {
        }

        [Authorize]
        [HttpGet("ChartsTop")]
        public async Task<object> ChartsTop(int? weekType) => await Service.ChartsTop(weekType, AccountId, FullName, Phone);

        [Authorize]
        [HttpGet("OldTop")]
        public async Task<object> OldTop() => await Service.OldTop(AccountId);
    }
}
