﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]   
    [Route("api/[controller]")]
    public class PostController : Controller<Post, IPostService>
    {
        public PostController(IPostService service)
           : base(service)
        {
        }

        [HttpGet("NewsFeed")]
        public async Task<object> NewsFeed(int page, int limit, int? accountRatingId, string key) => await Service.Gets(page, limit, accountRatingId, null, key, EPostStatus.Approved, AccountId);

        [HttpGet("MyPost")]
        public async Task<object> MyPost(int page, int limit, EPostStatus? status) => await Service.Gets(page, limit, null, AccountId, null, status, AccountId);

        [HttpGet("PostByCompetition")]
        public async Task<object> PostByCompetition(int page, int limit, int? accountRatingId, string key) => await Service.Gets(page, limit, (accountRatingId != null && accountRatingId > 0) ? accountRatingId : 99999999, 0, key, EPostStatus.Approved, AccountId);

        [HttpPost("UploadImage")]
        public object UploadImage([FromBody] AccountUploadCommand request) =>  Service.UploadImage(request, AccountId, Request);
        
        [HttpPost("Add")]
        [CheckTimeOutRequest]
        public async Task<object> Add([FromBody] PostCommand cm) => await Service.Add(cm, AccountId, Phone, FullName);

        [HttpPost("Like/{postId}/{isLike}")]
        [CheckTimeOutRequest]
        public async Task<object> Like(int postId, bool isLike) => await Service.Like(AccountId, postId, isLike);

        [HttpDelete("Remove/{id}")]
        [CheckTimeOutRequest]
        public async Task<object> Remove(int id) => await Service.Remove(AccountId, id);

        [HttpGet("CurrentCompetitions")]
        public async Task<object> CurrentCompetitions() => await Service.CurrentCompetitions();
    }
}