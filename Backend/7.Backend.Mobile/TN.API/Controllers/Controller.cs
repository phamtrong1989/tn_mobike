﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IdentityModel.Tokens.Jwt;
using TN.API.Services;

namespace TN.API.Controllers
{
    public abstract class Controller<TModel, TService> : Controller
            where TService : IService<TModel>
    {
        protected readonly TService Service;

        protected Controller(TService service)
        {
            Service = service;
        }

        public int AccountId
        {
            get
            {
                return Convert.ToInt32(User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value );
            }
        }

        public string Phone
        {
            get
            {
                try
                {
                    return User.FindFirst("Phone")?.Value;
                }
                catch
                {
                    return $"{Convert.ToInt32(User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value)}";
                }
            }
        }

        public string FullName
        {
            get
            {
                try
                {
                    return User.FindFirst("FullName")?.Value;
                }
                catch
                {
                    return $"{Convert.ToInt32(User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value)}";
                }
            }
        }

        public string DeviceId
        {
            get
            {
                return User.FindFirst("DeviceId")?.Value;
            }
        }
    }
}
