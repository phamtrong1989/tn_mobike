﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class NotificationsController : Controller<NotificationData, INotificationService>
    {
        public NotificationsController(INotificationService service)
           : base(service)
        {
        }

        [HttpGet("Search")]
        public async Task<object> SearchAsync(int page = 1, int limit = 10) => await Service.Search(AccountId, page, limit );
    }
}
