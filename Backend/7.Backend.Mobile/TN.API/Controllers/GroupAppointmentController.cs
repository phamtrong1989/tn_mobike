﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Model;
using TN.API.Services;
using TN.Backend.Model.Command;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class GroupAppointmentController : Controller<GroupAppointment, IGroupAppointmentService>
    {
        public GroupAppointmentController(IGroupAppointmentService service)
           : base(service)
        {
        }

        [HttpGet("Gets")]
        public async Task<object> Gets(string lat_lng) => await Service.Gets(AccountId, lat_lng);

        [HttpGet("RequestList")]
        public async Task<object> RequestList() => await Service.RequestList(AccountId);

        [HttpGet("Details/{groupAppointmentId}")]
        public async Task<object> Details(int groupAppointmentId) => await Service.Details(AccountId, groupAppointmentId);

        [HttpPost("LeaderCreate")]
        public async Task<object> LeaderCreate([FromBody] GroupAppointmentCommand cm) => await Service.LeaderCreate(AccountId, cm, Request);

        [HttpPost("LeaderApproval")]
        public async Task<object> LeaderApproval(int memberId, int groupAppointmentItemId) => await Service.LeaderApproval(AccountId, memberId, groupAppointmentItemId);

        [HttpPost("LeaderCancel")]
        public async Task<object> LeaderCancel(int groupAppointmentId) => await Service.LeaderCancel(AccountId, groupAppointmentId);

        [HttpGet("MyGroup")]
        public async Task<object> MyGroup() => await Service.MyGroup(AccountId);

        [HttpPost("MemberJoin")]
        public async Task<object> MemberJoin(int groupAppointmentId) => await Service.MemberJoin(AccountId, groupAppointmentId);

        [HttpPost("MemberUnJoin")]
        public async Task<object> MemberUnJoin(int groupAppointmentId) => await Service.MemberUnJoin(AccountId, groupAppointmentId);
    }
}
