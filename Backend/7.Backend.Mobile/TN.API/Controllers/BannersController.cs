﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BannersController : Controller<Banner, IBannersService>
    {
        public BannersController(IBannersService service)
           : base(service)
        {
        }

        [HttpGet("WalletBanner")]
        public async Task<object> WalletBanner() => await Service.WalletBanner();
    }
}
