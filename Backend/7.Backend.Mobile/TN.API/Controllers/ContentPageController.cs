﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Route("api/[controller]")]
    public class ContentPageController : Controller<Account, IContentPageService>
    {
        public ContentPageController(IContentPageService service)
           : base(service)
        {
        }
      
        [HttpGet("TermsOfUse") ]
        public async Task<object> TermsOfUse() => await Service.TermsOfUse();

        [HttpGet("TentalAgreement")]
        public async Task<object> TentalAgreement() => await Service.TentalAgreement();

        [HttpGet("Introduction")]
        public async Task<object> Introduction() => await Service.Introduction();
    }
}
