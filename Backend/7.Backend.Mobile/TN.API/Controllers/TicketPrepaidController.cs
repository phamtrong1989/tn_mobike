﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TicketPrepaidController : Controller<TicketPrepaid, ITicketPrepaidService>
    {
        public TicketPrepaidController(ITicketPrepaidService service)
           : base(service)
        {
        }

        [HttpGet("Search")]
        [SetOnlineAccount]
        public async Task<object> Search(int page = 1, int limit = 10) => await Service.Search(AccountId, page, limit, Request);

        [HttpGet("Buy")]
        [SetOnlineAccount]
        public async Task<object> Buy() => await Service.Buy(AccountId, Request);

        [HttpPost("Buy")]
        [SetOnlineAccount]
        [CheckTimeOutRequest]
        public async Task<object> Buy([FromBody] ProjectBuyCommand cm) => await Service.Buy(AccountId, cm, Request);
    }
}
