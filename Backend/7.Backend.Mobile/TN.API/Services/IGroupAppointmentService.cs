﻿using Microsoft.Extensions.Options;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.API.Infrastructure.Interfaces;
using TN.Domain.Seedwork;
using TN.API.Model;
using PT.Domain.Model;
using Microsoft.AspNetCore.Http;
using TN.Shared;
using TN.Backend.Model.Command;

namespace TN.API.Services
{
    public interface IGroupAppointmentService : IService<GroupAppointment>
    {
        Task<ApiResponse<object>> Gets(int accountId, string lat_lng);
        Task<ApiResponse<object>> LeaderCreate(int accountId, GroupAppointmentCommand cm, HttpRequest request);
        Task<ApiResponse<object>> LeaderCancel(int accountId, int id);
        Task<ApiResponse<object>> MyGroup(int accountId);
        Task<ApiResponse<object>> MemberJoin(int accountId, int id);
        Task<ApiResponse<object>> MemberUnJoin(int accountId, int id);
        Task<ApiResponse<object>> Details(int accountId, int groupAppointmentId);
        Task<ApiResponse<object>> RequestList(int accountId);
        Task<ApiResponse<object>> LeaderApproval(int leaderId, int memberId, int groupAppointmentItemId);
    }

    public class GroupAppointmentService : IGroupAppointmentService
    {

        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IMemoryCacheService _iMemoryCacheService;
        private readonly IGroupAppointmentRepository _iGroupAppointmentRepository;
        private readonly IGroupAppointmentItemRepository _iGroupAppointmentItemRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IGroupAppointmentAccountRepository _iGroupAppointmentAccountRepository;
        public GroupAppointmentService
        (
            IMongoDBRepository iMongoDBRepository,
            IOptions<LogSettings> logSettings,
            IMemoryCacheService iMemoryCacheService,
            IGroupAppointmentRepository iGroupAppointmentRepository,
            IGroupAppointmentItemRepository iGroupAppointmentItemRepository,
            IAccountRepository iAccountRepository,
            IGroupAppointmentAccountRepository iGroupAppointmentAccountRepository
        )
        {
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _iMemoryCacheService = iMemoryCacheService;
            _iGroupAppointmentRepository = iGroupAppointmentRepository;
            _iGroupAppointmentItemRepository = iGroupAppointmentItemRepository;
            _iAccountRepository = iAccountRepository;
            _iGroupAppointmentAccountRepository = iGroupAppointmentAccountRepository;
        }

        public async Task<ApiResponse<object>> Gets(int accountId, string lat_lng)
        {
            try
            {
                var latLng = StringToLatLng(lat_lng);
                if(latLng.Lat <= 0 || latLng.Lng <= 0)
                {
                    return ApiResponse.WithStatusOk();
                }
                var stations = await _iMemoryCacheService.StationSearchAsync();
                var newList = new List<GroupAppointmentDTO>();
                var findProject = await _iMemoryCacheService.LatLngToCustomerGroup(latLng.Lat.ToString(), latLng.Lng.ToString(), 0);

                var data = await _iGroupAppointmentRepository.SearchAsync(
                    x => x.ProjectId == findProject.ProjectId && x.Status == EGroupAppointment.Default,
                    x => x.OrderByDescending(m => (m.MaxAmount - m.CurrentAmount)));

                foreach(var item in data)
                {
                    var station = stations.FirstOrDefault(x => x.Id == item.StationId);
                    item.LeaderPhone = Function.PhoneReplate(item.LeaderPhone);
                    newList.Add(new GroupAppointmentDTO(item, station?.Name, station?.Address));
                }    
                return ApiResponse.WithData(newList);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "GroupAppointmentService.Gets", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> Details(int accountId, int groupAppointmentId)
        {
            try
            {
                var kt = await _iGroupAppointmentRepository.SearchOneAsync(false, x => x.Id == groupAppointmentId && x.Status == EGroupAppointment.Default);
                if (kt == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }
                var stations = await _iMemoryCacheService.StationSearchAsync();
                var station = stations.FirstOrDefault(x => x.Id == kt.StationId);
                var model = new GroupAppointmentDTO(kt, station?.Name, station?.Address);
                var items = await _iGroupAppointmentItemRepository.SearchAsync(x => x.GroupAppointmentId == groupAppointmentId);
                model.Items = items.Select(x => new GroupAppointmentItemDTO(x));
                foreach(var item in items)
                {
                    item.MemberPhone = Function.PhoneReplate(item.MemberPhone);
                }    
                return ApiResponse.WithData(model);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "GroupAppointmentService.Details", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> RequestList(int accountId)
        {
            try
            {
                var stations = await _iMemoryCacheService.StationSearchAsync();
                var newList = new List<GroupAppointmentDTO>();
                var data =  _iGroupAppointmentRepository.RequestList(accountId);
                foreach (var item in data)
                {
                    var station = stations.FirstOrDefault(x => x.Id == item.StationId);
                    item.LeaderPhone = Function.PhoneReplate(item.LeaderPhone);
                    newList.Add(new GroupAppointmentDTO(item, station?.Name, station?.Address));
                }
                return ApiResponse.WithData(newList);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "GroupAppointmentService.Gets", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }


        public async Task<ApiResponse<object>> LeaderCreate(int accountId, GroupAppointmentCommand cm, HttpRequest request)
        {
            try
            {
                var latLng = StringToLatLng(cm.Lat_Lng);
                if (latLng.Lat <= 0 || latLng.Lng <= 0)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.GPSKhongHopLe);
                }

                var findProject = await _iMemoryCacheService.LatLngToCustomerGroup(latLng.Lat.ToString(), latLng.Lng.ToString(), 0);
                if(findProject.LocationProjectOverwrite == true)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.GPSKhongHopLe);
                }

                // Kiểm tra tài khoản đã được xác thực chưa
                var account = await _iAccountRepository.SearchOneAsync(true,x => x.Id == accountId && x.Status == EAccountStatus.Active && x.VerifyStatus == EAccountVerifyStatus.Ok);
                if(account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanCanXacThucMoiTaoNhom);
                }

                //Mỗi tài khoản chỉ được tạo phòng 2 lần kể cả những phòng hủy
                var kt = await _iGroupAppointmentRepository.CountAsync(x => x.LeaderId == accountId && x.CreatedDate.Date == DateTime.Now.Date);
                if(kt >= 2)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeDuocTao2PhongTrongNgay);
                }

                var ktprofile= await _iGroupAppointmentAccountRepository.SearchOneAsync(true, x => x.AccountId == accountId);
                if(ktprofile == null)
                {
                    ktprofile = new GroupAppointmentAccount
                    {
                        AccountId = accountId,
                        Lock = false,
                        ReputationLevel = null
                    };
                    await _iGroupAppointmentAccountRepository.AddAsync(ktprofile);
                    await _iGroupAppointmentAccountRepository.CommitAsync();
                }   
                
                if(ktprofile.Lock)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeBiKhoaTaoPhong);
                }

                var project = await _iMemoryCacheService.ProjectGetById(findProject.ProjectId);
                var station = await _iMemoryCacheService.StationGetById(cm.StationId);
                if(station.ProjectId != findProject.ProjectId)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeTramKhongThuocPhamVi);
                }

                // Kiểm tra xem nếu khách hàng đã có phòng rồi thì không thể xóa
                var ktRoom = await _iGroupAppointmentRepository.AnyAsync(x => x.LeaderId == accountId && x.CreatedDate.Date == DateTime.Now.Date && x.Status == EGroupAppointment.Default);
                if(ktRoom)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDapXeDaTonTaiRoom);
                }

                var timeStart = Convert.ToDateTime($"{DateTime.Now:yyyy/MM/dd} {cm.Time}:00");
                if((timeStart - DateTime.Now).TotalMinutes < 30)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeThoiGianBatDauKhongHopLe);
                }
                var add = new GroupAppointment()
                {
                    Code = $"{project?.Code}",
                    MaxAmount = cm.MaxAmount,
                    LeaderFullName = account.FullName,
                    LeaderId = account.Id,
                    LeaderSex = account.Sex,
                    ReputationLevel = ktprofile.ReputationLevel,
                    CreatedDate = DateTime.Now,
                    CurrentAmount = 1,
                    ProjectId = findProject.ProjectId,
                    StartTime = timeStart,
                    Descriptions = cm.Descriptions,
                    StationId = cm.StationId,
                    StationLat = station?.Lat ?? 0,
                    StationLng = station?.Lng ?? 0,
                    LeaderPhone = account?.Phone,
                    Status = EGroupAppointment.Default,
                    TransactionCode = null,
                    LeaderAvartar = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{account.Avatar}"
                };
                await _iGroupAppointmentRepository.AddAsync(add);
                await _iGroupAppointmentRepository.CommitAsync();
                add.Code = $"{project?.Code}{add.Id:D4}";
                _iGroupAppointmentRepository.Update(add);
                await _iGroupAppointmentRepository.CommitAsync();

                return ApiResponse.WithStatusOk(new GroupAppointmentDTO(add, station?.Name, station?.Address));
            }
            catch (Exception ex)
            {
                AddLog(accountId, "GroupAppointmentService.LeaderCreate", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> LeaderCancel(int accountId, int id)
        {
            // Hủy chuyến trước thời gian bắt đầu 30p
            try
            {
                var kt = await _iGroupAppointmentRepository.SearchOneAsync(false, 
                    x => x.Id == id 
                    && x.LeaderId == accountId 
                    && x.Status == EGroupAppointment.Default
                );

                if (kt == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                if (!(kt.StartTime >= DateTime.Now.AddMinutes(-30)))
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeBanKhongDuocHuyNhom);
                }

                kt.Status = EGroupAppointment.Cancel;
                _iGroupAppointmentRepository.Update(kt);
                await _iGroupAppointmentRepository.CommitAsync();
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(accountId, "GroupAppointmentService.LeaderCancel", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> LeaderApproval(int leaderId, int memberId, int groupAppointmentItemId)
        {
            try
            {
                var ktItem = await _iGroupAppointmentItemRepository.SearchOneAsync(false, x => x.Id == groupAppointmentItemId && x.MemberId == memberId && x.IsConfirm == false);
                if(ktItem == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                var kt = await _iGroupAppointmentRepository.SearchOneAsync(true, x => x.Id == ktItem.GroupAppointmentId && x.CreatedDate.Date == DateTime.Now.Date && x.LeaderId == leaderId);
                if (kt == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                if (kt.Status != EGroupAppointment.Default)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeChiXinThamGiaNhomDangCho);
                }
                // Nếu là chủ nhóm của nhóm nào đó
                var ktlead = await _iGroupAppointmentRepository.SearchOneAsync(true, x => x.LeaderId == memberId && x.Status == EGroupAppointment.Default && x.CreatedDate.Date == DateTime.Now.Date);
                if (ktlead != null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeBanDangONhomKhac);
                }
                // Kiểm tra xem thời gian hiện tại đã ở trong nhóm nào chưa
                var checkTrongNHom = _iGroupAppointmentRepository.CheckInAnyRoom(memberId);
                if (checkTrongNHom)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeBanDangONhomKhac);
                }
                ktItem.IsConfirm = true;
                _iGroupAppointmentItemRepository.Update(ktItem);
                await _iGroupAppointmentItemRepository.CommitAsync();
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(leaderId, "GroupAppointmentService.LeaderApproval", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> MyGroup(int accountId)
        {
            try
            {
                var kt = await _iGroupAppointmentRepository.SearchOneAsync(true, x => x.LeaderId == accountId && x.Status == EGroupAppointment.Default && x.CreatedDate.Date == DateTime.Now.Date);
                if (kt == null)
                {
                    var memberMyGroup =  _iGroupAppointmentRepository.MyGroup(accountId);
                    if(memberMyGroup == null)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                    }
                    kt = memberMyGroup;
                }
                
                var stations = await _iMemoryCacheService.StationSearchAsync();
                var station = stations.FirstOrDefault(x => x.Id == kt.StationId);
                var model = new GroupAppointmentDTO(kt, station?.Name, station?.Address);
                var items = await _iGroupAppointmentItemRepository.SearchAsync(x => x.GroupAppointmentId == kt.Id && x.IsConfirm);
                model.Items = items.Select(x => new GroupAppointmentItemDTO(x));
                return ApiResponse.WithData(model);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "GroupAppointmentService.MyGroup", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> MemberJoin(int accountId, int id)
        {
            try
            {
                var kt = await _iGroupAppointmentRepository.SearchOneAsync(true, x=>x.Id == id && x.CreatedDate.Date == DateTime.Now.Date);
                if (kt == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                if(kt.Status!= EGroupAppointment.Default)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeChiXinThamGiaNhomDangCho);
                }

                // Nếu là chủ nhóm của nhóm nào đó
                var ktlead = await _iGroupAppointmentRepository.SearchOneAsync(true, x => x.LeaderId == accountId && x.Status == EGroupAppointment.Default && x.CreatedDate.Date == DateTime.Now.Date);
                if (ktlead != null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeBanDangONhomKhac);
                }
                // Kiểm tra xem thời gian hiện tại đã ở trong nhóm nào chưa
                var checkTrongNHom =  _iGroupAppointmentRepository.CheckInAnyRoom(accountId);
                if(checkTrongNHom)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeBanDangONhomKhac);
                }
                // Kiểm tra xem bạn đã gửi bao nhiều yêu cầu join rồi
                var soLoiYeuCau = _iGroupAppointmentRepository.SendJoinRoomCount(accountId);
                if (soLoiYeuCau >= 5)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.NhomDiXeKhongQua5XinGiaNhapNhom);
                }

                var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                // Tạo bản ghi xin duyệt vào nhóm
                await _iGroupAppointmentItemRepository.AddAsync(new GroupAppointmentItem
                {
                    IsConfirm = false,
                    GroupAppointmentId = kt.Id,
                    TransactionCode = null,
                    MemberId = account?.Id ?? 0,
                    MemberFullName = account?.FullName,
                    MemberSex = account?.Sex ?? ESex.Khac,
                    MemberPhone = account?.Phone
                });
                await _iGroupAppointmentItemRepository.CommitAsync();
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(accountId, "GroupAppointmentService.MemberJoin", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> MemberUnJoin(int accountId, int id)
        {
            try
            {
                var kt = await _iGroupAppointmentRepository.SearchOneAsync(false, x => x.Id == id && x.Status == EGroupAppointment.Default && x.CreatedDate.Date == DateTime.Now.Date);
                if (kt == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }
                _iGroupAppointmentItemRepository.DeleteWhere(x => x.IsConfirm == false && x.GroupAppointmentId == kt.Id && x.MemberId == accountId);
                await _iGroupAppointmentItemRepository.CommitAsync();
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(accountId, "GroupAppointmentService.MemberUnJoin", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private LatLngModel StringToLatLng(string lat_lng)
        {
            try
            {
                if (string.IsNullOrEmpty(lat_lng))
                {
                    return null;
                }
                string[] comps = lat_lng.Split(';');
                return new LatLngModel
                {
                    Lat = Convert.ToDouble(comps[0]),
                    Lng = Convert.ToDouble(comps[1])
                };
            }
            catch
            {
                return null;
            }
        }

        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
    }
}
