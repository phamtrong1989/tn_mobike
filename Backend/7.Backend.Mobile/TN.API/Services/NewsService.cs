﻿using Microsoft.Extensions.Options;
using PT.Domain.Model;
using System;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;

namespace TN.API.Services
{
    public interface INewsService : IService<News>
    {
        Task<ApiResponse<object>> Search(int accountId, int page = 1, int limit = 10);
    }

    public class NewsService : INewsService
    {
        private readonly INewsRepository _iNewsRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IMemoryCacheService _iMemoryCacheService;

        public NewsService(INewsRepository iNewsRepository, IMongoDBRepository iMongoDBRepository, IOptions<LogSettings> logSettings, IMemoryCacheService iMemoryCacheService)
        {
            _iNewsRepository = iNewsRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _iMemoryCacheService = iMemoryCacheService;
        }
        
        public async Task<ApiResponse<object>> Search(int accountId, int page = 1, int limit = 10)
        {
            try
            {
                var lanId = await _iMemoryCacheService.CurrentLanguageId();
                var data = await _iNewsRepository.SearchPagedList(page, limit, x => (x.AccountId == accountId || x.AccountId == 0) && x.LanguageId == lanId, x => x.OrderByDescending(m => m.PublicDate));
                foreach(var item in data.Data)
                {
                    item.Content = $"<div style='padding: 0 20px 0 20px;'>{item.Content}</div>";
                }
                return ApiResponse.WithData(data.Data, data.TotalRows, data.Limit);
            }
            catch (Exception ex)
            {
                AddLog(0, "NewsService.Search", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
        #endregion
    }
}
