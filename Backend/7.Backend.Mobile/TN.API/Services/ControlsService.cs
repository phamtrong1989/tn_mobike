﻿using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;
using FirebaseAdmin.Messaging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.API.Services
{
    public interface IControlsService : IService<Account>
    {
        Task<ApiResponse<object>> ControlDock(string serialNumber, DockEventType type, string rfid = null);
        Task<ApiResponse<object>> SendLocation(int accountId, string lat_lng);
        Task<ApiResponse<object>> CurrentVersion();
        Task<ApiResponse<object>> CheckService(int accountId, string lat_lng);
        ApiResponse<object> ShareCodeInfo();
        ApiResponse<object> Settings();
        Task<ApiResponse<object>> BikeWarningSend(BikeWarningSendCommand model);
        object TotalAccountOnline(int seconds = 60);
        Task<ApiResponse<object>> Resources();
    }

    public class ControlsService : IControlsService
    {
        private readonly Model.Setting.BaseSetting _baseSetting;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly LogSettings _logSettings;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly IEventSystemReportRepository _iEventSystemReportRepository;
        private readonly IHubService _iHubService;
        private readonly Model.Setting.MobileSettings _mobileSettings;
        private readonly IRFIDBookingRepository _iRFIDBookingRepository;
        private readonly IOpenLockRequestRepository _iOpenLockRequestRepository;
        private readonly IMemoryCacheService _memoryCache;
        private readonly IFireBaseService _iFireBaseService;
        private readonly IBookingService _iBookingService;
        private readonly ISMSRepository _iSMSRepository;
        private readonly ZaloSettings _zaloSettings;
        private readonly IBikePrivateRepository _bikePrivateRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;


        public ControlsService(
            IOptions<Model.Setting.BaseSetting> baseSetting,
            IAccountRepository iAccountRepository,
            IDockRepository iDockRepository,
            IBookingRepository iBookingRepository,
            IOptions<LogSettings> logSettings,
            IMongoDBRepository iMongoDBRepository,
            IStationRepository iStationRepository,
            IEventSystemReportRepository iEventSystemReportRepository,
            IHubService iHubService,
            IOptions<Model.Setting.MobileSettings> mobileSettings,
            IRFIDBookingRepository iRFIDBookingRepository,
            IOpenLockRequestRepository iOpenLockRequestRepository,
            IMemoryCacheService memoryCache,
            IFireBaseService iFireBaseService,
            IBookingService iBookingService,
            ISMSRepository iSMSRepository,
            IOptions<ZaloSettings> zaloSettings,
            IBikePrivateRepository bikePrivateRepository,
            IWebHostEnvironment iWebHostEnvironment
            )
        {
            _baseSetting = baseSetting.Value;
            _iAccountRepository = iAccountRepository;
            _iDockRepository = iDockRepository;
            _iBookingRepository = iBookingRepository;
            _logSettings = logSettings.Value;
            _iMongoDBRepository = iMongoDBRepository;
            _iStationRepository = iStationRepository;
            _iEventSystemReportRepository = iEventSystemReportRepository;
            _iHubService = iHubService;
            _mobileSettings = mobileSettings.Value;
            _iRFIDBookingRepository = iRFIDBookingRepository;
            _iOpenLockRequestRepository =  iOpenLockRequestRepository;
            _memoryCache = memoryCache;
            _iFireBaseService = iFireBaseService;
            _iBookingService = iBookingService;
            _iSMSRepository = iSMSRepository;
            _zaloSettings =  zaloSettings.Value;
            _bikePrivateRepository = bikePrivateRepository;
            _iWebHostEnvironment =  iWebHostEnvironment;
        }

        public async Task<ApiResponse<object>> ControlDock(string serialNumber, DockEventType type, string rfid = null)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "FireBaseService.ControlDock";
            try
            {
                // check trường hợp event đóng khóa, khi mà khóa này đang được thuê
                if (type == DockEventType.DockClose)
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Nhận lệnh {type.GetDisplayName()} khóa ({type},{serialNumber})");

                    var dock = await _iDockRepository.SearchOneAsync(true, x => (x.SerialNumber == serialNumber || x.IMEI == serialNumber));
                    if (dock == null)
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} không tồn tại, END");
                        return ApiResponse.WithStatus(false, null, ResponseCode.DockNotExist);
                    }
                    // kiểm tra xem với khóa này có tồn tại chuyến đi ko, nếu tồn tại thì mới gửi lệnh cho client
                    var ktBooking = await _iBookingRepository.SearchOneAsync(true, x => x.DockId == dock.Id && x.Status == EBookingStatus.Start);
                    if (ktBooking == null)
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Không tồn tại giao dịch đang đi, END");
                        return ApiResponse.WithStatus(false, null, ResponseCode.BookingNotExist);
                    }
                    else
                    {
                        AddLog(ktBooking.AccountId, functionName, requestId, EnumMongoLogType.Center, $"AccountID ({ktBooking.AccountId}) tạo các bản điều khiển client ");
                        await _iFireBaseService.SendNotificationByAccount(functionName, requestId, ktBooking.AccountId, ENotificationTempType.SuKienDongKhoa, true, type, null);
                        // Đính cờ thời gian đóng khóa gần nhất
                        await _iEventSystemReportRepository.FlagLastCloseBikeAdd(ktBooking.TransactionCode, ktBooking.DockId, ktBooking.AccountId);
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                }
                else if (type == DockEventType.ScanRFID)
                {

                    if (string.IsNullOrEmpty(rfid))
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                    }

                    AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Nhận lệnh {type.GetDisplayName()} khóa ({type},{serialNumber},{rfid})", false, rfid);

                    var dock = await _iDockRepository.SearchOneAsync(true, x => (x.SerialNumber == serialNumber || x.IMEI == serialNumber));
                    if (dock == null)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.DockNotExist);
                    }

                    var bike = await _iDockRepository.BikeByDock(dock.Id);
                    if (bike == null)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.BikeNotExist);
                    }

                    // Kiểm tra xe có phải thuộc dang private ko
                    var ktBikePrivate = await _bikePrivateRepository.SearchOneAsync(false, x => x.BikeId == bike.Id);
                    if (ktBikePrivate != null)
                    {
                        if(ktBikePrivate.RFID != rfid)
                        {
                            AddLog(0, functionName, requestId, EnumMongoLogType.End, $"RFID này không gắn với xe {bike.Id}", false, rfid);
                            return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                        }
                        else
                        {
                            await _iOpenLockRequestRepository.AddAsync(new OpenLockRequest
                            {
                                CreatedDate = DateTime.Now,
                                IMEI = dock.IMEI,
                                OpenTime = null,
                                Retry = 0,
                                StationId = dock.StationId,
                                Status = LockRequestStatus.Waiting,
                                DockId = dock.Id,
                                AccountId = 0,
                                TransactionCode = $"RFID-Private-{rfid}",
                                SerialNumber = dock.SerialNumber,
                                InvestorId = dock.InvestorId,
                                ProjectId = dock.ProjectId,
                                CommandType = ECommandType.L0,
                                RFID = rfid,
                                BikeId = bike.Id
                            });
                            await _iOpenLockRequestRepository.CommitAsync();

                            ktBikePrivate.ScanDate = DateTime.Now;
                             _bikePrivateRepository.Update(ktBikePrivate);
                            await _bikePrivateRepository.CommitAsync();

                            AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Mở khóa cho xe private thành công", false, rfid);
                            return ApiResponse.WithStatusOk();
                        }    
                    }
                    else
                    {
                        // Kiểm tra xem xe này có đang thuê ko, nếu đang thuê và ta
                        var ktBooking = await _iBookingRepository.SearchOneAsync(true, x => x.BikeId == bike.Id && x.Status == EBookingStatus.Start);
                        if (ktBooking != null)
                        {
                            // Logic mở khóa lại hoặc kết thúc chuyến
                            if (ktBooking.RFID == rfid)
                            {
                                // Nếu trong trạm thì kết thúc chuyến đi
                                var checkInStation = await _iBookingService.InStationVerified($"{dock.Lat};{dock.Long}", 0, ktBooking.AccountId, functionName, requestId, dock.Id);
                                if (!checkInStation.Item1.Status)
                                {
                                    // Mở khóa xe
                                    var outA = await _iBookingService.RFIDBookingReOpen(rfid, serialNumber, ktBooking.AccountId, ktBooking.Id, requestId, functionName);
                                    AddLog(ktBooking.AccountId, functionName, requestId, EnumMongoLogType.End, $"REOPEN => {Newtonsoft.Json.JsonConvert.SerializeObject(outA)}", false, rfid);
                                    return outA;
                                }
                                else
                                {
                                    // Kết thúc chuyến đi
                                    var outA = await _iBookingService.BookingCheckOut(ktBooking.Id, ktBooking.AccountId, requestId, functionName, true);
                                    AddLog(ktBooking.AccountId, functionName, requestId, EnumMongoLogType.End, $"Kết thúc chuyến đi => {Newtonsoft.Json.JsonConvert.SerializeObject(outA)}", false, rfid);
                                    return ApiResponseT<object>.WithStatus(outA.Status, outA.Data, outA.ErrorCode);
                                }
                            }
                            else
                            {
                                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Xe này đã trong chuyến đi không thể điều phối hoặc thuê", false, rfid);
                                return ApiResponse.WithStatusNotOk();
                            }
                        }
                        else
                        {
                            // Xác định RFID của ai
                            var userRFID = await _iAccountRepository.UserByRFID(rfid);
                            if (userRFID != null)
                            {
                                // Kiểm tra xe này đã điều phối hoặc thuê du lịch chưa
                                var checkRFIDBooking = await _iRFIDBookingRepository.SearchOneAsync(false, x => x.BikeId == bike.Id && x.Status == ERFIDCoordinatorStatus.Start);
                                if (checkRFIDBooking == null)
                                {
                                    // Add Chuyến RFID
                                    await _iRFIDBookingRepository.AddAsync(new RFIDBooking
                                    {
                                        Status = ERFIDCoordinatorStatus.Start,
                                        BikeId = bike?.Id ?? 0,
                                        StartLat = dock.Lat ?? 0,
                                        StartLng = dock.Long ?? 0,
                                        InvestorId = dock.InvestorId,
                                        ProjectId = dock.ProjectId,
                                        StartTime = DateTime.Now,
                                        UserId = userRFID.Id,
                                        RFID = rfid,
                                        DockId = dock.Id,
                                        StationIn = dock.StationId,
                                        Note = null,
                                        AccountId = 0,
                                        Type = ERFIDBookingType.Admin,
                                        TransactionCode = $"{dock.ProjectId:D4}{DateTime.Now:yyyyMMddHHmmssfff}"
                                    });
                                    await _iRFIDBookingRepository.CommitAsync();
                                    AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Tạo log lịch sử mở RFID", false, rfid);
                                }

                                // Check đủ đk mở khóa ko
                                if (checkRFIDBooking == null || checkRFIDBooking.RFID == rfid)
                                {
                                    await _iOpenLockRequestRepository.AddAsync(new OpenLockRequest
                                    {
                                        CreatedDate = DateTime.Now,
                                        IMEI = dock.IMEI,
                                        OpenTime = null,
                                        Retry = 0,
                                        StationId = dock.StationId,
                                        Status = LockRequestStatus.Waiting,
                                        DockId = dock.Id,
                                        AccountId = 0,
                                        TransactionCode = $"RFID-{rfid}",
                                        SerialNumber = dock.SerialNumber,
                                        InvestorId = dock.InvestorId,
                                        ProjectId = dock.ProjectId,
                                        CommandType = ECommandType.L0,
                                        RFID = rfid,
                                        BikeId = bike.Id
                                    });
                                    await _iOpenLockRequestRepository.CommitAsync();
                                    AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Gửi lệnh mở khóa", false, rfid);
                                }
                                else
                                {
                                    AddLog(0, functionName, requestId, EnumMongoLogType.End, $"RFID mở khóa ko đúng với lúc tạo log", false, rfid);
                                }

                                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Tìm thấy User {userRFID.PhoneNumber} {userRFID.FullName}", false, rfid);
                            }
                            else
                            {
                                // Xử lý tạo chuyến đi nếu rfid  của 1 tài khoản nào đó
                                var accountRFID = await _iAccountRepository.SearchOneAsync(true, x => x.RFID == rfid && x.Status == EAccountStatus.Active);
                                if (accountRFID != null)
                                {
                                    AddLog(accountRFID.Id, functionName, requestId, EnumMongoLogType.Center, $"Tìm thấy Account {accountRFID.Phone} {accountRFID.FullName}");
                                    var outA = await _iBookingService.RFIDBookingScanSubmit(rfid, accountRFID.Id, dock, bike);
                                    AddLog(accountRFID.Id, functionName, requestId, EnumMongoLogType.End, $"Hệ thống => {Newtonsoft.Json.JsonConvert.SerializeObject(outA)}", false, rfid);
                                    return outA;
                                }
                                else
                                {
                                    AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy admin, member có rfid tương ứng", false, rfid);
                                    return ApiResponse.WithStatusNotOk();
                                }
                            }
                        }
                    }
                }
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public object TotalAccountOnline(int seconds = 60)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "FireBaseService.TotalAccountOnline";
            try
            {
                if (Startup.AccountLocations != null && Startup.AccountLocations.Any())
                {
                    var data = Startup.AccountLocations.Where(x => x.Value?.LastActive != null && x.Value?.LastActive >= DateTime.Now.AddSeconds(-seconds));
                    if(data != null)
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Số online {data.Count()}, tổng số session {Startup.AccountLocations.Count}", false);
                        return ApiResponse.WithData(data.Count());
                    }    
                    else
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Tổng số session {Startup.AccountLocations.Count}", false);
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }    
                }    
                else
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.End, $"NULL", false);
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }
            }
            catch (Exception ex)
            {
                Startup.AccountLocations = new Dictionary<int, AccountLocationModel>();
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{Newtonsoft.Json.JsonConvert.SerializeObject(Startup.AccountLocations)}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> SendLocation(int accountId, string lat_lng)
        {
            if (_baseSetting.WarningSystemStatus)
            {
                return ApiResponse.WithStatus(true, null, ResponseCode.ChucNangNayDangBiVoHieuHoa);
            }
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "FireBaseService.SendLocation";
            try
            {
                // Check tk có bị khóa không nếu khóa trả về lỗi 401
                var ktLock = await _iAccountRepository.AnyAsync(x => x.Id == accountId && x.Status == EAccountStatus.Lock);
                if (ktLock)
                {
                    return ApiResponse.WithStatusNotOk();
                }    
                var getToaDo = StringToLatLng(lat_lng);
                if (getToaDo == null)
                {
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }
                return await LocationProcess(accountId, getToaDo, functionName, requestId);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(true, null, ResponseCode.ServerError);
            }
        }

        private async Task<ApiResponse<object>> LocationProcess(int accountId, LatLngModel getToaDo, string functionName, string requestId)
        {
            try
            {
                // Set tọa độ cuối cùng theo khách hàng
                await _iAccountRepository.SetLocation(accountId, 0, 0, getToaDo.Lat, getToaDo.Lng);
                // Kiểm tra kh có tồn tại chuyến đi ko
                var booking = await _iBookingRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (booking != null)
                {
                    var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == booking.DockId);
                    if (dock != null && dock.Lat > 0 && dock.Long > 0)
                    {
                        var stations = await _memoryCache.StationSearchAsync();
                        // Xử lý logic trong chuyến đi xem khác hàng đi quá xa (không gần trạm nào 5km) => notify cho khách hàng 30p 1 lần
                        var anyInDeta =  _iStationRepository.FindByLatLngAnyByList(stations, dock.Lat.Value - _baseSetting.WarningOutRangeDeta, dock.Lat.Value + _baseSetting.WarningOutRangeDeta, dock.Long.Value - _baseSetting.WarningOutRangeDeta, dock.Long.Value + _baseSetting.WarningOutRangeDeta);
                        if (!anyInDeta)
                        {
                            string keyFlag = $"SendLocation_{EEventSystemType.M_3010_KhachHangDiQuaXaPhamVi}_{booking.TransactionCode}";
                            if (!_memoryCache.GetValue(keyFlag, out bool isKT))
                            {
                                _memoryCache.SetValue(keyFlag, true, 1000 * 60 * 30);

                                await _iHubService.EventSend(
                                    EEventSystemType.M_3010_KhachHangDiQuaXaPhamVi,
                                    EEventSystemWarningType.Warning,
                                   accountId,
                                   booking.TransactionCode,
                                   booking.ProjectId,
                                   booking.StationIn,
                                   booking.BikeId,
                                   booking.DockId,
                                   $"Khách hàng đi quá xa phạm vi '{Function.FormatMoney(_baseSetting.WarningOutRangeDeta * 100000)}m', mã chuyến đi '{booking.TransactionCode}' thời gian bắt đầu chuyến đi '{booking.EndTime:HH:mm:ss dd/MM/yyyy}'"
                               );
                                 await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.BanDiXeQuaXa, true, DockEventType.Not, null, $"{Function.FormatMoney(_baseSetting.WarningOutRangeDeta * 100000)}m");
                            }
                        }
                    }
                    
                    // Đang trong chuyến đi, khóa đang mở, gps của người và xe xa nhau quá 500m => Cảnh báo "bạn đang đi xa khỏi xe mà quên khoá xe phải ko, hãy kiểm tra lại nhé"
                    if (dock != null && !dock.LockStatus)
                    {
                        var kc = Function.DistanceInMeter(getToaDo.Lat, getToaDo.Lng, dock.Lat ?? 0, dock.Long ?? 0);
                        if (kc >= _baseSetting.MaxDistanceDockOpenInBooking)
                        {
                            string keyFlag = $"SendLocation_{ENotificationTempType.KHDiXaKhoiXeMaKhongKhoaXe}_{booking.TransactionCode}";
                            if (!_memoryCache.GetValue(keyFlag, out bool isKT))
                            {
                                _memoryCache.SetValue(keyFlag, true, 1000 * 60 * 30);
                                await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.KHDiXaKhoiXeMaKhongKhoaXe, true, DockEventType.Not, null);
                                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Khoảng cách người với khóa {kc}m, ({getToaDo.Lat},{ getToaDo.Lng}), ({dock.Lat},{ dock.Long}) => Gửi notify {ENotificationTempType.KHDiXaKhoiXeMaKhongKhoaXe}", false);
                            }
                        }
                    }
                }
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(true, null, ResponseCode.ServerError);
            }
        } 

        private static LatLngModel StringToLatLng(string lat_lng)
        {
            try
            {
                if (string.IsNullOrEmpty(lat_lng))
                {
                    return null;
                }
                string[] comps = lat_lng.Split(';');
                return new LatLngModel
                {
                    Lat = Convert.ToDouble(comps[0]),
                    Lng = Convert.ToDouble(comps[1])
                };
            }
            catch
            {
                return null;
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false, string deviceKey = null)
        {
            Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, deviceKey, isException)).ConfigureAwait(false);
        }
        #endregion

        public async Task<ApiResponse<object>> CurrentVersion()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "FireBaseService.CurrentVersion";
            try
            {
               return ApiResponse.WithStatus(true, await _iStationRepository.CurrentVersion(), ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> CheckService(int accountId, string lat_lng)
        {
            if (_baseSetting.WarningSystemStatus)
            {
                return ApiResponse.WithStatus(false, null, ResponseCode.ChucNangNayDangBiVoHieuHoa);
            }
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "FireBaseService.CheckService";
            try
            {
                var getToaDo = StringToLatLng(lat_lng);
                if (getToaDo == null)
                {
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }
                return await CheckService(getToaDo);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private async Task<ApiResponse<object>> CheckService(LatLngModel getToaDo)
        {
            double delta = Math.Round(Convert.ToDouble(_baseSetting.NonServiceDistance) / Convert.ToDouble(100000), 3);
            // Giới hạn 30KM xen có những trạm nào
            var stations = await _memoryCache.StationSearchAsync();
            var dtoStations = new List<StationDTO>();
            foreach (var item in stations)
            {
                if (item.Lat != 0 && item.Lng != 0)
                {
                    var distance = Function.DistanceInMeter(getToaDo.Lat, getToaDo.Lng, item.Lat, item.Lng);
                    var dtoStation = new StationDTO(item, Convert.ToInt32(distance));
                    dtoStations.Add(dtoStation);
                }
            }
            dtoStations = dtoStations.OrderBy(x => x.Distance).ToList();
            var station = dtoStations.FirstOrDefault();
            if (station == null || station.Distance > _baseSetting.NonServiceDistance)
            {
                return ApiResponse.WithStatus(false, new { Title = "TNGO chưa khả dụng tại đây", Note = "Hiện tại TNGO đang cung cấp dịch vụ tại Thành phố Hồ Chí Minh và sẽ sớm có mặt tại Hà Nội, Vũng Tàu" }, ResponseCode.Ok);
            }
            return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
        }

        public ApiResponse<object> ShareCodeInfo()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "FireBaseService.ShareCodeInfo";
            try
            {
                var str = string.Format(_baseSetting.ShareCodeInfo, Function.FormatMoney(_baseSetting.OwnerShareCodePrice));
                return ApiResponse.WithStatus(true, new { Text = str, Price = _baseSetting.OwnerShareCodePrice }, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public ApiResponse<object> Settings()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "FireBaseService.Settings";
            try
            {
                return ApiResponse.WithStatus(true, _mobileSettings, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> BikeWarningSend(BikeWarningSendCommand model)
        {
            if (_baseSetting.WarningSystemStatus)
            {
                return ApiResponse.WithStatus(false, null, ResponseCode.ChucNangNayDangBiVoHieuHoa);
            }

            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "FireBaseService.BikeWarningSend";
            try
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Nhận lệnh {model.IMEI} {model.Type.GetDisplayName()},{model.Time:dd/MM/yyyy HH:mm:ss})", false, model.IMEI);

                var dock = await _iDockRepository.SearchOneAsync(true, x => x.IMEI == model.IMEI);
                if (dock == null)
                {
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }
                var bike = await _iDockRepository.BikeByDock(dock.Id);
                if (bike == null)
                {
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }
                var eventSystemType = EEventSystemType.M_1007_DiChuyenBatHopPhap;

                if (model.Type == EBikeWarningType.DiChuyenBatHopPhap)
                {
                    eventSystemType = EEventSystemType.M_1007_DiChuyenBatHopPhap;
                    var booking = await _iBookingRepository.SearchOneAsync(true, x => x.Status == EBookingStatus.Start && x.DockId == dock.Id);
                    if (booking != null)
                    {
                        AddLog(booking.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Khách hàng nhận thông báo xe khóa đang di chuyên bất hợp pháp", true, model.IMEI);

                        // Đối với khách hàng 15 phút gửi 1 lần
                        string keyFlag = $"{ENotificationTempType.KHKhoaXeNhanCanhBaoBatHopPhap}_{booking.AccountId}_{booking.TransactionCode}";

                        if (!_memoryCache.GetValue(keyFlag, out bool isKT))
                        {
                            _memoryCache.SetValue(keyFlag, true, 1000 * 60 * 15);
                            await _iFireBaseService.SendNotificationByAccount(functionName, requestId, booking.AccountId, ENotificationTempType.KHKhoaXeNhanCanhBaoBatHopPhap, true, DockEventType.Not, null);

                        }
                        await _iHubService.EventSend(eventSystemType, EEventSystemWarningType.Danger, booking.AccountId, booking.TransactionCode, booking.ProjectId, booking.StationIn, booking.BikeId, booking.DockId,
                          $"[EVENT] Xe {bike.Plate} { eventSystemType.GetDisplayName() } trong chuyến đi {booking.TransactionCode}", $"[{Newtonsoft.Json.JsonConvert.SerializeObject(booking)}]");
                    }
                    else
                    {
                        await _iHubService.EventSend(eventSystemType, EEventSystemWarningType.Danger, 0, null, bike.ProjectId, bike.StationId, bike.Id, bike.DockId,
                            $"[EVENT] Xe {bike.Plate} { eventSystemType.GetDisplayName() }", $"[{Newtonsoft.Json.JsonConvert.SerializeObject(new { bikeId = bike.Id, plate = bike.Plate, dockId = bike.DockId, projectId = bike.ProjectId })}]");
                    }    
                }
                else if (model.Type == EBikeWarningType.DoXe)
                {
                    eventSystemType = EEventSystemType.M_1002_XeDo;
                    string keyFlag = $"{model.Type}_{model.IMEI}";
                    if (!_memoryCache.GetValue(keyFlag, out bool isKT))
                    {
                        _memoryCache.SetValue(keyFlag, true, 1000 * 60 * 15);
                        await _iHubService.EventSend(eventSystemType, EEventSystemWarningType.Warning, 0, null, bike.ProjectId, bike.StationId, bike.Id, bike.DockId, $"Xe {bike.Plate} { eventSystemType.GetDisplayName() }", $"[{Newtonsoft.Json.JsonConvert.SerializeObject(new { bikeId = bike.Id, plate = bike.Plate, dockId = bike.DockId, projectId = bike.ProjectId })}]");
                    }
                }
                else if (model.Type == EBikeWarningType.XeKhongTrongTram)
                {
                    eventSystemType = EEventSystemType.M_1003_XeKhongLamTrongTram;
                    string keyFlag = $"{model.Type}_{model.IMEI}";
                    if (!_memoryCache.GetValue(keyFlag, out bool isKT))
                    {
                        _memoryCache.SetValue(keyFlag, true, 1000 * 60 * 15);
                        await _iHubService.EventSend(eventSystemType, EEventSystemWarningType.Warning, 0, null, bike.ProjectId, bike.StationId, bike.Id, bike.DockId, $"Xe {bike.Plate} { eventSystemType.GetDisplayName() }", $"[{Newtonsoft.Json.JsonConvert.SerializeObject(new { bikeId = bike.Id, plate = bike.Plate, dockId = bike.DockId, projectId = bike.ProjectId })}]");
                    }
                }
                else if (model.Type == EBikeWarningType.XeLoiGPS)
                {
                    eventSystemType = EEventSystemType.M_1005_XeMatGPS;
                    string keyFlag = $"{model.Type}_{model.IMEI}";

                    if (!_memoryCache.GetValue(keyFlag, out bool isKT))
                    {
                        _memoryCache.SetValue(keyFlag, true, 1000 * 60 * 15);
                        await _iHubService.EventSend(eventSystemType, EEventSystemWarningType.Warning, 0, null, bike.ProjectId, bike.StationId, bike.Id, bike.DockId, $"Xe {bike.Plate} { eventSystemType.GetDisplayName() }", $"[{Newtonsoft.Json.JsonConvert.SerializeObject(new { bikeId = bike.Id, plate = bike.Plate, dockId = bike.DockId, projectId = bike.ProjectId })}]");
                    }
                }
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> Resources()
        {
            var listLang = await _memoryCache.LanguageAllAsync();

            var path = $"{_iWebHostEnvironment.ContentRootPath}/appsettings.Resources.json";


            var data = new ResourcesDTO { Languages = listLang, ResourcesForm = Newtonsoft.Json.JsonConvert.DeserializeObject(File.ReadAllText(path)) };
            return ApiResponse.WithStatus(true, data, ResponseCode.Ok);
        }
    }
}