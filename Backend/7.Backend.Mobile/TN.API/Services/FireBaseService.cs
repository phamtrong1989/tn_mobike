﻿using FirebaseAdmin.Messaging;
using Microsoft.Extensions.Options;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;

namespace TN.API.Services
{
    public interface IFireBaseService : IService<Account>
    {
        Task<ApiResponse<object>> SendNotificationByAccount(string functionName, string requestId, int accountId, ENotificationTempType type, bool isShow, DockEventType dataDockEventType, string dataContent, params string[] prs);
        Task NotifyRefeshStation(string functionName, string requestId, int stationId, string topic);
    }

    public class FireBaseService : IFireBaseService
    {
        private readonly Model.Setting.BaseSetting _baseSetting;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly INotificationRepository _iNotificationRepository;
        private readonly List<NotificationTemp> _notificationTempSettings;
        private readonly LogSettings _logSettings;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly IStationRepository _iStationRepository;
      

        public FireBaseService(
            IOptions<Model.Setting.BaseSetting> baseSetting,
            IAccountRepository iAccountRepository,
            IDockRepository iDockRepository,
            INotificationRepository iNotificationRepository,
            IOptions<List<NotificationTemp>> notificationTempSettings,
            IOptions<LogSettings> logSettings,
            IMongoDBRepository iMongoDBRepository,
            IStationRepository iStationRepository
            )
        {
            _baseSetting = baseSetting.Value;
            _iAccountRepository = iAccountRepository;
            _iDockRepository = iDockRepository;
            _iNotificationRepository = iNotificationRepository;
            _notificationTempSettings = notificationTempSettings.Value;
            _logSettings = logSettings.Value;
            _iMongoDBRepository = iMongoDBRepository;
            _iStationRepository = iStationRepository;
         
        }

        public async Task NotifyRefeshStation(string functionName, string requestId, int stationId, string topic)
        {
            try
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Nhận yêu cầu gửi thông tin trạm {stationId}");
                var station = await _iStationRepository.SearchOneAsync(true, x => x.Id == stationId);
                if (station == null)
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Không tìm thấy trạm {stationId}");
                    return;
                }
                var stations = await _iDockRepository.BindTotalDockFree(new List<Station>() { station }, 0, _baseSetting.MinBattery);
                var dtoStation = new StationDTO(stations.FirstOrDefault(x => x.Id == stationId), 0);
                var sendData = new Dictionary<string, string>
                {
                    { "AccoutId", "0" },
                    { "DeviceId", "0" },
                    { "Time", $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}" },
                    { "Type", $"{((int)ETNGOHubDTO.UpdateStation)}" },
                    { "Data", $"{ Newtonsoft.Json.JsonConvert.SerializeObject(dtoStation, Newtonsoft.Json.Formatting.Indented) }" }
                };
                await SendNotifyData(functionName, requestId, topic, sendData);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi {ex}", true);
            }
        }

        private async Task<ApiResponse> SendNotifyData(string functionName, string requestId, string topic, Dictionary<string, string> data)
        {
            try
            {
                var outData = await FirebaseMessaging.DefaultInstance.SendAsync(new Message()
                {
                    Topic = topic,
                    Data = data
                    ,
                    Apns = new ApnsConfig
                    {
                        Aps = new Aps { ContentAvailable = true },
                        Headers = new Dictionary<string, string>() {
                            { "apns-push-type","alert" },
                            { "apns-priority","5" },
                            { "apns-topic", topic },
                        }
                    }
                });
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Gửi notify data topic '{topic}' nội dung {Newtonsoft.Json.JsonConvert.SerializeObject(data)}, trả về là {outData}");
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(0, "ControlDock", requestId, EnumMongoLogType.Center, $"Gửi notify data topic '{topic}' nội dung {Newtonsoft.Json.JsonConvert.SerializeObject(data)} => lỗi {ex}", true);
                return ApiResponse.WithStatusNotOk();
            }
        }

        public async Task<ApiResponse<object>> SendNotificationByAccount(string functionName, string requestId, int accountId, ENotificationTempType type, bool isShow, DockEventType dataDockEventType, string dataContent, params string[] prs)
        {
            try
            {
                var lang = AppHttpContext.AppLanguage;
                if(dataDockEventType == DockEventType.DockClose)
                {
                    lang = (await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId))?.Language ?? "vi";
                }    

                var dataByLan = _notificationTempSettings.FirstOrDefault(x => x.Language == lang);

                var getNotifyByType = dataByLan?.Data?.FirstOrDefault(x => x.Type == type);
                if (getNotifyByType == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi notify => không tìm thấy teamplate {type.GetDisplayName()}");
                    getNotifyByType = new NotificationTempData()
                    {
                        Body = type.GetDisplayName(),
                        Title = "TNGO",
                        Type = type,
                        Name = type.GetDisplayName()
                    };
                }

                string newBody = getNotifyByType.Body;
                try
                {
                    if (prs != null && prs.Length > 0)
                    {
                        newBody = string.Format(newBody, prs);
                    }
                }
                catch { }

                string actionType = "";

                if (type == ENotificationTempType.NapTienThanhCong || type == ENotificationTempType.NhapMaGioiThieu || type == ENotificationTempType.MuaVeTraTruocThanhCong
                    || type == ENotificationTempType.TangDiemThanhCong || type == ENotificationTempType.NhanDiemThanhCong || type == ENotificationTempType.NapMaKhuyenMaiThanhCong
                    || type == ENotificationTempType.DoiDiemTichLuyThanhCong
                    || type == ENotificationTempType.DauThangTruTienTKKM
                    || type == ENotificationTempType.CoNguoiDungMaChiaSeCuaBan)
                {
                    actionType = ENotificationActionType.WalletTransaction.ToString();
                }
                else if (type == ENotificationTempType.KetThucChuyenDi || type == ENotificationTempType.HuyChuyenDi || type == ENotificationTempType.KetThucChuyenDiNoCuoc)
                {
                    actionType = ENotificationActionType.Transaction.ToString();
                }

                await _iNotificationRepository.AddAsync(new NotificationData
                {
                    AccountId = accountId,
                    CreatedDate = DateTime.Now,
                    DeviceId = accountId.ToString(),
                    Tile = getNotifyByType.Title,
                    Message = newBody,
                    TransactionCode = "Notify",
                    Type = type,
                    Icon = "",
                    ActionType = actionType
                });
                await _iNotificationRepository.CommitAsync();

                var getDevices = (await _iAccountRepository.FilterDevices(accountId)).Where(x => !string.IsNullOrEmpty(x.CloudMessagingToken)).ToList();

                foreach (var item in getDevices)
                {
                    if(string.IsNullOrEmpty(item.CloudMessagingToken))
                    {
                        continue;
                    }

                    await Task.Run(() => SendByTask(getNotifyByType.Title, newBody, item.CloudMessagingToken, isShow, dataDockEventType, dataContent, actionType, dataDockEventType != DockEventType.DockClose)).ConfigureAwait(false);
                }
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi notify => loại '{type.GetDisplayName()}' nội dung '{newBody}', số lượng device {getDevices.Count}");
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Gửi notify => lỗi {ex}", true);
                return ApiResponse.WithStatusNotOk();
            }
        }

        private async void SendByTask(string title, string newBody, string cloudMessagingToken, bool isShow, DockEventType dataDockEventType, string dataContent, string actionType, bool enableDelay)
        {
            try
            {
                if(enableDelay)
                {
                    await Task.Delay(3000);
                }    
                await FirebaseMessaging.DefaultInstance.SendAsync(new Message()
                {
                    Notification = new Notification
                    {
                        Title = title,
                        Body = newBody
                    },
                    Token = cloudMessagingToken,
                    Data = new Dictionary<string, string>() {
                            { "isShow", isShow.ToString() },
                            { "type", ((byte)dataDockEventType).ToString() },
                            { "time", DateTime.Now.Ticks.ToString() },
                            { "content", dataContent },
                            { "actionType", actionType }
                        },
                    Apns = new ApnsConfig
                    {
                        Aps = new Aps { ContentAvailable = true },
                        Headers = new Dictionary<string, string>() {
                            { "apns-push-type","alert" },
                            { "apns-priority","5" },
                            { "apns-topic", "" }
                        }
                    }
                });
            }
            catch
            {
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false, string deviceKey = null)
        {
            Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, deviceKey, isException)).ConfigureAwait(false);
        }
        #endregion
    }
}