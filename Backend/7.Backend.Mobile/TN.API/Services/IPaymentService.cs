﻿using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Payoo.Lib;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.Backend.Model.Command;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;
using ZaloPay.Helper;
using ZaloPay.Helper.Crypto;
using static TN.API.Services.PaymentService;

namespace TN.API.Services
{
    public interface IPaymentService : IService<Account>
    {
        Task<ApiResponse<object>> Deposit(int accountId, string accountName, PaymentDepositCommand model, HttpRequest request);
        Task<ApiResponse<object>> DepositDone(
            int accountid,
            int amount,
            string message,
            string payment_type,
            string reference_number,
            int status,
            int trans_ref_no,
            int website_id,
            string signature,
            int type,
            bool rst);
        Task<ApiResponse<object>> WalletGets(int accountid);
        Task<ApiResponse<object>> WalletTransactionGets(int accountid, int page, int limit);
        Task<ApiResponse<object>> WalletImportCode(int accountid, string code, string lat, string lng);
        Task<ApiResponse<object>> RedeemPointAdd(int accountId, int id);
        Task<ApiResponse<object>> GiftCardGetData(int accountId, string lat, string lng);
        Task<ApiResponse<object>> UserMomoConfirm(int accountId, MomoConfirmCommand model);
        Task<object> MomoCallbackNotify(HttpRequest request, MomoNotifyDataDTO dataX);
        Task<object> ZaloCallbackNotify(dynamic cbdata);
        Task<ApiResponse<object>> UserZaloConfirm(int accountId, UserZaloConfirmCommand model);
        Task<ApiResponse<object>> WalletTransactionGiftSearch(int accountid, int page, int limit);
        Task<ApiResponse<object>> WalletTransactionGiftCheckAccount(int accountId, string phone);
        Task<ApiResponse<object>> WalletTransactionGiftPost(int accountId, WalletTransactionGiftCommand model);
        Task<ApiResponse<object>> RecheckCallBackZalo(int id);
        Task<ApiResponse<object>> MomoRefund(int id);
        Task<ApiResponse<object>> MomoCheckStatus(int id);
        Task<ApiResponse<object>> ClearSubPoint();
        Task<ApiResponse<object>> SubPointSettlement();
        Task<ApiResponse<object>> RecheckCallBackVTCPay();
        Task<ApiResponse<object>> RefundPoint(int accountId, DateTime time);
        Task<ApiResponse<object>> RefundAddPoint(DateTime time);

        Task<ApiResponse<object>> UserPayooConfirm(int accountId, UserPayooConfirmCommand model);
        Task<object> PayooCallbackNotify(PayooNotifyCommand cbdata);
        Task<ApiResponse<object>> RecheckCallBackPayoo(int id);
        Task<ApiResponse<object>> DiscountCodes(int accountId, string lat, string lng);
    }

    public class PaymentService : IPaymentService
    {
        private readonly IWalletRepository _iWalletRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly IVoucherCodeRepository _iVoucherCodeRepository;
        private readonly ICampaignRepository _iCampaignRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly IRedeemPointRepository _iRedeemPointRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly MonoSettings _monoSettings;
        private readonly ZaloSettings _zaloSettings;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IFireBaseService _iFireBaseService;
        private readonly BaseSetting _baseSetting;
        private readonly IBookingRepository _iBookingRepository;
        private readonly IHubService _iHubService;
        private readonly IEmailManageRepository _iEmailManageRepository;
        private readonly IEmailBoxRepository _iEmailBoxRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly ITransactionDiscountRepository _iTransactionDiscountRepository;
        private readonly PayooSettings _payooSettings;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IMemoryCacheService _iMemoryCacheService;
        private readonly IDiscountCodeRepository _discountCodeRepository;

        public PaymentService(
            IWalletRepository iWalletRepository, 
            IWalletTransactionRepository iWalletTransactionRepository, 
            IVoucherCodeRepository iVoucherCodeRepository,
            ICampaignRepository iCampaignRepository,
            IProjectAccountRepository iProjectAccountRepository,
            IRedeemPointRepository iRedeemPointRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IProjectRepository iProjectRepository,
            IMongoDBRepository iMongoDBRepository,
            IOptions<LogSettings> logSettings,
            IOptions<MonoSettings> monoSettings,
            IAccountRepository iAccountRepository,
            IOptions<ZaloSettings> zaloSettings,
            IFireBaseService iFireBaseService,
            IOptions<BaseSetting> baseSetting, 
            IBookingRepository iBookingRepository,
            IHubService iHubService,
            IEmailManageRepository iEmailManageRepository,
            IEmailBoxRepository iEmailBoxRepository,
            IWebHostEnvironment iWebHostEnvironment,
            ITransactionRepository iTransactionRepository,
            ITransactionDiscountRepository iTransactionDiscountRepository,
            IOptions<PayooSettings> payooSettings,
            IHttpClientFactory httpClientFactory,
            IMemoryCacheService iMemoryCacheService,
            IDiscountCodeRepository discountCodeRepository
            )
        {
            _iWalletRepository = iWalletRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iVoucherCodeRepository = iVoucherCodeRepository;
            _iCampaignRepository = iCampaignRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iRedeemPointRepository = iRedeemPointRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iProjectRepository = iProjectRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _monoSettings = monoSettings.Value;
            _iAccountRepository = iAccountRepository;
            _zaloSettings = zaloSettings.Value;
            _iFireBaseService = iFireBaseService;
            _baseSetting = baseSetting.Value;
            _iBookingRepository = iBookingRepository;
            _iHubService = iHubService;
            _iEmailManageRepository = iEmailManageRepository;
            _iEmailBoxRepository = iEmailBoxRepository;
            _iWebHostEnvironment = iWebHostEnvironment;
            _iTransactionRepository = iTransactionRepository;
            _iTransactionDiscountRepository = iTransactionDiscountRepository;
            _payooSettings = payooSettings.Value;
            _httpClientFactory = httpClientFactory;
            _iMemoryCacheService = iMemoryCacheService;
            _discountCodeRepository = discountCodeRepository;
        }

        #region [Gửi email đơn]
        public async Task SendBil(string functionName, string requestId, int accountId, WalletTransaction dlAdd)
        {
            try
            {
                dlAdd = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.Id == dlAdd.Id);
                if(dlAdd == null)
                {
                    return;
                }    
                // Gửi email cho khách hàng
                var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                if (account != null && !string.IsNullOrEmpty(account.Email))
                {
                    var emailManager = await _iEmailManageRepository.SearchOneAsync(true, x => x.Id > 0);
                    if (emailManager != null)
                    {
                        var template = $"{_iWebHostEnvironment.ContentRootPath}/Export/Templates/bill.html";
                        if (File.Exists(template))
                        {
                            var fileData = File.ReadAllText(template);
                            fileData = fileData.Replace("{{PadTime}}", dlAdd.PadTime?.ToString("HH:mm dd/MM/yyyy"))
                                               .Replace("{{OrderIdRef}}", dlAdd.OrderIdRef ?? "#")
                                               .Replace("{{TransactionType}}", dlAdd.Type.GetDisplayName())
                                               .Replace("{{CustomerCode}}", account.Code)
                                               .Replace("{{CustomerName}}", account.FullName)
                                               .Replace("{{Amount}}", Function.FormatMoney(dlAdd.Amount))
                                               .Replace("{{SubAmount}}", Function.FormatMoney(dlAdd.SubAmount))
                                               .Replace("{{Price}}", Function.FormatMoney(dlAdd.Price))
                                               .Replace("{{PaymentGroup}}", dlAdd.PaymentGroup.GetDisplayName())
                                               .Replace("{{TotalAmount}}", Function.FormatMoney(dlAdd.Amount + dlAdd.SubAmount));

                            await _iEmailBoxRepository.AddAsync(new EmailBox
                            {
                                IsSend = false,
                                From = emailManager.From,
                                To = account.Email,
                                Subject = $"[TNGO MAIL] Bạn vừa thực hiện giao dịch nạp điểm cho TNGO #{dlAdd.TransactionCode}",
                                Body = fileData,
                                CreatedDate = DateTime.Now,
                                NotificationDataId = 0,
                                NotSend = false,
                                ObjectId = dlAdd.Id,
                                SendDate = DateTime.Now,
                                Type = 0
                            });
                            await _iEmailBoxRepository.CommitAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"{ex}", true);
            }
           
        }
        #endregion

        #region Zalo 1.4 Tự check giao dịch VTCPay

        private async Task<VTCCheckStatusDTO> CheckStatusVTCPayAsync(List<ListMerchantOrderCode> orderCodes)
        {
            var sendObj = new
            {
                merchantType= "WEBSITE",
                intergratedID= 10309,
                revceiverAccount= "0916992864",
                sign= "52c70b9302bc1d0c7cd6f643d35f8fb0",
                listMerchantOrderCode = orderCodes
            };

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri("https://vtcpay.vn/bank-gateway/api/AccountApi/VTCPayGetOrderStatus"))
            {
                Content = new StringContent(JsonConvert.SerializeObject(sendObj), Encoding.UTF8, "application/json")
            };
            using var http = new HttpClient();
            var response = await http.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VTCCheckStatusDTO>(content);
            }
            else
            {
                return null;
            }
        }

        public async Task<ApiResponse<object>> RecheckCallBackVTCPay()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.RecheckCallBackVTCPay";
            try
            {
                var startDate = Convert.ToDateTime($"2021-12-21");
                var historys = await _iWalletTransactionRepository.SearchByTopAsync(20, x => x.Type == EWalletTransactionType.Deposit && x.PaymentGroup == EPaymentGroup.VTCPay && (x.Status == EWalletTransactionStatus.Draft || x.Status == EWalletTransactionStatus.Pending) && x.CreatedDate <= DateTime.Now.AddMinutes(-10) && x.CreatedDate>= DateTime.Now.AddDays(-10));
                AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Tìm thấy {historys.Count} bản ghi");
                if (!historys.Any())
                {
                    return ApiResponse.WithStatus(true, "Không tìm thấy bản ghi nào", ResponseCode.Ok);
                }

                var dataCall = await CheckStatusVTCPayAsync(historys.Select(x => new ListMerchantOrderCode { OrderCode = x.TransactionCode }).ToList());

                if(dataCall == null || dataCall.ResponseCode != 1 || dataCall.ListOrderStatus == null)
                {
                    return ApiResponse.WithStatus(true, "Gọi API check status không thành công", ResponseCode.Ok);
                }

                foreach(var item in dataCall.ListOrderStatus)
                {
                    if(item.Status == 0 && item.Description?.ToLower() != "Order not success".ToLower())
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Giao dịch {item.OrderCode} trạng thái nháp");
                        // Không xử lý gì
                    }
                    else if (item.Status == 1)
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Giao dịch {item.OrderCode} set trạng thái thành công");

                        // + tiền cho khách
                        var history = await _iWalletTransactionRepository.SearchOneAsync(false, x => x.TransactionCode == item.OrderCode && x.Type == EWalletTransactionType.Deposit && x.PaymentGroup == EPaymentGroup.VTCPay && x.Status != EWalletTransactionStatus.Done);
                        if(history != null)
                        {
                            var gVi = await _iWalletRepository.GetWalletAsync(history.AccountId);

                            history.Status = EWalletTransactionStatus.Done;
                            history.Note += $";Recheck nạp tiền thành công";
                            history.Wallet_Balance = gVi.Balance;
                            history.Wallet_SubBalance = gVi.SubBalance;
                            history.Wallet_HashCode = gVi.HashCode;
                            history.Wallet_TripPoint = gVi.TripPoint;

                            var depsitEvent = await _iWalletTransactionRepository.EventByAccount(history.AccountId, history.Amount, history.LocationProjectId ?? 0, history.LocationCustomerGroupId ?? 0);
                            if (depsitEvent != null)
                            {
                                history.DepositEventId = depsitEvent.Id;
                                history.CampaignId = depsitEvent.CampaignId;
                                history.DepositEvent_SubAmount = depsitEvent.ToSubPrice;
                                history.DepositEvent_Amount = depsitEvent.ToPrice;
                                history.ReqestId = depsitEvent.TransactionCode;
                                history.Amount += depsitEvent.ToPrice;
                                history.SubAmount += depsitEvent.ToSubPrice;
                                history.Note += $" (Trong sự kiện '{ depsitEvent.Campaign?.Name}' giao dịch được +{Function.FormatMoney(depsitEvent.ToPrice)} điểm, +{Function.FormatMoney(depsitEvent.ToSubPrice)} điểm khuyến mãi)";
                                AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $" (Trong sự kiện '{ depsitEvent.Campaign?.Name}' giao dịch được +{Function.FormatMoney(depsitEvent.ToPrice)} điểm, +{Function.FormatMoney(depsitEvent.ToSubPrice)} điểm khuyến mãi)");
                                await _iWalletTransactionRepository.UpdateUsed(depsitEvent.Id);
                            }
                            history.TotalAmount = history.Amount + history.SubAmount;
                            history.HashCode = Function.TransactionHash(history.OrderIdRef, history.Amount, history.SubAmount, history.TripPoint, _baseSetting.TransactionSecretKey);
                            history.PadTime = DateTime.Now;
                            _iWalletTransactionRepository.Update(history);
                            await _iWalletTransactionRepository.CommitAsync();

                            await _iWalletRepository.UpdateAddWalletAsync(history.AccountId, history.Amount, history.SubAmount, 0, _baseSetting.TransactionSecretKey, true, true, depsitEvent?.SubPointExpiry ?? 0);

                            AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Nạp tiền thành công {history.Amount}, note { history.Note }");
                            // Gửi notify
                            await _iFireBaseService.SendNotificationByAccount(functionName, requestId, history.AccountId, ENotificationTempType.NapTienThanhCong, true, DockEventType.Not, null, $"{Function.FormatMoney(history.TotalAmount)} điểm", EPaymentGroup.VTCPay.GetDisplayName());
                            // Gửi cho admin phần quản lý
                            await _iHubService.EventSend(EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, history.AccountId, null, 0, 0, 0, 0, $"[VTCPAY] Khách hàng vừa nạp thành công {Function.FormatMoney(history.Amount)} điểm vào tài khoản");
                        }    
                    }
                    else if (item.Status == 7 || item.Status == 3)
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Giao dịch {item.OrderCode} set trạng thái pedding");
                        // set giao dịch thành pending
                        var ktHistory = await _iWalletTransactionRepository.SearchOneAsync(false, x => x.TransactionCode == item.OrderCode && x.Type == EWalletTransactionType.Deposit && x.PaymentGroup == EPaymentGroup.VTCPay && x.Status == EWalletTransactionStatus.Draft);
                        if (ktHistory != null)
                        {
                            ktHistory.Status = EWalletTransactionStatus.Pending;
                            ktHistory.SetPenddingTime = DateTime.Now;
                            ktHistory.PadTime = DateTime.Now;
                            ktHistory.Note += "; ReCheck giao dịch đang chờ";
                            _iWalletTransactionRepository.Update(ktHistory);
                            await _iWalletTransactionRepository.CommitAsync();
                        }
                    }
                    else
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Giao dịch {item.OrderCode}  set trạng thái hủy, trạng thái {item.Status}");
                        var ktHistory = await _iWalletTransactionRepository.SearchOneAsync(false, x => x.TransactionCode == item.OrderCode && x.Type == EWalletTransactionType.Deposit && x.PaymentGroup == EPaymentGroup.VTCPay && (x.Status == EWalletTransactionStatus.Draft || x.Status == EWalletTransactionStatus.Pending));
                        if(ktHistory != null)
                        {
                            ktHistory.Status = EWalletTransactionStatus.Cancel;
                            ktHistory.SetPenddingTime = DateTime.Now;
                            ktHistory.PadTime = DateTime.Now;
                            ktHistory.Note += $"; ReCheck giao dịch thất bại status {item.Status}";
                            _iWalletTransactionRepository.Update(ktHistory);
                            await _iWalletTransactionRepository.CommitAsync();
                        }
                    }
                }
                return ApiResponse.WithStatus(true, $"Hệ thống trả về {JsonConvert.SerializeObject(dataCall)}", ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [VTC pay 2. Khi nạp tiền xong link sẽ redirect về đây để xác nhận đã nạp tiền]
        public async Task<ApiResponse<object>> DepositDone(
          int accountid,
          int amount,
          string message,
          string payment_type,
          string reference_number,
          int status,
          int trans_ref_no,
          int website_id,
          string signature,
          int type,
          bool rst)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.DepositDone";
            try
            {
                AddLog(accountid, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountid} Deposit Done");
                if (rst)
                {
                    AddLog(accountid, functionName, requestId, EnumMongoLogType.Center, string.Format("accountid={0} <br/>amount={1}<br/>message={2}<br/>payment_type={3}<br/>reference_number={4}<br/>status={5}<br/>trans_ref_no={6}<br/>website_id={7}<br/>signature={8}", accountid, amount, message, payment_type, reference_number, status, trans_ref_no, website_id, signature));
                    var history = await _iWalletTransactionRepository.SearchOneAsync(false,
                        x => x.AccountId == accountid 
                        && x.TransactionCode == reference_number 
                        && x.Status == EWalletTransactionStatus.Draft 
                        && x.Type == EWalletTransactionType.Deposit 
                        && x.PaymentGroup == EPaymentGroup.VTCPay
                        && x.CreatedDate >= DateTime.Now.AddDays(-10) 
                        && x.CreatedDate < DateTime.Now.AddHours(1)
                        );

                    if (history == null)
                    {
                        AddLog(accountid, functionName, requestId, EnumMongoLogType.End, $"Nạp tiền thất bại Không tìm thấy mã nạp tiền online accountid {accountid}, reference_number {reference_number}");
                        return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                    }

                    var checkGD = await VTCPayHistoryBy(history.AccountId, history.OrderIdRef, functionName, requestId);
                    if(checkGD == null)
                    {
                        AddLog(accountid, functionName, requestId, EnumMongoLogType.End, $"Nạp tiền thất bại Không tìm thấy mã blockchange");
                        return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                    }    

                    var gVi = await _iWalletRepository.GetWalletAsync(accountid);

                    history.Status = EWalletTransactionStatus.Done;
                    history.Note = $"Nạp tiền qua cổng trực tuyến {payment_type}, trans_ref_no {trans_ref_no}, message {message}";
                    history.Wallet_Balance = gVi.Balance;
                    history.Wallet_SubBalance = gVi.SubBalance;
                    history.Wallet_HashCode = gVi.HashCode;
                    history.Wallet_TripPoint = gVi.TripPoint;

                    var depsitEvent = await _iWalletTransactionRepository.EventByAccount(accountid, history.Amount, history.LocationProjectId ?? 0, history.LocationCustomerGroupId ?? 0);
                    if (depsitEvent != null)
                    {
                        history.DepositEventId = depsitEvent.Id;
                        history.CampaignId = depsitEvent.CampaignId;
                        history.DepositEvent_SubAmount = depsitEvent.ToSubPrice;
                        history.DepositEvent_Amount = depsitEvent.ToPrice;
                        history.ReqestId = depsitEvent.TransactionCode;
                        history.Amount += depsitEvent.ToPrice;
                        history.SubAmount += depsitEvent.ToSubPrice;
                        history.Note += $" (Trong sự kiện '{ depsitEvent.Campaign?.Name}' giao dịch được +{Function.FormatMoney(depsitEvent.ToPrice)} điểm, +{Function.FormatMoney(depsitEvent.ToSubPrice)} điểm khuyến mãi)";
                        AddLog(accountid, functionName, requestId, EnumMongoLogType.Center, $" (Trong sự kiện '{ depsitEvent.Campaign?.Name}' giao dịch được +{Function.FormatMoney(depsitEvent.ToPrice)} điểm, +{Function.FormatMoney(depsitEvent.ToSubPrice)} điểm khuyến mãi)");
                        await _iWalletTransactionRepository.UpdateUsed(depsitEvent.Id);
                    }
                    history.TotalAmount = history.Amount + history.SubAmount;
                    history.HashCode = Function.TransactionHash(history.OrderIdRef, history.Amount, history.SubAmount, history.TripPoint, _baseSetting.TransactionSecretKey);
                    history.PadTime = DateTime.Now;
                    _iWalletTransactionRepository.Update(history);
                    await _iWalletTransactionRepository.CommitAsync();

                    await _iWalletRepository.UpdateAddWalletAsync(accountid, history.Amount, history.SubAmount, 0, _baseSetting.TransactionSecretKey, true, true, depsitEvent?.SubPointExpiry ?? 0);
                    
                    AddLog(accountid, functionName, requestId, EnumMongoLogType.End, $"Nạp tiền thành công {history.Amount}, note { history.Note }");
                    // Gửi notify
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId,history.AccountId, ENotificationTempType.NapTienThanhCong, true, DockEventType.Not, null, $"{Function.FormatMoney(history.TotalAmount)} điểm", EPaymentGroup.VTCPay.GetDisplayName());
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, accountid, null, 0, 0, 0, 0, $"[VTCPAY] Khách hàng vừa nạp thành công {Function.FormatMoney(history.Amount)} đ vào tài khoản");
                    await SendBil(functionName, requestId, history.AccountId, history);
                    return ApiResponse.WithData(history);

                }
                else
                {
                    AddLog(accountid, functionName, requestId, EnumMongoLogType.End, "Nạp tiền thất bại " + string.Format("accountid={0} <br/>amount={1}<br/>message={2}<br/>payment_type={3}<br/>reference_number={4}<br/>status={5}<br/>trans_ref_no={6}<br/>website_id={7}<br/>signature={8}", accountid, amount, message, payment_type, reference_number, status, trans_ref_no, website_id, signature));
                    // Gửi notify
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountid, ENotificationTempType.NapTienKhongThanhCong, true, DockEventType.Not, null);
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }
            }
            catch (Exception ex)
            {
                AddLog(accountid, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [All 1. Lấy link nạp tiền]
        public  async Task<ApiResponse<object>> Deposit(int accountId, string accountName, PaymentDepositCommand model, HttpRequest request)
        {
            // Lấy projectId khi nạp điểm
            var findProject = await _iMemoryCacheService.LatLngToCustomerGroup(model.Lat, model.Lng, accountId);

            if (model.Type == "momo")
            {
                return await DepositMomo(accountId, model, findProject);
            }    
            else if (model.Type == "zalo")
            {
                return await DepositZalo(accountId, model, findProject);
            }
            else if (model.Type == "payoo")
            {
                return await DepositPayoo(accountId, model, request, findProject);
            }
            else
            {
                return await DepositVTC(accountId, model, findProject);
            }    
        }

        private async Task<ApiResponse<object>> DepositPayoo(int accountId, PaymentDepositCommand model, HttpRequest request, ProjectAccountModel locationProject)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.DepositPayoo";
            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} data: {JsonConvert.SerializeObject(model)} location Project {JsonConvert.SerializeObject(locationProject)}");
            try
            {
                if (model.Total < 10000)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Money nhỏ hơn 10k {model.Total}");
                    return ApiResponse.WithStatus(false, null, ResponseCode.SmallestDeposit);
                }

                var orderId = Function.GenOrderId((int)EPaymentGroup.PayooPay);

                string description = string.Format(_payooSettings.Description ?? "{0}", orderId);

                var wallet = await _iWalletRepository.GetWalletAsync(accountId);
                // Tạo một giao dịch pending để xử lý dữ liệu trả về có tồn tại không
                var dlAdd = new WalletTransaction
                {
                    AccountId = accountId,
                    Amount = model.Total,
                    CampaignId = 0,
                    CreatedDate = DateTime.Now,
                    Type = EWalletTransactionType.Deposit,
                    TransactionCode = null,
                    TransactionId = 0,
                    Status = EWalletTransactionStatus.Draft,
                    WalletId = wallet.Id,
                    OrderIdRef = orderId,
                    IsAsync = false,
                    SubAmount = 0,
                    TotalAmount = model.Total,
                    PaymentGroup = EPaymentGroup.PayooPay,
                    Note = description,
                    Price = model.Total,
                    PadTime = DateTime.Now,
                    LocationProjectId = locationProject.ProjectId,
                    LocationProjectOverwrite = locationProject.LocationProjectOverwrite,
                    LocationCustomerGroupId = locationProject.CustomerGroupId
                };
                await _iWalletTransactionRepository.AddAsync(dlAdd);
                await _iWalletTransactionRepository.CommitAsync();

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Tạo ra lịch sử giao dịch nháp, #{dlAdd.Id}, orderId {orderId}");

                var order = new PayooDepositCommand
                {
                    Username = _payooSettings.BusinessUsername,
                    ShopId = _payooSettings.ShopID.ToString(),
                    ShopTitle = _payooSettings.ShopTitle,
                    ShopDomain = _payooSettings.ShopDomain,
                    // Thông tin điều chỉnh
                    Session = orderId,
                    OrderNo = dlAdd.OrderIdRef, // Mã giao dịch (mã đơn hàng)
                    OrderShipDate = DateTime.Now.ToString("dd/MM/yyyy"),     // Thời gian dự kiến thanh toán > hiện tại
                    OrderShipDays = "0",
                    NotifyUrl = $"{_payooSettings.NotifyUrl}",
                    ValidityTime = DateTime.Now.AddMinutes(30).ToString("yyyyMMddHHmmss"), // Thời gian sẵn sàng thanh toán > hiện tại
                    JsonResponse = "true",
                    // Thông tin đơn hàng app gửi lên
                    ShopBackUrl = _payooSettings.ShopBackUrl,
                    OrderCashAmount = Convert.ToInt32(model.Total).ToString().Replace(".", "").Replace(",", ""),
                    OrderDescription = HttpUtility.UrlEncode(description)
                };

                var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                order.Name = account?.FullName;
                order.Phone = account?.Phone;
                order.Email = account?.Email;
                order.Address = account?.Address;
                order.AccountId = account?.Id ?? 0;
                var orderXml = PaymentXMLFactory.GetPaymentXML(order);

                var checksumValue = PayooPayGenerateHashString(_payooSettings.ChecksumKey + orderXml);

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi cho server Payoo {JsonConvert.SerializeObject(order)}");

                return ApiResponse.WithStatus(true, new { ShopId = _payooSettings.ShopID, OrderInfo = orderXml, CheckSum = checksumValue, MerchantShareKey = _payooSettings.MerchantShareKey, CashAmount = order.OrderCashAmount}, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private async Task<ApiResponse<object>> DepositZalo(int accountId, PaymentDepositCommand model, ProjectAccountModel locationProject)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.DepositZalo";
            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} data: {JsonConvert.SerializeObject(model)}, { JsonConvert.SerializeObject(locationProject) }");
            try
            {
                if (model.Total < 10000)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Money nhỏ hơn 10k {model.Total}");
                    return ApiResponse.WithStatus(false, null, ResponseCode.SmallestDeposit);
                }

                var orderId = Function.GenOrderId((int)EPaymentGroup.ZaloPay);

                string description = string.Format(_zaloSettings.Description, orderId);
                var wallet = await _iWalletRepository.GetWalletAsync(accountId);
                // Tạo một giao dịch pending để xử lý dữ liệu trả về có tồn tại không
                var dlAdd = new WalletTransaction
                {
                    AccountId = accountId,
                    Amount = model.Total,
                    CampaignId = 0,
                    CreatedDate = DateTime.Now,
                    Type = EWalletTransactionType.Deposit,
                    TransactionCode = null,
                    TransactionId = 0,
                    Status = EWalletTransactionStatus.Draft,
                    WalletId = wallet.Id,
                    OrderIdRef = orderId,
                    IsAsync = false,
                    SubAmount = 0,
                    TotalAmount = model.Total,
                    PaymentGroup = EPaymentGroup.ZaloPay,
                    Note = description,
                    Price = model.Total,
                    PadTime = DateTime.Now,
                    LocationProjectId = locationProject.ProjectId,
                    LocationCustomerGroupId = locationProject.CustomerGroupId,
                    LocationProjectOverwrite = locationProject.LocationProjectOverwrite
                };
                await _iWalletTransactionRepository.AddAsync(dlAdd);
                await _iWalletTransactionRepository.CommitAsync();

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Tạo ra lịch sử giao dịch nháp, #{dlAdd.Id}, orderId {orderId}");

                var embedData = new DepositZaloEmbedDataDTO()
                {
                    Redirecturl = _zaloSettings.NotifyResultUrl,
                    ColumnInfo = JsonConvert.SerializeObject(new ZaloColumnInfoDTO { AccountId = accountId, Amount = dlAdd.Amount, Description = description, OrderIdRef = orderId, PromotionInfo = null }),
                    ZlppaymentId = null
                };

                var param = new Dictionary<string, string>
                {
                    // Field request
                    {"app_user", accountId.ToString() },
                    {"amount", model.Total.ToString() },
                    {"embed_data", JsonConvert.SerializeObject(embedData)},
                    {"item", JsonConvert.SerializeObject(new List<ZaloItemDTO>(){ new ZaloItemDTO { Id = "ZALO", Name = description, Price = dlAdd.Amount, Quantity = 1 } })},
                    {"bank_code", ""},
                     // Field setting
                    {"app_trans_id", orderId},
                    {"app_id", _zaloSettings.AppId},
                    {"app_time", Utils.GetTimeStamp().ToString()},
                    {"description",description},
                    {"currency", _zaloSettings.Language  }
                };

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi cho server Zalo {JsonConvert.SerializeObject(param)}");

                var createOrder = await CreateOrder(accountId , functionName, requestId, param);

                if(createOrder == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Phản hồi Null, đơn hàng thất bại");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaoDonHangThatBai);
                }    
                else if(createOrder.ReturnCode == 1)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tạo đơn hàng thành công");
                    return ApiResponse.WithStatus(true, createOrder, ResponseCode.Ok);
                }    
                else if (createOrder.ReturnCode == 2)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Phản hồi {createOrder.ReturnCode}, đơn hàng thất bại");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaoDonHangThatBai);
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Phản hồi {createOrder.ReturnCode}, đơn hàng thất bại");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DongHangChuaThanhToan);
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private async Task<ApiResponse<object>> DepositMomo(int accountId, PaymentDepositCommand model, ProjectAccountModel locationProject)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.DepositMomo";

            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} data: {JsonConvert.SerializeObject(model)}, {JsonConvert.SerializeObject(locationProject)}");
            try
            {
                if (model.Total < 1000)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Money nhỏ hơn 10k {model.Total}");
                    return ApiResponse.WithStatus(false, null, ResponseCode.SmallestDeposit);
                }

                var code = Function.GenOrderId((int)EWalletTransactionType.Deposit);

                var objectOut = new DepositMomoDTO()
                {
                    Action = _monoSettings.Action,
                    Amount = model.Total,
                    AppScheme = _monoSettings.AppScheme,
                    Description = string.Format(_monoSettings.Description, code),
                    Extra = null,
                    Fee = _monoSettings.Fee,
                    Language = _monoSettings.Language,
                    MerchantCode = _monoSettings.MerchantCode,
                    MerchantName = _monoSettings.MerchantName,
                    MerchantNameLabel = _monoSettings.MerchantNameLabel,
                    OrderId = code,
                    OrderLabel = $"Mã giao dịch: {code}",
                    Partner = _monoSettings.Partner,
                    Username = _monoSettings.Username
                };

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Data : {JsonConvert.SerializeObject(objectOut.OrderId)}");

                var wallet = await _iWalletRepository.GetWalletAsync(accountId);
                // Tạo một giao dịch pending để xử lý dữ liệu trả về có tồn tại không
                var dlAdd = new WalletTransaction
                {
                    AccountId = accountId,
                    Amount = model.Total,
                    CampaignId = 0,
                    CreatedDate = DateTime.Now,
                    Type = EWalletTransactionType.Deposit,
                    TransactionCode = null,
                    TransactionId = 0,
                    Status = EWalletTransactionStatus.Draft,
                    WalletId = wallet.Id,
                    OrderIdRef = code,
                    IsAsync = false,
                    SubAmount = 0,
                    TotalAmount = model.Total,
                    PaymentGroup = EPaymentGroup.MomoPay,
                    Note = objectOut.Description,
                    Price = model.Total,
                    PadTime = DateTime.Now,
                    LocationProjectId = locationProject.ProjectId,
                    LocationProjectOverwrite = locationProject.LocationProjectOverwrite,
                    LocationCustomerGroupId = locationProject.CustomerGroupId
                };

                await _iWalletTransactionRepository.AddAsync(dlAdd);
                await _iWalletTransactionRepository.CommitAsync();
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tạo ra lịch sử giao dịch nháp, #{dlAdd.Id}");
                return ApiResponse.WithData(objectOut);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private async Task<PaymentHistoryResponseDTO> VTCPayHistoryBy(int accountId, string orderIdRef,string functionName, string requestId)
        {
            try
            {
                var api = new PaymentApiService(_baseSetting.AddressAPIPayment, _baseSetting.ApiRequestTimeout);
                var data = await api.History(new PaymentHistoryCommand
                {
                    Accountid = accountId,
                    Cmd = "history",
                    Start = 0,
                    Size = 20,
                    MerchantCode = _baseSetting.MerchantCode,
                    Smartcontract = _baseSetting.SmartContract
                });

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Data Blockchange {JsonConvert.SerializeObject(data)}");

                if (data.Status == 1)
                {
                    return data.Data.FirstOrDefault(x => x.OrderIdRef == orderIdRef && x.TypeTrans == "deposit" && x.StatusBanking == "1");
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"{ex}", true);
            }
            return null;
        }

        private async Task<ApiResponse<object>> DepositVTC(int accountId, PaymentDepositCommand model, ProjectAccountModel locationProject)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.DepositVTC";
     
            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} data: {JsonConvert.SerializeObject(model)}, { JsonConvert.SerializeObject(locationProject) }");
            try
            {
                if (model.Total < 10000)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Money nhỏ hơn 10k {model.Total}");
                    return ApiResponse.WithStatus(false, null, ResponseCode.SmallestDeposit);
                }

                var code = Function.GenOrderId((int)EWalletTransactionType.Deposit);

                using var client = new HttpClient();
                model.Cmd = "deposit";
                model.Code = code;
                model.Merchantcode = _baseSetting.MerchantCode;
                model.Accountid = accountId;

                var api = new PaymentApiService(_baseSetting.AddressAPIPayment, _baseSetting.ApiRequestTimeout);
                var data = await api.Deposit(model);
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Data : {data?.Data?.Txtid}");
                var wallet = await _iWalletRepository.GetWalletAsync(accountId);
                if (data != null && data.Status == 1)
                {
                    // Tạo một giao dịch pending để xử lý dữ liệu trả về có tồn tại không
                    var dlAdd = new WalletTransaction
                    {
                        AccountId = accountId,
                        Amount = model.Total,
                        CampaignId = 0,
                        CreatedDate = DateTime.Now,
                        Type = EWalletTransactionType.Deposit,
                        TransactionCode = data.Data.Txtid,
                        TransactionId = 0,
                        Status = EWalletTransactionStatus.Draft,
                        WalletId = wallet.Id,
                        OrderIdRef = code,
                        IsAsync = false,
                        SubAmount = 0,
                        TotalAmount = model.Total,
                        PaymentGroup = EPaymentGroup.VTCPay,
                        Price = model.Total,
                        PadTime = DateTime.Now,
                        LocationProjectId = locationProject.ProjectId,
                        LocationProjectOverwrite = locationProject.LocationProjectOverwrite,
                        LocationCustomerGroupId = locationProject.CustomerGroupId
                    };

                    await _iWalletTransactionRepository.AddAsync(dlAdd);
                    await _iWalletTransactionRepository.CommitAsync();
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Tạo ra lịch sử giao dịch ẩn giao dịch {dlAdd.Id}");
                    return ApiResponse.WithData(data.Data.UrlTemp);
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Tạo link thanh toán thất bại");
                    return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}",true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError, ex.ToString());
            }
        }
        #endregion

        #region [PAYOO]

        #region Payoo 2.Tạo một đơn & Xác nhận pending giao dịch (quy ước với app)
        public async Task<ApiResponse<object>> UserPayooConfirm(int accountId, UserPayooConfirmCommand model)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.UserPayooConfirm";
            try
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} data: {JsonConvert.SerializeObject(model)}");

                if (model.ReturnCode == 1)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"User xác nhận thanh toán thành công status = {model.ReturnCode} để set pending");
                    var ktHistory = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.OrderIdRef == model.OrderNo && x.PaymentGroup == EPaymentGroup.PayooPay && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));
                    if (ktHistory == null)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy lịch sử mẫu giao dịch {model.OrderNo}");
                        return ApiResponse.WithStatus(false, null, ResponseCode.HetThoiGianThucHienGD);
                    }
                    else if (ktHistory.Status == EWalletTransactionStatus.Done)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Giao dịch đã thành công rồi, không cần set pending");
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                    else if (ktHistory.Status == EWalletTransactionStatus.Pending)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Giao dịch đã pending rồi, không cần set pending");
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                    else
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Set pending");
                        await WalletTransactionSetPending(functionName, requestId, accountId, EPaymentGroup.PayooPay, model.OrderNo);
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                }
                else if (model.ReturnCode == 2)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Hủy giao dịch");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanHuyGiaoDich);
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Nạp tiền payoo thất bại");
                    return ApiResponse.WithStatus(false, null, ResponseCode.NapTienThatBai);
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region Payoo 3. Nhận phản hồi từ backend payoo
        public async Task<object> PayooCallbackNotify(PayooNotifyCommand model)
        {
            await Task.Delay(1000);
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = $"PaymentService.PayooCallbackNotify";
            int accoutnId = 0;
            try
            {
                AddLog(accoutnId, functionName, requestId, EnumMongoLogType.Start, $"Payoo gửi phản hồi 1 { JsonConvert.SerializeObject(model) }");

                if (model == null || model.ResponseData == null || model.Signature == null)
                {
                   return new PayooCallback.ReturnCallback { ReturnCode = 0, Description = "NOTIFY_RECEIVED" };
                }

                var mySignature = GenerateCheckSum(_payooSettings.ChecksumKey + model.ResponseData + _payooSettings.IPPayoo);

                AddLog(accoutnId, functionName, requestId, EnumMongoLogType.Start, $"Payoo gửi phản hồi 0 { AppHttpContext.Current.Request.HttpContext.Connection.RemoteIpAddress }");

                if (mySignature != model.Signature)
                {
                    AddLog(accoutnId, functionName, requestId, EnumMongoLogType.Start, $"Signature does not match");
                    return new PayooCallback.ReturnCallback { ReturnCode = 0, Description = "NOTIFY_RECEIVED" };
                }
                else
                {
                    var dataRp = JsonConvert.DeserializeObject<PayooCallback.ResponseData>(model.ResponseData);
                    if (dataRp == null)
                    {
                        AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Không giải mã đc");
                        return new PayooCallback.ReturnCallback { ReturnCode = 0, Description = "NOTIFY_RECEIVED" };
                    }

                    if(dataRp.PaymentStatus != 1)
                    {
                        AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"PaymentStatus không bằng 1");
                        return new PayooCallback.ReturnCallback { ReturnCode = 0, Description = "NOTIFY_RECEIVED" };
                    }

                    var history = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.PaymentGroup == EPaymentGroup.PayooPay && x.Status != EWalletTransactionStatus.Done && x.OrderIdRef == dataRp.OrderNo && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));
                    if (history == null)
                    {
                        // Log lại những sự kiện ko tìm thấy giao dịch log
                        AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy lịch sử giao dịch PAYOO tương ứng, OrderIdRef: {dataRp.OrderNo}");
                        await WalletTransactionWarningHistory(functionName, requestId, accoutnId, EPaymentGroup.ZaloPay, dataRp.OrderNo, JsonConvert.SerializeObject(dataRp));
                        await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accoutnId, ENotificationTempType.NapTienKhongThanhCong, true, DockEventType.ZaloRespond, JsonConvert.SerializeObject(ApiResponse.WithStatus(false, null, ResponseCode.NapTienThatBai)));
                        return new PayooCallback.ReturnCallback { ReturnCode = 0, Description = "NOTIFY_RECEIVED" };
                    }
                    accoutnId = history.AccountId;
                    if (history.Status == EWalletTransactionStatus.Done)
                    {
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Giao dịch PAYOO đã thành công rồi: {history.OrderIdRef}");
                        return new PayooCallback.ReturnCallback { ReturnCode = 0, Description = "NOTIFY_RECEIVED" };
                    }

                    await WalletTransactionSetPad(functionName, requestId, history.TransactionCode, history.Id, EWalletTransactionStatus.Done, $"Pay by {dataRp.PaymentMethod}, {dataRp.CardIssuanceType}", requestId);
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Cộng tiền cho tài khoản khách hàng + {history.TotalAmount} điểm cho tài khoản");
                    // Gửi notify
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accoutnId, ENotificationTempType.NapTienThanhCong, true, DockEventType.PayooRespond, JsonConvert.SerializeObject(ApiResponse.WithStatus(true, null, ResponseCode.NapTienThanhCong)), $"{Function.FormatMoney(history.TotalAmount)} điểm", EPaymentGroup.PayooPay.GetDisplayName());
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, history.AccountId, null, 0, 0, 0, 0, $"[PAYOOPAY] Khách hàng vừa nạp thành công {Function.FormatMoney(history.Amount)} đ vào tài khoản");
                    await SendBil(functionName, requestId, history.AccountId, history);
                    return new PayooCallback.ReturnCallback { ReturnCode = 0, Description = "NOTIFY_RECEIVED" };
                }
            }
            catch (Exception ex)
            {
                AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region Payoo 3.1 Tự check giao dịch pending Payoo
        public async Task<ApiResponse<object>> RecheckCallBackPayoo(int id)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.RecheckCallBackPayoo";
            try
            {
                var history = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.Id == id && (x.Status == EWalletTransactionStatus.Pending || x.Status == EWalletTransactionStatus.Draft) && x.PaymentGroup == EPaymentGroup.PayooPay && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));
                if (history != null)
                {
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Start, $"Tìm thấy lịch sử giao dịch Payoo: {history.OrderIdRef}");

                    var requestData = new { OrderId = history.OrderIdRef, ShopId = _payooSettings.ShopID };
                    var objAPIRequest = new PayooRequestDataCommand
                    {
                        RequestData = JsonConvert.SerializeObject(requestData),
                        Signature = PayooPayGenerateHashString(_payooSettings.ChecksumKey + JsonConvert.SerializeObject(requestData))
                    };

                    using var httpclient = new HttpClient(new HttpClientHandler { ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; } });
                    httpclient.DefaultRequestHeaders.Add("APIUsername", _payooSettings.APIUsername);
                    httpclient.DefaultRequestHeaders.Add("APIPassword", _payooSettings.APIPassword);
                    httpclient.DefaultRequestHeaders.Add("APISignature", _payooSettings.APISignature);

                    var responseAPI = await httpclient.PostAsync($"{_payooSettings.UrlPayooAPI}/GetOrderInfo", new StringContent(JsonConvert.SerializeObject(objAPIRequest), Encoding.UTF8, "application/json") ).ConfigureAwait(false);
                    if (responseAPI.StatusCode != HttpStatusCode.OK)
                    {
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Không gửi đc api info");
                        return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
                    }

                    var dataOut = await responseAPI.Content.ReadAsStringAsync();

                    var objAPIResponse = JsonConvert.DeserializeObject<PayooRecheckResponseDTO>(dataOut);

                    var objAPIResponseData = JsonConvert.DeserializeObject<PayooRecheckResponseDataDTO>(objAPIResponse.ResponseData);

                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Tìm thấy lịch sử giao dịch payoo {JsonConvert.SerializeObject(objAPIResponse.ResponseData)}");
                    // Đơn không thành công
                    if(objAPIResponseData.ResponseCode == 3 || objAPIResponseData.ResponseCode == 8 || (objAPIResponseData.ResponseCode == 9 && history.CreatedDate.AddHours(1) < DateTime.Now))
                    {
                        history.IsAsync = true;
                        history.Status = EWalletTransactionStatus.Cancel;
                        history.PadTime = DateTime.Now;
                        history.Note += $";Recheck đơn hàng Payoo, trả về thất bại '{objAPIResponseData.ResponseCode}'";
                        _iWalletTransactionRepository.Update(history);
                        await _iWalletTransactionRepository.CommitAsync();

                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Recheck đơn hàng Payoo, trả về thất bại '{objAPIResponseData.ResponseCode}', đơn hàng tạo từ {history.CreatedDate:dd/MM/yyyy HH:mm:ss} ");
                        return ApiResponse.WithData(objAPIResponseData);
                    }
                    else if(objAPIResponseData.ResponseCode == 9)
                    {
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Recheck đơn hàng Payoo, chưa thanh toán '{objAPIResponseData.ResponseCode}', đơn hàng tạo từ {history.CreatedDate:dd/MM/yyyy HH:mm:ss}");
                        return ApiResponse.WithData(objAPIResponseData);
                    }    
                    else if(objAPIResponseData.ResponseCode == 0 && (objAPIResponseData.OrderStatus == 0 || objAPIResponseData.OrderStatus == 2 || objAPIResponseData.OrderStatus == 4))
                    {
                        // Đơn hàng oke
                        history = await _iWalletTransactionRepository.SearchOneAsync(false, x => x.Id == id && (x.Status == EWalletTransactionStatus.Pending || x.Status == EWalletTransactionStatus.Draft) && x.PaymentGroup == EPaymentGroup.PayooPay);
                        if (history == null)
                        {
                            AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy đơn hàng #{id}");
                            return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                        }
                        await WalletTransactionSetPad(functionName, requestId, null, history.Id, EWalletTransactionStatus.Done, "", requestId);
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Cộng tiền cho tài khoản khách hàng + {history.TotalAmount} điểm cho tài khoản");

                        await _iFireBaseService.SendNotificationByAccount(functionName, requestId, history.AccountId, ENotificationTempType.NapTienThanhCong, true, DockEventType.Not, null, $"{Function.FormatMoney(history.TotalAmount)} điểm", EPaymentGroup.PayooPay.GetDisplayName());

                        // Gửi cho admin phần quản lý
                        await _iHubService.EventSend(EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, history.AccountId, null, 0, 0, 0, 0, $"[PAYOOPAY] Khách hàng vừa nạp thành công {Function.FormatMoney(history.Amount)} đ vào tài khoản");
                        await SendBil(functionName, requestId, history.AccountId, history);
                        return ApiResponse.WithData(objAPIResponseData);
                    }
                    return ApiResponse.WithStatus(true, objAPIResponseData, ResponseCode.Ok);
                }
                else
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy đơn hàng #{id}");
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }


        #endregion

        #endregion

        #region [MOMO]

        #region [Momo 2. gửi yêu cầu treo tiền khách hàng]
        public async Task<ApiResponse<object>> UserMomoConfirm(int accountId, MomoConfirmCommand model)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.UserMomoConfirm";

            try
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} data: {JsonConvert.SerializeObject(model)}");
                if (model.Status == 0)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"User xác nhận thanh toán thành công status = {model.Status}");
                    var ktHistory = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.Status == EWalletTransactionStatus.Draft && x.OrderIdRef == model.OrderId && x.PaymentGroup == EPaymentGroup.MomoPay && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));
                    if (ktHistory == null)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy lịch sử mẫu giao dịch {model.OrderId}");
                        return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanHuyGiaoDich);
                    }
                    // Gửi lệnh cho momo giữ tiền khách hàng. giữ tiền ko dc => trả về hủy giao dịch => trả về cho app là thanh toán thành công
                    // Lưu ý: Thời gian timeout khi gọi API này tối thiểu phải là 20s để đảm bảo nhận được response từ MoMo.
                    var jsonString = new JsonMonoCommand()
                    {
                        Amount = ktHistory.Amount,
                        PartnerCode = _monoSettings.MerchantCode,
                        PartnerRefId = model.OrderId,
                        PartnerName = _monoSettings.MerchantName,
                        PartnerTransId = $"{accountId}"
                    };

                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"JsonString: {JsonConvert.SerializeObject(jsonString)}");
                    // Mã hóa json
                    var jsonStringEncryption = Function.RSAEncryption(JsonConvert.SerializeObject(jsonString), _monoSettings.PublicKey);

                    if (jsonStringEncryption == null)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mã hóa thất bại JsonStringEncryption Null");
                        return ApiResponse.WithStatus(false, null, ResponseCode.HetThoiGianThucHienGD);
                    }

                    var confirmMomo = new TNConfirmMonoCommand()
                    {
                        AppData = model.Data,
                        CustomerNumber = model.Phonenumber,
                        Description = string.Format(_monoSettings.Description, model.OrderId ?? ""),
                        ExtraData = null,
                        Version = 2,
                        PayType = 3,
                        Hash = jsonStringEncryption,
                        PartnerCode = _monoSettings.MerchantCode,
                        PartnerRefId = model.OrderId
                    };

                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi API đến Momo server yêu cầu momo treo tiền KH: { JsonConvert.SerializeObject(confirmMomo) }");
                    // Yêu cầu Momo server treo tiền
                    var confirmOutput = await MomoPathSetPendingMoney(confirmMomo, accountId, functionName, requestId, EnumMongoLogType.Center);

                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Momo server trả về: { (confirmOutput == null ? "" : JsonConvert.SerializeObject(confirmOutput))}");

                    if (confirmOutput == null)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Treo tiền momo thất bại");
                        // Gửi notify
                        await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.NapTienKhongThanhCong,true, DockEventType.Not, null);
                        return ApiResponse.WithStatus(false, null, ResponseCode.HetThoiGianThucHienGD);
                    }

                    if (confirmOutput.Status == 0)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Treo tiền momo thành công status {confirmOutput.Status}, transid {confirmOutput.Transid}, signature {confirmOutput.Signature}, Amount {confirmOutput.Amount}");
                        // Update log = pending
                        await WalletTransactionSetPending(functionName, requestId, accountId, EPaymentGroup.MomoPay, model.OrderId, confirmOutput.Transid, confirmOutput.Amount);
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);

                    }
                    else
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Xác nhận treo tiền momo thất bại");
                        return ApiResponse.WithStatus(false, null, ResponseCode.HetThoiGianThucHienGD);
                    }
                }
                else if (model.Status == 5)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Account hết thời gian thực hiện giao dịch (Timeout transaction)");
                    return ApiResponse.WithStatus(false, null, ResponseCode.HetThoiGianThucHienGD);
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Account hủy giao dịch");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanHuyGiaoDich);
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        #region [Function Set treo tiền momo]
        private async Task<MomoHangingMoneyDTO> MomoPathSetPendingMoney(TNConfirmMonoCommand model, int accountId, string functionName, string requestId, EnumMongoLogType type)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, new Uri(_monoSettings.PathSetPendingMoney))
                {
                    Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
                };
                using var http = new HttpClient();
                var response = await http.SendAsync(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MomoHangingMoneyDTO>(content);
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                AddLog(accountId, functionName, requestId, type, ex.ToString(), true);
                return null;
            }
        }
        #endregion
        #endregion

        #region [Momo 3. nhận phản hồi từ momo khi đã gửi lệnh xác nhận treo tiền]
        public async Task<object> MomoCallbackNotify(HttpRequest request, MomoNotifyDataDTO model)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.MomoCallbackNotify";
            int accoutnId = 0;
            if(model != null)
            {
                _ = int.TryParse(model.PartnerTransId, out accoutnId);
            }    

            AddLog(accoutnId, functionName, requestId, EnumMongoLogType.Start, $"Momo gửi phản hồi về xác nhận chuyển tiền từ TK treo");

            try
            {
                AddLog(accoutnId, functionName, requestId, EnumMongoLogType.Center, $"Nhận về {JsonConvert.SerializeObject(model)}");

                if (model == null)
                {
                    AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Momo gửi phản hồi về API TNGO null");
                    return new MomoNotifyConfirmCommand(new MomoNotifyDataDTO { Status = 2400, Message = "Dữ liệu truyền vào bị sai định dạng" }, _monoSettings.SecretKey);
                }   
                // veryfy Signature để xác định thông tin có đúng ko
                if(!VerifiedData(model))
                {
                    AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Verify data => false");
                    return new MomoNotifyConfirmCommand(new MomoNotifyDataDTO { Status = 153, Message = "Giải mã hash thất bại" }, _monoSettings.SecretKey);
                }

                var history = await _iWalletTransactionRepository.SearchOneAsync(true, x => (x.Status == EWalletTransactionStatus.Pending || x.Status == EWalletTransactionStatus.Draft) && x.TransactionCode == model.MomoTransId && x.OrderIdRef == model.PartnerRefId && x.PaymentGroup == EPaymentGroup.MomoPay && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));

                if (model.Status != 0)
                {
                    AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Xử lý treo tiền KH thất bại");
                    if(history!=null)
                    {
                        await WalletTransactionSetPad(functionName, requestId, model.MomoTransId, history.Id, EWalletTransactionStatus.Cancel, " [MOMO phản hồi treo tiền tài khoản khách hàng thất bại]", requestId);
                    }
                    // Gửi notify
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accoutnId, ENotificationTempType.NapTienKhongThanhCong, true, DockEventType.Not, null);
                    return new MomoNotifyConfirmCommand(model, _monoSettings.SecretKey);
                }
                else
                {
                    AddLog(accoutnId, functionName, requestId, EnumMongoLogType.Center, $"Server MOMO phản hồi treo tiền khách hàng thành công");
                   
                    if (history == null)
                    {
                        // Log lại những sự kiện ko tìm thấy giao dịch log
                        AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy lịch sử giao dịch momo tương ứng Status: {EWalletTransactionStatus.Pending}, TransactionCode: {model.MomoTransId}, OrderIdRef: {model.PartnerRefId}");
                        await WalletTransactionWarningHistory(functionName, requestId, accoutnId, EPaymentGroup.MomoPay, model.PartnerRefId, JsonConvert.SerializeObject(model));
                        model.Status = 0;
                        model.Message = "Thành công";
                        return new MomoNotifyConfirmCommand(model, _monoSettings.SecretKey);
                    }
                    else
                    {
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Tìm thấy lịch sử giao dịch {history.OrderIdRef}");
                        var sendData = new MomoPadCommand(_monoSettings.MerchantCode, history.OrderIdRef, "capture", requestId, history.TransactionCode, _monoSettings.SecretKey);
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Data gửi cho momo {JsonConvert.SerializeObject(sendData)}");
                        // Gọi api xác nhận giao dịch chuyển tiền qua TNGO
                        var outSetPad = await MomoPad(functionName, requestId, sendData);
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Server gửi lệnh xác nhận chuyển tiền cho TNGO data nhận về : { (outSetPad == null ? "" : JsonConvert.SerializeObject(outSetPad))}");
                        if (outSetPad != null && outSetPad.Status == 0)
                        {
                            await WalletTransactionSetPad(functionName, requestId, model.MomoTransId, history.Id, EWalletTransactionStatus.Done, " ", requestId);
                          
                            AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Cộng tiền cho tài khoản khách hàng + {history.TotalAmount} điểm cho tài khoản");
                            // Gửi notify
                            await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accoutnId, ENotificationTempType.NapTienThanhCong, true, DockEventType.MomoRespond, JsonConvert.SerializeObject(ApiResponse.WithStatus(true, null, ResponseCode.NapTienThanhCong)), $"{Function.FormatMoney(history.TotalAmount)} điểm", EPaymentGroup.MomoPay.GetDisplayName());
                            // Gửi cho admin phần quản lý
                            await _iHubService.EventSend(EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, history.AccountId, null, 0, 0, 0, 0,
                               $"[MOMOPAY] Khách hàng vừa nạp thành công {Function.FormatMoney(history.Amount)} đ vào tài khoản");
                            await SendBil(functionName, requestId, history.AccountId, history);
                        }
                        else
                        {
                            AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Gửi xác nhận chuyển tiền thất bại, đơn hàng vẫn ở trạng thái pending");
                            // Gửi notify
                            await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accoutnId, ENotificationTempType.NapTienKhongThanhCong, true, DockEventType.MomoRespond, JsonConvert.SerializeObject(ApiResponse.WithStatus(false, null, ResponseCode.NapTienThatBai)));
                        }
                        return new MomoNotifyConfirmCommand(model, _monoSettings.SecretKey);
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private bool VerifiedData(MomoNotifyDataDTO model)
        {
            string templateData = $"accessKey={model.AccessKey}&amount={Convert.ToInt64(model.Amount)}&message={model.Message}&momoTransId={model.MomoTransId}&partnerCode={model.PartnerCode}&partnerRefId={model.PartnerRefId}&partnerTransId={model.PartnerTransId}&responseTime={model.ResponseTime}&status={model.Status}&storeId={model.StoreId}&transType=momo_wallet";
            var maHoa = Function.SignSHA256(templateData, _monoSettings.SecretKey);
            if (maHoa == model.Signature)
            {
                return true;
            }
            return false;
        }

        private async Task<MomoPadDTO> MomoPad(string functionName, string requestId, MomoPadCommand model)
        {
         
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, new Uri(_monoSettings.PathNotifyConfirm))
                {
                    Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
                };
                using var http = new HttpClient();
                var response = await http.SendAsync(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MomoPadDTO>(content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi : {ex}", true);
                return null;
            }
        }

        private async Task WalletTransactionWarningHistory(string functionName, string requestId, int accountId, EPaymentGroup group, string orderId, string backup)
        {
            try
            {
                var history = await _iWalletTransactionRepository.SearchOneAsync(false, x => x.AccountId == accountId && x.PaymentGroup == group  && x.OrderIdRef == orderId && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));
                if (history != null)
                {
                    history.PadTime = DateTime.Now;
                    history.IsWarningHistory = true;
                    history.WarningHistoryData = backup;
                    _iWalletTransactionRepository.Update(history);
                    await _iWalletTransactionRepository.CommitAsync();
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Lỗi : {ex}", true);
            }
        }

        private async Task WalletTransactionSetPending(string functionName, string requestId, int accountId, EPaymentGroup group, string orderId, string transid = null, decimal? amount =null)
        {
            try
            {
                var history = await _iWalletTransactionRepository.SearchOneAsync(false, x => x.AccountId == accountId && x.Status == EWalletTransactionStatus.Draft && x.PaymentGroup == group  && x.OrderIdRef == orderId && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));
                if (history != null)
                {
                    history.Status = EWalletTransactionStatus.Pending;

                    if(transid!= null)
                    {
                        history.TransactionCode = transid;
                    }    
                    if(amount!=null)
                    {
                        history.TotalAmount = amount.Value;
                    }    
                    history.SetPenddingTime = DateTime.Now;
                    _iWalletTransactionRepository.Update(history);
                    await _iWalletTransactionRepository.CommitAsync();
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Lỗi : {ex}", true);
            }
        }

        private async Task WalletTransactionSetPad(string functionName, string requestId, string transactionCode, int id, EWalletTransactionStatus status, string note, string payRequestId)
        {
            try
            {
                var history = await _iWalletTransactionRepository.SearchOneAsync(false, x => x.Id == id && (x.Status == EWalletTransactionStatus.Draft || x.Status == EWalletTransactionStatus.Pending));
                if (history != null)
                {
                    history.Status = status;
                    history.PadTime = DateTime.Now;
                    history.Note += note;
                    history.ReqestId = payRequestId;
                    history.IsAsync = true;

                    if (transactionCode != null)
                    {
                        history.TransactionCode = transactionCode;
                    }

                    if(status == EWalletTransactionStatus.Done)
                    {
                        var getVi = await _iWalletRepository.GetWalletAsync(history.AccountId);

                        var depsitEvent = await _iWalletTransactionRepository.EventByAccount(history.AccountId, history.Amount, history.LocationProjectId ?? 0, history.LocationCustomerGroupId ?? 0);
                        if (depsitEvent != null)
                        {
                            history.DepositEventId = depsitEvent.Id;
                            history.CampaignId = depsitEvent.CampaignId;
                            history.DepositEvent_SubAmount = depsitEvent.ToSubPrice;
                            history.DepositEvent_Amount = depsitEvent.ToPrice;
                            history.ReqestId = depsitEvent.TransactionCode;
                            history.Amount += depsitEvent.ToPrice;
                            history.SubAmount += depsitEvent.ToSubPrice;
                            history.PadTime = DateTime.Now;
                            history.Note += $" (Trong sự kiện '{ depsitEvent.Campaign?.Name}' giao dịch được +{Function.FormatMoney(depsitEvent.ToPrice)} điểm, +{Function.FormatMoney(depsitEvent.ToSubPrice)} điểm khuyến mãi, +{(depsitEvent?.SubPointExpiry ?? 0)} ngày sử dụng điểm KM)";
                            AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $" (Trong sự kiện '{ depsitEvent.Campaign?.Name}' giao dịch được +{Function.FormatMoney(depsitEvent.ToPrice)} điểm, +{Function.FormatMoney(depsitEvent.ToSubPrice)} điểm khuyến mãi, +{(depsitEvent?.SubPointExpiry ?? 0)} ngày sử dụng điểm KM)");
                            await _iWalletTransactionRepository.UpdateUsed(depsitEvent.Id);
                        }
                        history.TotalAmount = history.Amount + history.SubAmount;
                        history.HashCode = Function.TransactionHash(history.OrderIdRef, history.Amount, history.SubAmount, history.TripPoint, _baseSetting.TransactionSecretKey);
                        history.Wallet_Balance = getVi.Balance;
                        history.Wallet_SubBalance = getVi.SubBalance;
                        history.Wallet_HashCode = getVi.HashCode;

                        _iWalletTransactionRepository.Update(history);
                        await _iWalletTransactionRepository.CommitAsync();
                        // cập nhật số tiền trong ví
                        await _iWalletRepository.UpdateAddWalletAsync(history.AccountId, history.Amount, history.SubAmount, 0, _baseSetting.TransactionSecretKey, true, true, depsitEvent?.SubPointExpiry ?? 0);
                    }
                    else
                    {
                        _iWalletTransactionRepository.Update(history);
                        await _iWalletTransactionRepository.CommitAsync();
                    }    
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi : {ex}", true);
            }
        }
        #endregion

        #region momo 4 Xem trạng thái giao dịch & rolback
        public async Task<ApiResponse<object>> MomoCheckStatus(int  id)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.MomoCheckStatus";

            try
            {
                var history = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.Id == id && x.PaymentGroup == EPaymentGroup.MomoPay);
                if (history == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                if(history.Status != EWalletTransactionStatus.Pending)
                {
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"{history.TransactionCode} Momo không phải trạng thái pending");
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }
                // logic check > 60s là ko thành công
                if((history.SetPenddingTime == null && (DateTime.Now - history.CreatedDate).TotalSeconds > 90) || ((DateTime.Now - history.SetPenddingTime.Value).TotalSeconds > 90))
                {
                    history.IsAsync = true;
                    history.Status = EWalletTransactionStatus.Cancel;
                    history.PadTime = DateTime.Now;
                    history.Note += $"; Đơn pedding vượt quá 90s hệ thống tự động hủy đơn này";
                    _iWalletTransactionRepository.Update(history);
                    await _iWalletTransactionRepository.CommitAsync();

                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Vượt quá 60s pedding set = 0 thành công");
                    return ApiResponse.WithStatus(false, null, ResponseCode.HetThoiGianThucHienGD);
                }

                var stringJson = JsonConvert.SerializeObject(new MomoQueryStatusHasCommand
                {
                    RequestId = requestId,
                    MomoTransId = history.TransactionCode,
                    PartnerCode = _monoSettings.MerchantCode,
                    PartnerRefId = history.OrderIdRef
                });

                var jsonStringEncryption = Function.RSAEncryption(stringJson, _monoSettings.PublicKey);
                if (jsonStringEncryption == null)
                {
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Mã hóa thất bại JsonStringEncryption Null");
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                var objSend = new MomoQueryStatusCommand
                {
                    MomoTransId = history.TransactionCode,
                    PartnerCode = _monoSettings.MerchantCode,
                    PartnerRefId = history.OrderIdRef,
                    Version = 2.0,
                    Hash = jsonStringEncryption
                };

                AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Start, $"Gửi lệnh request trạng thái {JsonConvert.SerializeObject(objSend)}", false);

                var data = await MomoQueryStatus(objSend, history.AccountId, functionName, requestId, EnumMongoLogType.Center);

                AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Start, $"Trả về {Newtonsoft.Json.JsonConvert.SerializeObject(data)}", false);

                if(data.Status ==0 && data.Data.Status == 0)
                {
                    await WalletTransactionSetPad(functionName, requestId, history.TransactionCode, history.Id, EWalletTransactionStatus.Done, " ", requestId);
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Recheck Cộng tiền cho tài khoản khách hàng + {history.TotalAmount} điểm cho tài khoản");
                    // Gửi notify
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, history.AccountId, ENotificationTempType.NapTienThanhCong, true, DockEventType.MomoRespond, JsonConvert.SerializeObject(ApiResponse.WithStatus(true, null, ResponseCode.NapTienThanhCong)), $"{Function.FormatMoney(history.TotalAmount)} điểm", EPaymentGroup.MomoPay.GetDisplayName());
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, history.AccountId, null, 0, 0, 0, 0,
                       $"[MOMO] Khách hàng vừa nạp thành công {Function.FormatMoney(history.Amount)} đ vào tài khoản");
                    await SendBil(functionName, requestId, history.AccountId, history);
                }    
                //Logic + tiền
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi : {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> MomoRefund(int id)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.MomoRefund";

            try
            {
                var kt = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.Id == id && x.PaymentGroup == EPaymentGroup.MomoPay);
                if (kt == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                var stringJson = JsonConvert.SerializeObject(new MomoRefundHashCommand
                {
                    MomoTransId = kt.TransactionCode,
                    PartnerCode = _monoSettings.MerchantCode,
                    PartnerRefId = kt.OrderIdRef,
                    Amount = kt.Amount,
                    Description = "Hoàn tiền cho khách"
                });

                var jsonStringEncryption = Function.RSAEncryption(stringJson, _monoSettings.PublicKey);
                if (jsonStringEncryption == null)
                {
                    AddLog(kt.AccountId, functionName, requestId, EnumMongoLogType.End, $"Mã hóa thất bại JsonStringEncryption Null");
                    return ApiResponse.WithStatus(false, null, ResponseCode.HetThoiGianThucHienGD);
                }

                var objSend = new MomoRefundCommand
                {
                    RequestId = requestId,
                    PartnerCode = _monoSettings.MerchantCode,
                    Version = 2.0,
                    Hash = jsonStringEncryption
                };

                AddLog(kt.AccountId, functionName, requestId, EnumMongoLogType.Start, $"Gửi lệnh request trạng thái {JsonConvert.SerializeObject(objSend)}", false);

                var data = await MomoSendRefund(objSend, kt.AccountId, functionName, requestId, EnumMongoLogType.Center);

                AddLog(kt.AccountId, functionName, requestId, EnumMongoLogType.Start, $"Trả về {data}", false);

                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi : {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
            }
        }

        private async Task<MomoHangingStatusDTO> MomoQueryStatus(MomoQueryStatusCommand model, int accountId, string functionName, string requestId, EnumMongoLogType type)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, new Uri(_monoSettings.PathQueryStatus))
                {
                    Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
                };
                using var http = new HttpClient();
                var response = await http.SendAsync(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var stringdata =  await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MomoHangingStatusDTO>(stringdata);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, type, ex.ToString(), true);
                return new MomoHangingStatusDTO { Status = -1, Message = "Giải mã json lỗi" };
            }
        }

        private async Task<string> MomoSendRefund(MomoRefundCommand model, int accountId, string functionName, string requestId, EnumMongoLogType type)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, new Uri(_monoSettings.PathRefund))
                {
                    Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
                };
                using var http = new HttpClient();
                var response = await http.SendAsync(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return await response.Content.ReadAsStringAsync();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, type, ex.ToString(), true);
                return null;
            }
        }
        #endregion
        #endregion

        #region [ZALO]

        #region Zalo 2.Tạo một đơn & Xác nhận pending giao dịch (quy ước với app)
        public async Task<ApiResponse<object>> UserZaloConfirm(int accountId, UserZaloConfirmCommand model)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.UserZaloConfirm";
            try
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} data: {JsonConvert.SerializeObject(model)}");

                if (model.ReturnCode == 1)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"User xác nhận thanh toán thành công status = {model.ReturnCode} để set pending");
                    var ktHistory = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.OrderIdRef == model.AppTransId && x.PaymentGroup == EPaymentGroup.ZaloPay && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));
                    if (ktHistory == null)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy lịch sử mẫu giao dịch {model.AppTransId}");
                        return ApiResponse.WithStatus(false, null, ResponseCode.HetThoiGianThucHienGD);
                    }
                    else if (ktHistory.Status == EWalletTransactionStatus.Done)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Giao dịch đã thành công rồi, không cần set pending");
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                    else if (ktHistory.Status == EWalletTransactionStatus.Pending)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Giao dịch đã pending rồi, không cần set pending");
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                    else
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Set pending");
                        await WalletTransactionSetPending(functionName, requestId, accountId, EPaymentGroup.ZaloPay, model.AppTransId);
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                }
                else if (model.ReturnCode == 2)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Hủy giao dịch");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanHuyGiaoDich);
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Nạp tiền zalo thất bại");
                    return ApiResponse.WithStatus(false, null, ResponseCode.NapTienThatBai);
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private async Task<DepositZaloReturnDTO> CreateOrder(int accountId, string functionName, string requestId, Dictionary<string, string> param)
        {
            try
            {
                param.Add("mac", HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, _zaloSettings.SecretKeyCreateOrder, $"{_zaloSettings.AppId}|{param["app_trans_id"]}|{param["app_user"]}|{param["amount"]}|{param["app_time"]}|{param["embed_data"]}|{param["item"]}"));
                Dictionary<string, object> result = await HttpHelper.PostFormAsync(_zaloSettings.CreateOrderUrl, param);
                AddLog(accountId, "PaymentService.DepositZalo", requestId, EnumMongoLogType.Center, $"Tạo đơn hàng Zalo server gửi về {JsonConvert.SerializeObject(result)}");

                return new DepositZaloReturnDTO
                {
                    ReturnCode = !result.ContainsKey("return_code") ? 0 : Convert.ToInt32(result["return_code"]?.ToString()),
                    ReturnMessage = !result.ContainsKey("return_message") ? null : result["return_message"]?.ToString(),
                    SubReturnCode = !result.ContainsKey("sub_return_code") ? 0 : Convert.ToInt32(result["sub_return_code"]?.ToString()),
                    SubReturnMessage = !result.ContainsKey("sub_return_message") ? null : result["sub_return_message"]?.ToString(),
                    OrderUrl = !result.ContainsKey("order_url") ? null : result["order_url"].ToString(),
                    OrderToken = !result.ContainsKey("zp_trans_token") ? null : result["zp_trans_token"]?.ToString(),
                };
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi : {ex}", true);
                return null;
            }
        }

        private async Task<ZaloReCheckOderDTO> GetOrderStatus(int accountId, string functionName, string requestId, string orderId)
        {
            try
            {
                var hmacinput = $"{_zaloSettings.AppId}|{orderId}|{_zaloSettings.SecretKeyCreateOrder}";
                var param = new Dictionary<string, string>
                {
                    { "app_id", _zaloSettings.AppId.ToString() },
                    { "app_trans_id", orderId },
                    { "mac", HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, _zaloSettings.SecretKeyCreateOrder, hmacinput) }
                };

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi Recheck đơn hàng pending ZALO {JsonConvert.SerializeObject(param)}");

                var result = await HttpHelper.PostFormAsync(_zaloSettings.ZaloRecheckOrderPath, param);
                return new ZaloReCheckOderDTO
                {
                    ReturnCode = !result.ContainsKey("return_code") ? 0 : Convert.ToInt32(result["return_code"]?.ToString()),
                    ReturnMessage = !result.ContainsKey("return_message") ? null : result["return_message"]?.ToString(),
                    SubReturnCode = !result.ContainsKey("sub_return_code") ? 0 : Convert.ToInt32(result["sub_return_code"]?.ToString()),
                    SubReturnMessage = !result.ContainsKey("sub_return_message") ? null : result["sub_return_message"]?.ToString(),
                    IsProcessing = result.ContainsKey("is_processing") && Convert.ToBoolean(result["is_processing"]),
                    Amount = !result.ContainsKey("amount") ? 0 : Convert.ToDecimal(result["amount"]),
                    ZpTransId = !result.ContainsKey("zp_trans_id") ? 0 : Convert.ToInt64(result["zp_trans_id"])
                };
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi : {ex}", true);
                return null;
            }
        }
        #endregion

        #region Zalo 3. Nhận phản hồi từ backend
        public async Task<object> ZaloCallbackNotify(dynamic cbdata)
        {
            // Dừng để đợi app config trước
            //Thread.Sleep(2000);
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = $"PaymentService.ZaloCallbackNotify";
            int accoutnId = 0;
            try
            {
                var data = new ZaloCallbackNotifyDataDTO();
                var model = new ZaloCallbackNotifyDTO()
                {
                    Type = cbdata.ContainsKey("type") ? Convert.ToString(cbdata["type"]) : null,
                    Data = cbdata.ContainsKey("data") ? Convert.ToString(cbdata["data"]) : null,
                    Mac = cbdata.ContainsKey("mac") ? Convert.ToString(cbdata["mac"]) : null
                };

                if (model.Data != null)
                {
                    data = JsonConvert.DeserializeObject<ZaloCallbackNotifyDataDTO>(model.Data);
                    _ = int.TryParse(data.AppUser, out accoutnId);
                }

                AddLog(accoutnId, functionName, requestId, EnumMongoLogType.Start, $"Zalo gửi phản hồi về giao dịch thành công { JsonConvert.SerializeObject(cbdata)}");

                if (model == null)
                {
                    AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Zalo gửi phản hồi về API TNGO null");
                    return new ZaloCallbackNotifyConfirmDTO { ReturnCode = 3, ReturnMessage = "Gửi Null" };
                }
                // veryfy Signature để xác định thông tin có đúng ko
                var mac = HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, _zaloSettings.SecretKeyCallback, model.Data);
                if (model.Mac != mac)
                {
                    AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Verify data => false");
                    return new ZaloCallbackNotifyConfirmDTO { ReturnCode = 4, ReturnMessage = "Giải mã thất bại" };
                }

                var history = await _iWalletTransactionRepository.SearchOneAsync(true, x => (x.Status == EWalletTransactionStatus.Pending || x.Status == EWalletTransactionStatus.Draft) && x.OrderIdRef == data.AppTransId && x.AccountId == accoutnId && x.CreatedDate >= DateTime.Now.AddDays(-10) && x.CreatedDate < DateTime.Now.AddHours(1));
                if (history == null)
                {
                    // Log lại những sự kiện ko tìm thấy giao dịch log
                    AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy lịch sử giao dịch ZALO tương ứng Status: {EWalletTransactionStatus.Draft}, OrderIdRef: {data.AppTransId}");
                    await WalletTransactionWarningHistory(functionName, requestId, accoutnId, EPaymentGroup.ZaloPay, data.AppTransId, JsonConvert.SerializeObject(model));
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accoutnId, ENotificationTempType.NapTienKhongThanhCong, true, DockEventType.ZaloRespond, JsonConvert.SerializeObject(ApiResponse.WithStatus(false, null, ResponseCode.NapTienThatBai)));
                    return new ZaloCallbackNotifyConfirmDTO { ReturnCode = 5, ReturnMessage = "Không tìm thấy đơn hàng tương ứng" };
                }
                else
                {
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Tìm thấy lịch sử giao dịch ZALO: {history.OrderIdRef}, zalo ID: {data.ZpTransId}");
                    await WalletTransactionSetPad(functionName, requestId, data.ZpTransId.ToString(), history.Id, EWalletTransactionStatus.Done, "", requestId);
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Cộng tiền cho tài khoản khách hàng + {history.TotalAmount} điểm cho tài khoản");
                    // Gửi notify
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accoutnId, ENotificationTempType.NapTienThanhCong, true, DockEventType.ZaloRespond, JsonConvert.SerializeObject(ApiResponse.WithStatus(true, null, ResponseCode.NapTienThanhCong)), $"{Function.FormatMoney(history.TotalAmount)} điểm", EPaymentGroup.ZaloPay.GetDisplayName());
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, history.AccountId, null, 0, 0, 0, 0, $"[ZALOPAY] Khách hàng vừa nạp thành công {Function.FormatMoney(history.Amount)} đ vào tài khoản");
                    await SendBil(functionName, requestId, history.AccountId, history);
                    return new ZaloCallbackNotifyConfirmDTO { ReturnCode = 1, ReturnMessage = "Thành công" }; 
                }
            }
            catch (Exception ex)
            {
                AddLog(accoutnId, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region Zalo 3.1 Tự check giao dịch pending Zalo
        public async Task<ApiResponse<object>> RecheckCallBackZalo(int id)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.RecheckCallBackZalo";
            try
            {
                var history = await _iWalletTransactionRepository.SearchOneAsync(true, x => x.Id == id && (x.Status == EWalletTransactionStatus.Pending || x.Status == EWalletTransactionStatus.Draft) && x.PaymentGroup == EPaymentGroup.ZaloPay);
                if (history != null)
                {
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Start, $"Tìm thấy lịch sử giao dịch ZALO: {history.OrderIdRef}");
                    var outRecheck = await GetOrderStatus(history.AccountId, functionName, requestId, history.OrderIdRef);
                    AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.Center, $"ZALO phản hồi {JsonConvert.SerializeObject(outRecheck)}");

                    history = await _iWalletTransactionRepository.SearchOneAsync(false, x => x.Id == id && (x.Status == EWalletTransactionStatus.Pending || x.Status == EWalletTransactionStatus.Draft) && x.PaymentGroup == EPaymentGroup.ZaloPay);
                    if(history == null)
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy đơn hàng #{id}");
                        return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                    }   
                    
                    if (outRecheck == null)
                    {
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Gói tin recheck NULL");
                        return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                    }    
                    else if(outRecheck.ReturnCode == 1)
                    {
                        await WalletTransactionSetPad(functionName, requestId, outRecheck.ZpTransId.ToString(), history.Id, EWalletTransactionStatus.Done, "", requestId);
                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Cộng tiền cho tài khoản khách hàng + {history.TotalAmount} điểm cho tài khoản");
                        await _iFireBaseService.SendNotificationByAccount(functionName, requestId, history.AccountId, ENotificationTempType.NapTienThanhCong, true, DockEventType.Not, null, $"{Function.FormatMoney(history.TotalAmount)} điểm", EPaymentGroup.ZaloPay.GetDisplayName());
                        // Gửi cho admin phần quản lý
                        await _iHubService.EventSend(EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, history.AccountId, null, 0, 0, 0, 0,
                           $"[ZALOPAY] Khách hàng vừa nạp thành công {Function.FormatMoney(history.Amount)} đ vào tài khoản");
                        await SendBil(functionName, requestId, history.AccountId, history);
                        return ApiResponse.WithData(outRecheck);
                    }
                    else if(outRecheck.ReturnCode == 2)
                    {
                        // Set trạng thái hủy cho đơn hàng
                        history.IsAsync = true;
                        history.Status = EWalletTransactionStatus.Cancel;
                        history.PadTime = DateTime.Now;
                        history.Note += $";Recheck đơn hàng ZALOPAY, trả về thất bại '{outRecheck.ReturnMessage}'";
                        _iWalletTransactionRepository.Update(history);
                        await _iWalletTransactionRepository.CommitAsync();

                        AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Recheck đơn hàng ZALOPAY, trả về thất bại '{outRecheck.ReturnMessage}'");
                        return ApiResponse.WithData(outRecheck);

                    }
                    else if (outRecheck.ReturnCode == 3)
                    {
                        if (outRecheck.ReturnMessage.ToLower() == "Giao dịch chưa được thực hiện".ToLower())
                        {
                            AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Recheck '{outRecheck.ReturnMessage}'");
                            return ApiResponse.WithData(outRecheck);
                        }
                        else
                        {
                            history.IsAsync = true;
                            history.PadTime = DateTime.Now;
                            if (!history.IsAsync)
                            {
                                history.Note += $";Recheck đơn hàng ZALOPAY, trả về Đơn hàng chưa thanh toán hoặc giao dịch đang xử lý '{outRecheck.ReturnMessage}'";
                            }
                            _iWalletTransactionRepository.Update(history);
                            await _iWalletTransactionRepository.CommitAsync();

                            AddLog(history.AccountId, functionName, requestId, EnumMongoLogType.End, $"Recheck đơn hàng ZALOPAY, trả về Đơn hàng chưa thanh toán hoặc giao dịch đang xử lý '{outRecheck.ReturnMessage}'");
                            return ApiResponse.WithData(outRecheck);
                        }
                    }
                    else
                    {
                        return ApiResponse.WithStatus(false, outRecheck, ResponseCode.Ok);
                    }
                }
                else
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy đơn hàng #{id}");
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #endregion

        #region [Lấy thông tin ví]
        public async Task<ApiResponse<object>> WalletGets(int accountid)
        {
            try
            {
                var data = await _iWalletRepository.GetWalletAsync(accountid);
                if (data != null)
                {
                    data.HashCode = "";
                }
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                AddLog(accountid, "PaymentService.WalletGets", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [Lấy danh sách giao dịch]
        public async Task<ApiResponse<object>> WalletTransactionGets(int accountid, int page, int limit)
        {
            try
            {
                var newList = new List<WalletTransactionDTO>();
                var data = await _iWalletTransactionRepository.SearchPagedList(page, limit, x => x.AccountId == accountid && x.Status != EWalletTransactionStatus.Draft && x.CreatedDate >= DateTime.Now.AddMonths(-30), x => x.OrderByDescending(m => m.CreatedDate));
                foreach(var item in data.Data)
                {
                    newList.Add(new WalletTransactionDTO(item));
               }
                return ApiResponse.WithData(newList, data.TotalRows, limit);
            }
            catch (Exception ex)
            {
                AddLog(accountid, "PaymentService.WalletTransactionGets", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [Nhập code khuyến mãi]
        public async Task<ApiResponse<object>> WalletImportCode(int accountId, string code, string lat, string lng)
        {
            string functionName = "PaymentService.WalletImportCode";
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            try
            {
                code = code.Trim();
                var getCode = await _iVoucherCodeRepository.SearchOneAsync(true, x => x.Code == code);
                if (getCode == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);
                }

                if(getCode.ProvisoType == ECampaignProvisoType.VerifyOke)
                {
                    var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                    if(account == null)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);
                    }  

                    if(account.VerifyStatus != EAccountVerifyStatus.Ok)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                    }
                }
                else if (getCode.ProvisoType == ECampaignProvisoType.ForAccount && accountId != getCode.BeneficiaryAccountId)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                }

                // Kiểm tra còn không
                if (getCode.CurentLimit <= 0)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.MaKhuyenMaiHetSoLan);
                }
                // Kiểm tra chiến dịch tồn tại không
                var camp = await _iCampaignRepository.SearchOneAsync(true, x => x.Id == getCode.CampaignId && x.Status == ECampaignStatus.Active);
                if (camp == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);

                }
                // Kiểm tra mã còn hiệu lực không
                if (!(getCode.StartDate <= DateTime.Now && getCode.EndDate.AddDays(1) > DateTime.Now))
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.MaKhuyenMaiKhongConHieuLuc);
                }
                // Kiểm tra khách hàng này đã add code cùng chiến dịch
                var isCampaign = await _iWalletTransactionRepository.AnyAsync(x => x.CampaignId == getCode.CampaignId && x.AccountId == accountId && x.CreatedDate >= DateTime.Now.AddDays(-90));
                if (isCampaign)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.MaKhuyenMaiBanDaThamGiaRoi);
                }

                // Kiểm tra xem có áp dụng với nhóm khách hàng nào không
                // Logic mới nếu ko thuộc nhóm khách hàng thì sẽ check thêm tọa độ người đó có thuộc địa lý của dự án không
                var findProject = new ProjectAccountModel();
                bool isOke = true;
                if(getCode.ProjectId > 0)
                {
                    if (getCode.CustomerGroupId > 0)
                    {
                        isOke = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId && x.CustomerGroupId == getCode.CustomerGroupId);
                    }
                    else
                    {
                        isOke = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId);
                    }
                    // nếu ko đạt thì check 1 lần nữa theo tọa độ người đứng
                    if (!isOke)
                    {
                        findProject = await _iMemoryCacheService.LatLngToCustomerGroup(lat, lng, accountId);
                        if (getCode.CustomerGroupId > 0 && getCode.CustomerGroupId != findProject.CustomerGroupId)
                        {
                            return ApiResponse.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                        }
                        else if (getCode.CustomerGroupId <= 0 && getCode.ProjectId != findProject.ProjectId)
                        {
                            return ApiResponse.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                        }
                    }
                }
                getCode = await _iVoucherCodeRepository.SearchOneAsync(false, x => x.Id == getCode.Id);
                getCode.CurentLimit -= 1;
                _iVoucherCodeRepository.Update(getCode);
                await _iVoucherCodeRepository.CommitAsync();
                // lấy ví
                var getW = await _iWalletRepository.GetWalletAsync(accountId);
                // Thêm transaction
                // Add log

                string noteText = $"Nạp mã giảm giá {code} của chiến dịch khuyến mãi '{camp.Name}', tài khoản được +{Function.FormatMoney(getCode.Point)} điểm vào tài khoản khuyến mãi";
                // Tạo giao dịch log
                var wt = new WalletTransaction
                {
                    AccountId = accountId,
                    CampaignId = getCode.CampaignId,
                    IsAsync = true,
                    Note = noteText,
                    OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.Voucher),
                    CreatedDate = DateTime.Now,
                    CreatedUser = 0,
                    TransactionId = 0,
                    TransactionCode = code,
                    WalletId = getW.Id,
                    Status = EWalletTransactionStatus.Done,
                    SubAmount = getCode.Point ?? 0,
                    Amount = 0,
                    TotalAmount = getCode.Point ?? 0,
                    Type = EWalletTransactionType.Voucher,
                    Wallet_Balance = getW.Balance,
                    Wallet_SubBalance = getW.SubBalance,
                    Wallet_HashCode = getW.HashCode,
                    VoucherCodeId = getCode.Id,
                    Wallet_TripPoint = getW.TripPoint,
                    PadTime = DateTime.Now,
                    LocationProjectId = findProject.ProjectId,
                    LocationProjectOverwrite = findProject.LocationProjectOverwrite
                };
                wt.HashCode = Function.TransactionHash(wt.OrderIdRef, wt.Amount, wt.SubAmount, wt.TripPoint, _baseSetting.TransactionSecretKey);
                await _iWalletTransactionRepository.AddAsync(wt);
                await _iWalletTransactionRepository.CommitAsync();
                // Trừ tiền trong ví
                await _iWalletRepository.UpdateAddWalletAsync(accountId, 0, getCode.Point ?? 0, 0, _baseSetting.TransactionSecretKey, true, true, 0);

                await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.NapMaKhuyenMaiThanhCong, true, DockEventType.Not, null, $"{Function.FormatMoney(getCode.Point)}");

                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [Nạp gift nhanh]
        public async Task<ApiResponse<object>> GiftCardGetData(int accountId, string lat, string lng)
        {
            try
            {
                var redeemPoints = (await _iRedeemPointRepository.SearchAsync()).Select(x => new RedeemPointDTO(x));

                var findProject = await _iMemoryCacheService.LatLngToCustomerGroup(lat, lng, accountId);

                var voucherCodes = (await _iVoucherCodeRepository.VoucherCodePublicByAccount(accountId, findProject?.ProjectId ?? 0, findProject?.CustomerGroupId ?? 0)).Select(x => new VoucherCodeDTO(x));

                var w = await _iWalletRepository.GetWalletAsync(accountId);
                return ApiResponse.WithData(new { redeemPoints, voucherCodes, totalTripPoint = w?.TripPoint });
            }
            catch (Exception ex)
            {
                AddLog(accountId, "PaymentService.GiftCardGetData", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [Đổi điểm tích lũy]
        public async Task<ApiResponse<object>> RedeemPointAdd(int accountId, int id)
        {
            string functionName = "PaymentService.RedeemPointAdd";
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            try
            {
                var getReadeem = await _iRedeemPointRepository.SearchOneAsync(true, x => x.Id == id);
                if (getReadeem == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                var getVi = await _iWalletRepository.GetWalletAsync(accountId);
                if (getVi.TripPoint < getReadeem.TripPoint)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.DiemChuyenDiKhongDu);
                }

                string noteText = $"Quy đổi {Function.FormatMoney(getReadeem.TripPoint)} điểm tích chuyển đi qua {Function.FormatMoney(getReadeem.ToPoint)} điểm vào tài khoản khuyến mãi";
                // Ghi lịch sử giao dịch
                var wt = new WalletTransaction
                {
                    AccountId = accountId,
                    CampaignId = 0,
                    IsAsync = true,
                    Note = noteText,
                    OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.RedeemPoint),
                    CreatedDate = DateTime.Now,
                    CreatedUser = accountId,
                    TransactionId = 0,
                    TransactionCode = null,
                    WalletId = getVi.Id,
                    Status = EWalletTransactionStatus.Done,
                    SubAmount = getReadeem.ToPoint,
                    Amount = 0,
                    TotalAmount = getReadeem.ToPoint,
                    Type = EWalletTransactionType.RedeemPoint,
                    Wallet_Balance = getVi.Balance,
                    Wallet_SubBalance = getVi.SubBalance,
                    Wallet_HashCode = getVi.HashCode,
                    TripPoint = getReadeem.TripPoint,
                    Wallet_TripPoint = getVi.TripPoint,
                    PadTime = DateTime.Now
                };
                wt.HashCode = Function.TransactionHash(wt.OrderIdRef, wt.Amount, wt.SubAmount, wt.TripPoint, _baseSetting.TransactionSecretKey);
                await _iWalletTransactionRepository.AddAsync(wt);
                await _iWalletTransactionRepository.CommitAsync();
                // Cộng điểm tặng
                await _iWalletRepository.UpdateAddWalletAsync(accountId, 0, getReadeem.ToPoint, -getReadeem.TripPoint, _baseSetting.TransactionSecretKey, true, true, 0);

                await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.DoiDiemTichLuyThanhCong, true, DockEventType.Not, null, $"{Function.FormatMoney(getReadeem.TripPoint)}", $"{Function.FormatMoney(getReadeem.ToPoint)}");

                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [Share Point]
        public async Task<ApiResponse<object>> WalletTransactionGiftSearch(int accountid, int page, int limit)
        {
            try
            {
                var data = await _iWalletTransactionRepository.SearchPagedList(page, limit, 
                    x => x.AccountId == accountid 
                    && x.Status != EWalletTransactionStatus.Draft && (x.Type == EWalletTransactionType.GiftPoint || x.Type == EWalletTransactionType.ReceivedPoint), 
                    x => x.OrderByDescending(m => m.CreatedDate));

                var accountIds = data.Data.Select(x => x.GiftFromAccountId).ToList();
                accountIds.AddRange(data.Data.Select(x => x.GiftToAccountId).ToList());
                accountIds = accountIds.Where(x => x > 0).ToList();
                var accounts = await _iAccountRepository.SearchAsync(x => accountIds.Contains(x.Id));

                var newList = new List<WalletTransactionShareDTO>();

                foreach (var item in data.Data)
                {
                    var accountFrom = accounts.FirstOrDefault(x => x.Id == item.GiftFromAccountId);
                    var accountTo = accounts.FirstOrDefault(x => x.Id == item.GiftToAccountId);

                    newList.Add(new WalletTransactionShareDTO
                    {
                        CreatedDate = item.CreatedDate,
                        Amount = item.Amount,
                        SubAmount = item.SubAmount,
                        Type = item.Type,
                        GiftFromAccount = $"{accountFrom?.FullName} - {accountFrom?.Phone}",
                        GiftToAccount = $"{accountTo?.FullName} - {accountTo?.Phone}",
                        Id = item.Id,
                        GiftFromAccountId = accountFrom?.Id ?? 0,
                        GiftToAccountId = accountTo?.Id ?? 0,
                        GiftToAccountPhone = item.Type == EWalletTransactionType.GiftPoint ? accountTo?.Phone : null,
                        GiftToAccountFullName = item.Type == EWalletTransactionType.GiftPoint ? accountTo?.FullName : null,
                        GiftToAccountVerifyStatus = item.Type == EWalletTransactionType.GiftPoint ? accountTo?.VerifyStatus : EAccountVerifyStatus.Waiting,
                        PointType = item.Amount > 0 ? 0 : 1
                    });
                }
                return ApiResponse.WithData(newList, data.TotalRows, limit);
            }
            catch (Exception ex)
            {
                AddLog(accountid, "PaymentService.WalletTransactionGets", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> WalletTransactionGiftCheckAccount(int accountId, string phone)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            var functionName = "PaymentService.WalletTransactionGiftCheckAccount";
            try
            {
                phone = phone?.Trim();
                var to = await _iAccountRepository.SearchOneAsync(true, x => x.Phone == phone && x.Id != accountId);
                if (to == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidAccount);
                }
                return ApiResponse.WithData(new
                {
                    AccountId = to.Id,
                    to.FullName,
                    to.VerifyStatus,
                    to.Phone
                });
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }    

        public async Task<ApiResponse<object>> WalletTransactionGiftPost(int accountId, WalletTransactionGiftCommand model)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            var functionName = "PaymentService.WalletTransactionGiftPost";
            try
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"{JsonConvert.SerializeObject(model)}", false);

                if (model.Amount < _baseSetting.MinGiftPoint)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Số điểm muốn tặng phải tối thiểu {Function.FormatMoney(_baseSetting.MinGiftPoint)}", false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.SoDiemToiThieuMuonTang, $"{Function.FormatMoney(_baseSetting.MinGiftPoint)}");
                }

                if (model.PointType != 0)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                if (model.GiftToAccountId == accountId)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanGuiKhongDuocLaTaiKhoanNhan);
                }
                var from = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                var to = await _iAccountRepository.SearchOneAsync(true, x => x.Id == model.GiftToAccountId);
                if (to == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản gửi không tồn tại", false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidAccount);
                }

                if(to.VerifyStatus != EAccountVerifyStatus.Ok)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản nhận phải cần được xác thực mới có thể nhận được điểm", false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanNhanChiaSePhaiXacNhan);
                }

                // đang trong chuyến đi không thể chia sẻ điểm
                var kt = await _iBookingRepository.AnyAsync(x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (kt)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Bạn đang trong chuyến đi không thể chia sẻ điểm", false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.DangTrongChuyenDiKhongTheChiaSeDiem);
                }

                var walletFrom = await _iWalletRepository.GetWalletAsync(accountId);
                var walletTo = await _iWalletRepository.GetWalletAsync(to.Id);

                if (walletFrom.Balance < model.Amount && model.PointType == 0)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, ResponseCode.BalanceNotMoney.GetDisplayName(), false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.BalanceNotMoney);
                }

                if (walletFrom.SubBalance < model.Amount && model.PointType == 1)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, ResponseCode.SubBalanceNotMoney.GetDisplayName(), false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.SubBalanceNotMoney);
                }

                if ((walletFrom.Balance + walletFrom.SubBalance - model.Amount) < _baseSetting.MinGiftPoint)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không thể chia sẻ điểm, điều kiện sau khi chia sẻ thì tài khoản phải còn ít nhất {Function.FormatMoney(_baseSetting.MinGiftPoint)}", false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanKhongDuChiaSeDiem, $"{Function.FormatMoney(_baseSetting.MinGiftPoint)}");
                }

                // tạo log lịch sử cho tài khoản gửi
                var dlAddFrom = new WalletTransaction
                {
                    AccountId = accountId,
                    WalletId = walletFrom.Id,
                    Type = EWalletTransactionType.GiftPoint,
                    Amount = model.Amount,
                    Status = EWalletTransactionStatus.Done,
                    TotalAmount = model.Amount,
                    CampaignId = 0,
                    DepositEvent_SubAmount = 0,
                    TripPoint = 0,
                    CreatedDate = DateTime.Now,
                    CreatedUser = 0,
                    IsAsync = false,
                    Wallet_Balance = walletFrom.Balance,
                    Wallet_SubBalance = walletFrom.SubBalance,
                    Wallet_HashCode = walletFrom.HashCode,
                    PaymentGroup = EPaymentGroup.Default,
                    PadTime = DateTime.Now,
                    OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.GiftPoint),
                    Note = $"Tặng cho tài khoản có SĐT {to.Phone} số điểm là {Function.FormatMoney(model.Amount)}",
                    GiftFromAccountId = accountId,
                    GiftToAccountId = to.Id,
                    Wallet_TripPoint = walletFrom.TripPoint
                };
                dlAddFrom.HashCode = Function.TransactionHash(dlAddFrom.OrderIdRef, dlAddFrom.Amount, dlAddFrom.SubAmount, dlAddFrom.TripPoint, _baseSetting.TransactionSecretKey);

                // tạo log lịch sử cho tài khoản nhận
                var dlAddTo = new WalletTransaction
                {
                    AccountId = to.Id,
                    WalletId = walletTo.Id,
                    Type = EWalletTransactionType.ReceivedPoint,
                    Amount = model.Amount,
                    Status = EWalletTransactionStatus.Done,
                    TotalAmount = model.Amount,
                    CampaignId = 0,
                    DepositEvent_SubAmount = 0,
                    TripPoint = 0,
                    CreatedDate = DateTime.Now,
                    CreatedUser = 0,
                    IsAsync = false,
                    Wallet_Balance = walletTo.Balance,
                    Wallet_SubBalance = walletTo.SubBalance,
                    Wallet_HashCode = walletTo.HashCode,
                    PaymentGroup = EPaymentGroup.Default,
                    PadTime = DateTime.Now,
                    OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.ReceivedPoint),
                    Note = $"Nhận từ tài khoản có SĐT {from.Phone} số điểm là {Function.FormatMoney(model.Amount)}",
                    GiftFromAccountId = accountId,
                    GiftToAccountId = to.Id,
                    Wallet_TripPoint= walletTo.TripPoint
                };
                dlAddTo.HashCode = Function.TransactionHash(dlAddTo.OrderIdRef, dlAddTo.Amount, dlAddTo.SubAmount, dlAddTo.TripPoint, _baseSetting.TransactionSecretKey);

                await _iWalletTransactionRepository.AddAsync(dlAddFrom);
                await _iWalletTransactionRepository.AddAsync(dlAddTo);
                await _iWalletTransactionRepository.CommitAsync();
                //Trừ ví của tài khoản
                await _iWalletRepository.UpdateAddWalletAsync(from.Id, -model.Amount, 0, 0, _baseSetting.TransactionSecretKey, false, false, 0);
                await _iWalletRepository.UpdateAddWalletAsync(to.Id, model.Amount, 0, 0, _baseSetting.TransactionSecretKey, true, false, 0);
                // Gửi notify
                await _iFireBaseService.SendNotificationByAccount(functionName, requestId, dlAddFrom.GiftFromAccountId, ENotificationTempType.TangDiemThanhCong, true, DockEventType.Not, null, to.Phone, (model.PointType == 1 ? "khuyến mãi" : ""), Function.FormatMoney(model.Amount));
                await _iFireBaseService.SendNotificationByAccount(functionName, requestId, dlAddFrom.GiftToAccountId, ENotificationTempType.NhanDiemThanhCong, true, DockEventType.Not, null, from.Phone, (model.PointType == 1 ? "khuyến mãi" : ""), Function.FormatMoney(model.Amount));

                walletFrom = await _iWalletRepository.GetWalletAsync(accountId);
                var dataOut = new WalletTransactionGiftDTOCommand()
                {
                    Amount = model.Amount,
                    Id = dlAddFrom.Id,
                    Wallet_Balance = walletFrom.Balance,
                    Wallet_SubBalance = walletFrom.SubBalance,
                    FullName = to.FullName,
                    Phone = to.Phone
                };
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Done {JsonConvert.SerializeObject(dataOut)}", false);
                return ApiResponse.WithData(dataOut);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region Chốt số điểm trừ cho khách hàng khi hết hạn điểm km
        public async Task<ApiResponse<object>> SubPointSettlement()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.SubPointSettlement";
            try
            {
                var clearPointByAccounts = await _iWalletRepository.ClearPointByAccounts(200);
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
        }

        #endregion

        #region [Tính toán giảm giá cho khác hàng]
        public async Task<ApiResponse<object>> RefundPoint(int accountId, DateTime time)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.RefundPoint";
            try
            {
                if(_baseSetting.EnableDiscountTransaction)
                {
                    var transactionInDay = await _iTransactionRepository.DiscountTransactionByDay(accountId, time);
                    return ApiResponse.WithData(transactionInDay);
                }    
                else
                {
                    return ApiResponse.WithStatusOk();
                }
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatusNotOk();
            }
        }
        #endregion

        #region [Thực hiện hoàn tiền giảm giá cho khách hàng]
        public async Task<ApiResponse<object>> RefundAddPoint(DateTime time)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.RefundAddPoint";
            try
            {
                if (!_baseSetting.EnableDiscountTransaction)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.Ok);
                }

                var ds = await _iTransactionDiscountRepository.SearchAsync(x=> x.Status == false && x.Date.Date == time.Date);

                AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Bắt đầu kiểm tra hoàn điểm, tìm thấy số bản ghi {ds.Count}", false);
                var listWT = new List<WalletTransaction>();

                foreach (var item in ds)
                {
                    var checkTT = await _iTransactionDiscountRepository.SearchOneAsync(false, x => x.Id == item.Id);
                    if(checkTT != null)
                    {
                        checkTT.Status = true;
                        checkTT.UpdatedDate = DateTime.Now;
                        _iTransactionDiscountRepository.Update(checkTT);
                        await _iTransactionDiscountRepository.CommitAsync();
                    }   
                    
                    var wallet = await _iWalletRepository.GetWalletAsync(item.AccountId);

                    var dlAdd = new WalletTransaction
                    {
                        AccountId = item.AccountId,
                        WalletId = wallet.Id,
                        Type = EWalletTransactionType.DiscountTransaction,
                        Amount = 0,
                        Status = EWalletTransactionStatus.Done,
                        TotalAmount = item.DiscountPoint,
                        CampaignId = 0,
                        DepositEvent_SubAmount = 0,
                        TripPoint = 0,
                        CreatedDate = DateTime.Now,
                        CreatedUser = 0,
                        IsAsync = false,
                        Wallet_Balance = wallet.Balance,
                        Wallet_SubBalance = wallet.SubBalance,
                        Wallet_HashCode = wallet.HashCode,
                        PaymentGroup = EPaymentGroup.Default,
                        PadTime = DateTime.Now,
                        OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.GiftPoint),
                        Note = $"Sự kiện '7 ngày lành mạnh cùng TNGO' hoàn điểm của ngày {time:dd/MM/yyyy}",
                        Wallet_TripPoint = wallet.TripPoint,
                        SubAmount = item.DiscountPoint
                    };
                    dlAdd.HashCode = Function.TransactionHash(dlAdd.OrderIdRef, dlAdd.Amount, dlAdd.SubAmount, dlAdd.TripPoint, _baseSetting.TransactionSecretKey);
                    listWT.Add(dlAdd);
                    await _iWalletTransactionRepository.AddAsync(dlAdd);
                    await _iWalletTransactionRepository.CommitAsync();
                    await _iWalletRepository.UpdateAddWalletAsync(dlAdd.AccountId, 0, item.DiscountPoint, 0, _baseSetting.TransactionSecretKey, true, false, 0);
                    AddLog(dlAdd.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Hoàn điểm {dlAdd.TotalAmount} của ngày '{time:dd/MM/yyyy}' trong sự kiện 7 ngày lành mạnh cùng TNGO", false);
                }
                // Gửi notify
                foreach(var item in listWT)
                {
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, item.AccountId, ENotificationTempType.BayNgayLanhManhCungTNGO, true, DockEventType.Not, null, Function.FormatMoney(item.TotalAmount), $"{time:dd/MM/yyyy}");
                }
                return ApiResponse.WithData(ds);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatusNotOk();
            }
        }
        #endregion

        #region Khách hàng nào hết hạn sử dụng tài khoản km thì sẽ bị clear
        public async Task<ApiResponse<object>> ClearSubPoint()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "PaymentService.ClearSubPoint";
            
            try
            {
                var clearPointByAccounts = await _iWalletRepository.SubPointExpirySettlementGets(5000);
                if(!clearPointByAccounts.Any())
                {
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }

                var listWT = new List<WalletTransaction>();

                foreach(var item in clearPointByAccounts)
                {
                    var subPointBiTru = item.SubBalance;
                    var wallet = await _iWalletRepository.GetWalletAsync(item.AccountId);
                    if(wallet.SubBalance <= 0)
                    {
                        await _iWalletRepository.SubPointExpirySettlementSetTrue(item.Id);
                        continue;
                    }    

                    if (wallet.SubBalance < subPointBiTru)
                    {
                        subPointBiTru = wallet.SubBalance;
                    }   
                    
                    var orderId = Function.GenOrderId((int)EWalletTransactionType.ClearTripPoint);
                    var wtransaction = new WalletTransaction
                    {
                        AccountId = item.AccountId,
                        CampaignId = 0,
                        IsAsync = true,
                        Note = $"Khách hàng hết hạn sử dụng điểm khuyến mãi thời gian hết ngày {item.ExpiryDate:dd/MM/yyyy}",
                        OrderIdRef = orderId,
                        CreatedDate = DateTime.Now,
                        CreatedUser = 0,
                        TransactionId = 0,
                        TransactionCode = null,
                        WalletId = wallet.Id,
                        Status = EWalletTransactionStatus.Done,
                        SubAmount = subPointBiTru,
                        Amount = 0,
                        TotalAmount = subPointBiTru,
                        Type = EWalletTransactionType.ClearTripPoint,
                        Wallet_Balance = wallet.Balance,
                        Wallet_SubBalance = wallet.SubBalance,
                        Wallet_HashCode = wallet.HashCode,
                        Wallet_TripPoint = wallet.TripPoint,
                        TripPoint = 0,
                        PadTime = DateTime.Now
                    };
                    wtransaction.HashCode = Function.TransactionHash(wtransaction.OrderIdRef, wtransaction.Amount, wtransaction.SubAmount, wtransaction.TripPoint , _baseSetting.TransactionSecretKey);
                    await _iWalletTransactionRepository.AddAsync(wtransaction);
                    await _iWalletTransactionRepository.CommitAsync();
                    await _iWalletRepository.UpdateAddWalletAsync(item.AccountId, 0, -subPointBiTru, 0, _baseSetting.TransactionSecretKey, false, false, 0);
                    await _iWalletRepository.SubPointExpirySettlementSetTrue(item.Id);
                    listWT.Add(wtransaction);
                }

                foreach (var item in listWT)
                {
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, item.AccountId, ENotificationTempType.DauThangTruTienTKKM, true, DockEventType.Not, null, Function.FormatMoney(item.TotalAmount));
                }
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [Function Add log]

        private bool PayooVerifySignature(string myKey, string yourKey)
        {
            try
            {
                if (string.IsNullOrEmpty(myKey) || string.IsNullOrEmpty(yourKey))
                {
                    return false;
                }
                return myKey.Equals(PayooPayGenerateHashString(yourKey), StringComparison.OrdinalIgnoreCase);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string PayooStringBuilder(object APIParam, string KeyFields, string strChecksumKey)
        {
            try
            {
                var strData = new StringBuilder();
                strData.Append(strChecksumKey);
                string[] arrKeyFields = KeyFields.Split('|');
                foreach (string strKey in arrKeyFields)
                {
                    strData.Append("|" + APIParam.GetType().GetProperty(strKey).GetValue(APIParam, null));
                }
                return strData.ToString();
            }
            catch
            {
                return "";
            }
        }

        private static string GenerateCheckSum(string input)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input);
            using (SHA512 hash = System.Security.Cryptography.SHA512.Create())
            {
                byte[] hashedInputBytes = hash.ComputeHash(bytes);

                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                StringBuilder hashedInputStringBuilder = new System.Text.StringBuilder(128);
                foreach (byte b in hashedInputBytes)
                {
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                }
                return hashedInputStringBuilder.ToString();
            }
        }

        private string PayooPayGenerateHashString(string input)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(input);
            using (var hash = SHA512.Create())
            {
                byte[] hashedInputBytes = hash.ComputeHash(bytes);
                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                StringBuilder hashedInputStringBuilder = new StringBuilder(128);
                foreach (byte b in hashedInputBytes)
                {
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                }
                return hashedInputStringBuilder.ToString();
            }
        }

        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
        }
        #endregion

        #region [DiscountCode]
        public async Task<ApiResponse<object>> DiscountCodes(int accountId, string lat, string lng)
        {
            try
            {
                var findProject = await _iMemoryCacheService.LatLngToCustomerGroup(lat, lng, accountId);
                var discountCodes = (await _discountCodeRepository.DiscountCodePublicByAccount(accountId, findProject.ProjectId, findProject.CustomerGroupId)).Select(x => new DiscountCodeDTO(x));
                return ApiResponse.WithData(discountCodes);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "PaymentService.DiscountCodes", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion
    }

    public class PaymentXMLFactory
    {
        public static PayooPaymentNotification GetPaymentNotify(string notifyData)
        {
            try
            {
                string Data = Encoding.UTF8.GetString(Convert.FromBase64String(notifyData));
                var invoice = new PayooPaymentNotification();
                var dock = new XmlDocument();
                dock.LoadXml(Data);
                if (!string.IsNullOrEmpty(ReadNodeValue(dock, "BillingCode")))
                {
                    // Pay at store
                    if (!string.IsNullOrEmpty(ReadNodeValue(dock, "ShopId")))
                    {
                        invoice.ShopId = long.Parse(ReadNodeValue(dock, "ShopId"));
                    }
                    invoice.OrderNo = ReadNodeValue(dock, "OrderNo");
                    if (!string.IsNullOrEmpty(ReadNodeValue(dock, "OrderCashAmount")))
                    {
                        invoice.OrderCashAmount = long.Parse(ReadNodeValue(dock, "OrderCashAmount"));
                    }
                    invoice.State = ReadNodeValue(dock, "State");
                    invoice.PaymentMethod = ReadNodeValue(dock, "PaymentMethod");
                    invoice.BillingCode = ReadNodeValue(dock, "BillingCode");
                    invoice.PaymentExpireDate = ReadNodeValue(dock, "PaymentExpireDate");
                   // invoice.PaymentStatus = int.Parse(ReadNodeValue(dock, "PaymentStatus"));
                }
                else
                {
                    invoice.Session = ReadNodeValue(dock, "session");
                    invoice.BusinessUsername = ReadNodeValue(dock, "username");
                    invoice.ShopId = long.Parse(ReadNodeValue(dock, "shop_id"));
                    invoice.ShopTitle = ReadNodeValue(dock, "shop_title");
                    invoice.ShopDomain = ReadNodeValue(dock, "shop_domain");
                    invoice.ShopBackUrl = ReadNodeValue(dock, "shop_back_url");
                    invoice.OrderNo = ReadNodeValue(dock, "order_no");
                    invoice.OrderCashAmount = long.Parse(ReadNodeValue(dock, "order_cash_amount"));
                    invoice.StartShippingDate = ReadNodeValue(dock, "order_ship_date");
                    invoice.ShippingDays = short.Parse(ReadNodeValue(dock, "order_ship_days"));
                    invoice.OrderDescription = System.Web.HttpUtility.UrlDecode((ReadNodeValue(dock, "order_description")));
                    invoice.NotifyUrl = ReadNodeValue(dock, "notify_url");
                    invoice.State = ReadNodeValue(dock, "State");
                    invoice.PaymentMethod = ReadNodeValue(dock, "PaymentMethod");
                    invoice.PaymentExpireDate = ReadNodeValue(dock, "validity_time");
                    //invoice.PaymentStatus = int.Parse(ReadNodeValue(dock, "PaymentStatus"));
                }
                return invoice;
            }
            catch
            {
                return null;
            }
        }

        public static PayooCallbackNotifyResponseDTO GetPayooConnectionPackage(string strPackage)
        {
            try
            {
                string PackageData = strPackage;
                var objPackage = new PayooCallbackNotifyResponseDTO();
                var Doc = new XmlDocument();
                Doc.LoadXml(PackageData);
                objPackage.Data = ReadNodeValue(Doc, "Data");
                objPackage.Signature = ReadNodeValue(Doc, "Signature");
                objPackage.KeyFields = ReadNodeValue(Doc, "KeyFields");
                objPackage.PayooSessionID = ReadNodeValue(Doc, "PayooSessionID");
                return objPackage;
            }
            catch
            {
                return null;
            }
        }


        private static string ReadNodeValue(XmlDocument Doc, string TagName)
        {
            XmlNodeList nodeList = Doc.GetElementsByTagName(TagName);
            string nodeValue = null;
            if (nodeList.Count > 0)
            {
                XmlNode node = nodeList.Item(0);
                nodeValue = (node == null) ? "" : node.InnerText;
            }
            return nodeValue == null ? "" : nodeValue;
        }

        private static readonly string _XML = @"<shops>
                                <shop>
                                    <session>{0}</session>
                                    <username>{1}</username>
                                    <shop_id>{2}</shop_id>
                                    <shop_title>{3}</shop_title>
                                    <shop_domain>{4}</shop_domain>
                                    <shop_back_url>{5}</shop_back_url>
                                    <order_no>{6}</order_no>
                                    <order_cash_amount>{7}</order_cash_amount>
                                    <order_ship_date>{8}</order_ship_date>
                                    <order_ship_days>{9}</order_ship_days>
                                    <order_description>{10}</order_description>
                                    <notify_url>{11}</notify_url>
                                    <validity_time>{12}</validity_time>
                                    <JsonResponse>true</JsonResponse>
                                    <customer>
                                       <name>{13}</name>
                                       <phone>{14}</phone>
                                       <address>{15}</address>
                                       <city>{16}</city>
                                       <email>{17}</email>
                                    </customer>
                                </shop>
                            </shops>";

        private static readonly string _XML_Temp = @"<shops>
                                <shop>
                                    <session>{0}</session>
                                    <username>{1}</username>
                                    <shop_id>{2}</shop_id>
                                    <shop_title>{3}</shop_title>
                                    <shop_domain>{4}</shop_domain>
                                    <shop_back_url>{5}</shop_back_url>
                                    <order_no>{6}</order_no>
                                    <order_cash_amount>{7}</order_cash_amount>
                                    <order_ship_date>{8}</order_ship_date>
                                    <order_ship_days>{9}</order_ship_days>
                                    <order_description>{10}</order_description>
                                    <notify_url>{11}</notify_url>
                                    <JsonResponse>true</JsonResponse>
                                    <customer>
                                       <name>{12}</name>
                                       <phone>{13}</phone>
                                       <address>{14}</address>
                                       <city>{15}</city>
                                       <email>{16}</email>
                                    </customer>
                                </shop>
                            </shops>";

        public static string GetPaymentXML(PayooDepositCommand Info)
        {
            try
            {
                if (Info == null)
                    return "";
                if (string.IsNullOrEmpty(Info.ValidityTime))
                {
                    return string.Format(_XML_Temp, Info.Session, Info.Username, Info.ShopId, Info.ShopTitle,
                              Info.ShopDomain, Info.ShopBackUrl, Info.OrderNo, Info.OrderCashAmount, Info.OrderShipDate,
                              Info.OrderShipDays, Info.OrderDescription, Info.NotifyUrl,
                              Info.Name, Info.Phone, Info.Address, "---", "---");
                }
                return string.Format(_XML, Info.Session, Info.Username, Info.ShopId, Info.ShopTitle,
                              Info.ShopDomain, Info.ShopBackUrl, Info.OrderNo, Info.OrderCashAmount, Info.OrderShipDate,
                              Info.OrderShipDays, Info.OrderDescription, Info.NotifyUrl, Info.ValidityTime,
                              Info.Name, Info.Phone, Info.Address, "---", "---");
            }
            catch
            {
                return "";
            }
        }

       
    }
}
