﻿using Microsoft.Extensions.Logging;
using TN.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using PT.Domain.Model;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Caching.Memory;
using TN.API.Model.Setting;
using TN.Shared;

namespace TN.API.Services
{
    public interface IDashboardService : IService<Transaction>
    {
        Task<ApiResponse<object>> ChartsTop(int? weekType, int accountId, string fullName, string phone);
        Task<ApiResponse<object>> OldTop(int accountId);
    }
    public class DashboardService : IDashboardService
    {
        private readonly IDockRepository _iDockRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IMemoryCacheService _memoryCache;
        private readonly IOptions<BaseSetting> _baseSettings;
        private readonly IAccountRatingItemRepository _iAccountRatingItemRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        public DashboardService
        (
            IDockRepository iDockRepository,
            IMongoDBRepository iMongoDBRepository,
            IOptions<LogSettings> logSettings,
            IMemoryCacheService memoryCache,
            IOptions<BaseSetting> baseSettings,
            IAccountRatingItemRepository iChartsTopRepository,
            IAccountRepository iAccountRepository,
            ITransactionRepository iTransactionRepository
        )
        {
            _iDockRepository = iDockRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _memoryCache = memoryCache;
            _baseSettings = baseSettings;
            _iAccountRatingItemRepository = iChartsTopRepository;
            _iAccountRepository = iAccountRepository;
            _iTransactionRepository = iTransactionRepository;
        }

        public virtual async Task<ApiResponse<object>> ChartsTop(int? weekTypes, int accountId, string fullName, string phone)
        {
            try
            {
                var dateTimeNow = DateTime.Now;
                var thisWeek = Function.GetIso8601WeekOfYear(dateTimeNow);
                if(weekTypes == 0)
                {
                    thisWeek = Function.GetIso8601WeekOfYear(thisWeek.StartDate.AddDays(-2));
                }
                var code = $"W_{thisWeek.WeekNumber:D2}_{thisWeek.Year}";
                var parent = await _iAccountRatingItemRepository.AccountRatingGetByCodeAsync(code);
                if(parent == null)
                {
                    return new ApiResponse<object>();
                }    
                var accounts = new List<Account>();
                var outData = new ChartsRatingDTO
                {
                    Reward = parent.Reward
                };
                var data = await _iAccountRatingItemRepository.SearchByTopAsync(20, x => x.Code == code);
                   
                var accountIds = data.Select(x => x.AccountId).Distinct().ToList();
                if(accountIds.Any())
                {
                    accounts = await _iAccountRepository.SearchAsync(x => accountIds.Contains(x.Id));
                }

                outData.TopRating = data.Select(x => new ChartsTopDTO(x, accounts.FirstOrDefault(m => m.Id == x.AccountId), accountId)).ToList();
                
                if(outData.TopRating.Any(x=>x.AccountId == accountId))
                {
                    outData.CurrentRating = outData.TopRating.FirstOrDefault(x => x.AccountId == accountId);
                }
                else
                {
                    var totalKM = _iTransactionRepository.TotalKMByAccount(accountId, thisWeek.StartDate, thisWeek.EndDate.AddDays(1));
                    var isDeb = await _iTransactionRepository.AnyAsync(x => x.AccountId == accountId && x.EndTime >= thisWeek.StartDate && x.EndTime < thisWeek.EndDate.AddDays(1) && x.Status == EBookingStatus.Debt);
                    outData.CurrentRating = new ChartsTopDTO { AccountId = accountId, IsBold = false, FullName = fullName, Order = 0, Phone = phone, Value = Convert.ToDecimal(totalKM), IsDebt = isDeb };
                    AddLog(0, "StationService.ChartsTop", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{Newtonsoft.Json.JsonConvert.SerializeObject(outData.CurrentRating)}", true);
                }
                return ApiResponse.WithData(outData);
            }
            catch (Exception ex)
            {
                AddLog(0, "StationService.ChartsTop", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> OldTop(int accountId)
        {
            try
            {
                var datas = await _iAccountRatingItemRepository.SearchByTopAsync(20, x => x.AccountId == accountId, x=>x.OrderByDescending(m=>m.Id));
                var newList = new List<OldTopDTO>();

                foreach(var item in datas)
                {
                    var spl = item.Code.Split('_').ToList();
                    var dl = new OldTopDTO
                    {
                        Order = item.Order,
                        Value = item.Value,
                        Week = Convert.ToInt32(spl[1]),
                        Year = Convert.ToInt32(spl[2]),
                        Days = item.Code
                    };
                    newList.Add(dl);
                }

                return ApiResponse.WithData(newList);
            }
            catch (Exception ex)
            {
                AddLog(0, "StationService.OldTop", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
        #endregion
    }
}
