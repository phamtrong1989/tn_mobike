﻿using Microsoft.Extensions.Options;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.API.Infrastructure.Interfaces;
using TN.Domain.Seedwork;
using TN.API.Model;
using PT.Domain.Model;
using Microsoft.AspNetCore.Http;
using TN.Shared;

namespace TN.API.Services
{
    public interface ITicketPrepaidService : IService<TicketPrepaid>
    {
        Task<ApiResponse<object>> Search(int accountId, int page, int limit, HttpRequest request);
        Task<ApiResponse<object>> Buy(int accountId, HttpRequest request);
        Task<ApiResponse<object>> Buy(int accountId, ProjectBuyCommand cm, HttpRequest request);
    }

    public class TicketPrepaidService : ITicketPrepaidService
    {
        private readonly ITicketPrepaidRepository _iTicketPrepaidRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly Model.Setting.BaseSetting _baseSettings;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IFireBaseService _iFireBaseService;
        private readonly IMemoryCacheService _iMemoryCacheService;
        private readonly IBookingRepository _iBookingRepository;
        public TicketPrepaidService
        (
            ITicketPrepaidRepository iTicketPrepaidRepository,
            IAccountRepository iAccountRepository,
            ITicketPriceRepository iTicketPriceRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IProjectAccountRepository iProjectAccountRepository,
            ITransactionRepository iTransactionRepository,
            IWalletRepository iWalletRepository,
            IWalletTransactionRepository iWalletTransactionRepository,
            IOptions<Model.Setting.BaseSetting> baseSettings,
            IProjectRepository iProjectRepository,
            IMongoDBRepository iMongoDBRepository,
            IOptions<LogSettings> logSettings,
            IFireBaseService iFireBaseService ,
            IMemoryCacheService iMemoryCacheService,
            IBookingRepository iBookingRepository
        )
        {
            _iTicketPrepaidRepository = iTicketPrepaidRepository;
            _iAccountRepository = iAccountRepository;
            _iTicketPriceRepository = iTicketPriceRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iTransactionRepository = iTransactionRepository;
            _iWalletRepository = iWalletRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _baseSettings = baseSettings.Value;
            _iProjectRepository = iProjectRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _iFireBaseService = iFireBaseService;
            _iMemoryCacheService = iMemoryCacheService;
            _iBookingRepository = iBookingRepository;
        }

        public async Task<ApiResponse<object>> Search(int accountId, int page, int limit, HttpRequest request)
        {
            try
            {
                var data = await _iTicketPrepaidRepository.SearchPagedList(
                    page,
                    limit,
                    x => x.AccountId == accountId, 
                    x=> x.OrderByDescending(m=>m.DateOfPayment), 
                    x=> new TicketPrepaid { 
                        Id = x.Id, 
                        ProjectId = x.ProjectId, 
                        TicketPrice_TicketType = x.TicketPrice_TicketType, 
                        TicketValue = x.TicketValue, 
                        CustomerGroupId = x.CustomerGroupId, 
                        StartDate = x.StartDate, 
                        EndDate = x.EndDate, 
                        MinutesSpent = x.MinutesSpent, 
                        AccountId = x.AccountId,
                        DateOfPayment = x.DateOfPayment,
                        Note = x.Note,
                        Code = x.Code,
                        NumberExpirations = x.NumberExpirations,
                        TicketPriceId = x.TicketPriceId,
                        TicketPrice_LimitMinutes = x.TicketPrice_LimitMinutes
                    });

                var projects = await _iMemoryCacheService.ProjectSearchAsync();
                var customerGroups = await _iMemoryCacheService.CustomerGroupSearchAsync();

                var newList = new List<TicketPrepaidDTO>();
                foreach (var item in data.Data)
                {
                    newList.Add(new TicketPrepaidDTO(item, projects.FirstOrDefault(x=>x.Id == item.ProjectId)?.Name, customerGroups.FirstOrDefault(x=>x.Id == item.CustomerGroupId)?.Name));
                }
                return ApiResponse.WithData(newList, data.TotalRows, data.Limit) ;
            }
            catch (Exception ex)
            {
                AddLog(accountId, "TicketPrepaidService.Search", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> Buy(int accountId, HttpRequest request)
        {
            try
            {
                var projects = await _iMemoryCacheService.ProjectSearchAsync();
                var projectIds = projects.Select(x => x.Id).ToList();
                var projectAccounts = await _iProjectAccountRepository.SearchAsync(x => x.AccountId == accountId && projectIds.Contains(x.ProjectId));

                var customerGroupInAccount = projectAccounts.Where(x => x.CustomerGroupId > 0).Select(x => x.CustomerGroupId).ToList();
                var prices = (await _iMemoryCacheService.TicketPriceSearchAsync()).Where(x => projectIds.Contains(x.ProjectId) && (customerGroupInAccount.Contains(x.CustomerGroupId) || x.IsDefault) && x.IsActive == true && x.TicketType != ETicketType.Block);
                var customerGroups = (await _iMemoryCacheService.CustomerGroupSearchAsync()).Where(x => projectIds.Contains(x.ProjectId) && x.Status == ECustomerGroupStatus.Active);
                var outData = new List<ProjectViewDTO>();
                // Nếu con bất kì vé trả trước mà hôm nay còn hạn thì không thể mua mới, phải dùng hết vé trả trước cũ mới được mua thêm
                var anyPrepad = await _iTicketPrepaidRepository.SearchAsync(x => projectIds.Contains(x.ProjectId) && x.AccountId == accountId && x.StartDate <= DateTime.Now.Date && x.EndDate >= DateTime.Now.Date);
                foreach (var item in projects)
                {
                    var priceByProject = prices.Where(x => x.ProjectId == item.Id).OrderByDescending(x => x.CustomerGroupId).ToList();
                    var ticketPriceByProject = new List<TicketPriceDTO>();
                    var exitsPrepad = anyPrepad.FirstOrDefault(x => x.ProjectId == item.Id);
                    priceByProject.ForEach(item =>
                    {
                        var customerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);
                        if (customerGroup != null)
                        {
                            var dlAdd = new TicketPriceDTO()
                            {
                                Id = item.Id,
                                Name = $"{ (customerGroup.IsDefault ? "" : $"{customerGroup.Name} - ") }{ item.TicketType.GetDisplayName(AppHttpContext.AppLanguage) } / {Function.FormatMoney(item.TicketValue)} {((exitsPrepad?.TicketPrice_TicketType == item.TicketType) ? "(gia hạn/extend)" : "")}",
                                TicketValue = item.TicketValue,
                                StartDate = item.TicketType == ETicketType.Day ? DateTime.Now.Date : Convert.ToDateTime($"{DateTime.Now.Year}-{DateTime.Now.Month}-01"),
                                EndDate = item.TicketType == ETicketType.Day ? DateTime.Now.Date : Convert.ToDateTime($"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)}"),
                                Note = $"{_iTransactionRepository.GetTicketPriceString(item, ResponseCode.NoiDungChiTietVe.ResponseCodeToString(), AppHttpContext.AppLanguage)}"
                            };
                            if(item.TicketType == ETicketType.Day)
                            {
                                item.Note = $"{_iTransactionRepository.GetTicketPriceString(item, ResponseCode.NoiDungChiTietVeNgay.ResponseCodeToString(), AppHttpContext.AppLanguage)}";
                            }   
                            else if (item.TicketType == ETicketType.Month)
                            {
                                item.Note = $"{_iTransactionRepository.GetTicketPriceString(item, ResponseCode.NoiDungChiTietVeThang.ResponseCodeToString(), AppHttpContext.AppLanguage)}";
                            }
                            ticketPriceByProject.Add(dlAdd);
                        }
                    });

                    var dlAdd = new ProjectViewDTO()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        TicketPrices = ticketPriceByProject
                    };
                    outData.Add(dlAdd);
                }
                return ApiResponse.WithData(outData, 1, 1);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "TicketPrepaidService.BuyView", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> Buy(int accountId, ProjectBuyCommand cm, HttpRequest request)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            var functionName = "TicketPrepaidService.BuyPost";
            var startDate = DateTime.Now.Date;
            var endDate = DateTime.Now.Date;

            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Tài khoản #{accountId} gửi lênh mua vé trả trước {Newtonsoft.Json.JsonConvert.SerializeObject(cm)}", false);
            
            try
            {
                if (cm.Count <= 0 || cm.Count > 20)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                var ticketPrice = await _iMemoryCacheService.TicketPriceGetById(cm.TicketPriceId);
                if (ticketPrice == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tồn tại bảng giá vé", false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                if (ticketPrice.TicketType == ETicketType.Month)
                {
                    startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                }

                //Kiếm tra có nợ cước không
                var ktNoCuoc = await _iTransactionRepository.AnyAsync(x => x.Status == EBookingStatus.Debt && x.AccountId == accountId);
                if (ktNoCuoc)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanConNoCuoc);
                }

                var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                if (account == null || account.VerifyStatus != EAccountVerifyStatus.Ok)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản chưa được xác nhận", false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanCanXacMinh);
                }
             
                // Nếu bảng giá vé là thuộc nhóm đặc biệt mà ko phải mặc dịnh thì phải check xem khách hàng này có thuộc nhóm kh không
                if (ticketPrice.CustomerGroupId > 0 && !ticketPrice.IsDefault)
                {
                    var checkInCustomerGroup = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == ticketPrice.ProjectId && x.CustomerGroupId == ticketPrice.CustomerGroupId);
                    if (!checkInCustomerGroup)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản không thuộc nhóm khách hàng ({ticketPrice.CustomerGroupId}), END");
                        return ApiResponse.WithStatus(false, null, ResponseCode.GiaVeKhongThuocNhomKhachHang);
                    }
                }

                var getW = await _iWalletRepository.GetWalletAsync(accountId);
                // Kiểm tra đủ tiền không
                if (ticketPrice.TicketValue * cm.Count > (getW.Balance + getW.SubBalance))
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không đủ điều khiển mua vé trả trước", false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.NotMoney);
                }

                decimal chargeInAccount = 0;
                decimal chargeInSubAccount = 0;

                for (int i = 0; i < cm.Count; i++)
                {
                    getW = await _iWalletRepository.GetWalletAsync(accountId);
                    // Kiểm tra đủ tiền không
                    if (ticketPrice.TicketValue > (getW.Balance + getW.SubBalance))
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không đủ điều khiển mua vé trả trước", false);
                        return ApiResponse.WithStatus(false, null, ResponseCode.NotMoney);
                    }
                    // Chia tiền
                    var splitTotalPrice = _iWalletRepository.SplitTotalPrice(ticketPrice.TicketValue, ticketPrice.AllowChargeInSubAccount, getW);
                    string noteTransaction = "";
                    var exitsPrepad = await _iTicketPrepaidRepository.SearchOneAsync(false, x => x.ProjectId == ticketPrice.ProjectId && x.AccountId == accountId && x.StartDate == startDate.Date && x.TicketPrice_TicketType == ticketPrice.TicketType);
                    if (exitsPrepad == null)
                    {
                        // Thực hiện mua vé trả trước
                        exitsPrepad = new TicketPrepaid
                        {
                            AccountId = accountId,
                            CustomerGroupId = ticketPrice.CustomerGroupId,
                            StartDate = startDate,
                            EndDate = endDate,
                            TicketPriceId = ticketPrice.Id,
                            DateOfPayment = DateTime.Now,
                            CreatedDate = DateTime.Now,
                            CreatedUser = 0,
                            ProjectId = ticketPrice.ProjectId,
                            Code = $"{ticketPrice.ProjectId:D4}{((byte)ticketPrice.TicketType):D2}{accountId:D6}{DateTime.Now:yyyyMMddHHmmss}",
                            TicketPrice_AllowChargeInSubAccount = ticketPrice.AllowChargeInSubAccount,
                            TicketPrice_BlockPerMinute = ticketPrice.BlockPerMinute ?? 0,
                            TicketPrice_BlockPerViolation = ticketPrice.BlockPerViolation ?? 0,
                            TicketPrice_BlockViolationValue = ticketPrice.BlockViolationValue ?? 0,
                            TicketPrice_IsDefault = ticketPrice.IsDefault,
                            TicketPrice_LimitMinutes = ticketPrice.LimitMinutes,
                            TicketPrice_TicketType = ticketPrice.TicketType,
                            TicketPrice_TicketValue = ticketPrice.TicketValue,
                            TicketValue = splitTotalPrice.Item1 + splitTotalPrice.Item2,
                            ChargeInAccount = splitTotalPrice.Item1,
                            ChargeInSubAccount = splitTotalPrice.Item2,
                            MinutesSpent = 0,
                            NumberExpirations = 1
                        };
                        await _iTicketPrepaidRepository.AddAsync(exitsPrepad);
                        await _iTicketPrepaidRepository.CommitAsync();
                        noteTransaction = "Khách hàng mua vé trên APP";
                    }
                    else
                    {
                        // gia hạn
                        exitsPrepad.TicketPrice_LimitMinutes += ticketPrice.LimitMinutes;
                        exitsPrepad.NumberExpirations += 1;
                        exitsPrepad.UpdatedDate = DateTime.Now;

                        _iTicketPrepaidRepository.Update(exitsPrepad);
                        await _iTicketPrepaidRepository.CommitAsync();

                        noteTransaction = $"Khách hàng gia hạn vé trả trước lần {exitsPrepad.NumberExpirations} trên APP";
                        // Cập nhật time cho transaction
                        var bookings = await _iBookingRepository.SearchAsync(x => x.AccountId == accountId && x.TicketPrepaidId == exitsPrepad.Id && x.Status == EBookingStatus.Start && x.TicketPrice_TicketType != ETicketType.Block);
                        if (bookings.Any())
                        {
                            foreach (var item in bookings)
                            {
                                var ktBooking = await _iBookingRepository.SearchOneAsync(false, x => x.Id == item.Id);
                                if(ktBooking!= null)
                                {
                                    ktBooking.TicketPrice_LimitMinutes = exitsPrepad.TicketPrice_LimitMinutes;
                                    _iBookingRepository.Update(ktBooking);
                                }    
                            }
                            await _iBookingRepository.CommitAsync();
                        }
                    }
                    // Tạo giao dịch log
                    await _iWalletTransactionRepository.AddAsync(new WalletTransaction
                    {
                        AccountId = accountId,
                        CampaignId = 0,
                        IsAsync = true,
                        Note = noteTransaction,
                        OrderIdRef = null,
                        CreatedDate = DateTime.Now,
                        CreatedUser = 0,
                        TransactionId = exitsPrepad.Id,
                        TransactionCode = exitsPrepad.Code,
                        WalletId = getW.Id,
                        Status = EWalletTransactionStatus.Done,
                        SubAmount = splitTotalPrice.Item2,
                        Amount = splitTotalPrice.Item1,
                        TotalAmount = splitTotalPrice.Item1 + splitTotalPrice.Item2,
                        Type = EWalletTransactionType.BuyPrepaid,
                        Wallet_Balance = getW.Balance,
                        Wallet_SubBalance = getW.SubBalance,
                        Wallet_HashCode = getW.HashCode,
                        HashCode = Function.SignSHA256((splitTotalPrice.Item1 + splitTotalPrice.Item2).ToString(), _baseSettings.TransactionSecretKey),
                        PadTime = DateTime.Now
                    });

                    await _iWalletTransactionRepository.CommitAsync();

                    chargeInAccount += exitsPrepad.ChargeInAccount;
                    chargeInSubAccount += exitsPrepad.ChargeInSubAccount;
                    // Trừ tiền trong ví
                    await _iWalletRepository.UpdateAddWalletAsync(accountId, -splitTotalPrice.Item1, -splitTotalPrice.Item2, 0, _baseSettings.TransactionSecretKey, false, false, 0);
                }
                getW = await _iWalletRepository.GetWalletAsync(accountId);
                // Gửi notify
                await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.MuaVeTraTruocThanhCong, true, DockEventType.Not, null, ticketPrice.TicketType.GetDisplayName(AppHttpContext.AppLanguage).ToLower(), $"{ Function.FormatMoney(chargeInAccount + chargeInSubAccount) }", $"{startDate:dd/MM/yyyy}", $"{endDate:dd/MM/yyyy}");

                var project = await _iMemoryCacheService.ProjectGetById(ticketPrice.ProjectId);
                getW = await _iWalletRepository.GetWalletAsync(accountId);
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mua vé trả trước thành công");
                return ApiResponse.WithData(new ProjectPostDTO
                {
                    ProjectName = project?.Name,
                    StartDate = startDate,
                    EndDate = endDate,
                    TicketName = cm.TicketPriceName,
                    UserInfo = $"{account.FullName} - {account.Phone}",
                    Wallet_Balance = getW.Balance,
                    Wallet_SubBalance = getW.SubBalance
                });
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
    }
}
