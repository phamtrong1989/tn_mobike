﻿using Microsoft.Extensions.Logging;
using TN.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using PT.Domain.Model;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Caching.Memory;
using TN.API.Model.Setting;
using TN.Shared;
using Newtonsoft.Json;
using System.Globalization;

namespace TN.API.Services
{
    public interface IMemoryCacheService : IService<object>
    {
        //bool GetValue<T>(ECacheType type, out T t);
        bool GetValue<T>(string type, out T t);
        // void SetValue<T>(ECacheType type, T t, int millisecondsExpiration = 1000);
        void SetValue<T>(string type, T t, int millisecondsExpiration = 1000);
        Task<List<Station>> StationSearchAsync();
        Task<List<Language>> LanguageAllAsync();
        Task<Language> LanguageByCodeAsync(string code);
        string CurrentLanguage();
        Task<int> CurrentLanguageId();
        Task<List<BannerGroup>> BannerGroupAsync();
        Task<List<Project>> ProjectSearchAsync();
        Task<List<CustomerGroup>> CustomerGroupSearchAsync();
        Task<Station> StationGetById(int id);
        Task<List<TicketPrice>> TicketPriceSearchAsync();
        Task<Project> ProjectGetById(int id);
        Task<TicketPrice> TicketPriceGetById(int id);
        Task<decimal> TicketPriceGetDefaultPrice(int projectId);
        Task<ProjectAccountModel> LatLngToCustomerGroup(string lat, string lng, int accountId);
    }

    public class MemoryCacheService : IMemoryCacheService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly BaseSetting _baseSettings;
        private readonly IStationRepository _iStationRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly ILanguageRepository _iLanguageRepository;
        private readonly IBannerGroupRepository _iBannerGroupRepository;
        private readonly IBannerRepository _iBannerRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IProjectResourceRepository _iProjectResourceRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly ICustomerGroupResourceRepository _iCustomerGroupResourceRepository;
        private readonly IStationResourceRepository _iStationResourceRepository;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly INewsRepository _iNewsRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IAccountRatingItemRepository _iChartsTopRepository;

        public MemoryCacheService
        (
            IMemoryCache memoryCache,
            IOptions<BaseSetting> baseSettings,
            IStationRepository iStationRepository,
            IMongoDBRepository iMongoDBRepository,
            IOptions<LogSettings> logSettings,
            ILanguageRepository iLanguageRepository,
            IBannerRepository iBannerRepository,
            IBannerGroupRepository iBannerGroupRepository,
            IProjectRepository iProjectRepository,
            IProjectResourceRepository iProjectResourceRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            ICustomerGroupResourceRepository iCustomerGroupResourceRepository,
            IStationResourceRepository iStationResourceRepository,
            ITicketPriceRepository iTicketPriceRepository,
            INewsRepository iNewsRepository,
            IAccountRepository iAccountRepository,
            IAccountRatingItemRepository iChartsTopRepository
        )
        {
            _memoryCache = memoryCache;
            _baseSettings = baseSettings.Value;
            _iStationRepository = iStationRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _iLanguageRepository = iLanguageRepository;
            _iBannerRepository = iBannerRepository;
            _iBannerGroupRepository = iBannerGroupRepository;
            _iProjectRepository = iProjectRepository;
            _iProjectResourceRepository = iProjectResourceRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iCustomerGroupResourceRepository = iCustomerGroupResourceRepository;
            _iStationResourceRepository = iStationResourceRepository;
            _iTicketPriceRepository = iTicketPriceRepository;
            _iNewsRepository = iNewsRepository;
            _iAccountRepository = iAccountRepository;
            _iChartsTopRepository = iChartsTopRepository;
        }

        public string CurrentLanguage()
        {
            return AppHttpContext.AppLanguage;
        }

        public async Task<int> CurrentLanguageId()
        {
            var langObject = await LanguageByCodeAsync(AppHttpContext.AppLanguage);
            return langObject?.Id ?? 2;
        }

        public virtual async Task<List<Language>> LanguageAllAsync()
        {
            var languagess = new List<Language>();
            try
            {
                if (!GetValue(ECacheType.Languages, out languagess))
                {
                    languagess = await _iLanguageRepository.SearchAsync(x => x.Id > 0);
                    SetValue(ECacheType.Languages, languagess, 1000 * 60 * 10);
                }
                return languagess;
            }
            catch (Exception)
            {
                languagess = await _iLanguageRepository.SearchAsync(x => x.Id > 0);
                return languagess;
            }
        }

        public async Task<Language> LanguageByCodeAsync(string code)
        {
            var languagess = new List<Language>();
            try
            {
                if (!GetValue(ECacheType.Languages, out languagess))
                {
                    languagess = await _iLanguageRepository.SearchAsync(x => x.Id > 0);
                    SetValue(ECacheType.Languages, languagess, 1000 * 60 * 10);
                }
                return languagess.FirstOrDefault(x => x.Code1 == code);
            }
            catch (Exception)
            {
                return await _iLanguageRepository.SearchOneAsync(true, x => x.Code1 == code);
            }
        }

        public virtual async Task<Station> StationGetById(int id)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.StationGetById";
            try
            {
                var stations = await StationSearchAsync();
                return stations.FirstOrDefault(x => x.Id == id && x.Status == ESationStatus.Active);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                var station = await _iStationRepository.SearchOneAsync(true, x => x.Id == id);
                if (station != null)
                {
                    var stationResource = await _iStationResourceRepository.SearchOneAsync(true, x => x.StationId == id);
                    if (stationResource != null)
                    {
                        station.Name = stationResource.Name;
                        station.DisplayName = stationResource.DisplayName;
                        station.Address = stationResource.Address;
                    }
                }
                return station;
            }
        }

        public virtual async Task<Project> ProjectGetById(int id)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.ProjectGetById";
            try
            {
                var project = await ProjectSearchAsync();
                return project.FirstOrDefault(x => x.Id == id && x.Status == EProjectStatus.Active);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                var project = await _iProjectRepository.SearchOneAsync(true, x => x.Id == id && x.Status == EProjectStatus.Active);
                if (project != null)
                {
                    var stationResource = await _iStationResourceRepository.SearchOneAsync(true, x => x.StationId == id);
                    if (stationResource != null)
                    {
                        project.Name = stationResource.Name;
                    }
                }
                return project;
            }
        }

        public virtual async Task<TicketPrice> TicketPriceGetById(int id)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.ProjectGetById";
            try
            {
                var datas = await TicketPriceSearchAsync();
                return datas.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return await _iTicketPriceRepository.SearchOneAsync(true, x => x.Id == id);
            }
        }

        public virtual async Task<decimal> TicketPriceGetDefaultPrice(int projectId)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.TicketPriceGetDefaultPrice";
            try
            {
                var datas = await TicketPriceSearchAsync();
                var data = datas.FirstOrDefault(x => x.ProjectId == projectId && x.TicketType == ETicketType.Block && x.IsDefault)?.TicketValue;
                return data ?? 5000;
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return (await _iTicketPriceRepository.SearchOneAsync(true, x => x.ProjectId == projectId && x.TicketType == ETicketType.Block && x.IsDefault))?.TicketValue ?? 5000;
            }
        }

        public virtual async Task<List<Station>> StationSearchAsync()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.StationSearchAsync";
            var stations = new List<Station>();
            var stationResource = new List<StationResource>();
            var stationIds = new List<int>();
            try
            {
                if (!GetValue(ECacheType.Stations, AppHttpContext.AppLanguage, out stations))
                {
                    stations = await _iStationRepository.SearchAsync(x => x.Status == ESationStatus.Active);
                    if (AppHttpContext.AppLanguage != "vi")
                    {
                        stationIds = stations.Select(x => x.Id).ToList();
                        stationResource = await _iStationResourceRepository.SearchAsync(x => stationIds.Contains(x.StationId));
                        foreach (var item in stations)
                        {
                            var res = stationResource.FirstOrDefault(x => x.Language == AppHttpContext.AppLanguage && x.StationId == item.Id);
                            item.Name = res == null ? item.Name : res.Name;
                            item.Address = res == null ? item.Address : res.Address;
                            item.DisplayName = res == null ? item.DisplayName : res.DisplayName;
                        }
                    }
                    SetValue(ECacheType.Stations, AppHttpContext.AppLanguage, stations, 1000 * 60 * 5);
                }
                return stations;
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                stations = await _iStationRepository.SearchAsync(x => x.Status == ESationStatus.Active);
                if (AppHttpContext.AppLanguage != "vi")
                {
                    stationIds = stations.Select(x => x.Id).ToList();
                    stationResource = await _iStationResourceRepository.SearchAsync(x => stationIds.Contains(x.StationId));
                    foreach (var item in stations)
                    {
                        var res = stationResource.FirstOrDefault(x => x.Language == AppHttpContext.AppLanguage && x.StationId == item.Id);
                        item.Name = res == null ? item.Name : res.Name;
                        item.Address = res == null ? item.Address : res.Address;
                        item.DisplayName = res == null ? item.DisplayName : res.DisplayName;
                    }
                }
                return stations;
            }
        }

        public virtual async Task<List<TicketPrice>> TicketPriceSearchAsync()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.TicketPriceSearchAsync";
            var ticketPrices = new List<TicketPrice>();
            try
            {
                if (!GetValue(ECacheType.TicketPrices, out ticketPrices))
                {
                    ticketPrices = await _iTicketPriceRepository.SearchAsync(x => x.ProjectId > 0);
                    SetValue(ECacheType.TicketPrices, ticketPrices, 1000 * 60 * 5);
                }
                return ticketPrices;
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                ticketPrices = await _iTicketPriceRepository.SearchAsync(x => x.ProjectId > 0);
                return ticketPrices;
            }
        }

        public virtual async Task<List<Project>> ProjectSearchAsync()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.ProjectSearchAsync";
            var projects = new List<Project>();
            var projectResources = new List<ProjectResource>();
            var projectIds = new List<int>();
            try
            {
                if (!GetValue(ECacheType.Project, AppHttpContext.AppLanguage, out projects))
                {
                    projects = await _iProjectRepository.SearchAsync(x => x.Status == EProjectStatus.Active);
                    foreach (var item in projects)
                    {
                        if (string.IsNullOrEmpty(item.CityAreaData))
                        {
                            continue;
                        }
                        item.ListCityAreaData = new List<LatLngCityAreaDataModel>();
                        var arrayData = item.CityAreaData.Replace("\n", "").Replace("\r", "").Trim('[', ']').Split("],").Select(x => x.Replace("[", "").Replace("]", "").Trim()).ToArray();
                        foreach (var dataItem in arrayData)
                        {
                            try
                            {
                                if (string.IsNullOrEmpty(dataItem))
                                {
                                    continue;
                                }
                                var itemSpl = dataItem.Split(",").ToArray();
                                item.ListCityAreaData.Add(new LatLngCityAreaDataModel { Latitude = double.Parse(itemSpl[1].Trim(), CultureInfo.GetCultureInfo("en-US")), Longitude = double.Parse(itemSpl[0].Trim(), CultureInfo.GetCultureInfo("en-US")) });
                            }
                            catch
                            {
                                continue;
                            }
                        }
                        item.CityAreaData = "[]";
                    }
                    if (AppHttpContext.AppLanguage != "vi")
                    {
                        projectIds = projects.Select(x => x.Id).ToList();
                        projectResources = await _iProjectResourceRepository.SearchAsync(x => projectIds.Contains(x.ProjectId));
                        foreach (var item in projects)
                        {
                            var res = projectResources.FirstOrDefault(x => x.Language == AppHttpContext.AppLanguage && x.ProjectId == item.Id);
                            item.Name = res == null ? item.Name : res.Name;
                        }
                    }
                    SetValue(ECacheType.Project, AppHttpContext.AppLanguage, projects, 1000 * 60 * 15);
                }
                return projects;
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                projects = await _iProjectRepository.SearchAsync(x => x.Status == EProjectStatus.Active);
                foreach (var item in projects)
                {
                    if (string.IsNullOrEmpty(item.CityAreaData))
                    {
                        continue;
                    }
                    item.ListCityAreaData = new List<LatLngCityAreaDataModel>();
                    var arrayData = item.CityAreaData.Replace("\n", "").Replace("\r", "").Trim('[', ']').Split("],").Select(x => x.Replace("[", "").Replace("]", "").Trim()).ToArray();
                    foreach (var dataItem in arrayData)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(dataItem))
                            {
                                continue;
                            }
                            var itemSpl = dataItem.Split(",").ToArray();
                            item.ListCityAreaData.Add(new LatLngCityAreaDataModel { Latitude = double.Parse(itemSpl[1].Trim(), CultureInfo.GetCultureInfo("en-US")), Longitude = double.Parse(itemSpl[0].Trim(), CultureInfo.GetCultureInfo("en-US")) });
                        }
                        catch
                        {
                            continue;
                        }
                    }
                    item.CityAreaData = "[]";
                }
                if (AppHttpContext.AppLanguage != "vi")
                {
                    projectIds = projects.Select(x => x.Id).ToList();
                    projectResources = await _iProjectResourceRepository.SearchAsync(x => projectIds.Contains(x.ProjectId));
                    foreach (var item in projects)
                    {
                        var res = projectResources.FirstOrDefault(x => x.Language == AppHttpContext.AppLanguage && x.ProjectId == item.Id);
                        item.Name = res == null ? item.Name : res.Name;
                        item.CityAreaData = "[]";
                    }
                }
                return projects;
            }
        }

        public virtual async Task<ProjectAccountModel> LatLngToCustomerGroup(string lat, string lng, int accountId)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.LatLngToCustomerGroup";
            var newData = new ProjectAccountModel();
            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"KH gửi tọa độ {lat}, {lng}", false);
            try
            {
                if (lat == "0" || lng == "0" || string.IsNullOrEmpty(lat) || string.IsNullOrEmpty(lng) || lat?.ToLower() == "null".ToLower() || lng?.ToLower() == "null".ToLower())
                {
                    if (accountId > 0)
                    {
                        var accountLocation = await _iAccountRepository.GetCurrentLocation(accountId);
                        if (accountLocation != null)
                        {
                            AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Lấy tọa độ trong DB {accountLocation.Lat}, {accountLocation.Lng}", false);
                            newData = new ProjectAccountModel()
                            {
                                Lat = accountLocation.Lat,
                                Lng = accountLocation.Lng
                            };
                        }
                        else
                        {
                            var dataOut = new ProjectAccountModel
                            {
                                ProjectId = 1,
                                CustomerGroupId = 9,
                                LocationProjectOverwrite = true
                            };
                            AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Data trả về {JsonConvert.SerializeObject(dataOut)}", false);
                            return dataOut;
                        }
                    }
                    else
                    {
                        var dataOut = new ProjectAccountModel
                        {
                            ProjectId = 1,
                            CustomerGroupId = 9,
                            LocationProjectOverwrite = true
                        };
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Data trả về {JsonConvert.SerializeObject(dataOut)}", false);
                        return dataOut;
                    }
                }
                else
                {
                    newData = new ProjectAccountModel()
                    {
                        Lat = Convert.ToDouble(lat),
                        Lng = Convert.ToDouble(lng)
                    };
                }

                var listProject = await ProjectSearchAsync();
                var customerGroup = await CustomerGroupSearchAsync();
                foreach (var item in listProject)
                {
                    item.DistanceToProject = Function.DistanceInMeter(newData.Lat, newData.Lng, item.Lat ?? 0, item.Lng ?? 0);
                }
                listProject = listProject.OrderBy(x => x.DistanceToProject).ToList();
                foreach (var item in listProject)
                {
                    if (IsPointInPolygon(new LatLngCityAreaDataModel { Latitude = newData.Lat, Longitude = newData.Lng }, item.ListCityAreaData))
                    {
                        newData.ProjectId = item.Id;
                        newData.CustomerGroupId = customerGroup.FirstOrDefault(x => x.ProjectId == item.Id && x.IsDefault)?.Id ?? 0;
                        newData.LocationProjectOverwrite = false;
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Data trả về {JsonConvert.SerializeObject(newData)}", false);
                        return newData;
                    }
                }
                newData.ProjectId = listProject.FirstOrDefault()?.Id ?? 0;
                newData.CustomerGroupId = customerGroup.FirstOrDefault(x => x.ProjectId == newData.ProjectId && x.IsDefault)?.Id ?? 0;
                newData.LocationProjectOverwrite = true;
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Data trả về {JsonConvert.SerializeObject(newData)}", false);
                return newData;
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return newData;
            }
        }

        private bool IsPointInPolygon(LatLngCityAreaDataModel p, List<LatLngCityAreaDataModel> polygon)
        {
            if (polygon == null || polygon.Count <= 0)
            {
                return false;
            }
            double minX = polygon[0].Latitude;
            double maxX = polygon[0].Latitude;
            double minY = polygon[0].Longitude;
            double maxY = polygon[0].Longitude;
            for (int i = 1; i < polygon.Count; i++)
            {
                LatLngCityAreaDataModel q = polygon[i];
                minX = Math.Min(q.Latitude, minX);
                maxX = Math.Max(q.Latitude, maxX);
                minY = Math.Min(q.Longitude, minY);
                maxY = Math.Max(q.Longitude, maxY);
            }
            if (p.Latitude < minX || p.Latitude > maxX || p.Longitude < minY || p.Longitude > maxY)
            {
                return false;
            }
            // https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html
            bool inside = false;
            for (int i = 0, j = polygon.Count - 1; i < polygon.Count; j = i++)
            {
                if ((polygon[i].Longitude > p.Longitude) != (polygon[j].Longitude > p.Longitude) &&
                     p.Latitude < (polygon[j].Latitude - polygon[i].Latitude) * (p.Longitude - polygon[i].Longitude) / (polygon[j].Longitude - polygon[i].Longitude) + polygon[i].Latitude)
                {
                    inside = !inside;
                }
            }
            return inside;
        }

        public virtual async Task<List<CustomerGroup>> CustomerGroupSearchAsync()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.CustomerGroupSearchAsync";
            var customerGroups = new List<CustomerGroup>();
            var customerGroupsResources = new List<CustomerGroupResource>();
            var customerGroupIds = new List<int>();
            try
            {
                if (!GetValue(ECacheType.CustomerGroup, AppHttpContext.AppLanguage, out customerGroups))
                {
                    customerGroups = await _iCustomerGroupRepository.SearchAsync(x => x.Status == ECustomerGroupStatus.Active);
                    if (AppHttpContext.AppLanguage != "vi")
                    {
                        customerGroupIds = customerGroups.Select(x => x.Id).ToList();
                        customerGroupsResources = await _iCustomerGroupResourceRepository.SearchAsync(x => customerGroupIds.Contains(x.CustomerGroupId));
                        foreach (var item in customerGroups)
                        {
                            var res = customerGroupsResources.FirstOrDefault(x => x.Language == AppHttpContext.AppLanguage && x.CustomerGroupId == item.Id);
                            item.Name = res == null ? item.Name : res.Name;
                        }
                    }
                    SetValue(ECacheType.CustomerGroup, AppHttpContext.AppLanguage, customerGroups, 1000 * 60 * 10);
                }
                return customerGroups;
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                customerGroups = await _iCustomerGroupRepository.SearchAsync(x => x.Status == ECustomerGroupStatus.Active);
                if (AppHttpContext.AppLanguage != "vi")
                {
                    customerGroupIds = customerGroups.Select(x => x.Id).ToList();
                    customerGroupsResources = await _iCustomerGroupResourceRepository.SearchAsync(x => customerGroupIds.Contains(x.CustomerGroupId));
                    foreach (var item in customerGroups)
                    {
                        var res = customerGroupsResources.FirstOrDefault(x => x.Language == AppHttpContext.AppLanguage && x.CustomerGroupId == item.Id);
                        item.Name = res == null ? item.Name : res.Name;
                    }
                }
                return customerGroups;
            }
        }

        public virtual async Task<List<BannerGroup>> BannerGroupAsync()
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "MemoryCacheService.BannerGroupAsync";
            var banerGroups = new List<BannerGroup>();
            try
            {
                if (!GetValue(ECacheType.BannerGroup, AppHttpContext.AppLanguage, out banerGroups))
                {
                    banerGroups = await _iBannerGroupRepository.SearchAsync(x => x.Status == EBannerGroupStatus.Active);
                    var groupIds = banerGroups.Select(x => x.Id).ToList();
                    var banners = await _iBannerRepository.SearchAsync(x => groupIds.Contains(x.BannerGroupId) && x.Status == EBannerStatus.Active);
                    foreach (var item in banerGroups)
                    {
                        item.Banners = banners.Where(x => x.BannerGroupId == item.Id).ToList();
                        var bannerForNews = item.Banners.Where(x => x.Type == EBannerType.DEPLink).ToList();
                        if (bannerForNews.Any())
                        {
                            var newsId = bannerForNews.Select(x => Convert.ToInt32(string.IsNullOrEmpty(x.Data) ? 0 : x.Data)).ToList();
                            var news = await _iNewsRepository.SearchAsync(x => newsId.Contains(x.Id));
                            foreach (var b in item.Banners.Where(x => x.Type == EBannerType.DEPLink))
                            {
                                b.DataObject = news.FirstOrDefault(x => x.Id == Convert.ToInt32(string.IsNullOrEmpty(b.Data) ? 0 : b.Data));
                            }
                        }
                    }
                    SetValue(ECacheType.BannerGroup, AppHttpContext.AppLanguage, banerGroups, 1000 * 60 * 10);
                }
                return banerGroups;
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                banerGroups = await _iBannerGroupRepository.SearchAsync(x => x.Status == EBannerGroupStatus.Active && x.Language == AppHttpContext.AppLanguage);
                var groupIds = banerGroups.Select(x => x.Id).ToList();
                var banners = await _iBannerRepository.SearchAsync(x => groupIds.Contains(x.BannerGroupId) && x.Status == EBannerStatus.Active);
                foreach (var item in banerGroups)
                {
                    item.Banners = banners.Where(x => x.BannerGroupId == item.Id).ToList();
                    var bannerForNews = item.Banners.Where(x => x.Type == EBannerType.DEPLink).ToList();
                    if (bannerForNews.Any())
                    {
                        var newsId = bannerForNews.Select(x => Convert.ToInt32(string.IsNullOrEmpty(x.Data) ? 0 : x.Data)).ToList();
                        var news = await _iNewsRepository.SearchAsync(x => newsId.Contains(x.Id));
                        foreach (var b in item.Banners.Where(x => x.Type == EBannerType.DEPLink))
                        {
                            b.DataObject = news.FirstOrDefault(x => x.Id == Convert.ToInt32(string.IsNullOrEmpty(b.Data) ? 0 : b.Data));
                        }
                    }
                }
                return banerGroups;
            }
        }

        public bool GetValue<T>(string type, out T t)
        {
            try
            {
                if (_baseSettings.EnableMemoryCache)
                {
                    return _memoryCache.TryGetValue(type, out t);
                }
                else
                {
                    t = default;
                    return false;
                }
            }
            catch
            {
                t = default;
                return false;
            }

        }

        public bool GetValue<T>(ECacheType type, out T t)
        {
            try
            {
                if (_baseSettings.EnableMemoryCache)
                {
                    return _memoryCache.TryGetValue(type.ToString(), out t);
                }
                else
                {
                    t = default;
                    return false;
                }
            }
            catch
            {
                t = default;
                return false;
            }

        }

        public void SetValue<T>(string type, T t, int millisecondsExpiration = 1000)
        {
            try
            {
                if (t != null)
                {
                    _memoryCache.Set(type, t, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMilliseconds(millisecondsExpiration)));
                }
            }
            catch { }
        }

        public void SetValue<T>(ECacheType type, T t, int millisecondsExpiration = 1000)
        {
            try
            {
                if (t != null)
                {
                    _memoryCache.Set(type.ToString(), t, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMilliseconds(millisecondsExpiration)));
                }
            }
            catch { }
        }

        public void SetValue<T>(ECacheType type, string language, T t, int millisecondsExpiration = 1000)
        {
            try
            {
                if (t != null)
                {
                    _memoryCache.Set($"{type}_{language}", t, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMilliseconds(millisecondsExpiration)));
                }
            }
            catch { }
        }

        public bool GetValue<T>(ECacheType type, string language, out T t)
        {
            try
            {
                if (_baseSettings.EnableMemoryCache)
                {
                    return _memoryCache.TryGetValue($"{type}_{language}", out t);
                }
                else
                {
                    t = default;
                    return false;
                }
            }
            catch
            {
                t = default;
                return false;
            }

        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false, string deviceKey = null)
        {
            Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, deviceKey, isException)).ConfigureAwait(false);
        }
        #endregion
    }
}
