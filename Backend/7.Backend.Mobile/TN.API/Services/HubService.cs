﻿using Microsoft.Extensions.Options;
using PT.Domain.Model;
using System;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.API.Services
{
    public interface IHubService : IService<Account>
    {
        Task EventSend(
            EEventSystemType type,
            EEventSystemWarningType warningType,
            int accountId,
            string transactionCode,
            int projectId,
            int stationId,
            int bikeId,
            int dockId,
            string content, string datas = null);
    }
   
    public class HubService : IHubService
    {
        private readonly IAccountRepository _iAccountRepository;
        private readonly LogSettings _logSettings;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly IMongoDBRepository _mongoDBRepository;
        public HubService(
            IAccountRepository iAccountRepository,
            IOptions<LogSettings> logSettings,
            IMongoDBRepository iMongoDBRepository,
            IMongoDBRepository mongoDBRepository
            )
        {
            _iAccountRepository = iAccountRepository;
            _logSettings = logSettings.Value;
            _iMongoDBRepository = iMongoDBRepository;
            _mongoDBRepository = mongoDBRepository;
        }

        public async Task EventSend(
            EEventSystemType type, 
            EEventSystemWarningType warningType,  
            int accountId, 
            string transactionCode, 
            int projectId, 
            int stationId, 
            int bikeId, 
            int dockId, 
            string content,
            string datas = null
            )
        {
            try
            {
                if (accountId > 0)
                {
                    var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                    if (account != null)
                    {
                        content += $" | khách hàng {account.Code} {account.Phone} {account.FullName}";
                    }
                }

                await _mongoDBRepository.EventSystemAddAsync(_logSettings, new EventSystemMongo
                {
                    AccountId = accountId,
                    TransactionCode = transactionCode,
                    BikeId = bikeId,
                    Content = $"{content}",
                    CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                    DockId = dockId,
                    IsMultiple = false,
                    ProjectId = projectId,
                    StationId = stationId,
                    Title = type.GetDisplayName(),
                    Name = type.GetDisplayName(),
                    UserId = 0,
                    Type = type,
                    WarningType = warningType,
                    RoleType = RoleManagerType.Default,
                    UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                    IsJob = true,
                    Datas = datas,
                    Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}"),
                    CreatedDateTicks = DateTime.Now.Ticks,
                    UpdatedDateTicks = DateTime.Now.Ticks,
                    IsFlag = false,
                    IsProcessed = false,
                    ParentId = 0,
                    Id = long.Parse($"{DateTime.Now:yyyyMMddHHmmssffff}")
                });
            }
            catch (Exception ex)
            {
                AddLog(0, "FireBaseService.EventSend", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"Gửi notify system => lỗi {ex}", true);
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
        }
        #endregion
    }
}