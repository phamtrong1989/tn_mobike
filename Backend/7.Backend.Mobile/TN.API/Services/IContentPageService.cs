﻿using Microsoft.Extensions.Options;
using TN.Domain.Model;
using System;
using System.Threading.Tasks;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using PT.Domain.Model;
using TN.API.Model;

namespace TN.API.Services
{
    public interface IContentPageService : IService<Account>
    {
        Task<ApiResponse<object>> TermsOfUse();
        Task<ApiResponse<object>> TentalAgreement();
        Task<ApiResponse<object>> Introduction();
    }

    public class ContentPageService : IContentPageService
    {
        private readonly IContentPageRepository _iContentPageRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IMemoryCacheService _memoryCacheService;
        public ContentPageService(IContentPageRepository iContentPageRepository, IMongoDBRepository iMongoDBRepository, IOptions<LogSettings> logSettings, IMemoryCacheService memoryCacheService)
        {
            _iContentPageRepository = iContentPageRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _memoryCacheService = memoryCacheService;
        }

        public virtual async Task<ApiResponse<object>> TermsOfUse()
        {
            try
            {
                var langId = await _memoryCacheService.CurrentLanguageId();
                var data = await _iContentPageRepository.SearchOneAsync(true, x => x.Type == EContentPageType.TermsOfUse && x.LanguageId == langId);
                if(data!=null)
                {
                    data.Content = $"<div style='padding: 0 20px 0 20px;'>{data.Content}</div>";
                }
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                AddLog(0, "ContentPageService.TermsOfUse", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> TentalAgreement()
        {
            try
            {
                var langId = await _memoryCacheService.CurrentLanguageId();
                var data = await _iContentPageRepository.SearchOneAsync(true, x => x.Type == EContentPageType.TentalAgreement && x.LanguageId == langId);
                if (data != null)
                {
                    data.Content = $"<div style='padding: 0 20px 0 20px;'>{data.Content}</div>";
                }
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                AddLog(0, "ContentPageService.TermsOfUse", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> Introduction()
        {
            try
            {
                var langId = await _memoryCacheService.CurrentLanguageId();
                var data = await _iContentPageRepository.SearchOneAsync(true, x => x.Type == EContentPageType.Introduction && x.LanguageId == langId);
                if (data != null)
                {
                    data.Content = $"<div style='padding: 0 20px 0 20px;'>{data.Content}</div>";
                }
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                AddLog(0, "ContentPageService.Introduction", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
        #endregion
    }
}
