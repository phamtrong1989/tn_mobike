﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TN.API.Model;
using TN.Utility;

namespace TN.API.Services
{
    public class PaymentApiService : RestApi
    {
        public PaymentApiService(string baseAddress, int timeout) : base(baseAddress, timeout)
        {
        }
        public async Task<PaymentResponseDTO<PaymentHistoryResponseDTO>> Deposit(PaymentDepositCommand info, CancellationToken ctoken = default)
        {
            return await SendRequestAsync<PaymentResponseDTO<PaymentHistoryResponseDTO>>(HttpMethod.Post, $"/api/payment/deposit", info, ctoken);
        }
        public async Task<PaymentResponseDTO<object>> Order(PaymentOrderCommand info, CancellationToken ctoken = default(CancellationToken))
        {
            return await SendRequestAsync<PaymentResponseDTO<object>>(HttpMethod.Post, $"/api/Payment/pay", info, ctoken);
        }
        public async Task<PaymentResponseDTO<object>> Balance(PaymentQueryBalanceCommand info, CancellationToken ctoken = default(CancellationToken))
        {
            return await SendRequestAsync<PaymentResponseDTO<object>>(HttpMethod.Post, $"/api/Payment/cmd", info, ctoken);
        }
        public async Task<PaymentResponseDTO<List<PaymentHistoryResponseDTO>>> History(PaymentHistoryCommand info, CancellationToken ctoken = default)
        {
            return await SendRequestAsync<PaymentResponseDTO<List<PaymentHistoryResponseDTO>>>(HttpMethod.Post, $"/api/Payment/cmd", info, ctoken);
        }
    }
}
