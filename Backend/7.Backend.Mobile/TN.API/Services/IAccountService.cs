﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;
using static TN.Domain.Model.AccountOTP;

namespace TN.API.Services
{
    public interface IAccountService : IService<Account>
    {
        Task<ApiResponse<object>> Login(string username, string password, HttpRequest request);
        Task<ApiResponse<object>> Register(AccountRegisterCommand model, HttpRequest request);
        Task<ApiResponse<object>> RecoveryPassword(AccountRecoveryPasswordCommand model);
        Task<ApiResponse<object>> UpdateProfile(AccountUpdateProfileCommand model, int accountId, HttpRequest request);
        Task<ApiResponse<object>> UploadAvatar(AccountUploadAvatarCommand model, int accountId, HttpRequest request);
        Task<ApiResponse<object>> OTPRegister(OTPType type, AccountOTPRegisterCommand model);
        Task<ApiResponse<object>> ConfirmOTPRegister(OTPType type, AccountConfirmOTPRegisterCommand model);
        Task<ApiResponse<object>> Logout(AccountLogoutCommand model);
        Task<ApiResponse<object>> Profile(int userId, HttpRequest request);
        Task<ApiResponse<object>> CheckEmail(string email);
        Task<ApiResponse<object>> CheckOldPassword(string phone, string oldPassword);
        Task<ApiResponse<object>> ChangePassword(string phone, AccountChangePasswordCommand model);
        Task<ApiResponse<object>> ProfileVerify(AccountUpdatProfileVerifyCommand model, int accountId, HttpRequest request);
        Task<ApiResponse<object>> CheckShareCode(string shareCode);
        Task<ApiResponse<object>> UpdateDeviceToken(int accountId, string token, HttpRequest request);
        Task<ApiResponse<object>> ChangeLanguage(int accountId, string language);
    }

    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IOptions<BaseSetting> _baseSetting;
        private readonly IAccountOTPRepository _iAccountOTPRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly ISMSRepository _iSMSRepository;
        private readonly IFireBaseService _iFireBaseService;
        private readonly ZaloSettings _zaloSettings;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IHubService _iHubService;
        private readonly IMemoryCacheService _memoryCache;

        public AccountService(IAccountRepository accountRepository,
                            IOptions<BaseSetting> baseSetting,
                            IAccountOTPRepository iAccountOTPRepository,
                            IWalletTransactionRepository iWalletTransactionRepository,
                            IWalletRepository iWalletRepository,
                            ISMSRepository iSMSRepository,
                            IFireBaseService iFireBaseService,
                            IOptions<ZaloSettings> zaloSettings,
                            IMongoDBRepository iMongoDBRepository,
                            IOptions<LogSettings> logSettings,
                            IHubService iHubService,
                            IMemoryCacheService memoryCache
            )
        {
            _accountRepository = accountRepository;
            _baseSetting = baseSetting;
            _iAccountOTPRepository = iAccountOTPRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iWalletRepository = iWalletRepository;
            _iSMSRepository = iSMSRepository;
            _iFireBaseService = iFireBaseService;
            _zaloSettings = zaloSettings.Value;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _iHubService = iHubService;
            _memoryCache = memoryCache;
        }

        public async Task<ApiResponse<object>> OTPRegister(OTPType type, AccountOTPRegisterCommand model)
        {
            try
            {
                var isAccount = await _accountRepository.AnyAsync(m => m.Phone == model.Phone.Trim());
                if (type == OTPType.Register && isAccount)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.PhoneNumberExist);
                }
                else if (type == OTPType.RecoveryPassword && !isAccount)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.AccountNotExist);
                }

                if (!(await CheckTimeOutOTP(model.Phone.Trim(), type)))
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.OTPSendTimeOut);
                }

                await SendOTPAsync(model.Phone.Trim(), type, model.Type ?? OTPSystemType.SMS);
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.OTPRegister", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> ConfirmOTPRegister(OTPType type, AccountConfirmOTPRegisterCommand model)
        {
            try
            {
                var isPhone = await _accountRepository.AnyAsync(m => m.Phone == model.Phone.Trim());
                if (type == OTPType.Register && isPhone)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.PhoneNumberExist);
                }
                else if (type == OTPType.RecoveryPassword && !isPhone)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.AccountNotExist);
                }
                var ktOTP = _iAccountOTPRepository.CheckConfigOTP(model.Phone, type);
                if (ktOTP == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.OTPOutTime);
                }
                else
                {
                    if (ktOTP.OTP == model.OTP && ktOTP.InvalidCount < _baseSetting.Value.OTPInvalidMax)
                    {
                        ktOTP.Status = true;
                        ktOTP.ConfirmDate = DateTime.Now;
                        _iAccountOTPRepository.Update(ktOTP);
                        await _iAccountOTPRepository.CommitAsync();
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                    else
                    {
                        if (ktOTP.InvalidCount < _baseSetting.Value.OTPInvalidMax)
                        {
                            await _iAccountOTPRepository.AddInvalid(ktOTP.Id);
                            // Đếm số lần sai OTP, sai 10 lần liên tiếp thì otp này sẽ bị khóa
                            return ApiResponse.WithStatus(false, null, ResponseCode.OTPInvalid, (_baseSetting.Value.OTPInvalidMax - ktOTP.InvalidCount).ToString());
                        }
                        else
                        {
                            _iAccountOTPRepository.DeleteWhere(x => x.Id == ktOTP.Id);
                            await _iAccountOTPRepository.CommitAsync();
                            return ApiResponse.WithStatus(false, null, ResponseCode.OTPInvalid, "0");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.ConfirmOTPRegister", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> CheckShareCode(string shareCode)
        {
            if (!string.IsNullOrEmpty(shareCode))
            {
                var account = await _accountRepository.SearchOneAsync(true, x => x.Code == shareCode.Trim());
                if (account == null)
                {
                    // Check xem người này có phải là nv VTS không
                    var ktnv = await _accountRepository.UserByCode(shareCode);
                    if (ktnv != null)
                    {
                        return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                    }
                    else
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.ShareCodeInvalid);
                    }
                }
                // Check tổng số tiền nạp
                var totalDeposit = await _iWalletRepository.TotalDepositAmount(account.Id);
                // Check account đã  xác thực chưa
                if (account.VerifyStatus != EAccountVerifyStatus.Ok || totalDeposit < _baseSetting.Value.MinTotalDepositAmount)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.ShareCodeInvalid);
                }
                else
                {
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }
            }
            else
            {
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
        }

        public async Task<ApiResponse<object>> Register(AccountRegisterCommand model, HttpRequest request)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            string functionName = "AccountService.Register";
            int ctvId = 0;
            int qlkhId = 0;

            try
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Model app gửi lên {Newtonsoft.Json.JsonConvert.SerializeObject(model)} ", false);
                if (model == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaoTaiKhoanThatBai);
                }
                AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Khách hàng số ĐT {model.Phone}, tên {model.FullName} đăng ký tài khoản", false);

                string imagePath = $"/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/avt_{model.Phone}.jpg";
                string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}{imagePath}";
                string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}{imagePath}";

                if (!Directory.Exists(Path.GetDirectoryName(pathAvatarServer)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(pathAvatarServer));
                }
                // Kiểm tra đã xác thực OTP chưa
                var ktOTP = _iAccountOTPRepository.CheckConfigOTPOKE(model.Phone, model.OTP, OTPType.Register);
                if (ktOTP == null)
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.End, ResponseCode.OTPOutTime.GetDisplayName(), false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.OTPOutTime);
                }
                // Kiểm tra số điện thoại này đã tồn tại chưa
                var isPhone = await _accountRepository.AnyAsync(m => m.Phone == model.Phone);
                if (isPhone)
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.End, ResponseCode.PhoneNumberExist.GetDisplayName(), false);
                    return ApiResponse.WithStatus(false, null, ResponseCode.PhoneNumberExist);
                }

                if (!string.IsNullOrEmpty(model.Email))
                {
                    // Kiểm tra Email này đã có người sử dụng chưa
                    var isEmail = await _accountRepository.AnyAsync(m => m.Email == model.Email);
                    if (isEmail)
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.End, ResponseCode.EmailExist.GetDisplayName(), false);
                        return ApiResponse.WithStatus(false, null, ResponseCode.EmailExist);
                    }
                }

                bool isShareCodeAccount = false;
                model.ShareCode = model.ShareCode?.Trim();
                // Xử lý shre Code 
                // Người giới thiệu sẽ được số điểm cấu hình
                if (!string.IsNullOrEmpty(model.ShareCode))
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Nhập ShareCode {model.ShareCode}", false);
                    //Kt nếu là share code của nhân viên
                    var ktnv = await _accountRepository.UserByCode(model.ShareCode);
                    if (ktnv != null)
                    {
                        if (ktnv.RoleType == RoleManagerType.CTV_VTS)
                        {
                            ctvId = ktnv.Id;
                            qlkhId = ktnv.ManagerUserId;
                        }
                        else
                        {
                            qlkhId = ktnv.Id;
                        }
                        isShareCodeAccount = true;
                    }
                    else
                    {
                        // tài khoản hưởng thụ
                        var accountShare = await _accountRepository.SearchOneAsync(false, x => x.Code == model.ShareCode);
                        if (accountShare != null)
                        {
                            var totalDeposit = await _iWalletRepository.TotalDepositAmount(accountShare.Id);
                            // Tài khoản chia sẻ nếu chưa được xác thực thì phải nạp tối thiểu 20k mới được cho người khác sử dụng mã chia sẻ
                            if (accountShare.VerifyStatus != EAccountVerifyStatus.Ok || totalDeposit < _baseSetting.Value.MinTotalDepositAmount)
                            {
                                return ApiResponse.WithStatus(false, null, ResponseCode.ShareCodeInvalid);
                            }
                            // Nạp điểm cho tk chia sẻ code
                            await WalletTransactionOwnerShareCode(functionName, requestId, model.ShareCode, accountShare.Id);
                            // Thông báo có người nạp mã của bạn thành công
                            await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountShare.Id, ENotificationTempType.CoNguoiDungMaChiaSeCuaBan, true, DockEventType.Not, null, model.Phone, Function.FormatMoney(_baseSetting.Value.ShareCodePrice));
                            isShareCodeAccount = true;
                        }
                        else
                        {
                            AddLog(0, functionName, requestId, EnumMongoLogType.End, $"ShareCode {model.ShareCode} không tồn tại", false);
                            return ApiResponse.WithStatus(false, null, ResponseCode.ShareCodeInvalid);
                        }
                    }
                }

                // Auto select ProjectId 
                var findProject = await _memoryCache.LatLngToCustomerGroup(model.Lat, model.Lng, 0);
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"ProjectId Location {JsonConvert.SerializeObject(findProject)}", false);

                // Thêm tài khoản
                var dataAdd = new Account
                {
                    Phone = model.Phone,
                    Username = model.Phone,
                    Email = model.Email,
                    Avatar = pathAvatarWeb,
                    LastName = model.FullName,
                    Sex = model.Sex ?? ESex.Khac,
                    Birthday = model.Birthday,
                    Status = EAccountStatus.Active,
                    Type = EAccountType.Normal,
                    CreatedDate = DateTime.Now,
                    Password = Function.HMACMD5Password(model.Password),
                    ShareCode = model.ShareCode,
                    CreatedSystemUserId = 0,
                    VerifyStatus = EAccountVerifyStatus.Not,
                    FullName = model.FullName,
                    ManagerUserId = qlkhId,
                    SubManagerUserId = ctvId,
                    LocationProjectId = findProject?.ProjectId,
                    LocationCustomerGroupId = findProject?.CustomerGroupId,
                    LocationProjectOverwrite = findProject?.LocationProjectOverwrite
                };
                await _accountRepository.AddAsync(dataAdd);
                await _accountRepository.CommitAsync();

                dataAdd.Code = $"KH{dataAdd.Id:D6}";
                _accountRepository.Update(dataAdd);
                await _accountRepository.CommitAsync();

                if (findProject.ProjectId > 0)
                {
                    // Gán project
                    await _accountRepository.ProjectAcountAdd(dataAdd.Id, findProject.ProjectId, findProject.CustomerGroupId);
                }    
                try
                {
                   await _iWalletRepository.AddDayExpirysByDirect(dataAdd.Id, 60, "Đăng ký tài khoản +60 ngày hạn sử dụng điểm khuyến mãi");
                }
                catch (Exception ex)
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi {ex}", true);
                }

                await _iAccountOTPRepository.SetConfigOTPDisabled(model.Phone, model.OTP, OTPType.Register);

                try
                {
                    var statusImage = Function.ByteArrayToImage(model.AvatarData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
                }
                catch (Exception ex)
                {
                    AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"Lỗi {ex}", true);
                }

                var exp = DateTime.Now.AddMinutes(_baseSetting.Value.JwtTokenExpiredMinute);
                var deviceId = GetDeviceId(dataAdd.Id, request);
                var token = CreateToken(dataAdd, exp, deviceId);
                await _accountRepository.AddDevice(dataAdd.Id, deviceId, token, DateTime.Now, exp, request.HttpContext.Connection.RemoteIpAddress.ToString());
                // Mã chia sẻ của tài khoản khách hàng khác mới được tặng điểm
                if (isShareCodeAccount)
                {
                    // Nạp điểm cho người nhập
                    await WalletTransactionAddShareCode(functionName, requestId, model.ShareCode, dataAdd.Id);
                    // Thông báo nhập mã giới thiệu thành công
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, dataAdd.Id, ENotificationTempType.NhapMaGioiThieu, true, DockEventType.Not, null, model.ShareCode, Function.FormatMoney(_baseSetting.Value.ShareCodePrice));
                }
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Tạo thành công tài khoản {dataAdd.Username}", false);
                // Thông báo cho QT
                await _iHubService.EventSend(EEventSystemType.M_4004_CoTaiKhoanMoiTao, EEventSystemWarningType.Info, dataAdd.Id, null, 0, 0, 0, 0, $"Khách hàng '{dataAdd.FullName}' SĐT '{dataAdd.Phone}' đăng ký tài khoản thành công");
                return ApiResponse.WithStatus(true, new AccountDTO(dataAdd, token, deviceId), ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private async Task WalletTransactionOwnerShareCode(string functionName, string requestId, string shareCode, int accountShareId)
        {
            // Thưởng điểm tặng cho người có mã share
            var vi = await _iWalletRepository.GetWalletAsync(accountShareId);
            var wt = new WalletTransaction
            {
                CampaignId = 0,
                Status = EWalletTransactionStatus.Done,
                TransactionCode = $"{shareCode}",
                AccountId = accountShareId,
                Amount = 0,
                TransactionId = 0,
                WalletId = vi.Id,
                Type = EWalletTransactionType.OwnerShareCode,
                CreatedDate = DateTime.Now,
                OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.OwnerShareCode),
                IsAsync = true,
                SubAmount = _baseSetting.Value.OwnerShareCodePrice,
                TotalAmount = _baseSetting.Value.OwnerShareCodePrice,
                DepositEvent_SubAmount = 0,
                IsWarningHistory = false,
                PadTime = DateTime.Now,
                ReqestId = requestId,
                Wallet_Balance = vi.Balance,
                Wallet_SubBalance = vi.SubBalance,
                Wallet_HashCode = vi.HashCode,
                Wallet_TripPoint = vi.TripPoint
            };
            wt.HashCode = Function.TransactionHash(wt.OrderIdRef, wt.Amount, wt.SubAmount, wt.TripPoint, _baseSetting.Value.TransactionSecretKey);
            await _iWalletTransactionRepository.AddAsync(wt);
            await _iWalletTransactionRepository.CommitAsync();
            await _iWalletRepository.UpdateAddWalletAsync(accountShareId, 0, _baseSetting.Value.OwnerShareCodePrice, 0, _baseSetting.Value.TransactionSecretKey, true, true, 0);
            AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"AccountId: {accountShareId} nhận thành công share code {_baseSetting.Value.OwnerShareCodePrice}", false);
        }

        private async Task WalletTransactionAddShareCode(string functionName, string requestId, string shareCode, int accountId)
        {
            // người nhập mã cũng được hưởng 5000
            var vi = await _iWalletRepository.GetWalletAsync(accountId);
            var wt = new WalletTransaction
            {
                CampaignId = 0,
                Status = EWalletTransactionStatus.Done,
                TransactionCode = $"{shareCode}",
                AccountId = accountId,
                Amount = 0,
                TransactionId = 0,
                WalletId = vi.Id,
                Type = EWalletTransactionType.AddShareCode,
                CreatedDate = DateTime.Now,
                OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.AddShareCode),
                IsAsync = true,
                SubAmount = _baseSetting.Value.ShareCodePrice,
                TotalAmount = _baseSetting.Value.ShareCodePrice,
                DepositEvent_SubAmount = 0,
                IsWarningHistory = false,
                PadTime = DateTime.Now,
                ReqestId = requestId,
                Wallet_Balance = vi.Balance,
                Wallet_SubBalance = vi.SubBalance,
                Wallet_HashCode = vi.HashCode,
                Wallet_TripPoint = vi.TripPoint
            };
            wt.HashCode = Function.TransactionHash(wt.OrderIdRef, wt.Amount, wt.SubAmount, wt.TripPoint, _baseSetting.Value.TransactionSecretKey);
            await _iWalletTransactionRepository.AddAsync(wt);
            await _iWalletTransactionRepository.CommitAsync();
            await _iWalletRepository.UpdateAddWalletAsync(accountId, 0, _baseSetting.Value.ShareCodePrice, 0, _baseSetting.Value.TransactionSecretKey, true, true, 0);
            AddLog(0, functionName, requestId, EnumMongoLogType.Center, $"AccountId: {accountId} nhập thành công share code {_baseSetting.Value.ShareCodePrice}", false);
        }

        private static string GetDeviceId(int accountId, HttpRequest request)
        {
            return $"Device{accountId:D6}";
        }

        public virtual async Task<ApiResponse<object>> UpdateProfile(AccountUpdateProfileCommand model, int accountId, HttpRequest request)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            string functionName = "AccountService.UpdateProfile";

            try
            {
                var account = await _accountRepository.SearchOneAsync(false, m => m.Id == accountId);
                if (account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.UsernameNotExist);
                }

                if (!string.IsNullOrEmpty(model.Email))
                {
                    var isEmail = await _accountRepository.AnyAsync(m => m.Id != accountId && m.Email == model.Email);
                    if (isEmail)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.EmailExist);
                    }
                }

                account.Email = model.Email;
                account.Sex = model.Sex;
                account.FullName = model.FullName;
                account.Birthday = model.Birthday;
                account.Address = model.Address;
                account.IdentificationID = model.IdentificationID;


                if (model.AvatarData != null && model.AvatarData.Count() > 0)
                {
                    string imagePath = $"/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/avt_{account.Phone}_{Guid.NewGuid()}.jpg";
                    string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}{imagePath}";
                    string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}{imagePath}";

                    if (!Directory.Exists(Path.GetDirectoryName(pathAvatarServer)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(pathAvatarServer));
                    }

                    try
                    {
                        Function.ByteArrayToImage(model.AvatarData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
                    }
                    catch (Exception ex)
                    {
                        AddLog(0, functionName, requestId, EnumMongoLogType.Start, $"Lỗi {ex}", true);
                    }
                    account.Avatar = pathAvatarWeb;
                }

                _accountRepository.Update(account);
                await _accountRepository.CommitAsync();
                return await Profile(accountId, request);
            }
            catch (Exception ex)
            {
                AddLog(0, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> ProfileVerify(AccountUpdatProfileVerifyCommand model, int accountId, HttpRequest request)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            string functionName = "AccountService.ProfileVerify";
            try
            {
                var account = await _accountRepository.SearchOneAsync(false, m => m.Id == accountId);
                if (account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.UsernameNotExist);
                }

                if (account.VerifyStatus == EAccountVerifyStatus.Ok)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.AccountDoesNotNeedVerifyOk);
                }

                if (account.VerifyStatus == EAccountVerifyStatus.Waiting)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.AccountDoesNotNeedVerifyWaiting);
                }

                account.IdentificationType = model.IdentificationType;
                account.VerifyStatus = EAccountVerifyStatus.Waiting;

                if (model.IdentificationPhotoFrontData != null && model.IdentificationPhotoFrontData.Count() > 0)
                {
                    string imagePath = $"/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/IdF_{account.Phone}_{Guid.NewGuid()}.jpg";
                    string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}/{imagePath}";
                    string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/{imagePath}";
                    if (!Directory.Exists(Path.GetDirectoryName(pathAvatarServer)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(pathAvatarServer));
                    }
                    try
                    {
                        Function.ByteArrayToImage(model.IdentificationPhotoFrontData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
                    }
                    catch (Exception ex)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Lỗi {ex}", true);
                    }
                    account.IdentificationPhotoFront = pathAvatarWeb;
                }

                if (model.IdentificationPhotoBacksideData != null && model.IdentificationPhotoBacksideData.Count() > 0)
                {
                    string imagePath = $"/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/Idb_{account.Phone}_{Guid.NewGuid()}.jpg";
                    string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}/{imagePath}";
                    string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/{imagePath}";
                    if (!Directory.Exists(Path.GetDirectoryName(pathAvatarServer)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(pathAvatarServer));
                    }
                    try
                    {
                        Function.ByteArrayToImage(model.IdentificationPhotoBacksideData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
                    }
                    catch (Exception ex)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Lỗi {ex}", true);
                    }
                    account.IdentificationPhotoBackside = pathAvatarWeb;
                }

                _accountRepository.Update(account);
                await _accountRepository.CommitAsync();

                return await Profile(accountId, request);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> UploadAvatar(AccountUploadAvatarCommand model, int accountId, HttpRequest request)
        {
            try
            {
                var account = await _accountRepository.SearchOneAsync(false, m => m.Id == accountId);
                if (account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidAccount);
                }
                string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/avt_{account.Phone}_{Guid.NewGuid()}.jpg";
                string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/avt_{account.Phone}_{Guid.NewGuid()}.jpg";

                if (!Directory.Exists(Path.GetDirectoryName(pathAvatarServer)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(pathAvatarServer));
                }

                account.Avatar = pathAvatarWeb;
                _accountRepository.Update(account);
                await _accountRepository.CommitAsync();
                var kt = Function.ByteArrayToImage(model.AvatarData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
                return ApiResponse.WithStatus(true, $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{pathAvatarWeb}", ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "AccountService.UploadAvatar", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> Profile(int accountId, HttpRequest request)
        {
            try
            {
                var account = await _accountRepository.SearchOneAsync(false, m => m.Id == accountId);
                if (account != null)
                {
                    var dataOut = new AccountDTO(account)
                    {
                        Avatar = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{account.Avatar}",
                        IdentificationPhotoFront = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{account.IdentificationPhotoFront}",
                        IdentificationPhotoBackside = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{account.IdentificationPhotoBackside}"
                    };
                    return ApiResponse.WithStatus(true, dataOut, ResponseCode.Ok);
                }
                else
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.UsernameNotExist);
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, "AccountService.Profile", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> Login(string username, string password, HttpRequest request)
        {
            try
            {
                int maxNumberWrongPasswords = 10;
                int lockLoginInMinutes = 5;
                var account = await _accountRepository.SearchOneAsync(false, x => x.Phone == username);
                if (account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidLogin);
                }

                if (account.ExpirationWrongPassword != null && account.ExpirationWrongPassword.Value >= DateTime.Now && account.NumberWrongPasswords >= maxNumberWrongPasswords)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.SaiMatKhauNhieuLanChoPhep, $"{account.ExpirationWrongPassword.Value.AddMinutes(1):dd/MM/yyyy HH:mm}", $"{maxNumberWrongPasswords}");
                }

                if (account.Status == EAccountStatus.Lock)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanDangBiKhoa);
                }    

                if (account.Password != Function.HMACMD5Password(password))
                {
                    account.NumberWrongPasswords = (account.NumberWrongPasswords ?? 0) + 1;
                    if (account.NumberWrongPasswords >= maxNumberWrongPasswords)
                    {
                        account.ExpirationWrongPassword = DateTime.Now.AddMinutes(lockLoginInMinutes);
                    }
                    _accountRepository.Update(account);
                    await _accountRepository.CommitAsync();
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidLogin);
                }
                else
                {
                    account.Language = $"{AppHttpContext.AppLanguage}";
                    var lang = await _memoryCache.LanguageByCodeAsync(account.Language);
                    account.NumberWrongPasswords = 0;
                    account.ExpirationWrongPassword = null;
                    account.LanguageId = lang?.Id ?? 0;
                    _accountRepository.Update(account);
                    await _accountRepository.CommitAsync();

                    var exp = DateTime.Now.AddMinutes(_baseSetting.Value.JwtTokenExpiredMinute);
                    var deviceId = GetDeviceId(account.Id, request);
                    var token = CreateToken(account, exp, deviceId);
                    // Lưu 1 device
                    await _accountRepository.AddDevice(account.Id, deviceId, token, DateTime.Now, exp, request.HttpContext.Connection.RemoteIpAddress.ToString());
                    var outData = new AccountDTO(account, token, deviceId)
                    {
                        IdentificationPhotoFront = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{account.IdentificationPhotoFront}",
                        IdentificationPhotoBackside = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{account.IdentificationPhotoBackside}",
                        Avatar = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{account.Avatar}"
                    };
                    await _iHubService.EventSend(EEventSystemType.M_4006_KhachHangDangNhap, EEventSystemWarningType.Info, account.Id, "", 0, 0, 0, 0, $"Khách hàng đăng nhập thành công'");
                    return ApiResponse.WithStatus(true, outData, ResponseCode.Ok);
                }
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.Login", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> UpdateDeviceToken(int accountId, string token, HttpRequest request)
        {
            try
            {
                AddLog(accountId, "AccountService.UpdateDeviceToken", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Update token {token}", false);
                var deviceId = GetDeviceId(accountId, request);
                await _accountRepository.UpdateDeviceToken(accountId, deviceId, token);
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "AccountService.UpdateDeviceToken", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(true, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> ChangeLanguage(int accountId, string language)
        {
            try
            {
                var lang = await _memoryCache.LanguageByCodeAsync(language);
                await _accountRepository.UpdateLanguage(accountId, language, lang?.Id ?? 2);
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "AccountService.ChangeLanguage", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> Logout(AccountLogoutCommand model)
        {
            try
            {
                var kt = await _accountRepository.SearchOneAsync(false, m => m.Username == model.Username || m.Phone == model.Username);
                if (kt == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.UsernameNotExist);
                }
                else
                {
                    await _iHubService.EventSend(EEventSystemType.M_4007_KhachHangDangXuat, EEventSystemWarningType.Info, kt.Id, "", 0, 0, 0, 0, $"Khách hàng đăng xuất thành công'");
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.Logout", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> RecoveryPassword(AccountRecoveryPasswordCommand model)
        {
            try
            {
                // Kiểm tra đã xác thực OTP chưa
                var ktOTP = _iAccountOTPRepository.CheckConfigOTPOKE(model.Phone, model.OTP, OTPType.RecoveryPassword);
                if (ktOTP == null || (ktOTP != null && ktOTP.OTP != model.OTP))
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.OTPOutTime);
                }
                var account = await _accountRepository.SearchOneAsync(false, x => x.Phone == model.Phone);
                if (account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidAccount);
                }
                account.Password = Function.HMACMD5Password(model.NewPassword);
                account.PasswordSalt = Function.SignSHA256(model.NewPassword, _baseSetting.Value.TransactionSecretKey);
                _accountRepository.Update(account);
                await _accountRepository.CommitAsync();
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.RecoveryPassword", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> ChangePassword(string phone, AccountChangePasswordCommand model)
        {
            try
            {
                var account = await _accountRepository.Login(phone, Function.HMACMD5Password(model.OldPassword));
                if (account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidOldPassword);
                }
                account.Password = Function.HMACMD5Password(model.Password);
                account.PasswordSalt = Function.SignSHA256(model.Password, _baseSetting.Value.TransactionSecretKey);
                _accountRepository.Update(account);
                await _accountRepository.CommitAsync();
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.ChangePassword", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> CheckOldPassword(string phone, string oldPassword)
        {
            try
            {
                var account = await _accountRepository.Login(phone, Function.HMACMD5Password(oldPassword));
                if (account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidOldPassword);
                }
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.CheckOldPassword", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        #region [Functions]
        private string CreateToken(Account account, DateTime expires, string deviceId)
        {
            if (account == null)
            {
                return null;
            }

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sid, account.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, account.Username),
                new Claim("Phone", account.Phone ?? account.Username),
                new Claim("FullName", account.FullName ?? account.FullName),
                new Claim("Language", account.Language ?? "vi"),
                new Claim("RFID", account.RFID ?? ""),
                new Claim("DeviceId", deviceId),
                new Claim("Type", ((byte)account.Type).ToString()),
                new Claim("Expires", $"{expires:yyyy-MM-dd HH:mm:ss}"),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_baseSetting.Value.SecurityKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: _baseSetting.Value.API_ValidIssuer,
                audience: _baseSetting.Value.API_ValidAudience,
                claims: claims,
                expires: expires,
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<ApiResponse<object>> CheckEmail(string email)
        {
            try
            {
                var account = await _accountRepository.SearchOneAsync(true, m => m.Email == email);
                if (account != null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.EmailExist);
                }
                else
                {
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.CheckEmail", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private static int RandomNumber()
        {
            var random = new Random();
            return random.Next(1000, 9999);
        }

        // Mỗi OTP gửi cách nhau ít nhất số phút ?
        private async Task<bool> CheckTimeOutOTP(string phone, OTPType type)
        {
            var lastOTP = (await _iAccountOTPRepository.SearchAsync(x => x.Phone == phone && x.Type == type, x => x.OrderByDescending(m => m.Id))).FirstOrDefault();
            if (lastOTP != null && (DateTime.Now - lastOTP.CreatedDate).TotalSeconds < _baseSetting.Value.OTPTimeOut)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private async Task AccountOTPAdd(string phone, OTPType type, OTPSystemType systemType, int otp)
        {

            await _iAccountOTPRepository.AddAsync(new AccountOTP
            {
                CreatedDate = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddMinutes(_baseSetting.Value.OTPRegiterExpiredMinute),
                Phone = phone,
                Type = type,
                OTP = otp,
                SystemType = systemType
            });
            await _iAccountOTPRepository.CommitAsync();
        }

        private async Task SendOTPAsync(string phone, OTPType type, OTPSystemType systemType)
        {
            int autoGenOTP = RandomNumber();
            try
            {
                if (systemType == OTPSystemType.ZALOSMS)
                {
                    var outData = await _iSMSRepository.ZALOSendOTP(_zaloSettings, type, phone, autoGenOTP.ToString());
                    if (outData != null)
                    {
                        await AccountOTPAdd(phone, type, systemType, autoGenOTP);
                    }
                }
                else
                {
                    var outData = await _iSMSRepository.Send(_baseSetting.Value.SMS_Path, _baseSetting.Value.SMS_Token, _baseSetting.Value.SMS_Username, _baseSetting.Value.SMS_Password, _baseSetting.Value.SMS_Brandname, string.Format(_baseSetting.Value.SMS_ContentOTP, autoGenOTP), phone);
                    if (outData != null)
                    {
                        await AccountOTPAdd(phone, type, systemType, autoGenOTP);
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(0, "AccountService.SendOTPAsync", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
            }
        }

        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
        #endregion
    }
}
