﻿using Microsoft.Extensions.Options;
using TN.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Backend.Model.Command;
using TN.Infrastructure.Interfaces;
using System.IO;
using PT.Domain.Model;
using TN.Shared;

namespace TN.API.Services
{
    public interface IBookingService : IService<Booking>
    {
        Task<ApiResponse<object>> Transactions(int accountId, int page, int limit);
        Task<ApiResponse<object>> GetTransaction(int accountId, int id = 1);
        Task<ApiResponse<object>> SelectStation(int id, string lat_lng, int accountId);
        Task<ApiResponse<object>> BookingScanView(string serialNumber, int accountId, string lat_lng);
        
        Task<ApiResponse<object>> BookingCheckIn(int accountId, string serialNumber, int ticketPriceId, int ticketPrepaidId, int dockId, string lat_lng, string rfid = null, string requestId = null, string functionName = null, bool isOpenRFID = false, string discountCode = null);
        Task<ApiResponse<object>> BookingScanSubmit(int accountId, string serialNumber, int ticketPriceId, int ticketPrepaidId, int dockId, string lat_lng, string rfid = null, string requestId = null, string functionName = null, bool isOpenRFID = false);
        Task<ApiResponse<TransactionDTO>> BookingCheckOut(int bookingId, int accountId, string requestId = null, string functionName = null, bool isEndByRFID = false, bool sendNotifi = true);
        Task<ApiResponse<object>> BookingCheckOutAll(int accountId);
        Task<ApiResponse<object>> BookingCancel(BookingCancelCommand cm, int accountId);
        Task<ApiResponse<object>> BookingCurrent(int accountId);
        Task<ApiResponse<object>> BookingFeedback(int accountId, BookingFeedbackCommand cm);
        Task<ApiResponse<object>> BookingReOpen(string serialNumber, int accountId, int bookingId, string requestId = null, string functionName = null);
        Task<ApiResponse<object>> BikeReportAddError(int accountId, BikeReportCommand cm);
        ApiResponse<object> DashboardTransactions(int accountId);
        Task<ApiResponse<object>> CheckBikeInStation(int bookingId, int accountId);
        Task<ApiResponse<object>> RFIDBookingScanSubmit(string rfid, int accountId, Dock dock, Bike bike);
        Task<ApiResponse<object>> RFIDBookingReOpen(string rfid, string serialNumber, int accountId, int bookingId, string requestId = null, string functionName = null);
        Task<Tuple<ApiResponse, int, double, double>> InStationVerified(string lat_lng, int stationIn, int accountId, string functionName, string requestId, int dockId);
        Task<ApiResponse<object>> BookingReOpenAll(int accountId);
        Task<ApiResponse<object>> BookingsCurrent(int accountId);
        Task<ApiResponse<object>> BookingsFeedback(int accountId, BookingsFeedbackCommand cm);
        Task<ApiResponse<DiscountCode>> AddVerifyDiscountCode(string functionName, string requestId, int accountId, string code, string lat, string lng);
        Task<ApiResponse<DiscountCode>> VerifyDiscountCode(string functionName, string requestId, int accountId, string code, string lat, string lng);
    }

    public class BookingService : IBookingService
    {
        private readonly IStationRepository _iStationRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly IOptions<Model.Setting.BaseSetting> _baseSetting;
        private readonly IOpenLockRequestRepository _iOpenLockRequestRepository;
        private readonly IBikeReportRepository _iBikeReportRepository;
        private readonly IWalletTransactionRepository _iIWalletTransactionRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly IFireBaseService _iFireBaseService;
        private readonly IFeedbackRepository _iFeedbackRepository;
        private readonly IHubService _iHubService;
        private readonly IRFIDBookingRepository _iRFIDBookingRepository;
        private readonly ISystemConfigurationRepository _iSystemConfigurationRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ICyclingWeekRepository _iCyclingWeekRepository;
        private readonly IMemoryCacheService _iMemoryCacheService;
        private readonly ITicketPrepaidRepository _iTicketPrepaidRepository;
        private readonly IBikePrivateRepository _bikePrivateRepository;
        private readonly IDiscountCodeRepository _iDiscountCodeRepository;
        private readonly ICampaignRepository _iCampaignRepository;

        public BookingService
        (
            IStationRepository iStationRepository,
            IDockRepository iDockRepository,
            IWalletRepository iWalletRepository,
            IBookingRepository iBookingRepository,
            ITransactionRepository iTransactionRepository,
            IOptions<Model.Setting.BaseSetting> baseSettings,
            IOpenLockRequestRepository iOpenLockRequestRepository,
            IBikeReportRepository iBikeReportRepository,
            IWalletTransactionRepository iWalletTransactionRepository,
            IMongoDBRepository iMongoDBRepository,
            IOptions<LogSettings> logSettings,
            ITicketPriceRepository iTicketPriceRepository,
            IProjectAccountRepository iProjectAccountRepository,
            IFireBaseService iFireBaseService,
            IFeedbackRepository iFeedbackRepository,
            IHubService iHubService,
            IRFIDBookingRepository iRFIDBookingRepository,
            ISystemConfigurationRepository iSystemConfigurationRepository,
            IAccountRepository iAccountRepository,
            ICyclingWeekRepository iCyclingWeekRepository,
            IMemoryCacheService iMemoryCacheService,
            ITicketPrepaidRepository iTicketPrepaidRepository,
            IBikePrivateRepository bikePrivateRepository,
            IDiscountCodeRepository discountCodeRepository,
            ICampaignRepository campaignRepository
        )
        {
            _iStationRepository = iStationRepository;
            _iDockRepository = iDockRepository;
            _iWalletRepository = iWalletRepository;
            _iBookingRepository = iBookingRepository;
            _iTransactionRepository = iTransactionRepository;
            _baseSetting = baseSettings;
            _iOpenLockRequestRepository = iOpenLockRequestRepository;
            _iBikeReportRepository = iBikeReportRepository;
            _iIWalletTransactionRepository = iWalletTransactionRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _iTicketPriceRepository = iTicketPriceRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iFireBaseService = iFireBaseService;
            _iFeedbackRepository = iFeedbackRepository;
            _iHubService = iHubService;
            _iRFIDBookingRepository = iRFIDBookingRepository;
            _iSystemConfigurationRepository = iSystemConfigurationRepository;
            _iAccountRepository = iAccountRepository;
            _iCyclingWeekRepository = iCyclingWeekRepository;
            _iMemoryCacheService = iMemoryCacheService;
            _iTicketPrepaidRepository = iTicketPrepaidRepository;
            _bikePrivateRepository = bikePrivateRepository;
            _iDiscountCodeRepository = discountCodeRepository;
            _iCampaignRepository = campaignRepository;
        }

        #region [Transactions : lịch sử giao dịch]
        public async Task<ApiResponse<object>> Transactions(int accountId, int page = 1, int limit = 10)
        {
            try
            {
                var data = await _iTransactionRepository.SearchPagedList(page, limit, x => x.AccountId == accountId, x => x.OrderByDescending(m => m.EndTime));
                var stations = (await _iMemoryCacheService.StationSearchAsync()).Where(x => data.Data.Select(x => x.StationIn).Contains(x.Id) || data.Data.Select(x => x.StationOut).Contains(x.Id)).Select(x => new StationViewDTO(x));
                var listOut = new List<TransactionDTO>();
                foreach (var item in data.Data)
                {
                    listOut.Add(new TransactionDTO(item, stations.FirstOrDefault(x => x.Id == item.StationIn), stations.FirstOrDefault(x => x.Id == item.StationOut), new List<GPSData>()));
                }
                return ApiResponse.WithData(listOut, data.TotalRows, limit);
            }
            catch (Exception ex)
            {
                AddLog(0, "BookingService.Transactions", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [GetTransaction : chi tiết lịch sử giao dịch]
        public async Task<ApiResponse<object>> GetTransaction(int accountId, int id = 1)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            string functionName = "BookingService.GetTransaction";
            try
            {
                var data = await _iTransactionRepository.SearchOneAsync(true, x => x.Id == id && x.AccountId == accountId);
                if (data == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.BookingNotExist);
                }
                var stations = (await _iMemoryCacheService.StationSearchAsync()).Where(x => data.StationIn == x.Id || data.StationIn == x.Id).Select(x => new StationViewDTO(x));

                var gps = new List<GPSData>();

                try
                {
                    var gpsData = _iMongoDBRepository.GetTransactionGPS(_logSettings, data.Id, data.EndTime?.AddSeconds(10) ?? DateTime.Now);
                    if (gpsData != null)
                    {
                        gps = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GPSData>>(gpsData.Data);
                    }
                }
                catch (Exception ex)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Lỗi {ex}", "", true);
                }
                
                var outData = new TransactionDTO(data, stations.FirstOrDefault(x => x.Id == data.StationIn), stations.FirstOrDefault(x => x.Id == data.StationOut), gps);
                var bike = await _iDockRepository.GetByDock(data.DockId);
                var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == data.DockId);
                outData.Bike = new SelectStationBikeDTO(bike, dock);
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [DashboardTransactions : Thông tin số liệu trong form danh sách chuyến đi]
        public ApiResponse<object> DashboardTransactions(int accountId)
        {
            try
            {
                var data = _iTransactionRepository.DashboardTransactions(accountId);
                if (data != null)
                {
                    data.TotalKMStr = Function.DistanceString(Convert.ToDecimal(data.TotalKM ?? 0));
                }
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                AddLog(0, "BookingService.DashboardTransactions", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [SelectStation : Xem trạm còn những xe nào]
        public async Task<ApiResponse<object>> SelectStation(int stationId, string lat_lng, int accountId)
        {
            try
            {
                var station = await _iMemoryCacheService.StationGetById(stationId);
                if (station == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.StationNotExist);
                }
                var vi = await _iWalletRepository.GetWalletAsync(accountId);
                var newData = new SelectStationDTO(station)
                {
                    Distance = GetDistance(station, lat_lng),
                    InSession = DateTime.Now >= Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {_baseSetting.Value.SessionStartTime}") && DateTime.Now < Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {_baseSetting.Value.SessionEndTime}")
                };

                var bikes = await _iDockRepository.BikeByStation(stationId);
                var bikeIds = bikes.Select(m => m.DockId).Distinct().ToList();
                var docks = await _iDockRepository.SearchAsync(x => bikeIds.Contains(x.Id));
                var projects = (await _iMemoryCacheService.ProjectSearchAsync()).Where(m=> docks.Select(x => x.ProjectId).Contains(m.Id)).ToList();
                foreach (var item in projects)
                {
                    item.TicketPriceBinds = await GetTicketPriceByProjectAsync(item.Id, accountId);
                }

                newData.TotalFreeDock = docks.Where(x=>x.Battery >= _baseSetting.Value.MinBattery).Count();
                newData.TotalParkingSpaces = (station.Spaces - bikes.Count < 0) ? 0 : (station.Spaces - bikes.Count);
                newData.WalletBalance = vi?.Balance ?? 0;
                newData.WalletSubBalance = vi?.SubBalance ?? 0;
                newData.Bikes = new List<SelectStationBikeDTO>();

                foreach (var item in bikes)
                {
                    var dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                    if (dock != null)
                    {
                        newData.Bikes.Add(new SelectStationBikeDTO(item, dock, projects.FirstOrDefault(x => x.Id == item.ProjectId)?.TicketPriceBinds ?? new List<TicketPriceBindModel>(), GetDistance(dock, lat_lng), _baseSetting.Value.BikeReturnDistance));
                    }
                }
                return ApiResponse.WithData(newData);
            }
            catch (Exception ex)
            {
                AddLog(0, "BookingService.SelectStation", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BookingScanView : Đến trạm quẹt vé hiện ra thông tin]
        public async Task<ApiResponse<object>> BookingScanView(string serialNumber, int accountId, string lat_lng)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";
            var functionName = "BookingService.BookingScanView";

            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} quẹt để xem thông tin {serialNumber}, tọa độ {lat_lng}", "");
            try
            {
                //Kiểm tra xem tài khoản có đang bắt đầu chuyến đi không
                var dock = await _iDockRepository.SearchOneAsync(true, x => (x.SerialNumber == serialNumber || x.IMEI == serialNumber));
                if (dock == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.BikeIsError);
                }
                var bike = await _iDockRepository.BikeByDock(dock.Id);
                if (bike == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Bike trên dock không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.BikeIsError);
                }
                // Kiểm tra xem giao dịch nào đã sử dụng khóa này chưa, phòng trường hợp update ko dc
                var ktsd = await _iBookingRepository.AnyAsync(x => x.DockId == dock.Id && x.Status == EBookingStatus.Start);
                if (ktsd)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đã sử dụng bởi 1 chuyến đi khác, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockBusy);
                }

                var station = await _iMemoryCacheService.StationGetById(dock.StationId);
                if (station == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Trạm {dock.StationId} không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.StationNotExist);
                }
                // Lấy ví của người dùng
                var wInfo = await _iWalletRepository.GetWalletAsync(accountId);
                var newData = new SelectStationDTO(station)
                {
                    Distance = GetDistance(station, lat_lng),
                    InSession = DateTime.Now >= Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {_baseSetting.Value.SessionStartTime}") && DateTime.Now < Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {_baseSetting.Value.SessionEndTime}")
                };
                var ticketPriceBinds = await GetTicketPriceByProjectAsync(dock.ProjectId, accountId);

                var ktChuyenDiHT = await _iBookingRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (ktChuyenDiHT != null)
                {
                    if(ktChuyenDiHT.TicketPrice_TicketType == ETicketType.Block)
                    {
                        //AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Ticket Id {ktChuyenDiHT.TicketPriceId} {Newtonsoft.Json.JsonConvert.SerializeObject(ticketPriceBinds)}", "", true);
                         ticketPriceBinds = ticketPriceBinds.Where(x => x.TicketPriceId == ktChuyenDiHT.TicketPriceId).ToList();
                    }
                    else
                    {
                        ticketPriceBinds = ticketPriceBinds.Where(x => x.TicketPriceId == ktChuyenDiHT.TicketPriceId && x.TicketPrepaidId ==  ktChuyenDiHT.TicketPrepaidId).ToList();
                    }
                }

                newData.Bike = new SelectStationBikeDTO(bike, dock, ticketPriceBinds, GetDistance(dock, lat_lng), _baseSetting.Value.BikeReturnDistance);
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Data trả về client {Newtonsoft.Json.JsonConvert.SerializeObject(newData)}", "");
                return ApiResponse.WithData(newData);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lỗi {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [Thuê thêm xe trong chuyến đi]
        public async Task<ApiResponse<object>> BookingAddBike(Booking parentBooking, string requestId, string functionName, Bike bike, Dock dock, string rfid, DiscountCode discountCodeObject)
        {
            try
            {
                // Kiểm tra xem khách hàng được cùng lúc thuê bao nhiêu xe
                var currentCount = await _iBookingRepository.CountAsync(x => x.AccountId == parentBooking.AccountId && x.Status == EBookingStatus.Start);
                var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == parentBooking.AccountId);
                if(account == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.VuotQuaSoXeBanMuonThue, "1");
                }

                if (currentCount >= (account.MaxBookingBike <= 0 ? 3 : account.MaxBookingBike))
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.VuotQuaSoXeBanMuonThue, account.MaxBookingBike.ToString());
                }

                if(account.VerifyStatus != EAccountVerifyStatus.Ok)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.VeNhomPhaiXacThuc);
                }
                // Set cho khóa bận
                await _iDockRepository.DockStatusSetProcess(dock.Id, true);
                // Kiểm tra điều kiện để 1 acc thue thêm xe
                // +Tính tổng chi phí khách hàng phải trả là bao nhiêu
                // + Vé block (Tổng số điểm còn trong tk - tổng số điểm đang được tính toán) >= (số xe đang thuê x 20000) + 20000
                if (parentBooking.TicketPrice_TicketType == ETicketType.Block)
                {
                    var totalBooking = await CurrentTotalPrice(parentBooking.AccountId);
                    if(totalBooking.Item3 == 0)
                    {
                        return ApiResponse.WithStatusInvalidData();
                    }
                    decimal blockPriceDefault = await _iMemoryCacheService.TicketPriceGetDefaultPrice(parentBooking.ProjectId);
                    var wInfo = await _iWalletRepository.GetWalletAsync(parentBooking.AccountId);
                    //    var statusCheck = (wInfo.Balance + wInfo.SubBalance - totalBooking.Item1) >= ((totalBooking.Item3 + 1) * 10000);
                    var statusCheck = ((wInfo.Balance + wInfo.SubBalance - totalBooking.Item1) + (totalBooking.Item3 * blockPriceDefault)) >= (blockPriceDefault * 2 * (totalBooking.Item3 + 1));
                    if (!statusCheck)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.KhongDuDieuKienThueThemXeVeLuot);
                    }    
                }
                // + Vé trả trước (Tổng số phút của vé trả trước còn - tổng số phút đang đi) >= (số xe đang thuê * 30) + 450 hoặc số tiền trong ví dư 50 * (số xe + 1)
                else
                {
                    var totalBooking = await CurrentTotalPrice(parentBooking.AccountId);
                    if (totalBooking.Item3 == 0)
                    {
                        return ApiResponse.WithStatusInvalidData();
                    }
                    var ticketPp = await _iTicketPrepaidRepository.SearchOneAsync(true, x => x.Id == parentBooking.TicketPrepaidId && x.AccountId == parentBooking.AccountId);
                    if(ticketPp == null)
                    {
                        return ApiResponse.WithStatusInvalidData();
                    }
                    // Điều kiện mua số lượng vé trả trước
                    if (currentCount >= ticketPp.NumberExpirations)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.VuotQuaSoXeBanMuonThue, ticketPp.NumberExpirations.ToString());
                    }
                    // 
                    var statusCheck = (Convert.ToDouble(parentBooking.TicketPrice_LimitMinutes - totalBooking.Item2) / 120) > (currentCount + 1) ;
                    if (!statusCheck)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.KhongDuDieuKienThueThemXeVeNgay);
                    }
                }

                string openDockKey = $"{DateTime.Now:yyyyMMddHHmmssfff}";

                var newBooking = new Booking()
                {
                    Id = 0,
                    AccountId = parentBooking.AccountId,
                    BikeId = bike.Id,
                    DockId = dock.Id,
                    CustomerGroupId = parentBooking.CustomerGroupId,
                    InvestorId = bike.InvestorId,
                    ProjectId = bike.ProjectId,
                    IsDockNotOpen = false,
                    CreatedDate = DateTime.Now,
                    Debt = 0,
                    IsOpenRFID = parentBooking.IsOpenRFID,
                    Vote = 5,
                    TicketPrice_TicketValue = parentBooking.TicketPrice_TicketValue,
                    TicketPrice_AllowChargeInSubAccount = parentBooking.TicketPrice_AllowChargeInSubAccount,
                    TicketPrice_BlockPerMinute = parentBooking.TicketPrice_BlockPerMinute,
                    TicketPrice_BlockPerViolation = parentBooking.TicketPrice_BlockPerViolation,
                    TicketPrice_TicketType = parentBooking.TicketPrice_TicketType,
                    TicketPrice_BlockViolationValue = parentBooking.TicketPrice_BlockViolationValue,
                    TicketPrice_IsDefault = parentBooking.TicketPrice_IsDefault,
                    TicketPrice_Note = parentBooking.TicketPrice_Note,
                    TicketPrice_LimitMinutes = parentBooking.TicketPrice_LimitMinutes,
                    ChargeInAccount = parentBooking.ChargeInAccount,
                    ChargeInSubAccount = parentBooking.ChargeInSubAccount,
                    DateOfPayment = null,
                    EndTime = null,
                    EstimateChargeInAccount = 0,
                    EstimateChargeInSubAccount = 0,
                    IsEndRFID = false,
                    KCal = 0,
                    KM = 0,
                    KG = 0,
                    IsForgotrReturnTheBike = false,
                    Prepaid_EndDate = parentBooking.Prepaid_EndDate,
                    Prepaid_EndTime = parentBooking.Prepaid_EndTime,
                    Prepaid_MinutesSpent = parentBooking.Prepaid_MinutesSpent,
                    Prepaid_StartDate = parentBooking.Prepaid_StartDate,
                    Prepaid_StartTime = parentBooking.Prepaid_StartTime,
                    TicketPrepaidId = parentBooking.TicketPrepaidId,
                    TicketPriceId = parentBooking.TicketPriceId,
                    Status = EBookingStatus.Start,
                    StationIn = bike.StationId,
                    ReCallM = 0,
                    RFID = rfid,
                    StartTime = DateTime.Now,
                    TicketPrepaidCode = parentBooking.TicketPrepaidCode,
                    TotalPrice = 0,
                    VehicleRecallPrice = 0,
                    TransactionCode = $"{bike.ProjectId:D4}{DateTime.Now:yyyyMMddHHmmssfff}",
                    TotalMinutes = 0,
                    TripPoint = 0,
                    StationOut = null,
                    Note = null,
                    Feedback = null,
                    GPS = new List<GPSData>(),
                    VehicleRecallStatus = EVehicleRecallStatus.Default,
                    Bike = null,
                    Dock = null,
                    Wallet = null,
                    Project = null,
                    TicketPrice = null,
                    Account = null,
                    CustomerGroup = null,
                    Investor = null,
                    ObjStationIn = null,
                    ObjStationOut = null,
                    StationInObject = null,
                    DiscountCodeId = discountCodeObject?.Id,
                    DiscountCode_Point = discountCodeObject?.Point,
                    DiscountCode_Price = 0,
                    DiscountCode_ProjectId = discountCodeObject?.ProjectId,
                    DiscountCode_Type = discountCodeObject?.Type,
                    DiscountCode_CampaignId = discountCodeObject?.CampaignId,
                    DiscountCode_Percent = discountCodeObject?.Percent,
                    OpenLockTransaction = openDockKey,
                    GroupTransactionOrder= parentBooking.GroupTransactionOrder == null ? 2 : (parentBooking.GroupTransactionOrder ?? 0) + 1,
                    GroupTransactionCode = parentBooking.GroupTransactionCode
                };
                await _iBookingRepository.AddAsync(newBooking);
                await _iBookingRepository.CommitAsync();
                // Gửi lệnh mở khóa
                bool sendOpen = await OpenLock(dock, newBooking.TransactionCode, parentBooking.AccountId, openDockKey);
                // Add log
                AddLog(parentBooking.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi lệnh mở khóa => {sendOpen}", "");

                await _iDockRepository.DockStatusSetProcess(dock.Id, false);

                await _iFireBaseService.SendNotificationByAccount(functionName, requestId, parentBooking.AccountId, ENotificationTempType.LayXeThanhCong, true, DockEventType.Not, null);
                // Update số lượng xe free trong trạm
                await _iFireBaseService.NotifyRefeshStation(functionName, requestId, dock.StationId, "all");
                // Gửi cho admin phần quản lý
                await _iHubService.EventSend(EEventSystemType.M_3002_CoGiaoDichMoi, EEventSystemWarningType.Info, parentBooking.AccountId, newBooking.TransactionCode, newBooking.ProjectId, newBooking.StationIn, newBooking.BikeId, newBooking.DockId, $"Khách hàng bắt đầu chuyến đi thành công, mã chuyến đi '{newBooking.TransactionCode}' ");
                //
                AddLog(parentBooking.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Bắt đầu chuyến đi #({newBooking.Id}) thành công", "");
                // Retry xem khóa mở chưa để log là giao dịch này cần log lại
                bool isOpen = false;
                int retries = _baseSetting.Value.NumRetryOpenDock > 0 ? _baseSetting.Value.NumRetryOpenDock : 25;
                for (int i = 0; i <= retries; i++)
                {
                    var dockx = await _iDockRepository.SearchOneAsync(true, x => x.Id == dock.Id);
                    isOpen = dockx.LockStatus == false;
                    if (isOpen)
                    {
                        AddLog(parentBooking.AccountId, functionName, requestId, EnumMongoLogType.Center, $"Break retries lần {i}", "");
                        break;
                    }
                    else
                    {
                        await Task.Delay(500);
                    }
                }

                if (!isOpen)
                {
                    await _iBookingRepository.FlagNotOpen(newBooking.Id);
                    AddLog(parentBooking.AccountId, functionName, requestId, EnumMongoLogType.End, $"Try({ retries}) Không thể mở khóa trong thời gian retry", "");
                    await _iHubService.EventSend(EEventSystemType.M_3001_KhachHangMoXeKhongThanhCong, EEventSystemWarningType.Danger, newBooking.AccountId, null, dock.ProjectId, dock.StationId, bike.Id, dock.Id, $"Bắt đầu chuyến đi, khách hàng mở khóa xe '{bike.Plate}' barcode '{dock.SerialNumber}' không thành công do thời hạn quá lâu do khóa không phản hồi hệ thống");
                }
                await _iDockRepository.DockStatusSetProcess(dock.Id, false);
                return ApiResponse.WithStatusOk(new CurrentBookingDataDTO(newBooking));
            }
            catch (Exception ex)
            {
                AddLog(parentBooking.AccountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                await _iDockRepository.DockStatusSetProcess(dock.Id, false);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BookingScanSubmit : Xác nhật mở khóa xe bắt đầu chuyến đi]
        public async Task<ApiResponse<object>> BookingScanSubmit(int accountId, string serialNumber, int ticketPriceId, int ticketPrepaidId, int dockId, string lat_lng, string rfid = null, string requestId = null, string functionName = null, bool isOpenRFID = false)
        {
            if (_baseSetting.Value.WarningSystemStatus)
            {
                return ApiResponse.WithStatus(false, null, ResponseCode.ChucNangNayDangBiVoHieuHoa);
            }

            if(requestId == null || requestId == "")
            {
                requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            }  
            
            if(functionName == null || functionName == "")
            {
                functionName = "BookingService.BookingScanSubmit";
            }    

            if(string.IsNullOrEmpty(rfid))
            {
                var getAccount = await _iAccountRepository.SearchOneAsync(false, x => x.Id == accountId);
                rfid = getAccount?.RFID ?? null;
            }    

            TicketPrepaid ticketPrepaid = null;
            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} quẹt mở khóa {serialNumber}, ticketPriceId {ticketPriceId}, dockId {dockId}, ticketPrepaidId {ticketPrepaidId}, tọa độ {lat_lng}", "");
            try
            {
                //Kiểm tra session làm việc của hệ thống
                if (!(await IsInSesssion()))
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Hết ca làm việc không thể mở khóa, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.HetCaLamViec);
                }

                // Kiểm tra xem tài khoản bị khóa ko
                var ktKhoa = await _iAccountRepository.AnyAsync(x => x.Id == accountId && x.Status == EAccountStatus.Lock);
                if (ktKhoa)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản bị khóa", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanDangBiKhoa);
                }

                var ktNoCuoc = await _iTransactionRepository.AnyAsync(x => x.Status == EBookingStatus.Debt && x.AccountId == accountId);
                if (ktNoCuoc)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Đang nợ cước", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanConNoCuoc);
                }

                // Kiểm tra khóa có active không
                var dock = await _iDockRepository.SearchOneAsync(true, x => (x.SerialNumber == serialNumber || x.IMEI == serialNumber));
                if (dock == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockNotExist);
                }

                if ((dock.ConnectionStatus == false && dock.Battery < 1) || (dock.Battery < 1))
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} hết pin, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.XeHetPin);
                }
                // Check thời lượng pin
                if (dock.Battery < _baseSetting.Value.MinBattery)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Thời lượng {dock.Battery}/{_baseSetting.Value.MinBattery} không đủ, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.LuongPinKhongDu);
                }

                if (dockId > 0 && dock.Id != dockId)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} không đúng với dock {dockId} đã chọn, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.MaQRKhongDungVoiKhoaDaChon);
                }
                // Lấy ra thông tin xe kiều kiện tồn tại và active
                var bike = await _iDockRepository.BikeByDock(dock.Id);
                if (bike == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Bike không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.BikeIsError);
                }
                // Kiểm tra có phải xe private không
                var anyBikePrivate = await _bikePrivateRepository.AnyAsync(x => x.BikeId == bike.Id);
                if (anyBikePrivate)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Xe này là xe private, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DaTonTaiChuyenDiTruoc);
                }
                // Kiểm tra xem giao dịch nào đã sử dụng khóa này chưa
                var ktsd = await _iBookingRepository.AnyAsync(x => x.DockId == dock.Id && x.Status == EBookingStatus.Start);
                if (ktsd)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đã sử dụng bởi 1 chuyến đi khác, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockBusy);
                }
                // Kiểm tra xe này đang đc điều phối hay có người khác thuê du lịch rồi
                var rfidBooking = await _iRFIDBookingRepository.SearchOneAsync(true, x => x.BikeId == bike.Id && x.Status == ERFIDCoordinatorStatus.Start);
                if (rfidBooking != null && rfidBooking.Type == ERFIDBookingType.Account)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đã sử dụng bởi người thuê đi du lịch, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockBusy);
                }
                else if (rfidBooking != null && rfidBooking.Type == ERFIDBookingType.Admin)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đang được điều phối chưa thuê được, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.BikeIsError);
                }
                // Kiểm tra xem xem tài khoản có đang thuê 1 chuyến nào rồi, thì sẽ cho thuê xe thêm
                var ktBooking = await _iBookingRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (ktBooking != null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.DaTonTaiChuyenDiTruoc);
                }
                dockId = dock.Id;
                await _iDockRepository.DockStatusCheckAny(dockId, accountId);
                var ktProcess = await _iDockRepository.DockStatusCheckProcess(dockId, _baseSetting.Value.DockProcessAllowTime);
                if (ktProcess)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đang bận với tiến trình khác, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockIsProcess);
                }
                //
                var ticketPrice = await _iTicketPriceRepository.SearchOneAsync(true, x => x.Id == ticketPriceId && x.IsActive);
                if (ticketPrice == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mã giá vé {ticketPriceId} không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TicketInvalid);
                }
                // Kiểm tra xem có gần khóa không
                var checkInDock = InDockVerified(dock, lat_lng, accountId, requestId, functionName, out int kc);
                if (!checkInDock.Status)
                {
                    await _iHubService.EventSend(EEventSystemType.M_3001_KhachHangMoXeKhongThanhCong, EEventSystemWarningType.Warning, accountId, null, dock.ProjectId, dock.StationId, bike.Id, dock.Id,
                      $"Khách muốn thuê xe, mở khóa không thành công '{bike.Plate}' barcode '{dock.SerialNumber}' do không gần khóa {kc}/{_baseSetting.Value.BikeReturnDistance}");
                    return checkInDock;
                }
                // Check trường hợp vé trả trước
                if (ticketPrepaidId > 0 && ticketPrice.TicketType != ETicketType.Block)
                {
                    var prepad = await _iTicketPriceRepository.GetTicketPrepaidByAccount(accountId, ticketPrepaidId, dock.ProjectId);
                    if (prepad == null || (prepad.MinutesSpent >= prepad.TicketPrice_LimitMinutes))
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mã vé trả trước {ticketPrepaidId} không tồn tại hoặc đã hết hạn, END", "");
                        return ApiResponse.WithStatus(false, null, ResponseCode.VeTraTruocKhongTonTai);
                    }
                    ticketPrepaid = prepad;
                }
                else if (ticketPrepaidId == 0 && ticketPrice.TicketType == ETicketType.Block)
                {
                    // key là vé lượt
                    var getVi = await _iWalletRepository.GetWalletAsync(accountId);
                    // Kiểm tra có đủ trả tiền cho vé lượt này không
                    var checkPoint = await _iWalletRepository.WalletChecked(ticketPrice.TicketValue, getVi, _baseSetting.Value.MinTotalStartBooking);
                    if (checkPoint == false)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản không đủ điều kiện thực hiện chuyến đi {ticketPrice.TicketValue}/({getVi.Balance},{getVi.SubBalance}), END", "");
                        // Gửi notify
                        return ApiResponse.WithStatus(false, null, ResponseCode.NotMoney);
                    }
                    // Kiểm tra tài khoản này có thuộc nhóm tài khoản đặc biệt không
                    if (ticketPrice.CustomerGroupId > 0 && !ticketPrice.IsDefault)
                    {
                        var checkInCustomerGroup = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == ticketPrice.ProjectId && x.CustomerGroupId == ticketPrice.CustomerGroupId);
                        if (!checkInCustomerGroup)
                        {
                            AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản không thuộc nhóm khách hàng ({ticketPrice.CustomerGroupId}), END", "");
                            return ApiResponse.WithStatus(false, null, ResponseCode.GiaVeKhongThuocNhomKhachHang);
                        }
                    }
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mã giá vé {ticketPriceId} không hợp lệ, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TicketInvalid);
                }
                // Set cho khóa bận
                await _iDockRepository.DockStatusSetProcess(dockId, true);
                string openDockKey = $"{DateTime.Now:yyyyMMddHHmmssfff}";
                var booking = await AddBooking(accountId, ticketPrice, dock.StationId, dock.Id, bike, ticketPrepaid, rfid, isOpenRFID, null, openDockKey);
               
                if (booking != null)
                {
                    // Gửi lệnh mở khóa
                    bool sendOpen = await OpenLock(dock, null, accountId, openDockKey);
                    // Add log
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi lệnh mở khóa => {sendOpen}", "");
                    await _iDockRepository.DockStatusSetProcess(dockId, false);

                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.LayXeThanhCong, true, DockEventType.Not, null);
                    // Update số lượng xe free trong trạm
                    await _iFireBaseService.NotifyRefeshStation(functionName, requestId, dock.StationId, "all");
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_3002_CoGiaoDichMoi, EEventSystemWarningType.Info, accountId, booking.TransactionCode, booking.ProjectId, booking.StationIn, booking.BikeId, booking.DockId, $"Khách hàng bắt đầu chuyến đi thành công, mã chuyến đi '{booking.TransactionCode}' ");
                    //
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Bắt đầu chuyến đi #({booking.Id}) thành công", "");
                    // Retry xem khóa mở chưa để log là giao dịch này cần log lại
                    bool isOpen = false;
                    int retries = _baseSetting.Value.NumRetryOpenDock > 0 ? _baseSetting.Value.NumRetryOpenDock : 25;
                    for (int i = 0; i <= retries; i++)
                    {
                        var dockx = await _iDockRepository.SearchOneAsync(true, x => x.Id == dock.Id);
                        isOpen = dockx.LockStatus == false;
                        if (isOpen)
                        {
                            AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Break retries lần {i}", "");
                            break;
                        }
                        else
                        {
                           await Task.Delay(500);
                        }
                    }

                    if (!isOpen)
                    {
                        await _iBookingRepository.FlagNotOpen(booking.Id);
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Try({ retries}) Không thể mở khóa trong thời gian retry", "");
                        await _iHubService.EventSend(EEventSystemType.M_3001_KhachHangMoXeKhongThanhCong, EEventSystemWarningType.Danger, accountId, null, dock.ProjectId, dock.StationId, bike.Id, dock.Id, $"Bắt đầu chuyến đi, khách hàng mở khóa xe '{bike.Plate}' barcode '{dock.SerialNumber}' không thành công do thời hạn quá lâu do khóa không phản hồi hệ thống");
                    }
                    await _iDockRepository.DockStatusSetProcess(dockId, false);
                    return ApiResponse.WithStatusOk(new CurrentBookingDataDTO(booking));
                }
                else
                {
                    await _iDockRepository.DockStatusSetProcess(dockId, false);
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Bắt đầu chuyến đi không thành công", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockIsProcess);
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                await _iDockRepository.DockStatusSetProcess(dockId, false);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BookingCheckIn : Xác nhật mở khóa xe bắt đầu chuyến đi]
        public async Task<ApiResponse<object>> BookingCheckIn(int accountId, string serialNumber, int ticketPriceId, int ticketPrepaidId, int dockId, string lat_lng, string rfid = null, string requestId = null, string functionName = null, bool isOpenRFID = false, string discountCode = null)
        {
            if (_baseSetting.Value.WarningSystemStatus)
            {
                return ApiResponse.WithStatus(false, null, ResponseCode.ChucNangNayDangBiVoHieuHoa);
            }

            if (requestId == null || requestId == "")
            {
                requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            }

            if (functionName == null || functionName == "")
            {
                functionName = "BookingService.BookingCheckIn";
            }

            if (string.IsNullOrEmpty(rfid))
            {
                var getAccount = await _iAccountRepository.SearchOneAsync(false, x => x.Id == accountId);
                rfid = getAccount?.RFID ?? null;
            }

            TicketPrepaid ticketPrepaid = null;
            DiscountCode discountCodeObject = null;

            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} quẹt mở khóa {serialNumber}, ticketPriceId {ticketPriceId}, dockId {dockId}, ticketPrepaidId {ticketPrepaidId}, tọa độ {lat_lng}", "");
            try
            {
                //Kiểm tra session làm việc của hệ thống
                if (!(await IsInSesssion()))
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Hết ca làm việc không thể mở khóa, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.HetCaLamViec);
                }
                // Kiểm tra xem tài khoản bị khóa ko
                var ktKhoa = await _iAccountRepository.AnyAsync(x => x.Id == accountId && x.Status == EAccountStatus.Lock);
                if (ktKhoa)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản bị khóa", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanDangBiKhoa);
                }    

                var ktNoCuoc = await _iTransactionRepository.AnyAsync(x => x.Status == EBookingStatus.Debt && x.AccountId == accountId);
                if (ktNoCuoc)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Đang nợ cước", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TaiKhoanConNoCuoc);
                }

                // Kiểm tra khóa có active không
                var dock = await _iDockRepository.SearchOneAsync(true, x => (x.SerialNumber == serialNumber || x.IMEI == serialNumber));
                if (dock == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockNotExist);
                }

                if ((dock.ConnectionStatus == false && dock.Battery < 1) || (dock.Battery < 1))
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} hết pin, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.XeHetPin);
                }
                // Check thời lượng pin
                if (dock.Battery < _baseSetting.Value.MinBattery)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Thời lượng {dock.Battery}/{_baseSetting.Value.MinBattery} không đủ, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.LuongPinKhongDu);
                }

                if (dockId > 0 && dock.Id != dockId)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} không đúng với dock {dockId} đã chọn, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.MaQRKhongDungVoiKhoaDaChon);
                }
                // Lấy ra thông tin xe kiều kiện tồn tại và active
                var bike = await _iDockRepository.BikeByDock(dock.Id);
                if (bike == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Bike không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.BikeIsError);
                }
                // Kiểm tra có phải xe private không
                var anyBikePrivate = await _bikePrivateRepository.AnyAsync(x => x.BikeId == bike.Id);
                if (anyBikePrivate)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Xe này là xe private, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DaTonTaiChuyenDiTruoc);
                }
                // Kiểm tra xem giao dịch nào đã sử dụng khóa này chưa
                var ktsd = await _iBookingRepository.AnyAsync(x => x.DockId == dock.Id && x.Status == EBookingStatus.Start);
                if (ktsd)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đã sử dụng bởi 1 chuyến đi khác, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockBusy);
                }
                // Kiểm tra xe này đang đc điều phối hay có người khác thuê du lịch rồi
                var rfidBooking = await _iRFIDBookingRepository.SearchOneAsync(true, x => x.BikeId == bike.Id && x.Status == ERFIDCoordinatorStatus.Start);
                if (rfidBooking != null && rfidBooking.Type == ERFIDBookingType.Account)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đã sử dụng bởi người thuê đi du lịch, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockBusy);
                }
                else if (rfidBooking != null && rfidBooking.Type == ERFIDBookingType.Admin)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đang được điều phối chưa thuê được, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.BikeIsError);
                }

                var ticketPrice = await _iTicketPriceRepository.SearchOneAsync(true, x => x.Id == ticketPriceId && x.IsActive);
                if (ticketPrice == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mã giá vé {ticketPriceId} không tồn tại, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TicketInvalid);
                }

                if (!string.IsNullOrEmpty(discountCode))
                {
                    if (ticketPrice.TicketType != ETicketType.Block)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mã giảm giá chỉ áp dụng với vé block, END", "");
                        return ApiResponse.WithStatus(false, null, ResponseCode.MaGiamGiaChiApDungVeBlock);
                    }
                    var toaDo = StringToLatLng(lat_lng);
                    var vrfDiscountCode = await AddVerifyDiscountCode(functionName, requestId, accountId, discountCode, toaDo.Lat.ToString(), toaDo.Lng.ToString());
                    if (vrfDiscountCode.ErrorCode != ResponseCode.Ok && vrfDiscountCode.Data == null)
                    {
                        return ApiResponse.WithStatus(false, null, vrfDiscountCode.ErrorCode);
                    }
                    discountCodeObject = vrfDiscountCode.Data;
                }

                // Kiểm tra xem xem tài khoản có đang thuê 1 chuyến nào rồi, thì sẽ cho thuê xe thêm
                var ktBooking = await _iBookingRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (ktBooking != null)
                {
                    return await BookingAddBike(ktBooking, requestId, functionName, bike, dock, rfid, discountCodeObject);
                }
                dockId = dock.Id;
                await _iDockRepository.DockStatusCheckAny(dockId, accountId);
                var ktProcess = await _iDockRepository.DockStatusCheckProcess(dockId, _baseSetting.Value.DockProcessAllowTime);
                if (ktProcess)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} đang bận với tiến trình khác, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockIsProcess);
                }

                // Kiểm tra xem có gần khóa không
                var checkInDock = InDockVerified(dock, lat_lng, accountId, requestId, functionName, out int kc);
                if (!checkInDock.Status)
                {
                    await _iHubService.EventSend(EEventSystemType.M_3001_KhachHangMoXeKhongThanhCong, EEventSystemWarningType.Warning, accountId, null, dock.ProjectId, dock.StationId, bike.Id, dock.Id,
                      $"Khách muốn thuê xe, mở khóa không thành công '{bike.Plate}' barcode '{dock.SerialNumber}' do không gần khóa {kc}/{_baseSetting.Value.BikeReturnDistance}");
                    return checkInDock;
                }

                // Check trường hợp vé trả trước
                if (ticketPrepaidId > 0 && ticketPrice.TicketType != ETicketType.Block)
                {
                    var prepad = await _iTicketPriceRepository.GetTicketPrepaidByAccount(accountId, ticketPrepaidId, dock.ProjectId);
                    if (prepad == null || (prepad.MinutesSpent >= prepad.TicketPrice_LimitMinutes))
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mã vé trả trước {ticketPrepaidId} không tồn tại hoặc đã hết hạn, END", "");
                        return ApiResponse.WithStatus(false, null, ResponseCode.VeTraTruocKhongTonTai);
                    }
                    ticketPrepaid = prepad;
                }
                else if (ticketPrepaidId == 0 && ticketPrice.TicketType == ETicketType.Block)
                {
                    // key là vé lượt
                    var getVi = await _iWalletRepository.GetWalletAsync(accountId);
                    // Kiểm tra có đủ trả tiền cho vé lượt này không
                    var checkPoint = await _iWalletRepository.WalletChecked(ticketPrice.TicketValue, getVi, _baseSetting.Value.MinTotalStartBooking);
                    if (checkPoint == false)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản không đủ điều kiện thực hiện chuyến đi {ticketPrice.TicketValue}/({getVi.Balance},{getVi.SubBalance}), END", "");
                        // Gửi notify
                        return ApiResponse.WithStatus(false, null, ResponseCode.NotMoney);
                    }
                    // Kiểm tra tài khoản này có thuộc nhóm tài khoản đặc biệt không
                    if (ticketPrice.CustomerGroupId > 0 && !ticketPrice.IsDefault)
                    {
                        var checkInCustomerGroup = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == ticketPrice.ProjectId && x.CustomerGroupId == ticketPrice.CustomerGroupId);
                        if (!checkInCustomerGroup)
                        {
                            AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tài khoản không thuộc nhóm khách hàng ({ticketPrice.CustomerGroupId}), END", "");
                            return ApiResponse.WithStatus(false, null, ResponseCode.GiaVeKhongThuocNhomKhachHang);
                        }

                    }
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mã giá vé {ticketPriceId} không hợp lệ, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.TicketInvalid);
                }
                // Set cho khóa bận
                await _iDockRepository.DockStatusSetProcess(dockId, true);
                string openDockKey = $"{DateTime.Now:yyyyMMddHHmmssfff}";
                var booking = await AddBooking(accountId, ticketPrice, dock.StationId, dock.Id, bike, ticketPrepaid, rfid, isOpenRFID, discountCodeObject, openDockKey);

                if (booking != null)
                {
                    // Gửi lệnh mở khóa
                    bool sendOpen = await OpenLock(dock, booking.TransactionCode, accountId, openDockKey);
                    // Add log
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gửi lệnh mở khóa => {sendOpen}", "");
                    await _iDockRepository.DockStatusSetProcess(dockId, false);

                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.LayXeThanhCong, false, DockEventType.Not, null);
                    // Update số lượng xe free trong trạm
                    await _iFireBaseService.NotifyRefeshStation(functionName, requestId, dock.StationId, "all");
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_3002_CoGiaoDichMoi, EEventSystemWarningType.Info, accountId, booking.TransactionCode, booking.ProjectId, booking.StationIn, booking.BikeId, booking.DockId, $"Khách hàng bắt đầu chuyến đi thành công, mã chuyến đi '{booking.TransactionCode}' ");
                    //
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Bắt đầu chuyến đi #({booking.Id}) thành công", "");
                    // Retry xem khóa mở chưa để log là giao dịch này cần log lại
                    bool isOpen = false;
                    int retries = _baseSetting.Value.NumRetryOpenDock > 0 ? _baseSetting.Value.NumRetryOpenDock : 25;
                    for (int i = 0; i <= retries; i++)
                    {
                        var dockx = await _iDockRepository.SearchOneAsync(true, x => x.Id == dock.Id);
                        isOpen = dockx.LockStatus == false;
                        if (isOpen)
                        {
                            AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Break retries lần {i}", "");
                            break;
                        }
                        else
                        {
                            await Task.Delay(500);
                        }
                    }

                    if (!isOpen)
                    {
                        await _iBookingRepository.FlagNotOpen(booking.Id);
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Try({ retries}) Không thể mở khóa trong thời gian retry", "");
                        await _iHubService.EventSend(EEventSystemType.M_3001_KhachHangMoXeKhongThanhCong, EEventSystemWarningType.Danger, accountId, null, dock.ProjectId, dock.StationId, bike.Id, dock.Id, $"Bắt đầu chuyến đi, khách hàng mở khóa xe '{bike.Plate}' barcode '{dock.SerialNumber}' không thành công do thời hạn quá lâu do khóa không phản hồi hệ thống");
                    }
                    await _iDockRepository.DockStatusSetProcess(dockId, false);
                    return ApiResponse.WithStatusOk(new CurrentBookingDataDTO(booking));
                }
                else
                {
                    await _iDockRepository.DockStatusSetProcess(dockId, false);
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Bắt đầu chuyến đi không thành công", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockIsProcess);
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                await _iDockRepository.DockStatusSetProcess(dockId, false);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<DiscountCode>> AddVerifyDiscountCode(string functionName, string requestId, int accountId, string code, string lat, string lng)
        {
            try
            {
                code = code.Trim();
                var getCode = await _iDiscountCodeRepository.SearchOneAsync(true, x => x.Code == code);
                if (getCode == null)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);
                }

                if (getCode.ProvisoType == ECampaignProvisoType.VerifyOke)
                {
                    var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                    if (account == null)
                    {
                        return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);
                    }

                    if (account.VerifyStatus != EAccountVerifyStatus.Ok)
                    {
                        return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                    }
                }
                else if (getCode.ProvisoType == ECampaignProvisoType.ForAccount && accountId != getCode.BeneficiaryAccountId)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                }    

                // Kiểm tra còn không
                if (getCode.CurentLimit <= 0)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaiHetSoLan);
                }
                // Kiểm tra chiến dịch tồn tại không
                var camp = await _iCampaignRepository.SearchOneAsync(true, x => x.Id == getCode.CampaignId && x.Status == ECampaignStatus.Active);
                if (camp == null)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);

                }
                // Kiểm tra mã còn hiệu lực không
                if (!(getCode.StartDate <= DateTime.Now && getCode.EndDate.AddDays(1) > DateTime.Now))
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaiKhongConHieuLuc);
                }
                // Kiểm tra khách hàng này đã add code cùng chiến dịch
                var isCampaign = await _iIWalletTransactionRepository.AnyAsync(x => x.CampaignId == getCode.CampaignId && x.AccountId == accountId && x.CreatedDate >= camp.StartDate.AddDays(-1));
                if (isCampaign)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaiBanDaThamGiaRoi);
                }
                //Chuyến đi hiện tại có chuyến đi nào add code chưa
                var isBooking = await _iBookingRepository.AnyAsync(x => x.AccountId == accountId && x.DiscountCode_CampaignId == getCode.CampaignId && x.Status == EBookingStatus.Start);
                if (isBooking)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaiBanDaThamGiaRoi);
                }
                // Kiểm tra xem có áp dụng với nhóm khách hàng nào không
                // Logic mới nếu ko thuộc nhóm khách hàng thì sẽ check thêm tọa độ người đó có thuộc địa lý của dự án không
                var findProject = new ProjectAccountModel();
                bool isOke = true;
                if (getCode.ProjectId > 0)
                {
                    if (getCode.CustomerGroupId > 0)
                    {
                        isOke = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId && x.CustomerGroupId == getCode.CustomerGroupId);
                    }
                    else
                    {
                        isOke = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId);
                    }
                    // nếu ko đạt thì check 1 lần nữa theo tọa độ người đứng
                    if (!isOke)
                    {
                        findProject = await _iMemoryCacheService.LatLngToCustomerGroup(lat, lng, accountId);
                        if (getCode.CustomerGroupId > 0 && getCode.CustomerGroupId != findProject.CustomerGroupId)
                        {
                            return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                        }
                        else if (getCode.CustomerGroupId <= 0 && getCode.ProjectId != findProject.ProjectId)
                        {
                            return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                        }
                    }
                }
                getCode = await _iDiscountCodeRepository.SearchOneAsync(false, x => x.Id == getCode.Id);
                getCode.CurentLimit -= 1;
                _iDiscountCodeRepository.Update(getCode);
                await _iDiscountCodeRepository.CommitAsync();
                return ApiResponseT<DiscountCode>.WithStatus(true, getCode, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", null, true);
                return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<DiscountCode>> VerifyDiscountCode(string functionName, string requestId, int accountId, string code, string lat, string lng)
        {
            try
            {
                code = code.Trim();
                var getCode = await _iDiscountCodeRepository.SearchOneAsync(true, x => x.Code == code);
                if (getCode == null)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);
                }

                if (getCode.ProvisoType == ECampaignProvisoType.VerifyOke)
                {
                    var account = await _iAccountRepository.SearchOneAsync(true, x => x.Id == accountId);
                    if (account == null)
                    {
                        return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);
                    }

                    if (account.VerifyStatus != EAccountVerifyStatus.Ok)
                    {
                        return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                    }
                }
                else if (getCode.ProvisoType == ECampaignProvisoType.ForAccount && accountId != getCode.BeneficiaryAccountId)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                }

                // Kiểm tra còn không
                if (getCode.CurentLimit <= 0)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaiHetSoLan);
                }
                // Kiểm tra chiến dịch tồn tại không
                var camp = await _iCampaignRepository.SearchOneAsync(true, x => x.Id == getCode.CampaignId && x.Status == ECampaignStatus.Active);
                if (camp == null)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaVeKhuyenMaiKhongTonTai);

                }
                // Kiểm tra mã còn hiệu lực không
                if (!(getCode.StartDate <= DateTime.Now && getCode.EndDate.AddDays(1) > DateTime.Now))
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaiKhongConHieuLuc);
                }
                // Kiểm tra khách hàng này đã add code cùng chiến dịch
                var isCampaign = await _iIWalletTransactionRepository.AnyAsync(x => x.CampaignId == getCode.CampaignId && x.AccountId == accountId && x.CreatedDate >= camp.StartDate.AddDays(-1));
                if (isCampaign)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaiBanDaThamGiaRoi);
                }
                //Chuyến đi hiện tại có chuyến đi nào add code chưa
                var isBooking = await _iBookingRepository.AnyAsync(x => x.AccountId == accountId && x.DiscountCode_CampaignId == getCode.CampaignId && x.Status == EBookingStatus.Start);
                if (isBooking)
                {
                    return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaiBanDaThamGiaRoi);
                }
                // Kiểm tra xem có áp dụng với nhóm khách hàng nào không
                // Logic mới nếu ko thuộc nhóm khách hàng thì sẽ check thêm tọa độ người đó có thuộc địa lý của dự án không
                var findProject = new ProjectAccountModel();
                bool isOke = true;
                if (getCode.ProjectId > 0)
                {
                    if (getCode.CustomerGroupId > 0)
                    {
                        isOke = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId && x.CustomerGroupId == getCode.CustomerGroupId);
                    }
                    else
                    {
                        isOke = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId);
                    }
                    // nếu ko đạt thì check 1 lần nữa theo tọa độ người đứng
                    if (!isOke)
                    {
                        findProject = await _iMemoryCacheService.LatLngToCustomerGroup(lat, lng, accountId);
                        if (getCode.CustomerGroupId > 0 && getCode.CustomerGroupId != findProject.CustomerGroupId)
                        {
                            return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                        }
                        else if (getCode.CustomerGroupId <= 0 && getCode.ProjectId != findProject.ProjectId)
                        {
                            return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.MaKhuyenMaNayKhongApDungVoiBan);
                        }
                    }
                }
                return ApiResponseT<DiscountCode>.WithStatus(true, getCode, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", null, true);
                return ApiResponseT<DiscountCode>.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [RFIDBookingScanSubmit : Bắt đầu chuyến đi bằng thẻ RFID]
        public async Task<ApiResponse<object>> RFIDBookingScanSubmit(string rfid, int accountId, Dock dock, Bike bike)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "BookingService.RFIDBookingScanSubmit";
            try
            {
                // Kiểm tra tài khoản có vé trả trước còn hạn không
                var prePadByAccount = (await _iTicketPriceRepository.GetsTicketPrepaidByAccount(accountId, dock.ProjectId)).OrderByDescending(x => (x.TicketPrice_LimitMinutes - x.MinutesSpent)).FirstOrDefault();
                if (prePadByAccount != null)
                {
                    return await BookingCheckIn(accountId, dock.SerialNumber, prePadByAccount.TicketPriceId, prePadByAccount.Id, dock.Id, $"{dock.Lat};{dock.Long}", rfid, requestId, functionName, true);
                }
                else
                {
                    // lấy bảng vé 
                    var ticketPrice = (await _iTicketPriceRepository.SearchAsync(x => x.ProjectId == dock.ProjectId && x.IsActive && x.TicketType == ETicketType.Block)).OrderBy(x => x.TicketValue).FirstOrDefault();
                    if (ticketPrice != null)
                    {
                        return await BookingCheckIn(accountId, dock.SerialNumber, ticketPrice.Id, 0, dock.Id, $"{dock.Lat};{dock.Long}", rfid, requestId, functionName, true);
                    }
                    else
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BookingReOpen : Mở lại khi đang đi mà đóng khóa]

        public async Task<ApiResponse<object>> BookingReOpenAll(int accountId)
        {
           var requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
           var functionName = $"BookingService.BookingReOpenAll";

            try
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} quẹt mở lại khóa all", "");

                var bookings = await _iBookingRepository.SearchAsync(x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (!bookings.Any())
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy chuyến xe đang đi hoặc không phả xe bạn đang đặt", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.NotExistBooking);
                }

                foreach(var booking in bookings)
                {
                    // Loại bỏ nghi vấn nếu kh mở khóa
                    if (booking.IsForgotrReturnTheBike)
                    {
                        await _iBookingRepository.CancelForgotrReturnTheBike(booking.Id);
                    }
                    var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == booking.DockId);
                    if (dock == null)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Khóa không tồn tại", booking.TransactionCode);
                        return ApiResponse.WithStatus(false, null, ResponseCode.KhongPhaiXeBanMuonMoLai);
                    }

                    if ((dock.ConnectionStatus == false && dock.Battery < 1) || (dock.Battery < 1))
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {dock.SerialNumber} hết pin, END", "");
                        return ApiResponse.WithStatus(false, null, ResponseCode.XeHetPin);
                    }

                    if(dock.LockStatus)
                    {
                        // Gửi lệnh mở khóa đợi phản hồi mới cho phép oke với điều kiện là khóa đang đóng
                        _ = await OpenLock(dock, booking.TransactionCode, accountId, null);
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Gửi lệnh mở khóa giao dịch {booking.TransactionCode}", "");
                    }
                }

                foreach (var booking in bookings)
                {
                    int retries = _baseSetting.Value.NumRetryOpenDock > 0 ? _baseSetting.Value.NumRetryOpenDock : 30;
                    bool isOpen = false;
                    for (int i = 0; i <= retries; i++)
                    {
                        var dockx = await _iDockRepository.SearchOneAsync(true, x => x.Id == booking.DockId);
                        isOpen = dockx.LockStatus == false;
                        if (isOpen)
                        {
                            AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Break retries lần {i}", booking.TransactionCode);
                            break;
                        }
                        else
                        {
                            await Task.Delay(500);
                        }
                    }
                }    
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public async Task<ApiResponse<object>> BookingReOpen(string serialNumber, int accountId, int bookingId, string requestId = null, string functionName = null)
        {
            requestId ??= $"{DateTime.Now:yyyyMMddHHMMssfff}";
            functionName ??= $"BookingService.BookingReOpen";
            try
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} quẹt mở lại khóa {serialNumber}", "");

                var booking = await _iBookingRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.Id == bookingId && x.Status == EBookingStatus.Start);
                if (booking == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy chuyến xe đang đi hoặc không phả xe bạn đang đặt", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.NotExistBooking);
                }

                // Loại bỏ nghi vấn nếu kh mở khóa
                if(booking.IsForgotrReturnTheBike)
                {
                    await _iBookingRepository.CancelForgotrReturnTheBike(bookingId);
                } 
                
                var dock = await _iDockRepository.SearchOneAsync(true, x => (x.SerialNumber == serialNumber || x.IMEI == serialNumber) && x.Id == booking.DockId);
                if (dock == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Khóa không tồn tại", booking.TransactionCode);
                    return ApiResponse.WithStatus(false, null, ResponseCode.KhongPhaiXeBanMuonMoLai);
                }

                if ((dock.ConnectionStatus == false && dock.Battery < 1) || (dock.Battery < 1))
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {serialNumber} hết pin, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.XeHetPin);
                }

                if(!dock.LockStatus)
                {
                    return ApiResponse.WithData(booking);
                }

                // Gửi lệnh mở khóa
                _ = await OpenLock(dock, booking.TransactionCode, accountId, null);
                // Đợi đủ điều kiện thì ta tiến hành mở khóa
                bool isOpen = false;
                int retries = _baseSetting.Value.NumRetryOpenDock > 0 ? _baseSetting.Value.NumRetryOpenDock : 30;
                for (int i = 0; i <= retries; i++)
                {
                    var dockx = await _iDockRepository.SearchOneAsync(true, x => x.Id == dock.Id);
                    isOpen = dockx.LockStatus == false;
                    if (isOpen)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Break retries lần {i}", booking.TransactionCode);
                        break;
                    }
                    else
                    {
                        await Task.Delay(500);
                    }
                }

                if (!isOpen)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mở khóa không thành công", booking.TransactionCode);
                    var bike = await _iDockRepository.BikeByDock(dock.Id);
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_3001_KhachHangMoXeKhongThanhCong, EEventSystemWarningType.Warning, accountId, booking.TransactionCode, dock.ProjectId, dock.StationId, bike?.Id ?? 0, dock.Id,
                       $"Khách hàng mở lại khóa không thành công, mã chuyến đi '{booking.TransactionCode}' xe '{bike?.Plate}', barcode '{dock.SerialNumber}'");
                    return ApiResponse.WithStatus(false, booking, ResponseCode.CantOpen);
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mở khóa thành công", booking.TransactionCode);
                    return ApiResponse.WithData(booking);
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [RFID Booking reOpen]
        public async Task<ApiResponse<object>> RFIDBookingReOpen(string rfid ,string serialNumber, int accountId, int bookingId, string requestId = null, string functionName = null)
        {
            try
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} quẹt mở lại khóa rfid {rfid} của khóa {serialNumber}", "");
                return await BookingReOpen(serialNumber, accountId, bookingId, requestId, functionName);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BookingCancel : Hủy chuyến đi]
        public async Task<ApiResponse<object>> BookingCancel(BookingCancelCommand cm, int accountId)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functionName = "BookingService.BookingCancel";
            try
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} chọn mã booking {cm.BookingId} feedback {cm.Feedback}", "");
                // Chỉ được hủy khi ở trạng thái đang đi
                var booking = await _iBookingRepository.SearchOneAsync(false, x => x.Id == cm.BookingId && x.Status == EBookingStatus.Start);
                if (booking == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, "Không giao dịch đang đi, END", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }
                var totalMi = (DateTime.Now - booking.StartTime).TotalMinutes;
                // Check thời gian hủy
                if (totalMi > _baseSetting.Value.CancelForTime)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Vượt quá thời gian có thể hủy {totalMi}/{_baseSetting.Value.CancelForTime}, END", booking.TransactionCode);
                    return ApiResponse.WithStatus(false, null, ResponseCode.VuotQuaThoiGianCoTheHuy);
                }
                var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == booking.DockId);
                if (dock == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy khóa, END", booking.TransactionCode);
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockNotExist);
                }
                // Check xem có đang ở trong trạm không
                var checkInStation = await InStationVerified($"{dock.Lat};{dock.Long}", booking?.StationIn ?? 0, accountId, functionName, requestId, booking.DockId);
                if (!checkInStation.Item1.Status)
                {
                    await _iHubService.EventSend(EEventSystemType.M_3007_KhachHangCoKetThucGiaoDichKhongThanhCong, EEventSystemWarningType.Info, accountId, null, dock.ProjectId, booking.StationIn, booking.Id, dock.Id,
                        $"Khách hàng hủy chuyến không ở trong trạm {checkInStation.Item2}/{checkInStation.Item3}, Chuyển đi '{booking.TransactionCode}'");
                    return checkInStation.Item1;
                }

                dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == dock.Id);
                if (!dock.LockStatus)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Khóa {booking.DockId} chưa thấy đóng", booking.TransactionCode);
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockNotClose);
                }

                booking.Status = EBookingStatus.Cancel;
                booking.Feedback = cm.Feedback;
                booking.TotalMinutes = Convert.ToInt32((DateTime.Now - booking.StartTime).TotalMinutes);
                booking.EndTime = DateTime.Now;
                booking.StationOut = checkInStation.Item2;
                _iBookingRepository.Update(booking);
                await _iBookingRepository.CommitAsync();

                await _iDockRepository.SetStationId(booking.DockId, checkInStation.Item2);

                var dataAdd = await MoveBookingToTransaction(booking, 0);

                if (dataAdd != null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Tạo giao dịch Hủy Transaction {dataAdd.Id}, END", booking.TransactionCode);
                    await _iFireBaseService.SendNotificationByAccount("BookingService.BookingCancel", requestId, accountId, ENotificationTempType.HuyChuyenDi, false, DockEventType.Not, null, booking.TransactionCode);
                    // Update số lượng xe free trong trạm
                    await _iFireBaseService.NotifyRefeshStation(functionName, requestId, booking.StationOut ?? 0, "all");
                   // await FilterGPS(functionName, requestId, dataAdd.Id, dataAdd.TransactionCode, dock.Id, dataAdd.StartTime, dataAdd.EndTime ?? DateTime.Now, accountId);
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_3003_KetThucGiaoDichThanhCong, EEventSystemWarningType.Info, accountId, booking.TransactionCode, dock.ProjectId, dock.StationId, booking.BikeId, booking.DockId,
                       $"Khách hàng hủy chuyến thành công, mã chuyến '{booking.TransactionCode}'");
                    return ApiResponse.WithData(dataAdd);
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Add transaction thất bại, END", booking.TransactionCode);
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [CheckBikeInStation : Kiểm tra xe có đang ở trong trạm không]
        public async Task<ApiResponse<object>> CheckBikeInStation(int bookingId, int accountId)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            string functioName = "BookingService.CheckBikeInStation";
            try
            {
                var booking = await _iBookingRepository.SearchOneAsync(true, x => x.Id == bookingId && x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (booking == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == booking.DockId);
                if (dock == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockNotExist);
                }

                var checkInStation = await InStationVerified($"{dock.Lat};{dock.Long}", booking?.StationIn ?? 0, accountId, functioName, requestId, booking.DockId);
                if (!checkInStation.Item1.Status)
                {
                    return checkInStation.Item1;
                }
                var station = await _iStationRepository.SearchOneAsync(true, x => x.Id == checkInStation.Item2);
                return ApiResponse.WithData($"{station?.Id}, {station?.Name}");

            }
            catch (Exception ex)
            {
                AddLog(accountId, functioName, requestId, EnumMongoLogType.End, $"{ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region[BookingCheckOutAll : Kết thúc chuyến đi]
        public async Task<ApiResponse<object>> BookingCheckOutAll(int accountId)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            var functionName = "BookingService.BookingCheckOutAll";
            try
            {
                var bookings = await _iBookingRepository.SearchAsync(x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (!bookings.Any())
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm  thấy chuyến đi đang đi", "");
                    return ApiResponse.WithStatus(false, null, ResponseCode.BookingNotExist);
                }
                var bookingIds = bookings.Select(x => x.DockId);
                // Kiểm tra xem tất cả xe đang khóa không
                var docks = await _iDockRepository.SearchAsync(x => bookingIds.Contains(x.Id));
                var dockNotClose = docks.Any(x => !x.LockStatus);
                if(dockNotClose)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Trong số xe thuê có xe chưa khóa", null);
                    return ApiResponse.WithStatus(false, null, ResponseCode.DockNotClose);
                }
                 
                var dockBatteryLower = docks.Any(dock => (dock.ConnectionStatus == false && dock.Battery < 1) || (dock.Battery < 1));
                if (dockBatteryLower)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Mất kết nối do hết pin hoặc gần hết pin", null);
                    return ApiResponse.WithStatus(false, null, ResponseCode.XeHetPin);
                }

                var dockDiscolect = docks.Any(dock => dock.ConnectionStatus == false);
                if (dockDiscolect)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Xe mất kết nối", null);
                    return ApiResponse.WithStatus(false, null, ResponseCode.XeHetPin);
                }

                // Recheck có tồn tại một lệnh mở chưa phản hồi về chưa
                foreach (var booking in bookings)
                {
                    if (string.IsNullOrEmpty(booking.OpenLockTransaction) || !_baseSetting.Value.AddTransactionOpenLock)
                    {
                        continue;
                    }
                    var checkOpenLockRet = await _iOpenLockRequestRepository.IsOpenLockHistory(booking.TransactionCode, booking.AccountId, booking.OpenLockTransaction);
                    if (!checkOpenLockRet)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Khóa {booking.DockId} chưa thấy đóng vì không tìm thấy phản hồi {booking.OpenLockTransaction}", booking.TransactionCode);
                        return ApiResponse.WithStatus(false, null, ResponseCode.DockNotCloseKeyOpen);
                    }
                }

                foreach (var dock in docks)
                {
                    var checkInStation = await InStationVerified($"{dock.Lat};{dock.Long}", 0, accountId, functionName, requestId, dock.Id);
                    if (!checkInStation.Item1.Status)
                    {
                        return checkInStation.Item1;
                    }
                }

                int totalSuccess = 0;
                var listTran = new List<TransactionDTO>();
                decimal totalPrice = 0;
                decimal totalDebit = 0;
                foreach (var item in bookings)
                {
                   var outCall = await BookingCheckOut(item.Id, accountId, requestId, functionName, false, false);
                    if(outCall.Status)
                    {
                        totalSuccess++;
                        totalPrice += outCall.Data.TotalPrice;
                        totalDebit += outCall.Data.Debt;
                        listTran.Add(outCall.Data);                    
                    }
                    await Task.Delay(1);
                }
                if (totalDebit <= 0)
                {
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.KetThucChuyenDi, false, DockEventType.Not, null, $"{ Function.FormatMoney(totalPrice) }");
                }
                else
                {
                    await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.KetThucChuyenDiNoCuoc, false, DockEventType.Not, null, $"{ Function.FormatMoney(totalPrice) }", $"{ Function.FormatMoney(totalDebit) }");
                }
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Có {totalSuccess} chuyến trả xe thành công", null);
                return ApiResponse.WithStatusOk(listTran);
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Error {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region[BookingCheckOut : Kết thúc chuyến đi]
        public async Task<ApiResponse<TransactionDTO>> BookingCheckOut(int bookingId, int accountId, string requestId = null, string functionName = null, bool isEndRFID = false, bool sendNotifi = true)
        {
            requestId ??= $"{DateTime.Now:yyyyMMddHHMMssfff}";
            functionName ??= "BookingService.BookingCheckOut";
            decimal debit = 0;
            AddLog(accountId, functionName, requestId, EnumMongoLogType.Start, $"Account: {accountId} thực hiện kết thúc chuyên đi {bookingId}", "");
            try
            {
                var booking = await _iBookingRepository.SearchOneAsync(false, x => x.Id == bookingId && x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (booking == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm  thấy chuyến đi đang đi", "");
                    return ApiResponseT<TransactionDTO>.WithStatus(false, null, ResponseCode.BookingNotExist);
                }
                var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == booking.DockId);
                if (dock == null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không tìm thấy dock {booking.DockId}", booking.TransactionCode);
                    return ApiResponseT<TransactionDTO>.WithStatus(false, null, ResponseCode.DockNotExist);
                }

                if ((dock.ConnectionStatus == false && dock.Battery < 1) || (dock.Battery < 1))
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {dock.SerialNumber} hết pin, END", "");
                    return ApiResponseT<TransactionDTO>.WithStatus(false, null, ResponseCode.XeHetPin);
                }

                if (dock.ConnectionStatus == false)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Lock {dock.SerialNumber} mất kết nối, END", "");
                    return ApiResponseT<TransactionDTO>.WithStatus(false, null, ResponseCode.BikeIsError, "Xe tạm thời mất kết nối, vui lòng thử lại sau ít phút hoặc gọi cho hotline để được hỗ trợ");
                }

                var checkInStation = await InStationVerified($"{dock.Lat};{dock.Long}", booking?.StationIn ?? 0, accountId, functionName, requestId, booking.DockId);
                if (!checkInStation.Item1.Status)
                {
                    await _iHubService.EventSend(
                        EEventSystemType.M_3007_KhachHangCoKetThucGiaoDichKhongThanhCong, 
                        EEventSystemWarningType.Warning, accountId, null, dock.ProjectId, booking.StationIn, booking.Id, dock.Id,
                    $"Khách hàng trả xe không trong trạm {checkInStation.Item2}/{checkInStation.Item3}, chuyển đi '{booking.TransactionCode}'");
                    return ApiResponseT<TransactionDTO>.WithStatus(checkInStation.Item1.Status, null, checkInStation.Item1.ErrorCode);
                }

                dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == dock.Id);
                if (!dock.LockStatus)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Khóa {booking.DockId} chưa thấy đóng", booking.TransactionCode);
                    return ApiResponseT<TransactionDTO>.WithStatus(false, null, ResponseCode.DockNotClose);
                }

                if(!string.IsNullOrEmpty(booking.OpenLockTransaction) && _baseSetting.Value.AddTransactionOpenLock)
                {
                    //Check thêm 1 lần nữa xem khóa có thực sự là đang vương 1 transaction mở khóa không
                    var checkOpenLockRet = await _iOpenLockRequestRepository.IsOpenLockHistory(booking.TransactionCode, booking.AccountId, booking.OpenLockTransaction);
                    if (!checkOpenLockRet)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Khóa {booking.DockId} chưa thấy đóng vì không tìm thấy phản hồi {booking.OpenLockTransaction}", booking.TransactionCode);
                        return ApiResponseT<TransactionDTO>.WithStatus(false, null, ResponseCode.DockNotCloseKeyOpen);
                    }
                }    
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Giao dịch có mã mở khóa rỗng => chưa Re Open lần nào", booking.TransactionCode);
                }    
                // Lấy ví của người dùng
                var wInfo = await _iWalletRepository.GetWalletAsync(accountId);
                booking.EndTime = DateTime.Now;
                // Tính tổng số tiền
                var totalPriceObj = _iTransactionRepository.ToTotalPrice(
                    booking.TicketPrice_TicketType, booking.StartTime, 
                    booking.EndTime.Value, 
                    booking.TicketPrice_TicketValue, 
                    booking.TicketPrice_LimitMinutes, 
                    booking.TicketPrice_BlockPerMinute, 
                    booking.TicketPrice_BlockPerViolation, 
                    booking.TicketPrice_BlockViolationValue, 
                    booking.Prepaid_EndTime ?? DateTime.Now, 
                    booking.Prepaid_MinutesSpent,
                    new DiscountCode { Percent = booking.DiscountCode_Percent ?? 0, Point = booking.DiscountCode_Point ?? 0, Type = booking.DiscountCode_Type ?? EDiscountCodeType.Point, Id = booking.DiscountCodeId ?? 0 }
                    );
                // Chia tiền 2 loại tài khoản
                // Có 2 trường hợp chia 1 là ko đủ thành toán, 2 là đủ thành toán, với trường hợp không đủ thanh toán thì ta phải ưu tiên nợ tài khoản km
                
                var splitTotalPrice = _iWalletRepository.SplitTotalPrice(totalPriceObj.Price, booking.TicketPrice_AllowChargeInSubAccount, wInfo);
                if ((wInfo.Balance + wInfo.SubBalance) < totalPriceObj.Price)
                {
                    debit = totalPriceObj.Price - (wInfo.Balance + wInfo.SubBalance);
                }
                // Chi phí thực tế
                booking.ChargeInAccount = splitTotalPrice.Item1;
                booking.ChargeInSubAccount = splitTotalPrice.Item2 - debit;
                // Chi phí ước tính đủ
                booking.EstimateChargeInAccount = splitTotalPrice.Item1;
                booking.EstimateChargeInSubAccount = splitTotalPrice.Item2;
                booking.TotalPrice = totalPriceObj.Price;
                booking.DiscountCode_Price = totalPriceObj.DiscountPrice;
                booking.StationOut = checkInStation.Item2;
                booking.DateOfPayment = DateTime.Now;
                booking.Status = EBookingStatus.End;
               
                // Nợ cước sẽ ko cho + chuyến đi
                if(debit <= 0)
                {
                    booking.TripPoint = booking.TicketPrice_TicketType != ETicketType.Block ? 0 : ((totalPriceObj.TotalPrice / 1000) * _baseSetting.Value.AddTripPoint);
                }

                booking.TotalMinutes = Function.TotalMilutes(booking.StartTime, booking.EndTime.Value);
                booking.KCal = 0;
                booking.KG = 0;
                booking.KM = 0;
                booking.Status = debit > 0 ? EBookingStatus.Debt : EBookingStatus.End;
                booking.IsEndRFID = isEndRFID;
                _iBookingRepository.Update(booking);
                await _iBookingRepository.CommitAsync();

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Cập nhật thành công cho booking", booking.TransactionCode);

                if (booking.TicketPrice_TicketType != ETicketType.Block)
                {
                    // Trừ số lượng phút của ngày
                    await _iTicketPriceRepository.PrepaidUpdatePriceTypeDay(accountId, booking.TicketPrepaidId, booking.TotalMinutes);
                }
                 
                // Chuyển qua bản transactionđasass
                var outTransaction = await MoveBookingToTransaction(booking, debit);
                if (outTransaction != null)
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Tạo dữ liệu giao dịch ({outTransaction.Id},{outTransaction.TransactionCode})", booking.TransactionCode);
                    var wt = new WalletTransaction
                    {
                        AccountId = accountId,
                        CampaignId = outTransaction.DiscountCode_CampaignId ?? 0,
                        CreatedDate = DateTime.Now,
                        Status = EWalletTransactionStatus.Done,
                        TransactionCode = booking.TransactionCode,
                        Note = $"Trừ điểm chuyến đi, loại vé {outTransaction.TicketPrice_TicketType.GetDisplayName()} { (debit > 0 ? $", khách hàng nợ {Function.FormatMoney(debit)} điểm cho chuyến đi này" : "")} { (outTransaction.DiscountCode_Price > 0 ? $", giảm giá {outTransaction.DiscountCode_Price}" : "")}",
                        TransactionId = outTransaction.Id,
                        Type = EWalletTransactionType.Paid,
                        WalletId = wInfo.Id,
                        OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.Paid),
                        Amount = splitTotalPrice.Item1,
                        IsAsync = false,
                        SubAmount = splitTotalPrice.Item2 - debit,
                        TotalAmount = totalPriceObj.Price,
                        TripPoint = outTransaction.TripPoint,
                        DepositEvent_SubAmount = 0,
                        ReqestId = requestId,
                        PadTime = DateTime.Now,
                        DebtAmount = debit,
                        Wallet_Balance = wInfo.Balance,
                        Wallet_SubBalance = wInfo.SubBalance,
                        Wallet_HashCode = wInfo.HashCode,
                        Wallet_TripPoint = wInfo.TripPoint,
                        DepositEventId = outTransaction.DiscountCodeId ?? 0,
                        VoucherCodeId = outTransaction.DiscountCodeId ?? 0,
                        DepositEvent_Amount = totalPriceObj.DiscountPrice
                    };
                    wt.HashCode = Function.TransactionHash(wt.OrderIdRef, wt.Amount, wt.SubAmount, wt.TripPoint, _baseSetting.Value.TransactionSecretKey);
                    await _iIWalletTransactionRepository.AddAsync(wt);
                    await _iIWalletTransactionRepository.CommitAsync();

                    AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Trừ điểm trong ví ({totalPriceObj}) trừ điểm tỉ lệ ({splitTotalPrice.Item1},{splitTotalPrice.Item2})", booking.TransactionCode);
                }
                // Cập nhật ví tiền
                wInfo = await _iWalletRepository.UpdateAddWalletAsync(accountId, -splitTotalPrice.Item1, -splitTotalPrice.Item2, outTransaction.TripPoint, _baseSetting.Value.TransactionSecretKey, false, false, 0);

                await _iWalletRepository.AddDayExpirys(accountId, outTransaction.TotalPrice, 0, true);

                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Trừ tiền trong ví ({-splitTotalPrice.Item1},{-(splitTotalPrice.Item2 - debit)},{outTransaction.TripPoint}), ví sau trừ {Newtonsoft.Json.JsonConvert.SerializeObject(wInfo)}", booking.TransactionCode);
                // Update cho xe về với trạm trả xe
                await _iDockRepository.SetStationId(dock.Id, checkInStation.Item2);

                var stationIn = await _iMemoryCacheService.StationGetById(outTransaction.StationIn ?? 0);

                var stationOut = await _iMemoryCacheService.StationGetById(outTransaction.StationOut ?? 0);

                // Update số lượng xe free trong trạm
                await _iFireBaseService.NotifyRefeshStation(functionName, requestId, outTransaction.StationOut ?? 0, "all");

                if(sendNotifi)
                {
                    if (debit <= 0)
                    {
                        await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.KetThucChuyenDi, false, DockEventType.Not, null, $"{ Function.FormatMoney(outTransaction.TotalPrice) }");
                    }
                    else
                    {
                        await _iFireBaseService.SendNotificationByAccount(functionName, requestId, accountId, ENotificationTempType.KetThucChuyenDiNoCuoc, false, DockEventType.Not, null, $"{ Function.FormatMoney(outTransaction.TotalPrice) }", $"{ Function.FormatMoney(debit) }");
                    }
                }    

                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Kết thúc chuyến đi tìm thấy", booking.TransactionCode);
                // Gửi cho admin phần quản lý
                await _iHubService.EventSend(EEventSystemType.M_3003_KetThucGiaoDichThanhCong, EEventSystemWarningType.Info, accountId, booking.TransactionCode, dock.ProjectId, dock.StationId, booking.BikeId, booking.DockId, $"Kết thúc chuyến đi thành công, mã chuyến đi '{booking.TransactionCode}'");
                outTransaction.Bike = await _iDockRepository.BikeById(outTransaction.BikeId);
                outTransaction.Dock = dock;
                return ApiResponseT<TransactionDTO>.WithData(new TransactionDTO(outTransaction, new StationViewDTO(stationIn), new StationViewDTO(stationOut), new List<GPSData>()));
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Error {ex}", "", true);
                return ApiResponseT<TransactionDTO>.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [Tính tổng số điểm ước tính khác phải thanh toán]
        public async Task<Tuple<decimal, int, int>> CurrentTotalPrice(int accountId)
        {
            var bookings = await _iBookingRepository.SearchAsync(x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
            if (!bookings.Any())
            {
                return new Tuple<decimal, int, int>(0,0,0) ;
            }
            decimal totalPrice = 0;
            int prepaid_MinutesSpent = bookings.FirstOrDefault().Prepaid_MinutesSpent;
            int totalMinutes = 0;
            foreach (var booking in bookings)
            {
                var endTime = DateTime.Now;
                if (booking.TicketPrice_TicketType == ETicketType.Block)
                {
                    var minutes = Function.TotalMilutes(booking.StartTime, endTime);
                    totalMinutes += minutes;
                    totalPrice += _iTransactionRepository.ToTotalPrice(
                        booking.TicketPrice_TicketType,
                        booking.StartTime,
                        endTime,
                        booking.TicketPrice_TicketValue,
                        booking.TicketPrice_LimitMinutes,
                        booking.TicketPrice_BlockPerMinute,
                        booking.TicketPrice_BlockPerViolation,
                        booking.TicketPrice_BlockViolationValue,
                        booking.Prepaid_EndTime ?? DateTime.Now,
                        booking.Prepaid_MinutesSpent,
                        new DiscountCode { Percent = booking.DiscountCode_Percent ?? 0, Point = booking.DiscountCode_Point ?? 0, Type = booking.DiscountCode_Type ?? EDiscountCodeType.Point, Id = booking.DiscountCodeId ?? 0 }
                      ).Price;
                }    
                else
                {
                    var minutes = Function.TotalMilutes(booking.StartTime, endTime);
                    totalMinutes += minutes;
                    totalPrice += _iTransactionRepository.ToTotalPrice(
                        booking.TicketPrice_TicketType,
                        booking.StartTime,
                        endTime,
                        booking.TicketPrice_TicketValue,
                        booking.TicketPrice_LimitMinutes,
                        booking.TicketPrice_BlockPerMinute,
                        booking.TicketPrice_BlockPerViolation,
                        booking.TicketPrice_BlockViolationValue,
                        booking.Prepaid_EndTime ?? DateTime.Now,
                        prepaid_MinutesSpent, null).Price;

                    prepaid_MinutesSpent += minutes;
                }    
            }
            return new Tuple<decimal, int, int>(totalPrice, totalMinutes, bookings.Count);
        }
        #endregion

        #region [BookingCurrent : Chuyến đi hiện tại]
        public async Task<ApiResponse<object>> BookingCurrent(int accountId)
        {
            string functionName = "BookingService.BookingCurrent";
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            try
            {
                var booking = await _iBookingRepository.SearchOneAsync(true, x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (booking == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.BookingNotExist);
                }

                var dataT = _iTransactionRepository.ToTotalPrice(
                        booking.TicketPrice_TicketType,
                        booking.StartTime,
                        DateTime.Now,
                        booking.TicketPrice_TicketValue,
                        booking.TicketPrice_LimitMinutes,
                        booking.TicketPrice_BlockPerMinute,
                        booking.TicketPrice_BlockPerViolation,
                        booking.TicketPrice_BlockViolationValue,
                        booking.Prepaid_EndTime ?? DateTime.Now,
                        booking.Prepaid_MinutesSpent,
                        new DiscountCode { Percent = booking.DiscountCode_Percent ?? 0, Point = booking.DiscountCode_Point ?? 0, Type = booking.DiscountCode_Type ?? EDiscountCodeType.Point, Id = booking.DiscountCodeId ?? 0 }
                        );

                var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == booking.DockId);

                var bike = await _iDockRepository.BikeByDock(dock?.Id ?? 0);

                var ticketPrice = new TicketPrice
                {
                    AllowChargeInSubAccount = booking.TicketPrice_AllowChargeInSubAccount,
                    BlockPerMinute = booking.TicketPrice_BlockPerMinute,
                    BlockPerViolation = booking.TicketPrice_BlockPerViolation,
                    BlockViolationValue = booking.TicketPrice_BlockViolationValue,
                    CustomerGroupId = booking.CustomerGroupId,
                    LimitMinutes = booking.TicketPrice_LimitMinutes,
                    ProjectId = booking.ProjectId,
                    TicketValue = booking.TicketPrice_TicketValue,
                    TicketType = booking.TicketPrice_TicketType,
                    IsDefault = booking.TicketPrice_IsDefault
                };

                var mv = new TicketPriceBindModel
                {
                    Name = "",
                    Note = ticketPrice.TicketType == ETicketType.Day ? _iTransactionRepository.GetTicketPriceString(ticketPrice, ResponseCode.NoiDungChiTietVeNgay.ResponseCodeToString(), AppHttpContext.AppLanguage) : (ticketPrice.TicketType == ETicketType.Month ? _iTransactionRepository.GetTicketPriceString(ticketPrice, ResponseCode.NoiDungChiTietVeThang.ResponseCodeToString(), AppHttpContext.AppLanguage) : _iTransactionRepository.GetTicketPriceString(ticketPrice, ResponseCode.NoiDungChiTietVe.ResponseCodeToString(), AppHttpContext.AppLanguage)),
                    Order = 1,
                    TicketPrepaidId = booking.TicketPrepaidId,
                    TicketPriceId = booking.TicketPriceId
                };
                return ApiResponse.WithData(new CurrentBookingDataDTO(booking, dataT.Price, new SelectStationBikeDTO(bike, dock, mv, 0, 0), new List<GPSData>(), null));
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Error {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BookingsCurrent : Chuyến đi hiện tại]
        public async Task<ApiResponse<object>> BookingsCurrent(int accountId)
        {
            string functionName = "BookingService.BookingCurrent";
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            try
            {
                var timeNow = DateTime.Now;
                var bookings = await _iBookingRepository.SearchAsync(x => x.AccountId == accountId && x.Status == EBookingStatus.Start);
                if (!bookings.Any())
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.BookingNotExist);
                }
                var listOut = new List<CurrentBookingDataDTO>();
                int totalMinutes = 0, prepaid_MinutesSpent = bookings.FirstOrDefault()?.Prepaid_MinutesSpent ?? 0;
                decimal totalPrice = 0;
                foreach(var booking in bookings)
                {
                    var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == booking.DockId);
                    var bike = await _iDockRepository.BikeByDock(dock?.Id ?? 0);

                    var ticketPrice = new TicketPrice
                    {
                        AllowChargeInSubAccount = booking.TicketPrice_AllowChargeInSubAccount,
                        BlockPerMinute = booking.TicketPrice_BlockPerMinute,
                        BlockPerViolation = booking.TicketPrice_BlockPerViolation,
                        BlockViolationValue = booking.TicketPrice_BlockViolationValue,
                        CustomerGroupId = booking.CustomerGroupId,
                        LimitMinutes = booking.TicketPrice_LimitMinutes,
                        ProjectId = booking.ProjectId,
                        TicketValue = booking.TicketPrice_TicketValue,
                        TicketType = booking.TicketPrice_TicketType,
                        IsDefault = booking.TicketPrice_IsDefault
                    };

                    if (booking.TicketPrice_TicketType == ETicketType.Block)
                    {
                        totalMinutes += Function.TotalMilutes(booking.StartTime, timeNow);

                        var price = _iTransactionRepository.ToTotalPrice(
                           booking.TicketPrice_TicketType,
                           booking.StartTime,
                           timeNow,
                           booking.TicketPrice_TicketValue,
                           booking.TicketPrice_LimitMinutes,
                           booking.TicketPrice_BlockPerMinute,
                           booking.TicketPrice_BlockPerViolation,
                           booking.TicketPrice_BlockViolationValue,
                           booking.Prepaid_EndTime ?? DateTime.Now,
                           booking.Prepaid_MinutesSpent,
                           new DiscountCode { Percent = booking.DiscountCode_Percent ?? 0, Point = booking.DiscountCode_Point ?? 0, Type = booking.DiscountCode_Type ?? EDiscountCodeType.Point, Id = booking.DiscountCodeId ?? 0 }
                           );

                        totalPrice += price.Price;

                        var mv = new TicketPriceBindModel
                        {
                            Name = "",
                            Note = _iTransactionRepository.GetTicketPriceString(ticketPrice, ResponseCode.NoiDungChiTietVeNgay.ResponseCodeToString(), AppHttpContext.AppLanguage),
                            Order = 1,
                            TicketPrepaidId = booking.TicketPrepaidId,
                            TicketPriceId = booking.TicketPriceId
                        };
                        string discountValue = null;
                        if(booking.DiscountCodeId > 0 && booking.DiscountCode_Type == EDiscountCodeType.Point)
                        {
                            discountValue = $"{Function.FormatMoney(booking.DiscountCode_Point)}";
                        }    
                        else if(booking.DiscountCodeId > 0 && booking.DiscountCode_Type == EDiscountCodeType.Percent)
                        {
                            discountValue = $"{booking.DiscountCode_Percent}%";
                        }
                        listOut.Add(new CurrentBookingDataDTO(booking, price.Price, new SelectStationBikeDTO(bike, dock, mv, 0, 0), new List<GPSData>(), discountValue));
                    }   
                    else
                    {
                        totalMinutes += Function.TotalMilutes(booking.StartTime, timeNow);

                        totalPrice += _iTransactionRepository.ToTotalPrice(
                            booking.TicketPrice_TicketType,
                            booking.StartTime,
                            timeNow,
                            booking.TicketPrice_TicketValue,
                            booking.TicketPrice_LimitMinutes,
                            booking.TicketPrice_BlockPerMinute,
                            booking.TicketPrice_BlockPerViolation,
                            booking.TicketPrice_BlockViolationValue,
                            booking.Prepaid_EndTime ?? DateTime.Now,
                            prepaid_MinutesSpent, null).Price;

                        prepaid_MinutesSpent += totalMinutes;

                        var mv = new TicketPriceBindModel
                        {
                            Name = "",
                            Note = ticketPrice.TicketType == ETicketType.Month ? _iTransactionRepository.GetTicketPriceString(ticketPrice, ResponseCode.NoiDungChiTietVeThang.ResponseCodeToString(), AppHttpContext.AppLanguage) : _iTransactionRepository.GetTicketPriceString(ticketPrice, ResponseCode.NoiDungChiTietVe.ResponseCodeToString(), AppHttpContext.AppLanguage),
                            Order = 1,
                            TicketPrepaidId = booking.TicketPrepaidId,
                            TicketPriceId = booking.TicketPriceId
                        };
                        listOut.Add(new CurrentBookingDataDTO(booking, 0, new SelectStationBikeDTO(bike, dock, mv, 0, 0), new List<GPSData>(), null));
                    }
                }  
               
                return ApiResponse.WithData(new CurrentBookingDTO { List = listOut, Count = listOut.Count, TotalMinutes = totalMinutes, TotalPrice = totalPrice });
            }
            catch (Exception ex)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Error {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BookingFeedback : Feedback nội dung kết thúc chuyến đi]
        public async Task<ApiResponse<object>> BookingFeedback(int accountId, BookingFeedbackCommand cm)
        {
            try
            {
                var transaction = await _iTransactionRepository.SearchOneAsync(false, x => x.Id == cm.TransactionId && x.AccountId == accountId);
                if (transaction == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }

                var ktVote = await _iFeedbackRepository.SearchOneAsync(true, x => x.TransactionId == cm.TransactionId);
                if (ktVote != null)
                {
                    return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
                }

                transaction.Feedback = cm.Content;
                transaction.Vote = cm.Vote;
                _iTransactionRepository.Update(transaction);
                await _iTransactionRepository.CommitAsync();

                var feetbackAdd = new Feedback
                {
                    AccountId = transaction.AccountId,
                    Status = Feedback.EFeedbackStatus.UnRead,
                    CreatedDate = DateTime.Now,
                    Message = cm.Content,
                    Type = Feedback.FeedbackType.Transaction,
                    Rating = cm.Vote,
                    TransactionCode = transaction.TransactionCode,
                    TransactionId = transaction.Id
                };
                await _iFeedbackRepository.AddAsync(feetbackAdd);
                await _iFeedbackRepository.CommitAsync();

                if (feetbackAdd.TransactionId > 0)
                {
                    var wt = EEventSystemWarningType.Info;
                    if (feetbackAdd.Rating == 1 || feetbackAdd.Rating == 2)
                    {
                        wt = EEventSystemWarningType.Danger;
                    }
                    else if (feetbackAdd.Rating == 3 || feetbackAdd.Rating == 4)
                    {
                        wt = EEventSystemWarningType.Warning;
                    }
                    else
                    {
                        wt = EEventSystemWarningType.Info;
                    }
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_4002_PhanHoiChuyenDi, wt, accountId, feetbackAdd.TransactionCode, transaction.ProjectId, transaction.StationOut ?? 0, transaction.BikeId, transaction.DockId, $"Khách hàng đánh giá {feetbackAdd.Rating} sao, nội dung '{feetbackAdd.Message}' chuyến đi '{feetbackAdd.TransactionCode}'");
                }
                return ApiResponse.WithData(new FeedbackDTO(feetbackAdd));
            }
            catch (Exception ex)
            {
                AddLog(accountId, "BookingService.BookingFeedback", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"Error {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BookingFeedback : Feedback nội dung kết thúc chuyến đi]
        public async Task<ApiResponse<object>> BookingsFeedback(int accountId, BookingsFeedbackCommand cm)
        {
            try
            {
                if(cm.TransactionIds == null)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.InvalidData);
                }    

                foreach(var transactionId in cm.TransactionIds)
                {
                    var transaction = await _iTransactionRepository.SearchOneAsync(false, x => x.Id == transactionId && x.AccountId == accountId);
                    if (transaction == null)
                    {
                        continue;
                    }

                    var ktVote = await _iFeedbackRepository.SearchOneAsync(true, x => x.TransactionId == transactionId);
                    if (ktVote != null)
                    {
                        continue;
                    }

                    transaction.Feedback = cm.Content;
                    transaction.Vote = cm.Vote;
                    _iTransactionRepository.Update(transaction);
                    await _iTransactionRepository.CommitAsync();

                    var feetbackAdd = new Feedback
                    {
                        AccountId = transaction.AccountId,
                        Status = Feedback.EFeedbackStatus.UnRead,
                        CreatedDate = DateTime.Now,
                        Message = cm.Content,
                        Type = Feedback.FeedbackType.Transaction,
                        Rating = cm.Vote,
                        TransactionCode = transaction.TransactionCode,
                        TransactionId = transaction.Id
                    };
                    await _iFeedbackRepository.AddAsync(feetbackAdd);
                    await _iFeedbackRepository.CommitAsync();

                    var wt = EEventSystemWarningType.Info;
                    if (feetbackAdd.Rating == 1 || feetbackAdd.Rating == 2)
                    {
                        wt = EEventSystemWarningType.Danger;
                    }
                    else if (feetbackAdd.Rating == 3 || feetbackAdd.Rating == 4)
                    {
                        wt = EEventSystemWarningType.Warning;
                    }
                    else
                    {
                        wt = EEventSystemWarningType.Info;
                    }
                    // Gửi cho admin phần quản lý
                    await _iHubService.EventSend(EEventSystemType.M_4002_PhanHoiChuyenDi, wt, accountId, feetbackAdd.TransactionCode, transaction.ProjectId, transaction.StationOut ?? 0, transaction.BikeId, transaction.DockId, $"Khách hàng đánh giá {feetbackAdd.Rating} sao, nội dung '{feetbackAdd.Message}' chuyến đi '{feetbackAdd.TransactionCode}'");
                }

                return ApiResponse.WithData(cm);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "BookingService.BookingsFeedback", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"Error {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        #region [BikeReportAddError : khách gửi note xe lỗi]
        public async Task<ApiResponse<object>> BikeReportAddError(int accountId, BikeReportCommand cm)
        {
            try
            {
                var ktBook = await _iBookingRepository.SearchOneAsync(false, x => x.Id == cm.BookingId);

                var dlAdd = new BikeReport
                {
                    AccountId = accountId,
                    BikeId = cm.BikeId,
                    Description = cm.Description,
                    BookingId = cm.BookingId,
                    TransactionCode = ktBook?.TransactionCode,
                    Type = cm.Type,
                    UpdatedDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    DockId = ktBook?.DockId ?? 0,
                    ProjectId = ktBook?.ProjectId ?? 0,
                    InvestorId = ktBook?.InvestorId ?? 0
                };

                await _iBikeReportRepository.AddAsync(dlAdd);
                await _iBikeReportRepository.CommitAsync();
                if (cm.FileData != null && cm.FileData.Length > 0)
                {
                    string imagePath = $"/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/{Guid.NewGuid()}.jpg";
                    string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}{imagePath}";
                    string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}{imagePath}";

                    if (!Directory.Exists(Path.GetDirectoryName(pathAvatarServer)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(pathAvatarServer));
                    }
                    try
                    {
                        var statusImage = Function.ByteArrayToImage(cm.FileData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
                        dlAdd = await _iBikeReportRepository.SearchOneAsync(false, x => x.Id == dlAdd.Id);
                        if (dlAdd != null)
                        {
                            dlAdd.File = pathAvatarWeb;
                            _iBikeReportRepository.Update(dlAdd);
                            await _iBikeReportRepository.CommitAsync();
                        }
                    }
                    catch { }
                }

                if (cm.Type == EBikeReportType.LoiTraXe)
                {
                    var bike = await _iDockRepository.BikeById(cm.BikeId);
                    await _iHubService.EventSend(
                      EEventSystemType.M_3012_KhachBaoLoiTraXe,
                      EEventSystemWarningType.Danger,
                      accountId,
                      null,
                      ktBook?.ProjectId ?? 0,
                      ktBook?.StationIn ?? 0,
                      cm.BikeId,
                      bike?.DockId ?? 0,
                      $"Khách báo lỗi trả xe Chuyến đi(nếu trong chuyến đi) '{ktBook?.TransactionCode}' xe {bike?.Plate}"
                    );
                }

                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(accountId, "BookingService.BikeReportAddError", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"Error {ex}", "", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        #endregion

        private async Task<Transaction> MoveBookingToTransaction(Booking booking, decimal? debit)
        {
            var dlAdd = new Transaction
            {
                AccountId = booking.AccountId,
                BikeId = booking.BikeId,
                ChargeInAccount = booking.ChargeInAccount,
                ChargeInSubAccount = booking.ChargeInSubAccount,
                CreatedDate = booking.CreatedDate,
                CustomerGroupId = booking.CustomerGroupId,
                DateOfPayment = booking.DateOfPayment,
                DockId = booking.DockId,
                StationOut = booking.StationOut,
                Status = booking.Status,
                TransactionCode = booking.TransactionCode,
                Vote = booking.Vote,
                Feedback = booking.Feedback,
                TotalMinutes = booking.TotalMinutes,
                TotalPrice = booking.TotalPrice,
                EndTime = booking.EndTime,
                InvestorId = booking.InvestorId,
                KM = booking.KM,
                KCal = booking.KCal,
                Prepaid_EndDate = booking.Prepaid_EndDate,
                Prepaid_EndTime = booking.Prepaid_EndTime,
                Note = booking.Note,
                KG = booking.KG,
                Prepaid_MinutesSpent = booking.Prepaid_MinutesSpent,
                Prepaid_StartDate = booking.Prepaid_StartDate,
                Prepaid_StartTime = booking.Prepaid_StartTime,
                ProjectId = booking.ProjectId,
                StartTime = booking.StartTime,
                StationIn = booking.StationIn,
                TicketPrepaidCode = booking.TicketPrepaidCode,
                TicketPriceId = booking.TicketPriceId,
                TicketPrice_AllowChargeInSubAccount = booking.TicketPrice_AllowChargeInSubAccount,
                TicketPrice_BlockPerMinute = booking.TicketPrice_BlockPerMinute,
                TicketPrice_BlockPerViolation = booking.TicketPrice_BlockPerViolation,
                TicketPrice_BlockViolationValue = booking.TicketPrice_BlockViolationValue,
                TicketPrice_LimitMinutes = booking.TicketPrice_LimitMinutes,
                TicketPrepaidId = booking.TicketPrepaidId,
                TicketPrice_IsDefault = booking.TicketPrice_IsDefault,
                TicketPrice_TicketType = booking.TicketPrice_TicketType,
                TicketPrice_TicketValue = booking.TicketPrice_TicketValue,
                TicketPrice_Note = booking.TicketPrice_Note,
                TripPoint = booking.TripPoint,
                ChargeDebt = debit,
                EstimateChargeInAccount = booking.EstimateChargeInAccount,
                EstimateChargeInSubAccount = booking.EstimateChargeInSubAccount,
                IsDockNotOpen = booking.IsDockNotOpen,
                IsEndRFID = booking.IsEndRFID,
                IsOpenRFID = booking.IsOpenRFID,
                RFID = booking.RFID,
                DiscountCodeId = booking.DiscountCodeId,
                DiscountCode_Point = booking.DiscountCode_Point,
                DiscountCode_Price = booking.DiscountCode_Price,
                DiscountCode_ProjectId = booking.DiscountCode_ProjectId,
                DiscountCode_Type = booking.DiscountCode_Type,
                DiscountCode_CampaignId = booking.DiscountCode_CampaignId,
                DiscountCode_Percent = booking.DiscountCode_Percent,
                OpenLockTransaction = booking.OpenLockTransaction,
                GroupTransactionOrder = booking.GroupTransactionOrder,
                GroupTransactionCode = booking.GroupTransactionCode,
                ReCallM = booking.ReCallM
            };
            // Thêm một bản ghi vào transaction, sau đó xóa booking đi
            await _iTransactionRepository.AddAsync(dlAdd);
            await _iTransactionRepository.CommitAsync();

            _iBookingRepository.DeleteWhere(x => x.Id == booking.Id && x.TransactionCode == dlAdd.TransactionCode && x.AccountId == dlAdd.AccountId);
            await _iBookingRepository.CommitAsync();
            
            try
            {
                // Chuyên đi kết thúc thống thường, chuyến đi kết thúc bởi admin, chuyến đi thu hồi không nợ cước mới có thể gim
                if ((dlAdd.Status == EBookingStatus.End || dlAdd.Status == EBookingStatus.EndByAdmin || dlAdd.Status == EBookingStatus.ReCall) && _baseSetting.Value.EnableDiscountTransaction)
                {
                    await _iCyclingWeekRepository.UpdateFlag(dlAdd.AccountId, dlAdd.EndTime ?? DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                AddLog(dlAdd.AccountId, "CyclingWeek.UpdateFlag", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"Error {ex}", "", true);
            }
            return dlAdd;
        }

        private async Task<Booking> AddBooking(int accountId, TicketPrice ticketPrice, int stationId, int dockId, Bike bike, TicketPrepaid ticketPrepaid, string rfid, bool isOpenRFID, DiscountCode discountCodeObject, string openDockKey)
        {
            // check lại lần nữa nếu dock này đã sử dụng rồi thì không cho thêm
            var ktDockUse = await _iBookingRepository.AnyAsync(x => x.DockId == dockId && x.Status == EBookingStatus.Start);
            if (ktDockUse)
            {
                return null;
            }

            var wInfo = await _iWalletRepository.GetWalletAsync(accountId);
            string transactionCode = $"{bike.ProjectId:D4}{DateTime.Now:yyyyMMddHHmmssfff}";
            var addBooking = new Booking
            {
                Vote = 0,
                TicketPrice = null,
                StationOut = null,
                Feedback = null,
                DateOfPayment = null,
                Note = null,
                ObjStationIn = null,
                Account = null,
                Bike = null,
                CustomerGroup = null,
                EndTime = null,
                KM = 0,
                StationInObject = null,
                TicketPrice_Note = null,
                TotalMinutes = 0,
                TotalPrice = 0,
                TripPoint = 0,
                KCal = 0,
                KG = 0,
                ChargeInAccount = 0,
                ChargeInSubAccount = 0,
                TransactionCode = transactionCode,
                AccountId = accountId,
                CreatedDate = DateTime.Now,
                CustomerGroupId = ticketPrice.CustomerGroupId,
                StationIn = stationId,
                InvestorId = bike.InvestorId,
                TicketPrice_LimitMinutes = ticketPrepaid?.TicketPrice_LimitMinutes ?? 0,
                TicketPriceId = ticketPrice.Id,
                TicketPrice_TicketType = ticketPrice.TicketType,
                TicketPrice_TicketValue = ticketPrice.TicketValue,
                TicketPrice_IsDefault = ticketPrice.IsDefault,
                TicketPrice_AllowChargeInSubAccount = ticketPrice.AllowChargeInSubAccount,
                TicketPrice_BlockPerMinute = ticketPrice.BlockPerMinute ?? 0,
                TicketPrice_BlockPerViolation = ticketPrice.BlockPerViolation ?? 0,
                TicketPrice_BlockViolationValue = ticketPrice.BlockViolationValue ?? 0,
                Status = EBookingStatus.Start,
                BikeId = bike.Id,
                DockId = dockId,
                Prepaid_StartDate = ticketPrepaid?.StartDate,
                Prepaid_EndDate = ticketPrepaid?.EndDate,
                ProjectId = bike.ProjectId,
                StartTime = DateTime.Now,
                TicketPrepaidCode = ticketPrepaid?.Code,
                TicketPrepaidId = ticketPrepaid?.Id ?? 0,
                Prepaid_StartTime = Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {_baseSetting.Value.SessionStartTime}"),
                Prepaid_EndTime = Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {_baseSetting.Value.SessionEndTime}"),
                RFID = rfid,
                IsOpenRFID = isOpenRFID,
                IsEndRFID = false,
                DiscountCodeId = discountCodeObject?.Id,
                DiscountCode_Point = discountCodeObject?.Point,
                DiscountCode_Price = 0,
                DiscountCode_ProjectId = discountCodeObject?.ProjectId,
                DiscountCode_Type = discountCodeObject?.Type,
                DiscountCode_CampaignId = discountCodeObject?.CampaignId,
                DiscountCode_Percent = discountCodeObject?.Percent,
                OpenLockTransaction = openDockKey,
                GroupTransactionCode = transactionCode,
                GroupTransactionOrder = 1
            };

            if (ticketPrepaid?.TicketPrice_TicketType == ETicketType.Month || ticketPrepaid?.TicketPrice_TicketType == ETicketType.Day)
            {
                addBooking.Prepaid_MinutesSpent = ticketPrepaid?.MinutesSpent ?? 0;
            }

            await _iBookingRepository.AddAsync(addBooking);
            await _iBookingRepository.CommitAsync();
            return addBooking;
        }

        private static int GetDistance(Station station, string lat_lng)
        {
            try
            {
                if (string.IsNullOrEmpty(lat_lng))
                {
                    return 0;
                }
                string[] comps = lat_lng.Split(';');
                var lat = Convert.ToDouble(comps[0]);
                var lng = Convert.ToDouble(comps[1]);

                if (station.Lat != 0 && station.Lng != 0)
                {
                    var distance = Function.DistanceInMeter(lat, lng, station.Lat, station.Lng);
                    return Convert.ToInt32(distance);
                }
                return 99999;
            }
            catch
            {
                return -1;
            }
        }

        private static LatLngModel StringToLatLng(string lat_lng)
        {
            try
            {
                if (string.IsNullOrEmpty(lat_lng))
                {
                    return null;
                }
                string[] comps = lat_lng.Split(';');
                return new LatLngModel
                {
                    Lat = Convert.ToDouble(comps[0]),
                    Lng = Convert.ToDouble(comps[1])
                };
            }
            catch
            {
                return null;
            }
        }

        private static int GetDistance(Dock dock, string lat_lng)
        {
            try
            {
                if (string.IsNullOrEmpty(lat_lng))
                {
                    return -1;
                }
                string[] comps = lat_lng.Split(';');
                var lat = Convert.ToDouble(comps[0]);
                var lng = Convert.ToDouble(comps[1]);
                if (dock.Lat != 0 && dock.Long != 0 && dock.Lat != null && dock.Long != null)
                {
                    var distance = Function.DistanceInMeter(lat, lng, dock.Lat ?? 0, dock.Long ?? 0);
                    return Convert.ToInt32(distance);
                }
                else if(dock.Lat == lat && dock.Long == lng)
                {
                    return 0;
                }    
                return -2;
            }
            catch
            {
                return -3;
            }
        }

        public async Task<Tuple<ApiResponse, int, double, double>> InStationVerified(string lat_lng, int stationIn, int accountId, string functionName, string requestId, int dockId)
        {
            try
            {
                var curentLatLng = StringToLatLng(lat_lng);
                var defaultStationReturnDistance = 100;
                int totalSec = 0;
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Kiểm tra khóa {lat_lng} => {curentLatLng.Lat} / {curentLatLng.Lng}", "");
                if (curentLatLng != null)
                {

                    var stations = (await StationDistance(curentLatLng.Lat, curentLatLng.Lng)).OrderBy(x => x.Distance);
                    var station = stations.FirstOrDefault();

                    var dock = await _iDockRepository.SearchOneAsync(true, x => x.Id == dockId);
                    if (dock != null && dock.LastGPSTime != null)
                    {
                        totalSec = Convert.ToInt32((DateTime.Now - dock.LastGPSTime.Value).TotalSeconds);
                        defaultStationReturnDistance = Convert.ToInt32(station?.ReturnDistance ?? 0) + (totalSec * 10);
                    }

                    if (station == null)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không đứng trong trạm", "");
                        return new Tuple<ApiResponse, int, double, double>(ApiResponse.WithStatus(false, null, ResponseCode.NotInStation), stationIn, -1, -1);
                    }
                    else if (station.Distance < 0)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không ở trong trạm ({station.Distance}/{defaultStationReturnDistance})", "");
                        return new Tuple<ApiResponse, int, double, double>(ApiResponse.WithStatus(false, null, ResponseCode.NotInStation), stationIn, station.Distance, defaultStationReturnDistance);
                    }
                    else if (station.Distance > defaultStationReturnDistance)
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không ở trong trạm ({station.Distance}/{defaultStationReturnDistance})", "");
                        return new Tuple<ApiResponse, int, double, double>(ApiResponse.WithStatus(false, null, ResponseCode.NotInStation), stationIn, station.Distance, defaultStationReturnDistance);
                    }
                    else
                    {
                        AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Ở trong trạm ({station.Distance}/{defaultStationReturnDistance}), top 2 tọa độ {Newtonsoft.Json.JsonConvert.SerializeObject(stations.Take(2))}", "");
                        return new Tuple<ApiResponse, int, double, double>(ApiResponse.WithStatus(true, null, ResponseCode.Ok), station.Id, station.Distance, defaultStationReturnDistance);
                    }
                }
                else
                {
                    AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không có tọa độ", "");
                    return new Tuple<ApiResponse, int, double, double>(ApiResponse.WithStatus(false, null, ResponseCode.NotInStation), stationIn, -2, -2);
                }
            }
            catch
            {
                return new Tuple<ApiResponse, int, double, double>(ApiResponse.WithStatus(false, null, ResponseCode.NotInStation), stationIn, -3, -3);
            }
        }

        private ApiResponse InDockVerified(Dock dock, string lat_lng, int accountId, string requestId, string functionName, out int oDistance)
        {
            var distance = GetDistance(dock, lat_lng);
            if (distance > _baseSetting.Value.BikeReturnDistance || distance < 0)
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.End, $"Không gần khóa ({distance}/{_baseSetting.Value.BikeReturnDistance})", "");
                oDistance = distance;
                return ApiResponse.WithStatus(false, null, ResponseCode.NotInDock);
            }
            else
            {
                AddLog(accountId, functionName, requestId, EnumMongoLogType.Center, $"Gần khóa ({distance}/{_baseSetting.Value.BikeReturnDistance})", "");
                oDistance = distance;
                return ApiResponse.WithStatus(true, null, ResponseCode.Ok);
            }
        }

        private async Task<bool> IsInSesssion()
        {
            bool statusNotConfig = DateTime.Now >= Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {_baseSetting.Value.SessionStartTime}") && DateTime.Now < Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {_baseSetting.Value.SessionEndTime}");
            try
            {
                var kt = await _iSystemConfigurationRepository.SearchOneAsync(true, x => x.Id > 0);
                if(kt == null)
                {
                    return statusNotConfig;
                }
                return DateTime.Now >= Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {kt.SystemStartTime}") && DateTime.Now < Convert.ToDateTime($"{DateTime.Now:yyyy-MM-dd} {kt.SystemEndTime}");
            }
            catch
            {
                return statusNotConfig;
            }
        }

        private async Task<List<StationDTO>> StationDistance(double lat, double lng)
        {
            double delta = 0.01;
            var stations = await _iStationRepository.FindByLatLng(lat - delta, lat + delta, lng - delta, lng + delta);
            var dtoStations = new List<StationDTO>();
            foreach (var station in stations)
            {
                if (station.Lat != 0 && station.Lng != 0)
                {
                    var distance = Function.DistanceInMeter(lat, lng, station.Lat, station.Lng);
                    var dtoStation = new StationDTO(station, Convert.ToInt32(distance));
                    dtoStations.Add(dtoStation);
                }
            }
            return dtoStations;
        }

        private async Task<bool> OpenLock(Dock dock, string transactionCode, int accountId, string openDockKey)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            if(!string.IsNullOrEmpty(openDockKey))
            {
                requestId = openDockKey;
            }    

            try
            {
                var kt = new OpenLockRequest
                {
                    CreatedDate = DateTime.Now,
                    IMEI = dock.IMEI,
                    OpenTime = null,
                    Retry = 0,
                    StationId = dock.StationId,
                    Status = LockRequestStatus.Waiting,
                    DockId = dock.Id,
                    AccountId = accountId,
                    TransactionCode = transactionCode,
                    SerialNumber = dock.SerialNumber,
                    InvestorId = dock.InvestorId,
                    ProjectId = dock.ProjectId,
                    CommandType = ECommandType.L0,
                    Phone = requestId
                };
                await _iOpenLockRequestRepository.AddAsync(kt);
                await _iOpenLockRequestRepository.CommitAsync();
                // Map vào transactionCode Id booking
                if(!string.IsNullOrEmpty(transactionCode))
                {
                    var booking = await _iBookingRepository.SearchOneAsync(false, x => x.TransactionCode == transactionCode && x.AccountId == accountId && x.Status == EBookingStatus.Start);
                    if(booking!= null)
                    {
                        booking.OpenLockTransaction = requestId;
                        _iBookingRepository.Update(booking);
                        await _iBookingRepository.CommitAsync();
                    }    
                }    

                return true;
            }
            catch
            {
                return false;
            }
        }

        private  async Task<List<TicketPriceBindModel>> GetTicketPriceByProjectAsync(int projectId, int accountId)
        {
            var newList = new List<TicketPriceBindModel>();
            var ticketPrices = (await _iMemoryCacheService.TicketPriceSearchAsync()).Where(x => x.ProjectId == projectId && x.IsActive).ToList();

            var accountIsInProject = await _iAccountRepository.ProjectAccountGet(accountId, projectId);

            var customerGroups = (await _iMemoryCacheService.CustomerGroupSearchAsync()).Where(x => ticketPrices.Select(x => x.CustomerGroupId).Contains(x.Id) && x.ProjectId == projectId).ToList();

            var listPrepad = await _iTicketPrepaidRepository.SearchAsync(x => x.ProjectId == projectId && x.AccountId == accountId && x.StartDate <= DateTime.Now && x.EndDate.AddDays(1) > DateTime.Now);

            foreach (var item in ticketPrices.Where(x => x.TicketType == ETicketType.Block))
            {
                var customerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);
                string textCustomer = "TNGO";
                if (customerGroup != null)
                {
                    textCustomer = customerGroup.Name;
                }

                if (item.IsDefault)
                {
                    newList.Add(new TicketPriceBindModel
                    {
                        TicketPriceId = item.Id,
                        Name = $"{ textCustomer }, {ETicketType.Block.GetDisplayName(AppHttpContext.AppLanguage)}",
                        Note = _iTransactionRepository.GetTicketPriceString(item, ResponseCode.NoiDungChiTietVe.ResponseCodeToString(), AppHttpContext.AppLanguage),
                        Order = 10001
                    });
                }
                else if (accountIsInProject != null && accountIsInProject.CustomerGroupId == item.CustomerGroupId)
                {
                    newList.Add(new TicketPriceBindModel
                    {
                        TicketPriceId = item.Id,
                        Name = $"{ textCustomer }, {ETicketType.Block.GetDisplayName(AppHttpContext.AppLanguage)}",
                        Note = _iTransactionRepository.GetTicketPriceString(item, ResponseCode.NoiDungChiTietVe.ResponseCodeToString(), AppHttpContext.AppLanguage),
                        Order = 10000
                    });
                }
            }

            foreach (var item in listPrepad)
            {
                var ticketPrice = new TicketPrice()
                {
                    AllowChargeInSubAccount = item.TicketPrice_AllowChargeInSubAccount,
                    BlockPerMinute = item.TicketPrice_BlockPerMinute,
                    BlockPerViolation = item.TicketPrice_BlockPerViolation,
                    BlockViolationValue = item.TicketPrice_BlockViolationValue,
                    CustomerGroupId = item.CustomerGroupId,
                    LimitMinutes = item.TicketPrice_LimitMinutes,
                    ProjectId = item.ProjectId,
                    TicketValue = item.TicketPrice_TicketValue,
                    TicketType = item.TicketPrice_TicketType,
                    IsDefault = item.TicketPrice_IsDefault,
                    Id = item.TicketPriceId
                };

                if (item.TicketPrice_TicketType == ETicketType.Month)
                {
                    newList.Add(new TicketPriceBindModel
                    {
                        TicketPriceId = item.TicketPriceId,
                        Name = $"{item.TicketPrice_TicketType.GetDisplayName(AppHttpContext.AppLanguage)} {item.StartDate:dd/MM/yyyy}-{item.EndDate:dd/MM/yyyy}",
                        Note = _iTransactionRepository.GetTicketPriceString(ticketPrice, ResponseCode.NoiDungChiTietVeThang.ResponseCodeToString(), AppHttpContext.AppLanguage),
                        Order = 9999,
                        TicketPrepaidId = item.Id,
                        StartDate = item.StartDate,
                        EndDate = item.EndDate
                    });
                }
                else
                {
                    newList.Add(new TicketPriceBindModel
                    {
                        TicketPriceId = item.TicketPriceId,
                        Name = $"{item.TicketPrice_TicketType.GetDisplayName(AppHttpContext.AppLanguage)} {item.StartDate:dd/MM/yyyy}",
                        Note = _iTransactionRepository.GetTicketPriceString(ticketPrice, ResponseCode.NoiDungChiTietVeNgay.ResponseCodeToString(), AppHttpContext.AppLanguage),
                        Order = 9999,
                        TicketPrepaidId = item.Id,
                        StartDate = item.StartDate,
                        EndDate = item.EndDate
                    });
                }
            }
            return newList.OrderBy(x => x.Order).ToList();
        }

        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, string deviceKey, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, deviceKey, isException)).ConfigureAwait(false);
            }
            catch
            {
            }
        }
    }
}