﻿using Microsoft.Extensions.Logging;
using TN.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using PT.Domain.Model;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Caching.Memory;
using TN.API.Model.Setting;
using Microsoft.AspNetCore.Http;
using System.IO;
using TN.Shared;

namespace TN.API.Services
{
    public interface IPostService : IService<Post>
    {
        Task<ApiResponse<object>> Gets(int page, int limit, int? accountRatingId, int? accountId, string key, EPostStatus? status, int myAccountId);
        ApiResponse<object> UploadImage(AccountUploadCommand model, int accountId, HttpRequest request);
        Task<ApiResponse<object>> Add(PostCommand cm, int accountId, string phone, string fullname);
        Task<ApiResponse<object>> Like(int accountId, int postId, bool isLike);
        Task<ApiResponse<object>> CurrentCompetitions();
        Task<ApiResponse<object>> Remove(int accountId, int id);
    }
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IMemoryCacheService _memoryCache;
        private readonly IOptions<BaseSetting> _baseSettings;
        private readonly IOptions<MobileSettings > _mobileSettings;
        private readonly IAccountRatingRepository _accountRatingRepository;

        public PostService
        (
            IPostRepository postRepository,
            IMongoDBRepository iMongoDBRepository,
            IOptions<LogSettings> logSettings,
            IMemoryCacheService memoryCache,
            IOptions<BaseSetting> baseSettings,
            IOptions<MobileSettings> mobileSettings,
            IAccountRatingRepository accountRatingRepository
        )
        {
            _postRepository = postRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _memoryCache = memoryCache;
            _baseSettings = baseSettings;
            _mobileSettings = mobileSettings;
            _accountRatingRepository = accountRatingRepository;
        }

        public virtual async Task<ApiResponse<object>> Gets(int page, int limit, int? accountRatingId, int? accountId, string key, EPostStatus? status, int myAccountId)
        {
            try
            {
                var data = await _postRepository.SearchPagedList(page, limit, accountId, accountRatingId, key, status, myAccountId);
                return ApiResponse.WithData(data.Data.Select(x => new PostDTO(x)));
            }
            catch (Exception ex)
            {
                AddLog(0, "PostService.Gets", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> CurrentCompetitions()
        {
            try
            {
                var data = await _accountRatingRepository.SearchAsync(x=> x.Group == EAccountRatingGroup.Image && x.StartDate <= DateTime.Now && x.EndDate >= DateTime.Now.AddDays(-1));
                return ApiResponse.WithData(data.Select(x=> new AccountRatingDTO(x)));
            }
            catch (Exception ex)
            {
                AddLog(0, "PostService.CurrentCompetitions", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> Add(PostCommand cm, int accountId, string phone, string fullname)
        {
            try
            {
                if(cm.AccountRatingId > 0)
                {
                    var ck = await _postRepository.AnyAsync(x => x.AccountRatingId == cm.AccountRatingId && x.AccountId == accountId);
                    if(ck)
                    {
                        return ApiResponse.WithStatus(false, null, ResponseCode.BanDaThamGiaChuongTrinhHinhAnh);
                    }
                } 
                var dlAdd = new Post
                {
                    AccountId = accountId,
                    AccountRatingId = cm.AccountRatingId,
                    Content = cm.Content,
                    CreatedDate = DateTime.Now,
                    Thumbnail = cm.Thumbnail,
                    Image = cm.Image,
                    LikeCount = 0,
                    Status = EPostStatus.Default
                };
                await _postRepository.AddAsync(dlAdd);
                await _postRepository.CommitAsync();
                dlAdd.Account = new Account { Id = accountId, FullName = fullname, Phone = phone };
                return ApiResponse.WithStatusOk(new PostDTO(dlAdd));
            }
            catch (Exception ex)
            {
                AddLog(0, "PostService.Add", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public virtual async Task<ApiResponse<object>> Like(int accountId, int postId, bool isLike)
        {
            try
            {
                await _postRepository.PostLikeAdd(postId, accountId, isLike);
                return ApiResponse.WithStatusOk(isLike);
            }
            catch (Exception ex)
            {
                AddLog(0, "PostService.Like", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        public ApiResponse<object> UploadImage(AccountUploadCommand model, int accountId, HttpRequest request)
        {
            try
            {
                var nameDefault = Guid.NewGuid();
                string pathServer = $"{_baseSettings.Value.FolderMobikeImageServer}/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/{accountId}_{nameDefault}.jpg";
                string pathWeb = $"{_baseSettings.Value.FolderMobikeImageWeb}/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/{accountId}_{nameDefault}.jpg";

                string pathThumbServer = $"{_baseSettings.Value.FolderMobikeImageServer}/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/{accountId}_{nameDefault}_thumb.jpg";
                string pathThumbWeb = $"{_baseSettings.Value.FolderMobikeImageWeb}/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/{accountId}_{nameDefault}_thumb.jpg";

                if (!Directory.Exists(Path.GetDirectoryName(pathServer)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(pathServer));
                }

                Function.ByteArrayToImage(model.Data, pathServer, pathThumbServer, _mobileSettings.Value.PostThumbImageWidth);

                var outObje = new
                {
                    Image = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{pathWeb}",
                    Thumbnail = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{pathThumbWeb}",
                };
                return ApiResponse.WithStatus(true, outObje, ResponseCode.Ok);
            }
            catch (Exception ex)
            {
                AddLog(accountId, "PostService.UploadImage", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }
        // Xóa khi bài chưa được duyệt
        public virtual async Task<ApiResponse<object>> Remove(int accountId, int id)
        {
            try
            {
                var kt = await _postRepository.SearchOneAsync(false, x => x.Id == id && x.AccountId == accountId);
                if(kt == null)
                {
                    return ApiResponse.WithStatusInvalidData();
                }

                if (kt.Status == EPostStatus.Approved)
                {
                    return ApiResponse.WithStatus(false, null, ResponseCode.KhongTheXoaBaiDang);
                }

                if (kt != null)
                {
                    _postRepository.Delete(kt);
                    await _postRepository.CommitAsync();
                }    
                return ApiResponse.WithStatusOk();
            }
            catch (Exception ex)
            {
                AddLog(0, "PostService.Remove", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
        #endregion
    }
}
