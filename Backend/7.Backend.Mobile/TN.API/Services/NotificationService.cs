﻿using Microsoft.Extensions.Options;
using PT.Domain.Model;
using System;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;

namespace TN.API.Services
{
    public interface INotificationService : IService<NotificationData>
    {
        Task<ApiResponse<object>> Search(int accountId, int page = 1, int limit = 10);
    }

    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _iNotificationRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;

        public NotificationService(INotificationRepository iNotificationRepository, IMongoDBRepository iMongoDBRepository, IOptions<LogSettings> logSettings)
        {
            _iNotificationRepository = iNotificationRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
        }

        public async Task<ApiResponse<object>> Search(int accountId, int page = 1, int limit = 10)
        {
            try
            {
                var data = await _iNotificationRepository.SearchPagedList(page, limit, 
                    x => (x.AccountId == accountId || x.AccountId == 0) && !x.IsAdmin, 
                    x => x.OrderByDescending(m => m.CreatedDate));

                return ApiResponse.WithData(data.Data, data.TotalRows, data.Limit);
            }
            catch (Exception ex)
            {
                AddLog(0, "NotificationService.Search", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
        #endregion
    }
}
