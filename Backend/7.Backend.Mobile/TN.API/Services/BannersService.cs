﻿using Microsoft.Extensions.Options;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using TN.Shared;

namespace TN.API.Services
{
    public interface IBannersService : IService<Banner>
    {
        Task<ApiResponse<object>> WalletBanner();
    }

    public class BannersService : IBannersService
    {
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IMemoryCacheService _iMemoryCacheService;

        public BannersService(
            IMongoDBRepository iMongoDBRepository, 
            IOptions<LogSettings> logSettings,
            IMemoryCacheService iMemoryCacheService
            )
        {
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _iMemoryCacheService = iMemoryCacheService;
        }

        public async Task<ApiResponse<object>> WalletBanner()
        {
            try
            {
                var langG = _iMemoryCacheService.CurrentLanguage();
                var grs = await _iMemoryCacheService.BannerGroupAsync();
                var gr = grs.FirstOrDefault(x => x.Language == langG && x.Type == EBannerGroupType.B1);
                if(gr == null)
                {
                    return ApiResponse.WithData(new List<BannerDTO>());
                }

                var listData = new List<BannerDTO>();

                foreach(var item in gr.Banners)
                {
                    listData.Add(new BannerDTO { 
                        Data = item.Data,
                        EndTime = item.EndTime,
                        Image = $"{(AppHttpContext.Current?.Request.IsHttps == true ? "https://":"http://")}{AppHttpContext.Current?.Request.Host}{item.Image}",
                        Name = item.Name,
                        StartTime = item.StartTime,
                        Type = item.Type,
                        Id = item.Id,
                        DataObject = item.DataObject
                    });
                }
                return ApiResponse.WithData(listData);
            }
            catch (Exception ex)
            {
                AddLog(0, "NewsService.WalletBanner", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"Lỗi {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
        #endregion
    }
}
