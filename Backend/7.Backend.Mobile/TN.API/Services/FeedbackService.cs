﻿using Microsoft.Extensions.Options;
using PT.Domain.Model;
using System;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.API.Services
{
    public interface IFeedbackService : IService<Feedback>
    {
        Task<ApiResponse<object>> Contact(int accountId, FeedbackContactCommand model);
    }

    public class FeedbackService : IFeedbackService
    {
        private readonly IFeedbackRepository _FeedbackRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;

        public FeedbackService(IFeedbackRepository FeedbackRepository, IOptions<LogSettings> logSettings, IMongoDBRepository iMongoDBRepository)
        {
            _FeedbackRepository = FeedbackRepository;
            _logSettings = logSettings.Value;
            _iMongoDBRepository = iMongoDBRepository;
        }
        public  async Task<ApiResponse<object>> Contact(int accountId, FeedbackContactCommand model)
        {
            try
            {
                var dataAdd = new Feedback
                {
                    AccountId = accountId,
                    Status = Feedback.EFeedbackStatus.UnRead,
                    CreatedDate = DateTime.Now,
                    Message = model.Message,
                    Type = Feedback.FeedbackType.Default
                };
                await _FeedbackRepository.AddAsync(dataAdd);
                await _FeedbackRepository.CommitAsync();
                return ApiResponse.WithData(new FeedbackDTO(dataAdd));
            }
            catch (Exception ex)
            {
                AddLog(accountId, "IFeedbackService.Contact", $"{DateTime.Now:yyyyMMddHHMMssfff}", EnumMongoLogType.End, $"Error {ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }

        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content,"", isException)).ConfigureAwait(false);
        }
    }
}
