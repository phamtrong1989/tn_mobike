﻿using Microsoft.Extensions.Logging;
using TN.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using PT.Domain.Model;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Caching.Memory;
using TN.API.Model.Setting;

namespace TN.API.Services
{
    public interface IStationService : IService<Station>
    {
        Task<ApiResponse<object>> FindByLatLng(string lat_lng);
    }
    public class StationService : IStationService
    {
        private readonly IDockRepository _iDockRepository;
        private readonly IMongoDBRepository _iMongoDBRepository;
        private readonly LogSettings _logSettings;
        private readonly IMemoryCacheService _memoryCache;
        private readonly IOptions<BaseSetting> _baseSettings;
        public StationService
        (
            IDockRepository iDockRepository,
            IMongoDBRepository iMongoDBRepository,
            IOptions<LogSettings> logSettings,
            IMemoryCacheService memoryCache,
            IOptions<BaseSetting> baseSettings
        )
        {
            _iDockRepository = iDockRepository;
            _iMongoDBRepository = iMongoDBRepository;
            _logSettings = logSettings.Value;
            _memoryCache = memoryCache;
            _baseSettings = baseSettings;
        }

        public virtual async Task<ApiResponse<object>> FindByLatLng(string lat_lng)
        {
            try
            {
                if (string.IsNullOrEmpty(lat_lng))
                {
                    return ApiResponse.WithData(new List<Station>());
                }

                string[] comps = lat_lng.Split(';');
                var lat = Convert.ToDouble(comps[0]);
                var lng = Convert.ToDouble(comps[1]);
                var stations = await _memoryCache.StationSearchAsync();
                var dtoStations = new List<StationDTO>();

                stations = await _iDockRepository.BindTotalDockFree(stations, 0, _baseSettings.Value.MinBattery);

                foreach (var station in stations)
                {
                    if (station.Lat != 0 && station.Lng != 0)
                    {
                        var distance = Function.DistanceInMeter(lat, lng, station.Lat, station.Lng);
                        var dtoStation = new StationDTO(station, Convert.ToInt32(distance));
                        dtoStations.Add(dtoStation);
                    }
                }
                dtoStations = dtoStations.OrderBy(x => x.Distance).ToList();

                return ApiResponse.WithData(dtoStations);
            }
            catch (Exception ex)
            {
                AddLog(0, "StationService.FindByLatLng", $"{DateTime.Now:yyyyMMddHHmmssfff}", EnumMongoLogType.End, $"{ex}", true);
                return ApiResponse.WithStatus(false, null, ResponseCode.ServerError);
            }
        }


        #region [Function Add log]
        private void AddLog(int accountId, string functionName, string logKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => _iMongoDBRepository.AddLogRun(_logSettings, accountId, functionName, logKey, type, content, "", isException)).ConfigureAwait(false);
            }
            catch { }
        }
        #endregion
    }
}
