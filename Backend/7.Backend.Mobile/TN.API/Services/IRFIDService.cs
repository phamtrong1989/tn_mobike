﻿using Microsoft.Extensions.Options;
using TN.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Seedwork;
using TN.Backend.Model.Command;
using System.Threading;
using TN.Infrastructure.Interfaces;
using System.IO;
using PT.Domain.Model;

namespace TN.API.Services
{
    public interface IRFIDService : IService<Booking>
    {
        Task<ApiResponse<object>> RFIDBookingScanSubmit(Account accountRFID, string rfid, Dock dock, Bike bike);
    }

    public class RFIDService : IRFIDService
    {
        private readonly IBookingService _iBookingService;

        public RFIDService
        (
            IBookingService iBookingService
        )
        {
            _iBookingService = iBookingService;
        }

        public async Task<ApiResponse<object>> RFIDBookingScanSubmit(Account accountRFID, string rfid, Dock dock, Bike bike)
        {
            return await _iBookingService.RFIDBookingScanSubmit(rfid, accountRFID.Id, dock, bike);
        }
    }
}