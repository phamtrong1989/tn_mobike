﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Text;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PT.Shared;
using Serilog;
using TN.API.Infrastructure;
using TN.API.Infrastructure.Interfaces;
using TN.API.Infrastructure.Repositories;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.API.Services;
using TN.Backend.Services;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Infrastructure.Repositories;
using TN.Shared;

namespace TN.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment IWebHostEnvironment { get; }
        public static Dictionary<int, AccountLocationModel> AccountLocations = new();
        public Startup(IConfiguration configuration, IWebHostEnvironment iWebHostEnvironment)
        {
            Configuration = configuration;
            IWebHostEnvironment = iWebHostEnvironment;
            Serilog.Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Warning()
            .WriteTo.RollingFile(Path.Combine(iWebHostEnvironment.ContentRootPath, "logs/log-{Date}.txt"))
            .CreateLogger();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddDataProtection().SetApplicationName("mobike_mobile");
            services.AddRouting();
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None;
            });
            services.AddDbContextPool<MobikeContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("TN.API.Infrastructure")));
            services.AddSignalR();
            //  services.AddDbContext<MobikeContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("TN.API.Infrastructure")), ServiceLifetime.Transient);
            //services.AddSignalR();
            services.AddHttpClient();
            // Dependency Injection - Settings
            services.Configure<BaseSetting>(Configuration.GetSection("BaseSettings"));
            services.Configure<LogSettings>(Configuration.GetSection("LogSettings"));
            services.Configure<MonoSettings>(Configuration.GetSection("MonoSettings"));
            services.Configure<ZaloSettings>(Configuration.GetSection("ZaloSettings"));
            services.Configure<MobileSettings>(Configuration.GetSection("MobileSettings"));
            services.Configure<PayooSettings>(Configuration.GetSection("PayooPaySettings"));

            services.Configure<List<NotificationTemp>>(Configuration.GetSection("NotificationTempSettings"));
            services.Configure<List<ResponseCodeResources>>(Configuration.GetSection("ResponseCodeResources"));
            //services.Configure<List<ResourcesForm>>(Configuration.GetSection("ResourcesForm"));

            // Dependency Injection - Repositories
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IDockRepository, DockRepository>();
            services.AddScoped<IBikeRepository, BikeRepository>();
            services.AddScoped<IAccountOTPRepository, AccountOTPRepository>();
            services.AddScoped<IBookingRepository,BookingRepository>();
            services.AddScoped<IWalletRepository, WalletRepository>();
            services.AddScoped<IWalletTransactionRepository, WalletTransactionRepository>();
            services.AddScoped<IFeedbackRepository, FeedbackRepository>();
            services.AddScoped<ITicketPriceRepository, TicketPriceRepository>();
            services.AddScoped<IStationRepository, StationRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<ISystemNotificationRepository, SystemNotificationRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<IOpenLockRequestRepository, OpenLockRequestRepository>();
            services.AddScoped<IOpenLockHistoryRepository, OpenLockHistoryRepository>();
            services.AddScoped<ICampaignRepository, CampaignRepository>();
            services.AddScoped<IProjectAccountRepository, ProjectAccountRepository>();
            services.AddScoped<ITicketPrepaidRepository, TicketPrepaidRepository>();
            services.AddScoped<IEmailBoxRepository, EmailBoxRepository>();
            services.AddScoped<IEmailManageRepository, EmailManageRepository>();
            services.AddScoped<IBikeReportRepository, BikeReportRepository>();
            services.AddScoped<IVoucherCodeRepository, VoucherCodeRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<ISMSRepository, SMSRepository>();
            services.AddScoped<IMongoDBRepository, MongoDBRepository>();
            services.AddScoped<IContentPageRepository, ContentPageRepository>();
            services.AddScoped<IContentPageService, ContentPageService>();
            services.AddScoped<IInvestorRepository, InvestorRepository>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();
            services.AddScoped<ICyclingWeekRepository, CyclingWeekRepository>();
            services.AddScoped<IStationResourceRepository, StationResourceRepository>();
            services.AddScoped<ICustomerGroupResourceRepository, CustomerGroupResourceRepository>();
            services.AddScoped<IProjectResourceRepository, ProjectResourceRepository>();
            services.AddScoped<IBannerGroupRepository, BannerGroupRepository>();
            services.AddScoped<IBannerRepository, BannerRepository>();

            services.AddScoped<IRedeemPointRepository, RedeemPointRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<ICustomerGroupRepository, CustomerGroupRepository>();
            services.AddScoped<INotificationJobRepository, NotificationJobRepository>();
            services.AddScoped<IEventSystemReportRepository, EventSystemReportRepository>();
            services.AddScoped<IRFIDBookingRepository, RFIDBookingRepository>();
            services.AddScoped<ISystemConfigurationRepository , SystemConfigurationRepository>();
            services.AddScoped<ITransactionDiscountRepository, TransactionDiscountRepository>();
            services.AddScoped<IBikePrivateRepository, BikePrivateRepository>();
            services.AddScoped<IAccountRatingItemRepository, AccountRatingItemRepository>();
            services.AddScoped<IDiscountCodeRepository, DiscountCodeRepository>();
            services.AddScoped<IAccountRatingRepository, AccountRatingRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IGroupAppointmentItemRepository, GroupAppointmentItemRepository>();
            services.AddScoped<IGroupAppointmentRepository, GroupAppointmentRepository>();
            services.AddScoped<IGroupAppointmentAccountRepository, GroupAppointmentAccountRepository>();

            // Dependency Injection - Services
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IFeedbackService, FeedbackService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IStationService, StationService>();
            services.AddScoped<IHubService, HubService>();
            services.AddScoped<IMemoryCacheService, MemoryCacheService>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<IFireBaseService, FireBaseService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<ITicketPrepaidService, TicketPrepaidService>();
            services.AddScoped<IRFIDService, RFIDService>();
            services.AddScoped<IControlsService, ControlsService>();
            services.AddScoped<IBannersService, BannersService>();
            services.AddScoped<IDashboardService, DashboardService>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IGroupAppointmentService, GroupAppointmentService>();
            
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal);
            services.Configure<FormOptions>(options => { options.MultipartBodyLengthLimit = 209_715_200; });
            services.AddDistributedMemoryCache();
            services.AddResponseCompression();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["BaseSettings:API_ValidIssuer"],
                    ValidAudience = Configuration["BaseSettings:API_ValidAudience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["BaseSettings:SecurityKey"]))
                };
            });

            try
            {
                if (FirebaseMessaging.DefaultInstance == null)
                {
                    var path = $"{IWebHostEnvironment.ContentRootPath}/appsettings.Notification.json";
                    var defaultApp = FirebaseApp.Create(new AppOptions()
                    {
                        Credential = GoogleCredential.FromFile(path),
                    });
                    var defaultAuth = FirebaseAuth.GetAuth(defaultApp);
                    defaultAuth = FirebaseAuth.DefaultInstance;
                }
            }
            catch { }

            services.AddMemoryCache();
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en-US")
                };
                options.DefaultRequestCulture = new RequestCulture("en-US", "en-US");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            services.AddCors();

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                }
            );
           
            services.AddSwaggerGen();
            services.AddSwaggerDocumentation();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
            AppHttpContext.Services = app.ApplicationServices;

            app.UseSwaggerDocumentation();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
             
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 31536000;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = "public,max-age=" + durationInSeconds;
                }
            });

            app.UseStaticFiles();
            
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(builder =>
            {
                builder
                    .WithOrigins(Configuration["BaseSettings:UseCors"].Split(','))
                    .WithMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            });

            app.UseEndpoints(e =>
            {
                e.MapControllers();
            });
        }
    }
}
