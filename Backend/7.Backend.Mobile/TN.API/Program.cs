﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace TN.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
               .ConfigureLogging((hostingContext, config) =>
               {
                   config.ClearProviders();
               })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile($"appsettings.{hostingContext.HostingEnvironment}.json", optional: true)
                            .AddJsonFile("appsettings.Base.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Momo.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Zalo.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Mobile.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.NotificationTemp.json", optional: true, reloadOnChange: true)
                            //.AddJsonFile("appsettings.Resources.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.ResponseCodeResources.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.PayooPay.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Log.json", optional: true, reloadOnChange: true);

                })
                  .UseKestrel(options =>
                  {
                      options.Limits.MinRequestBodyDataRate = new MinDataRate(100.0, TimeSpan.FromSeconds(10.0));
                  })
                .UseStartup<Startup>();
    }
}
