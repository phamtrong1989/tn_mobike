﻿namespace TN.API.Model
{
    public class PaymentHistoryCommand
    {
        public int Accountid { get; set; }
        public string Cmd { get; set; }
        public string MerchantCode { get; set; }
        public string Smartcontract { get; set; }
        public int Start { get; set; }
        public int Size { get; set; }
    }
}
