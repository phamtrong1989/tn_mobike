﻿using System;

namespace TN.API.Model
{
    public class TransactionHistoryGetByAccountCommand
    {
        public int AccountId { get; set; }
        public int TypeId { get; set; }
    }

    public class AccountLocationModel
    {
        public DateTime? LastActive { get; set; }
        public DateTime? Last_Controls_SendLocation { get; set; }
        public DateTime? Last_Controls_CheckService { get; set; }
        public DateTime? Last_Controls_Settings { get; set; }
        public DateTime? Last_Booking_BookingScanSubmit { get; set; }
        public DateTime? Last_TicketPrepaid_Buy { get; set; }

        public DateTime? Last_Booking_BookingCheckOut { get; set; }
        public DateTime? Last_Booking_BookingReOpen { get; set; }

        public DateTime? Last_Post_Add { get; set; }
        public DateTime? Last_Post_Like { get; set; }
        public DateTime? Last_Post_Remove { get; set; }


        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
