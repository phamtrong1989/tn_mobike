﻿using System.ComponentModel.DataAnnotations;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class UpdateTransactionHistoryCommand
    {
        public int AccountId { get; set; }
        public int TicketTransactionId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
    }

    public class BikeReportCommand
    {
        public int BookingId { get; set; }
        public string TransactionCode { get; set; }
        public int BikeId { get; set; }
        public string Description { get; set; }
        public EBikeReportType Type { get; set; }
        public byte[] FileData { get; set; }
    }
}
