﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Backend.Model.Command
{
    public class BookingCancelCommand
    {
        public int BookingId { get; set; }
        public string Feedback { get; set; }
    }

    public class GroupAppointmentCommand
    {
        public int MaxAmount { get; set; }
        public string Descriptions { get; set; }
        public string Lat_Lng { get; set; }
        public string Time { get; set; }
        public int StationId { get; set; }
    }

    public class GroupAppointmentDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CurrentAmount { get; set; }
        public int MaxAmount { get; set; }
        public string StationName { get; set; }
        public string StationAddress { get; set; }
        public string LeaderFullName { get; set; }
        public string LeaderAvartar { get; set; }
        public ESex LeaderSex { get; set; }
        public string Descriptions { get; set; }
        public string LeaderPhone { get; set; }
        public int? ReputationLevel { get; set; }
        public IEnumerable<GroupAppointmentItemDTO> Items { get; internal set; }

        public GroupAppointmentDTO(GroupAppointment item, string stationName, string stationAdress)
        {
            Code = item.Code;
            Descriptions = item.Descriptions;
            LeaderFullName = item.LeaderFullName;
            LeaderSex = item.LeaderSex;
            CurrentAmount = item.CurrentAmount;
            MaxAmount = item.MaxAmount;
            ReputationLevel = item.ReputationLevel;
            StationAddress = stationAdress;
            StationName = stationName ;
            Id = item.Id;
            LeaderPhone = item.LeaderPhone;
            LeaderAvartar = item.LeaderAvartar;
        }
    }

    public class GroupAppointmentItemDTO
    {
        public GroupAppointmentItemDTO(GroupAppointmentItem cr)
        {
            Id = cr.Id;
            GroupAppointmentId = cr.GroupAppointmentId;
            MemberId = cr.MemberId;
            MemberFullName = cr.MemberFullName;
            MemberPhone = cr.MemberPhone;
            MemberSex = cr.MemberSex;
            IsConfirm = cr.IsConfirm;
            TransactionCode = cr.TransactionCode;
        }

        public int Id { get; set; }

        public int GroupAppointmentId { get; set; }

        public int MemberId { get; set; }

        public string MemberFullName { get; set; }

        public string MemberPhone { get; set; }

        public ESex MemberSex { get; set; }

        public bool IsConfirm { get; set; }

        public string TransactionCode { get; set; }
    }
}

