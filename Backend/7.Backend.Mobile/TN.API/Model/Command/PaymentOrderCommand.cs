﻿namespace TN.API.Model
{
    public class PaymentOrderCommand
    {
        public string Cmd { get; set; }
        public string Merchantcode { get; set; }
        public double Total { get; set; }
        public string Currency { get; set; }
        public string Billid { get; set; }
        public string Smartcontract { get; set; }
    }

    public class LatLngModel
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
