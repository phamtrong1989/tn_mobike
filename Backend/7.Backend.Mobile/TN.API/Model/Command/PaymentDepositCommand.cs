﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class PaymentDepositCommand
    {
        public int Accountid { get; set; }

        public string Cmd { get; set; }

        public string Merchantcode { get; set; }

        [Required]
        [Range(10000, 10000000)]
        public int Total { get; set; }

        [Required]
        public string Currency { get; set; }

        public string Code { get; set; }

        public string Type { get; set; }

        public string Lat { get; set; }

        public string Lng { get; set; }
    }

    public class PayooDepositCommand
    {
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }
        // Do Payoo cung cấp
        [JsonProperty(PropertyName = "shop_id")]
        public string ShopId { get; set; }
        // Do Payoo cung cấp
        [JsonProperty(PropertyName = "session")]
        public string Session { get; set; }

        [JsonProperty(PropertyName = "shop_title")]
        public string ShopTitle { get; set; }
        // Đăng ký tên để Payoo cung cấp
        [JsonProperty(PropertyName = "shop_domain")]
        public string ShopDomain { get; set; }

        [JsonProperty(PropertyName = "shop_back_url")]
        public string ShopBackUrl { get; set; }

        [JsonProperty(PropertyName = "order_no")]
        public string OrderNo { get; set; }

        [JsonProperty(PropertyName = "order_ship_date")]
        public string OrderShipDate { get; set; }// format dd/MM/yyyy

        [JsonProperty(PropertyName = "order_ship_days")]
        public string OrderShipDays { get; set; }

        [JsonProperty(PropertyName = "order_cash_amount")]
        public string OrderCashAmount { get; set; }

        [JsonProperty(PropertyName = "order_description")]
        public string OrderDescription { get; set; }

        [JsonProperty(PropertyName = "notify_url")]
        public string NotifyUrl { get; set; }

        [JsonProperty(PropertyName = "validity_time")]
        public string ValidityTime { get; set; }

        [JsonProperty(PropertyName = "JsonResponse")]
        public string JsonResponse { get; set; } = "true";

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }
        public int AccountId { get; internal set; }
    }

    public class PayooDepositCallbackCommand
    {
        public string Username { get; set; }
        // Do Payoo cung cấp
        public long ShopId { get; set; }
        // Do Payoo cung cấp
        public string Session { get; set; }

        public string ShopTitle { get; set; }
        // Đăng ký tên để Payoo cung cấp
        public string ShopDomain { get; set; }

        public string ShopBackUrl { get; set; }

        public string OrderNo { get; set; }

        public string OrderShipDate { get; set; }// format dd/MM/yyyy

        public string OrderShipDays { get; set; }

        public long OrderCashAmount { get; set; }

        public string OrderDescription { get; set; }

        public string NotifyUrl { get; set; }

        public string ValidityTime { get; set; }

        public string JsonResponse { get; set; } = "true";

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }
        public string BillingCode { get; internal set; }
        public string PaymentExpireDate { get; internal set; }
        public string BusinessUsername { get; internal set; }
        public string StartShippingDate { get; internal set; }
        public short ShippingDays { get; internal set; }
        public int PaymentStatus { get; set; }
    }

    public class PayooCallback
    {
        public class ResponseData
        {
            public string OrderNo { get; set; }
            public decimal OrderCash { get; set; }
            public int PaymentStatus { get; set; }
            public EPaymentMethod PaymentMethod { get; set; }
            public string PurchaseDate { get; set; }                // format yyyyMMddHHmmss
            public string MerchantUsername { get; set; }
            public long ShopId { get; set; }
            public string BankName { get; set; }
            public string CardNumber { get; set; }
            public string BillingCode { get; set; }
            public ECardIssuanceType CardIssuanceType { get; set; }
        }


        public enum EPaymentMethod
        {
            E_WALLET = 1,
            INTERNATIONAL_CARD = 2,
            DOMESTIC_CARD = 3,
            PAYMENT_LATER = 4,
            QRCODE = 5,
            INSTALLMENT = 6,
        }

        public enum ECardIssuanceType
        {
            DOMESTIC = 0,
            FOREIGN = 1,
        }

        public class ReturnCallback
        {
            public int ReturnCode { get; set; } 
            public string Description { get; set; }
        }


    }
    public class PayooGetBillingCodeCommand
    {
        public string OrderNo { get; set; }
        public long ShopID { get; set; }
        public string FromShipDate { get; set; }
        public int ShipNumDay { get; set; }
        public string Description { get; set; }
        public decimal CyberCash { get; set; }
        public string PaymentExpireDate { get; set; }
        public string NotifyUrl { get; set; }
        public string InfoEx { get; set; }
    }

    public class PayooGetBillingCodeResponseDTO
    {
        public string BillingCode { get; set; }
        public string ResponseData { get; set; }
        public string Signature { get; set; }
    }

    public class PayooGetBillingCodeResponseStatusDTO
    {
        public int ResponseCode { get; set; }
        public string BillingCode { get; set; }
    }

    public class PayooAPIRequestCommand
    {
        public string RequestData { get; set; }
        public string Signature { get; set; }
    }
}
