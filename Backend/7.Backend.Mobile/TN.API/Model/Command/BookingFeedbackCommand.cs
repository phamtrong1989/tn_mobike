﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class BookingFeedbackCommand
    {
        public int TransactionId { get; set; }
        [Required]
        [StringLength(500)]
        public string Content { get; set; }
        [Required]
        [Range(1, 10)]
        public byte Vote { get; set; }
    }

    public class BookingsFeedbackCommand
    {
        public int[] TransactionIds { get; set; }
        [Required]
        [StringLength(500)]
        public string Content { get; set; }
        [Required]
        [Range(1, 10)]
        public byte Vote { get; set; }
    }

    public class ReCallCommand
    {
        public int BookingId { get; set; }
        [Required]
        [StringLength(500)]
        public string Feedback { get; set; }
    }
}
