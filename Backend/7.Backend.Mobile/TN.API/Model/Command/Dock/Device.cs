﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TN.API.Model.Command.Dock
{
    public class Device
    {
        public string No { get; set; }
        public string IMEI { get; set; }
        public string MAC { get; set; }
    }

    public class OpenLockRequestCommand
    {
        [MaxLength(50)]
        [Required]
        public string IMEI { get; set; }
    }
}
