﻿namespace TN.API.Model
{
    public class PaymentQueryBalanceCommand
    {
        public int AccountId { get; set; }
        public string CMD { get; set; }
        public string MerchantCode { get; set; }
        public string Smartcontract { get; set; }
    }
}
