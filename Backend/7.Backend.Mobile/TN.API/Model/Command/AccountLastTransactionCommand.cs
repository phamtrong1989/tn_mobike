﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class BikeWarningSendCommand
    {
        public string IMEI { get; set; }
        public EBikeWarningType Type { get; set; }
        public DateTime Time { get; set; }
    }

    public class AccountLastTransactionCommand
    {
        public int AccountId { get; set; }
    }
}
