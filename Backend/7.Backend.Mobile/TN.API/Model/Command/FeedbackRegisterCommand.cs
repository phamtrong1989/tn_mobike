﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class FeedbackRegisterCommand
    {
        [Required]
        public int AccountId { get; set; }
        public Feedback.EFeedbackStatus StatusId { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Rating { get; set; }
    }

    public class FeedbackContactCommand
    {
        public string Message { get; set; }
    }
}
