﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PT.Domain.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TN.API.Services;
using TN.Domain.Model;
using TN.Shared;

namespace TN.API.Model
{
    public  static class FunctionCode
    {
        public static string ResponseCodeToString(this ResponseCode errorCode)
        {
            try
            {
                string lang = AppHttpContext.AppLanguage;
                var responseCodeResources = AppHttpContext.Services?.GetRequiredService<IOptions<List<ResponseCodeResources>>>().Value ?? new List<ResponseCodeResources>();
                var dataByLan = responseCodeResources.FirstOrDefault(x => x.Language == lang);

                return dataByLan.Data.FirstOrDefault(x => x.Code == (int)errorCode)?.Text ?? errorCode.GetDisplayName();
            }
            catch
            {
                return errorCode.GetDisplayName();
            }
        }
    }

    public enum ResponseCode
    {
        [Display(Name = "Success")]
        Ok = 0,

        [Display(Name = "Dữ liệu không hợp lệ")]
        InvalidData = 1,

        [Display(Name = "Thao tác không thành công, vui lòng thử lại")]
        ServerError = 2,

        [Display(Name = "Khóa xe không tồn tại")]
        DockNotExist = 3,

        [Display(Name = "Khóa xe chưa được kích hoạt")]
        DockNotActive = 4,

        [Display(Name = "Khóa xe gặp sự cố, vui lòng thử lại hoặc sử dụng xe khác")]
        DockError = 5,

        [Display(Name = "Khóa xe không gắn trên xe")]
        DockHasNoBike = 6,

        [Display(Name = "Xe này không thể thuê vì không thuộc trạm nào, vui lòng liên hệ để biết thêm chi tiết")]
        StationNotExist = 7,

        [Display(Name = "Xe này không thể thuê vì không thuộc trạm nào, vui lòng liên hệ để biết thêm chi tiết")]
        SaiMatKhauNhieuLanChoPhep = 8,

        [Display(Name = "Mã vé không hợp lệ")]
        TicketInvalid = 10,

        [Display(Name = "Tài khoản không tồn tại")]
        AccountNotExist = 11,

        [Display(Name = "Xe không tồn tại")]
        BikeNotExist = 12,

        [Display(Name = "Giao dịch không tồn tại")]
        TransactionNotExist = 13,

        [Display(Name = "Số điện thoại đã tồn tại, vui lòng nhập số điện thoại khác")]
        PhoneNumberExist = 15,

        [Display(Name = "Tài khoản không tồn tại")]
        UsernameNotExist = 16,

        [Display(Name = "Email này đã có người sử dụng, vui lòng nhập email khác")]
        EmailExist = 17,

        [Display(Name = "Thông tin đăng nhập không hợp lệ")]
        InvalidLogin = 18,

        [Display(Name = "Xác nhận OTP hết hạn hoặc không chính xác vui lòng quay lại và thực hiện lại")]
        OTPOutTime = 19,

        [Display(Name = "Mã giao dịch không hợp lệ")]
        TransactionInvalid = 20,

        [Display(Name = "Mã OTP không chính xác, vui lòng nhập lại, số lần nhập lại của bạn còn {0}, quá số lần bạn phải thực hiện lại từ đầu")]
        OTPInvalid = 21,

        [Display(Name = "Trừ tiền thành công")]
        OrderDone = 22,

        [Display(Name = "Tài khoản không hợp lệ")]
        InvalidAccount = 23,

        [Display(Name = "Thông tin đặt xe không hợp lệ")]
        BookingNotExist = 24,

        [Display(Name = "Tài khoản không còn đủ điểm để thực hiện giao dịch")]
        BalanceNotMoney = 26,

        [Display(Name = "Email không tồn tại")]
        EmailNotExist = 28,

        [Display(Name = "Xe đang gặp sự cố hoặc chưa sẵn sàng cho thuê, vui lòng thử lại hoặc sử dụng xe khác")]
        BikeIsError = 29,

        [Display(Name = "Xe đang được người khác thuê")]
        BikeIsRented = 30,

        [Display(Name = "Lịch đặt xe đã hoàn thành")]
        BookingEnd = 31,

        [Display(Name = "Xe đang di chuyển không thể đặt lịch mới")]
        BookingStart = 32,

        [Display(Name = "Bạn đã đặt lịch nhưng chưa thuê xe")]
        BookingIsNotRented = 33,

        [Display(Name = "Hệ thống đang thực hiện mở khóa, vui lòng đợi kết quả xử lý")]
        OpenLockRequestExist = 34,

        [Display(Name = "Không tồn tại lệnh mở khóa")]
        OpenLockRequestNotExist = 35,

        [Display(Name = "Xe đang được thuê")]
        DockBusy = 36,

        [Display(Name = "Trạm không còn xe")]
        StationNotDock = 37,

        [Display(Name = "Tài khoản không đủ điều kiện để thực hiện giao dịch này")]
        NotMoney = 38,

        [Display(Name = "Đã tồn tại lịch đặt xe, vui lòng hủy lịch trước đó và thử lại")]
        ExistBooking = 39,

        [Display(Name = "Lịch đặt ở trạng thái đang đặt mới có thể hủy")]
        DontCancelBooking = 40,

        [Display(Name = "Đây không phải xe bạn đặt")]
        NotBikeOrder = 41,

        [Display(Name = "Không tìm thấy lịch đặt trước")]
        NotExistBooking = 42,

        [Display(Name = "Mở khóa xe thất bại vui lòng thử lại")]
        CantOpen = 43,

        [Display(Name = "Vị trí GPS của bạn đang không ở trong phạm vi trạm, vui lòng thử lại")]
        NotInStation = 44,

        [Display(Name = "Xe bạn không nằm ở trạm bạn muốn đặt")]
        NotBookingInStation = 45,

        [Display(Name = "Bạn chưa đóng khóa xe, hãy đóng khóa trước và thử lại")]
        DockNotClose = 46,

        [Display(Name = "Voucher không tồn tại")]
        VoucherNotExist = 47,

        [Display(Name = "Voucher đã quá hạn")]
        VoucherExpired = 48,

        [Display(Name = "Mã pin cũ không chính xác")]
        InvalidOldPassword = 49,

        [Display(Name = "Số tiền nạp tối thiếu từ 10.000")]
        SmallestDeposit = 50,

        [Display(Name = "Tài khoản đã được xác minh")]
        AccountDoesNotNeedVerifyOk = 51,

        [Display(Name = "Tài khoản đang chờ xác minh")]
        AccountDoesNotNeedVerifyWaiting = 52,

        [Display(Name = "Mã khuyến mãi chỉ được sử dụng một lần duy nhất, vui lòng nhập mã khác")]
        VoucherExpiredOnyOne = 53,

        [Display(Name = "Mã chia sẻ không đủ điều kiện: Tài khoản chia sẻ cần xác thực và đã từng nạp tối thiểu 20.000đ")]
        ShareCodeInvalid = 54,

        [Display(Name = "Trả xe thất bại, xe trả không phải là xe đang thuê")]
        KhongPhaiXeTra = 55,

        [Display(Name = "Tài khoản không thuộc đối tượng sử dụng mã khuyến mãi này")]
        MaKhuyenMaiKhongApDung = 56,

        [Display(Name = "Hệ thống đã gửi mã OTP, vui lòng thử lại sau một phút")]
        OTPSendTimeOut = 57,

        [Display(Name = "Xe bạn chọn không đúng với mã khóa vừa quét")]
        MaQRKhongDungVoiKhoaDaChon = 72,

        [Display(Name = "Lượng pin của xe không đủ để thực hiện giao dịch")]
        LuongPinKhongDu = 58,

        [Display(Name = "Mã vé trả trước hết hạn sử dụng hoặc không tồn tại")]
        VeTraTruocKhongTonTai = 59,

        [Display(Name = "Tài khoản không thuộc diện áp dụng giá vé ưu đãi này")]
        GiaVeKhongThuocNhomKhachHang = 60,

        [Display(Name = "Hiện tại hệ thống hết giờ làm việc, không thể thực hiện giao dịch mở khóa xe")]
        HetCaLamViec = 61,

        [Display(Name = "Bạn đang trong chuyến đi, thao tác này không hiệu lực")]
        DaTonTaiChuyenDiTruoc = 62,

        [Display(Name = "Không thể hủy chuyến đi do đã quá thời gian cho phép")]
        VuotQuaThoiGianCoTheHuy = 63,

        [Display(Name = "Mở lại thất bại, xe này không phải xe bạn đang thuê")]
        KhongPhaiXeBanMuonMoLai = 64,

        [Display(Name = "Mã vé khuyến mãi không tồn tại")]
        MaVeKhuyenMaiKhongTonTai = 65,

        [Display(Name = "Sử dụng mã khuyến mại thất bại, mã khuyễn mãi đã hết số lần sử dụng")]
        MaKhuyenMaiHetSoLan = 66,

        [Display(Name = "Mã khuyến mãi chưa khả dụng hoặc đã quá hạn")]
        MaKhuyenMaiKhongConHieuLuc = 67,
        //Chiến dịch khuyến mãi này bạn đã tham gia rồi
        [Display(Name = "Sử dụng mã khuyến mại thất bại, tài khoản của bạn đã tham gia chiến dịch khuyến mãi này rồi")]
        MaKhuyenMaiBanDaThamGiaRoi = 68,

        [Display(Name = "Sử dụng mã khuyến mại thất bại, tài khoản của bạn không thuộc đối tượng sử dụng mã này")]
        MaKhuyenMaNayKhongApDungVoiBan = 69,

        [Display(Name = "Điểm tích lũy không đủ để quy đổi, hãy tiếp tục tham gia các chuyến đi để tích lũy thêm")]
        DiemChuyenDiKhongDu = 70,

        [Display(Name = "Hệ thống đang xử lý yêu cầu, vui lòng thử lại trong ít giây")]
        DockIsProcess = 71,

        [Display(Name = "Hết thời gian thực hiện giao dịch")]
        HetThoiGianThucHienGD = 73,

        [Display(Name = "Tài khoản hủy giao dịch")]
        TaiKhoanHuyGiaoDich = 74,

        [Display(Name = "Nạp điểm thành công")]
        NapTienThanhCong = 75,

        [Display(Name = "Giao dịch nạp tiền của bạn đang ở trạng thái chờ, vui lòng chờ trong ít phút để xử lý, bạn có thể liên hệ hotline để kiểm tra giao dịch")]
        NapTienThatBai = 76,

        [Display(Name = "Giao dịch thất bại, vui lòng thử lại")]
        TaoDonHangThatBai = 77,

        [Display(Name = "Giao dịch chưa thanh toán hoặc giao dịch đang xử lý")]
        DongHangChuaThanhToan = 78,

        [Display(Name = "Vị trí GPS hiện tại của bạn không gần vị trí xe, vui lòng thử lại hoặc chọn xe khác")]
        NotInDock = 79,

        [Display(Name = "Tài khoản đang nợ cước nên không thể thực hiện thao tác này, vui lòng kiểm tra lại")]
        DebtCharges = 80,

        [Display(Name = "Vé trả trước cũ bạn còn hiệu lưc, vui lòng sử dụng hết hạn mới có thể mua tiếp")]
        VeTraTruocCuConHieuLuc = 81,

        [Display(Name = "Tài khoản gửi không được là tài khoản nhận, vui long kiểm tra lại")]
        TaiKhoanGuiKhongDuocLaTaiKhoanNhan = 82,

        [Display(Name = "Tài khoản không còn đủ điểm khuyến mãi để thực hiện")]
        SubBalanceNotMoney = 83,

        [Display(Name = "Tài khoản còn nợ cước chuyến đi, vui lòng nạp thêm điểm để tiếp tục sử dụng dịch vụ")]
        TaiKhoanConNoCuoc = 84,

        [Display(Name = "Bạn đang trong chuyến đi nên không thực hiện được thao tác này")]
        DangTrongChuyenDi = 85,

        [Display(Name = "Tài khoản của bạn cần xác minh mới có thể thực hiện thao tác này")]
        TaiKhoanCanXacMinh = 86,

        [Display(Name = "Xe hết pin vui lòng điện hotline để được hỗ trợ")]
        XeHetPin = 87,

        [Display(Name = "Chức năng này tạm thời bị vô hiệu hóa")]
        ChucNangNayDangBiVoHieuHoa = 88,

        [Display(Name = "Tạo tài khoản thất bại, thông tin không hợp lệ vui lòng thử lại")]
        TaoTaiKhoanThatBai = 89,

        [Display(Name = "Số điểm muốn tặng phải tối thiểu {0}")]
        SoDiemToiThieuMuonTang = 90,

        [Display(Name = "Tài khoản nhận phải cần được xác thực mới có thể nhận được điểm")]
        TaiKhoanNhanChiaSePhaiXacNhan = 91,

        [Display(Name = "Bạn đang trong chuyến đi không thể chia sẻ điểm")]
        DangTrongChuyenDiKhongTheChiaSeDiem = 92,

        [Display(Name = "Không thể chia sẻ điểm, điều kiện sau khi chia sẻ thì tài khoản phải còn ít nhất {0}")]
        TaiKhoanKhongDuChiaSeDiem = 93,

        [Display(Name = "Bạn thao tác quá nhanh, vui lòng đợi ít giây")]
        ThaoTacQuaNhanh = 94,

        [Display(Name = "Bạn không đủ điều kiện thuê thêm xe, vui lòng nạp thêm điểm và thực hiện tại")]
        KhongDuDieuKienThueThemXeVeLuot = 95,

        [Display(Name = "Bạn không đủ điều kiện thuê thêm xe, vui lòng gia hạn thêm vé ngày và thực hiện lại")]
        KhongDuDieuKienThueThemXeVeNgay = 96,

        [Display(Name = "Bạn chỉ được thuê cùng lúc {0} xe, mọi thắc mắc vui lòng liên hệ chăm sóc khách hàng")]
        VuotQuaSoXeBanMuonThue = 97,

        [Display(Name = "Bạn không đủ điều kiện thuê xe thứ 2, tài khoản cần phải được xác thực")]
        VeNhomPhaiXacThuc = 98,

        [Display(Name = "Mã giảm giá chỉ áp dụng với vé block")]
        MaGiamGiaChiApDungVeBlock = 99,

        [Display(Name = "Bạn chưa đóng khóa xe, hoặc khóa chưa phản hồi, vui lòng liên hệ chăm sóc khách hàng để được trợ giúp")]
        DockNotCloseKeyOpen = 100,

        [Display(Name = "Tài khoản đang bị khóa, vui lòng liên hệ hotline để hỗ trợ")]
        TaiKhoanDangBiKhoa = 101,

        [Display(Name = "Không thể xóa, bài đăng này đã được duyệt")]
        KhongTheXoaBaiDang = 102,

        [Display(Name = "Bạn đang tham gia chương trình này rồi, bạn không thể tiếp tục thực hiện thao tác này")]
        BanDaThamGiaChuongTrinhHinhAnh = 103,

        [Display(Name = "{0}, {1} điểm cho {2} phút đầu tiên, nếu vượt quá quy định mỗi {3} phút vượt qua tiếp theo tính thêm {4} điểm")]
        NoiDungChiTietVe = 1001,

        [Display(Name = "{0}, {1} điểm cho {2} phút sử dụng trong ngày, nếu vượt quá quy định mỗi {3} phút vượt qua tiếp theo tính thêm {4} điểm")]
        NoiDungChiTietVeNgay = 1002,

        [Display(Name = "{0}, {1} điểm cho {2} phút sử dụng trong tháng, nếu vượt quá quy định mỗi {3} phút vượt qua tiếp theo tính thêm {4} điểm")]
        NoiDungChiTietVeThang = 1003,


        [Display(Name = "Bạn cần được xác thực tài khoản mới thực hiện được thao tác này")]
        TaiKhoanCanXacThucMoiTaoNhom = 2000,
        [Display(Name = "Một ngày bạn chỉ được tạo 2 phòng đạp xe")]
        NhomDiXeDuocTao2PhongTrongNgay = 2001,
        [Display(Name = "Chức năng tạo phòng của bạn tạm thời bị khóa, vui lòng liên hệ hỗ trợ khách hàng để biết thêm chi tiết")]
        NhomDiXeBiKhoaTaoPhong = 2002,
        [Display(Name = "Hiện tại bạn đã tạo nhóm đạp xe rồi, vui lòng kiểm tra lại")]
        NhomDapXeDaTonTaiRoom = 2003,
        [Display(Name = "Bạn chưa bật GPS ở thiết bị hoặc GPS không chính xác")]
        GPSKhongHopLe = 2004,
        [Display(Name = "Thời gian bắt đầu không hợp lệ, phải lớn hơn thời gian hiện tại 15 phút")]
        NhomDiXeThoiGianBatDauKhongHopLe = 2005,
        [Display(Name = "Trạm này không thuộc khu vực của bạn, vui lòng chọn trạm khác")]
        NhomDiXeTramKhongThuocPhamVi = 2006,
        [Display(Name = "Bạn đang ở trong nhóm đạp xe khác, vui lòng kiểm tra lại")]
        NhomDiXeBanDangONhomKhac = 2007,
        [Display(Name = "Bạn không thể gửi tối đa 5 lời đề nghị tham gia nhóm cùng lúc")]
        NhomDiXeKhongQua5XinGiaNhapNhom = 2008,
        [Display(Name = "Bạn chỉ tham gia được nhóm đang chờ")]
        NhomDiXeChiXinThamGiaNhomDangCho = 2009,
        [Display(Name = "Bạn chỉ được hủy nhóm trước thời gian kết thúc ít nhất 30 phút")]
        NhomDiXeBanKhongDuocHuyNhom = 2010,
    }

    public class ApiResponse<T> where T : class
    {
        public bool Status { get; set; } = false;
        public string Message { get; set; }
        public ResponseCode ErrorCode { get; set; }
        public T Data { get; set; }
        public long? TotalRecord { get; set; }
        public int Limit { get; set; }
        public string Language { get; set; }
    }

    public class ApiResponseT<T> : ApiResponse<T> where T : class
    {
        public static ApiResponse<T> WithData(T data, long total = 1, int limit = 10)
        {
            var responseCodeResources = AppHttpContext.Services?.GetRequiredService<IOptions<List<ResponseCodeResources>>>().Value ?? new List<ResponseCodeResources>();
            var lang = AppHttpContext.Current?.Request.Headers["Accept-Language"].ToString() == "" ? "vi" : AppHttpContext.Current?.Request.Headers["Accept-Language"].ToString();
            var dataByLan = responseCodeResources.FirstOrDefault(x => x.Language == lang);

            var response = new ApiResponse<T>
            {
                Status = true,
                Data = data,
                Limit = limit,
                TotalRecord = total,
                Language = lang
            };

            if (dataByLan != null)
            {
                response.Message = dataByLan.Data.FirstOrDefault(x => x.Code == 0)?.Text ?? ResponseCode.Ok.GetDisplayName();
            }
            return response;
        }

        public static ApiResponse<T> WithStatusOk(T data = null)
        {
            return WithStatus(true, data, ResponseCode.Ok);
        }

        public static ApiResponse<T> WithStatusInvalidData(T data = null)
        {
            return WithStatus(false, data, ResponseCode.InvalidData);
        }

        public static ApiResponse<T> WithStatusNotOk(T data = null)
        {
            return WithStatus(false, data, ResponseCode.ServerError);
        }

        public static ApiResponse<T> WithStatus(bool status, T data, ResponseCode errorCode, params string[] errorCodeParam)
        {
            var responseCodeResources = AppHttpContext.Services?.GetRequiredService<IOptions<List<ResponseCodeResources>>>().Value ?? new List<ResponseCodeResources>();
            var lang = AppHttpContext.Current?.Request.Headers["Accept-Language"].ToString() == "" ? "vi" : AppHttpContext.Current?.Request.Headers["Accept-Language"].ToString();

            var dataByLan = responseCodeResources.FirstOrDefault(x => x.Language == lang);
            var dataOut = new ApiResponse<T>();
            if (dataByLan != null)
            {
                dataOut = new ApiResponse<T>
                {
                    Status = status,
                    ErrorCode = errorCode,
                    Data = data,
                    Message = dataByLan.Data.FirstOrDefault(x => x.Code == (int)errorCode)?.Text ?? errorCode.GetDisplayName(),
                    Language = lang
                };
            }
            else
            {
                dataOut = new ApiResponse<T>
                {
                    Status = status,
                    ErrorCode = errorCode,
                    Data = data,
                    Message = errorCode.GetDisplayName(),
                    Language = lang
                };
            }

            if (errorCodeParam != null && errorCodeParam.Count() > 0)
            {
                dataOut.Message = string.Format(dataOut.Message, errorCodeParam);
            }
            return dataOut;
        }
    }

    public class ApiResponse : ApiResponse<object>
    {
        public static ApiResponse WithData(object data, long total = 1, int limit = 10)
        {
            var responseCodeResources = AppHttpContext.Services?.GetRequiredService<IOptions<List<ResponseCodeResources>>>().Value ?? new List<ResponseCodeResources>();
            var lang = AppHttpContext.Current?.Request.Headers["Accept-Language"].ToString() == "" ? "vi" : AppHttpContext.Current?.Request.Headers["Accept-Language"].ToString();
            var dataByLan = responseCodeResources.FirstOrDefault(x => x.Language == lang);

            var response = new ApiResponse
            {
                Status = true,
                Data = data,
                Limit = limit,
                TotalRecord = total,
                Language = lang
            };

            if (dataByLan != null)
            {
                response.Message = dataByLan.Data.FirstOrDefault(x => x.Code == 0)?.Text ?? ResponseCode.Ok.GetDisplayName();
            }
            return response;
        }

        public static ApiResponse WithStatusOk(object data = null)
        {
            return WithStatus(true, data, ResponseCode.Ok);
        }

        public static ApiResponse WithStatusInvalidData(object data = null)
        {
            return WithStatus(false, data, ResponseCode.InvalidData);
        }

        public static ApiResponse WithStatusNotOk(object data = null)
        {
            return WithStatus(false, data, ResponseCode.ServerError);
        }

        public static ApiResponse WithStatus(bool status, object data, ResponseCode errorCode, params string[] errorCodeParam)
        {
            var responseCodeResources = AppHttpContext.Services?.GetRequiredService<IOptions<List<ResponseCodeResources>>>().Value ?? new List<ResponseCodeResources>();
            var lang = AppHttpContext.Current?.Request.Headers["Accept-Language"].ToString() == "" ? "vi" : AppHttpContext.Current?.Request.Headers["Accept-Language"].ToString();

            var dataByLan = responseCodeResources.FirstOrDefault(x => x.Language == lang);
            var dataOut = new ApiResponse();
            if (dataByLan != null)
            {
                dataOut = new ApiResponse
                {
                    Status = status,
                    ErrorCode = errorCode,
                    Data = data,
                    Message = dataByLan.Data.FirstOrDefault(x=>x.Code == (int)errorCode)?.Text ?? errorCode.GetDisplayName(),
                    Language = lang
                };
            }    
            else
            {
                dataOut = new ApiResponse
                {
                    Status = status,
                    ErrorCode = errorCode,
                    Data = data,
                    Message = errorCode.GetDisplayName(),
                    Language = lang
                };
            }

            if (errorCodeParam != null && errorCodeParam.Count() > 0)
            {
                dataOut.Message = string.Format(dataOut.Message, errorCodeParam);
            }
            return dataOut;
        }


    }
}
