﻿using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Model;
using static TN.Domain.Model.Account;

namespace TN.API.Model
{
    public class AccountUpdateProfileCommand
    {
        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(100)]
        public string IdentificationID { get; set; }
        public DateTime? Birthday { get; set; }
        public ESex Sex { get; set; }
        [MaxLength(500)]
        public string Address { get; set; }
        public byte[] AvatarData { get; set; }
    }

    public class AccountUpdatProfileVerifyCommand
    {
        [Required]
        public byte[] IdentificationPhotoBacksideData { get; set; }
        [Required]
        public byte[] IdentificationPhotoFrontData { get; set; }
        [Required]
        public EAccountIdentificationType IdentificationType { get; set; }
    }
}
