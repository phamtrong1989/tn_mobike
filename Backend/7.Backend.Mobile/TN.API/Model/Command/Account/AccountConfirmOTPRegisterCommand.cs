﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountConfirmOTPRegisterCommand
    {
        [Required]
        public string Phone { get; set; }
        [Required]
        public int OTP { get; set; }
    }
}
