﻿namespace TN.API.Model
{
    public class UpdateMobileInfoCommand
    {
        public string Token { get; set; }
    }
}
