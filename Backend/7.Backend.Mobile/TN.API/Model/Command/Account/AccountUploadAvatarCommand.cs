﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountUploadAvatarCommand
    {
        [Required]
        public byte[] AvatarData { get; set; }
    }

    public class AccountUploadCommand
    {
        [Required]
        public byte[] Data { get; set; }
    }

    public class PostCommand
    {
        public int AccountRatingId { get; set; }

        [MaxLength(500)]
        public string Content { get; set; }

        [MaxLength(500)]
        [Required]
        public string Image { get; set; }

        [MaxLength(500)]
        [Required]
        public string Thumbnail { get; set; }
    }
}
