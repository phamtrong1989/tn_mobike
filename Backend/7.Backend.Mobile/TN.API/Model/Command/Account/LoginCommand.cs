﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class LoginCommand
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }

    public class UpdateDeviceCommand
    {
        [Required]
        public string Token { get; set; }
    }
}
