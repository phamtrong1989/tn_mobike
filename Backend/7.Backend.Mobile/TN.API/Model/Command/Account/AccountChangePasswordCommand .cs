﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountChangePasswordCommand
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string Password { get; set; }
        [Required, Compare("Password")]
        public string RePassword { get; set; }
    }

    public class AccountRecoveryPasswordCommand
    {
        [Required]
        public string Phone { get; set; }
        [Required]
        public int OTP { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}
