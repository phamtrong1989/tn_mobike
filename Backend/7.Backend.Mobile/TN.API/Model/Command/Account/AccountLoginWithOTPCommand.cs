﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountLoginWithOTPCommand
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public int OTP { get; set; }
    }
}
