﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class AccountRegisterCommand
    {
        [Required]
        [StringLength(20)]
        public string Phone { get; set; }

        [Required]
        [StringLength(6, ErrorMessage = "Mật khẩu phải 6 ký tự", MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        [StringLength(50)]
        public string FullName { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public ESex? Sex { get; set; }

        public DateTime? Birthday { get; set; }
    
        public byte[] AvatarData { get; set; }

        public string ShareCode { get; set; }

        [Required]
        public int OTP { get; set; }

        public string Lat { get; set; }

        public string Lng { get; set; }
    }
}
