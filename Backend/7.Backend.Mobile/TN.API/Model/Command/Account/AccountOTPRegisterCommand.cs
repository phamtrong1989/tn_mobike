﻿using System.ComponentModel.DataAnnotations;
using static TN.Domain.Model.AccountOTP;

namespace TN.API.Model
{

    public class AccountOTPRegisterCommand
    {
        [Required]
        public string Phone { get; set; }

        public OTPSystemType? Type { get; set; } = OTPSystemType.SMS;
    }
}
