﻿
using System;

namespace TN.API.Model.Setting
{
    public class MobileSettings
    {
        public int AvatarImageWidth { get; set; }
        public int VerifyImageWidht { get; set; }
        public int OtherImageWidht { get; set; }
        public int PostImageWidth { get; set; }
        public int PostThumbImageWidth { get; set; }

    }
}