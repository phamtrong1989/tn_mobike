﻿
using System;
using System.Collections.Generic;

namespace TN.API.Model.Setting
{
    public class BaseSetting
    {
        public int ApiRequestTimeout { get; set; }
        public string SecurityKey { get; set; }
        public int JwtTokenExpiredMinute { get; set; }
        public string FolderMobikeAvatarWeb { get; set; }
        public string FolderMobikeAvatarServer { get; set; }

        public int OTPInvalidMax { get; set; }
        public int OTPRegiterExpiredMinute { get; set; }
        public int ZipAvatarWidth { get; set; }

        public string AddressAPIPayment { get; set; }
        public string SmartContract { get; set; }
        public string MerchantCode { get; set; }
        public string API_ValidIssuer { get; set; }
        public string API_ValidAudience { get; set; }
        public int BikeReturnDistance { get; set; }
        public int NumRetryOpenDock { get; set; }
        public decimal ShareCodePrice { get; set; }
        public decimal OwnerShareCodePrice { get; set; }
        public string SMS_Path { get; set; }
        public string SMS_Token { get; set; }
        public string SMS_Username { get; set; }
        public string SMS_Password { get; set; }
        public string SMS_Brandname { get; set; }
        public string SMS_ContentOTP { get; set; }
        public int OTPTimeOut { get; set; }
        public int MinBattery { get; set; }
        public string SessionStartTime { get; set; }
        public string SessionEndTime { get; set; }
        public int CancelForTime { get; set; }
        public int AddTripPoint { get; set; }
        public int DockProcessAllowTime { get; set; }
        public string TransactionSecretKey { get; set; }
        public string TokenEventDock { get; set; }
        public string AllowReferer { get; set; }
        public decimal MinGiftPoint { get; set; }
        public double InStationDelta { get; set; }
        public int NonServiceDistance { get; set; }
        public decimal MinTotalDepositAmount { get; set; }
        public decimal MinTotalStartBooking { get; set; }
        public string ShareCodeInfo { get; set; }
        public double WarningOutRangeDeta { get; set; }
        public int MaxDistanceDockOpenInBooking { get; set; }
        public bool WarningSystemStatus { get; set; }
        public int TimeOutSendLocation { get; set; }
        public bool EnableMemoryCache { get; set; }
        public bool EnableDiscountTransaction { get; set; }
        public bool ControllAPIOnyLocal { get;  set; }
        public bool AddTransactionOpenLock { get; set; }
        public string FolderMobikeImageServer { get;  set; }
        public string FolderMobikeImageWeb { get; set; }
    }
}