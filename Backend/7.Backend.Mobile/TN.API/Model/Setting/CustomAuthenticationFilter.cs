﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IdentityModel.Tokens.Jwt;
using TN.API;
using TN.API.Model;
using TN.Domain.Model;

namespace ActionFilters.Filters
{
    public class AllowOnyLocalAttribute : TypeFilterAttribute
    {
        public AllowOnyLocalAttribute() : base(typeof(AllowOnyLocalAttributeFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class AllowOnyLocalAttributeFilter : Attribute, IAuthorizationFilter
    {
        private readonly TN.API.Model.Setting.BaseSetting _baseSettings;
        public AllowOnyLocalAttributeFilter(IOptions<TN.API.Model.Setting.BaseSetting> baseSettings)
        {
            _baseSettings = baseSettings.Value;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var domain = context.HttpContext.Request.Host.Host.ToString();
            if(domain != "localhost" && _baseSettings.ControllAPIOnyLocal)
            {
                context.Result = new UnauthorizedResult();
            }    
        }
    }

    public class AppVerificationControlDeviceAttribute : TypeFilterAttribute
    {
        public AppVerificationControlDeviceAttribute() : base(typeof(AppVerificationControlDeviceFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class AppVerificationControlDeviceFilter : Attribute, IAuthorizationFilter
    {
        private readonly TN.API.Model.Setting.BaseSetting _baseSettings;
        public AppVerificationControlDeviceFilter(IOptions<TN.API.Model.Setting.BaseSetting> baseSettings)
        {
            _baseSettings = baseSettings.Value;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var toke = context.HttpContext.Request.Headers["Authorization"].ToString();
            if(toke != _baseSettings.TokenEventDock)
            {
                context.Result = new UnauthorizedResult();
            }    
        }
    }

    public class CheckTimeOutRequestAttribute : TypeFilterAttribute
    {
        public CheckTimeOutRequestAttribute() : base(typeof(CheckTimeOutRequestAttributeFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class CheckTimeOutRequestAttributeFilter : Attribute, IAuthorizationFilter
    {
        private readonly TN.API.Model.Setting.BaseSetting _baseSettings;
        private readonly ILogger _logger;
        public CheckTimeOutRequestAttributeFilter(IOptions<TN.API.Model.Setting.BaseSetting> baseSettings, ILogger<SetOnlineAccountAttributeFilter> logger)
        {
            _baseSettings = baseSettings.Value;
            _logger = logger;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                if (Startup.AccountLocations == null)
                {
                    Startup.AccountLocations = new System.Collections.Generic.Dictionary<int, AccountLocationModel>();
                }

                if (context.HttpContext.User.FindFirst(JwtRegisteredClaimNames.Sid) != null)
                {
                    var controller = context.RouteData.Values["controller"].ToString();
                    var action = context.RouteData.Values["action"].ToString();
                    int accountId = Convert.ToInt32(context.HttpContext.User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value);

                    if (!Startup.AccountLocations.ContainsKey(accountId))
                    {
                        Startup.AccountLocations.Add(accountId, new AccountLocationModel { LastActive = DateTime.Now });
                    }
                    else
                    {
                        if(Startup.AccountLocations[accountId] == null)
                        {
                            Startup.AccountLocations[accountId] = new AccountLocationModel { };
                        }    
                        Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                    }

                    if (!string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(action))
                    {
                        if (Startup.AccountLocations[accountId] == null)
                        {
                            Startup.AccountLocations[accountId] = new AccountLocationModel { };
                        }
                        if (controller == "Booking" && (action == "BookingScanSubmit" || action == "BookingCheckIn") && (Startup.AccountLocations[accountId].Last_Booking_BookingScanSubmit == null || Startup.AccountLocations[accountId].Last_Booking_BookingScanSubmit?.AddSeconds(2) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_Booking_BookingScanSubmit = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else if (controller == "TicketPrepaid" && action == "Buy" && (Startup.AccountLocations[accountId].Last_TicketPrepaid_Buy == null || Startup.AccountLocations[accountId].Last_TicketPrepaid_Buy?.AddSeconds(2) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_TicketPrepaid_Buy = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else if (controller == "Controls" && action == "CheckService" && (Startup.AccountLocations[accountId].Last_Controls_CheckService == null || Startup.AccountLocations[accountId].Last_Controls_CheckService?.AddSeconds(_baseSettings.TimeOutSendLocation) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_Controls_CheckService = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else if (controller == "Controls" && action == "SendLocation" && (Startup.AccountLocations[accountId].Last_Controls_SendLocation == null || Startup.AccountLocations[accountId].Last_Controls_SendLocation?.AddSeconds(_baseSettings.TimeOutSendLocation) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_Controls_SendLocation = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else if (controller == "Booking" && action == "BookingCheckOut" && (Startup.AccountLocations[accountId].Last_Booking_BookingCheckOut == null || Startup.AccountLocations[accountId].Last_Booking_BookingCheckOut?.AddSeconds(5) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_Booking_BookingCheckOut = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else if (controller == "Booking" && action == "BookingReOpen" && (Startup.AccountLocations[accountId].Last_Booking_BookingReOpen == null || Startup.AccountLocations[accountId].Last_Booking_BookingReOpen?.AddSeconds(5) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_Booking_BookingReOpen = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else if (controller == "Post" && action == "Add" && (Startup.AccountLocations[accountId].Last_Post_Add == null || Startup.AccountLocations[accountId].Last_Post_Add?.AddSeconds(5) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_Post_Add = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else if (controller == "Post" && action == "Like" && (Startup.AccountLocations[accountId].Last_Post_Like == null || Startup.AccountLocations[accountId].Last_Post_Like?.AddSeconds(2) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_Post_Like = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else if (controller == "Post" && action == "Remove" && (Startup.AccountLocations[accountId].Last_Post_Remove == null || Startup.AccountLocations[accountId].Last_Post_Remove?.AddSeconds(2) < DateTime.Now))
                        {
                            Startup.AccountLocations[accountId].Last_Post_Remove = DateTime.Now;
                            Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                        }
                        else
                        {
                            context.Result = new RedirectToActionResult("DefaultData", "Controls", null);
                        }    
                    }    
                }    
            }
            catch (Exception ex)
            {
                Startup.AccountLocations = new System.Collections.Generic.Dictionary<int, AccountLocationModel>();
                _logger.LogError($"CheckTimeOutRequestAttributeFilter.OnAuthorization => {ex}");
            }
        }
    }

    public class SetOnlineAccountAttribute : TypeFilterAttribute
    {
        public SetOnlineAccountAttribute() : base(typeof(SetOnlineAccountAttributeFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class SetOnlineAccountAttributeFilter : Attribute, IAuthorizationFilter
    {
        private readonly TN.API.Model.Setting.BaseSetting _baseSettings;
        private readonly ILogger _logger;
        public SetOnlineAccountAttributeFilter(IOptions<TN.API.Model.Setting.BaseSetting> baseSettings, ILogger<SetOnlineAccountAttributeFilter> logger)
        {
            _baseSettings = baseSettings.Value;
            _logger = logger;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                if (Startup.AccountLocations == null)
                {
                    Startup.AccountLocations = new System.Collections.Generic.Dictionary<int, AccountLocationModel>();
                }    

                if (context.HttpContext.User.FindFirst(JwtRegisteredClaimNames.Sid) != null)
                {
                    int accountId = Convert.ToInt32(context.HttpContext.User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value);

                    if (!Startup.AccountLocations.ContainsKey(accountId))
                    {
                        Startup.AccountLocations.Add(accountId, new AccountLocationModel { LastActive = DateTime.Now });
                    }
                    else
                    {
                        if (Startup.AccountLocations[accountId] == null)
                        {
                            Startup.AccountLocations[accountId] = new AccountLocationModel { };
                        }
                        Startup.AccountLocations[accountId].LastActive = DateTime.Now;
                    }
                }    
            }
            catch(Exception ex)
            {
                Startup.AccountLocations = new System.Collections.Generic.Dictionary<int, AccountLocationModel>();
                _logger.LogError($"SetOnlineAccountAttributeFilter.OnAuthorization => {ex}");
            }
        }
    }
}