﻿namespace TN.API.Model.Setting
{
    public class PayooSettings
    {
        public string ApiPayooCheckout { get; set; }
        public string UrlPayooAPI { get; set; }
        public string IPPayoo { get; set; }
        public string BusinessUsername { get; set; }
        public long ShopID { get; set; }
        public string ShopTitle { get; set; }
        public string ShopDomain { get; set; }
        public string ShopBackUrl { get; set; }
        public string ChecksumKey { get; set; }
        public string APIUsername { get; set; }
        public string APIPassword { get; set; }
        public string APISignature { get; set; }
        public string Description { get; set; }
        public string NotifyUrl { get; set; }
        public string MerchantShareKey { get;  set; }
    }
}