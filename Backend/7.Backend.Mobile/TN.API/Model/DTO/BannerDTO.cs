﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class BannerDTO 
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public string Data { get; set; }

        public EBannerType Type { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public object DataObject { get; set; }
    }
}
