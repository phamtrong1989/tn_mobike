﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Model
{
    public enum ETNGOHubDTO
    {
        UpdateStation = 0
    }

    public class TNGOHubDTO<T> where T : class
    {
        public int AccoutId { get; set; }
        public string DeviceId { get; set; }
        public ETNGOHubDTO Type { get; set; }
        public T Data { get; set; }
    }   
    
    public class RedeemPointDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal TripPoint { get; set; }
        public decimal ToPoint { get; set; }
        public string Note { get; set; }
        public RedeemPointDTO(RedeemPoint data)
        {
            Id = data.Id;
            Name = data.Name;
            TripPoint = data.TripPoint;
            ToPoint = data.ToPoint;
            Note = data.Note;
        }
    }

    public class TransactionListDTO
    {
        public int Id { get; set; }
        public string TransactionCode { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public EBookingStatus Status { get; set; }
        public Station StationStart { get; set; }
        public Station StationEnd { get; set; }
    }

    public class VoucherCodeDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public EVoucherCodeType Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Stacked { get; set; }
        public int Limit { get; set; }
        public int CurentLimit { get; set; }
        public decimal? Point { get; set; }
        public string Name { get; set; }

        public VoucherCodeDTO(VoucherCode data)
        {
            Id = data.Id;
            Code = data.Code;
            Type = data.Type;
            StartDate = data.StartDate;
            EndDate = data.EndDate;
            Stacked = data.Stacked;
            Limit = data.Limit;
            CurentLimit = data.CurentLimit;
            Point = data.Point;
            Name = $"Nhập mã {Code} nhận {Function.FormatMoney(Point)} điểm";
        }
    }

    public class StationViewDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int TotalDock { get; set; } = 0;
        public StationViewDTO(Station station)
        {
            if(station == null)
            {
                Id = 0;
                Name = "Không tồn tại trạmTrạm không xác định";
                Lat = 0;
                Lng = 0;
            }   
            else
            {
                Id = station.Id;
                Name = station?.DisplayName ?? station.Name;
                Address = station.Address;
                Lat = station.Lat;
                Lng = station.Lng;
                TotalDock = station.TotalDock;
            }    
        }
    }

    public class WalletTransactionDTO
    {
        public WalletTransactionDTO(WalletTransaction obj)
        {
            Id = obj.Id;
            Type = obj.Type;
            Amount = obj.Amount;
            SubAmount = obj.SubAmount;
            TotalAmount = obj.TotalAmount;
            TripPoint = obj.TripPoint;
            Status = obj.Status;
            CreatedDate = obj.CreatedDate;
            PaymentGroup = obj.PaymentGroup;
            Note = obj.Note;
        }

        public int Id { get; set; }

        public EWalletTransactionType Type { get; set; }

        [DataType("money")]
        public decimal Amount { get; set; }

        [DataType("money")]
        public decimal SubAmount { get; set; }

        [DataType("money")]
        public decimal TotalAmount { get; set; }

        [DataType("money")]
        public decimal TripPoint { get; set; }

        public EWalletTransactionStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public EPaymentGroup PaymentGroup { get; set; } = EPaymentGroup.Default;
        public string Note { get; set; }
    }

    public class DiscountCodeDTO
    {
        public string Code { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Limit { get; set; }
        public int CurentLimit { get; set; }
        public decimal? Point { get; set; }
        public int Percent { get; set; }
        public EDiscountCodeType Type { get; set; }

        public DiscountCodeDTO(DiscountCode data)
        {
            Code = data.Code;
            StartDate = data.StartDate;
            EndDate = data.EndDate;
            CurentLimit = data.CurentLimit;
            Limit = data.Limit;
            Point = data.Point;
            Percent = data.Percent;
            Type = data.Type;
        }
    }

    public class PostDTO
    {
        public PostDTO(Post cm)
        {
            Id = cm.Id;
            AccountRatingId = cm.AccountRatingId;
            Content = cm.Content;
            Image = cm.Image;
            Thumbnail = cm.Thumbnail;
            UpdatedDate = cm.UpdatedDate ?? cm.CreatedDate;
            LikeCount = cm.LikeCount;
            Fullname = cm.Account?.FullName;
            Phone = Function.PhoneReplate(cm.Account?.Phone);
            IsLike = cm.IsLike;
            ApprovedDate = cm.ApprovedDate;
            ApprovedUse = cm.ApprovedUse;
            NotApprovedNote = cm.NotApprovedNote;
            AccountRatingName = cm.AccountRatingName;
            Status = cm.Status;
        }

        public int Id { get; set; }

        public int AccountRatingId { get; set; }

        [MaxLength(500)]
        public string Content { get; set; }

        [MaxLength(500)]
        public string Image { get; set; }

        [MaxLength(500)]
        public string Thumbnail { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int LikeCount { get; set; }
        public bool IsLike { get; set; }

        public string Phone { get; set; }

        public string Fullname { get; set; }

        public EPostStatus Status { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int? ApprovedUse { get; set; }

        public string NotApprovedNote { get; set; }
        public string AccountRatingName { get; set; }
    }

    public class AccountRatingDTO
    {
        public AccountRatingDTO(AccountRating cm)
        {
            Id = cm.Id;
            Code = cm.Code;
            Name = cm.Name;
            Note = cm.Note;
            Reward = cm.Reward;
            StartDate = cm.StartDate;
            EndDate = cm.EndDate;
        }

        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }

        [MaxLength(2000)]
        public string Reward { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
