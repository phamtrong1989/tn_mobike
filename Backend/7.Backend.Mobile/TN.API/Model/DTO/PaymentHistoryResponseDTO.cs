﻿using System;

namespace TN.API.Model
{
    public class PaymentHistoryResponseDTO
    {
        public int Id { get; set; }
        public string Txtid { get; set; }
        public string Address { get; set; }
        public Int64 Amount1 { get; set; }
        public Int64 Amount2 { get; set; }
        public DateTime Datecreated { get; set; }
        public object Currency1 { get; set; }
        public object Currency2 { get; set; }
        public object Status_url { get; set; }
        public object Qrcore_url { get; set; }
        public object RateWS { get; set; }
        public object StatusWS { get; set; }
        public object HashWS { get; set; }
        public object IdRef { get; set; }
        public object KeyRef { get; set; }
        public string TypeTrans { get; set; }
        public object RateUSD1 { get; set; }
        public object RateUSD2 { get; set; }
        public object AddressWS { get; set; }
        public int Memberid { get; set; }
        public object Txthash { get; set; }
        public object StatusWS1 { get; set; }
        public object TxtHashFee { get; set; }
        public object StatusFee { get; set; }
        public object MemberidTo { get; set; }
        public object AddressTo { get; set; }
        public object AddressFrom { get; set; }
        public object TypeName { get; set; }
        public object TxtHash1 { get; set; }
        public object StatusETH { get; set; }
        public object Username { get; set; }
        public object Note { get; set; }
        public string StatusBanking { get; set; }
        public object StatusBankingResult { get; set; }
        public object Bankid { get; set; }
        public object Fullname { get; set; }
        public object Phone { get; set; }
        public object Email { get; set; }
        public object Smartcontract { get; set; }
        public string OrderIdRef { get; set; }
        public object UrlTemp { get; set; }
    }
}
