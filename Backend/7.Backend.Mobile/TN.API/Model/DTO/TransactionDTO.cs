﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Utility;

namespace TN.API.Model
{
    public class TransactionDTO
    {
        public int Id { get; set; }
        public string TicketPrepaidCode { get; set; }
        public ETicketType TicketType { get; set; }
        [MaxLength(30)]
        public string TransactionCode { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public decimal TotalPrice { get; set; }
        public int TotalMinutes { get; }
        public double? KCal { get; }
        public double? KG { get; }
        public double? KM { get; }
        public decimal ChargeInAccount { get; set; }
        public decimal ChargeInSubAccount { get; set; }
        public string KMStr { get; set; }
        public EBookingStatus Status { get; set; }
        public StationViewDTO ObjStationIN { get; set; }
        public StationViewDTO ObjStationOut { get; set; }
        public List<GPSData> GPS { get; set; }
        public decimal TripPoint { get;  set; }
        public SelectStationBikeDTO Bike { get; set; }
        public decimal Debt { get; set; }

        public TransactionDTO(Transaction item, StationViewDTO stationIn, StationViewDTO stationOut, List<GPSData> gps)
        {
            Id = item.Id;
            TicketType = item.TicketPrice_TicketType;
            TicketPrepaidCode = item.TicketPrepaidCode;
            TransactionCode = item.TransactionCode;
            StartTime = item.StartTime;
            EndTime = item.EndTime;
            TotalPrice = item.TotalPrice;
            Status = item.Status;
            ObjStationIN = stationIn;
            ObjStationOut = stationOut;
            ChargeInAccount = item.ChargeInAccount;
            ChargeInSubAccount = item.ChargeInSubAccount;
            TotalMinutes = item.TotalMinutes;
            KCal = item.KCal;
            KG = item.KG;
            KM = item.KM;
            GPS = gps;
            TripPoint = item.TripPoint;
            KMStr = Function.DistanceString(Convert.ToDecimal(item.KM ?? 0));
            Debt = item.ChargeDebt ?? 0;
            Bike = new SelectStationBikeDTO(item.Bike, item.Dock); 
        }
    }
}
