﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Backend.Model.Command
{
    public class TicketPriceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TenantId { get; set; }
        public int CustomerGroupId { get; set; }
        public ETicketType TicketType { get; set; }
        public bool AllowChargeInSubAccount { get; set; }
        public decimal TicketValue { get; set; }
        public int? BlockPerMinute { get; set; }
        public int? BlockPerViolation { get; set; }
        public decimal? BlockViolationValue { get; set; }
        public int LimitMinutes { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public string Note { get; set; }
        public int BookingId { get; set; }
        public Project Project { get; set; }
        public Booking Booking { get; set; }

        public TicketPriceDTO(TicketPrice item, Project project, Booking booking = null)
        {
            Id = item.Id;
            Name = item.Name;
            TenantId = item.ProjectId;
            CustomerGroupId = item.CustomerGroupId;
            TicketType = item.TicketType;
            AllowChargeInSubAccount = item.AllowChargeInSubAccount;
            TicketValue = item.TicketValue;
            BlockPerMinute = item.BlockPerMinute;
            BlockPerViolation = item.BlockPerViolation;
            BlockViolationValue = item.BlockViolationValue;
            LimitMinutes = item.LimitMinutes;
            IsDefault = item.IsDefault;
            IsActive = item.IsActive;
            Project = project;

            if (item.TicketType == ETicketType.Block)
            {
                Note = $"{Convert.ToInt32(item.TicketValue)} điểm cho {item.BlockPerMinute} phút đầu tiên, mỗi {item.BlockPerViolation} phút vượt qua tiếp theo cộng thêm {Convert.ToInt32(item.BlockViolationValue)} điểm";
            }
            else if (item.TicketType == ETicketType.Day)
            {
                Note = $"{Convert.ToInt32(item.TicketValue)} điểm cho {item.LimitMinutes} phút sử dụng trong ngày, mỗi {item.BlockPerViolation} phút vượt qua tiếp theo cộng thêm {Convert.ToInt32(item.BlockViolationValue)} điểm";
            }
            else if (item.TicketType == ETicketType.Month)
            {
                Note = $"{Convert.ToInt32(item.TicketValue)} điểm cho 1 tháng sử dụng, mỗi ngày sử dụng không quá {item.LimitMinutes} phút, mỗi {item.BlockPerViolation} phút tiếp theo cộng thêm {Convert.ToInt32(item.BlockViolationValue)} điểm";
            }

            if(booking!=null)
            {
                BookingId = booking.Id;
                Booking = booking;
            }    
        }
    }
}
