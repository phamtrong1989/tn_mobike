﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TN.Backend.Model.Command;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Model
{
    public enum ECacheType
    {
        Stations,
        BikeInStation,
        Languages,
        BannerGroup,
        Project,
        CustomerGroup,
        TicketPrices
    }   
    
    public class StationCacheDTO
    {
        public int Id { get; set; }
        public int Width { get; set; }
        public int Spaces { get; set; }
        public int Height { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int TotalDock { get; set; } = 0;
        public int TotalFreeDock { get; set; } = 0;
        public double ReturnDistance { get; set; }
    }

    public class StationDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("totalDock")]
        public int TotalDock { get; set; }

        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lng")]
        public double Lng { get; set; }

        [JsonProperty("distance")]
        public int Distance { get; set; }

        [JsonProperty("totalFreeDock")]
        public int TotalFreeDock { get; set; }

        [JsonProperty("totalParkingSpaces")]
        public int TotalParkingSpaces { get; set; }

        [JsonProperty("distanceStr")]
        public string DistanceStr { get; set; }

        [JsonProperty("returnDistance")]
        public double ReturnDistance { get; set; }

        public StationDTO()
        {

        }
        public StationDTO(Station station, int distance = 0)
        {
            if (station == null)
            {
                return;
            }
            Id = station.Id;
            Name = string.IsNullOrEmpty(station.DisplayName) ? station.Name : station.DisplayName;
            Address = station.Address;
            TotalDock = station.TotalDock;
            Lat = station.Lat;
            Lng = station.Lng;
            Distance = distance;
            TotalFreeDock = station.TotalDock;
            TotalParkingSpaces = (station.Spaces - station.TotalDock < 0) ? 0 : (station.Spaces - station.TotalDock);
            if(TotalParkingSpaces <= 0)
            {
                TotalParkingSpaces = 0;
            }    
            DistanceStr = Function.DistanceString(distance);
            ReturnDistance = station.ReturnDistance;
        }
    }

    public class SelectStationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Distance { get; set; }
        public decimal WalletBalance { get; set; }
        public decimal WalletSubBalance { get; set; }
        public int TotalFreeDock { get; internal set; }
        public int TotalParkingSpaces { get; set; }
        public List<SelectStationBikeDTO> Bikes { get;  set; }
        public SelectStationBikeDTO Bike { get; set; }
        public string DistanceStr { get; set; }
        public bool InSession { get; set; }

        public SelectStationDTO(Station station, int distance = 0)
        {
            if (station == null)
            {
                return;
            }

            Id = station.Id;
            Name = station.DisplayName ?? station.Name;
            Address = station.Address;
            Lat = station.Lat;
            Lng = station.Lng;
            Distance = distance;
            DistanceStr = Function.DistanceString(distance);
        }
    }
    public class ChartsRatingDTO
    {
        public string Reward { get; set; }
        public List<ChartsTopDTO> TopRating { get; set; }
        public ChartsTopDTO CurrentRating { get; set; }
    }

    public class ChartsTopDTO
    {
        public ChartsTopDTO()
        {

        }

        public ChartsTopDTO(AccountRatingItem use, string phone, string fullName, int currentAccountId, bool isDebt)
        {
            FullName = fullName;
            Phone = phone;
            Order = use?.Order ?? 0;
            Value = use?.Value ?? 0;
            AccountId = currentAccountId;
            IsBold = false;
        }

        public ChartsTopDTO(AccountRatingItem use, Account account, int currentAccountId)
        {
            FullName = account?.FullName;
            Phone = Function.PhoneReplate(account?.Phone);
            Order = use?.Order ?? 0;
            Value = use?.Value ?? 0;
            AccountId = account.Id;
            IsBold = account.Id == currentAccountId;
        }

        public int Order { get; set; }
        public int AccountId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public decimal Value { get; set; }
        public bool IsBold { get; set; }
        public bool IsDebt { get; set; } = false;
        public string Note { get; set; }
    }

    public class OldTopDTO
    {
        public int Order { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }
        public string Days { get; set; }
    }
}
