﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Model
{
    public class CurrentBookingDTO
    {
        public decimal TotalPrice { get; set; }
        public int TotalMinutes { get; set; }
        public int Count { get; set; }
        public List<CurrentBookingDataDTO> List { get; set; }
    }

    public class CurrentBookingDataDTO
    {
        public int Id { get; set; }
        public string TransactionCode { get; set; }
        public int BikeId { get; set; }
        public int DockId { get; set; }
        public int TotalMinutes { get; set; }
        public int TotalSeconds { get; set; }

        public decimal TripPoint { get; set; }

        public decimal TotalPrice { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? EndDate { get; set; }
        public EBookingStatus Status { get; set; }
        public SelectStationBikeDTO Bike { get; set; }
        public List<GPSData> GPS { get; set; }
        public string DiscountValue { get; set; }

        public CurrentBookingDataDTO(Transaction use, List<GPSData> gps)
        {
            Id = use.Id;
            BikeId = use.BikeId;
            DockId = use.DockId;
            TransactionCode = use.TransactionCode;
            StartDate = use.EndTime;
            CreatedDate = use.CreatedDate;
            Status = use.Status;
            TotalMinutes = Convert.ToInt32((DateTime.Now - use.StartTime).TotalMinutes);
            TotalSeconds = Convert.ToInt32((DateTime.Now - use.StartTime).TotalSeconds);
            TotalPrice = use.TotalPrice;
            TripPoint = use.TripPoint;
            EndDate = use.EndTime;
            GPS = gps;
        }

        public CurrentBookingDataDTO(Booking use)
        {
            Id = use.Id;
            BikeId = use.BikeId;
            DockId = use.DockId;
            TransactionCode = use.TransactionCode;
            StartDate = use.EndTime;
            CreatedDate = use.CreatedDate;
            Status = use.Status;
            TotalMinutes = Convert.ToInt32((DateTime.Now - use.StartTime).TotalMinutes);
            TotalSeconds = Convert.ToInt32((DateTime.Now - use.StartTime).TotalSeconds);
            TotalPrice = use.TotalPrice;
            TripPoint = use.TripPoint;
            GPS = new List<GPSData>();
        }

        public CurrentBookingDataDTO(Booking use, decimal totalPrice, SelectStationBikeDTO bike, List<GPSData> gsp, string discountValue)
        {
            Id = use.Id;
            BikeId = use.BikeId;
            DockId = use.DockId;
            TransactionCode = use.TransactionCode;
            StartDate = use.EndTime;
            CreatedDate = use.CreatedDate;
            Status = use.Status;
            TotalMinutes = Function.TotalMilutes(use.StartTime, DateTime.Now);
            TotalSeconds = Convert.ToInt32((DateTime.Now - use.StartTime).TotalSeconds);
            TotalPrice = totalPrice;
            Bike = bike;
            GPS = gsp;
            TripPoint = use.TripPoint;
            DiscountValue = discountValue;
        }
    }
}
