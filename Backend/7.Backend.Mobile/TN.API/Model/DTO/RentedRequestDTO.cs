﻿using TN.Domain.Model;

namespace TN.API.Model
{
    public class RentedRequestDTO
    {
        public int BookingId { get; set; }
        public int TransactionId { get; set; }
        public int TicketId { get; set; }

        public RentedRequestDTO(int bookingid, int transactionId, int ticketid)
        {
            this.BookingId = bookingid;
            this.TransactionId = transactionId;
            this.TicketId = ticketid;
        }
    }
}
