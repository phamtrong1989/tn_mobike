﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Model
{
    public class TransactionDetailsDTO
    {
        public Transaction Transaction { get; set; }
        public string TripID { get; set; }
        public double Time { get; set; }
        public int KG { get; set; }
        public double KCal { get; set; }
        public double KM { get; set; }
        public int BikeId { get; set; }
        public decimal Price { get; set; }
        public decimal SubPrice { get; set; }
        public List<TransactionRouteModel> Routes { get; set; }
    }

    public class CurrentTransactionDetailsDTO
    {
        public Booking Booking { get; set; }
        public string TripID { get; set; }
        public double Time { get; set; }
        public int KG { get; set; }
        public double KCal { get; set; }
        public double KM { get; set; }
        public int BikeId { get; set; }
        public decimal Price { get; set; }
        public decimal SubPrice { get; set; }
        public List<TransactionRouteModel> Routes { get; set; }
    }

    public class TransactionRouteModel
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
