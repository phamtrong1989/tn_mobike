using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class TicketPrepaidRepository : EntityBaseRepository<TicketPrepaid>, ITicketPrepaidRepository
    {
        public TicketPrepaidRepository(MobikeContext context) : base(context)
        {
        }
    }
}

