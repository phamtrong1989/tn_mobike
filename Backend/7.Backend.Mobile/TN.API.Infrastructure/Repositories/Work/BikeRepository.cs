﻿using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class BikeRepository : EntityBaseRepository<Bike>, IBikeRepository
    {
        public BikeRepository(MobikeContext context) : base(context)
        {
        }
    }
}
