using TN.Domain.Model;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class CampaignRepository : EntityBaseRepository<Campaign>, ICampaignRepository
    {
        public CampaignRepository(MobikeContext context) : base(context)
        {
        }
        public async Task<List<SumLimitModel>> GetSumLimit(List<int> campaignIds)
        {
            if(!campaignIds.Any())
            {
                return new List<SumLimitModel>();
            }

            var data = _context.VoucherCodes.Where(x => campaignIds.Contains(x.CampaignId))
                .GroupBy(x => x.CampaignId)
                .Select(x => new SumLimitModel{
                    CampaignId = x.Key,
                    CurentLimit = x.Sum(m=>m.CurentLimit),
                    Limit = x.Sum(m => m.Limit)
                });

            return await data.ToListAsync();
        }
    }
}

