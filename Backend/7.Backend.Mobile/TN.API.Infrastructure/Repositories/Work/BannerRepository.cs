﻿using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class BannerRepository : EntityBaseRepository<Banner>, IBannerRepository
    {
        public BannerRepository(MobikeContext context) : base(context)
        {
        }
    }
}
