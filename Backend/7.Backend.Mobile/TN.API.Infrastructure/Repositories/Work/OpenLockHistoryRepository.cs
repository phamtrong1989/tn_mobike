﻿using TN.API.Infrastructure.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Repositories
{
    public class OpenLockHistoryRepository : EntityBaseRepository<OpenLockHistory>, IOpenLockHistoryRepository
    {
        public OpenLockHistoryRepository(MobikeContext context) : base(context)
        {
        }
    }
}
