﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class FeedbackRepository : EntityBaseRepository<Feedback>, IFeedbackRepository
    {
        private readonly MobikeContext _mobikeContext;
        public FeedbackRepository(MobikeContext context) : base(context)
        {
            _mobikeContext = context;
        }
    }
}
