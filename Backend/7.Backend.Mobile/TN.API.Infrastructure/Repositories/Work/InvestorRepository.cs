﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class InvestorRepository : EntityBaseRepository<Investor>, IInvestorRepository
    {
        public InvestorRepository(MobikeContext context) : base(context)
        {
        }
    }
}
