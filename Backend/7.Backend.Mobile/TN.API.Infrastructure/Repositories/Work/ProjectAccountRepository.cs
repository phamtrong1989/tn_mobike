using TN.Domain.Model;
using TN.Infrastructure;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using TN.Domain.Model.Common;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure;

namespace TN.Infrastructure.Repositories
{
    public class ProjectAccountRepository : EntityBaseRepository<ProjectAccount>, IProjectAccountRepository
    {
        public ProjectAccountRepository(MobikeContext context) : base(context)
        {
        }
        public async Task<ApiResponseData<List<ProjectAccount>>> SearchPagedAsync(
            int pageIndex, 
            int pageSize,
            int? projectId,
            int? customerGroupId,
            EAccountStatus? status,
            EAccountType? type,
            ESex? sex,
            string key,
            int? id,
            Func<IQueryable<ProjectAccount>, IOrderedQueryable<ProjectAccount>> orderBy = null, 
            Expression<Func<ProjectAccount, ProjectAccount>> select = null)
        {
            pageIndex = pageIndex <= 0 ? 1 : pageIndex;
            pageSize = (pageSize > 100 || pageSize < 0) ? 10 : pageSize;

            var query = _context.ProjectAccounts.AsNoTracking();

            query = query.Where(x=> 
                (x.CustomerGroupId == customerGroupId || customerGroupId == null)
                && (x.AccountId == id || id == null)
                && (x.ProjectId == projectId || projectId == null)
            ).AsNoTracking();

            if(status!=null || type != null || sex != null || key != null)
            {
                query = query.Where(x =>  _context.Accounts.Any(m => 
                    (status == null || m.Status == status) && 
                    (type == null || m.Type == type) && 
                    (sex == null || m.Sex == sex) &&
                    (key == null || m.Address.Contains(key) || m.Phone == key || m.FirstName == key || m.LastName == key || m.Email == key)
                  )
                ).AsNoTracking();
            }    

            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }

            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }
            var data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync();
            var accountIds = data.Select(x => x.AccountId).ToList();
            var listAccount = await _context.Accounts.Where(x => accountIds.Contains(x.Id)).ToListAsync();
            foreach(var item in data)
            {
                item.Account = listAccount.FirstOrDefault(x => x.Id == item.AccountId);
                if(item.Account!=null)
                {
                    item.Account.Note = "";
                    item.Account.Password = "";
                    item.Account.PasswordSalt = "";
                }
            }    

            return new ApiResponseData<List<ProjectAccount>>
            {
                Data = data,
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = await query.CountAsync()
            };
        }
    }
}

