using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class TransactionDiscountRepository : EntityBaseRepository<TransactionDiscount>, ITransactionDiscountRepository
    {
        public TransactionDiscountRepository(MobikeContext context) : base(context)
        {
        }
    }
}

