﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using TN.Utility;

namespace TN.API.Infrastructure.Repositories
{
    public class WalletRepository : EntityBaseRepository<Wallet>, IWalletRepository
    {
        public WalletRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<decimal> TotalDepositAmount(int accountId)
        {
            return await _context.WalletTransactions.Where(x => x.AccountId == accountId && (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money) && x.Status == EWalletTransactionStatus.Done).SumAsync(x => x.Amount);
        }

        public async Task<List<SubPointExpiry>> ClearPointByAccounts(int top)
        {
            var subPointExpirys = await _context.SubPointExpirys.Where(x =>
                  x.ExpiryDate.Value < DateTime.Now.Date
                  && _context.Wallets.Any(m => m.AccountId == x.AccountId && m.SubBalance > 0)
                  && !_context.SubPointExpirySettlements.Any(m=> m.AccountId == x.AccountId && x.ExpiryDate.Value.Date == m.ExpiryDate.Date)
            ).Take(top).ToListAsync();

            var accountIds = subPointExpirys.Select(x => x.AccountId).ToList();

            var wallets = await _context.Wallets.Where(x => x.SubBalance > 0 && accountIds.Contains(x.AccountId)).ToListAsync();
            bool anyAdd = false;
            foreach (var item in subPointExpirys)
            {
                var wallet = wallets.FirstOrDefault(x => x.AccountId == item.AccountId);
                if(wallet != null)
                {
                    await _context.SubPointExpirySettlements.AddAsync(new SubPointExpirySettlement
                    {
                        AccountId = item.AccountId,
                        CreatedDate = DateTime.Now,
                        ExpiryDate = item.ExpiryDate != null ? item.ExpiryDate.Value : DateTime.Now,
                        SubBalance = wallet.SubBalance
                    });
                    anyAdd = true;
                }    
            }
            if(anyAdd)
            {
               await _context.SaveChangesAsync();
            }    
            return subPointExpirys;
        }

        public async Task<List<SubPointExpirySettlement>> SubPointExpirySettlementGets(int top)
        {
            return await _context.SubPointExpirySettlements.Where(x => x.Status == false).Take(top).ToListAsync();
        }
        public async Task SubPointExpirySettlementSetTrue(int id)
        {
            var kt = await _context.SubPointExpirySettlements.FirstOrDefaultAsync(x => x.Id == id);
            if(kt != null)
            {
                kt.Status = true;
                kt.UpdatedDate = DateTime.Now;
                _context.SubPointExpirySettlements.Update(kt);
                await  _context.SaveChangesAsync();
            }    
        }

        public Tuple<decimal, decimal> SplitTotalPrice(decimal totalPrice, bool allowChargeInSubAccount, Wallet wallet)
        {
            // Số tiền đủ để trừ tiền
            if(totalPrice <= (wallet.SubBalance + wallet.Balance))
            {
                if (totalPrice <= wallet.SubBalance)
                {
                    return new Tuple<decimal, decimal>(0, totalPrice);
                }
                else
                {
                    return new Tuple<decimal, decimal>(totalPrice - wallet.SubBalance, wallet.SubBalance);
                }
            }    
            else
            {
                // Trường hợp ko đủ trừ tiền thì sẽ phải ưu tiền tài khoản phụ âm trước
                return new Tuple<decimal, decimal>(wallet.Balance, totalPrice - wallet.Balance);
            }
        }

        public async Task<bool> WalletChecked(decimal ticketValue, Wallet wInfo, decimal? minMoney = 10000)
        {
            // Không đủ tiền trả cho 1 block
            if ((wInfo.Balance + wInfo.SubBalance) < ticketValue)
            {
                return false;
            }
            else
            {
                var dlAccount = await _context.Accounts.AsNoTracking().SingleOrDefaultAsync(x => x.Id == wInfo.AccountId);
                if (dlAccount == null)
                {
                    return false;
                }
                // Tài khoản đã xác nhận
                if(dlAccount.VerifyStatus == EAccountVerifyStatus.Ok)
                {
                    return true;
                }    
                else
                {
                    // Hoặc tài khoản nạp tối thiểu 20k
                    var total = await TotalDepositAmount(wInfo.AccountId);
                    if(total >= minMoney)
                    {
                        return true;
                    }    
                    else
                    {
                        return false;
                    }    
                }
            }
        }

        public async Task<Wallet> GetWalletAsync(int accountId)
        {
            var kt = await _context.Wallets.AsNoTracking().FirstOrDefaultAsync(x => x.AccountId == accountId);
            if (kt == null)
            {
                kt = new Wallet()
                {
                    AccountId = accountId,
                    Balance = 0,
                    Status = EWalletStatus.Active,
                    CreateDate = DateTime.Now,
                    SubBalance = 0
                };
                await _context.Wallets.AddAsync(kt);
                await _context.SaveChangesAsync();
            }

            var subEx = await _context.SubPointExpirys.FirstOrDefaultAsync(x => x.AccountId == accountId);
            if(subEx != null)
            {
                kt.SubBalanceExpiryDate = subEx.ExpiryDate;
            }
            var dataDeb = await _context.Transactions.Where(x => x.Status == EBookingStatus.Debt && x.AccountId == accountId).SumAsync(x => x.ChargeDebt);
            kt.Debt = dataDeb ?? 0;
            return kt;
        }

        public async Task<Wallet> UpdateAddWalletAsync(int accountId, decimal balance, decimal subBalance, decimal tripPoint, string haskey, bool isPush, bool enableDayExpirys, int directDayExpirys)
        {
            try
            {
                var kt = await _context.Wallets.FirstOrDefaultAsync(x => x.AccountId == accountId);
                if (kt == null)
                {
                    kt = new Wallet()
                    {
                        AccountId = accountId,
                        Balance = 0,
                        Status = EWalletStatus.Active,
                        CreateDate = DateTime.Now,
                        SubBalance = 0
                    };
                    await _context.Wallets.AddAsync(kt);
                    await _context.SaveChangesAsync();
                }
                kt.Balance += balance;
                kt.SubBalance += subBalance;
                kt.TripPoint += tripPoint;

                if (kt.Balance < 0)
                {
                    kt.Balance = 0;
                }

                if (kt.SubBalance < 0)
                {
                    kt.SubBalance = 0;
                }

                kt.HashCode = Function.SignSHA256((kt.Balance + kt.SubBalance).ToString(), haskey);

                _context.Wallets.Update(kt);
                await _context.SaveChangesAsync();

                if (balance > 0 && isPush)
                {
                    await ExpirysFirstDeposit(accountId);
                }
                // Add hạn cho các giao dịch phát sinh
                if ((balance > 0 || subBalance > 0) && isPush && enableDayExpirys)
                {
                    await AddDayExpirys(accountId, balance, subBalance, false);
                }

                // Add trực tiếp số ngày do phát sinh khi có sự kiện nạp
                if (directDayExpirys > 0 && balance > 0 && isPush && enableDayExpirys)
                {
                    await AddDayExpirysByDirect(accountId, directDayExpirys, "Phát sinh trong sự kiện nạp điểm trong TG khuyến mãi");
                }

                if (isPush)
                {
                    return await HandlingDebtCharges(accountId, kt, haskey);
                }

                return kt;
            }
            catch
            {
                return null;
            }
        }

        public async Task AddDayExpirysByDirect(int accountId, int days, string note)
        {
            try
            {
                var ktA = await _context.SubPointExpirys.FirstOrDefaultAsync(x => x.AccountId == accountId);
                if (ktA == null)
                {
                    ktA = new SubPointExpiry
                    {
                        AccountId = accountId,
                        IsFirst = false
                    };
                    await _context.SubPointExpirys.AddAsync(ktA);
                    await _context.SaveChangesAsync();
                }
                // Add Log
                var start = (ktA.ExpiryDate == null || ktA.ExpiryDate.Value.Date < DateTime.Now.Date) ? DateTime.Now.Date : ktA.ExpiryDate.Value.Date;
                await _context.SubPointExpiryLogs.AddAsync(new SubPointExpiryLog
                {
                    CreatedDate = DateTime.Now,
                    AccountId = accountId,
                    Days = days,
                    ExpiryDate = start,
                    Note = note
                });
                await _context.SaveChangesAsync();

                ktA.PrvExpiryDate = start;
                ktA.ExpiryDate = start.AddDays(days);

                if (ktA.ExpiryDate.Value > DateTime.Now && (ktA.ExpiryDate.Value - DateTime.Now).TotalDays > 365)
                {
                    ktA.ExpiryDate = DateTime.Now.AddDays(365);
                }

                _context.SubPointExpirys.Update(ktA);
                await _context.SaveChangesAsync();
            }
            catch
            {
            }
        }

        private async Task<Wallet> HandlingDebtCharges(int accountId, Wallet wallet, string hasKey)
        {
            // Bất kỳ có phát sinh giao dịch + tk chính hay tk phụ đều phải xử lý cắt nợ
            // Trừ tài khoản phụ hết sau đó trừ tài khoản chính
            // Lấy ra giao dịch xe qua nợ cước
            var transactions = await _context.Transactions.Where(x => x.AccountId == accountId && x.Status == EBookingStatus.Debt && x.ChargeDebt <= (wallet.Balance + wallet.SubBalance)).ToListAsync();
            if (!transactions.Any())
            {
                return wallet;
            }

            foreach (var item in transactions)
            {
                if ((wallet.Balance + wallet.SubBalance) < item.ChargeDebt)
                {
                    continue;
                }
                // Cắt xem có thể trừ tk nào
                var splitprice = SplitTotalPrice(item.ChargeDebt ?? 0, item.TicketPrice_AllowChargeInSubAccount, wallet);
                string textNote = $"Hệ thống tự động thanh toán {Function.FormatMoney(item.ChargeDebt)} điểm nợ cước mã chuyến đi {item.TransactionCode}";
                // Tạo lịch sử giao dịch
                await _context.WalletTransactions.AddAsync(new WalletTransaction
                {
                    AccountId = accountId,
                    WalletId = wallet.Id,
                    Type = EWalletTransactionType.TruTienNoCuoc,
                    TransactionCode = item.TransactionCode,
                    OrderIdRef = $"{accountId:D6}-{DateTime.Now:yyyyMMddHHmmssfff}",
                    Amount = splitprice.Item1,
                    SubAmount = splitprice.Item2,
                    Status = EWalletTransactionStatus.Done,
                    TotalAmount = splitprice.Item1 + splitprice.Item2,
                    CreatedDate = DateTime.Now,
                    Wallet_Balance = wallet.Balance,
                    Wallet_SubBalance = wallet.SubBalance,
                    Wallet_HashCode = wallet.HashCode,
                    PaymentGroup = EPaymentGroup.Default,
                    PadTime = DateTime.Now,
                    Note = textNote,
                    HashCode = Function.SignSHA256((splitprice.Item1 + splitprice.Item2).ToString(), hasKey),
                    TransactionId = item.Id
                });
                await _context.SaveChangesAsync();
                //Cập nhật ví 
                await UpdateAddWalletAsync(accountId, -splitprice.Item1, -splitprice.Item2, 0, hasKey, false, false, 0);
                // Cập nhật giao dịch
                item.Status = EBookingStatus.End;
                item.UpdatedDate = DateTime.Now;
                item.ChargeInAccount += splitprice.Item1;
                item.ChargeInSubAccount += splitprice.Item2;
                _context.Transactions.Update(item);
                await _context.SaveChangesAsync();
                // Gửi thông báo cho khác về thanh toàn
                await _context.NotificationJobs.AddAsync(new NotificationJob
                {
                    AccountId = accountId,
                    CreatedDate = DateTime.Now,
                    DeviceId = "",
                    IsSend = false,
                    IsSystem = true,
                    LanguageId = 0,
                    PublicDate = DateTime.Now,
                    Tile = "TNGO thông báo",
                    Message = textNote
                });
                await _context.SaveChangesAsync();
            }
            return wallet;
        }

        public async Task AddDayExpirys(int accountId, decimal balance, decimal subBalance, bool isEndTransaction)
        {
            try
            {
                int tile = isEndTransaction ? 5000 : 2000;
                // 2k 1 ngày
                int totalData = 0;
                totalData += Convert.ToInt32(balance / tile);
                if(subBalance > 0)
                {
                    totalData += Convert.ToInt32(subBalance / (tile * 2));
                }   
                
                if (totalData <= 0)
                {
                    return;
                }

                var ktA = await _context.SubPointExpirys.FirstOrDefaultAsync(x => x.AccountId == accountId);
                if (ktA == null)
                {
                    ktA = new SubPointExpiry
                    {
                        AccountId = accountId,
                        IsFirst = false
                    };
                    await _context.SubPointExpirys.AddAsync(ktA);
                    await _context.SaveChangesAsync();
                }
                // Add Log
                var start = (ktA.ExpiryDate == null || ktA.ExpiryDate.Value.Date < DateTime.Now.Date) ? DateTime.Now.Date : ktA.ExpiryDate.Value.Date;
                await _context.SubPointExpiryLogs.AddAsync(new SubPointExpiryLog
                {
                    CreatedDate = DateTime.Now,
                    AccountId = accountId,
                    Days = totalData,
                    ExpiryDate = start,
                    Note = $"{(isEndTransaction ? "Phát sinh khi kết thúc chuyến đi" : "Phát sinh điểm ")} {balance} được thêm {totalData} ngày"
                });
                await _context.SaveChangesAsync();
                ktA.PrvExpiryDate = start;
                ktA.ExpiryDate = start.AddDays(totalData);

                if (ktA.ExpiryDate.Value > DateTime.Now && (ktA.ExpiryDate.Value - DateTime.Now).TotalDays > 365)
                {
                    ktA.ExpiryDate = DateTime.Now.AddDays(365);
                }

                _context.SubPointExpirys.Update(ktA);
                await _context.SaveChangesAsync();
            }
            catch
            {
            }
        }

        private async Task<bool> ExpirysFirstDeposit(int accountId)
        {
            try
            {
                var ktA = await _context.SubPointExpirys.FirstOrDefaultAsync(x => x.AccountId == accountId);
                if (ktA == null)
                {
                    ktA = new SubPointExpiry
                    {
                        AccountId = accountId,
                        IsFirst = false
                    };
                    await _context.SubPointExpirys.AddAsync(ktA);
                    await _context.SaveChangesAsync();
                }

                if (ktA.IsFirst)
                {
                    return false;
                }

                var ktFirst = await _context.WalletTransactions.AnyAsync(x =>
                    x.AccountId == accountId
                    && (x.Status == EWalletTransactionStatus.Done || x.Status == EWalletTransactionStatus.Pending)
                    && (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                );

                if (ktFirst)
                {
                    // Add Log
                    var start = ktA.ExpiryDate == null ? DateTime.Now : ktA.ExpiryDate.Value;

                    await _context.SubPointExpiryLogs.AddAsync(new SubPointExpiryLog
                    {
                        CreatedDate = DateTime.Now,
                        AccountId = accountId,
                        Days = 60,
                        ExpiryDate = start,
                        Note = "Nạp lần đầu +60 ngày hạn(tương đương 2 tháng)"
                    });
                    await _context.SaveChangesAsync();

                    ktA.PrvExpiryDate = start;
                    ktA.ExpiryDate = start.AddDays(60);
                    ktA.IsFirst = true;
                    _context.SubPointExpirys.Update(ktA);
                    await _context.SaveChangesAsync();
                    return true;
                }
                else
                {
                    return false;
                }    
            }
            catch
            {
                return false;
            }
        }
    }
}


