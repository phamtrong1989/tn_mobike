using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure;

namespace TN.Infrastructure.Repositories
{
    public class RedeemPointRepository : EntityBaseRepository<RedeemPoint>, IRedeemPointRepository
    {
        public RedeemPointRepository(MobikeContext context) : base(context)
        {
        }
    }
}

