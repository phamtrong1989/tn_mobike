﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using PT.Domain.Model;

namespace TN.API.Infrastructure.Repositories
{
    public class StationRepository : EntityBaseRepository<Station>, IStationRepository
    {
        private readonly ITransactionRepository _iTransactionRepository;
        public StationRepository(MobikeContext context, ITransactionRepository iTransactionRepository ) : base(context)
        {
            _iTransactionRepository = iTransactionRepository;
        }

        public async Task<AppVersion> CurrentVersion()
        {
            var data = await _context.AppVersions.AsNoTracking().FirstOrDefaultAsync(x=>x.Id == 1);
            return data;
        }

        public async Task<List<Project>> ProjectByIds(List<int> ids)
        {
            return await _context.Projects.Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<List<Station>> FindByLatLng(double min_lat, double max_lat, double min_lng, double max_lng)
        {
            return await _context.Stations.Where(x => x.Lat >= min_lat && x.Lat <= max_lat && x.Lng >= min_lng && x.Lng <= max_lng && x.Status == ESationStatus.Active).ToListAsync();
        }

        public async Task<bool> FindByLatLngAny(double min_lat, double max_lat, double min_lng, double max_lng)
        {
            return await _context.Stations.AnyAsync(x => x.Lat >= min_lat && x.Lat <= max_lat && x.Lng >= min_lng && x.Lng <= max_lng && x.Status == ESationStatus.Active);
        }

        public bool FindByLatLngAnyByList(List<Station> stations,double min_lat, double max_lat, double min_lng, double max_lng)
        {
            return  stations.Any(x => x.Lat >= min_lat && x.Lat <= max_lat && x.Lng >= min_lng && x.Lng <= max_lng && x.Status == ESationStatus.Active);
        }

        public List<Station> FindByLatLngByList(List<Station> stations, double min_lat, double max_lat, double min_lng, double max_lng)
        {
            return stations.Where(x => x.Lat >= min_lat && x.Lat <= max_lat && x.Lng >= min_lng && x.Lng <= max_lng && x.Status == ESationStatus.Active).ToList();
        }
    }
}
