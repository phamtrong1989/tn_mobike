﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Repositories
{
    public class OpenLockRequestRepository : EntityBaseRepository<OpenLockRequest>, IOpenLockRequestRepository
    {
        public OpenLockRequestRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<bool> IsOpenLockHistory(string transactionCode, int accountId, string openLockTransaction)
        {
            return await _context.OpenLockHistorys.AnyAsync(x => x.Phone == openLockTransaction && x.TransactionCode == transactionCode && x.AccountId == accountId);
        }    
    }
}