using TN.Domain.Model;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure.Interfaces;
using TN.API.Infrastructure;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace TN.Infrastructure.Repositories
{
    public class DiscountCodeRepository : EntityBaseRepository<DiscountCode>, IDiscountCodeRepository
    {
        public DiscountCodeRepository(MobikeContext context) : base(context)
        {

        }

        public async Task<List<DiscountCode>> DiscountCodePublicByAccount(int accountId, int projectIdAdd, int customerGroupIdAdd)
        {
            var groupsMyAccount = await _context.ProjectAccounts.Where(x => x.AccountId == accountId)
                .Select(x=> new ProjectAccount { 
                    Id = x.Id, 
                    AccountId = x.AccountId, 
                    ProjectId = x.ProjectId,
                    CustomerGroupId = x.CustomerGroupId
                }).ToListAsync();

            var projectIds = groupsMyAccount.Select(x => x.ProjectId).ToList();

            var customerGroupIds = groupsMyAccount.Select(x => x.CustomerGroupId).ToList();

            if(projectIdAdd > 0)
            {
                projectIds.Add(projectIdAdd);
            }    

            if (customerGroupIdAdd > 0)
            {
                customerGroupIds.Add(customerGroupIdAdd);
            }    

            var data = await _context.DiscountCodes
                .Where(x =>
                x.Public
                && (x.ProjectId == 0 || x.ProjectId > 0 && projectIds.Contains(x.ProjectId))
                && (x.CustomerGroupId == 0 || x.CustomerGroupId > 0 && customerGroupIds.Contains(x.CustomerGroupId))
                && x.StartDate <= DateTime.Now
                && x.EndDate > DateTime.Now.AddDays(-1)
                && (x.BeneficiaryAccountId == null || x.BeneficiaryAccountId == 0 || (x.BeneficiaryAccountId > 0 && x.BeneficiaryAccountId == accountId)) 
                && !_context.Bookings.Any(m=> m.AccountId == accountId && m.Status == EBookingStatus.Start && m.DiscountCodeId == x.Id)
                && _context.Campaigns.Any(m => m.Id == x.CampaignId && m.Status == ECampaignStatus.Active && x.CurentLimit > 0)
                && !_context.WalletTransactions.Any(m => m.AccountId == accountId && m.CampaignId == x.CampaignId && x.CreatedDate >= x.StartDate.AddDays(-1) && x.CreatedDate <= x.EndDate.AddDays(1))
            ).OrderByDescending(x => x.Point).Take(200).ToListAsync();

            return data;
        }
    }
}

