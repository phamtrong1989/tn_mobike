using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure;

namespace TN.Infrastructure.Repositories
{
    public class CustomerGroupRepository : EntityBaseRepository<CustomerGroup>, ICustomerGroupRepository
    {
        public CustomerGroupRepository(MobikeContext context) : base(context)
        {
        }
    }
}

