﻿using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class EmailBoxRepository : EntityBaseRepository<EmailBox>, IEmailBoxRepository
    {
        public EmailBoxRepository(MobikeContext context) : base(context)
        {
        }
    }
}
