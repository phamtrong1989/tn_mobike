using TN.Domain.Model;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class RFIDBookingRepository : EntityBaseRepository<RFIDBooking>, IRFIDBookingRepository
    {
        public RFIDBookingRepository(MobikeContext context) : base(context)
        {
        }
    }
}

