﻿using Microsoft.EntityFrameworkCore;
using PT.Domain.Model;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Domain.Seedwork;
using TN.Utility;

namespace TN.API.Infrastructure.Repositories
{
    public class TransactionRepository : EntityBaseRepository<Transaction>, ITransactionRepository
    {

        public TransactionRepository(MobikeContext context) : base(context)
        {
        }

        public double TotalKMByAccount(int accountId, DateTime start, DateTime end)
        {
            var km = _context.Transactions.Where(x => x.AccountId == accountId && x.EndTime >= start && x.EndTime < end && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall) && x.GroupTransactionOrder == 1).Sum(x => x.KM);
            return Math.Round((km ?? 0) / 1000, 2);
        }    

        public async Task<TransactionDiscount> DiscountTransactionByDay(int accountId, DateTime time)
        {
            var kt = await _context.TransactionDiscounts.FirstOrDefaultAsync(x => x.AccountId == accountId && x.Date.Date == time.Date);
            if (kt != null)
            {
                return kt;
            }

            var cyclingWeek = await _context.CyclingWeeks.FirstOrDefaultAsync(x => x.AccountId == accountId && x.StartDate <= time && x.EndDate > time.AddDays(-1));
            if (cyclingWeek == null)
            {
                return null;
            }

            var totalPrice = await _context.Transactions
                    .Where(x => x.EndTime >= time.Date
                                && x.EndTime < time.AddDays(1).Date
                                && x.AccountId == accountId
                                && x.TotalPrice > 0
                                && (x.TicketPrice_TicketType == ETicketType.Block || x.TicketPrice_TicketType == ETicketType.Day)
                                && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall))
                    .SumAsync(x => (x.TotalPrice - x.VehicleRecallPrice));

            int discountValue = 0;
            var timeDayOfWeek = time.DayOfWeek;
            if (timeDayOfWeek == DayOfWeek.Sunday)
            {
                discountValue = cyclingWeek.SundayCycling ?? 0;
            }
            else if (timeDayOfWeek == DayOfWeek.Monday)
            {
                discountValue = cyclingWeek.MondayCycling ?? 0;
            }
            else if (timeDayOfWeek == DayOfWeek.Tuesday)
            {
                discountValue = cyclingWeek.TuesdayCycling ?? 0;
            }
            else if (timeDayOfWeek == DayOfWeek.Wednesday)
            {
                discountValue = cyclingWeek.WednesdayCycling ?? 0;
            }
            else if (timeDayOfWeek == DayOfWeek.Thursday)
            {
                discountValue = cyclingWeek.ThursdayCycling ?? 0;
            }
            else if (timeDayOfWeek == DayOfWeek.Friday)
            {
                discountValue = cyclingWeek.FridayCycling ?? 0;
            }
            else if (timeDayOfWeek == DayOfWeek.Saturday)
            {
                discountValue = cyclingWeek.SaturdayCycling ?? 0;
            }
            kt = new TransactionDiscount
            {
                AccountId = accountId,
                CyclingWeekId = cyclingWeek.Id,
                Date = time,
                Status = discountValue == 0,
                TotalPoint = totalPrice,
                CreatedDate = DateTime.Now,
                DiscountValue = discountValue,
                DiscountPoint = (totalPrice / 100) * discountValue
            };
            await _context.TransactionDiscounts.AddAsync(kt);
            await _context.SaveChangesAsync();
            return kt;
        }

        public string GetTicketPriceString(TicketPrice item, string format, string lang)
        {
            if (item.TicketType == ETicketType.Block)
            {
                return string.Format(format, item.TicketType.GetDisplayName(lang), FormatMoney(item.TicketValue), FormatMoney(item.BlockPerMinute), FormatMoney(item.BlockPerViolation), FormatMoney(item.BlockViolationValue));
            }
            else if (item.TicketType == ETicketType.Day)
            {
                return string.Format(format, item.TicketType.GetDisplayName(lang), FormatMoney(item.TicketValue), FormatMoney(item.LimitMinutes), FormatMoney(item.BlockPerViolation), FormatMoney(item.BlockViolationValue));
            }
            else if (item.TicketType == ETicketType.Month)
            {
                return string.Format(format, item.TicketType.GetDisplayName(lang), FormatMoney(item.TicketValue), FormatMoney(item.LimitMinutes), FormatMoney(item.BlockPerViolation), FormatMoney(item.BlockViolationValue));
            }
            return "";
        }

        private TotalPriceModel ToEndPrice(DiscountCode discountCode, decimal totalPrice)
        {
            decimal priceForDiscount = 0;
            if (discountCode != null && discountCode?.Id > 0)
            {
                if (discountCode.Type == EDiscountCodeType.Point)
                {
                    priceForDiscount = discountCode.Point;
                }
                else
                {
                    priceForDiscount = (totalPrice / 100) * discountCode.Percent;
                }

                if (priceForDiscount >= totalPrice)
                {
                    return new TotalPriceModel()
                    {
                        DiscountPrice = priceForDiscount,
                        TotalPrice = totalPrice,
                        Price = 0
                    };
                }    
                else
                {
                    return new TotalPriceModel()
                    {
                        DiscountPrice = priceForDiscount,
                        TotalPrice = totalPrice,
                        Price = totalPrice - priceForDiscount
                    };
                }    
            }
            return new TotalPriceModel()
            {
                TotalPrice = totalPrice,
                DiscountPrice = priceForDiscount,
                Price = totalPrice
            };
        }

        public TotalPriceModel ToTotalPrice(
            ETicketType ticketType,
            DateTime startTime,
            DateTime endTime,
            decimal ticketPrice_TicketValue,
            int ticketPrice_LimitMinutes,
            int ticketPrice_BlockPerMinute,
            int ticketPrice_BlockPerViolation,
            decimal ticketPrice_BlockViolationValue,
            DateTime prepaid_EndTime,
            int prepaid_MinutesSpent,
            DiscountCode discountCode
            )
        {
            var totalMinutes = Function.TotalMilutes(startTime, endTime);
            int soPhutPhat;
            if (ticketType == ETicketType.Block)
            {
                
                soPhutPhat = totalMinutes - ticketPrice_BlockPerMinute;
                if (soPhutPhat <= 0)
                {
                    return ToEndPrice(discountCode, ticketPrice_TicketValue);
                }
                var totalPrice =  ticketPrice_TicketValue + ((soPhutPhat % ticketPrice_BlockPerViolation > 0) ? ((soPhutPhat / ticketPrice_BlockPerViolation) + 1) : (soPhutPhat / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;
                return ToEndPrice(discountCode, totalPrice);
            }
            else if (ticketType == ETicketType.Day || ticketType == ETicketType.Month)
            {
                var soPhutConLai = ticketPrice_LimitMinutes - prepaid_MinutesSpent;

                if(soPhutConLai < 0)
                {
                    soPhutConLai = 0;
                }    

                // Vé trả trước trong ngày
                if (prepaid_EndTime > endTime)
                {
                    soPhutPhat = (totalMinutes < soPhutConLai) ? 0 : (totalMinutes - soPhutConLai);
                }
                // trường hợp đi hết hạn vé trả trước
                else
                {
                    // Tính theo end time là mốc hết hạn vé trả trước
                    // thời hạn vé trả trước - thời gian bắt đầu = số phút đi được theo hết mốc
                    // Tổng số phút còn lại > số thời gian theo hết mốc
                    double tongSoPhutHetMoc = (prepaid_EndTime - startTime).TotalMinutes;
                    if (soPhutConLai > tongSoPhutHetMoc)
                    {
                        soPhutPhat = Convert.ToInt32(Math.Ceiling(totalMinutes - tongSoPhutHetMoc));
                    }
                    else
                    {
                        soPhutPhat = totalMinutes - soPhutConLai;
                    }
                }

                if (soPhutPhat <= 0)
                {
                    return new TotalPriceModel();
                }
                var totalPrice = ((soPhutPhat % ticketPrice_BlockPerViolation > 0) ? ((soPhutPhat / ticketPrice_BlockPerViolation) + 1) : (soPhutPhat / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;
                return new TotalPriceModel()
                {
                    DiscountPrice= 0,
                    TotalPrice = totalPrice,
                    Price = totalPrice
                };
            }
            return new TotalPriceModel();
        }

        private string FormatMoney(decimal? number)
        {
            if (number == null)
            {
                return "";
            }
            if (number < 1000)
            {
                return number.ToString();
            }
            var culture = CultureInfo.GetCultureInfo("vi-VN");
            return string.Format(culture, "{0:00,0}", number);
        }

        public DashboardTransactionsModel DashboardTransactions(int accountId)
        {
            var baseQuery = _context.Transactions.Where(x => x.AccountId == accountId && x.Status == EBookingStatus.End).GroupBy(x => x.AccountId).Select(x => new
            {
                TotalKCal = x.Sum(m => m.KCal),
                TotalKG = x.Sum(m => m.KG),
                TotalKM = x.Sum(m => m.KM)
            });

            return new DashboardTransactionsModel
            {
                TotalKCal = Math.Round(baseQuery.Sum(x => x.TotalKCal) ?? 0, 1),
                TotalKG = Math.Round(baseQuery.Sum(x => x.TotalKG) ?? 0, 1),
                TotalKM = Math.Round(baseQuery.Sum(x => x.TotalKM) ?? 0, 1)
            };
        }
    }
}
