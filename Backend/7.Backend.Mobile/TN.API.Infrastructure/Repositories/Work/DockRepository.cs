﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using static TN.Domain.Model.Bike;

namespace TN.API.Infrastructure.Repositories
{
    public class DockRepository : EntityBaseRepository<Dock>, IDockRepository
    {
        public DockRepository(MobikeContext context) : base(context)
        {
        }

        public async Task DockStatusCheckAny(int dockId, int accountId)
        {
            var kt = await _context.DockStatus.AsNoTracking().AnyAsync(x => x.DockId == dockId);
            if(!kt)
            {
                await _context.DockStatus.AddAsync(new DockStatus() { AccountId = accountId, DockId = dockId, IsProcess = false, UpdatedTime = DateTime.Now });
                await _context.SaveChangesAsync();
            }
        }

        public async Task<bool> DockStatusCheckProcess(int dockId, int sAllow)
        {
            var kt = await _context.DockStatus.AsNoTracking().FirstOrDefaultAsync(x => x.DockId == dockId);
            if (kt == null)
            {
                return false;
            }
            else if (!kt.IsProcess)
            {
                return false;
            }
            else if (kt.UpdatedTime.AddSeconds(sAllow) >= DateTime.Now)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task DockStatusSetProcess(int dockId, bool isProcess)
        {
            try
            {
                var kt = await _context.DockStatus.FirstOrDefaultAsync(x => x.DockId == dockId);
                if (kt != null)
                {
                    kt.IsProcess = isProcess;
                    kt.UpdatedTime = DateTime.Now;
                    _context.DockStatus.Update(kt);
                    await _context.SaveChangesAsync();
                }
            }
            catch { }
        }

        public async Task SetStationId(int id, int stationId)
        {
            try
            {
                if(stationId <= 0)
                {
                    return;
                }    
                var kt = await _context.Docks.FirstOrDefaultAsync(x => x.Id == id);
                if (kt != null)
                {
                    kt.StationId = stationId;
                    _context.Docks.Update(kt);

                    var ktBike = await _context.Bikes.FirstOrDefaultAsync(x => x.DockId == id);
                    if (ktBike != null)
                    {
                        ktBike.StationId = stationId;
                        _context.Bikes.Update(ktBike);
                    }
                    await _context.SaveChangesAsync();
                }
            }
            catch
            {
            }
        }

       
        public async Task<List<Bike>> FindBayDocks(List<int> ids)
        {
            return await _context.Bikes.Where(x => ids.Contains(x.DockId) && x.Status == EBikeStatus.Active).ToListAsync();
        }

        public async Task<Bike> GetByDock(int dockId)
        {
            return await _context.Bikes.FirstOrDefaultAsync(x => x.DockId == dockId);
        }

        public async Task<Bike> BikeByDock(int dockId)
        {
            return await _context.Bikes.FirstOrDefaultAsync(x => x.DockId == dockId && x.Status == EBikeStatus.Active);
        }

        public async Task AddLog(int userId, int objectId, string action, LogType type)
        {
            await _context.Logs.AddAsync(new Log
            {
                Action = action,
                CreatedDate = DateTime.Now,
                Object = $"Docks",
                ObjectId = objectId,
                SystemUserId = userId,
                Type = type,
                ObjectType = $"Work.Docks.OpenLock"
            });
            await _context.SaveChangesAsync();
        }

        public async Task<Bike> BikeById(int id)
        {
            return await _context.Bikes.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Station>> BindTotalDockFree(List<Station> list, int projectId, int minBa)
        {
            var stationIds = list.Select(x => x.Id).ToList();
            var bikeFrees = await _context.Bikes
                .Where(x =>
                    (x.ProjectId == projectId || projectId == 0)
                    && x.DockId > 0
                    && x.Status == EBikeStatus.Active
                    && stationIds.Contains(x.StationId)
                    && !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)
                    && _context.Docks.Any(m => m.Id == x.DockId && m.ConnectionStatus == true && m.Battery >= minBa)
                 )
                .GroupBy(x => new { x.StationId })
                .Select(x => new
                {
                    x.Key.StationId,
                    DockCount = x.Count()
                }).ToListAsync();

            foreach (var item in list)
            {
                item.TotalDock = (bikeFrees.FirstOrDefault(x => x.StationId == item.Id)?.DockCount ?? 0);
            }
            return list;
        }

        public async Task<List<StationTotalDockModel>> GetTotalDockFree(List<Station> list, int projectId)
        {
            var stationIds = list.Select(x => x.Id).ToList();
            return await _context.Bikes
                .Where(x =>
                    (x.ProjectId == projectId || projectId == 0)
                    && x.DockId > 0
                    && x.Status == EBikeStatus.Active
                    && _context.Docks.Any(m => m.Id == x.DockId && m.ConnectionStatus == true)
                    && stationIds.Contains(x.StationId)
                    && !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)
                 )
                .GroupBy(x => new { x.StationId })
                .Select(x => new StationTotalDockModel
                {
                   StationId = x.Key.StationId,
                    TotalDock = x.Count()
                }).ToListAsync();
        }


        public async Task<List<Bike>> BikeByStation(int stationId)
        {
            return await _context.Bikes.Where(x => 
            x.StationId == stationId 
            && x.Status == EBikeStatus.Active 
            && x.DockId > 0 
            && _context.Docks.Any(m => m.Id == x.DockId && m.ConnectionStatus == true)
            && !_context.Bookings.Any(m=>m.BikeId == x.Id && m.Status == EBookingStatus.Start)).ToListAsync();
        }
    }
}
