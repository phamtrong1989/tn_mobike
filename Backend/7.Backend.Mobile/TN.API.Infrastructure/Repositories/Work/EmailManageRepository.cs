﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using TN.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class EmailManageRepository : EntityBaseRepository<EmailManage>, IEmailManageRepository
    {
        public EmailManageRepository(MobikeContext context) : base(context)
        {
        }
    }
}
