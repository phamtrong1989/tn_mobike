﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class AccountOTPRepository : EntityBaseRepository<AccountOTP>, IAccountOTPRepository
    {
        private readonly MobikeContext _mobikeContext;
        public AccountOTPRepository(MobikeContext context) : base(context)
        {
            _mobikeContext = context;
        }

        public  AccountOTP CheckConfigOTP(string phone, int otp, AccountOTP.OTPType type)
        {
            var data = _mobikeContext.AccountOTPs.Where(m => m.Phone == phone && m.OTP == otp && m.Type == type).OrderByDescending(m => m.Id).FirstOrDefault();
            if(data == null)
            {
                return null;
            }
            return (data.StartDate <= DateTime.Now && DateTime.Now <= data.EndDate) ? data : null;
        }

        public  AccountOTP CheckConfigOTP(string phone, AccountOTP.OTPType type)
        {
            var data = _mobikeContext.AccountOTPs.Where(m => m.Phone == phone && m.Type == type).OrderByDescending(m => m.Id).FirstOrDefault();
            if (data == null)
            {
                return null;
            }
            return (data.StartDate <= DateTime.Now && DateTime.Now <= data.EndDate) ? data : null;
        }

        public  async Task<AccountOTP> AddInvalid(int id)
        {
            var data = _mobikeContext.AccountOTPs.FirstOrDefault(x=>x.Id == id);
            if (data == null)
            {
                return null;
            }
            data.InvalidCount += 1;
            _mobikeContext.AccountOTPs.Update(data);
            await _mobikeContext.SaveChangesAsync();
            return data;
        }

        public virtual AccountOTP CheckConfigOTPOKE(string phone, int otp, AccountOTP.OTPType type)
        {
            return _mobikeContext.AccountOTPs.Where(m => m.Phone == phone && m.OTP == otp && m.Status == true && m.Type == type && m.StartDate <= DateTime.Now && DateTime.Now <= m.EndDate).OrderByDescending(m => m.Id).Take(1).ToList().LastOrDefault();
        }

        public async Task SetConfigOTPDisabled(string phone, int otp, AccountOTP.OTPType type)
        {
            try
            {
                var kt = await _mobikeContext.AccountOTPs.LastOrDefaultAsync(m => m.Phone == phone && m.OTP == otp && m.Status == true && m.Type == type && m.StartDate <= DateTime.Now && DateTime.Now <= m.EndDate);
                if(kt != null)
                {
                    kt.Status = false;
                    _mobikeContext.AccountOTPs.Update(kt);
                    await _mobikeContext.SaveChangesAsync();
                }    
            }
            catch
            {
                return;
            }
        }

        public virtual AccountOTP GetLastOTPByUsername(string Phone, AccountOTP.OTPType type)
        {
            return _mobikeContext.AccountOTPs.Where(m => m.Phone == Phone && m.Type == type).OrderByDescending(x=>x.Id).Take(1).ToList().FirstOrDefault();
        }
    }
}
