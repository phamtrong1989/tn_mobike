﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class BookingRepository : EntityBaseRepository<Booking>, IBookingRepository
    {
        public BookingRepository(MobikeContext context) : base(context)
        {
        }

        public async Task CancelForgotrReturnTheBike(int id)
        {
            try
            {
                var booking = await SearchOneAsync(false, x => x.Id == id);
                if (booking != null)
                {
                    booking.IsForgotrReturnTheBike = false;
                    _context.Bookings.Update(booking);
                    await _context.SaveChangesAsync();
                }
            }
            catch { }
        }

        public async Task<List<Booking>> GetListBooking(int accountid)
        {
            return await _context.Bookings.Where(x => x.AccountId ==accountid).ToListAsync();
        }

        public async Task<BookingFail> BookingFailAdd(BookingFail data)
        {
            await _context.BookingFails.AddAsync(data);
            await _context.SaveChangesAsync();
            return data;
        }

        public async Task FlagNotOpen(int id)
        {
            try
            {
                var booking = await SearchOneAsync(false, x => x.Id == id);
                if (booking != null)
                {
                    booking.IsDockNotOpen = true;
                    _context.Bookings.Update(booking);
                    await _context.SaveChangesAsync();
                }
            }
            catch {}
        }
    }
}
