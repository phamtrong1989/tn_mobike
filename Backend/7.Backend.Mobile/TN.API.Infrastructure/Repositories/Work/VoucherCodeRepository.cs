using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace TN.API.Infrastructure.Repositories
{
    public class VoucherCodeRepository : EntityBaseRepository<VoucherCode>, IVoucherCodeRepository
    {
        public VoucherCodeRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<List<VoucherCode>> VoucherCodePublicByAccount(int accountId, int projectIdAdd, int customerGroupIdAdd)
        {
            var groupsMyAccount = await _context.ProjectAccounts.Where(x => x.AccountId == accountId)
                .Select(x => new ProjectAccount
                {
                    Id = x.Id,
                    AccountId = x.AccountId,
                    ProjectId = x.ProjectId,
                    CustomerGroupId = x.CustomerGroupId
                }).ToListAsync();

            var projectIds = groupsMyAccount.Select(x => x.ProjectId).ToList();

            var customerGroupIds = groupsMyAccount.Select(x => x.CustomerGroupId).ToList();

            if (projectIdAdd > 0)
            {
                projectIds.Add(projectIdAdd);
            }

            if (customerGroupIdAdd > 0)
            {
                customerGroupIds.Add(customerGroupIdAdd);
            }

            var data = await _context.VoucherCodes
                .Where(x =>
               x.Public
                && (x.ProjectId == 0 || x.ProjectId > 0 && projectIds.Contains(x.ProjectId))
                && (x.CustomerGroupId == 0 || x.CustomerGroupId > 0 && customerGroupIds.Contains(x.CustomerGroupId))
                && x.StartDate <= DateTime.Now
                && x.EndDate > DateTime.Now.AddDays(-1)
                && (x.BeneficiaryAccountId == null || x.BeneficiaryAccountId == 0 || (x.BeneficiaryAccountId > 0 && x.BeneficiaryAccountId == accountId))
                && !_context.WalletTransactions.Any(m => m.AccountId == accountId && m.CampaignId == x.CampaignId && x.CreatedDate >= x.StartDate.AddDays(-1) && x.CreatedDate <= x.EndDate.AddDays(1))
                && _context.Campaigns.Any(m => m.Id == x.CampaignId && m.Status == ECampaignStatus.Active && x.CurentLimit > 0)
            ).OrderByDescending(x => x.Point).Take(200).ToListAsync();
            return data;
        }
    }
}

