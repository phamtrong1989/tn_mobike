using TN.Domain.Model;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure.Interfaces;
using TN.API.Infrastructure;

namespace TN.Infrastructure.Repositories
{
    public class AccountRatingRepository : EntityBaseRepository<AccountRating>, IAccountRatingRepository
    {
        public AccountRatingRepository(MobikeContext context) : base(context)
        {
        }
    }
}

