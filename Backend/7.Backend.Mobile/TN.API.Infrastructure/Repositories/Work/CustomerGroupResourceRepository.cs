using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class CustomerGroupResourceRepository : EntityBaseRepository<CustomerGroupResource>, ICustomerGroupResourceRepository
    {
        public CustomerGroupResourceRepository(MobikeContext context) : base(context)
        {
        }
    }
}

