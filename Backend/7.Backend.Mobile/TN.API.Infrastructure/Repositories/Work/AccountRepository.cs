﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class AccountRepository : EntityBaseRepository<Account>, IAccountRepository
    {
        private readonly MobikeContext _mobikeContext;
        public AccountRepository(MobikeContext context) : base(context)
        {
            _mobikeContext = context;
        }

        public async Task<ProjectAccount> ProjectAccountGet(int accountId, int projectId)
        {
            return await _context.ProjectAccounts.FirstOrDefaultAsync(x => x.AccountId == accountId && x.ProjectId == projectId);
        }

        public async Task<ApplicationUser> UserByRFID(string rfid)
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.RFID == rfid && x.IsLock == false && x.IsRetired == false);
        }

        public async Task<ApplicationUser> UserByCode(string code)
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.Code.ToLower() == code.Trim().ToLower() && (x.RoleType == RoleManagerType.CTV_VTS || x.RoleType == RoleManagerType.CSKH_VTS || x.RoleType == RoleManagerType.Coordinator_VTS));
        }

        public async Task<List<ApplicationUser>> UserByWarning()
        {
            return await _context.Users.Where(x => (x.RoleType == RoleManagerType.CSKH_VTS || x.RoleType == RoleManagerType.Coordinator_VTS) && x.IsLock == false && x.IsRetired == false).ToListAsync();
        }

        public async Task<Flag> FlagGet(string transaction)
        {
            return await _context.Flags.FirstOrDefaultAsync(x => x.Transaction == transaction);
        }

        public async Task FlagUpdate(string transaction)
        {
            var kt = await _context.Flags.FirstOrDefaultAsync(x => x.Transaction == transaction);
            if(kt == null)
            {
                await _context.Flags.AddAsync(new Flag() { Transaction = transaction, UpdatedTime = DateTime.Now });
            }
            else
            {
                kt.UpdatedTime = DateTime.Now;
                _context.Flags.Update(kt);
            }
            await _context.SaveChangesAsync();
        }

        public async Task<bool> CheckLocation(int accountId, int stationId)
        {
            return await _mobikeContext.AccountLocations.AnyAsync(x => x.AcountId == accountId && x.NextTime > DateTime.Now );
        }

        public async Task<AccountLocation> GetCurrentLocation(int accountId)
        {
            return await _mobikeContext.AccountLocations.Where(x => x.AcountId == accountId).FirstOrDefaultAsync();
        }

        public async Task SetLocation(int accountId, int stationId, int addMinutes, double lat, double lng)
        {
            var kt =  await _mobikeContext.AccountLocations.FirstOrDefaultAsync(x => x.AcountId == accountId);
            if(kt == null)
            {
               await  _mobikeContext.AccountLocations.AddAsync(new AccountLocation {
                   AcountId = accountId,
                   DeviceId = "",
                   Lat = lat,
                   Lng = lng,
                   UpdatedTime = DateTime.Now,
                   NextTime = DateTime.Now.AddMinutes(addMinutes),
                   StationId = stationId
               });
            }
            else
            {
                if(stationId > 0)
                {
                    kt.StationId = stationId;
                }    
                kt.Lat = lat;
                kt.Lng = lng;
                kt.UpdatedTime = DateTime.Now;
                if(addMinutes > 0)
                {
                    kt.NextTime = DateTime.Now.AddMinutes(addMinutes);
                }    
                _mobikeContext.AccountLocations.Update(kt);
            }    
            await _mobikeContext.SaveChangesAsync();
        }

        public async Task<List<AccountDevice>> FilterDevices(int accountId)
        {
            return await _mobikeContext.AccountDevices.Where(x => x.AcountId == accountId).ToListAsync();
        }

        public async Task UpdateDeviceToken(int accountId, string deviceId, string token)
        {
            var device = await _mobikeContext.AccountDevices.FirstOrDefaultAsync(x => x.AcountId == accountId && x.DeviceId == deviceId);
            if (device != null)
            {
                device.CloudMessagingToken = token;
                _mobikeContext.AccountDevices.Update(device);
                await _mobikeContext.SaveChangesAsync();
            }
        }

        public async Task UpdateLanguage(int accountId, string language, int languageId)
        {
            var account = await _mobikeContext.Accounts.FirstOrDefaultAsync(x => x.Id == accountId);
            if (account != null)
            {
                account.Language = language;
                account.LanguageId = languageId;
                _mobikeContext.Accounts.Update(account);
                await _mobikeContext.SaveChangesAsync();
            }
        }

        public async Task<AccountDevice> AddDevice(int accountId, string deviceId, string token, DateTime start, DateTime end, string ip)
        {
            try
            {
                var device = await _mobikeContext.AccountDevices.FirstOrDefaultAsync(x => x.AcountId == accountId && x.DeviceId == deviceId);
                if (device == null)
                {
                    device = new AccountDevice
                    {
                        AcountId = accountId,
                        CreatedDate = DateTime.Now,
                        DeviceId = deviceId,
                        Token = token,
                        StartDate = start,
                        EndDate = end,
                        CloudMessagingToken = null,
                        IP = ip
                    };
                    await _mobikeContext.AddAsync(device);
                    await _mobikeContext.SaveChangesAsync();
                }
                device.CreatedDate = DateTime.Now;
                device.Token = token;
                device.StartDate = start;
                device.EndDate = end;
                device.IP = ip;
                _mobikeContext.Update(device);
                await _mobikeContext.SaveChangesAsync();
                return device;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Account> Login(string username, string password)
        {
            return await SearchOneAsync(true, x => (x.Email == username || x.Phone == username) && x.Password == password);
        }

        public async Task<Account> FindByEtag(string etag)
        {
            return await SearchOneAsync(true, x => x.Etag == etag);
        }

        public virtual Account GetByUserName(string username)
        {
            var dateNow = DateTime.Now;
            var qr = _mobikeContext.Accounts.OrderByDescending(m => m.Id).Where(m => m.Phone == username).Skip(0).Take(1).AsQueryable();
            var data = qr.FirstOrDefault();
            if (data == null)
            {
                return null;
            }
            return data;
        }

        public async Task ProjectAcountAdd(int accountId, int projectId, int customerGroupId)
        {
            await _mobikeContext.ProjectAccounts.AddAsync(new ProjectAccount
            {
                ProjectId = projectId,
                CustomerGroupId = customerGroupId,
                AccountId = accountId,
                CreatedDate = DateTime.Now,
                CreatedUser = 0,
                InvestorId = 0,
                SourceType = EProjectAccountSourceType.Auto
            });
            await _mobikeContext.SaveChangesAsync();
        }
    }
}
