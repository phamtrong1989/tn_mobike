﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Repositories
{
    public class PostRepository : EntityBaseRepository<Post>, IPostRepository
    {
        public PostRepository(MobikeContext context) : base(context)
        {
        }

        public async Task PostLikeAdd(int postId, int accountId, bool isLike)
        {
            var kt = await _context.PostLikes.FirstOrDefaultAsync(x => x.PostId == postId && x.AccountId == accountId);
            if (kt == null && !isLike)
            {
                return;
            }
            else if (kt == null && isLike)
            {
                await _context.PostLikes.AddAsync(new PostLike { AccountId = accountId, CreatedDate = DateTime.Now, PostId = postId });
                var ktPost = await _context.Posts.FirstOrDefaultAsync(x => x.Id == postId);
                if(ktPost != null)
                {
                    ktPost.LikeCount += 1;
                    await _context.SaveChangesAsync();
                }    
            }
            else if (kt != null && isLike)
            {
                return;
            }
            else
            {
                _context.PostLikes.Remove(kt);
                var ktPost = await _context.Posts.FirstOrDefaultAsync(x => x.Id == postId);
                if (ktPost != null)
                {
                    ktPost.LikeCount -= 1;
                    if(ktPost.LikeCount < 0)
                    {
                        ktPost.LikeCount = 0;
                    }    
                    await _context.SaveChangesAsync();
                }
            }
        }

        public async Task<BaseSearchModel<List<Post>>> SearchPagedList(int page, int limit, int? accountId, int? accountRatingId, string key, EPostStatus? status, int myAccountId)
        {
            var query = _context.Posts.AsQueryable();

            if(accountId != null && accountId > 0)
            {
                query = query.Where(x => x.AccountId == accountId).AsQueryable();
            }

            if (status != null)
            {
                query = query.Where(x => x.Status == status).AsQueryable();
            }

            if (accountRatingId != null && accountRatingId > 0)
            {
                query = query.Where(x => x.AccountRatingId == accountRatingId).AsQueryable();
            }

            if(!string.IsNullOrEmpty(key))
            {
                query = query.Where(x => _context.Accounts.Any(m=>m.Id == x.AccountId && (m.Phone == key || m.FullName == key))).AsQueryable();
            }

            query = query.Where(x=>x.CreatedDate >= DateTime.Now.AddDays(-365)).OrderByDescending(x => x.CreatedDate);
            var data = await query.Skip((page - 1) * limit).Take(limit).AsNoTracking().ToListAsync();
            var total = await query.CountAsync();
            var accountIds = data.Select(x => x.AccountId).Distinct().ToList();
            var ids = data.Select(x => x.Id).Distinct().ToList();
            var accounts = await _context.Accounts.Where(x => accountIds.Contains(x.Id)).Select(x => new Account { Id = x.Id, FullName = x.FullName, Phone = x.Phone }).ToListAsync();
            var currentAccountLike = await _context.PostLikes.Where(x => x.AccountId == myAccountId && ids.Contains(x.PostId)).ToListAsync();
            var accountRatingIds = data.Where(x => x.AccountRatingId > 0).Select(x => x.AccountRatingId).Distinct().ToList();
            var listaccountRating = new List<AccountRating>();
            if(accountRatingIds.Any())
            {
                listaccountRating = await _context.AccountRatings.Where(x => x.Group == EAccountRatingGroup.Image && accountRatingIds.Contains(x.Id)).ToListAsync();
            }    
            foreach (var item in data)
            {
                item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                item.IsLike = currentAccountLike.Any(x => x.PostId == item.Id);
                item.AccountRatingName = listaccountRating.FirstOrDefault(x => x.Id == item.AccountRatingId)?.Name;
            }    
            return new BaseSearchModel<List<Post>>
            {
                Data = data,
                Limit = limit,
                Page = page,
                ReturnUrl = "",
                TotalRows = total
            };
        }
    }
}
