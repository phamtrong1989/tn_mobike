using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure;

namespace TN.Infrastructure.Repositories
{
    public class ContentPageRepository : EntityBaseRepository<ContentPage>, IContentPageRepository
    {
        public ContentPageRepository(MobikeContext context) : base(context)
        {
        }
    }
}

