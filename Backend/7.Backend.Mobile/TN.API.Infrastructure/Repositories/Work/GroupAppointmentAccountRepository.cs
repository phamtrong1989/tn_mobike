﻿using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class GroupAppointmentAccountRepository : EntityBaseRepository<GroupAppointmentAccount>, IGroupAppointmentAccountRepository
    {
        public GroupAppointmentAccountRepository(MobikeContext context) : base(context)
        {
        }
    }
}
