﻿using System;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class EventSystemReportRepository : EntityBaseRepository<Account>, IEventSystemReportRepository
    {
        public EventSystemReportRepository(MobikeContext context) : base(context)
        {
           
        }

        public async Task<Flag> FlagLastCloseBikeAdd(string transactionCode, int dockId, int accountId)
        {
            string tran = $"ECloseBike_{transactionCode}_{dockId}_{accountId}";
            var dlAdd = await _context.Flags.FirstOrDefaultAsync(x => x.Transaction == tran);
            if(dlAdd == null)
            {
                dlAdd = new Flag
                {
                    IsProcess = false,
                    Type = 0,
                    Transaction = tran,
                    UpdatedTime = DateTime.Now
                };
                await _context.Flags.AddAsync(dlAdd);
            }
            else
            {
                dlAdd.UpdatedTime = DateTime.Now;
                _context.Flags.Update(dlAdd);
            }
            await _context.SaveChangesAsync();
            return dlAdd;
        }
    }
}
