﻿using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;
using System.Linq;
using System.Collections.Generic;

namespace TN.API.Infrastructure.Repositories
{
    public class GroupAppointmentRepository : EntityBaseRepository<GroupAppointment>, IGroupAppointmentRepository
    {
        public GroupAppointmentRepository(MobikeContext context) : base(context)
        {
        }

        public bool CheckInAnyRoom(int accountId)
        {
            return _context.GroupAppointmentItems.Any(x=> 
                x.MemberId == accountId 
                && x.IsConfirm  && _context.GroupAppointments.Any(m=> m.Status == EGroupAppointment.Default && m.Id == x.GroupAppointmentId && m.CreatedDate.Date == System.DateTime.Now.Date));
        }

        public int SendJoinRoomCount(int accountId)
        {
            return _context.GroupAppointments.Count(x => 
                x.CreatedDate.Date == System.DateTime.Now.Date 
                && x.Status == EGroupAppointment.Default 
                && _context.GroupAppointmentItems.Any(m=>m.GroupAppointmentId == x.Id && m.MemberId == accountId && m.IsConfirm == false));
        }

        public GroupAppointment MyGroup(int accountId)
        {
            return _context.GroupAppointments.FirstOrDefault(x =>
                x.CreatedDate.Date == System.DateTime.Now.Date
                && x.Status == EGroupAppointment.Default
                && (x.LeaderId == accountId || _context.GroupAppointmentItems.Any(m => m.GroupAppointmentId == x.Id && m.MemberId == accountId)));
        }

        public List<GroupAppointment> RequestList(int accountId)
        {
            return _context.GroupAppointments.Where(x =>
               x.CreatedDate.Date == System.DateTime.Now.Date
               && x.Status == EGroupAppointment.Default
               && _context.GroupAppointmentItems.Any(m => m.GroupAppointmentId == x.Id && m.MemberId == accountId)).ToList();
        }
    }
}
