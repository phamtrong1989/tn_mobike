﻿using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class BannerGroupRepository : EntityBaseRepository<BannerGroup>, IBannerGroupRepository
    {
        public BannerGroupRepository(MobikeContext context) : base(context)
        {
        }
    }
}