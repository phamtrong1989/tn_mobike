using TN.Domain.Model;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure.Interfaces;
using TN.API.Infrastructure;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace TN.Infrastructure.Repositories
{
    public class AccountRatingItemRepository : EntityBaseRepository<AccountRatingItem>, IAccountRatingItemRepository
    {
        public AccountRatingItemRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<AccountRating> AccountRatingGetByCodeAsync(string code)
        {
            return await _context.AccountRatings.FirstOrDefaultAsync(x => x.Code == code);
        }
    }
}

