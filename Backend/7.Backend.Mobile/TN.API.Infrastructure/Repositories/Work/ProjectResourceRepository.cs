using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class ProjectResourceRepository : EntityBaseRepository<ProjectResource>, IProjectResourceRepository
    {
        public ProjectResourceRepository(MobikeContext context) : base(context)
        {
        }
    }
}

