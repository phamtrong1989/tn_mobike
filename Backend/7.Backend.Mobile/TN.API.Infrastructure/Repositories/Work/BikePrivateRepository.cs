using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure;

namespace TN.Infrastructure.Repositories
{
    public class BikePrivateRepository : EntityBaseRepository<BikePrivate>, IBikePrivateRepository
    {
        public BikePrivateRepository(MobikeContext context) : base(context)
        {
        }
    }
}

