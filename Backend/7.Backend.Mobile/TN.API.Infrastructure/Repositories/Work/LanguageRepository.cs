﻿using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class LanguageRepository : EntityBaseRepository<Language>, ILanguageRepository
    {
        public LanguageRepository(MobikeContext context) : base(context)
        {
        }
    }
}
