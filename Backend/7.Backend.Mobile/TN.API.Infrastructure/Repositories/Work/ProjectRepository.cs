using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.API.Infrastructure.Repositories;
using TN.API.Infrastructure;

namespace TN.Infrastructure.Repositories
{
    public class ProjectRepository : EntityBaseRepository<Project>, IProjectRepository
    {
        public ProjectRepository(MobikeContext context) : base(context)
        {
        }
    }
}

