using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class StationResourceRepository : EntityBaseRepository<StationResource>, IStationResourceRepository
    {
        public StationResourceRepository(MobikeContext context) : base(context)
        {
        }
    }
}

