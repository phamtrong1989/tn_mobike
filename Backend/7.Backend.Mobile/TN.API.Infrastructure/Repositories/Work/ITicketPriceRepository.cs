﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class TicketPriceRepository : EntityBaseRepository<TicketPrice>, ITicketPriceRepository
    {
        public TicketPriceRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<TicketPrepaid> GetTicketPrepaidByAccount(int accountId, int ticketPrepaid, int projectId)
        {
            return await _context.TicketPrepaids.FirstOrDefaultAsync(x => x.Id == ticketPrepaid && x.AccountId == accountId && x.StartDate <= DateTime.Now && x.EndDate.AddDays(1) > DateTime.Now && x.ProjectId == projectId);
        }

        public async Task<List<TicketPrepaid>> GetsTicketPrepaidByAccount(int accountId, int projectId)
        {
            return await _context.TicketPrepaids.Where(x => x.AccountId == accountId && x.StartDate <= DateTime.Now && x.EndDate.AddDays(1) > DateTime.Now && x.ProjectId == projectId).ToListAsync();
        }

        public async Task<TicketPrepaid> PrepaidUpdatePriceTypeDay(int accountId, int ticketPrepaidId, int addM)
        {
            var kt = await _context.TicketPrepaids.SingleOrDefaultAsync(x => x.AccountId == accountId && x.Id == ticketPrepaidId && (x.TicketPrice_TicketType == ETicketType.Day || x.TicketPrice_TicketType == ETicketType.Month));
            if(kt != null)
            {
                if((kt.MinutesSpent + addM) >  kt.TicketPrice_LimitMinutes)
                {
                    kt.MinutesSpent = kt.TicketPrice_LimitMinutes;
                }    
                else
                {
                    kt.MinutesSpent += addM;
                }    
                _context.TicketPrepaids.Update(kt);
                await _context.SaveChangesAsync();

                var bookings = await _context.Bookings.Where(x => x.AccountId == accountId && x.Status == EBookingStatus.Start && x.TicketPrepaidId == kt.Id).ToListAsync();
                if (bookings.Any())
                {
                    foreach (var item in bookings)
                    {
                        var ktBooking = await _context.Bookings.FirstOrDefaultAsync(x => x.Id == item.Id && x.Status == EBookingStatus.Start);
                        if (ktBooking != null)
                        {
                            ktBooking.Prepaid_MinutesSpent = kt.MinutesSpent;
                            _context.Bookings.Update(ktBooking);
                        }
                    }
                    await _context.SaveChangesAsync();
                }
                return kt;
            }
            return null;
        }
    }
}
