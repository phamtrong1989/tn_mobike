﻿using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class GroupAppointmentItemRepository : EntityBaseRepository<GroupAppointmentItem>, IGroupAppointmentItemRepository
    {
        public GroupAppointmentItemRepository(MobikeContext context) : base(context)
        {
        }
    }
}
