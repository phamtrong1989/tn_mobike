﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class SystemNotificationRepository : EntityBaseRepository<SystemNotification>, ISystemNotificationRepository
    {
        public SystemNotificationRepository(MobikeContext context) : base(context)
        {

        }
    }
}
