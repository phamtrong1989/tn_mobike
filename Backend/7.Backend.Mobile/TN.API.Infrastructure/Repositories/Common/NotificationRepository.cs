﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using System.Net;
using System.IO;
using FirebaseAdmin.Messaging;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using FirebaseAdmin.Auth;

namespace TN.API.Infrastructure.Repositories
{
    public class NotificationRepository : EntityBaseRepository<NotificationData>, INotificationRepository
    {
        public NotificationRepository(MobikeContext context) : base(context)
        {
        }
    }
}
