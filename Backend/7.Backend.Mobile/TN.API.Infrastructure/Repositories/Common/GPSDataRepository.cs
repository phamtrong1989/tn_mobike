﻿using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
namespace TN.Infrastructure.Repositories
{

    public class MongoDBRepository : IMongoDBRepository
    {
        private async Task MongoAddAsync(LogSettings settings, MongoLog data)
        {
            try
            {
                string dbName = settings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<MongoLog>(settings.MongoCollectionLog);
                await collection.InsertOneAsync(data);
            }
            catch{}
        }

        public async Task EventSystemAddAsync(LogSettings settings, EventSystemMongo data)
        {
            try
            {
                string dbName = settings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<EventSystemMongo>("EventSystem");
                await collection.InsertOneAsync(data);
            }
            catch { }
        }

        public List<GPSData> GPSGetByIMEI(LogSettings settings, string imei, DateTime startTime, DateTime endTime)
        {
            try
            {
                string dbName = settings.MongoDataBase.Replace("{TimeDB}", $"{startTime:yyyyMM}");
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                string query = "{ IMEI : \""+ imei + "\", Date : { $gte: " + startTime.ToString("yyyyMMdd") + ", $lte: " + endTime.ToString("yyyyMMdd") + " }, CreateTimeTicks: { $gte: " + startTime.Ticks.ToString() + ", $lte: " + endTime.Ticks.ToString() + " } }";
                var _context = db.GetCollection<GPSData>("GPS").Find(query).ToList();
                _context = _context.Where(x => x.Lat > 0 && x.Long > 0).OrderBy(x => x.CreateTimeTicks).ToList();
                return _context;
            }
            catch
            {
                return new List<GPSData>();
            }
        }

        public async void GPSAddRun(LogSettings settings, string imei, double lat, double lng)
        {
            try
            {
               await GPSAddAsync(settings, imei, lat, lng);
            }
            catch { }
        }

        private async Task GPSAddAsync(LogSettings settings, string imei, double lat, double lng)
        {
            try
            {
                string dbName = settings.MongoDataBase.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var datas = db.GetCollection<GPSData>(settings.MongoCollection);
                await datas.InsertOneAsync(new GPSData { 
                    CreateTimeTicks = DateTime.Now.Ticks, 
                    CreateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffffff"),
                    IMEI = imei,
                    Lat = lat,
                    Long = lng
                });
            }
            catch {}
        }

        public async void AddLogRun(LogSettings logSettings, int accountId, string functionName, string logKey, EnumMongoLogType type, string content, string deviceKey, bool isException = false)
        {
            try
            {
                var date = DateTime.Now;
                await MongoAddAsync(logSettings, new MongoLog
                {
                    AccountId = accountId,
                    Content = content,
                    CreateTime = date.ToString("yyyy-MM-dd HH:mm:ss:fff"),
                    CreateTimeTicks = date.Ticks,
                    FunctionName = functionName,
                    LogKey = logKey,
                    Type = type,
                    DeviceKey = deviceKey,
                    SystemType = EnumSystemType.APIAPP,
                    IsExeption = isException,
                    Date = Convert.ToInt64($"{date:yyyyMMdd}")
                });
            }
            catch{}
        }

        public TransactionGPS GetTransactionGPS(LogSettings settings, int transactionId, DateTime endTime)
        {
            try
            {
                string dbName = "TNGO-Data";
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                string query = "{ Date : " + endTime.ToString("yyyyMMdd") + ", TransactionId: " + transactionId + " }";
                var list = db.GetCollection<TransactionGPS>("TransactionGPS").Find(query).Limit(10).ToList();
                return list.LastOrDefault();
            }
            catch
            {
                return null;
            }
        }
    }
}