﻿using TN.Domain.Model;
using TN.API.Infrastructure.Interfaces;
using TN.Domain.Model.Common;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace TN.API.Infrastructure.Repositories
{
    public class SMSRepository : EntityBaseRepository<Account>, ISMSRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        public SMSRepository(MobikeContext context, IHttpClientFactory clientFactory) : base(context)
        {
            _clientFactory = clientFactory;
        }

        public async Task<ZNSSendMsg> ZALOSendOTP(ZaloSettings settings, AccountOTP.OTPType otpType , string toPhone, string otp)
        {
            //check xem số điện thoại đã có đầu 84 chưa, nếu có chiều dài sdt là 11
            if (toPhone.IndexOf("0") == 0 && toPhone.Length == 10)
            {
                toPhone = "84" + toPhone[1..];
            }

            var objectSend = new ZNSMessage
            {
                Mode = settings.ZaloSMSMode,
                Phone = toPhone,
                TemplateId = settings.ZaloSMSOTPRegisterTemplateId,
                TrackingId = $"TOGO-{DateTime.Now:yyyyMMddHHmmssfffff}"
            };

            if(otpType == AccountOTP.OTPType.Register)
            {
                objectSend.TemplateId = settings.ZaloSMSOTPRegisterTemplateId;
                objectSend.TemplateData = new ZNSTemplatOTPData { OTP = otp };
            }
            else if (otpType == AccountOTP.OTPType.RecoveryPassword)
            {
                objectSend.TemplateId = settings.ZaloSMSOTPRegisterTemplateId;
                objectSend.TemplateData = new ZNSTemplatOTPData { OTP = otp };
            }
            var todoItemJson = new StringContent(JsonConvert.SerializeObject(objectSend), Encoding.UTF8, "application/json");

            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Add("access_token", settings.ZaloSMSToken);

            using var httpResponse = await client.PostAsync(settings.ZaloSMSSendPath, todoItemJson);
    
            if (httpResponse.IsSuccessStatusCode)
            {
                var rsString = await httpResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ZNSSendMsg>(rsString);
            }
            else
            {
                return null;
            }
        }

        public async Task<ZNSSendMsg> ZALOSendWarning(ZaloSettings settings, string toPhone, string type, string plate, DateTime date, string message)
        {
            //check xem số điện thoại đã có đầu 84 chưa, nếu có chiều dài sdt là 11
            if (toPhone.IndexOf("0") == 0 && toPhone.Length == 10)
            {
                toPhone = "84" + toPhone[1..];
            }

            var objectSend = new ZNSMessage
            {
                Mode = settings.ZaloSMSMode,
                Phone = toPhone,
                TemplateId = settings.ZaloSMSSendWarningTemplateId,
                TrackingId = $"TOGO-{DateTime.Now:yyyyMMddHHmmssfffff}",
                TemplateData = new ZNSTemplatWarningData { Type = type, Plate = plate, Date = $"{date:HH:mm:ss dd/MM/yyyy}", Message = message }
            };
          
            var todoItemJson = new StringContent(JsonConvert.SerializeObject(objectSend), Encoding.UTF8, "application/json");

            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Add("access_token", settings.ZaloSMSToken);

            using var httpResponse = await client.PostAsync(settings.ZaloSMSSendPath, todoItemJson);

            if (httpResponse.IsSuccessStatusCode)
            {
                var rsString = await httpResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ZNSSendMsg>(rsString);
            }
            else
            {
                return null;
            }
        }

        public async Task<SMSResultV2Model> Send(string sms_path, string sms_token, string sms_username, string sms_password, string sms_brandname, string message, string phone)
        {
            //check xem số điện thoại đã có đầu 84 chưa, nếu có chiều dài sdt là 11
            if (phone.IndexOf("0") == 0 && phone.Length == 10)
            {
                phone = "84" + phone[1..];
            }

            var sms = new SMSV2Model
            {
                from = sms_brandname,
                text = message,
                to = phone
            };
            var confClient = _clientFactory.CreateClient();
            confClient.DefaultRequestHeaders.Accept.Clear();
            confClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            confClient.DefaultRequestHeaders.Add("Authorization", sms_token);

            HttpContent content = new StringContent(JsonConvert.SerializeObject(sms), Encoding.UTF8, "application/json");
            using var result = await confClient.PostAsync(sms_path, content);
            if (result.IsSuccessStatusCode)
            {
                string resultContent = await result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject <SMSResultV2Model>(resultContent);
            }    
            else
            {
                return null;
            }    
        }

        public async Task<SMSResultModel> Send2(string sms_path, string sms_token, string sms_username, string sms_password, string sms_brandname, string message, string phone)
        {
            //check xem số điện thoại đã có đầu 84 chưa, nếu có chiều dài sdt là 11
            if (phone.IndexOf("0") == 0 && phone.Length == 10)
            {
                phone = "84" + phone[1..];
            }

            var sms = new SMSModel
            {
                username = sms_username,
                password = sms_password,
                brandname = sms_brandname,
                phone = phone,
                message = message,
                type = 2,
                @ref = "TNGO"
            };
            var confClient = _clientFactory.CreateClient();
            confClient.DefaultRequestHeaders.Accept.Clear();
            confClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            confClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sms_token);
            HttpContent content = new StringContent(JsonConvert.SerializeObject(sms), Encoding.UTF8, "application/json");
            using var result = await confClient.PostAsync(sms_path, content);
            if (result.IsSuccessStatusCode)
            {
                string resultContent = await result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<SMSResultModel>(resultContent);
            }
            else
            {
                return null;
            }
        }
    }
}
