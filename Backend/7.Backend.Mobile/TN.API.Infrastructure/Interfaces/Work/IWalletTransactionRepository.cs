﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IWalletTransactionRepository : IEntityBaseRepository<WalletTransaction>
    {
        Task<DepositEvent> EventByAccount(int accountId, decimal amount, int addProjectId, int addCustomerGroupId);
        Task UpdateUsed(int id);
    }
}
