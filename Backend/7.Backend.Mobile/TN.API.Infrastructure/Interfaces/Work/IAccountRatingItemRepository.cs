using TN.Domain.Model;
using TN.Domain.Interfaces;
using System.Threading.Tasks;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IAccountRatingItemRepository : IEntityBaseRepository<AccountRatingItem>
    {
        Task<AccountRating> AccountRatingGetByCodeAsync(string code);
    }
}

