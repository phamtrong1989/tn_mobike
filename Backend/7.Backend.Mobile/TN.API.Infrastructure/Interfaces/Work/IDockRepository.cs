﻿using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Interfaces;
using System.Collections.Generic;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IDockRepository : IEntityBaseRepository<Dock>
    {
        Task<List<Bike>> FindBayDocks(List<int> ids);
        Task<Bike> BikeByDock(int dockId);
        Task<List<Station>> BindTotalDockFree(List<Station> list, int projectId, int minBas);
        Task SetStationId(int id, int stationId);
        Task<List<Bike>> BikeByStation(int stationId);
        Task<Bike> GetByDock(int dockId);
        Task DockStatusCheckAny(int dockId, int accountId);
        Task DockStatusSetProcess(int dockId, bool isProcess);
        Task<bool> DockStatusCheckProcess(int dockId, int sAllow);
        Task<Bike> BikeById(int id);
        Task AddLog(int userId, int objectId, string action, LogType type);
        Task<List<StationTotalDockModel>> GetTotalDockFree(List<Station> list, int projectId);
    }
}
