using TN.Domain.Model;
using TN.Domain.Interfaces;

namespace TN.Infrastructure.Interfaces
{
    public interface IProjectRepository : IEntityBaseRepository<Project>
    {
    }
}

