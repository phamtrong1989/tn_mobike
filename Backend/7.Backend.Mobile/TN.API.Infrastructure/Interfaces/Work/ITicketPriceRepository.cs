using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface ITicketPriceRepository : IEntityBaseRepository<TicketPrice>
    {
        Task<TicketPrepaid> GetTicketPrepaidByAccount(int accountId, int ticketPrepaid, int projectId);
        Task<TicketPrepaid> PrepaidUpdatePriceTypeDay(int accountId, int ticketPrepaidId, int addM);
        Task<List<TicketPrepaid>> GetsTicketPrepaidByAccount(int accountId, int projectId);
    }
}

