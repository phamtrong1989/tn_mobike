using TN.Domain.Model;
using TN.Domain.Interfaces;
using System;
using System.Threading.Tasks;

namespace TN.API.Infrastructure.Interfaces
{
    public interface ICyclingWeekRepository : IEntityBaseRepository<CyclingWeek>
    {
        Task<CyclingWeek> UpdateFlag(int accountId, DateTime checkOutTime);
    }
}

