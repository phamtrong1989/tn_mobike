using TN.Domain.Model;
using TN.Domain.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IVoucherCodeRepository : IEntityBaseRepository<VoucherCode>
    {
        Task<List<VoucherCode>> VoucherCodePublicByAccount(int accountId, int projectIdAdd, int customerGroupIdAdd);
    }
}

