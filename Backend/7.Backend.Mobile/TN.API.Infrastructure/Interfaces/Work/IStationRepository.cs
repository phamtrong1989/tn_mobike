﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IStationRepository : IEntityBaseRepository<Station>
    {
        Task<List<Project>> ProjectByIds(List<int> ids);
        Task<List<Station>> FindByLatLng(double min_lat, double max_lat, double min_lng, double max_lng);
        Task<AppVersion> CurrentVersion();
        Task<bool> FindByLatLngAny(double min_lat, double max_lat, double min_lng, double max_lng);
        bool FindByLatLngAnyByList(List<Station> stations, double min_lat, double max_lat, double min_lng, double max_lng);
        List<Station> FindByLatLngByList(List<Station> stations, double min_lat, double max_lat, double min_lng, double max_lng);
    }
}
