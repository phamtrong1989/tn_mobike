﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IPostRepository : IEntityBaseRepository<Post>
    {
        Task<BaseSearchModel<List<Post>>> SearchPagedList(int page, int limit, int? accountId, int? accountRatingId, string key, EPostStatus? status, int myAccountId);
        Task PostLikeAdd(int postId, int accountId, bool isLike);
    }
}
