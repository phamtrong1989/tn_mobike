﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IOpenLockRequestRepository : IEntityBaseRepository<OpenLockRequest>
    {
        Task<bool> IsOpenLockHistory(string transactionCode, int accountId, string openLockTransaction);
    }

}
