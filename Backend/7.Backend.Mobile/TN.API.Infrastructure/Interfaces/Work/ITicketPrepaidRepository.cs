using TN.Domain.Model;
using TN.Domain.Interfaces;

namespace TN.API.Infrastructure.Interfaces
{
    public interface ITicketPrepaidRepository : IEntityBaseRepository<TicketPrepaid>
    {
    }
}

