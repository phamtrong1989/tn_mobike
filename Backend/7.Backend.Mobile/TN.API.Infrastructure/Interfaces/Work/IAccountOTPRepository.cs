﻿using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IAccountOTPRepository : IEntityBaseRepository<AccountOTP>
    {
        AccountOTP CheckConfigOTP(string Phone, int OTP, AccountOTP.OTPType type);
        AccountOTP GetLastOTPByUsername(string Phone, AccountOTP.OTPType type);
        AccountOTP CheckConfigOTPOKE(string phone, int otp, AccountOTP.OTPType type);
        AccountOTP CheckConfigOTP(string phone, AccountOTP.OTPType type);
        Task<AccountOTP> AddInvalid(int id);
        Task SetConfigOTPDisabled(string phone, int otp, AccountOTP.OTPType type);
    }
}
