﻿using PT.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IEventSystemReportRepository : IEntityBaseRepository<Account>
    {
        Task<Flag> FlagLastCloseBikeAdd(string transactionCode, int dockId, int accountId);
    }
}
