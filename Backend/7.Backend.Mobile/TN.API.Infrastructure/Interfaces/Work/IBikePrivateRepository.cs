using TN.Domain.Model;
using TN.Domain.Interfaces;

namespace TN.Infrastructure.Interfaces
{
    public interface IBikePrivateRepository : IEntityBaseRepository<BikePrivate>
    {
    }
}

