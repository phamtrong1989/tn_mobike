﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IWalletRepository : IEntityBaseRepository<Wallet>
    {
        Task<Wallet> GetWalletAsync(int accountId);
        Task<Wallet> UpdateAddWalletAsync(int accountId, decimal balance, decimal subBalance, decimal tripPoint, string haskey, bool isPush, bool enableDayExpirys, int directDayExpirys);
        Task<bool> WalletChecked(decimal ticketValue, Wallet wInfo, decimal? minMoney = 10000);
        Tuple<decimal, decimal> SplitTotalPrice(decimal totalPrice, bool allowChargeInSubAccount, Wallet wallet);
        Task<List<SubPointExpiry>> ClearPointByAccounts(int accountId);
        Task<decimal> TotalDepositAmount(int accountId);
        Task AddDayExpirys(int accountId, decimal balance, decimal subBalance, bool isEndTransaction);
        Task<List<SubPointExpirySettlement>> SubPointExpirySettlementGets(int top);
        Task SubPointExpirySettlementSetTrue(int id);
        Task AddDayExpirysByDirect(int accountId, int days, string note);
    }
}
