using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IDiscountCodeRepository : IEntityBaseRepository<DiscountCode>
    {
        Task<List<DiscountCode>> DiscountCodePublicByAccount(int accountId, int projectIdAdd, int customerGroupIdAdd);
    }
}

