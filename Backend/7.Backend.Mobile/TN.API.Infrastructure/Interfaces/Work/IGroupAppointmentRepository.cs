﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IGroupAppointmentRepository : IEntityBaseRepository<GroupAppointment>
    {
        bool CheckInAnyRoom(int accountId);
        int SendJoinRoomCount(int accountId);
        GroupAppointment MyGroup(int accountId);
        List<GroupAppointment> RequestList(int accountId);
    }
}
