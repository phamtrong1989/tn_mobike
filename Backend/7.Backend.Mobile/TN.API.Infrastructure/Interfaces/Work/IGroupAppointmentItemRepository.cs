﻿using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IGroupAppointmentItemRepository : IEntityBaseRepository<GroupAppointmentItem>
    {
    }
}
