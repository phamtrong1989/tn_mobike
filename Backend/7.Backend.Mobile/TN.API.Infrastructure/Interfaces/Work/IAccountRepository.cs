﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IAccountRepository : IEntityBaseRepository<Account>
    {
        Task<Account> Login(string username, string password);
        Task<Account> FindByEtag(string etag);
        //Task<Tuple<int, Account>> LoginWithOTP(string username, int otp, int otpInvalidMax);
        Account GetByUserName(string username);
        Task<AccountDevice> AddDevice(int accountId, string deviceId, string token, DateTime start, DateTime end, string ip);
        Task UpdateDeviceToken(int accountId, string deviceId, string token);
        Task<List<AccountDevice>> FilterDevices(int accountId);
        Task<bool> CheckLocation(int accountId, int stationId);
        Task SetLocation(int accountId, int stationId, int addMinutes, double lat, double lng);
        Task<ApplicationUser> UserByCode(string code);
        Task<Flag> FlagGet(string transaction);
        Task FlagUpdate(string transaction);
        Task<ApplicationUser> UserByRFID(string rfid);
        Task<List<ApplicationUser>> UserByWarning();
        Task<ProjectAccount> ProjectAccountGet(int accountId, int projectId);
        Task UpdateLanguage(int accountId, string language, int languageId);
        Task ProjectAcountAdd(int accountId, int projectId, int customerGroupId);
        Task<AccountLocation> GetCurrentLocation(int accountId);
    }
}
