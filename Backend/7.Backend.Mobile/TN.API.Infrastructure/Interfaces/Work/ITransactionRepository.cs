﻿using TN.Domain.Model;
using TN.Domain.Interfaces;
using TN.Domain.Model.Common;
using System;
using System.Threading.Tasks;

namespace TN.API.Infrastructure.Interfaces
{
    public interface ITransactionRepository : IEntityBaseRepository<Transaction>
    {
        DashboardTransactionsModel DashboardTransactions(int accountId);
        string GetTicketPriceString(TicketPrice item, string format,string language);
        TotalPriceModel ToTotalPrice(
            ETicketType ticketType,
            DateTime startTime,
            DateTime endTime,
            decimal ticketPrice_TicketValue,
            int ticketPrice_LimitMinutes,
            int ticketPrice_BlockPerMinute,
            int ticketPrice_BlockPerViolation,
            decimal ticketPrice_BlockViolationValue,
            DateTime prepaid_EndTime,
            int prepaid_MinutesSpent,
            DiscountCode discountCode
            );
        Task<TransactionDiscount> DiscountTransactionByDay(int accountId, DateTime time);
        double TotalKMByAccount(int accountId, DateTime start, DateTime end);
    }
}
