﻿using System;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.API.Infrastructure.Interfaces
{
    public interface ISMSRepository : IEntityBaseRepository<Account>
    {
        Task<SMSResultV2Model> Send(string sms_path, string sms_token, string sms_username, string sms_password, string sms_brandname, string message, string phone);
        Task<ZNSSendMsg> ZALOSendOTP(ZaloSettings settings, AccountOTP.OTPType otpType, string toPhone, string otp);
        Task<ZNSSendMsg> ZALOSendWarning(ZaloSettings settings, string toPhone, string type, string plate, DateTime date, string message);
    }
}
