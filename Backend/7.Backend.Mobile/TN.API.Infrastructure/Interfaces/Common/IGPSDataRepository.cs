﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IMongoDBRepository
    {
        List<GPSData> GPSGetByIMEI(LogSettings settings, string imei, DateTime startTime, DateTime endTime);
        void AddLogRun(LogSettings logSettings, int accountId, string functionName, string logKey, EnumMongoLogType type, string content, string deviceKey, bool isException = false);
        void GPSAddRun(LogSettings settings, string imei, double lat, double lng);
        Task EventSystemAddAsync(LogSettings settings, EventSystemMongo data);
        TransactionGPS GetTransactionGPS(LogSettings settings, int transactionId, DateTime endTime);
    }
}
