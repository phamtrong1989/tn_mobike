﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using TN.Domain.Seedwork;
using TN.Domain.Model;

namespace TN.Domain.Interfaces
{
    public interface IEntityBaseRepository<T>: IRepository<T> where T : class, IAggregateRoot, new ()
    {
        Task<BaseSearchModel<List<T>>> SearchPagedList(int page, int limit, Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, Expression<Func<T, T>> select = null, params Expression<Func<T, object>>[] includeProperties);
        Task<List<T>> SearchAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
        Task<T> SearchOneAsync(bool asNoTracking, Expression<Func<T, bool>> predicate);
        Task AddAsync(T entity);
        Task AddRangeAsync(List<T> entity);
        void Update(T entity);
        void Delete(T entity);
        void DeleteWhere(Expression<Func<T, bool>> predicate);
        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate);
        Task<int> CountAsync(Expression<Func<T, bool>> predicate);
        Task CommitAsync();
        Task<List<T>> SearchByTopAsync(int top, Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
    }
}
