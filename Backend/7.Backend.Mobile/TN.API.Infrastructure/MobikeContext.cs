﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Infrastructure
{
    public class MobikeContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>, IUnitOfWork
    {
        public MobikeContext(DbContextOptions<MobikeContext> options)
             : base(options)
        {
        }



        // System User
        public DbSet<RoleController> RoleControllers { get; set; }
        public DbSet<RoleAction> RoleActions { get; set; }
        public DbSet<RoleDetail> RoleDetails { get; set; }
        public DbSet<RoleGroup> RoleGroups { get; set; }
        public DbSet<RoleArea> RoleAreas { get; set; }
        public DbSet<Log> Logs { get; set; }

        // Account
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountOTP> AccountOTPs { get; set; }
        public DbSet<Bike> Bikes { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<CustomerGroup> CustomerGroups { get; set; }
        public DbSet<Dock> Docks { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<NotificationData> Notifications { get; set; }
        public DbSet<OpenLockRequest> OpenLockRequests { get; set; }
        public DbSet<OpenLockHistory> OpenLockHistorys { get; set; }
        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<SystemNotification> SystemNotifications { get; set; }
        public DbSet<TicketPrepaid> TicketPrepaids { get; set; }
        public DbSet<TicketPrice> TicketPrices { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<VoucherCode> VoucherCodes { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<WalletTransaction> WalletTransactions { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<BikeReport> BikeReports { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<City> Citys { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Investor> Investors { get; set; }
        public DbSet<ProjectAccount> ProjectAccounts { get; set; }
        public DbSet<ProjectInvestor> ProjectInvestors { get; set; }

        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<Producer> Producers { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<RedeemPoint> RedeemPoints { get; set; }
        public DbSet<ContentPage> ContentPages { get; set; }
        public DbSet<AccountDevice> AccountDevices { get; set; }
        public DbSet<DockStatus> DockStatus { get; set; }
        public DbSet<DepositEvent> DepositEvents { get; set; }
        public DbSet<NotificationJob> NotificationJobs { get; set; }

        public DbSet<BikeInfo> BikeInfos { get; set; }
        public DbSet<MaintenanceRepair> MaintenanceRepairs { get; set; }
        public DbSet<MaintenanceRepairItem> MaintenanceRepairItems { get; set; }
        public DbSet<Supplies> Suppliess { get; set; }
        public DbSet<SuppliesWarehouse> SuppliesWarehouses { get; set; }
        public DbSet<ImportBill> ImportBills { get; set; }
        public DbSet<ImportBillItem> ImportBillItems { get; set; }
        public DbSet<DockInfo> DockInfos { get; set; }
        public DbSet<AccountLocation> AccountLocations { get; set; }

        public DbSet<ExportBillItem> ExportBillItems { get; set; }
        public DbSet<ExportBill> ExportBills { get; set; }
        public DbSet<Flag> Flags { get; set; }
        public DbSet<SubPointExpirySettlement> SubPointExpirySettlements { get; set; }
        public DbSet<CustomerComplaint> CustomerComplaints { get; set; }

        public DbSet<EmailManage> EmailManages { get; set; }

        public DbSet<EmailBox> EmailBoxs { get; set; }
        public DbSet<AppVersion> AppVersions { get; set; }
        public DbSet<SubPointExpiry> SubPointExpirys { get; set; }
        public DbSet<SubPointExpiryLog> SubPointExpiryLogs { get; set; }
        public DbSet<SMSBox> SMSBoxs { get; set; }
        public DbSet<VehicleRecallPrice> VehicleRecallPrices { get; set; }
        public DbSet<BikeWarning> BikeWarnings { get; set; }
        public DbSet<BikeWarningLog> BikeWarningLogs { get; set; }
        public DbSet<BookingFail> BookingFails { get; set; }
        public DbSet<DataInfo> DataInfos { get; set; }
        public DbSet<RFIDBooking> RFIDBookings { get; set; }
        public DbSet<RFIDTransaction> RFIDTransactions { get; set; }

        public DbSet<AccountBlackList> AccountBlackLists { get; set; }
        public DbSet<SystemConfiguration> SystemConfigurations { get; set; }
        public DbSet<CyclingWeek> CyclingWeeks { get; set; }
        public DbSet<TransactionDiscount> TransactionDiscounts { get; set; }
        public DbSet<Banner> Baners { get; set; }
        public DbSet<BannerGroup> BanerGroups { get; set; }
        public DbSet<ProjectResource> ProjectResources { get; set; }
        public DbSet<CustomerGroupResource> CustomerGroupResources { get; set; }
        public DbSet<StationResource> StationResources { get; set; }
        public DbSet<BikePrivate> BikePrivates { get; set; }
        public DbSet<AccountRatingItem> AccountRatingItems { get; set; }
        public DbSet<AccountRating> AccountRatings { get; set; }
        public DbSet<DiscountCode> DiscountCodes { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<PostLike> PostLikes { get; set; }
        public DbSet<GroupAppointment> GroupAppointments { get; set; }
        public DbSet<GroupAppointmentItem> GroupAppointmentItems { get; set; }
        public DbSet<GroupAppointmentAccount> GroupAppointmentAccounts { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(builder);

            builder.Entity<RoleDetail>().ToTable("RoleDetail", "adm").HasKey(c => new { c.ActionId, c.RoleId });
            builder.Entity<RoleController>().ToTable("RoleController", "adm");
            builder.Entity<RoleAction>().ToTable("RoleAction", "adm");
            builder.Entity<RoleGroup>().ToTable("RoleGroup", "adm");
            builder.Entity<RoleArea>().ToTable("RoleArea", "adm");
            builder.Entity<ApplicationUser>().ToTable("User", "adm");
            builder.Entity<ApplicationRole>().ToTable("Role", "adm");
            builder.Entity<IdentityUserRole<int>>().ToTable("UserRole", "adm");
            builder.Entity<IdentityRoleClaim<int>>().ToTable("RoleClaim", "adm");
            builder.Entity<IdentityUserLogin<int>>().ToTable("UserLogin", "adm");
            builder.Entity<IdentityUserToken<int>>().ToTable("UserToken", "adm");
            builder.Entity<IdentityUserClaim<int>>().ToTable("UserClaim", "adm");
            builder.Entity<Log>().ToTable("Log", "adm");

            builder.Entity<Account>().ToTable("Account");
            builder.Entity<AccountDevice>().ToTable("AccountDevice");
            builder.Entity<AccountOTP>().ToTable("AccountOTP");
            builder.Entity<Bike>().ToTable("Bike");
            builder.Entity<Booking>().ToTable("Booking");
            builder.Entity<Campaign>().ToTable("Campaign");
            builder.Entity<CustomerGroup>().ToTable("CustomerGroup");
            builder.Entity<Dock>().ToTable("Dock");
            builder.Entity<Feedback>().ToTable("Feedback");
            builder.Entity<NotificationData>().ToTable("Notification");
            builder.Entity<OpenLockRequest>().ToTable("OpenLockRequest");
            builder.Entity<OpenLockHistory>().ToTable("OpenLockHistory");
            builder.Entity<Parameter>().ToTable("Parameter");
            builder.Entity<Station>().ToTable("Station");
            builder.Entity<SystemNotification>().ToTable("SystemNotification");
            builder.Entity<TicketPrepaid>().ToTable("TicketPrepaid");
            builder.Entity<TicketPrice>().ToTable("TicketPrice");
            builder.Entity<Transaction>().ToTable("Transaction");
            builder.Entity<VoucherCode>().ToTable("VoucherCode");
            builder.Entity<Wallet>().ToTable("Wallet");
            builder.Entity<WalletTransaction>().ToTable("WalletTransaction");
            builder.Entity<News>().ToTable("News");
            builder.Entity<BikeReport>().ToTable("BikeReport");
            builder.Entity<District>().ToTable("District");
            builder.Entity<City>().ToTable("City");
            builder.Entity<Project>().ToTable("Project");
            builder.Entity<Investor>().ToTable("Investor");
            builder.Entity<ProjectAccount>().ToTable("ProjectAccount");
            builder.Entity<ProjectInvestor>().ToTable("ProjectInvestor");

            builder.Entity<Warehouse>().ToTable("Warehouse");
            builder.Entity<Producer>().ToTable("Producer");
            builder.Entity<Model>().ToTable("Model");
            builder.Entity<Color>().ToTable("Color");
            builder.Entity<Language>().ToTable("Language");
            builder.Entity<RedeemPoint>().ToTable("RedeemPoint");
            builder.Entity<ContentPage>().ToTable("ContentPage");
            builder.Entity<DockStatus>().ToTable("DockStatus");
            builder.Entity<DepositEvent>().ToTable("DepositEvent");
            builder.Entity<NotificationJob>().ToTable("NotificationJob");

            builder.Entity<BikeInfo>().ToTable("BikeInfo");
            builder.Entity<MaintenanceRepair>().ToTable("MaintenanceRepair");
            builder.Entity<MaintenanceRepairItem>().ToTable("MaintenanceRepairItem");
            builder.Entity<Supplies>().ToTable("Supplies");
            builder.Entity<SuppliesWarehouse>().ToTable("SuppliesWarehouse");
            builder.Entity<ImportBill>().ToTable("ImportBill");
            builder.Entity<ImportBillItem>().ToTable("ImportBillItem");
            builder.Entity<DockInfo>().ToTable("DockInfo");
            builder.Entity<AccountLocation>().ToTable("AccountLocation");

            builder.Entity<ExportBillItem>().ToTable("ExportBillItem");
            builder.Entity<ExportBill>().ToTable("ExportBill");
            builder.Entity<Flag>().ToTable("Flag");
            builder.Entity<SubPointExpirySettlement>().ToTable("SubPointExpirySettlement");
            builder.Entity<CustomerComplaint>().ToTable("CustomerComplaint");

            builder.Entity<EmailBox>().ToTable("EmailBox");
            builder.Entity<EmailManage>().ToTable("EmailManage");
            builder.Entity<AppVersion>().ToTable("AppVersion");
            builder.Entity<SubPointExpiry>().ToTable("SubPointExpiry");
            builder.Entity<SubPointExpiryLog>().ToTable("SubPointExpiryLog");
            builder.Entity<SMSBox>().ToTable("SMSBox");
            builder.Entity<VehicleRecallPrice>().ToTable("VehicleRecallPrice");
            builder.Entity<BikeWarning>().ToTable("BikeWarning");
            builder.Entity<BikeWarningLog>().ToTable("BikeWarningLog");
            builder.Entity<BookingFail>().ToTable("BookingFail");
            builder.Entity<DataInfo>().ToTable("DataInfo");
            builder.Entity<RFIDBooking>().ToTable("RFIDBooking");
            builder.Entity<RFIDTransaction>().ToTable("RFIDTransaction");
            builder.Entity<AccountBlackList>().ToTable("AccountBlackList");
            builder.Entity<SystemConfiguration>().ToTable("SystemConfiguration");
            builder.Entity<CyclingWeek>().ToTable("CyclingWeek");
            builder.Entity<TransactionDiscount>().ToTable("TransactionDiscount");
            builder.Entity<BannerGroup>().ToTable("BannerGroup");
            builder.Entity<Banner>().ToTable("Banner");
            builder.Entity<ProjectResource>().ToTable("ProjectResource");
            builder.Entity<CustomerGroupResource>().ToTable("CustomerGroupResource");
            builder.Entity<StationResource>().ToTable("StationResource");
            builder.Entity<BikePrivate>().ToTable("BikePrivate");
            builder.Entity<AccountRatingItem>().ToTable("AccountRatingItem");
            builder.Entity<AccountRating>().ToTable("AccountRating");
            builder.Entity<DiscountCode>().ToTable("DiscountCode");

            builder.Entity<Post>().ToTable("Post");
            builder.Entity<PostLike>().ToTable("PostLike");
            builder.Entity<GroupAppointment>().ToTable("GroupAppointment");
            builder.Entity<GroupAppointmentItem>().ToTable("GroupAppointmentItem");
            builder.Entity<GroupAppointmentAccount>().ToTable("GroupAppointmentAccount");
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            _ = await base.SaveChangesAsync();
            return true;
        }
    }
}
