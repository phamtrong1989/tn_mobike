﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Mobike.ServerServices.Entity
{
    public class Device
    {
        public string No { get; set; }
        public string IMEI { get; set; }
        public string MAC { get; set; }
    }
}
