﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.Mobike.ServerServices.Entity
{
    class OpenLock
    {
        public int Id { get; set; } = 0;
        public int TenantId { get; set; }
        public int StationId { get; set; }
        public int DockId { get; set; }
        public int BikeId { get; set; }
        public int AccountId { get; set; }
        public string Phone { get; set; }
        public string TransactionCode { get; set; }
        public string IMEI { get; set; }
        public string SerialNumber { get; set; }
        public int Retry { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime OpenTime { get; set; } = DateTime.Now;

        public OpenLock()
        {
            
        }

        public OpenLock(int id, int tenantId, int stationId, int dockId, int bikeId, int accountId, string phone, string transactionCode, string imei, string serialNumber, int retry, int status, DateTime createdDate, DateTime openTime)
        {
            TenantId = tenantId;
            StationId = stationId;
            DockId = dockId;
            BikeId = bikeId;
            AccountId = accountId;
            Phone = phone;
            TransactionCode = transactionCode;
            IMEI = imei;
            SerialNumber = serialNumber;
            Retry = retry;
            Status = status;
            CreatedDate = createdDate;
            OpenTime = openTime;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    class OpenLockRequest : OpenLock
    {

        public OpenLockRequest()
        {
            
        }

        public OpenLockRequest(OpenLockHistory open)
        {
            TenantId = open.TenantId;
            StationId = open.StationId;
            DockId = open.DockId;
            BikeId = open.BikeId;
            AccountId = open.AccountId;
            Phone = open.Phone;
            TransactionCode = open.TransactionCode;
            IMEI = open.IMEI;
            SerialNumber = open.SerialNumber;
            Retry = open.Retry;
            Status = open.Status;
            CreatedDate = open.CreatedDate;
            OpenTime = open.OpenTime;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }

    class OpenLockHistory : OpenLock
    {

        public OpenLockHistory()
        {
            
        }

        public OpenLockHistory(OpenLockRequest open)
        {
            TenantId = open.TenantId;
            StationId = open.StationId;
            DockId = open.DockId;
            BikeId = open.BikeId;
            AccountId = open.AccountId;
            Phone = open.Phone;
            TransactionCode = open.TransactionCode;
            IMEI = open.IMEI;
            SerialNumber = open.SerialNumber;
            Retry = open.Retry;
            Status = open.Status;
            CreatedDate = open.CreatedDate;
            OpenTime = open.OpenTime;
        }

        

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
