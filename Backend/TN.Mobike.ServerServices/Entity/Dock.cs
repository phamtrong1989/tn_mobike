﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Mobike.ServerServices.Entity
{
	public class Dock
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public int StationId { get; set; }
        public int BikeId { get; set; }
        public string IMEI { get; set; }
        public string SerialNumber { get; set; }
        public string Model { get; set; }
        public int Status { get; set; }
        public bool LockStatus { get; set; }
        public double Battery { get; set; }
        public bool Charging { get; set; }
        public bool ConnectionStatus { get; set; }
        public DateTime LastConnectionTime { get; set; }
        public int BookingStatus { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime UpdatedDate { get; set; } = DateTime.Now;

        public Dock()
        {
            
        }

        public Dock(int id, int tenantId, int stationId, int bikeId, string imei, string serialNumber, string model, int status, bool lockStatus, double battery, bool charging, bool connectionStatus, DateTime lastConnectionTime, int bookingStatus, double lat, double l, DateTime createdDate, DateTime updatedDate)
        {
            Id = id;
            TenantId = tenantId;
            StationId = stationId;
            BikeId = bikeId;
            IMEI = imei;
            SerialNumber = serialNumber;
            Model = model;
            Status = status;
            LockStatus = lockStatus;
            Battery = battery;
            Charging = charging;
            ConnectionStatus = connectionStatus;
            LastConnectionTime = lastConnectionTime;
            BookingStatus = bookingStatus;
            Lat = lat;
            Long = l;
            CreatedDate = createdDate;
            UpdatedDate = updatedDate;
        }
    }
}
