﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommonClassLibs;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using TN.Mobike.ServerServices.Data;
using TN.Mobike.ServerServices.Entity;
using TN.Mobike.ServerServices.Services;
using TN.Mobike.ServerServices.Settings;

namespace TN.Mobike.ServerServices.Core
{
    public class StateObject
    {
        public long connectionNumber = -1;
        public System.Net.Sockets.Socket workSocket { get; set; }
        public byte[] buffer { get; set; }
        public int fifoCount { get; set; }
        public string IMEI { get; set; }
    }

    public enum PROTOCOL_NUMBER
    {
        LOGIN_INFORMATION = 0x01,
        HEARTBEAT_PACKET = 0x23,
        ONLINE_COMMAND_RESPONSE_BY_TERMINAL = 0x21,
        GPS_LOCATION_INFORMATION = 0x32,
        LOCATION_INFORMATION_ALARM = 0x33,
        ONLINECOMMAND = 0x80,
        INFORMATION_TRANSMISSION_PACKET = 0x98,
        NONE
    }
    public enum UNPACK_STATUS
    {
        ERROR,
        NOT_ENOUGH_BYTES,
        BAD_CRC,
        GOOD_MESSAGE,
        DEFAULT
    }
    public enum PROCESS_STATE
    {
        ACCEPT,
        READ,
        PROCESS,
        UNPACK
    }
    public enum TERMINAL_INFOR
    {
        GPS_POSITIONING = 1,
        GPS_NOT_POSITIONING = 0,
        CHARGING = 1,
        NOT_CHARGE = 0,
        LOCK_ON = 1,
        LOCK_OFF = 0,

    }
    public enum ALARM : Byte
    {
        NORMAL = 0,
        SHOCK_ALARM = 1,
        POWER_CUT_ALARM = 2,
        LOW_BATTERY = 3,
        SOS = 4
    }

    public class Socket
    {
        const int BUFFER_SIZE = 1024;
        static long connectionNumber = 0;
        //mapping of connection number to StateObject
        static Dictionary<long, KeyValuePair<List<byte>, StateObject>> connectionDict = new Dictionary<long, KeyValuePair<List<byte>, StateObject>>();
        //fifo contains list of connections number wait with receive data
        public static List<long> fifo = new List<long>();
        public static AutoResetEvent allDone = new AutoResetEvent(false);
        public static AutoResetEvent acceptDone = new AutoResetEvent(false);
        static Server server = null;

        public static bool Unlock(string imei)
        {
            try
            {
                var check = false;
                lock (server.workerSockets)
                {
                    // Get List of values. [3]
                    List<Server.UserSock> valueList = new List<Server.UserSock>(server.workerSockets.Values);
                    // Display them.
                    foreach (var value in valueList)
                    {
                        if (value.szIMEI == imei)
                        {
                            Server.UserSock userSock = server.workerSockets[value.iClientID];
                            byte[] sendUnlock = { 0x78, 0x78, 0x11, 0x80, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x55, 0x4E, 0x4C, 0x4F, 0x43, 0x4B, 0x23, 0x00, 0x01, 0x53, 0x54, 0x0D, 0x0A };
                            Send(userSock.UserSocket, sendUnlock);
                            check = true;
                        }
                    }

                    return check;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[Socket.Unlock]", $"ERROR IMEI = {imei} : {e}");
                return false;
            }
        }

        public static bool ProcessData(string data)
        {

            try
            {
                var check = false;
                var device = JsonConvert.DeserializeObject<Device>(data);

                lock (server.workerSockets)
                {
                    // Get List of values. [3]
                    List<Server.UserSock> valueList = new List<Server.UserSock>(server.workerSockets.Values);
                    // Display them.
                    foreach (var value in valueList)
                    {
                        if (value.szIMEI == device.IMEI)
                        {
                            Server.UserSock userSock = server.workerSockets[value.iClientID];
                            byte[] sendUnlock = { 0x78, 0x78, 0x11, 0x80, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x55, 0x4E, 0x4C, 0x4F, 0x43, 0x4B, 0x23, 0x00, 0x01, 0x53, 0x54, 0x0D, 0x0A };
                            Send(userSock.UserSocket, sendUnlock);

                            //OpenLockHistory open = new OpenLockHistory {IMEI = device.IMEI};
                            //SqlHelper.InsertOne<OpenLockHistory>(open);
                            //Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"INSERT TO OpenLockHistory SUCCESSFUL : {JsonConvert.SerializeObject(open)}");
                            Utilities.WriteOperationLog("[Socket.ProcessData]", $"OPEN LOCK SUCCESSFUL : {data}");
                            
                            check = true;
                        }
                    }
                    return check;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[Socket.ProcessData]", $"ERROR Data = {data} : {e}");
                return false;
            }

        }

        public void Start()
        {
            try
            {
                StartPacketCommunicationsServiceThread();
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[Socket.Start]", $"[ ERROR: {e} ]");
            }
        }


        public static HubConnection _connection;
        static AutoResetEvent autoEvent;//mutex
        static AutoResetEvent autoEvent2;
        private Thread DataProcessThread = null;
        private Thread FullPacketDataProcessThread = null;
        private Queue<FullPacket> FullPacketList = null;
        private Dictionary<int, MotherOfRawPackets> dClientRawPacketList = null;
        bool ServerIsExiting = false;
        private int MyPort = AppSettings.PortService;

        private void StartPacketCommunicationsServiceThread()
        {
            try
            {
                //Packet processor mutex and loop
                autoEvent = new AutoResetEvent(false); //the RawPacket data mutex
                autoEvent2 = new AutoResetEvent(false);//the FullPacket data mutex
                DataProcessThread = new Thread(new ThreadStart(NormalizeThePackets));
                FullPacketDataProcessThread = new Thread(new ThreadStart(ProcessRecievedData));


                //Lists
                dClientRawPacketList = new Dictionary<int, MotherOfRawPackets>();
                FullPacketList = new Queue<FullPacket>();

                //Create HostServer
                server = new Server();

                server.Listen(MyPort);
                server.OnReceiveData += new Server.ReceiveDataCallback(OnDataReceived);
                server.OnClientConnect += new Server.ClientConnectCallback(NewClientConnected);
                server.OnClientDisconnect += new Server.ClientDisconnectCallback(ClientDisconnect);

                DataProcessThread.Start();
                FullPacketDataProcessThread.Start();

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"START SOCKET SERVER SUCCESSFUL ! - LISTENING ON PORT : {MyPort}");
                }

            }
            catch (Exception ex)
            {
                var exceptionMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                //Debug.WriteLine($"EXCEPTION IN: StartPacketCommunicationsServiceThread - {exceptionMessage}");
            }
        }


        private void NewClientConnected(int ConnectionID)
        {
            try
            {
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"(RT Client)NewClientConnected: {ConnectionID}");
                    Console.WriteLine($"Incoming Connection {ConnectionID}");
                }

                if (server.workerSockets.ContainsKey(ConnectionID))
                {
                    lock (dClientRawPacketList)
                    {
                        //Add the raw Packet collector
                        if (!dClientRawPacketList.ContainsKey(ConnectionID))
                        {
                            dClientRawPacketList.Add(ConnectionID, new MotherOfRawPackets(ConnectionID));
                        }
                    }

                    //SetNewConnectionData_FromThread(ConnectionID);
                }
                else
                {
                    Debug.WriteLine("UNKNOWN CONNECTION ID" + ConnectionID.ToString());
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_NewClientConnected", ex.ToString());

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"EXCEPTION: NewClientConnected on client {ConnectionID}, exception: {ex.Message}");
                }
            }
        }

        private void OnDataReceived(int clientNumber, byte[] message, int messageSize)
        {
            if (dClientRawPacketList.ContainsKey(clientNumber))
            {
                byte[] buffer = new byte[messageSize];
                Array.Copy(message, buffer, messageSize);
                dClientRawPacketList[clientNumber].AddToList(buffer, messageSize);
                ProcessMessages(buffer, server.workerSockets[clientNumber].UserSocket, clientNumber);

                if (AppSettings.ModeRun)
                {
                    Debug.WriteLine("Raw Data From: " + clientNumber.ToString() + ", Size of Packet: " + messageSize.ToString() + ", Message: " + string.Join("", buffer.Select(x => x.ToString("X2"))));
                }
                autoEvent.Set();//Fire in the hole
            }
        }

        private void PostUserCredentials(int clientNumber, byte[] message)
        {
            try
            {
                PACKET_DATA IncomingData = new PACKET_DATA();
                IncomingData = (PACKET_DATA)PACKET_FUNCTIONS.ByteArrayToStructure(message, typeof(PACKET_DATA));

                lock (server.workerSockets)
                {
                    string ComputerName = new string(IncomingData.szStringDataA).TrimEnd('\0');//Station/Computer's name
                    string VersionStr = new string(IncomingData.szStringDataB).TrimEnd('\0');//app version
                    string ClientsName = new string(IncomingData.szStringData150).TrimEnd('\0');//Client's Name

                    if (server.workerSockets.ContainsKey(clientNumber))
                    {
                        server.workerSockets[clientNumber].szStationName = ComputerName;

                        server.workerSockets[clientNumber].szClientName = ClientsName;

                        Console.WriteLine(
                            $"Registered Connection ({clientNumber}) for '{ClientsName}' on PC: {ComputerName}");
                    }
                }//end lock

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_PostUserCredentials", ex.ToString());
            }
        }

        private void UpdateTheConnectionTimers(int clientNumber, byte[] message)
        {
            lock (server.workerSockets)
            {
                try
                {
                    if (server.workerSockets.ContainsKey(clientNumber))
                    {
                        server.workerSockets[clientNumber].dTimer = DateTime.Now;
                        Int64 elapsedTime = server.workerSockets[clientNumber].PingStatClass.StopTheClock();

                        //Console.WriteLine("UpdateTheConnectionTimers: " + ConnectionID.ToString());
                        //Debug.WriteLine("Ping Time for " + ConnectionID.ToString() + ": " + elapsedTime.ToString() + "ms");

                        PACKET_DATA IncomingData = new PACKET_DATA();
                        IncomingData = (PACKET_DATA)PACKET_FUNCTIONS.ByteArrayToStructure(message, typeof(PACKET_DATA));

                        int ThisClientsMaxReturnDataSetValue = IncomingData.Data16;


                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"Ping From Server to client: {elapsedTime}ms");
                        }

                        UpdateThePingTimeFromThread(clientNumber, elapsedTime);
                        /****************************************************************************************/

                    }
                }
                catch (Exception ex)
                {
                    string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;
                    Utilities.WriteErrorLog("frmServer_UpdateTheConnectionTimers", ex.ToString());
                }
            }
        }

        private void UpdateThePingTimeFromThread(int clientNumber, long elapsedTimeInMilliseconds)
        {
            if (AppSettings.ModeRun)
            {
                Console.WriteLine($"{clientNumber.ToString()} : {elapsedTimeInMilliseconds:0.##}ms");
            }
        }

        private void ClientDisconnect(int clientNumber)
        {
            if (ServerIsExiting)
                return;

            /*******************************************************/
            lock (dClientRawPacketList)//Make sure we don't do this twice
            {
                if (!dClientRawPacketList.ContainsKey(clientNumber))
                {
                    lock (server.workerSockets)
                    {
                        if (!server.workerSockets.ContainsKey(clientNumber))
                        {
                            return;
                        }
                    }
                }
            }
            /*******************************************************/

            try
            {
                RemoveClient_FromThread(clientNumber);
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_ClientDisconnect", ex.ToString());
            }

            CleanupDeadClient(clientNumber);


            Thread.Sleep(10);
        }

        private void CleanupDeadClient(int clientNumber)
        {
            try
            {
                lock (dClientRawPacketList)
                {
                    if (dClientRawPacketList.ContainsKey(clientNumber))
                    {
                        dClientRawPacketList[clientNumber].ClearList();
                        dClientRawPacketList.Remove(clientNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_CleanupDeadClient", ex.ToString());
            }

            try
            {
                lock (server.workerSockets)
                {
                    if (server.workerSockets.ContainsKey(clientNumber))
                    {
                        server.workerSockets[clientNumber].UserSocket.Close();
                        server.workerSockets.Remove(clientNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_CleanupDeadClient", ex.ToString());
            }
        }

        private void SendMessageOfClientDisconnect(int clientId)
        {
            try
            {
                PACKET_DATA xdata = new PACKET_DATA();

                xdata.Packet_Type = (UInt16)PACKETTYPES.TYPE_ClientDisconnecting;
                xdata.Data_Type = 0;
                xdata.Packet_Size = (UInt16)Marshal.SizeOf(typeof(PACKET_DATA));
                xdata.maskTo = 0;
                xdata.idTo = 0;
                xdata.idFrom = (UInt32)clientId;

                byte[] byData = PACKET_FUNCTIONS.StructureToByteArray(xdata);
                server.SendMessage(byData);
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_SendMessageOfClientDisconnect", ex.ToString());
            }
        }

        private void RemoveClient_FromThread(int clientNumber)
        {
            try
            {
                if (AppSettings.ModeRun && server.workerSockets.TryGetValue(clientNumber, out var dataSock))
                {
                    Console.WriteLine($"Client Number : {clientNumber} -- IMEI : {dataSock.szIMEI} has disconnected");
                }

                SendMessageOfClientDisconnect(clientNumber);
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_RemoveClient_FromThread", ex.ToString());
            }
        }

        private void AssembleMessage(int clientID, byte[] message)
        {
            try
            {
                PACKET_DATA IncomingData = new PACKET_DATA();
                IncomingData = (PACKET_DATA)PACKET_FUNCTIONS.ByteArrayToStructure(message, typeof(PACKET_DATA));

                switch (IncomingData.Data_Type)
                {
                    case (UInt16)PACKETTYPES_SUBMESSAGE.SUBMSG_MessageStart:
                        {
                            if (server.workerSockets.ContainsKey(clientID) && AppSettings.ModeRun)
                            {
                                Console.WriteLine($"Client '{server.workerSockets[clientID].szClientName}' sent some numbers and some text... num1= {IncomingData.Data16} and num2= {IncomingData.Data17}:");
                                Console.WriteLine("Client also said:");

                                //sb = new StringBuilder(new string(IncomingData.szStringDataA).TrimEnd('\0'));
                                Console.WriteLine($"{new string(IncomingData.szStringDataA).TrimEnd('\0')}");
                            }
                        }
                        break;
                    case (UInt16)PACKETTYPES_SUBMESSAGE.SUBMSG_MessageGuts:
                        {
                            //sb.Append(new string(IncomingData.szStringDataA).TrimEnd('\0'));
                            if (AppSettings.ModeRun)
                            {
                                Console.WriteLine($"{new string(IncomingData.szStringDataA).TrimEnd('\0')}");
                            }
                        }
                        break;
                    case (UInt16)PACKETTYPES_SUBMESSAGE.SUBMSG_MessageEnd:
                        {
                            //sb = new StringBuilder(new string(IncomingData.szStringDataA).TrimEnd('\0'));
                            if (AppSettings.ModeRun)
                            {
                                Console.WriteLine("FINISHED GETTING MESSAGE");
                            }

                            /****************************************************************/
                            //Now tell the client teh message was received!
                            PACKET_DATA xdata = new PACKET_DATA();

                            xdata.Packet_Type = (UInt16)PACKETTYPES.TYPE_MessageReceived;

                            byte[] byData = PACKET_FUNCTIONS.StructureToByteArray(xdata);

                            server.SendMessage(clientID, byData);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_AssembleMessage", ex.ToString());

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine("ERROR Assembling message");
                }
            }
        }

        private void PassDataThru(UInt16 type, int MessageFrom, byte[] message)
        {
            try
            {
                int ForwardTo = (int)message[11] << 24 | (int)message[10] << 16 | (int)message[9] << 8 | (int)message[8];

                //Stuff in who this packet is from so we know who sent it
                byte[] x = BitConverter.GetBytes(MessageFrom);
                message[12] = (byte)x[0];//idFrom
                message[13] = (byte)x[1];//idFrom
                message[14] = (byte)x[2];//idFrom
                message[15] = (byte)x[3];//idFrom

                if (ForwardTo > 0)
                    server.SendMessage(ForwardTo, message);
                else
                    server.SendMessage(message);
            }
            catch (Exception ex)
            {
                string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"EXCEPTION in  PassDataThru - {msg}");
                }
            }
        }

        private void ProcessRecievedData()
        {
            if (server == null)
                return;

            while (server.IsListening)
            {
                //Debug.WriteLine("Before AutoEvent");
                autoEvent2.WaitOne();//wait at mutex until signal
                //Debug.WriteLine("After AutoEvent");

                try
                {
                    while (FullPacketList.Count > 0)
                    {
                        FullPacket fp;
                        lock (FullPacketList)
                            fp = FullPacketList.Dequeue();
                        //Console.WriteLine(GetDateTimeFormatted +" - Full packet fromID: " + fp.iFromClient.ToString() + ", Type: " + ((PACKETTYPES)fp.ThePacket[0]).ToString());
                        UInt16 type = (ushort)(fp.ThePacket[1] << 8 | fp.ThePacket[0]);
                        switch (type)//Interrigate the first 2 Bytes to see what the packet TYPE is
                        {
                            case (UInt16)PACKETTYPES.TYPE_MyCredentials:
                                {
                                    PostUserCredentials(fp.iFromClient, fp.ThePacket);
                                    //SendRegisteredMessage(fp.iFromClient, fp.ThePacket);
                                }
                                break;
                            case (UInt16)PACKETTYPES.TYPE_CredentialsUpdate:
                                break;
                            case (UInt16)PACKETTYPES.TYPE_PingResponse:
                                //Debug.WriteLine(DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToLongTimeString() + " - Received Ping from: " + fp.iFromClient.ToString() + ", on " + DateTime.Now.ToShortDateString() + ", at: " + DateTime.Now.ToLongTimeString());
                                UpdateTheConnectionTimers(fp.iFromClient, fp.ThePacket);
                                break;
                            case (UInt16)PACKETTYPES.TYPE_Close:
                                ClientDisconnect(fp.iFromClient);
                                break;
                            case (UInt16)PACKETTYPES.TYPE_Message:
                                {
                                    AssembleMessage(fp.iFromClient, fp.ThePacket);
                                }
                                break;
                            default:
                                PassDataThru(type, fp.iFromClient, fp.ThePacket);
                                break;
                        }
                    }//END  while (FullPacketList.Count > 0)
                }//try
                catch (Exception ex)
                {
                    string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"EXCEPTION in  ProcessRecievedData - {msg}");
                    }
                }

                if (ServerIsExiting)
                    break;
            }//End while (svr.IsListening)

            string info2 = $"AppIsExiting = {ServerIsExiting.ToString()}";
            string info3 = "Past the ProcessRecievedData loop";

            if (AppSettings.ModeRun)
            {
                Console.WriteLine(info2);
                Console.WriteLine(info3);
            }

            if (!ServerIsExiting && AppSettings.ModeRun)
            {
                //if we got here then something went wrong, we need to shut down the service
                Console.WriteLine("SOMETHING CRASHED");
            }
        }

        static unsafe void Copy(byte[] src, int srcIndex, byte[] dst, int dstIndex, int count)
        {
            try
            {
                if (src == null || srcIndex < 0 || dst == null || dstIndex < 0 || count < 0)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine("Serious Error in the Copy function 1");
                    }
                    throw new System.ArgumentException();
                }

                int srcLen = src.Length;
                int dstLen = dst.Length;
                if (srcLen - srcIndex < count || dstLen - dstIndex < count)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine("Serious Error in the Copy function 2");
                    }
                    throw new System.ArgumentException();
                }

                // The following fixed statement pins the location of the src and dst objects
                // in memory so that they will not be moved by garbage collection.
                fixed (byte* pSrc = src, pDst = dst)
                {
                    byte* ps = pSrc + srcIndex;
                    byte* pd = pDst + dstIndex;

                    // Loop over the count in blocks of 4 bytes, copying an integer (4 bytes) at a time:
                    for (int i = 0; i < count / 4; i++)
                    {
                        *((int*)pd) = *((int*)ps);
                        pd += 4;
                        ps += 4;
                    }

                    // Complete the copy by moving any bytes that weren't moved in blocks of 4:
                    for (int i = 0; i < count % 4; i++)
                    {
                        *pd = *ps;
                        pd++;
                        ps++;
                    }
                }
            }
            catch (Exception ex)
            {
                var exceptionMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                Utilities.WriteDebugLog("frmServer_Copy", ex.ToString());

                Debug.WriteLine("EXCEPTION IN: Copy - " + exceptionMessage);
            }

        }

        void NormalizeThePackets()
        {
            if (server == null)
                return;

            while (server.IsListening)
            {
                //Debug.WriteLine("Before AutoEvent");
                autoEvent.WaitOne(10000);//wait at mutex until signal, and drop through every 10 seconds if something strange happens
                //Debug.WriteLine("After AutoEvent");

                /**********************************************/
                lock (dClientRawPacketList)//http://www.albahari.com/threading/part2.aspx#_Locking
                {
                    foreach (MotherOfRawPackets MRP in dClientRawPacketList.Values)
                    {
                        if (MRP.GetItemCount.Equals(0))
                            continue;
                        try
                        {
                            byte[] packetplayground = new byte[11264];//good for 10 full packets(10240) + 1 remainder(1024)
                            RawPackets rp;

                            int actualPackets = 0;

                            while (true)
                            {
                                if (MRP.GetItemCount == 0)
                                    break;

                                int holdLen = 0;

                                if (MRP.bytesRemaining > 0)
                                    Copy(MRP.Remainder, 0, packetplayground, 0, MRP.bytesRemaining);

                                holdLen = MRP.bytesRemaining;

                                for (int i = 0; i < 10; i++)//only go through a max of 10 times so there will be room for any remainder
                                {
                                    rp = MRP.GetTopItem;//dequeue

                                    Copy(rp.dataChunk, 0, packetplayground, holdLen, rp.iChunkLen);

                                    holdLen += rp.iChunkLen;

                                    if (MRP.GetItemCount.Equals(0))//make sure there is more in the list befor continuing
                                        break;
                                }

                                actualPackets = 0;

                                #region PACKET_SIZE 1024
                                if (holdLen >= 1024)//make sure we have at least one packet in there
                                {
                                    actualPackets = holdLen / 1024;
                                    MRP.bytesRemaining = holdLen - (actualPackets * 1024);

                                    for (int i = 0; i < actualPackets; i++)
                                    {
                                        byte[] tmpByteArr = new byte[1024];
                                        Copy(packetplayground, i * 1024, tmpByteArr, 0, 1024);
                                        lock (FullPacketList)
                                            FullPacketList.Enqueue(new FullPacket(MRP.iListClientID, tmpByteArr));
                                    }
                                }
                                else
                                {
                                    MRP.bytesRemaining = holdLen;
                                }

                                //hang onto the remainder
                                Copy(packetplayground, actualPackets * 1024, MRP.Remainder, 0, MRP.bytesRemaining);
                                #endregion

                                if (FullPacketList.Count > 0)
                                    autoEvent2.Set();
                                //Call_ProcessRecievedData_FromThread();

                            }//end of while(true)
                        }
                        catch (Exception ex)
                        {
                            MRP.ClearList();//pe 03-20-2013
                            string msg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;
                        }
                    }//end of foreach (dClientRawPacketList)
                }//end of lock
                /**********************************************/
                if (ServerIsExiting)
                    break;
            }//Endof of while(svr.IsListening)

            Debug.WriteLine("Exiting the packet normalizer");
        }

        public DateTime getDateTimeLocal(byte[] DateTimeUtc)
        {
            DateTime dateTimeUTC = new DateTime(DateTimeUtc[0] + 2000, DateTimeUtc[1], DateTimeUtc[2], DateTimeUtc[3], DateTimeUtc[4], DateTimeUtc[5]);
            return dateTimeUTC.ToLocalTime();
        }
        public double getLatitudeLongitude(byte[] LatitudeLongitude)
        {
            return (double)(Convert.ToInt32(BytesToString(LatitudeLongitude), 16)) / 1800000;
        }
        public double getPercentVoltage(byte[] VoltageLevel)
        {
            double volNumber = (Convert.ToInt64(BytesToString(VoltageLevel), 16));
            return Math.Round((((volNumber / 100 - 3.50d) / 0.66) * 100), 2);
        }
        public char[] getTerminalInformationContent(byte[] TerminalInformationContent)
        {
           // Console.WriteLine($"TerminalInformationContent                   : {TerminalInformationContent[0]}");
           // Console.WriteLine($"TerminalInformationContent bit               : {(Convert.ToString(TerminalInformationContent[0], 2))}");
           // Console.WriteLine($"TerminalInformationContent bit arr           : {(Convert.ToString(TerminalInformationContent[0], 2)).ToArray()}");
            char[] TerminalInfo = (Convert.ToString(TerminalInformationContent[0], 2)).ToArray();
           // Console.WriteLine($"TerminalInformationContent bit arr to str    : { string.Join(",", TerminalInfo)}");

           if (!AppSettings.ModeRun) return TerminalInfo;

           Console.ForegroundColor = ConsoleColor.Cyan;
           Console.WriteLine($"GPS info                                 : {TerminalInfo[1]}");
           Console.WriteLine($"Battery status                           : {TerminalInfo[5]}");
           Console.WriteLine($"Lock status                              : {TerminalInfo[7]}");

           return TerminalInfo;
        }
        private void ClientImeiConnected(int clientNumber, string imei)
        {
            try
            {
                lock (server.workerSockets)
                {
                    string ComputerName = "Concox";
                    string ClientsName = "BL10";

                    if (server.workerSockets.ContainsKey(clientNumber))
                    {
                        server.workerSockets[clientNumber].szStationName = ComputerName;
                        server.workerSockets[clientNumber].szIMEI = imei;
                        server.workerSockets[clientNumber].szClientName = ClientsName;

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"Registered Connection ({clientNumber}) for '{imei}'");
                        }

                        Utilities.WriteOperationLog("ClientImeiConnected", $"Registered Connection (Client Number = {clientNumber}) for IMEI = {imei}");
                    }
                }//end lock

            }
            catch (Exception ex)
            {
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"EXCEPTION: PostUserCredentials on client {clientNumber}, EMEI = {imei}, exception: {ex.Message}");
                }

                Utilities.WriteErrorLog("frmServer_ClientImeiConnected", ex.ToString());
            }
        }

        private void ClientInfoConnected(int clientNumber, char[] TerminalInfo, double PercentVoltage, string imei)
        {
            try
            {
                lock (server.workerSockets)
                {
                    string ComputerName = "Concox";
                    string ClientsName = "BL10";
                    string Gps = (int)Char.GetNumericValue(TerminalInfo[0]) == 1 ? "Positioning" : "No Positioning";
                    string Battery = (int)Char.GetNumericValue(TerminalInfo[1]) == 1 ? $"{PercentVoltage}%(Charging)" : $"{PercentVoltage}%";

                    string Lock = "Unlock";

                    if ((int)Char.GetNumericValue(TerminalInfo[7]) == 1)
                    {
                        Lock = "Lock";
                    }

                    //string Lock = (int)Char.GetNumericValue(TerminalInfo[2]) == 1 ? "Lock" : "Unlock";

                    //Console.WriteLine($"info terminal: Gps {TerminalInfo[0]}");
                    //Console.WriteLine($"info terminal: Battery {TerminalInfo[1]}");
                    //Console.WriteLine($"info terminal: Lock {TerminalInfo[7]}");

                    if (AppSettings.ModeRun)
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine($"ClientNumber: {clientNumber} | IMEI: {imei} | GPS: {Gps} | Battery: {Battery} | Status: {Lock}");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("===============================================================");
                    }

                    //UpdateDockInfoIntoDb(
                    //    svr.workerSockets[clientNumber].szIMEI,
                    //    (int)Char.GetNumericValue(TerminalInfo[1]) == 1 ? true : false, // Trạng thái sạc của pin
                    //    PercentVoltage, // Phần trăm pin
                    //    (int)Char.GetNumericValue(TerminalInfo[2]) == 1 ? true : false // Trạng thái khóa đang đóng hay mở
                    //    );
                }//end lock

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("frmServer_ClientInfoConnected", ex.ToString());
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"EXCEPTION: PostUserCredentials on client {clientNumber}, exception: {ex.Message}");
                }
            }
        }

        public void ProcessMessages(byte[] receiveMessage, System.Net.Sockets.Socket userSock, int clientNumber)
        {
            UInt16 sendCRC = 0;
            byte[] packetStartBit = null;
            byte[] packetStopBit = { 0x0D, 0x0A };//fix
            byte[] packetReceiveLength = null;
            byte[] packetSendLength = null;
            byte[] packetProtocol = null;
            byte[] packetSerialNumber = null;
            byte[] packetErrorCheck = null;


            //Login packet
            byte[] login_modelCode = null;
            byte[] login_timeZone = null;
            byte[] login_imei = null;

            //Heartbeat packet
            byte[] heartbeat_terminalInformationContent = null;
            byte[] heartbeat_voltageLevel = null;
            byte[] heartbeat_gSMSignalStrength = null;
            byte[] heartbeat_Language = null;

            //Location packet
            byte[] location_DateTimeUTC = null;
            byte[] location_GPSInfoLength = null;
            byte[] location_QuantityGPSInfo = null;
            byte[] location_Latitude = null;
            byte[] location_Longitude = null;
            byte[] location_CourseStatus = null;
            byte[] location_MainBaseStation_Length = null;
            byte[] location_MCC = null;
            byte[] location_MNC = null;
            byte[] location_LAC = null;
            byte[] location_CI = null;
            byte[] location_RSSI = null;
            byte[] location_SubBaseStation_Length = null;
            byte[] location_NLAC1 = null;
            byte[] location_NLAC2 = null;
            byte[] location_NCI1 = null;
            byte[] location_NCI2 = null;
            byte[] location_NRSSI1 = null;
            byte[] location_NRSSI2 = null;
            byte[] location_WIFI_MessageLength = null;
            byte[] location_WIFI_MAC_1 = null;
            byte[] location_WIFI_MAC_2 = null;
            byte[] location_WIFI_Strength_1 = null;
            byte[] location_WIFI_Strength_2 = null;
            byte[] location_Status = null;
            byte[] location_ReservedExtensionBit_Length = null;
            byte[] location_ReservedExtensionBit = null;
            byte[] location_Speed = null;


            var imei = "";
            bool isLogin = false;
            bool isSendLock = false;

            double PercentVoltage = 0;
            DateTime lastConnectionTime = DateTime.MinValue;

            List<Char> TerminalInfoList = new List<char>();

            PROTOCOL_NUMBER protocolNumber = PROTOCOL_NUMBER.NONE;

            try
            {

                if (receiveMessage.Length < 1)
                {
                    protocolNumber = PROTOCOL_NUMBER.NONE;
                }
                else if (receiveMessage[1] == 121 || receiveMessage[1] == 0x79)
                {
                    protocolNumber = (PROTOCOL_NUMBER)receiveMessage[4];
                }
                else
                {
                    protocolNumber = (PROTOCOL_NUMBER)receiveMessage[3];
                }

                List<byte> DateTimeUTC = new List<byte>();
                DateTimeUTC.AddRange(TransformBytes(19, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Month, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Day, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Hour, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Minute, 1));
                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Second, 1));



                switch (protocolNumber)
                {
                    #region LOGIN_INFORMATION
                    case PROTOCOL_NUMBER.LOGIN_INFORMATION:

                        lastConnectionTime = DateTime.Now;

                        packetStartBit = receiveMessage.Take(2).ToArray();
                        packetReceiveLength = receiveMessage.Skip(2).Take(1).ToArray();
                        packetSendLength = new byte[] { 0x0C };
                        packetProtocol = new byte[] { 0x01 };
                        login_imei = receiveMessage.Skip(4).Take(8).ToArray();
                        login_modelCode = receiveMessage.Skip(12).Take(2).ToArray();
                        login_timeZone = receiveMessage.Skip(14).Take(2).ToArray();
                        packetSerialNumber = receiveMessage.Skip(16).Take(2).ToArray();
                        packetErrorCheck = receiveMessage.Skip(18).Take(2).ToArray();

                        byte[] loginMessageResponse = packetStartBit
                            .Concat(packetSendLength)
                            .Concat(packetProtocol)
                            .Concat(DateTimeUTC.ToArray())
                            .Concat(new byte[] { 0x00 })
                            .Concat(packetSerialNumber)
                            .Concat(packetErrorCheck)
                            .Concat(packetStopBit)
                            .ToArray();

                        byte[] checkCRC = loginMessageResponse.Skip(2).Take(11).ToArray();
                        sendCRC = Crc_bytes(checkCRC); //https://crccalc.com/ CRC-16/X-25
                        loginMessageResponse[loginMessageResponse.Length - 4] = (byte)((sendCRC >> 8) & 0xFF);
                        loginMessageResponse[loginMessageResponse.Length - 3] = (byte)((sendCRC) & 0xFF);
                        string errorCheck = BytesToString(loginMessageResponse.Skip(13).Take(2).ToArray());

                        if (AppSettings.ModeRun)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;

                            Console.WriteLine("0x78 0x78 - LOGIN_INFORMATION");

                            Console.WriteLine($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} Received Login Message Packet");
                            //Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                            //Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");

                            //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                            //Console.WriteLine($"- Packet Length                (1 byte): {BytesToString(packetReceiveLength)}");
                            //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                            Console.WriteLine($"- IMEI                         (8 byte): {BytesToString(login_imei)}");
                            //Console.WriteLine($"- Model Indentification Code   (2 byte): {BytesToString(login_modelCode)}");
                            //Console.WriteLine($"- Time Zone Language           (2 byte): {BytesToString(login_timeZone)}");
                            Console.WriteLine($"- Information Serial Number    (2 byte): {BytesToString(packetSerialNumber)}");
                            //Console.WriteLine($"- Error Check                  (2 byte): {BytesToString(packetErrorCheck)}");
                            //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                            Console.WriteLine($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} Send Login Message Response");
                            //Console.WriteLine($"Hexadecimal Value: : {BytesToString(loginMessageResponse)}");
                            Console.WriteLine($"Error Check: {BytesToString(checkCRC)} ~ {sendCRC} ~ {errorCheck}");

                            //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                            //Console.WriteLine($"- Packet Length                (1 byte): {BytesToString(packetSendLength)}");
                            //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                            //Console.WriteLine($"- DateTime (UTC)               (6 byte): {BytesToString(DateTimeUTC.ToArray())}");
                            //Console.WriteLine($"- Reserved Extension BitLength (1 byte): {BytesToString(new byte[] { 0x00 })}");
                            //Console.WriteLine($"- Information Serial Number    (2 byte): {BytesToString(packetSerialNumber)}");
                            //Console.WriteLine($"- Error Check                  (2 byte): {errorCheck}");
                            //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("===============================================================");
                        }

                        Send(userSock, loginMessageResponse);
                        isLogin = true;
                        ClientImeiConnected(clientNumber, BytesToString(login_imei));

                        break;
                    #endregion


                    #region HEARTBEAT_PACKET
                    case PROTOCOL_NUMBER.HEARTBEAT_PACKET:
                        packetStartBit = receiveMessage.Take(2).ToArray();
                        packetReceiveLength = receiveMessage.Skip(2).Take(1).ToArray();
                        packetSendLength = new byte[] { 0x05 };
                        packetProtocol = new byte[] { 0x23 };
                        heartbeat_terminalInformationContent = receiveMessage.Skip(4).Take(1).ToArray();
                        heartbeat_voltageLevel = receiveMessage.Skip(5).Take(2).ToArray();
                        heartbeat_gSMSignalStrength = receiveMessage.Skip(7).Take(1).ToArray();
                        heartbeat_Language = receiveMessage.Skip(9).Take(2).ToArray();
                        packetSerialNumber = receiveMessage.Skip(10).Take(2).ToArray();
                        packetErrorCheck = receiveMessage.Skip(12).Take(2).ToArray();

                        byte[] heartbeatMessageResponse = packetStartBit
                            .Concat(packetSendLength)
                            .Concat(packetProtocol)
                            .Concat(packetSerialNumber)
                            .Concat(packetErrorCheck)
                            .Concat(packetStopBit)
                            .ToArray();

                        byte[] checkCRC_heartbeat = heartbeatMessageResponse.Skip(2).Take(4).ToArray();
                        sendCRC = Crc_bytes(checkCRC_heartbeat); //https://crccalc.com/ CRC-16/X-25

                        heartbeatMessageResponse[heartbeatMessageResponse.Length - 4] = (byte)((sendCRC >> 8) & 0xFF);
                        heartbeatMessageResponse[heartbeatMessageResponse.Length - 3] = (byte)((sendCRC) & 0xFF);
                        string errorCheck_heartbeat = BytesToString(heartbeatMessageResponse.Skip(6).Take(2).ToArray());

                        if (AppSettings.ModeRun && server.workerSockets.TryGetValue(clientNumber, out var dataSockhb))
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            imei = dataSockhb.szIMEI;

                            Console.WriteLine("0x78 0x78 - HEARTBEAT_PACKET");
                            Console.WriteLine($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} Received Heartbeat Packet Sent By Terminal (16 bytes)");
                            Console.WriteLine($"- IMEI                                   : {dataSockhb.szIMEI}");
                            //Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                            //Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");

                            //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                            //Console.WriteLine($"- Packet Length                (1 byte): {BytesToString(packetReceiveLength)}");
                            //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                            Console.WriteLine($"- Terminal Information Content   (1 byte): {BytesToString(heartbeat_terminalInformationContent)} = {CharToString(getTerminalInformationContent(heartbeat_terminalInformationContent))}");
                            //Console.WriteLine($"- VoltageLevel                   (2 byte): {BytesToString(heartbeat_voltageLevel)} ");
                            Console.WriteLine($"- VoltageLevel                   (2 byte): {BytesToString(heartbeat_voltageLevel)} = {getPercentVoltage(heartbeat_voltageLevel)}%");
                            //Console.WriteLine($"- GSM Signal Strength          (1 byte): {BytesToString(login_timeZone)}");
                            //Console.WriteLine($"- Language                     (2 byte): {BytesToString(login_timeZone)}");
                            Console.WriteLine($"- Serial Number                  (2 byte): {BytesToString(packetSerialNumber)}");
                            //Console.WriteLine($"- Error Check                  (2 byte): {BytesToString(packetErrorCheck)}");
                            //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                            Console.WriteLine($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} Send  Server Responds The Heartbeat Packet (10 bytes)");
                            //Console.WriteLine($"Hexadecimal Value: : {BytesToString(heartbeatMessageResponse)}");
                            Console.WriteLine($"Error Check: {BytesToString(checkCRC_heartbeat)} ~ {sendCRC} ~ {errorCheck_heartbeat}");

                            //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                            //Console.WriteLine($"- Packet Length                (1 byte): {BytesToString(packetSendLength)}");
                            //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                            //Console.WriteLine($"- SerialNumber                 (1 byte): {BytesToString(packetSerialNumber)}");
                            //Console.WriteLine($"- Error Check                  (2 byte): {errorCheck_heartbeat}");
                            //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Send(userSock, heartbeatMessageResponse);
                        char[] TerminalInfo = getTerminalInformationContent(heartbeat_terminalInformationContent);
                        PercentVoltage = getPercentVoltage(heartbeat_voltageLevel);

                        TerminalInfoList = TerminalInfo.ToList();

                        ClientInfoConnected(clientNumber, TerminalInfo, PercentVoltage, imei);

                        Utilities.WriteOperationLog("[HEARTBEAT_PACKET]",$"IMEI:{imei}|TerminalInformationContent:{BytesToString(heartbeat_terminalInformationContent)} = {CharToString(getTerminalInformationContent(heartbeat_terminalInformationContent))}|VoltageLevel:{BytesToString(heartbeat_voltageLevel)} = {getPercentVoltage(heartbeat_voltageLevel)}%|LockStatus:{TerminalInfo[7]}");

                        break;
                    #endregion
                    #region GPS_LOCATION_INFORMATION

                    case PROTOCOL_NUMBER.GPS_LOCATION_INFORMATION:
                        packetStartBit = receiveMessage.Take(2).ToArray();
                        packetReceiveLength = receiveMessage.Skip(2).Take(2).ToArray();
                        packetSendLength = new byte[] { 0x0C }; //quest ?
                        packetProtocol = receiveMessage.Skip(4).Take(1).ToArray();
                        location_DateTimeUTC = receiveMessage.Skip(5).Take(6).ToArray();
                        location_GPSInfoLength = receiveMessage.Skip(11).Take(1).ToArray();
                        location_QuantityGPSInfo = receiveMessage.Skip(12).Take(1).ToArray();
                        location_Latitude = receiveMessage.Skip(13).Take(4).ToArray();
                        location_Longitude = receiveMessage.Skip(17).Take(4).ToArray();
                        location_Speed = receiveMessage.Skip(21).Take(1).ToArray();
                        location_CourseStatus = receiveMessage.Skip(22).Take(2).ToArray();
                        location_MainBaseStation_Length = receiveMessage.Skip(24).Take(1).ToArray();
                        location_MCC = receiveMessage.Skip(25).Take(2).ToArray();
                        location_MNC = receiveMessage.Skip(27).Take(1).ToArray();
                        location_LAC = receiveMessage.Skip(28).Take(2).ToArray();
                        location_CI = receiveMessage.Skip(30).Take(3).ToArray();
                        location_RSSI = receiveMessage.Skip(33).Take(1).ToArray();
                        location_SubBaseStation_Length = receiveMessage.Skip(34).Take(1).ToArray();
                        location_NLAC1 = receiveMessage.Skip(35).Take(2).ToArray();
                        location_NCI1 = receiveMessage.Skip(37).Take(3).ToArray();
                        location_NRSSI1 = receiveMessage.Skip(40).Take(1).ToArray();
                        location_NLAC2 = receiveMessage.Skip(41).Take(2).ToArray();
                        location_NCI2 = receiveMessage.Skip(43).Take(3).ToArray();
                        location_NRSSI2 = receiveMessage.Skip(46).Take(1).ToArray();
                        location_WIFI_MessageLength = receiveMessage.Skip(47).Take(1).ToArray();
                        location_WIFI_MAC_1 = receiveMessage.Skip(48).Take(6).ToArray();
                        location_WIFI_Strength_1 = receiveMessage.Skip(54).Take(1).ToArray();
                        location_WIFI_MAC_2 = receiveMessage.Skip(55).Take(6).ToArray();
                        location_WIFI_Strength_2 = receiveMessage.Skip(61).Take(1).ToArray();
                        location_Status = receiveMessage.Skip(62).Take(1).ToArray();
                        location_ReservedExtensionBit_Length = receiveMessage.Skip(63).Take(1).ToArray();
                        packetSerialNumber = receiveMessage.Skip(64).Take(2).ToArray();
                        packetErrorCheck = receiveMessage.Skip(66).Take(2).ToArray();

                        if (AppSettings.ModeRun && server.workerSockets.TryGetValue(clientNumber, out var dataSockgps))
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;

                            Console.WriteLine("0x79 0x79 - GPS_LOCATION_INFORMATION");

                            Console.WriteLine($"{DateTime.Now:dd/MM/yyyy HH:mm:ss}  Received GPS_LOCATION_INFORMATION");
                            //Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                            //Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");
                            imei = dataSockgps.szIMEI;
                            Console.WriteLine($"- IMEI                                 : {dataSockgps.szIMEI}");

                            //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                            //Console.WriteLine($"- Packet Length                (2 byte): {BytesToString(packetReceiveLength)}");
                            //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                            //Console.WriteLine($"- DateTime UTC                 (6 byte): {BytesToString(location_DateTimeUTC)} = DateTime Local: {getDateTimeLocal(location_DateTimeUTC.ToArray())}");
                            Console.WriteLine($"- GPS Info Length              (1 byte): {BytesToString(location_GPSInfoLength)}");
                            Console.WriteLine($"- Quantity GPS Info            (1 byte): {BytesToString(location_QuantityGPSInfo)}");
                            Console.WriteLine($"- Latitude                     (4 byte): {BytesToString(location_Latitude)} = {getLatitudeLongitude(location_Latitude)} ");
                            Console.WriteLine($"- Longitude                    (4 byte): {BytesToString(location_Longitude)} = {getLatitudeLongitude(location_Longitude)} ");
                            Console.WriteLine($"- Speed                        (1 byte): {BytesToString(location_Speed)}");
                            //Console.WriteLine($"- CourseStatus                 (2 byte): {BytesToString(location_CourseStatus)}");
                            //Console.WriteLine($"- Main Base Station Length     (2 byte): {BytesToString(location_MainBaseStation_Length)}");
                            //Console.WriteLine($"- MCC                          (2 byte): {BytesToString(location_MCC)}");
                            //Console.WriteLine($"- MNC                          (1 byte): {BytesToString(location_MNC)}");
                            //Console.WriteLine($"- LAC                          (2 byte): {BytesToString(location_LAC)}");
                            //Console.WriteLine($"- CI                           (3 byte): {BytesToString(location_CI)}");
                            //Console.WriteLine($"- RSSI                         (1 byte): {BytesToString(location_RSSI)}");
                            //Console.WriteLine($"- Sub BaseStationbLength       (1 byte): {BytesToString(location_SubBaseStation_Length)}");
                            //Console.WriteLine($"- NLAC1                        (2 byte): {BytesToString(location_NLAC1)}");
                            //Console.WriteLine($"- NCI1                         (3 byte): {BytesToString(location_NCI1)}");
                            //Console.WriteLine($"- NRSSI1                       (1 byte): {BytesToString(location_NRSSI1)}");
                            //Console.WriteLine($"- NLAC2                        (2 byte): {BytesToString(location_NLAC2)}");
                            //Console.WriteLine($"- NCI2                         (3 byte): {BytesToString(location_NCI2)}");
                            //Console.WriteLine($"- NRSSI2                       (1 byte): {BytesToString(location_NRSSI2)}");
                            //Console.WriteLine($"- WIFI Message Length          (1 byte): {BytesToString(location_WIFI_MessageLength)}");
                            //Console.WriteLine($"- WIFI MAC 1                   (6 byte): {BytesToString(location_WIFI_MAC_1)}");
                            //Console.WriteLine($"- WIFI Strength 1              (1 byte): {BytesToString(location_WIFI_Strength_1)}");
                            //Console.WriteLine($"- WIFI MAC 2                   (6 byte): {BytesToString(location_WIFI_MAC_2)}");
                            //Console.WriteLine($"- WIFI Strength 2              (1 byte): {BytesToString(location_WIFI_Strength_2)}");
                            //Console.WriteLine($"- Status                       (1 byte): {BytesToString(location_Status)}");
                            //Console.WriteLine($"- Reserved Extension Bit Length(1 byte): {BytesToString(location_ReservedExtensionBit_Length)}");
                            //Console.WriteLine($"- SerialNumber                 (2 byte): {BytesToString(packetSerialNumber)}");
                            //Console.WriteLine($"- ErrorCheck                   (2 byte): {BytesToString(packetErrorCheck)}");
                            //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("===============================================================");
                        }

                        var map = $"https://maps.google.com?q=*lat*,*long*";

                        Utilities.WriteOperationLog("GPS_LOCATION_INFORMATION", $@"IMEI:{imei}|GPS Info:{BytesToString(location_GPSInfoLength)}|Quantity GSP:{BytesToString(location_QuantityGPSInfo)}|Latitude:{BytesToString(location_Latitude)} = {getLatitudeLongitude(location_Latitude)}|Longitude:{BytesToString(location_Longitude)} = {getLatitudeLongitude(location_Longitude)}|Speed:{BytesToString(location_Speed)}|URL-MAP: {map.Replace("*lat*", $"{getLatitudeLongitude(location_Latitude)}").Replace("*long*", $"{getLatitudeLongitude(location_Longitude)}")}");

                        break;
                        #endregion
                }

                if (server.workerSockets.TryGetValue(clientNumber, out var dataSock))
                {
                    var dock = SqlHelper.GetAll<Dock>($"Where IMEI = '{dataSock.szIMEI}'").FirstOrDefault();

                    if (dock != null)
                    {
                        dock.UpdatedDate = DateTime.Now;

                        dock.LastConnectionTime = lastConnectionTime != DateTime.MinValue
                            ? lastConnectionTime
                            : dock.LastConnectionTime;

                        if (TerminalInfoList.Count > 6)
                        {
                            dock.LockStatus = Convert.ToBoolean(Convert.ToInt32(TerminalInfoList[7]));
                            dock.Charging = Convert.ToBoolean(Convert.ToInt32(TerminalInfoList[1]));
                        }

                        dock.Battery = PercentVoltage != 0 ? PercentVoltage : dock.Battery;

                        dock.ConnectionStatus = true;

                        dock.Lat = location_Latitude != null ? getLatitudeLongitude(location_Latitude) : dock.Lat;
                        dock.Long = location_Longitude != null ? getLatitudeLongitude(location_Longitude) : dock.Long;

                        SqlHelper.UpdateOne<Dock>(dock, dock.Id);
                    }

                    if (protocolNumber == PROTOCOL_NUMBER.GPS_LOCATION_INFORMATION)
                    {
                        SqlHelper.InsertGPS(dataSock.szIMEI, getLatitudeLongitude(location_Latitude), getLatitudeLongitude(location_Longitude));
                    }
                }
            }
            catch (Exception e)
            {
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine(e.Message);
                }

                Utilities.WriteErrorLog("[Socket.ProcessMessages]", $"[ERROR: {e}]");
            }
        }
        static byte[] TransformBytes(int num, int byteLength)
        {
            byte[] res = new byte[byteLength];

            byte[] temp = BitConverter.GetBytes(num);

            Array.Copy(temp, res, byteLength);

            return res;
        }
        static string BytesToString(byte[] bytes)
        {
            return string.Join("", bytes.Select(x => x.ToString("X2")));
        }

        static string CharToString(Char[] chars)
        {
            return string.Join("", chars.Select(x => x.ToString()));
        }

        static KeyValuePair<UNPACK_STATUS, byte[]> Unpack(KeyValuePair<List<byte>, StateObject> bitState)
        {
            List<byte> working_buffer = null;
            try
            {
                working_buffer = bitState.Key;
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"Data: {BytesToString(working_buffer.ToArray())} ; Length: {working_buffer.Count()}");
                }
                //return null indicates an error
                if (working_buffer.Count() < 3)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} Received new packet. Status: NOT ENOUGH BYTES. Total: {working_buffer.Count()} byte ");
                        Console.WriteLine("===============================================================");
                    }

                    return new KeyValuePair<UNPACK_STATUS, byte[]>(UNPACK_STATUS.NOT_ENOUGH_BYTES, null);
                }
                KeyValuePair<List<byte>, StateObject> byteState = ReadWrite(PROCESS_STATE.UNPACK, null, null, bitState.Value.connectionNumber);
                List<byte> packet = byteState.Key;

                return new KeyValuePair<UNPACK_STATUS, byte[]>(UNPACK_STATUS.GOOD_MESSAGE, packet.ToArray());
            }
            catch (Exception ex)
            {
                return new KeyValuePair<UNPACK_STATUS, byte[]>(UNPACK_STATUS.NOT_ENOUGH_BYTES, working_buffer.ToArray());
            }
        }
        public static UInt16 Crc_bytes(byte[] data)
        {
            ushort crc = 0xFFFF;

            for (int i = 0; i < data.Length; i++)
            {
                crc ^= (ushort)(Reflect(data[i], 8) << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x8000) > 0)
                        crc = (ushort)((crc << 1) ^ 0x1021);
                    else
                        crc <<= 1;
                }
            }
            crc = Reflect(crc, 16);
            crc = (ushort)~crc;
            return crc;
        }
        public static ushort Reflect(ushort data, int size)
        {
            ushort output = 0;
            for (int i = 0; i < size; i++)
            {
                int lsb = data & 0x01;
                output = (ushort)((output << 1) | lsb);
                data >>= 1;
            }
            return output;
        }
        static KeyValuePair<List<byte>, StateObject> ReadWrite(PROCESS_STATE ps, System.Net.Sockets.Socket handler, IAsyncResult ar, long unpackConnectionNumber)
        {
            KeyValuePair<List<byte>, StateObject> byteState = new KeyValuePair<List<byte>, StateObject>(); ;
            StateObject stateObject = null;
            int bytesRead = -1;
            int workingBufferLen = 0;
            List<byte> working_buffer = null;
            byte[] buffer = null;

            Object thisLock1 = new Object();

            lock (thisLock1)
            {
                switch (ps)
                {
                    case PROCESS_STATE.ACCEPT:

                        acceptDone.WaitOne();
                        acceptDone.Reset();
                        stateObject = new StateObject();
                        stateObject.buffer = new byte[BUFFER_SIZE];
                        connectionDict.Add(connectionNumber, new KeyValuePair<List<byte>, StateObject>(new List<byte>(), stateObject));
                        stateObject.connectionNumber = connectionNumber++;

                        stateObject.workSocket = handler;

                        byteState = new KeyValuePair<List<byte>, StateObject>(null, stateObject);
                        acceptDone.Set();
                        break;

                    case PROCESS_STATE.READ:
                        //catch when client disconnects

                        //wait if accept is being called
                        //acceptDone.WaitOne();
                        try
                        {
                            stateObject = ar.AsyncState as StateObject;
                            // Read data from the client socket. 
                            bytesRead = stateObject.workSocket.EndReceive(ar);

                            if (bytesRead > 0)
                            {
                                byteState = connectionDict[stateObject.connectionNumber];

                                buffer = new byte[bytesRead];
                                Array.Copy(byteState.Value.buffer, buffer, bytesRead);
                                string data = BytesToString(buffer);
                                byteState.Key.AddRange(buffer);
                            }
                            //only put one instance of connection number into fifo
                            if (!fifo.Contains(byteState.Value.connectionNumber))
                            {

                                fifo.Add(byteState.Value.connectionNumber);
                            }
                        }
                        catch (Exception ex)
                        {
                            //will get here if client disconnects
                            fifo.RemoveAll(x => x == byteState.Value.connectionNumber);
                            connectionDict.Remove(byteState.Value.connectionNumber);
                            byteState = new KeyValuePair<List<byte>, StateObject>(new List<byte>(), null);
                        }
                        break;
                    case PROCESS_STATE.PROCESS:
                        if (fifo.Count > 0)
                        {
                            //get message from working buffer
                            //unpack will later delete message
                            //remove connection number from fifo
                            // the list in the key in known as the working buffer
                            byteState = new KeyValuePair<List<byte>, StateObject>(connectionDict[fifo[0]].Key, connectionDict[fifo[0]].Value);
                            fifo.RemoveAt(0);
                            //put a valid value in fifoCount so -1 below can be detected.
                            byteState.Value.fifoCount = fifo.Count;
                        }
                        else
                        {
                            //getting here is normal when there is no more work to be performed
                            //set fifocount to zero so rest of code know fifo was empty so code waits for next receive message
                            byteState = new KeyValuePair<List<byte>, StateObject>(null, new StateObject() { fifoCount = -1 });
                        }
                        break;
                    case PROCESS_STATE.UNPACK:
                        try
                        {
                            working_buffer = connectionDict[unpackConnectionNumber].Key;

                            if (
                                (working_buffer[0] == 0x78) &&
                                (working_buffer[1] == 0x78))
                            {
                                workingBufferLen = working_buffer[2];

                            }
                            else if ((working_buffer[0] == 0x79) &&
                              (working_buffer[1] == 0x79))
                            {
                                workingBufferLen = working_buffer[2] + working_buffer[3];

                            }

                            if (AppSettings.ModeRun)
                            {
                                Console.WriteLine($"working_buffer :    {working_buffer[0]}");
                            }


                            if (
                                (working_buffer[0] != 0x78) &&
                                (working_buffer[0] != 0x79) &&
                                (working_buffer[workingBufferLen + 3] != 0x0D) &&
                                (working_buffer[workingBufferLen + 4] != 0x0A))
                            {
                                working_buffer.Clear();
                                return new KeyValuePair<List<byte>, StateObject>(new List<byte>(), null);
                            }
                            List<byte> packet = working_buffer.GetRange(0, workingBufferLen + 5);
                            working_buffer.RemoveRange(0, workingBufferLen + 5);
                            byteState = new KeyValuePair<List<byte>, StateObject>(packet, null);
                        }
                        catch (Exception ex)
                        {
                            int testPoint = 0;
                        }
                        break;

                }// end switch
            }
            return byteState;
        }
        static void Send(System.Net.Sockets.Socket socket, byte[] data)
        {
            // Convert the string data to byte data using ASCII encoding.
            // byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            socket.BeginSend(data, 0, data.Length, 0,
                new AsyncCallback(SendCallback), socket);
        }
        static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                System.Net.Sockets.Socket handler = ar.AsyncState as System.Net.Sockets.Socket;
                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                // Console.WriteLine("Sent {0} bytes to client.", bytesSent);
            }
            catch (Exception e)
            {
                // Console.WriteLine(e.ToString());
                int myerror = -1;
            }
        }
        public static void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                // Get the socket that handles the client request.
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.

                System.Net.Sockets.Socket listener = (System.Net.Sockets.Socket)ar.AsyncState;
                System.Net.Sockets.Socket handler = listener.EndAccept(ar);

                // Create the state object.
                StateObject state = ReadWrite(PROCESS_STATE.ACCEPT, handler, ar, -1).Value;

                handler.BeginReceive(state.buffer, 0, BUFFER_SIZE, 0,
                    new AsyncCallback(ReadCallback), state);
            }
            catch (Exception ex)
            {
                int myerror = -1;
            }
        }
        public static void ReadCallback(IAsyncResult ar)
        {
            try
            {
                StateObject state = ar.AsyncState as StateObject;
                System.Net.Sockets.Socket handler = state.workSocket;

                // Read data from the client socket. 
                KeyValuePair<List<byte>, StateObject> byteState = ReadWrite(PROCESS_STATE.READ, handler, ar, -1);

                if (byteState.Value != null)
                {
                    allDone.Set();
                    handler.BeginReceive(state.buffer, 0, BUFFER_SIZE, 0,
                        new AsyncCallback(ReadCallback), state);
                }
                else
                {
                    int testPoint = 0;
                }
            }
            catch (Exception ex)
            {
                int myerror = -1;
            }

            // Signal the main thread to continue.  
            allDone.Set();
        }
    }
}
