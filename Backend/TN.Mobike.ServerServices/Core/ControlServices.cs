﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using TN.Mobike.ServerServices.Data;
using TN.Mobike.ServerServices.Entity;
using TN.Mobike.ServerServices.Services;
using TN.Mobike.ServerServices.Settings;

namespace TN.Mobike.ServerServices.Core
{
    class ControlServices
    {
        private static bool check = true;
        public static void QueryDbToUnlock()
        {
            if (check)
            {
                try
                {
                    check = false;
                    var listOpenRequest = SqlHelper.GetAll<OpenLockRequest>($"Where CreatedDate > '{DateTime.Now.AddSeconds(-AppSettings.LimitTimeOpen):yyyy-MM-dd HH:mm:ss.ffffff}' AND CreatedDate < '{DateTime.Now:yyyy-MM-dd HH:mm:ss.ffffff}'");

                    if (listOpenRequest == null) return;

                    foreach (var item in listOpenRequest)
                    {
                        if (AppSettings.ModeRun)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("===============================================================");
                            Console.WriteLine(JsonConvert.SerializeObject(item));
                            Console.WriteLine("===============================================================");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        var check = Socket.Unlock(item.IMEI);

                        if (!check) continue;

                        var openLock = new OpenLockHistory(item);
                        openLock.Status = 0;

                        SqlHelper.InsertOne<OpenLockHistory>(openLock);
                        SqlHelper.DeleteRecord<OpenLockRequest>(item.Id);

                        var dock = (Dock) SqlHelper.GetOne<Dock>($"WHERE IMEI = '{item.IMEI}'");
                        if (dock != null)
                        {
                            dock.LockStatus = false;
                            SqlHelper.UpdateOne(dock, dock.Id);
                        }

                        Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"OPEN SUCCESSFUL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");
                        Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"INSERT TO OpenLockHistory SUCCESSFUL : {JsonConvert.SerializeObject(item)}");
                        Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"DELETE SUCCESSFUL FROM OpenLockRequest : {JsonConvert.SerializeObject(item)}");
                    }
                    check = true;
                }
                catch (Exception e)
                {
                    Utilities.WriteErrorLog("[ControlServices.QueryDbToUnlock]", $"[ERROR: {e}]");
                    check = true;
                }
            }
            else
            {
                return;
            }
        }

        public static void SocketForStatusLock()
        {
            Socket socket = new Socket();
            socket.Start();
        }

        public static async void ReceiverToUnlock()
        {
            SignalRHub.InitHub();

            await SignalRHub._connection.StartAsync();

            if (AppSettings.ModeRun)
            {
                Console.WriteLine("CONNECT SUCCESSFUL TO SIGNAL HUB !");
            }

            SignalRHub._connection.On<string>("DeviceReceiveMessage", (data) =>
            {
                if (AppSettings.ModeRun)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("===============================================================");
                    Console.WriteLine(data);
                    Console.WriteLine("===============================================================");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Socket.ProcessData(data);
            });

        }
    }
}
