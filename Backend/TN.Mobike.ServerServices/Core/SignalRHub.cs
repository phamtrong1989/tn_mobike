﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using CommonClassLibs;
using Dapper;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using TN.Mobike.ServerServices.Data;
using TN.Mobike.ServerServices.Entity;
using TN.Mobike.ServerServices.Services;
using TN.Mobike.ServerServices.Settings;

namespace TN.Mobike.ServerServices.Core
{
    class SignalRHub
    {
        public static HubConnection _connection;
        public static string _hubStringConnection = AppSettings.SignalRHub;

        public static void InitHub()
        {
            _connection = new HubConnectionBuilder()
                .WithUrl(_hubStringConnection, HttpTransportType.WebSockets | HttpTransportType.LongPolling)
                .WithAutomaticReconnect()
                //.ConfigureLogging(logging =>
                //{
                //    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                //    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                //})
                //.ConfigureLogging(logging =>
                //{
                //    // Log to the Console
                //    logging.AddConsole();
                    
                //    // This will set ALL logging to Debug level
                //    logging.SetMinimumLevel(LogLevel.Information);
                //    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                //    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                //})
                .Build();
            _connection.HandshakeTimeout = new TimeSpan(0, 1, 0);
            _connection.ServerTimeout = new TimeSpan(0, 1, 0);

            if (AppSettings.ModeRun)
            {
               Console.WriteLine($"INITIAL SUCCESSFUL SIGNAL HUB : {_hubStringConnection}");
            }
        }

        public static void DisconnectHub()
        {
            _connection?.StopAsync();
        }
    }
}
