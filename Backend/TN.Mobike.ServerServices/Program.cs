﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using TN.Mobike.ServerServices.Core;
using TN.Mobike.ServerServices.Data;
using TN.Mobike.ServerServices.Entity;
using TN.Mobike.ServerServices.Job;
using TN.Mobike.ServerServices.Services;
using TN.Mobike.ServerServices.Settings;
using Topshelf;

namespace TN.Mobike.ServerServices
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                TopshelfExitCode exitCode = HostFactory.Run(x =>
                {
                    x.Service<SyncServices>(s =>
                    {
                        s.ConstructUsing(name => new SyncServices());
                        s.WhenStarted(cb => cb.Start());
                        s.WhenStopped(cb => cb.Stop());
                        s.WhenShutdown(cb => cb.Stop());
                    });
                    x.RunAsLocalSystem();

                    x.SetServiceName(AppSettings.ServiceName);
                    x.SetDisplayName(AppSettings.ServiceDisplayName);
                    x.SetDescription(AppSettings.ServiceDescription);

                });
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("Program_Main", ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }

        public static async void Abc()
        {
            SignalRHub._connection.On<string>("DeviceReceiveMessage", (data) =>
            {
                Console.WriteLine(data);
            });

            await SignalRHub._connection.StartAsync();
        }

        public static bool isRetry = true;
    }
}
