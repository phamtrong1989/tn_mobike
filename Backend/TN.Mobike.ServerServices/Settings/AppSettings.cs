﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Mobike.ServerServices.Settings
{
    class AppSettings
    {
        // CONNECTION STRING
        public static string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];

        // SERVICES NAME
        public static string ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        public static string ServiceDisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"];
        public static string ServiceDescription = ConfigurationManager.AppSettings["ServiceDescription"];

        //  <!--Thời gian restart job sync file (X giây)--> 
        public static int JobSyncTimeSecond = Convert.ToInt32(ConfigurationManager.AppSettings["JobSyncTimeSecond"]);
        // <!--Thời gian restart job sync file (X phút)-->
        public static int JobSyncTimeMinute = Convert.ToInt32(ConfigurationManager.AppSettings["JobSyncTimeMinute"]);
        // <!--Thời gian restart job sync file (X giờ)-->
        public static int JobSyncTimeHour = Convert.ToInt32(ConfigurationManager.AppSettings["JobSyncTimeHour"]);

        //<!--Thời gian giới hạn để mở khóa-->
        public static int LimitTimeOpen = Convert.ToInt32(ConfigurationManager.AppSettings["LimitTimeOpen"]);

        //<!--URL connect to Hub SignalR-->
        public static string SignalRHub = ConfigurationManager.AppSettings["SignalRHub"];
        //<!--PORT nhận dữ liệu từ khóa trả về-->
        public static int PortService = Convert.ToInt32(ConfigurationManager.AppSettings["PortService"]);

        //<!--CONNECTION STRING TO MONGODB-->
        public static string ConnectionMongoDb = ConfigurationManager.AppSettings["ConnectionMongoDb"];
        //!--Tên database MongoDB-->
        public static string DatabaseNameMongo = ConfigurationManager.AppSettings["DatabaseNameMongo"];
        //<!--Tên bảng lưu lịch sử GPS MongoDb-->
        public static string TableGPSNameMongo = ConfigurationManager.AppSettings["TableGPSNameMongo"];

        //<!--MODE RUN SERVICES OR CONSOLE-->
        //<!--+==============+-->
        //<!--| 0 : SERVICES |-->
        //<!--| 1 : CONSOLE  |-->
        //<!--+==============+-->
        public static bool ModeRun = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["ModeRun"]));

        public static bool TurnOnSignalR = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnSignalR"]));
        public static bool TurnOnQueryDb = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnQueryDb"]));
        public static bool TurnOnSocket = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnSocket"]));
    }
}
