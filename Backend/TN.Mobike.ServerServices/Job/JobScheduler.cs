﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using TN.Mobike.ServerServices.Core;
using TN.Mobike.ServerServices.Data;
using TN.Mobike.ServerServices.Entity;
using TN.Mobike.ServerServices.Services;
using TN.Mobike.ServerServices.Settings;

namespace TN.Mobike.ServerServices.Job
{
    class JobScheduler
    {
        private static IScheduler scheduler;

        public static void Start()
        {
            try
            {
                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
                //Job capture frames

                IJobDetail job = JobBuilder.Create<Job>().Build();
                ITrigger restartTrigger = TriggerBuilder.Create()
                    //.StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(AppSettings.JobSyncTimeSecond)
                        .RepeatForever())
                    .Build();
                scheduler.ScheduleJob(job, restartTrigger);

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("RestartJob_Start", ex.ToString());
            }
        }

        public static void ReStart()
        {
            try
            {
                scheduler?.Shutdown();

                Thread.Sleep(1000);

                Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static void Stop()
        {
            try
            {
                scheduler?.Shutdown();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("restartJob_Stop", ex.ToString());
            }
        }
    }

    class Job : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                ControlServices.QueryDbToUnlock();
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[JOB.Excute]", $"[ERROR: {e}]");
            }
        }
    }
}
