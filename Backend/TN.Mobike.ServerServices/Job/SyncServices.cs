﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using TN.Mobike.ServerServices.Core;
using TN.Mobike.ServerServices.Data;
using TN.Mobike.ServerServices.Services;
using TN.Mobike.ServerServices.Settings;

namespace TN.Mobike.ServerServices.Job
{
    class SyncServices
    {
        public void Start()
        {
            try
            {
                Utilities.WriteDebugLog("Start", "App Started!");

                SqlHelper.OpenConnectMongoDb();

                if (AppSettings.TurnOnSocket)
                {
                    Thread threadSocketLock = new Thread(ControlServices.SocketForStatusLock);
                    threadSocketLock.Start();
                }

                if (AppSettings.TurnOnSignalR)
                {
                    Thread threadSignalR = new Thread(ControlServices.ReceiverToUnlock);
                    threadSignalR.Start();
                }

                if (AppSettings.TurnOnQueryDb)
                {
                    Thread threadScanDb = new Thread(JobScheduler.Start);
                    threadScanDb.Start();
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Start", ex.ToString());
            }
        }
        public void Stop()
        {
            try
            {
                JobScheduler.Stop();
                SignalRHub.DisconnectHub();
                Utilities.WriteDebugLog("ProgramInit_Stop", "Try to stop service...");
                Utilities.CloseLog();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Stop", ex.ToString());
            }
        }

    }
}
