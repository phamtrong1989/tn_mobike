﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MongoDB.Bson;
using MongoDB.Driver;
using TN.Mobike.ServerServices.Entity;
using TN.Mobike.ServerServices.Services;
using TN.Mobike.ServerServices.Settings;

namespace TN.Mobike.ServerServices.Data
{
    class SqlHelper
    {
        #region SQL

        public static SqlConnection GetConnection()
        {
            var cnn = AppSettings.ConnectionString;
            var sqlConnection = new SqlConnection(cnn);
            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            return sqlConnection;
        }

        public static object GetOne<T>(string where = "")
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = $"SELECT TOP(1) * FROM {typeof(T).Name} {where}";
                    var list = db.Query<T>(query).FirstOrDefault();

                    Utilities.WriteOperationLog("[ SqlHelper.GetOne ]", $"Get one record from table {typeof(T).Name} - {where} - successful !");

                    return list;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ SqlHelper.GetOne ]", $"Get one record from table {typeof(T).Name} - {where} -  fail ! : {e}");
                return null;
            }
        }

        public static List<T> GetAll<T>(string where = "")
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = $"SELECT * FROM {typeof(T).Name} {where}";
                    var list = db.Query<T>(query).ToList();

                    Utilities.WriteOperationLog("[ SqlHelper.GetAll ]", $"Get all record from table {typeof(T).Name} successful !");

                    return list;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ SqlHelper.GetAll ]", $"Get all record from table {typeof(T).Name} fail ! : {e}");
                return null;
            }
        }

        public static void UpdateOne<T>(T obj, int id)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>(id, false);
                    db.Execute(query, obj);
                }
                Utilities.WriteOperationLog("[ SqlHelper.UpdateOne ]", $"Update record table {typeof(T).Name} has id = {id} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ SqlHelper.GetAll ]", $"Update record table {typeof(T).Name} has id = {id} fail ! : {e}");
            }
        }

        public static void InsertMany<T>(List<T> list)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>(0);
                    db.Execute(query, list);
                }

                Utilities.WriteOperationLog("[ SqlHelper.InsertMany ]", $"Insert {list.Count} record to table {typeof(T).Name} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ SqlHelper.InsertMany ]", $"Insert {list.Count} record to table {typeof(T).Name}  fail ! : {e}");
            }
        }

        public static void InsertOne<T>(T obj)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>(0);
                    db.Execute(query, obj);
                }

                Utilities.WriteOperationLog("[ SqlHelper.InsertMany ]", $"Insert record to table {typeof(T).Name} successful [ {obj.ToString()} ]!");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ SqlHelper.InsertMany ]", $"Insert record to table {typeof(T).Name} fail! : {e}");
            }
        }

        public static void DeleteRecord<T>(int id)
        {
            try
            {
                using (var connection = GetConnection())
                {
                    var query = $"DELETE FROM {typeof(T).Name} WHERE id = {id}";
                    connection.Execute(query);
                }
                Utilities.WriteOperationLog("[ SqlHelper.DeleteRecord ]", $"Delete record {typeof(T).Name} has id = {id} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ SqlHelper.DeleteRecord ]", $"[ ERROR: {e} ]");
            }
        }

        #endregion

        #region Gennerate

        public static string GetQuery<T>(int id, bool isInsert = true)
        {
            var listProperties = typeof(T).GetProperties();
            if (isInsert)
            {
                var propertiesString = "";
                var propertiesValues = "";
                foreach (var prpInfo in listProperties)
                {
                    if (prpInfo == listProperties.Last())
                    {
                        propertiesString += $"{prpInfo.Name.ToLower()}";
                        propertiesValues += $"@{prpInfo.Name.ToLower()}";
                    }
                    else if (prpInfo.Name.ToLower() == "id")
                    {
                        continue;
                    }
                    else
                    {
                        propertiesString += $"{prpInfo.Name.ToLower()},";
                        propertiesValues += $"@{prpInfo.Name.ToLower()},";
                    }
                }
                return $"INSERT INTO {typeof(T).Name}({propertiesString}) VALUES ({propertiesValues})";
            }
            else
            {
                var updateValues = "";
                foreach (var prpInfo in listProperties)
                {
                    if (prpInfo == listProperties.Last())
                    {
                        updateValues += $"{prpInfo.Name.ToLower()} = @{prpInfo.Name.ToLower()}";
                    }
                    else if (prpInfo.Name.ToLower() == "id")
                    {
                        continue;
                    }
                    else
                    {
                        updateValues += $"{prpInfo.Name.ToLower()} = @{prpInfo.Name.ToLower()},";
                    }
                }
                return $"UPDATE {typeof(T).Name} SET {updateValues} WHERE id = {id}";
            }
        }

        #endregion

        #region MongoDb

        public static IMongoCollection<BsonDocument> collectionGPS = null;

        public static void OpenConnectMongoDb()
        {
            try
            {
                MongoClient dbClient = new MongoClient(AppSettings.ConnectionMongoDb);
                var database = dbClient.GetDatabase(AppSettings.DatabaseNameMongo);
                collectionGPS = database.GetCollection<BsonDocument>(AppSettings.TableGPSNameMongo);

                Utilities.WriteOperationLog("[SqlHelper.OpenConnectMongoDb]", $"OPEN CONNECT TO {AppSettings.ConnectionMongoDb} SUCCESSFUL !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[SqlHelper.OpenConnectMongoDb]", $"[ERROR: {e}]");
            }
        }

        public static void InsertGPS(string imei, double lat, double l)
        {
            try
            {
                if (collectionGPS == null)
                {
                    OpenConnectMongoDb();
                }
                else
                {
                    GPSData gps = new GPSData(imei, lat, l, DateTime.Now);
                    collectionGPS.InsertOneAsync(gps.ToBsonDocument());

                    Utilities.WriteOperationLog("[SqlHelper.InsertGPS]", $"INSERT SUCCESS GPSData: {gps.ToString()}");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[SqlHelper.InsertGPS]", $"[ERROR: {e}]");
            }
        }

        #endregion
    }
}
