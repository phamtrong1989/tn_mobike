using Microsoft.Extensions.Logging;
using TN.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;

namespace TN.API.Services
{
    public interface IStationService : IService<Station>
    {
        Task<ApiResponse<object>> Search(int projectId = 0);
        ApiResponse<object> Deeplink();
    }
    public class StationService : IStationService
    {
        private readonly ILogger _logger;
        private readonly IStationRepository _iStationRepository;
        private readonly IDockRepository _iDockRepository;
        public StationService
        (
            ILogger<StationService> logger,
            IStationRepository iStationRepository,
            IDockRepository iDockRepository
        )
        {
            _logger = logger;
            _iStationRepository = iStationRepository;
            _iDockRepository = iDockRepository;
        }

        public async Task<ApiResponse<object>> Search(int projectId = 0)
        {
            try
            {
                var stations = await _iStationRepository.SearchAsync(x=>x.Status == ESationStatus.Active && (x.ProjectId == projectId || projectId == 0));
                stations = await _iDockRepository.BindTotalDockFree(stations, 0, 10);
                var dtoStations = new List<StationPublicDTO>();
                foreach (var station in stations)
                {
                    if (station.Lat != 0 && station.Lng != 0)
                    {
                        var dtoStation = new StationPublicDTO(station);
                        dtoStations.Add(dtoStation);
                    }
                }
                dtoStations = dtoStations.OrderBy(x => x.Name).ToList();
                return new ApiResponse<object>() { Data = dtoStations, Status = true };
            }
            catch (Exception)
            {
                return ApiResponse.WithStatus(false, ResponseCode.ServerError);
            }
        }

        public ApiResponse<object> Deeplink()
        {
            return new ApiResponse<object>() { Data = "http://onelink.to/tngo", Status = true };
        }
    }
}
