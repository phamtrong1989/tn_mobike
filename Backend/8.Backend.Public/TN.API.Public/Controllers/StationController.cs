﻿using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Services;
using TN.Domain.Model;

namespace TN.API.Controllers
{

    [Route("api/[controller]")]
    public class StationController : Controller<Station, IStationService>
    {
        public StationController(IStationService service)
           : base(service)
        {
        }

        [HttpGet("Search/{projectId}")]
        [AppVerificationControlDevice]
        public async Task<object> Search(int projectId) => await Service.Search(projectId);

        [HttpGet("Deeplink")]
        [AppVerificationControlDevice]
        public object Deeplink() =>  Service.Deeplink();
    }
}
