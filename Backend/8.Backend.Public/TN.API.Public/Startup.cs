﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Text;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using TN.API.Infrastructure;
using TN.API.Infrastructure.Interfaces;
using TN.API.Infrastructure.Repositories;
using TN.API.Model.Setting;
using TN.API.Services;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Infrastructure.Repositories;
using TN.Shared;

namespace TN.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment _iWebHostEnvironment { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment iWebHostEnvironment)
        {
            Configuration = configuration;
            _iWebHostEnvironment = iWebHostEnvironment;
            Serilog.Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Warning()
            .WriteTo.RollingFile(Path.Combine(iWebHostEnvironment.ContentRootPath, "logs/log-{Date}.txt"))
            .CreateLogger();
        }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDataProtection().SetApplicationName("mobike_mobile");
            services.AddRouting();
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None;
            });
            services.AddDbContextPool<MobikeContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("TN.API.Infrastructure")));
            services.AddSignalR();
            //  services.AddDbContext<MobikeContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("TN.API.Infrastructure")), ServiceLifetime.Transient);
            //services.AddSignalR();
            services.AddHttpClient();
            // Dependency Injection - Settings
            services.Configure<BaseSetting>(Configuration.GetSection("BaseSettings"));
            services.Configure<LogSettings>(Configuration.GetSection("LogSettings"));
            services.Configure<MonoSettings>(Configuration.GetSection("MonoSettings"));
            services.Configure<ZaloSettings>(Configuration.GetSection("ZaloSettings"));
            services.Configure<List<NotificationTemp>>(Configuration.GetSection("NotificationTempSettings"));

            // Dependency Injection - Services
            services.AddScoped<IStationService, StationService>();
            // Dependency Injection - Repositories
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IDockRepository, DockRepository>();
            services.AddScoped<IBikeRepository, BikeRepository>();
            services.AddScoped<IAccountOTPRepository, AccountOTPRepository>();
            services.AddScoped<IBookingRepository,BookingRepository>();
            services.AddScoped<IWalletRepository, WalletRepository>();
            services.AddScoped<IWalletTransactionRepository, WalletTransactionRepository>();
            services.AddScoped<IFeedbackRepository, FeedbackRepository>();
            services.AddScoped<ITicketPriceRepository, TicketPriceRepository>();
            services.AddScoped<IStationRepository, StationRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<ISystemNotificationRepository, SystemNotificationRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<IOpenLockRequestRepository, OpenLockRequestRepository>();
            services.AddScoped<IOpenLockHistoryRepository, OpenLockHistoryRepository>();
            services.AddScoped<ICampaignRepository, CampaignRepository>();
            services.AddScoped<IProjectAccountRepository, ProjectAccountRepository>();
            services.AddScoped<ITicketPrepaidRepository, TicketPrepaidRepository>();
            services.AddScoped<IEmailBoxRepository, EmailBoxRepository>();
            services.AddScoped<IEmailManageRepository, EmailManageRepository>();
            services.AddScoped<IBikeReportRepository, BikeReportRepository>();
            services.AddScoped<IVoucherCodeRepository, VoucherCodeRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<ISMSRepository, SMSRepository>();
            services.AddScoped<IMongoDBRepository, MongoDBRepository>();
            services.AddScoped<IContentPageRepository, ContentPageRepository>();
            services.AddScoped<IInvestorRepository, InvestorRepository>();

            services.AddScoped<IProjectRepository, ProjectRepository>();

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal);
            services.Configure<FormOptions>(options => { options.MultipartBodyLengthLimit = 209_715_200; });

            services.AddDistributedMemoryCache();

            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddResponseCompression();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                //options.Events = new JwtBearerEvents
                //{
                //    OnMessageReceived = context =>
                //    {
                //        //try
                //        //{
                //        //    if (context.HttpContext.Request.Path.StartsWithSegments("/hubs/tngohub/negotiate"))
                //        //    {
                //        //    }
                //        //    else if (context.HttpContext.Request.Path.StartsWithSegments("/hubs/tngohub"))
                //        //    {
                //        //        var accessToken = context.Request.Query["access_token"].ToString();
                //        //        if (!string.IsNullOrEmpty(context.Request.Headers["Authorization"].ToString()))
                //        //        {
                //        //            accessToken = context.Request.Headers["Authorization"].ToString();
                //        //        }

                //        //        if (string.IsNullOrEmpty(accessToken))
                //        //        {
                //        //            context.Response.StatusCode = 401;
                //        //        }
                //        //        else
                //        //        {
                //        //            if (accessToken.StartsWith("Bearer"))
                //        //            {
                //        //                accessToken = accessToken.Replace("Bearer ", "");
                //        //            }

                //        //            var validate = TN.Utility.Function.ValidateToken(accessToken, Configuration["BaseSettings:API_ValidIssuer"], Configuration["BaseSettings:API_ValidAudience"], Configuration["BaseSettings:SecurityKey"]);
                //        //            if (validate > 0)
                //        //            {
                //        //                context.Token = accessToken;
                //        //            }
                //        //            else
                //        //            {
                //        //                context.Response.StatusCode = 401;
                //        //            }
                //        //        }
                //        //    }
                //        //}
                //        //catch
                //        //{
                //        //    context.Response.StatusCode = 200;
                //        //}
                //        return System.Threading.Tasks.Task.CompletedTask;
                //    }
                //};
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["BaseSettings:API_ValidIssuer"],
                    ValidAudience = Configuration["BaseSettings:API_ValidAudience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["BaseSettings:SecurityKey"]))
                };
            });

            try
            {
                if (FirebaseMessaging.DefaultInstance == null)
                {
                    var path = $"{_iWebHostEnvironment.ContentRootPath}/appsettings.Notification.json";
                    var defaultApp = FirebaseApp.Create(new AppOptions()
                    {
                        Credential = GoogleCredential.FromFile(path),
                    });
                    var defaultAuth = FirebaseAuth.GetAuth(defaultApp);
                    defaultAuth = FirebaseAuth.DefaultInstance;
                }
            }
            catch { }

            services.AddMemoryCache();
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en-US")
                };
                options.DefaultRequestCulture = new RequestCulture("en-US", "en-US");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            services.AddCors();

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                }
            );
            //services.AddSwaggerGen();
            //services.AddSwaggerDocumentation();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
            AppHttpContext.Services = app.ApplicationServices;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 31536000;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = "public,max-age=" + durationInSeconds;
                }
            });

            app.UseStaticFiles();
           // app.UseSwaggerDocumentation();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(builder =>
            {
                builder
                    .WithOrigins(Configuration["BaseSettings:UseCors"].Split(','))
                    .WithMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            });

            app.UseEndpoints(e =>
            {
                e.MapControllers();
            });
        }
    }
}
