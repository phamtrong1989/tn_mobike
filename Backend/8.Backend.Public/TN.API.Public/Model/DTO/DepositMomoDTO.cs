﻿
using Newtonsoft.Json;
using System;
using TN.Utility;

namespace TN.Backend.Model.Command
{
    public class DepositMomoDTO
    {
        public string Action { get; set; }
        public string Partner { get; set; }
        public string AppScheme { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public string MerchantCode { get; set; }
        public string MerchantName { get; set; }
        public string MerchantNameLabel { get; set; }
        public string Language { get; set; }
        public int Fee { get; set; }
        public string Username { get; set; }
        public string OrderId { get; set; }
        public string OrderLabel { get; set; }
        public string Extra { get; set; }
    }

    public class MomoConfirmCommand
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string Phonenumber { get; set; }
        public string Data { get; set; }
        public string OrderId { get; set; }
    }

    public class TNConfirmMonoCommand
    {
        [JsonProperty(PropertyName = "partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty(PropertyName = "partnerRefId")]
        public string PartnerRefId { get; set; }

        [JsonProperty(PropertyName = "customerNumber")]
        public string CustomerNumber { get; set; }

        [JsonProperty(PropertyName = "appData")]
        public string AppData { get; set; }

        [JsonProperty(PropertyName = "hash")]
        public string Hash { get; set; }

        [JsonProperty(PropertyName = "version")]
        public double Version { get; set; } = 2;

        [JsonProperty(PropertyName = "payType")]
        public int PayType { get; set; } = 3;

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "extraData")]
        public string ExtraData { get; set; }
    }

    public class JsonMonoCommand
    {
        [JsonProperty(PropertyName = "partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty(PropertyName = "partnerRefId")]
        public string PartnerRefId { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "partnerName")]
        public string PartnerName { get;  set; }

        [JsonProperty(PropertyName = "partnerTransId")]
        public string PartnerTransId { get;  set; }
        //public string StoreId { get;  set; }
        //public string StoreName { get;  set; }
    }

    public class MomoHangingMoneyDTO
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public decimal Amount { get; set; }
        public string Signature { get; set; }
        public string Transid { get; set; }
    }

    public class MomoNotifyDataDTO
    {
        public string PartnerCode { get; set; }
        public string AccessKey { get; set; }
        public decimal Amount { get; set; }
        public string PartnerRefId { get; set; }
        public string PartnerTransId { get; set; }
        public string TransType { get; set; }
        public string MomoTransId { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public long ResponseTime { get; set; }
        public string StoreId { get; set; }
        public string Signature { get; set; }
    }

    public class MomoNotifyConfirmCommand
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public decimal Amount { get; set; }
        public string PartnerRefId { get; set; }
        public string MomoTransId { get; set; }
        public string Signature { get; set; }

        public MomoNotifyConfirmCommand(MomoNotifyDataDTO outData, string key)
        {
            Status = outData.Status;
            Message = outData.Message;
            Amount = outData.Amount;
            MomoTransId = outData.MomoTransId;
            PartnerRefId = outData.PartnerRefId;
            Signature = Function.SignSHA256($"status={outData.Status}&message={outData.Message}&amount={Convert.ToInt64(outData.Amount)}&momoTransId={outData.MomoTransId}&partnerRefId={outData.PartnerRefId}", key);
        }
    }

    public class MomoPadCommand
    {
        [JsonProperty(PropertyName = "partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty(PropertyName = "partnerRefId")]
        public string PartnerRefId { get; set; }

        [JsonProperty(PropertyName = "requestType")]
        public string RequestType { get; set; }

        [JsonProperty(PropertyName = "requestId")]
        public string RequestId { get; set; }

        [JsonProperty(PropertyName = "momoTransId")]
        public string MomoTransId { get; set; }

        [JsonProperty(PropertyName = "signature")]
        public string Signature { get; set; }

        public MomoPadCommand(string partnerCode, string partnerRefId, string requestType, string requestId, string momoTransId, string key)
        {
            PartnerCode = partnerCode;
            PartnerRefId = partnerRefId;
            RequestType = requestType;
            RequestId = requestId;
            MomoTransId = momoTransId;
            var path = $"partnerCode={PartnerCode}&partnerRefId={PartnerRefId}&requestType={RequestType}&requestId={RequestId}&momoTransId={MomoTransId}";
            Signature = Function.SignSHA256(path, key);
        }
    }

    public class MomoPadDataDTO
    {
        public string PartnerCode { get; set; }
        public string MomoTransId { get; set; }
        public decimal Amount { get; set; }
        public string PartnerRefId { get; set; }
    }  
    
    public class MomoPadDTO
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string Signature { get; set; }
        public MomoPadDataDTO Data { get; set; }
    }

    public class MomoQueryStatusCommand
    {
        [JsonProperty(PropertyName = "partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty(PropertyName = "partnerRefId")]
        public string PartnerRefId { get; set; }

        [JsonProperty(PropertyName = "hash")]
        public string Hash { get; set; }

        [JsonProperty(PropertyName = "version")]
        public double Version { get; set; }

        [JsonProperty(PropertyName = "momoTransId")]
        public string MomoTransId { get; set; }
    }

    public class MomoQueryStatusHasCommand
    {
        [JsonProperty(PropertyName = "requestId")]
        public string RequestId { get; set; }

        [JsonProperty(PropertyName = "partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty(PropertyName = "partnerRefId")]
        public string PartnerRefId { get; set; }

        [JsonProperty(PropertyName = "momoTransId")]
        public string MomoTransId { get; set; }
    }

    public class MomoRefundCommand
    {
        [JsonProperty(PropertyName = "partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty(PropertyName = "requestId")]
        public string RequestId { get; set; }

        [JsonProperty(PropertyName = "hash")]
        public string Hash { get; set; }

        [JsonProperty(PropertyName = "version")]
        public double Version { get; set; }
    }

    public class MomoRefundHashCommand
    {
        [JsonProperty(PropertyName = "partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty(PropertyName = "partnerRefId")]
        public string PartnerRefId { get; set; }

        [JsonProperty(PropertyName = "momoTransId")]
        public string MomoTransId { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }
}
