﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Utility;
using static TN.Domain.Model.Bike;

namespace TN.API.Model
{
    public class SelectStationBikeDTO
    {
        public int Id { get; set; }
        public string Plate { get; set; }
        public string SerialNumber { get; set; }
        public EBikeStatus Status { get; set; }
        public EBikeType Type { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
        public int DockId { get; set; }
        public bool AdjacentDock { get; set; }
        public int Distance { get; set; }
        public SelectStationDockDTO Dock { get; set; }
        public List<TicketPriceBindModel> TicketPriceBinds { get; set; }
        public TicketPriceBindModel TicketPriceBind { get; set; }

        public SelectStationBikeDTO(Bike bike, Dock dock)
        {
            if(bike!=null)
            {
                Id = bike.Id;
                Plate = bike.Plate;
                SerialNumber = bike.SerialNumber;
                Status = bike.Status;
                Type = bike.Type;
                DockId = bike.DockId;
            }    

            if(dock != null)
            {
                Dock = new SelectStationDockDTO(dock);
                Lat = Dock?.Lat;
                Long = Dock?.Long;
            }    
        }

        public SelectStationBikeDTO(Bike bike, Dock dock, List<TicketPriceBindModel> ticketPriceBinds, int distance, int distanceAllow)
        {
            Id = bike.Id;
            Plate = bike.Plate;
            SerialNumber = bike.SerialNumber;
            Status = bike.Status;
            Type = bike.Type;
            DockId = bike.DockId;
            Dock = new SelectStationDockDTO(dock);
            Lat = Dock?.Lat;
            Long = Dock?.Long;
            TicketPriceBinds = ticketPriceBinds;
            Distance = distance;
            AdjacentDock = distance >= distanceAllow;
        }

        public SelectStationBikeDTO(Bike bike, Dock dock, TicketPriceBindModel ticketPriceBind, int distance, int distanceAllow)
        {
            Id = bike.Id;
            Plate = bike.Plate;
            SerialNumber = bike.SerialNumber;
            Status = bike.Status;
            Type = bike.Type;
            DockId = bike.DockId;
            Dock = new SelectStationDockDTO(dock);
            Lat = Dock?.Lat;
            Long = Dock?.Long;
            TicketPriceBind = ticketPriceBind;
            Distance = distance;
            AdjacentDock = distance >= distanceAllow;
        }
    }

    public class TicketPrepaidDTO
    {
        public TicketPrepaidDTO(TicketPrepaid obj,string projectName, string customerGroupName)
        {
            Id = obj.Id;
            StartDate = obj.StartDate;
            EndDate = obj.EndDate;
            TicketPriceId = obj.TicketPriceId;
            TicketValue = obj.TicketValue;
            Note = obj.Note;
            DateOfPayment = obj.DateOfPayment;
            MinutesSpent = obj.MinutesSpent;
            ProjectName = projectName;
            CustomerGroupName = customerGroupName;
            TicketTypeName = $"{customerGroupName} - {obj.TicketPrice_TicketType.GetDisplayName()} {Function.FormatMoney(obj.TicketValue)} điểm";
        }

        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TicketPriceId { get; set; }
        public decimal TicketValue { get; set; }
        public string Note { get; set; }
        public DateTime DateOfPayment { get; set; }
        public int MinutesSpent { get; set; }
        public string TicketTypeName { get; set; }
        public string CustomerGroupName { get; set; }
        public string ProjectName { get; set; }
    }

    public class ProjectViewDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TicketPriceDTO> TicketPrices { get; set; }
    }

    public class ProjectPostDTO
    {
        public string ProjectName { get; set; }
        public string UserInfo { get; set; }
        public string TicketName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Wallet_Balance { get; set; }
        public decimal Wallet_SubBalance { get; set; }
    }

    public class TicketPriceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal TicketValue { get;  set; }
        public DateTime StartDate { get;  set; }
        public DateTime EndDate { get;  set; }
        public string Note { get;  set; }
        public bool IsExist { get; set; }
        public string ExistNote { get;  set; }
    }

    public class ProjectBuyCommand
    {
        [Required]
        public int TicketPriceId { get; set; }
        [Required]
        public string TicketPriceName { get; set; }
        public string Note { get; set; }
    }

    public class WalletTransactionShareDTO
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public EWalletTransactionType Type { get; set; }
        public decimal Amount { get; set; }
        public decimal SubAmount { get; set; }
        public string GiftFromAccount { get; set; }
        public string GiftToAccount { get; set; }
        public int GiftFromAccountId { get;  set; }
        public int GiftToAccountId { get;  set; }
        public string GiftToAccountPhone { get;  set; }
        public string GiftToAccountFullName { get;  set; }
        public EAccountVerifyStatus? GiftToAccountVerifyStatus { get;  set; }
        public int PointType { get;  set; }
    }

    public class WalletTransactionGiftCommand
    {
        // 0: điểm, 1: điểm km
        [Required]
        public int PointType { get; set; }

        [Required]
        public int GiftToAccountId { get; set; }

        [Required, Range(0, 99999999)]
        public decimal Amount { get; set; }
    }

    public class WalletTransactionGiftDTOCommand
    {
        public int Id { get; set; }
        public decimal Wallet_Balance { get; set; }
        public decimal Wallet_SubBalance { get; set; }
        public decimal Amount { get; set; }
        public string FullName { get;  set; }
        public string Phone { get;  set; }
    }
}
