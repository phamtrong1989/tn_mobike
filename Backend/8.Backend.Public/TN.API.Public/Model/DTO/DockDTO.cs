﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using static TN.Domain.Model.Bike;

namespace TN.API.Model
{
    public enum EBatteryStatus
    {
        KhongTot = 1,
        Tot,
        RatTot
    }

    public class SelectStationDockDTO
    {
        public int Id { get; set; }
        public string IMEI { get; set; }
        public string SerialNumber { get; set; }
        public EBikeStatus Status { get; set; }
        public bool LockStatus { get; set; }
        public double Battery { get; set; }
        public bool Charging { get; set; }
        public bool ConnectionStatus { get; set; }
        public DateTime? LastConnectionTime { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
        public EBatteryStatus BatteryStatus { get; set; }

        public SelectStationDockDTO(Dock dock)
        {
            Id = dock.Id;
            IMEI = dock.IMEI;
            SerialNumber = dock.SerialNumber;
            Status = dock.Status;
            LockStatus = dock.LockStatus;
            Battery = dock.Battery;
            Charging = dock.Charging;
            ConnectionStatus = dock.ConnectionStatus;
            LastConnectionTime = dock.LastConnectionTime;
            Lat = dock.Lat;
            Long = dock.Long;
            if(Battery < 10)
            {
                // Không khả dụng
                BatteryStatus = EBatteryStatus.KhongTot;
            }
            else
            {
                // Khả dụng
                BatteryStatus = EBatteryStatus.Tot;
            }
            //else
            //{
            //    BatteryStatus = EBatteryStatus.RatTot;
            //}
        }
    }
}
