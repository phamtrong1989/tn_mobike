﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TN.Backend.Model.Command;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Model
{
    public class StationDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("totalDock")]
        public int TotalDock { get; set; }

        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lng")]
        public double Lng { get; set; }

        [JsonProperty("distance")]
        public int Distance { get; set; }

        [JsonProperty("totalFreeDock")]
        public int TotalFreeDock { get; set; }

        [JsonProperty("totalParkingSpaces")]
        public int TotalParkingSpaces { get; set; }

        [JsonProperty("distanceStr")]
        public string DistanceStr { get; set; }

        [JsonProperty("returnDistance")]
        public double ReturnDistance { get; set; }

        public StationDTO()
        {

        }
        public StationDTO(Station station, int distance = 0)
        {
            if (station == null)
            {
                return;
            }
            Id = station.Id;
            Name = string.IsNullOrEmpty(station.DisplayName) ? station.Name : station.DisplayName;
            Address = station.Address;
            TotalDock = station.TotalDock;
            Lat = station.Lat;
            Lng = station.Lng;
            Distance = distance;
            TotalFreeDock = station.TotalDock;
            TotalParkingSpaces = (station.Spaces - station.TotalDock < 0) ? 0 : (station.Spaces - station.TotalDock);
            if(TotalParkingSpaces <= 0)
            {
                TotalParkingSpaces = 0;
            }    
            DistanceStr = Function.DistanceString(distance);
            ReturnDistance = station.ReturnDistance;
        }
    }

    public class SelectStationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Distance { get; set; }
        public decimal WalletBalance { get; set; }
        public decimal WalletSubBalance { get; set; }
        public int TotalFreeDock { get; internal set; }
        public int TotalParkingSpaces { get; set; }
        public List<SelectStationBikeDTO> Bikes { get;  set; }
        public SelectStationBikeDTO Bike { get; set; }
        public string DistanceStr { get; set; }
        public bool InSession { get; set; }

        public SelectStationDTO(Station station, int distance = 0)
        {
            if (station == null)
            {
                return;
            }

            Id = station.Id;
            Name = station.DisplayName ?? station.Name;
            Address = station.Address;
            Lat = station.Lat;
            Lng = station.Lng;
            Distance = distance;
            DistanceStr = Function.DistanceString(distance);
        }
    }

    public class StationPublicDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lng")]
        public double Lng { get; set; }

        [JsonProperty("totalBike")]
        public int TotalBike { get; set; }

        [JsonProperty("space")]
        public int Space { get; set; }

        [JsonProperty("deeplink")]
        public string Deeplink { get; set; }

        public StationPublicDTO()
        {
        }

        public StationPublicDTO(Station station)
        {
            if (station == null)
            {
                return;
            }
            Id = station.Id;
            Name = string.IsNullOrEmpty(station.DisplayName) ? station.Name : station.DisplayName;
            Address = station.Address;
            Lat = station.Lat;
            Lng = station.Lng;
            TotalBike = station.TotalDock;
            Space = (station.Spaces - station.TotalDock < 0) ? 0 : (station.Spaces - station.TotalDock);
            Deeplink = $"tngo://station/{station.Id}";
            if (Space <= 0)
            {
                Space = 0;
            }
        }
    }
}
