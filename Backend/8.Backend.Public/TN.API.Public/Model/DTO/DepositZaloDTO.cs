﻿
using Newtonsoft.Json;
using System;
using TN.Utility;

namespace TN.Backend.Model.Command
{
    public class ZaloItemDTO
    {
        [JsonProperty(PropertyName = "itemid")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "itemname")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "itemquantity")]
        public int Quantity { get; set; }

        [JsonProperty(PropertyName = "itemprice")]
        public decimal Price { get; set; }
    }

    public class ZaloColumnInfoDTO
    {
        [JsonProperty(PropertyName = "accountId")]
        public int AccountId { get; set; }

        [JsonProperty(PropertyName = "promotioninfo")]
        public string PromotionInfo { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "orderidref")]
        public string OrderIdRef { get; set; }
    }

    public class ZaloPromotionInfoDTO
    {
        [JsonProperty(PropertyName = "campaigncode")]
        public string CampaignCode { get; set; }
    }

    public class DepositZaloEmbedDataDTO
    {
        [JsonProperty(PropertyName = "redirecturl")]
        public string Redirecturl { get; set; }

        [JsonProperty(PropertyName = "zlppaymentid")]
        public string ZlppaymentId { get; set; }

        [JsonProperty(PropertyName = "columninfo")]
        public string ColumnInfo { get; set; }
    }

    public class DepositZaloReturnDTO
    {

        [JsonProperty(PropertyName = "return_code")]
        public int ReturnCode { get; set; }

        [JsonProperty(PropertyName = "return_message")]
        public string ReturnMessage { get; set; }

        [JsonProperty(PropertyName = "sub_return_code")]
        public int SubReturnCode { get; set; }

        [JsonProperty(PropertyName = "sub_return_message")]
        public string SubReturnMessage { get; set; }

        [JsonProperty(PropertyName = "order_url")]
        public string OrderUrl { get; set; }

        [JsonProperty(PropertyName = "order_token")]
        public string OrderToken { get; set; }

        [JsonProperty(PropertyName = "zp_trans_token")]
        public string ZPTransToken { get; set; }

        [JsonProperty(PropertyName = "appid")]
        public string AppId { get; set; }
    }

    public class ZaloCallbackNotifyDTO
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "mac")]
        public string Mac { get; set; }

        [JsonProperty(PropertyName = "data")]
        public string Data { get; set; }
    }

    public class ZaloCallbackNotifyDataDTO
    {
        [JsonProperty(PropertyName = "app_id")]
        public string AppId { get; set; }

        [JsonProperty(PropertyName = "app_trans_id")]
        public string AppTransId { get; set; }

        [JsonProperty(PropertyName = "app_time")]
        public long AppTime { get; set; }

        [JsonProperty(PropertyName = "app_user")]
        public string AppUser { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "embeddata")]
        public string EmbedData { get; set; }

        [JsonProperty(PropertyName = "item")]
        public string Item { get; set; }

        [JsonProperty(PropertyName = "zp_trans_id")]
        public long ZpTransId { get; set; }

        [JsonProperty(PropertyName = "server_time")]
        public long ServerTime { get; set; }

        [JsonProperty(PropertyName = "channel")]
        public int Channel { get; set; }

        [JsonProperty(PropertyName = "merchant_user_id")]
        public string MerchantUserId { get; set; }

        [JsonProperty(PropertyName = "user_fee_amount")]
        public long UserFeeAmount { get; set; }

        [JsonProperty(PropertyName = "discount_amount")]
        public long DiscountAmount { get; set; }
    }

    public class ZaloCallbackNotifyConfirmDTO
    {
        //1: thành công
        //2: trùng mã giao dịch ZaloPay zptransid hoặc app_trans_id ( đã cung cấp dịch vụ cho user trước đó)
        [JsonProperty(PropertyName = "return_code")]
        public int ReturnCode { get; set; }

        [JsonProperty(PropertyName = "return_message")]
        public string ReturnMessage { get; set; }
    }

    public class UserZaloConfirmCommand
    {
        // 1: thành công
        // 2: thất bại
        // 3: cancel
        public int ReturnCode { get; set; }
        public string AppTransId { get; set; }
        public string TransactionId { get; set; }
        public string ZPTranstoken { get; set; }
    }

    public class ZaloReCheckOderDTO
    {

        [JsonProperty(PropertyName = "return_code")]
        public int ReturnCode { get; set; }

        [JsonProperty(PropertyName = "return_message")]
        public string ReturnMessage { get; set; }

        [JsonProperty(PropertyName = "sub_return_code")]
        public int SubReturnCode { get; set; }

        [JsonProperty(PropertyName = "sub_return_message")]
        public string SubReturnMessage { get; set; }

        [JsonProperty(PropertyName = "is_processing")]
        public bool IsProcessing { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "zp_trans_id")]
        public long ZpTransId { get; set; }
    }
}
