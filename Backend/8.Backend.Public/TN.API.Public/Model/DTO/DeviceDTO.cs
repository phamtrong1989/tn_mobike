﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Model
{
    public class DeviceDTO
    {
        public string No { get; set; }
        public string IMEI { get; set; }
        public string MAC { get; set; }
    }
}
