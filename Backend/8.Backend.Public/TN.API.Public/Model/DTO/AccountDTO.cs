﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Model
{
    public class AccountDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public ESex Sex { get; set; }
        public string FullName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Code { get; set; }
        public EAccountStatus Status { get; set; }
        public EAccountType Type { get; set; }
        public string Token { get; set; }
        public string ShareCode { get; set; }
        public EAccountIdentificationType IdentificationType { get; set; } = EAccountIdentificationType.CMTND;
        public string IdentificationID { get; set; }
        public string IdentificationPhotoFront { get; set; }
        public string IdentificationPhotoBackside { get; set; }
        public EAccountVerifyStatus VerifyStatus { get; set; }
        public string VerifyNote { get; set; }
        public string DeviceId { get; set; }

        public AccountDTO(Account account)
        {
            Id = account.Id;
            Username = account.Username;
            Avatar = account.Avatar;
            Email = account.Email;
            Sex = account.Sex;
            FullName = account.FullName;
            Birthday = account.Birthday;
            Phone = account.Phone;
            Address = account.Address;
            Status = account.Status;
            Type = account.Type;
            Code = account.Code;
            ShareCode = account.ShareCode;
            IdentificationType = account.IdentificationType;
            IdentificationID = account.IdentificationID;
            IdentificationPhotoFront = account.IdentificationPhotoFront;
            IdentificationPhotoBackside = account.IdentificationPhotoBackside;
            VerifyStatus = account.VerifyStatus;
            VerifyNote = account.VerifyNote;
            Token = account.MobileToken;
        }

        public AccountDTO(Account account, string token, string deviceId)
        {
            Id = account.Id;
            Username = account.Username;
            Avatar = account.Avatar;
            Email = account.Email;
            Sex = account.Sex;
            FullName = account.FullName;
            Birthday = account.Birthday;
            Phone = account.Phone;
            Address = account.Address;
            Status = account.Status;
            Type = account.Type;
            Token = token;
            Code = account.Code;
            ShareCode = account.ShareCode;
            IdentificationType = account.IdentificationType;
            IdentificationID = account.IdentificationID;
            IdentificationPhotoFront = account.IdentificationPhotoFront;
            IdentificationPhotoBackside = account.IdentificationPhotoBackside;
            VerifyStatus = account.VerifyStatus;
            VerifyNote = account.VerifyNote;
            DeviceId = deviceId;
        }
    }
}
