﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class BookingCurrentDTO
    {
        public int Id { get; set; }
        public string TransactionCode { get; set; }
        public int BikeId { get; set; }
        public int DockId { get; set; }
        public int TotalMinutes { get; set; }
        public int TotalSeconds { get; set; }

        public decimal TripPoint { get; set; }

        public decimal TotalPrice { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? EndDate { get; set; }
        public EBookingStatus Status { get; set; }
        public SelectStationBikeDTO Bike { get; set; }
        public List<GPSData> GPS { get; set; }

        public BookingCurrentDTO(Transaction use, List<GPSData> gps)
        {
            Id = use.Id;
            BikeId = use.BikeId;
            DockId = use.DockId;
            TransactionCode = use.TransactionCode;
            StartDate = use.EndTime;
            CreatedDate = use.CreatedDate;
            Status = use.Status;
            TotalMinutes = Convert.ToInt32((DateTime.Now - use.StartTime).TotalMinutes);
            TotalMinutes = Convert.ToInt32((DateTime.Now - use.StartTime).TotalSeconds);
            TotalPrice = use.TotalPrice;
            TripPoint = use.TripPoint;
            EndDate = use.EndTime;
            GPS = gps;
        }

        public BookingCurrentDTO(Booking use)
        {
            Id = use.Id;
            BikeId = use.BikeId;
            DockId = use.DockId;
            TransactionCode = use.TransactionCode;
            StartDate = use.EndTime;
            CreatedDate = use.CreatedDate;
            Status = use.Status;
            TotalMinutes = Convert.ToInt32((DateTime.Now - use.StartTime).TotalMinutes);
            TotalMinutes = Convert.ToInt32((DateTime.Now - use.StartTime).TotalSeconds);
            TotalPrice = use.TotalPrice;
            TripPoint = use.TripPoint;
            GPS = new List<GPSData>();
        }

        public BookingCurrentDTO(Booking use, decimal totalPrice, SelectStationBikeDTO bike, List<GPSData> gsp)
        {
            Id = use.Id;
            BikeId = use.BikeId;
            DockId = use.DockId;
            TransactionCode = use.TransactionCode;
            StartDate = use.EndTime;
            CreatedDate = use.CreatedDate;
            Status = use.Status;
            TotalMinutes = Convert.ToInt32((DateTime.Now - use.StartTime).TotalMinutes);
            TotalMinutes = Convert.ToInt32((DateTime.Now - use.StartTime).TotalSeconds);
            TotalPrice = totalPrice;
            Bike = bike;
            GPS = gsp;
            TripPoint = use.TripPoint;
        }
    }
}
