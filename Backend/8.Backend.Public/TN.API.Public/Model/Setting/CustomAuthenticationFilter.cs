﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System;

namespace ActionFilters.Filters
{
    public class AppVerificationAttribute : TypeFilterAttribute
    {
        public AppVerificationAttribute() : base(typeof(AppVerificationFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class AppVerificationFilter : Attribute, IAuthorizationFilter
    {
        private readonly TN.API.Model.Setting.BaseSetting _baseSettings;
        public AppVerificationFilter(IOptions<TN.API.Model.Setting.BaseSetting> baseSettings)
        {
            _baseSettings = baseSettings.Value;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var referer = context.HttpContext.Request.Headers["Referer"].ToString();
            if (referer != _baseSettings.AllowReferer)
            {
               // context.Result = new UnauthorizedResult();
            }
        }
    }

    public class AppVerificationControlDeviceAttribute : TypeFilterAttribute
    {
        public AppVerificationControlDeviceAttribute() : base(typeof(AppVerificationControlDeviceFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class AppVerificationControlDeviceFilter : Attribute, IAuthorizationFilter
    {
        private readonly TN.API.Model.Setting.BaseSetting _baseSettings;
        public AppVerificationControlDeviceFilter(IOptions<TN.API.Model.Setting.BaseSetting> baseSettings)
        {
            _baseSettings = baseSettings.Value;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var toke = context.HttpContext.Request.Headers["Authorization"].ToString();
            if(toke != _baseSettings.TokenEventDock)
            {
                context.Result = new UnauthorizedResult();
            }    
        }
    }
}