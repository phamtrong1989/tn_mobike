﻿
using System;

namespace TN.API.Model.Setting
{
    public class BaseSetting
    {
        public string ApiStationSocket { get; set; }
        public string ApiUnlockDevice { get; set; }
        public string ApiNotification { get; set; }
        public string ApiNotificationSysCode { get; set; }
        public int ApiNumberRetry { get; set; }
        public int ApiRequestTimeout { get; set; }
        public string SecurityKey { get; set; }
        public int JwtTokenExpiredMinute { get; set; }
        public string MobikeServiceId { get; set; }
        public string FolderMobikeAvatarWeb { get; set; }
        public string FolderMobikeAvatarServer { get; set; }

        public int OTPInvalidMax { get; set; }
        public int OTPRegiterExpiredMinute { get; set; }
        public int OTPLoginExpiredMinute { get; set; }
        public int ZipAvatarWidth { get; set; }

        public string AddressAPIPayment { get; set; }
        public string SmartContract { get; set; }
        public string MerchantCode { get; set; }
        public string API_ValidIssuer { get; set; }
        public string API_ValidAudience { get; set; }
        public string API_OpenDock { get; set; }
        public int BikeReturnDistance { get; set; }
        public int StationReturnDistance { get; set; }
        public int NumRetryOpenDock { get; set; }
        public int NumRetryCloseDock { get; set; }
        public decimal ShareCodePrice { get; set; }
        public decimal OwnerShareCodePrice { get; set; }
        public bool IsDev { get; set; }
        public bool IsOpenCloseInStation { get; set; }

        public string SMS_Path { get; set; }
        public string SMS_Token { get; set; }
        public string SMS_Username { get; set; }
        public string SMS_Password { get; set; }
        public string SMS_Brandname { get; set; }
        public string SMS_ContentOTP { get; set; }
        public int OTPTimeOut { get; set; }
        public int MinBattery { get; set; }
        public string SessionStartTime { get; set; }
        public string SessionEndTime { get; set; }
        public int CancelForTime { get; set; }
        public int AddTripPoint { get; set; }
        public string RealtimeDatabaseAPIKey { get; set; }
        public string RealtimeDatabaseAPIPath { get; set; }
        public int DockProcessAllowTime { get; set; }
        public string TransactionSecretKey { get; set; }
        public string TokenEventDock { get; set; }
        public string AllowReferer { get; set; }
        public decimal MinGiftPoint { get; set; }
        public double InStationDelta { get; set; }
        public int NonServiceDistance { get; set; }

        public DateTime ShareCodeStartDate { get; set; }
        public DateTime ShareCodeEndDate { get; set; }
        public decimal MinTotalDepositAmount { get; set; }
        public decimal MinTotalStartBooking { get; set; }
        public string ShareCodeInfo { get; set; }
        public double WarningOutRangeDeta { get; set; }

    }
}