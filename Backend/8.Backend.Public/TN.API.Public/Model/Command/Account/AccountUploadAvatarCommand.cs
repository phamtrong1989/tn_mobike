﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountUploadAvatarCommand
    {
        [Required]
        public byte[] AvatarData { get; set; }
    }
}
