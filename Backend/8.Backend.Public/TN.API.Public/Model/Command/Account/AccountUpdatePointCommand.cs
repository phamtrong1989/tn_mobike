﻿using System;
using System.ComponentModel.DataAnnotations;
using static TN.Domain.Model.Account;

namespace TN.API.Model
{
    public class AccountUpdatePointCommand
    {
        public double Distance { get; set; }
    }
}
