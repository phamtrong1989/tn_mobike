﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class PaymentDepositCommand
    {
        public int Accountid { get; set; }
        public string Cmd { get; set; }
        public string Merchantcode { get; set; }
        [Required]
        [Range(10000, 10000000)]
        public int Total { get; set; }
        [Required]
        public string Currency { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
    }


}
