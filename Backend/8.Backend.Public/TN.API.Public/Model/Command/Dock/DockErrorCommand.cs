﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using static TN.Domain.Model.Bike;

namespace TN.API.Model
{
    public class DockErrorCommand
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public EBikeStatus Status { get; set; }
    }
}
