﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Backend.Model.Command
{
    public class BookingCancelCommand
    {
        public int BookingId { get; set; }
        public string Feedback { get; set; }
    }
}
