﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Domain.Model;
using TN.Mobike.Controls;
using TN.Report.Code;

namespace TN.Report
{
    public partial class Main : Form
    {
        public static MongoDBSettings MongoDBSettings;

        public static List<GPSDataMini> PasteurZone;

        public Main()
        {
            InitializeComponent();
        }

        private async void BtnExport_Click(object sender, EventArgs e)
        {

            if ((PickerEnd.Value - PickerStart.Value).TotalDays > 31)
            {
                MessageBox.Show("Chỉ xuất tối đa 31 ngày", "Thông báo");
            }
            var newList = new List<DetailReportModle>();
            var totalDay = (PickerEnd.Value.Date - PickerStart.Value.Date).TotalDays;
            for (int i = 0; i <= totalDay; i++)
            {
                int crossRoadTotal = 0;
                var date = PickerStart.Value.Date.AddDays(i);
                LblInfo.Text = $"Đang chạy ngày {date:dd/MM/yyyy}";
                var data = Controller.TransactionByDate(date);
                var dataTake = Controller.TransactionTakeByDate(date);
                var dataReturn = Controller.TransactionReturnByDate(date);

                foreach (var item in data)
                {
                    var gpsItem = Controller.GPSGetByTransaction(item.Id, item.EndTime ?? item.StartTime);

                    Utilities.WriteErrorLog("BtnExport_Click", $" {item.Id} {item.EndTime?.ToString("yyyyMMdd") } { Newtonsoft.Json.JsonConvert.SerializeObject(gpsItem)}" );
                    if (gpsItem != null && gpsItem.Data != null)
                    {
                        var gpsData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GPSDataMini>>(gpsItem.Data);
                        var countIn = gpsData.Where(x => Controller.IsPointInPolygon(new LatLngCityAreaDataModel { Latitude = x.Lat, Longitude = x.Long }, PasteurZone.Select(m => new LatLngCityAreaDataModel { Latitude = m.Lat, Longitude = m.Long }).ToList())).Count();
                        if (countIn >= AppSettings.MinInZone)
                        {
                            crossRoadTotal++;
                        }
                    }
                }

                newList.Add(new DetailReportModle
                {
                    Date = date,
                    CrossRoadTotal = crossRoadTotal,
                    Station_01_Take = dataTake.FirstOrDefault(x=>x.Station == 77)?.Total ?? 0,
                    Station_01_Return = dataReturn.FirstOrDefault(x => x.Station == 77)?.Total ?? 0,

                    Station_02_Take = dataTake.FirstOrDefault(x => x.Station == 80)?.Total ?? 0,
                    Station_02_Return = dataReturn.FirstOrDefault(x => x.Station == 80)?.Total ?? 0,

                    Station_03_Take = dataTake.FirstOrDefault(x => x.Station == 87)?.Total ?? 0,
                    Station_03_Return = dataReturn.FirstOrDefault(x => x.Station == 87)?.Total ?? 0,

                    Station_04_Take = dataTake.FirstOrDefault(x => x.Station == 92)?.Total ?? 0,
                    Station_04_Return = dataReturn.FirstOrDefault(x => x.Station == 92)?.Total ?? 0,

                    Station_05_Take = dataTake.FirstOrDefault(x => x.Station == 96)?.Total ?? 0,
                    Station_05_Return = dataReturn.FirstOrDefault(x => x.Station == 96)?.Total ?? 0,

                    Station_06_Take = dataTake.FirstOrDefault(x => x.Station == 98)?.Total ?? 0,
                    Station_06_Return = dataReturn.FirstOrDefault(x => x.Station == 98)?.Total ?? 0,

                    Station_07_Take = dataTake.FirstOrDefault(x => x.Station == 103)?.Total ?? 0,
                    Station_07_Return = dataReturn.FirstOrDefault(x => x.Station == 103)?.Total ?? 0,
                });

                await Task.Delay(1000);
            }
            BindExcel(newList);
        }

        private void BindExcel(List<DetailReportModle> allData)
        {
            var sFileName = $"{DateTime.Now:yyyyMMddHHMMss}.xlsx";

            var template = new FileInfo($@"{Application.StartupPath}/Templates/Template1.xlsx");

            if (!template.Exists)
            {
                MessageBox.Show("Template excel không tồn tại!", "Hệ thống");
                this.Cursor = Cursors.Default;
                BtnExport.Enabled = true;
                return;
            }

            var fNewFile = new FileInfo(Path.Combine(SelectFolder(), sFileName));

            using (var MyExcel = new ExcelPackage(template))
            {
                ExcelWorksheet myWorksheet = MyExcel.Workbook.Worksheets[1];
                ExcelWorksheet myWorksheet2 = MyExcel.Workbook.Worksheets[0];

                myWorksheet.Cells["A4"].Value = $"({PickerStart.Value:dd/MM/yyyy} đến hết ngày {PickerEnd.Value:dd/MM/yyyy})";
                myWorksheet.Cells["A3"].Value = $"({PickerStart.Value:dd/MM/yyyy} đến hết ngày {PickerEnd.Value:dd/MM/yyyy})";

                DataToSheet(myWorksheet, allData);
                DataToSheetTotal(myWorksheet2, allData);
                MyExcel.SaveAs(fNewFile);
                MessageBox.Show("Export thành công", "Hệ thống");
            }
            this.Cursor = Cursors.Default;
            BtnExport.Enabled = true;
        }

        private void DataToSheetTotal(ExcelWorksheet myWorksheet, List<DetailReportModle> data)
        {
            if (myWorksheet == null)
            {
                return;
            }
            myWorksheet.Cells["C5"].Value = data.Sum(x=>x.Station_01_Take);
            myWorksheet.Cells["D5"].Value = data.Sum(x => x.Station_01_Return);

            myWorksheet.Cells["C6"].Value = data.Sum(x => x.Station_02_Take);
            myWorksheet.Cells["D6"].Value = data.Sum(x => x.Station_02_Return);

            myWorksheet.Cells["C7"].Value = data.Sum(x => x.Station_03_Take);
            myWorksheet.Cells["D7"].Value = data.Sum(x => x.Station_03_Return);

            myWorksheet.Cells["C8"].Value = data.Sum(x => x.Station_04_Take);
            myWorksheet.Cells["D8"].Value = data.Sum(x => x.Station_04_Return);

            myWorksheet.Cells["C9"].Value = data.Sum(x => x.Station_05_Take);
            myWorksheet.Cells["D9"].Value = data.Sum(x => x.Station_05_Return);

            myWorksheet.Cells["C10"].Value = data.Sum(x => x.Station_06_Take);
            myWorksheet.Cells["D10"].Value = data.Sum(x => x.Station_06_Return);

            myWorksheet.Cells["C11"].Value = data.Sum(x => x.Station_07_Take);
            myWorksheet.Cells["D11"].Value = data.Sum(x => x.Station_07_Return);

            myWorksheet.Cells["C12"].Value = data.Sum(x => x.CrossRoadTotal);
        }

        private void DataToSheet(ExcelWorksheet myWorksheet, List<DetailReportModle> data)
        {
            int StartRow = 8;
            int i = 0;
            int colurm = 1;

            if (myWorksheet == null)
            {
                return;
            }

            foreach (var item in data)
            {

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Date.ToString("dd/MM/yyyy");
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_01_Take;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_01_Return;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;


                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_02_Take;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_02_Return;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;


                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_03_Take;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_03_Return;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;


                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_04_Take;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_04_Return;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;


                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_05_Take;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_05_Return;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;


                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_06_Take;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_06_Return;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;


                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_07_Take;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.Station_07_Return;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                myWorksheet.Cells[StartRow + i, colurm].Value = item.CrossRoadTotal;
                SetBorder(myWorksheet, StartRow + i, colurm);
                colurm++;

                colurm = 1;
                i++;
            }
        }

        public ExcelWorksheet SetBorder(ExcelWorksheet ws, int row, int col)
        {
            ws.Cells[row, col].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells[row, col].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
            ws.Cells[row, col].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells[row, col].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
            ws.Cells[row, col].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells[row, col].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
            ws.Cells[row, col].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells[row, col].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
            return ws;
        }

        public ExcelWorksheet SetBorder(ExcelWorksheet ws, int startRow, int StartCol, int endRow, int endCol)
        {
            ws.Cells[startRow, StartCol, endRow, endCol].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells[startRow, StartCol, endRow, endCol].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
            ws.Cells[startRow, StartCol, endRow, endCol].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells[startRow, StartCol, endRow, endCol].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
            ws.Cells[startRow, StartCol, endRow, endCol].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells[startRow, StartCol, endRow, endCol].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
            ws.Cells[startRow, StartCol, endRow, endCol].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells[startRow, StartCol, endRow, endCol].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
            return ws;
        }

        private string SelectFolder()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    return fbd.SelectedPath;
                }
            }
            return "";
        }

        private void Main_Load(object sender, EventArgs e)
        {
            Utilities.WriteErrorLog("Main_Load", $"Start");
            MongoDBSettings = Controller.InitMongoDBSettings();
            PasteurZone = Controller.InitPasteurZoneSettings();

            //Controller.GPSGetByTransaction(100, DateTime.Now);
        }
    }
}
