﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TN.Report;
using TN.Report.Code;
using Dapper;
using TN.Domain.Model;
using System.IO;
using Newtonsoft.Json;
using TN.Mobike.Controls;

namespace TN.Report
{
    public class Controller
    {
        public static MongoDBSettings InitMongoDBSettings()
        {
            var path = $"{Environment.CurrentDirectory}/Settings/MongoDB.Settings.json";
            if (!File.Exists(path))
            {
                return null;
            }
            return JsonConvert.DeserializeObject<MongoDBSettings>(File.ReadAllText(path));
        }

        public static List<GPSDataMini> InitPasteurZoneSettings()
        {
            var path = $"{Environment.CurrentDirectory}/Settings/Pasteur.Zone.json";
            if (!File.Exists(path))
            {
                return null;
            }
            return JsonConvert.DeserializeObject<List<GPSDataMini>>(File.ReadAllText(path));
        }

        public static List<Transaction> TransactionByDate(DateTime date)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                        SELECT [Id], TransactionCode, StartTime, EndTime
                        FROM [dbo].[Transaction]
                        where StartTime >= '{date:yyyy-MM-dd}' and StartTime < '{date.AddDays(1):yyyy-MM-dd}' and Status in (2,5,6,7,8) order by Id desc
                    ";
                    return db.Query<Transaction>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception)
            {
                return new List<Transaction>();
            }
        }

        public static List<ReportTotalModle> TransactionTakeByDate(DateTime date)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
SELECT StationIn as Station, count(1) Total
FROM [dbo].[Transaction]
where 
StartTime >= '{date:yyyy-MM-dd}' and StartTime < '{date.AddDays(1):yyyy-MM-dd}'
and Status in (2,5,6,7,8) 
and StationIn in (77,80,87,92,96,98,103)
group by StationIn
                    ";
                    return db.Query<ReportTotalModle>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception)
            {
                return new List<ReportTotalModle>();
            }
        }

        public static List<ReportTotalModle> TransactionReturnByDate(DateTime date)
        {
            try
            {
                using (var db = new SqlConnection(AppSettings.ConnectionString))
                {
                    string sql = $@"
                    SELECT StationOut as Station, count(1) Total
                    FROM [dbo].[Transaction]
                    where 
                    StartTime >= '{date:yyyy-MM-dd}' and StartTime < '{date.AddDays(1):yyyy-MM-dd}'
                    and Status in (2,5,6,7,8) 
                    and StationOut in (77,80,87,92,96,98,103)
                    group by StationOut";
                    return db.Query<ReportTotalModle>(sql, commandType: CommandType.Text).ToList();
                }
            }
            catch (Exception)
            {
                return new List<ReportTotalModle>();
            }
        }

        public static TransactionGPS GPSGetByTransaction(int transactionId, DateTime endTime)
        {
            try
            {
                string dbName = "TNGO-Data";
                var dbClient = new MongoClient(Main.MongoDBSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                string query = "{ TransactionId: " + transactionId + ", Date: "+ endTime.ToString("yyyyMMdd") + "}";
                Utilities.WriteErrorLog("BtnExport_Click", $"{query}");
                var _context = db.GetCollection<TransactionGPS>("TransactionGPS").Find(query).ToList();
                return _context.FirstOrDefault();
            }
            catch(Exception ex)
            {
                Utilities.WriteErrorLog("BtnExport_Click", $"{ex}");
                return null;
            }
        }

        public static bool IsPointInPolygon(LatLngCityAreaDataModel p, List<LatLngCityAreaDataModel> polygon)
        {
            if (polygon == null || polygon.Count <= 0)
            {
                return false;
            }
            double minX = polygon[0].Latitude;
            double maxX = polygon[0].Latitude;
            double minY = polygon[0].Longitude;
            double maxY = polygon[0].Longitude;
            for (int i = 1; i < polygon.Count; i++)
            {
                LatLngCityAreaDataModel q = polygon[i];
                minX = Math.Min(q.Latitude, minX);
                maxX = Math.Max(q.Latitude, maxX);
                minY = Math.Min(q.Longitude, minY);
                maxY = Math.Max(q.Longitude, maxY);
            }
            if (p.Latitude < minX || p.Latitude > maxX || p.Longitude < minY || p.Longitude > maxY)
            {
                return false;
            }
            // https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html
            bool inside = false;
            for (int i = 0, j = polygon.Count - 1; i < polygon.Count; j = i++)
            {
                if ((polygon[i].Longitude > p.Longitude) != (polygon[j].Longitude > p.Longitude) &&
                     p.Latitude < (polygon[j].Latitude - polygon[i].Latitude) * (p.Longitude - polygon[i].Longitude) / (polygon[j].Longitude - polygon[i].Longitude) + polygon[i].Latitude)
                {
                    inside = !inside;
                }
            }
            return inside;
        }
    }
}