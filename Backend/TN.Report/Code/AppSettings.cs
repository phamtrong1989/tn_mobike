﻿using System;
using System.Configuration;
using System.Globalization;

namespace TN.Report
{
    class AppSettings
    {
        public static string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        public static int MinInZone = Convert.ToInt32(ConfigurationManager.AppSettings["MinInZone"]);
    }
}
