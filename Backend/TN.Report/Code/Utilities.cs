﻿using log4net;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.IO;
using System.Linq;

namespace TN.Mobike.Controls
{
    public static class Functions
    {
        public static string ToJson(this object obj)
        {
            try
            {
                var serializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
                return JsonConvert.SerializeObject(obj, serializerSettings);
            }
            catch
            {
                return "[]";
            }
        }   

        public static string FormatMoney(decimal v)
        {
            return string.Format("{0:00,0}", v);
        }
      
        public static double DistanceInMeter(double lat1, double lon1, double lat2, double lon2)
        {
            try
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(Deg2rad(lat1)) * Math.Sin(Deg2rad(lat2)) + Math.Cos(Deg2rad(lat1)) * Math.Cos(Deg2rad(lat2)) * Math.Cos(Deg2rad(theta));
                dist = Math.Acos(dist);
                dist = Rad2deg(dist);
                dist = dist * 60 * 1.1515;
                // meter
                dist = dist * 1.609344 * 1000;
                if (Double.IsNaN(dist))
                {
                    return 0;
                }
                return Math.Round(dist, 1);
            }
            catch
            {
                return 99999;
            }
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double Deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double Rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        //public static List<GPSData> GPSGetByIMEITop(string imei, int top = 30)
        //{
        //    try
        //    {
        //        string dbName = SyncService.MongoDBSettings.MongoDataBase.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
        //        var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
        //        var db = dbClient.GetDatabase(dbName);
        //        string query = "{ IMEI : \"" + imei + "\", Date : " + DateTime.Now.ToString("yyyyMMdd") + ", Lat : { $gte: 1 }}";
        //        return db.GetCollection<GPSData>("GPS").Find(query).SortByDescending(x=>x.CreateTimeTicks).Limit(top).ToList();
        //    }
        //    catch
        //    {
        //        return new List<GPSData>();
        //    }
        //}

        //public static List<GPSData> GPSGetByIMEI(string imei, DateTime startTime, DateTime endTime)
        //{
        //    try
        //    {
        //        string dbName = SyncService.MongoDBSettings.MongoDataBase.Replace("{TimeDB}", $"{startTime:yyyyMM}");
        //        var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
        //        var db = dbClient.GetDatabase(dbName);
        //        string query = "{ IMEI : \"" + imei + "\", Date : { $gte: " + startTime.ToString("yyyyMMdd") + ", $lte: " + endTime.ToString("yyyyMMdd") + " }, CreateTimeTicks: { $gte: " + startTime.Ticks.ToString() + ", $lte: " + endTime.Ticks.ToString() + " }, Lat: { $gte: 1} }";
        //        var _context = db.GetCollection<GPSData>("GPS").Find(query).ToList();
        //        _context = _context.Where(x => x.Lat > 0 && x.Long > 0).OrderBy(x => x.CreateTimeTicks).ToList();
        //        return _context;
        //    }
        //    catch
        //    {
        //        return new List<GPSData>();
        //    }
        //}

        //private static async Task MongoAddAsync(MongoLog data)
        //{
        //    try
        //    {
        //        string dbName = SyncService.MongoDBSettings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
        //        var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
        //        var db = dbClient.GetDatabase(dbName);
        //        var collection = db.GetCollection<MongoLog>(SyncService.MongoDBSettings.MongoCollectionLog);
        //        await collection.InsertOneAsync(data);
        //    }
        //    catch { }
        //}

        //public static void GPSDataAddAsync(TransactionGPS data)
        //{
        //    try
        //    {
        //        string dbName = "TNGO-Data";
        //        var dbClient = new MongoClient(SyncService.MongoDBSettings.MongoClient);
        //        var db = dbClient.GetDatabase(dbName);
        //        var collection = db.GetCollection<TransactionGPS>("TransactionGPS");
        //        collection.InsertOne(data);
        //    }
        //    catch { }
        //}

        //public static MongoDBSettings InitMongoDBSettings()
        //{
        //    var path = $"{Environment.CurrentDirectory}/Configs/MongoDB.Settings.json";
        //    if(!File.Exists(path))
        //    {
        //        return null;
        //    }
        //    return JsonConvert.DeserializeObject<MongoDBSettings>(File.ReadAllText(path));
        //}
    }

    public class Utilities
    {
        private static readonly ILog log = LogManager.GetLogger("TN.Report");
        public static bool ActiveOperationLog = true;
        public static bool ActiveDebugLog = true;
        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return (long)(datetime - sTime).TotalSeconds;
        }

        public static void StartLog(bool activeOperationLog, bool activeDebugLog)
        {
            ActiveOperationLog = activeOperationLog;
            ActiveDebugLog = activeDebugLog;
        }

        public static void CloseLog()
        {
            foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
            {
                app.Close();
            }
        }

        public static void WriteErrorLog(string logtype, string logcontent)
        {
            try
            {
                log.Error($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        public static void WriteOperationLog(string logtype, string logcontent)
        {
            if (!ActiveOperationLog)
                return;

            try
            {
                log.Info($"{logtype} \t {logcontent}");
            }
            catch 
            {
                // ignored
            }
        }

        public static void WriteDebugLog(string logtype, string logcontent)
        {
            if (!ActiveDebugLog)
                return;

            try
            {
                log.Debug($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                try
                {
                    if (stream != null)
                        stream.Close();
                }
                catch
                {

                }
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string GetBitStr(byte[] data)
        {
            BitArray bits = new BitArray(data);

            string strByte = string.Empty;
            for (int i = 0; i <= bits.Count - 1; i++)
            {
                if (i % 8 == 0)
                {
                    strByte += " ";
                }
                strByte += (bits[i] ? "1" : "0");
            }

            return strByte;
        }

        public static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
    }
}
