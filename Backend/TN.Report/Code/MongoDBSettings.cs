﻿namespace TN.Domain.Model
{
    public class MongoDBSettings
    {
        public bool Is { get; set; }
        public string MongoClient { get; set; }
        public string MongoDataBase { get; set; }
        public string MongoCollection { get; set; }
        public bool IsUseMongo { get; set; }
        public string MongoDataBaseLog { get; set; }
        public string MongoCollectionLog { get; set; }
    }
}
