﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Report.Code
{
    public enum EBookingStatus
    {
        Start = 1,

        End = 2,

        Cancel = 3,

        CancelByAdmin = 4,

        EndByAdmin = 5,

        Debt = 6,
    }

    public enum ETicketType : byte
    {
        Block = 1,
        Day = 2,
        Month = 3
    }

    public class Transaction
    {
        public int Id { get; set; }

        public string TicketPrepaidCode { get; set; }

        public int TicketPrepaidId { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int? StationIn { get; set; }

        public int? StationOut { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        public string TransactionCode { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public byte Vote { get; set; }

        public string Feedback { get; set; }

        public int TicketPriceId { get; set; }

        public ETicketType TicketPrice_TicketType { get; set; }

        public decimal TicketPrice_TicketValue { get; set; }
        // Cho phép trừ vào tài khoản phụ ko
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        // Là bảng giá mặc định
        public bool TicketPrice_IsDefault { get; set; }
        // Quá bao phút thì x tiền
        public int TicketPrice_BlockPerMinute { get; set; }
        // Số tiền x lên quá
        public int TicketPrice_BlockPerViolation { get; set; }

        public decimal TicketPrice_BlockViolationValue { get; set; }
        //Nếu là vé ngày: 1 ngày tối đi đa 12 tiếng = 720 phút,  Nếu vé tháng: Đi thoải mái các ngày trong tháng.Mỗi chuyến đi không quá 2 tiếng = 120 phút, Nếu vé quý: Tương tự tháng
        public int TicketPrice_LimitMinutes { get; set; }
        // Điểm trừ vào tài khoản chính
        public decimal ChargeInAccount { get; set; }
        // Điểm trừ vào tài khoản khuyến mãi
        public decimal ChargeInSubAccount { get; set; }

        public decimal? ChargeDebt { get; set; }

        // Total price
        public decimal TotalPrice { get; set; }
        // Thời gian giao dịch
        public DateTime? DateOfPayment { get; set; }
        public int TotalMinutes { get; set; }
        public DateTime CreatedDate { get; set; }
        public double? KCal { get; set; }
        public double? KG { get; set; }
        public double? KM { get; set; }
        public EBookingStatus Status { get; set; }

        public string Note { get; set; }

        public decimal TripPoint { get; set; }
        // Với trường hợp là vé trả trước dụng chung trường nhưng ý nghĩa khác nhau
        // Tổng số phút đã sử dụng trong ngày (vé ngày), chưa tính giao dịch hiện tại
        // Tổng số phút tối đa sử dụng 1 lần giao dịch (vé tháng)
        public int Prepaid_MinutesSpent { get; set; }
        // Ngày hiệu lực bắt đầu
        public DateTime? Prepaid_StartDate { get; set; }
        // Ngày hết hiệu lực của vé trả trước
        public DateTime? Prepaid_EndDate { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_StartTime { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_EndTime { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public decimal EstimateChargeInAccount { get; set; }

        public decimal EstimateChargeInSubAccount { get; set; }

        public EVehicleRecallStatus VehicleRecallStatus { get; set; } = EVehicleRecallStatus.Default;

        public decimal VehicleRecallPrice { get; set; } = 0;

        public double ReCallM { get; set; } = 0;

        public bool IsDockNotOpen { get; set; } = false;
        public bool IsForgotrReturnTheBike { get; set; } = false;
    }

    public enum EVehicleRecallStatus : byte
    {
        Default = 0,
        Unfulfilled = 1,
        Processed = 2
    }

    public class DetailReportModle
    {
        public DateTime Date { get; set; }

        public int Station_01_Take { get; set; }
        public int Station_01_Return { get; set; }

        public int Station_02_Take { get; set; }
        public int Station_02_Return { get; set; }

        public int Station_03_Take { get; set; }
        public int Station_03_Return { get; set; }

        public int Station_04_Take { get; set; }
        public int Station_04_Return { get; set; }

        public int Station_05_Take { get; set; }
        public int Station_05_Return { get; set; }

        public int Station_06_Take { get; set; }
        public int Station_06_Return { get; set; }

        public int Station_07_Take { get; set; }
        public int Station_07_Return { get; set; }

        public int CrossRoadTotal { get; set; }
    }

    public class ReportTotalModle
    {
        public int Total { get; set; }
        public int Station { get; set; } 
    }

    public class TransactionGPS
    {
        public object _id { get; set; }
        public int TransactionId { get; set; }
        public int Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string TransactionCode { get; set; }
        public string Data { get; set; }
    }

    public class GPSDataMini
    {
        public double Lat { get; set; }
        public double Long { get; set; }
        public long CreateTimeTicks { get; set; }
    }

    public class LatLngCityAreaDataModel
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
