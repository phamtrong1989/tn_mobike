﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.GO.LockServices.Entity;

namespace TN.GO.LockServices.Models
{
    class StationModels
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public List<Station> GetAll()
        {
            var list = sqlHelper.GetAll<Station>();
            return list;
        }
    }
}
