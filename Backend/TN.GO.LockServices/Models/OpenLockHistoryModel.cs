﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.GO.LockServices.Entity;

namespace TN.GO.LockServices.Models
{
    class OpenLockHistoryModel
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public void InsertOpenLockHistory(OpenLockHistory openLock)
        {
            sqlHelper.InsertOne(openLock);
        }
    }
}
