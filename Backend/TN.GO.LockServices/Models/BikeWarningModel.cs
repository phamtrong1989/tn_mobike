﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Models
{
    class BikeWarningModel
    {
        private SqlHelper sqlHelper = new SqlHelper();
        private BikeWarningLogModel bikeWarningLogModel = new BikeWarningLogModel();
        private BookingModel bookingModel = new BookingModel();
        private DockModels dockModel = new DockModels();
        private ApiProcess apiProcess = new ApiProcess();

        public BikeWarning GetOneBikeWarning(int id)
        {
            var bikeWarning = (BikeWarning)sqlHelper.GetOne<BikeWarning>($"Id = {id}");

            return bikeWarning;
        }

        public List<BikeWarning> GetOneBikeWarning(string Imei, EBikeWarningType warningType)
        {
            var bikeWarning = sqlHelper.GetAll<BikeWarning>($"WHERE IMEI = '{Imei}' AND WarningType = {Convert.ToInt32(warningType)}");

            return bikeWarning;
        }

        public List<Station> FindByLatLng(double min_lat, double max_lat, double min_lng, double max_lng)
        {
            var listFind = JobScanScheduler.ListStations.Where(x => x.Lat >= min_lat && x.Lat <= max_lat && x.Lng >= min_lng && x.Lng <= max_lng && x.Status == 1).ToList();
            return listFind;
        }

        public void InsertBikeWarningTask(string imei, string requestId, EBikeWarningType warningType, double lat, double Long, int dockId, bool isGPS = false)
        {
            try
            {
                string note = "";
                Task.Run(() =>
                {
                    if (isGPS)
                    {
                        Utilities.WriteOperationLog("InsertBikeWarningTask.1", $"{requestId} | GPS Process");
                        if (lat != 0 && Long != 0)
                        {

                            var min_Lat = lat - AppSettings.InStationDelta;
                            var max_lat = lat + AppSettings.InStationDelta;

                            var min_Long = Long - AppSettings.InStationDelta;
                            var max_Long = Long + AppSettings.InStationDelta;

                            Utilities.WriteOperationLog("InsertBikeWarningTask.2.1", $"{requestId} | Imei: {imei} | GPS OK | Lat: {lat} - Long: {Long} | MinLat: {min_Lat} -> MaxLat: {max_lat} | MinLong: {min_Long} -> MaxLong: {max_Long}");

                            List<Tuple<Station, double>> resultStations = new List<Tuple<Station, double>>();

                            var stations = FindByLatLng(min_Lat, max_lat, min_Long, max_Long);

                            Utilities.WriteOperationLog("InsertBikeWarningTask.2.1.1", $"{requestId} | Imei: {imei} | Tìm kiếm trạm bằng Min => Max (Lat, Long) | Bán kính: {AppSettings.InStationDelta * 100000} m | Số trạm: {stations.Count}");

                            if (stations != null && stations.Count > 0)
                            {
                                foreach (var station in stations)
                                {
                                    var check = Utilities.DistanceInMeterM(lat, Long, station.Lat, station.Lng);
                                    if (check <= station.ReturnDistance)
                                    {
                                        Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.0", $"{requestId} | Imei: {imei} | Thông số cấu hình trong trạm: {station.ReturnDistance} | Số mét: {check} | Trạm: {station.Id}");
                                        resultStations.Add(new Tuple<Station, double>(station, check));
                                    }
                                }

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.1", $"{requestId} | Imei: {imei} | Kiểm tra so với thông số cấu hình trạm | Số trạm thỏa mãn: {resultStations.Count}");

                                var booking = bookingModel.GetOneBooking(dockId);
                                if (resultStations.Count <= 0)
                                {
                                    if (booking == null)
                                    {
                                        warningType = EBikeWarningType.XeKhongTrongTram;
                                    }

                                    dockModel.UpdateStation(imei, 0, requestId);
                                    dockModel.UpdateBike(dockId, 0, /*dock.Lat, dock.Long,*/ requestId, imei);

                                    Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.2", $"{requestId} | 0 trạm thỏa mãn | Cập nhật Station = 0");
                                }
                                else
                                {
                                    var station = resultStations.OrderBy(x => x.Item2).FirstOrDefault();

                                    dockModel.UpdateStation(imei, station.Item1.Id, requestId);
                                    dockModel.UpdateBike(dockId, station.Item1.Id, /*dock.Lat, dock.Long,*/ requestId, imei);

                                    Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.3", $"{requestId} | Imei: {imei} | {resultStations.Count} trạm thỏa mãn | Cập nhật Station = {station.Item1.Id} | Số mét: {station.Item2}");
                                }
                            }
                            else
                            {
                                var overKm = AppSettings.InStationDelta * AppSettings.OverStationDelta;
                                min_Lat = lat - overKm;
                                max_lat = lat + overKm;

                                min_Long = Long - overKm;
                                max_Long = Long + overKm;

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.3", $"{requestId} | Imei: {imei} | GPS OK | Lat: {lat} - Long: {Long} | MinLat: {min_Lat} - MaxLat: {max_lat} | MinLong: {min_Long} - MaxLong: {max_Long}");

                                stations = FindByLatLng(min_Lat, max_lat, min_Long, max_Long);

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.1", $"{requestId} | Imei: {imei} | Tìm kiếm trạm bằng Min => Max (Lat, Long) | Bán kính: {AppSettings.InStationDelta * AppSettings.OverStationDelta * 100000} m | Số trạm: {stations.Count}");

                                if (stations != null && stations.Count > 0)
                                {
                                    foreach (var station in stations)
                                    {
                                        var check = Utilities.DistanceInMeterM(lat, Long, station.Lat, station.Lng);
                                        resultStations.Add(new Tuple<Station, double>(station, check));

                                        Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.2.0", $"{requestId} | Imei: {imei} | Số mét: {check} | Trạm: {station.Id}");
                                    }

                                    var stationMin = resultStations.OrderBy(x => x.Item2).FirstOrDefault();

                                    var booking = bookingModel.GetOneBooking(dockId);

                                    if (booking == null)
                                    {
                                        dockModel.UpdateStation(imei, stationMin.Item1.Id, requestId);
                                        dockModel.UpdateBike(dockId, stationMin.Item1.Id, /*dock.Lat, dock.Long,*/ requestId, imei);
                                    }

                                    var map = $"https://maps.google.com?q={lat},{Long}";
                                    var km = Math.Round(stationMin.Item2 / 1000, 3);

                                    note = $"Trạm gần nhất: {stationMin?.Item1.DisplayName} | Tọa độ xe: {map} | Khoảng cách vs trạm: {km} KM";

                                    Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.2.1", $"{requestId} | Imei: {imei} | {note}");

                                    if (km >= AppSettings.OverStationDelta)
                                    {
                                        warningType = EBikeWarningType.XeDiQuaXa;

                                        Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.2.1", $"{requestId} | Imei: {imei} | {note} | Xe đi quá xa");
                                    }
                                }
                                else
                                {
                                    warningType = EBikeWarningType.XeDiQuaXa;

                                    Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.3", $"{requestId} | Imei: {imei} | {note} | Xe đi quá xa | Station Null");
                                }
                            }

                        }
                        else
                        {
                            Utilities.WriteOperationLog("InsertBikeWarningTask.3", $"{requestId} | Xe lỗi GPS | Lat: {lat} - Long: {Long}");
                            warningType = EBikeWarningType.XeLoiGPS;
                        }
                    }

                    AddBikeWarning(imei, requestId, warningType, lat, Long, note);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("InsertBikeWarningTask", $"Error: {e.Message}");
            }
        }

        private async void AddBikeWarning(string imei, string requestId, EBikeWarningType warningType, double lat,
            double lng, string note = "")
        {
            await InsertBikeWarning(new BikeWarning(imei, requestId, warningType, lat, lng, true, note, DateTime.Now));
        }

        private async Task InsertBikeWarning(BikeWarning bikeWarning)
        {
            try
            {
                apiProcess.PushBikeWarning(bikeWarning.IMEI, bikeWarning.WarningType, bikeWarning.UpdatedDate, bikeWarning.Transaction);

                var getBikeList = GetOneBikeWarning(bikeWarning.IMEI, bikeWarning.WarningType);
                
                if (getBikeList != null && getBikeList.Count > 0)
                {
                    
                    var getBike = getBikeList.FirstOrDefault();
                    
                    if (getBike != null)
                    {
                        
                        var time = (bikeWarning.UpdatedDate - getBike.UpdatedDate).TotalSeconds;
                        
                        if (time > AppSettings.TimeCheckBikeWarning)
                        {
                            
                            sqlHelper.InsertOne<BikeWarning>(bikeWarning);
                            
                            var bikeLog = new BikeWarningLog(getBike);
                            
                            bikeWarningLogModel.InsertBikeWarning(bikeLog);
                            
                            DeleteBikeWarning(getBike.Id);
                        }
                        else
                        {
                            sqlHelper.UpdateOne<BikeWarning>(bikeWarning, $"IMEI = '{bikeWarning.IMEI}' AND WarningType = {Convert.ToInt32(bikeWarning.WarningType)}");
                        }
                    }
                }
                else
                {
                    sqlHelper.InsertOne<BikeWarning>(bikeWarning);
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("InsertBikeWarning", $"Error: {e}");
            }

            await Task.Delay(1);
        }

        public void DeleteBikeWarning(int id)
        {
            sqlHelper.DeleteRecord<BikeWarning>($"Id = {id}");
        }

        public void DeleteBikeWarning(string Imei)
        {
            sqlHelper.DeleteRecord<BikeWarning>($"IMEI = '{Imei}'");
        }
    }
}
