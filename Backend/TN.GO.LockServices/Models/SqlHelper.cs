﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MongoDB.Bson;
using MongoDB.Driver;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Models
{
    class SqlHelper
    {
        #region SQL

        public SqlConnection GetConnection(string connection = "")
        {
            var cnn = connection == "" ? AppSettings.ConnectionString : connection;
            var sqlConnection = new SqlConnection(cnn);
            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            return sqlConnection;
        }

        public object GetOne<T>(string where = "", string conn = "")
        {
            try
            {
                using (var db = GetConnection(conn))
                {
                    var query = $"SELECT TOP(1) * FROM {typeof(T).Name} Where {where}";
                    var list = db.Query<T>(query).FirstOrDefault();

                    Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.GetOne ]", $"Get one record from table {typeof(T).Name} - {where} - successful !");

                    return list;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.GetOne ]", $"Get one record from table {typeof(T).Name} - {where} -  fail ! : {e}");
                return null;
            }
        }

        public List<T> GetAll<T>(string where = "")
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = $"SELECT * FROM {typeof(T).Name} {where}";
                    var list = db.Query<T>(query).ToList();

                    Utilities.WriteOperationLog("[ SqlHelper.GetAll ]", $"Get all record from table {typeof(T).Name} successful !");

                    return list;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ SqlHelper.GetAll ]", $"Get all record from table {typeof(T).Name} fail ! : {e}");
                return null;
            }
        }

        public void UpdateOne<T>(T obj, string where)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>(where, false);
                    db.Execute(query, obj);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.UpdateOne ]", $"Update record table {typeof(T).Name} has {where} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.UpdateOne ]", $"Update record table {typeof(T).Name} has {where} fail ! : {e}");
            }
        }

        public void InsertMany<T>(List<T> list)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>("");
                    db.Execute(query, list);
                }

                Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.InsertMany ]", $"Insert {list.Count} record to table {typeof(T).Name} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.InsertMany ]", $"Insert {list.Count} record to table {typeof(T).Name}  fail ! : {e}");
            }
        }

        public void InsertOne<T>(T obj)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>("");
                    db.Execute(query, obj);
                }

                Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.InsertOne ]", $"Insert record to table {typeof(T).Name} successful [ {obj.ToString()} ]!");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.InsertOne ]", $"Insert record to table {typeof(T).Name} fail! : {e} -- [ {obj.ToString()} ]");
            }
        }

        public void DeleteRecord<T>(string where)
        {
            try
            {
                using (var connection = GetConnection())
                {
                    var query = $"DELETE FROM {typeof(T).Name} WHERE {where}";
                    connection.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.DeleteRecord ]", $"Delete record {typeof(T).Name} Where {where} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.DeleteRecord ]", $"[ ERROR: {e} ]");
            }
        }

        public bool CheckExist<T>(string where = "")
        {
            try
            {
                using (var connection = GetConnection())
                {
                    var query = $"SELECT * FROM {typeof(T).Name} Where {where}";
                    var result = connection.Query(query).Any();

                    return result;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.CheckExist ]", $"[ ERROR: {e} ]");
                return false;
            }
        }

        #endregion


        #region MongoDB

        public static IMongoCollection<BsonDocument> collectionGPS = null;

        public static void OpenConnectMongoDb()
        {
            try
            {
                MongoClient dbClient = new MongoClient(AppSettings.ConnectionMongoDb);
                var database = dbClient.GetDatabase($"{AppSettings.DatabaseNameMongo}-{DateTime.Now:yyyyMM}");
                collectionGPS = database.GetCollection<BsonDocument>(AppSettings.TableGPSNameMongo);

                Utilities.WriteOperationLog("[SqlHelper.OpenConnectMongoDb]", $"OPEN CONNECT TO {AppSettings.ConnectionMongoDb} SUCCESSFUL !");

                if (AppSettings.TurnOnLogSQL)
                {
                    new MongoUtilities().AddLog(0, $"OpenConnectMongoDb | Kết nối MONGO", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"OPEN CONNECT TO {AppSettings.ConnectionMongoDb} SUCCESSFUL !");
                    
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[SqlHelper.OpenConnectMongoDb]", $"[ERROR: {e}]");

                if (AppSettings.TurnOnLogSQL)
                {
                    new MongoUtilities().AddLog(0, $"OpenConnectMongoDb (Error) | Kết nối MONGO (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"OpenConnectMongoDb | Error: {e.Message}", true);
                }
            }
        }

        public static void InsertGPSTask(string imei, double lat, double l, string requestId = "")
        {
            try
            {
                Task.Run(() => InsertGPS(imei, lat, l, requestId)).ConfigureAwait(false);
            }
            catch
            {
                //
            }
        }

        public static async void InsertGPS(string imei, double lat, double l, string requestId = "")
        {

            try
            {
                if (collectionGPS == null)
                {
                    OpenConnectMongoDb();
                }
                else
                {
                    OpenConnectMongoDb();

                    GPSData gps = new GPSData(imei, lat, l, DateTime.Now);
                    await collectionGPS.InsertOneAsync(gps.ToBsonDocument());

                    Utilities.WriteOperationLog("[SqlHelper.InsertGPS]", $"INSERT SUCCESS GPSData: {gps.ToString()} | Table: {collectionGPS.Database.DatabaseNamespace.DatabaseName}");

                    if (AppSettings.TurnOnLogD0)
                    {
                        new MongoUtilities().AddLog(0, "D0 (InsertGPS) | Vị trí (Thêm GPS)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"INSERT SUCCESS GPSData: {gps.ToString()}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[SqlHelper.InsertGPS]", $"[ERROR: {e}]");

                if (AppSettings.TurnOnLogD0)
                {
                    new MongoUtilities().AddLog(0, "D0 (InsertGPS.Error) | Vị trí (Thêm GPS.Lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"[ERROR: {e}]", true);
                }
            }
            await Task.Delay(1);
        }

        #endregion


        #region Gennerate

        public string GetQuery<T>(string where, bool isInsert = true)
        {
            var listProperties = typeof(T).GetProperties();
            if (isInsert)
            {
                var propertiesString = "";
                var propertiesValues = "";
                foreach (var prpInfo in listProperties)
                {
                    if (prpInfo == listProperties.Last())
                    {
                        if (prpInfo.Name.ToLower() == "transaction")
                        {
                            propertiesString += $"[{prpInfo.Name.ToLower()}]";
                        }
                        else
                        {
                            propertiesString += $"{prpInfo.Name.ToLower()}";
                        }
                        propertiesValues += $"@{prpInfo.Name.ToLower()}";
                    }
                    else if (prpInfo.Name.ToLower() == "id")
                    {
                        continue;
                    }
                    else
                    {
                        if (prpInfo.Name.ToLower() == "transaction")
                        {
                            propertiesString += $"[{prpInfo.Name.ToLower()}],";
                        }
                        else
                        {
                            propertiesString += $"{prpInfo.Name.ToLower()},";
                        }
                        propertiesValues += $"@{prpInfo.Name.ToLower()},";
                    }
                }
                return $"INSERT INTO {typeof(T).Name}({propertiesString}) VALUES ({propertiesValues})";
            }
            else
            {
                var updateValues = "";
                foreach (var prpInfo in listProperties)
                {
                    if (prpInfo == listProperties.Last())
                    {
                        if (prpInfo.Name.ToLower() == "transaction")
                        {
                            updateValues += $"[{prpInfo.Name.ToLower()}] = @{prpInfo.Name.ToLower()}";
                        }
                        else
                        {
                            updateValues += $"{prpInfo.Name.ToLower()} = @{prpInfo.Name.ToLower()}";
                        }
                    }
                    else if (prpInfo.Name.ToLower() == "id")
                    {
                        continue;
                    }
                    else
                    {
                        if (prpInfo.Name.ToLower() == "transaction")
                        {
                            updateValues += $"[{prpInfo.Name.ToLower()}] = @{prpInfo.Name.ToLower()},";
                        }
                        else
                        {
                            updateValues += $"{prpInfo.Name.ToLower()} = @{prpInfo.Name.ToLower()},";
                        }
                    }
                }
                return $"UPDATE {typeof(T).Name} SET {updateValues} WHERE {where}";
            }
        }

        #endregion
    }
}
