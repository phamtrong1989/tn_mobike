﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Models
{
    class DockModels
    {
        private SqlHelper sqlHelper = new SqlHelper();
        private MongoUtilities mongoUtilities = new MongoUtilities();

        public List<Dock> GetAllDocks()
        {
            var list = sqlHelper.GetAll<Dock>();

            return list;
        }

        public Dictionary<string, Charging> GetOldPinFirst()
        {
            var listDock = GetAllDocks();
            Dictionary<string, Charging> dataLastPin = new Dictionary<string, Charging>();

            foreach (var dock in listDock)
            {
                dataLastPin.Add(dock.IMEI, new Charging(dock.IMEI, dock.Battery, dock.Charging ? ChargingE.IsCharging : ChargingE.NotCharging, dock.Id));
            }

            return dataLastPin;
        }

        public Dock GetOneDock(string imei)
        {
            var dock = (Dock)sqlHelper.GetOne<Dock>($"IMEI = '{imei}'");

            return dock;
        }

        public void UpdateDock(Dock dock)
        {
            sqlHelper.UpdateOne(dock, $"id = {dock.Id}");
        }

        public void InsertDock(Dock dock)
        {
            dock.CreatedDate = DateTime.Now;
            dock.UpdatedDate = DateTime.Now;

            Utilities.WriteOperationLog($"[ SqlHelper.Dock.InsertDock ]", $"Comment Insert");

            //try
            //{
            //    var dockCheck = GetOneDock(dock.IMEI);
            //    if (dockCheck == null)
            //    {
            //        sqlHelper.InsertOne<Dock>(dock);
            //    }
            //}
            //catch (Exception e)
            //{
            //    Utilities.WriteErrorLog("InsertDock", $"ERROR: {e.Message}");
            //}
        }

        public void UpdateBike(int dockId, double lat, double l, string requestId, string imei)
        {
            try
            {
                if (sqlHelper.CheckExist<Bike>($"DockId = {dockId}"))
                {
                    using (var db = sqlHelper.GetConnection())
                    {
                        var query = $"UPDATE Bike SET Lat = {lat}, Long = {l} WHERE DockId = {dockId}";
                        db.Execute(query);
                    }
                    Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update Bike has DockId = {dockId} successful !");

                    if (AppSettings.TurnOnLogD0)
                    {
                        mongoUtilities.AddLog(0, "D0 (Update Bike) | Vị trí (Cập nhật xe)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"UPDATE Bike SET Lat = {lat}, Long = {l} WHERE DockId = {dockId} | Successful");
                    }

                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update Bike has DockId = {dockId} fail ! : {e}");

                if (AppSettings.TurnOnLogD0)
                {
                    mongoUtilities.AddLog(0, "D0 (Update Bike Error) | Vị trí (Cập nhật xe lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"UPDATE Bike SET Lat = {lat}, Long = {l} WHERE DockId = {dockId} fail ! | {e.Message}", true);
                }
            }
        }

        public void UpdateBike(int dockId, int stationId, /*double lat, double l,*/ string requestId, string imei)
        {
            try
            {
                if (sqlHelper.CheckExist<Bike>($"DockId = {dockId}"))
                {
                    using (var db = sqlHelper.GetConnection())
                    {
                        //var query = $"UPDATE Bike SET Lat = {lat}, Long = {l}, StationId = {stationId} WHERE DockId = {dockId}";
                        var query = $"UPDATE Bike SET StationId = {stationId} WHERE DockId = {dockId}";
                        db.Execute(query);
                    }
                    Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateBike ]", $"Update Bike has DockId = {dockId} successful !");

                    if (AppSettings.TurnOnLogD0)
                    {
                        //mongoUtilities.AddLog(0, "D0 (Update Bike) | Vị trí (Cập nhật xe)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"UPDATE Bike SET Lat = {lat}, Long = {l}, StationId = {stationId} WHERE DockId = {dockId} | Successful");
                        mongoUtilities.AddLog(0, "D0 (Update Bike) | Vị trí (Cập nhật xe)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"UPDATE Bike SET StationId = {stationId} WHERE DockId = {dockId} | Successful");
                    }

                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateBike ]", $"Update Bike has DockId = {dockId} fail ! : {e}");

                if (AppSettings.TurnOnLogD0)
                {
                    //mongoUtilities.AddLog(0, "D0 (Update Bike Error) | Vị trí (Cập nhật xe lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"UPDATE Bike SET Lat = {lat}, Long = {l}, StationId = {stationId} WHERE DockId = {dockId} fail ! | {e.Message}", true);
                    mongoUtilities.AddLog(0, "D0 (Update Bike Error) | Vị trí (Cập nhật xe lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"UPDATE Bike SET StationId = {stationId} WHERE DockId = {dockId} fail ! | {e.Message}", true);
                }
            }
        }



        public void UpdateStation(string imei, int stationId, string requestId)
        {
            try
            {
                using (var db = sqlHelper.GetConnection())
                {
                    var query = $"UPDATE Dock SET StationId = {stationId} WHERE IMEI = '{imei}'";
                    db.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateStation ]", $"{requestId} | UPDATE Dock SET StationId = {stationId} WHERE IMEI = '{imei}'");

                if (AppSettings.TurnOnLogD0)
                {
                    mongoUtilities.AddLog(0, "D0 (Update Dock) | Vị trí (Cập nhật Khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"UPDATE Dock SET StationId = {stationId} WHERE IMEI = '{imei}'");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateStatus ]", $"{requestId} | UPDATE Dock SET StationId = {stationId} WHERE IMEI = '{imei}' Fail | Error: {e.Message}");

                if (AppSettings.TurnOnLogD0)
                {
                    mongoUtilities.AddLog(0, "D0 (Update Dock Error) | Vị trí (Cập nhật Khóa lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"UPDATE Dock SET StationId = {stationId} WHERE IMEI = '{imei}' Fail | Error: {e.Message}");
                }
            }
        }



        public void UpdateStatus()
        {
            try
            {
                using (var db = sqlHelper.GetConnection())
                {
                    var query = "UPDATE Dock SET ConnectionStatus = 0 WHERE ConnectionStatus = 1";
                    db.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update All record table Dock connection false successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update All record table Dock fail ! : {e}");
            }
        }

        public void UpdateStatus(string imei, int status)
        {
            try
            {
                using (var db = sqlHelper.GetConnection())
                {
                    var query = $"update Dock SET ConnectionStatus = {status} WHERE IMEI = '{imei}'";
                    db.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update record table Dock connection false Imei {imei} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update record table Dock connection false Imei {imei} fail ! : {e}");
            }
        }

        public void UpdateCharging(string imei, ChargingE charging)
        {
            try
            {
                using (var db = sqlHelper.GetConnection())
                {
                    var query = $"UPDATE Dock SET Charging = {Convert.ToInt32(charging)} WHERE IMEI = '{imei}'";
                    db.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateCharging ]", $"Update record table Dock charging Imei {imei} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateCharging ]", $"Update record table Dock charging Imei {imei} fail ! : {e}");
            }
        }

        public enum ChargingE
        {
            IsCharging = 1,
            NotCharging = 0
        }
    }
}
