﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Models
{
    class OpenLockRequestModel
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public List<OpenLockRequest> GetAllOpenLockRequests(DateTime nowTime)
        {
            List<OpenLockRequest> list = new List<OpenLockRequest>();
            list = sqlHelper.GetAll<OpenLockRequest>($"Where CreatedDate >= '{nowTime.AddSeconds(-AppSettings.LimitTimeOpen):yyyy-MM-dd HH:mm:ss.ffffff}' AND CreatedDate <= '{nowTime:yyyy-MM-dd HH:mm:ss.ffffff}'");
            //list = sqlHelper.GetAll<OpenLockRequest>();
            return list;
        }

        public void DeleteOpenLockRequest(int id)
        {
            sqlHelper.DeleteRecord<OpenLockRequest>($"id = {id}");
        }

        public OpenLockRequest GetOne(string imei, DateTime time)
        {
            // select top(1) * from OpenLockRequest where imei = '861123053530117' order by id desc
            var result = (OpenLockRequest) sqlHelper.GetOne<OpenLockRequest>($"imei = '{imei}' AND CreatedDate >= '{time.AddMinutes(-AppSettings.TimeCheckL0):yyyy-MM-dd HH:mm:ss}' And CreatedDate < '{time.AddSeconds(5):yyyy-MM-dd HH:mm:ss}' order by id desc");

            return result;
        }
    }
}
