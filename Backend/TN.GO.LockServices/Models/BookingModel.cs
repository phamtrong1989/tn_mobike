﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.GO.LockServices.Entity;

namespace TN.GO.LockServices.Models
{
    class BookingModel
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public Booking GetOneBooking(long id)
        {
            var booking = (Booking)sqlHelper.GetOne<Booking>($"DockId = {id}");

            return booking;
        }
    }
}
