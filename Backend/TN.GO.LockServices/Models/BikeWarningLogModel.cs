﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.GO.LockServices.Entity;

namespace TN.GO.LockServices.Models
{
    class BikeWarningLogModel
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public BikeWarningLog GetOneBikeWarning(int id)
        {
            var bikeWarning = (BikeWarningLog)sqlHelper.GetOne<BikeWarningLog>($"Id = {id}");

            return bikeWarning;
        }

        public BikeWarningLog GetOneBikeWarning(string Imei)
        {
            var bikeWarning = (BikeWarningLog)sqlHelper.GetOne<BikeWarningLog>($"IMEI = '{Imei}'");

            return bikeWarning;
        }

        public void InsertBikeWarning(BikeWarningLog bikeWarning)
        {
            sqlHelper.InsertOne<BikeWarningLog>(bikeWarning);
        }

       
    }
}
