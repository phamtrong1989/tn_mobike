﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.MinaCores;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Process
{
    class DBProcess
    {
        private OpenLockRequestModel openLockRequestModel = new OpenLockRequestModel();
        private OpenLockHistoryModel openLockHistoryModel = new OpenLockHistoryModel();
        private TNControl tnControl = new TNControl();

        public async void OpenLock(OpenLockRequest item)
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";

            var key = $"{item.CommandType.ToString()}";

            if (AppSettings.ModeRun)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("===============================================================");
                Console.WriteLine(JsonConvert.SerializeObject(item));
                Console.WriteLine("===============================================================");
                Console.ForegroundColor = ConsoleColor.White;
            }


            var unlock = tnControl.Unlock(item.IMEI, key, item.RFID);

            if (!unlock)
            {
                Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"OPEN FAIL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");
                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, $"QueryDbToUnlock (Error) | Quét DB mở khóa (Lỗi)", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"OPEN FAIL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");
                }
            }
            else
            {
                var openLock = new OpenLockHistory(item);
                openLock.Status = 0;

                try
                {
                    openLockHistoryModel.InsertOpenLockHistory(openLock);
                    if (AppSettings.TurnOnLogSendUnlock)
                    {
                        new MongoUtilities().AddLog(0, "InsertLockHistory | Xóa request mở khóa", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"Imei: {item.IMEI} | InsertLockHistory | {openLock.ToString()}");
                    }
                }
                catch (Exception e)
                {
                    Utilities.WriteErrorLog("[InsertLockHistory]", $"[ERROR: {e}]");
                    if (AppSettings.TurnOnLogSendUnlock)
                    {
                        new MongoUtilities().AddLog(0, "InsertLockHistory (Error) | Thêm lịch sử mở khóa (Lỗi)", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"Imei: {item.IMEI} | InsertLockHistory | [ERROR: {e}]");
                    }
                }

                try
                {
                    openLockRequestModel.DeleteOpenLockRequest(item.Id);

                    if (AppSettings.TurnOnLogSendUnlock)
                    {
                        new MongoUtilities().AddLog(0, "DeleteLockRequest | Xóa request mở khóa", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"Imei: {item.IMEI} | DeleteLockRequest | {item.ToString()}");
                    }
                }
                catch (Exception e)
                {
                    Utilities.WriteErrorLog("[DeleteLockRequest]", $"[ERROR: {e}]");

                    if (AppSettings.TurnOnLogSendUnlock)
                    {
                        new MongoUtilities().AddLog(0, "DeleteLockRequest (Error) | Xóa request mở khóa (Lỗi)", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"Imei: {item.IMEI} | DeleteLockRequest | [ERROR: {e}]");
                    }
                }

                Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"OPEN SUCCESSFUL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");

                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, $"QueryDbToUnlock | Quét DB mở khóa", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"OPEN SUCCESSFUL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");
                }
            }

            await Task.Delay(1);
        }

        public void QueryDbToUnlock()
        {
            var imei = "";
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";
            try
            {
                var listOpenRequest = openLockRequestModel.GetAllOpenLockRequests(DateTime.Now);

                if (listOpenRequest == null || listOpenRequest.Count <= 0)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"NO DATA IN {DateTime.Now:yyyy/MM/dd HH:mm:ss}");
                    }
                    return;
                }

                foreach (var item in listOpenRequest)
                {

                    var key = $"{item.CommandType.ToString()}";

                    imei = item.IMEI;
                    if (AppSettings.ModeRun)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("===============================================================");
                        Console.WriteLine(JsonConvert.SerializeObject(item));
                        Console.WriteLine("===============================================================");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    var unlock = tnControl.Unlock(item.IMEI, key, item.RFID);

                    if (JobCheckConnection.DataConnection.TryGetValue(imei, out var value) && key == "L0")
                    {
                        value.LastSendL0 = DateTime.Now;
                    }

                    Utilities.WriteOperationLog("JobOpenLock", $"{requestId} | Imei: {imei} | Unlock: {unlock}");

                    //if (unlock || key != "L0")
                    //{
                    //    var openLock = new OpenLockHistory(item);
                    //    openLock.Status = 1;

                    //    try
                    //    {
                    //        openLockHistoryModel.InsertOpenLockHistory(openLock);

                    //        if (AppSettings.TurnOnLogSendUnlock)
                    //        {
                    //            new MongoUtilities().AddLog(0, "InsertLockHistory | Xóa request mở khóa", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | InsertLockHistory | {openLock.ToString()}");
                    //        }
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        Utilities.WriteErrorLog("[InsertLockHistory]", $"[ERROR: {e}]");

                    //        if (AppSettings.TurnOnLogSendUnlock)
                    //        {
                    //            new MongoUtilities().AddLog(0, "InsertLockHistory (Error) | Thêm lịch sử mở khóa (Lỗi)", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | InsertLockHistory | [ERROR: {e}]");

                    //        }
                    //    }

                    //    try
                    //    {
                    //        openLockRequestModel.DeleteOpenLockRequest(item.Id);

                    //        if (AppSettings.TurnOnLogSendUnlock)
                    //        {
                    //            new MongoUtilities().AddLog(0, "DeleteLockRequest | Xóa request mở khóa", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | DeleteLockRequest | {item.ToString()}");
                    //        }
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        Utilities.WriteErrorLog("[DeleteLockRequest]", $"[ERROR: {e}]");

                    //        if (AppSettings.TurnOnLogSendUnlock)
                    //        {
                    //            new MongoUtilities().AddLog(0, "DeleteLockRequest (Error) | Xóa request mở khóa (Lỗi)", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | DeleteLockRequest | [ERROR: {e}]");
                    //        }
                    //    }

                    //    Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"OPEN SUCCESSFUL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");

                    //    if (AppSettings.TurnOnLogSendUnlock)
                    //    {
                    //        new MongoUtilities().AddLog(0, $"QueryDbToUnlock | Quét DB mở khóa", requestId, item.IMEI, MongoUtilities.EnumMongoLogType.Start, $"OPEN SUCCESSFUL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");

                    //    }
                    //}
                    //else
                    //{
                    //    continue;
                    //}

                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ControlServices.QueryDbToUnlock]", $"[ERROR: {e}]");

                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, $"QueryDbToUnlock (Error) | Quét DB mở khóa (Lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"[ERROR: {e.Message}]", true);
                }
            }
        }
    }
}
