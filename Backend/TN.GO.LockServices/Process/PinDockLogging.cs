﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Process
{

    class LogPin
    {
        public string  Imei { get; set; }
        public string requestId { get; set; }
        public double PinPercent { get; set; }
        public int PinNum { get; set; }
        public MongoUtilities.EnumSystemType SystemType { get; set; }

        public DateTime Time { get; set; }

        public bool IsExeption { get; set; }
    }

    class PinDockLogging
    {
        LogSettings _logSettings = new LogSettings();

        public static Dictionary<string, int> ListCheckPin = new Dictionary<string, int>();

        public void AddLog(string requestId, string imei, double pinPercent, int pinNum, bool isException, DateTime time)
        {
            try
            {
                Task.Run(() =>
                {
                    if (ListCheckPin.TryGetValue(imei, out var value))
                    {
                        if (value != pinNum)
                        {
                            ListCheckPin[imei] = pinNum;
                            AddLogRun(requestId, imei, pinPercent, pinNum, isException, time);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        ListCheckPin[imei] = pinNum;
                        AddLogRun(requestId, imei, pinPercent, pinNum, isException, time);
                    }
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("PinDockLogging",$"{requestId} | Imei: {imei} | Error: {e.Message}");
            }
        }

        private async void AddLogRun(string requestId, string imei, double pinPercent, int pinNum, bool isException, DateTime time)
        {
            await LogPin(_logSettings, new LogPin
            {
                Imei = imei,
                IsExeption = isException,
                PinNum = pinNum,
                PinPercent = pinPercent,
                requestId = requestId,
                SystemType = MongoUtilities.EnumSystemType.DockService,
                Time = time
            });
        }

        private async Task LogPin(LogSettings settings, LogPin data)
        {
            try
            {
                string dbName = settings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<LogPin>(settings.MongoCollectionLog);
                await collection.InsertOneAsync(data);
            }
            catch (Exception)
            {
                //
            }

            await Task.Delay(1);
        }

    }
}
