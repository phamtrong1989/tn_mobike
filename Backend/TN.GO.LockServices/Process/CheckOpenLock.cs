﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Process
{
    class CheckOpenLock
    {
        private DockModels dockModel = new DockModels();
        private BookingModel bookingModel = new BookingModel();
        public static Dictionary<string, OpenLockWarning> CHECKOPENWARNINGS = new Dictionary<string, OpenLockWarning>();

        public void CheckH0(string requestId, bool lockOpen, string imei)
        {
            try
            {
                if (lockOpen)
                {
                    var dock = dockModel.GetOneDock(imei);
                    if (dock != null)
                    {
                        var booking = bookingModel.GetOneBooking(dock.Id);
                        // Tìm thấy chuyến đi
                        if (booking != null)
                        {
                            Utilities.WriteDebugLog($"CheckH0 | {requestId}", $"Khóa có imei {imei} đang mở và có chuyến đi !");

                            if (CHECKOPENWARNINGS.TryGetValue(imei, out var value))
                            {
                                // Tìm thấy lần nhận gói tin mở khóa cuối cùng
                                Utilities.WriteDebugLog($"CheckH0 | {requestId}", $"Khóa có imei {imei} đang mở - xe trong chuyến và tìm thấy gói xác nhận mở khóa lúc {value.LastL0} !");
                            }
                            else
                            {
                                // Không thấy gói tin mở khóa
                                Utilities.WriteDebugLog($"CheckH0 | {requestId}", $"Khóa có imei {imei} đang mở - xe trong chuyến và không tìm thấy gói xác nhận mở khóa !");
                            }

                            return;
                        }
                        // Không tìm thấy chuyến đi
                        else
                        {
                            //if (CHECKOPENWARNINGS.TryGetValue(imei, out var value))
                            //{
                            //    // Tìm thấy lần nhận gói tin mở khóa cuối cùng
                            //}
                            //else
                            //{
                            //    // Không thấy gói tin mở khóa
                            //}
                        }
                    }
                    else
                    {
                        Utilities.WriteDebugLog($"CheckH0 | {requestId}", $"Khóa có imei {imei} không tìm thấy trong DB !");
                        return;
                    }
                }
                else
                {
                    Utilities.WriteDebugLog($"CheckH0 | {requestId}", $"Lock đang khóa, bỏ qua không xử lý !");
                    return;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"CheckH0 | {requestId}",$"Error: {e.Message}");
            }
        }
    }

    class OpenLockWarning
    {
        public string Imei { get; set; }
        public DateTime LastL0 { get; set; }
    }
}
