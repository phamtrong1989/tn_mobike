﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TN.GO.LockServices.RabbitMQ;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Process
{
    class RabbitMqProcess
    {
        private static RabbitMqReceive receive = new RabbitMqReceive(AppSettings.RMQAddress, AppSettings.RMQUser, AppSettings.RMQPassword, AppSettings.RMQPort, AppSettings.RMQQueue);

        public static void Start()
        {
            try
            {
                receive.Initial();

                Thread thread = new Thread(() => receive.GetMessageRuntime());
                thread.Start();
            }
            catch (Exception e)
            {
                Utilities.WriteOperationLog("RabbitMqProcess", $"Error start : {e.Message}");
            }
        }

        public static void Stop()
        {
            try
            {
                receive.Disconnect();
            }
            catch (Exception e)
            {
                Utilities.WriteOperationLog("RabbitMqProcess", $"Error stop : {e.Message}");
            }
        }
    }
}
