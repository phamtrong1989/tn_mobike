﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Process
{
    class ApiProcess
    {
        private class OpenRfid
        {
            public bool status { get; set; }
            public object message { get; set; }
            public int errorCode { get; set; }
            public object totalRecord { get; set; }
            public int limit { get; set; }

            public OpenRfid()
            {
                
            }

            public OpenRfid(bool status, object message, int errorCode, object totalRecord, int limit)
            {
                this.status = status;
                this.message = message;
                this.errorCode = errorCode;
                this.totalRecord = totalRecord;
                this.limit = limit;
            }

            public override string ToString()
            {
                return JsonConvert.SerializeObject(this);
            }
        }

        public bool PushOpenRfid(string imei, string requestId, string rfid)
        {
            try
            {
                var url = $"{AppSettings.ApiPushLock}/{imei}/2?rfid={rfid}";
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", AppSettings.TokenApi);
                IRestResponse response = client.Execute(request);

                var data = JsonConvert.DeserializeObject<OpenRfid>(response.Content);

                Utilities.WriteOperationLog("PushOpenRfid", $"{requestId} | Imei: {imei} | RFID: {rfid} | Data: {data.ToString()}");

                return data != null && data.status;
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("PushOpenRfid", $"{requestId} | Imei: {imei} | Rfid: {rfid} |Error: {e.Message}");

                if (AppSettings.TurnOnLogC0)
                {
                    new MongoUtilities().AddLog(0, "PushStatusLock (Error) | Gửi trạng thái khóa API (Lỗi)", $"{requestId}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} |Error: {e.Message}", true);
                }

                return false;
            }
        }


        public void PushStatus(string imei, string requestId)
        {
            try
            {
                Task.Run(() => PushStatusLock(imei, requestId)).ConfigureAwait(false);
            }
            catch
            {
                //
            }
        }

        private async void PushStatusLock(string imei, string requestId)
        {
            try
            {
                var url = $"{AppSettings.ApiPushLock}/{imei}/1";
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", AppSettings.TokenApi);
                IRestResponse response = client.Execute(request);

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"PushStatusLock | Imei: { imei} | Url: { url} | Response: { response.Content}");
                }

                Utilities.WriteOperationLog("PushStatusLock", $"Imei: {imei} | Url: {url} | Response: {response.Content}");

                if (AppSettings.TurnOnLogL1)
                {
                    new MongoUtilities().AddLog(0, "PushStatusLock | Gửi trạng thái khóa API", $"{requestId}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Url: {url} | Response: {response.Content}");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("PushStatusLock", $"Imei: {imei} |Error: {e.Message}");

                if (AppSettings.TurnOnLogL1)
                {
                    new MongoUtilities().AddLog(0, "PushStatusLock (Error) | Gửi trạng thái khóa API (Lỗi)", $"{requestId}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} |Error: {e.Message}", true);
                }
            }

            await Task.Delay(1);
        }

        public void PushBikeWarning(string imei, EBikeWarningType warningType, DateTime time, string requestId)
        {
            try
            {
                BikeWarningSendCommand command = new BikeWarningSendCommand(imei, warningType, time);

                PushWarning(requestId, command);

                //Task.Run(() => PushWarning(requestId, command)).ConfigureAwait(true);
            }
            catch
            {
                //
            }
        }

        private async void PushWarning(string requestId, BikeWarningSendCommand data)
        {
            try
            {
                var client = new RestClient(AppSettings.ApiPushBikeWarning);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", AppSettings.TokenApi);

                request.AddHeader("Content-Type", "application/json");

                var body = $"{data.ToString()}";

                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"PushBikeWarning | Imei: { data.IMEI} | Url: { AppSettings.ApiPushBikeWarning} | Response: { response.Content}");
                }

                Utilities.WriteOperationLog("PushBikeWarning", $"Imei: {data.IMEI} | Url: {AppSettings.ApiPushBikeWarning} | Response: {response.Content} | Data: {data.ToString()}");

                if (AppSettings.TurnOnLogL1)
                {
                    new MongoUtilities().AddLog(0, "PushBikeWarning | Gửi cảnh báo xe API", $"{requestId}", $"{data.IMEI}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {data.IMEI} | Url: {AppSettings.ApiPushBikeWarning} | Response: {response.Content}");
                }

            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("PushBikeWarning", $"Imei: {data.IMEI} |Error: {e.Message}");

                if (AppSettings.TurnOnLogL1)
                {
                    new MongoUtilities().AddLog(0, "PushBikeWarning (Error) | Gửi cảnh báo xe (Lỗi)", $"{requestId}", $"{data.IMEI}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {data.IMEI} |Error: {e.Message}", true);
                }
            }

            await Task.Delay(1);
        }
    }
}
