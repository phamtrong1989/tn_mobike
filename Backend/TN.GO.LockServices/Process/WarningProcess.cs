﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.MinaCores;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Process
{
    class WarningProcess
    {
        private SqlHelper sqlHelper = new SqlHelper();
        private BikeWarningLogModel bikeWarningLogModel = new BikeWarningLogModel();
        private BikeWarningModel bikeWarningModel = new BikeWarningModel();
        private BookingModel bookingModel = new BookingModel();
        private DockModels dockModel = new DockModels();
        private ApiProcess apiProcess = new ApiProcess();
        private List<Tuple<Station, double>> resultStations = new List<Tuple<Station, double>>();

        public void ProcessWarningGps(string imei, string requestId, double lat, double lng, int dockId)
        {

            try
            {
                string note = "";
                Task.Run(() =>
                {
                    var warningType = EBikeWarningType.Undefined;
                    Utilities.WriteOperationLog("InsertBikeWarningTask.1", $"{requestId} | GPS Process | WarningType: {warningType}");

                    if (lat != 0 && lng != 0)
                    {
                        // + với khoảng cách trong trạm
                        var minLat = lat - AppSettings.InStationDelta;
                        var maxLat = lat + AppSettings.InStationDelta;
                        var minLng = lng - AppSettings.InStationDelta;
                        var maxLng = lng + AppSettings.InStationDelta;

                        Utilities.WriteOperationLog("InsertBikeWarningTask.2.1", $"{requestId} | Imei: {imei} | GPS OK | Lat: {lat} - Long: {lng} | MinLat: {minLat} -> MaxLat: {maxLat} | MinLong: {minLng} -> MaxLong: {maxLng}");

                        var stations = bikeWarningModel.FindByLatLng(minLat, maxLat, minLng, maxLng);
                        Utilities.WriteOperationLog("InsertBikeWarningTask.2.1.1", $"{requestId} | Imei: {imei} | Tìm kiếm trạm bằng Min => Max (Lat, Long) | Bán kính: {AppSettings.InStationDelta * 100000} m | Số trạm: {stations.Count}");

                        if (stations != null && stations.Count > 0)
                        {
                            foreach (var station in stations)
                            {
                                var check = Utilities.DistanceInMeterM(lat, lng, station.Lat, station.Lng);

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.0", $"{requestId} | Imei: {imei} | Thông số cấu hình trong trạm: {station.ReturnDistance} | Số mét: {check} | Trạm: {station.Id}  - {station.DisplayName}");
                                resultStations.Add(new Tuple<Station, double>(station, check));
                            }

                            Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.1", $"{requestId} | Imei: {imei} | Kiểm tra so với thông số cấu hình trạm | Số trạm thỏa mãn: {resultStations.Count}");

                            var booking = bookingModel.GetOneBooking(dockId);

                            if (resultStations.Count <= 0)
                            {
                                if (booking == null)
                                {
                                    warningType = EBikeWarningType.XeKhongTrongTram;
                                }

                                //dockModel.UpdateStation(imei, 0, requestId);
                                //dockModel.UpdateBike(dockId, 0, /*dock.Lat, dock.Long,*/ requestId, imei);

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.2", $"{requestId} | 0 trạm thỏa mãn | Cập nhật Station = 0");
                            }
                            else
                            {
                                var station = resultStations.OrderBy(x => x.Item2).FirstOrDefault();

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.3", $"{requestId} | Imei: {imei} | Trạm được chọn : {station.Item1.ToString()}");

                                dockModel.UpdateStation(imei, station.Item1.Id, requestId);
                                dockModel.UpdateBike(dockId, station.Item1.Id, /*dock.Lat, dock.Long,*/ requestId, imei);

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.2.3.1", $"{requestId} | Imei: {imei} | {resultStations.Count} trạm thỏa mãn | Cập nhật Station = {station.Item1.Id} | Số mét: {station.Item2}");
                            }

                        }
                        else
                        {
                            var overKm = AppSettings.InStationDelta * AppSettings.OverStationDelta;

                            minLat = lat - overKm;
                            maxLat = lat + overKm;
                            minLng = lng - overKm;
                            maxLng = lng + overKm;

                            Utilities.WriteOperationLog("InsertBikeWarningTask.2.3", $"{requestId} | Imei: {imei} | GPS OK | Lat: {lat} - Long: {lng} | MinLat: {minLat} - MaxLat: {maxLat} | MinLong: {minLng} - MaxLong: {maxLng}");

                            stations = bikeWarningModel.FindByLatLng(minLat, maxLat, minLng, maxLng);

                            if (stations != null && stations.Count > 0)
                            {
                                foreach (var station in stations)
                                {
                                    var check = Utilities.DistanceInMeterM(lat, lng, station.Lat, station.Lng);
                                    resultStations.Add(new Tuple<Station, double>(station, check));

                                    Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.2.0", $"{requestId} | Imei: {imei} | Số mét: {check} | Trạm: {station.Id} - {station.DisplayName}");
                                }

                                var stationMin = resultStations.OrderBy(x => x.Item2).FirstOrDefault();

                                var booking = bookingModel.GetOneBooking(dockId);

                                if (booking == null)
                                {
                                    dockModel.UpdateStation(imei, stationMin.Item1.Id, requestId);
                                    dockModel.UpdateBike(dockId, stationMin.Item1.Id, /*dock.Lat, dock.Long,*/ requestId, imei);
                                }

                                var map = $"https://maps.google.com?q={lat},{lng}";
                                var km = Math.Round(stationMin.Item2 / 1000, 3);

                                note = $"Trạm gần nhất: {stationMin?.Item1.DisplayName} | Tọa độ xe: {map} | Khoảng cách vs trạm: {km} KM";

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.2.1", $"{requestId} | Imei: {imei} | {note}");

                                if (km >= AppSettings.OverStationDelta)
                                {
                                    warningType = EBikeWarningType.XeDiQuaXa;

                                    Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.2.1", $"{requestId} | Imei: {imei} | {note} | Xe đi quá xa");
                                }
                            }
                            else
                            {
                                warningType = EBikeWarningType.XeDiQuaXa;

                                Utilities.WriteOperationLog("InsertBikeWarningTask.2.3.3", $"{requestId} | Imei: {imei} | {note} | Xe đi quá xa | Station Null");
                            }
                        }

                        resultStations.Clear();
                    }
                    else
                    {
                        Utilities.WriteOperationLog("InsertBikeWarningTask.3", $"{requestId} | Xe lỗi GPS | Lat: {lat} - Long: {lng}");
                        warningType = EBikeWarningType.XeLoiGPS;
                    }

                    if (warningType != EBikeWarningType.Undefined)
                    {
                        AddWarning(imei, requestId, warningType, lat, lng, note);
                    }
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessWarningGps", $"Error: {e.Message}");
            }
        }

        public void ProcessWarning(string imei, string requestId, EBikeWarningType warningType)
        {
            try
            {
                string note = "";
                Task.Run(() =>
                {
                    AddWarning(imei, requestId, warningType, 0, 0, note);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessWarningGps", $"Error: {e.Message}");
            }
        }

        private async void AddWarning(string imei, string requestId, EBikeWarningType warningType, double lat, double lng, string note = "")
        {
            var bikeWarning = new BikeWarning(imei, requestId, warningType, lat, lng, true, note, DateTime.Now);

            await InsertWarning(bikeWarning);
        }

        private async Task InsertWarning(BikeWarning bikeWarning)
        {
            try
            {
                if (bikeWarning.WarningType != EBikeWarningType.Undefined)
                {
                    Utilities.WriteOperationLog("InsertWarning.1.0.0", $"{bikeWarning.Transaction} | WarningType is {bikeWarning.WarningType}");

                    BikeWarningCount warningCount = null;

                    try
                    {
                        warningCount = TNSocketHandlerV2.ListBikeWarningCounts.Find(x => x.IMEI == bikeWarning.IMEI && x.EBikeWarningType == bikeWarning.WarningType);
                        Utilities.WriteOperationLog("InsertWarning.1.0.0.1", $"{bikeWarning.Transaction} | Find success warningCount : {warningCount?.ToString()}");
                    }
                    catch (Exception e)
                    {
                        warningCount = new BikeWarningCount(bikeWarning);

                        Utilities.WriteOperationLog("InsertWarning.1.0.0.1", $"{bikeWarning.Transaction} | Not Found ! | Error: {e.Message}");
                    }


                    if (warningCount != null)
                    {
                        Utilities.WriteOperationLog("InsertWarning.1.0.1", $"{bikeWarning.Transaction} | warningCount is not null");

                        var check = false;

                        switch (warningCount.EBikeWarningType)
                        {
                            case EBikeWarningType.XeDiQuaXa:
                            case EBikeWarningType.XeLoiGPS:
                            case EBikeWarningType.XeKhongTrongTram:
                                if (warningCount.Count >= 3)
                                {
                                    check = true;
                                }
                                break;

                            case EBikeWarningType.DiChuyenBatHopPhap:
                            case EBikeWarningType.DoXe:
                                if (warningCount.Count >= 2)
                                {
                                    check = true;
                                }
                                break;
                        }

                        if (check)
                        {
                            Utilities.WriteOperationLog("InsertWarning.1.0.2", $"{bikeWarning.Transaction} | Warning count = {warningCount.Count} | Warning type: {warningCount.EBikeWarningType} => Push API");

                            apiProcess.PushBikeWarning(bikeWarning.IMEI, bikeWarning.WarningType, bikeWarning.UpdatedDate, bikeWarning.Transaction);

                            TNSocketHandlerV2.ListBikeWarningCounts.Remove(warningCount);

                            var getBikeList = bikeWarningModel.GetOneBikeWarning(bikeWarning.IMEI, bikeWarning.WarningType);

                            Utilities.WriteOperationLog("InsertWarning.1.0.3", $"{bikeWarning.Transaction} | Get Bike Warning List: {getBikeList.Count}");

                            if (getBikeList != null && getBikeList.Count > 0)
                            {
                                var getBike = getBikeList.FirstOrDefault();

                                var time = (bikeWarning.UpdatedDate - getBike.UpdatedDate).TotalSeconds;

                                Utilities.WriteOperationLog("InsertWarning.1.0.4", $"{bikeWarning.Transaction} | GetBike is not null | Time: {time}");

                                if (time > AppSettings.TimeCheckBikeWarning)
                                {
                                    sqlHelper.InsertOne<BikeWarning>(bikeWarning);

                                    var bikeLog = new BikeWarningLog(getBike);

                                    bikeWarningLogModel.InsertBikeWarning(bikeLog);

                                    bikeWarningModel.DeleteBikeWarning(getBike.Id);

                                    Utilities.WriteOperationLog("InsertWarning.1.0.5", $"{bikeWarning.Transaction} | Total Second = {time} > {AppSettings.TimeCheckBikeWarning} | Insert BikeWarning => Insert BikeWarningLog => Delete BikeWarning Old: ID = {getBike.Id}");
                                }
                                else
                                {
                                    sqlHelper.UpdateOne<BikeWarning>(bikeWarning, $"IMEI = '{bikeWarning.IMEI}' AND WarningType = {Convert.ToInt32(bikeWarning.WarningType)}");

                                    Utilities.WriteOperationLog("InsertWarning.1.0.5", $"{bikeWarning.Transaction} | Total Second = {time} <= {AppSettings.TimeCheckBikeWarning} | Update BikeWarning: IMEI = '{bikeWarning.IMEI}' AND WarningType = {Convert.ToInt32(bikeWarning.WarningType)}");
                                }
                            }
                            else
                            {
                                sqlHelper.InsertOne<BikeWarning>(bikeWarning);

                                Utilities.WriteOperationLog("InsertWarning.1.0.4", $"{bikeWarning.Transaction} | GetBike is null | Insert BikeWarning");
                            }

                            warningCount.Count = 0;
                            warningCount.LastTime = DateTime.Now;
                            TNSocketHandlerV2.ListBikeWarningCounts.Add(warningCount);

                            Utilities.WriteOperationLog("InsertWarning.1.0.6", $"{bikeWarning.Transaction} | List WarningCount = {TNSocketHandlerV2.ListBikeWarningCounts.Count}");

                        }
                        else
                        {
                            Utilities.WriteOperationLog("InsertWarning.1.0.2", $"{bikeWarning.Transaction} | Warning count = {warningCount.Count} | Warning type: {warningCount.EBikeWarningType}");

                            var time2Warning = (DateTime.Now - warningCount.LastTime).TotalSeconds;

                            Utilities.WriteOperationLog("InsertWarning.1.0.3", $"{bikeWarning.Transaction} | Time 2 Warning : {time2Warning}");

                            TNSocketHandlerV2.ListBikeWarningCounts.Remove(warningCount);

                            if (time2Warning < AppSettings.TotalTime2Warning)
                            {
                                warningCount.Count += 1;
                                warningCount.LastTime = DateTime.Now;
                            }
                            else
                            {
                                warningCount.Count = 1;
                                warningCount.LastTime = DateTime.Now;
                            }

                            Utilities.WriteOperationLog("InsertWarning.1.0.3", $"{bikeWarning.Transaction} | WarningType : {warningCount.EBikeWarningType} | Count : {warningCount.Count} | LastTime: {warningCount.LastTime}");

                            TNSocketHandlerV2.ListBikeWarningCounts.Add(warningCount);

                            Utilities.WriteOperationLog("InsertWarning.1.0.4", $"{bikeWarning.Transaction} | List WarningCount = {TNSocketHandlerV2.ListBikeWarningCounts.Count}");

                        }
                    }
                    else
                    {
                        Utilities.WriteOperationLog("InsertWarning.1.0.1", $"{bikeWarning.Transaction} | warningCount is null | Add new WarningCount");

                        warningCount = new BikeWarningCount(bikeWarning);

                        TNSocketHandlerV2.ListBikeWarningCounts.Add(warningCount);

                        Utilities.WriteOperationLog("InsertWarning.1.0.2", $"{bikeWarning.Transaction} | List WarningCount = {TNSocketHandlerV2.ListBikeWarningCounts.Count}");
                    }
                }
                else
                {
                    Utilities.WriteOperationLog("InsertWarning", $"{bikeWarning.Transaction} | WarningType is not pass : Undefined");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("InsertWarning", $"{bikeWarning.Transaction} | Error: {e}");
            }

            await Task.Delay(1);
        }
    }
}
