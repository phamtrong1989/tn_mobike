﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Quartz;
using Quartz.Impl;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Process
{
    class Charging
    {
        public string Imei { get; set; }
        public double OldPin { get; set; }
        public DockModels.ChargingE Status { get; set; }
        public long Id { get; set; }
        public int TotalDiffStatus { get; set; }
        public int SamePin { get; set; }

        public Charging()
        {

        }

        public Charging(string imei, double oldPin, DockModels.ChargingE status, long id)
        {
            Imei = imei;
            OldPin = oldPin;
            Status = status;
            Id = id;
        }

        public Charging(string imei, double oldPin, DockModels.ChargingE status, long id, int totalDiffStatus)
        {
            Imei = imei;
            OldPin = oldPin;
            Status = status;
            Id = id;
            TotalDiffStatus = totalDiffStatus;
        }

        public Charging(string imei, double oldPin, DockModels.ChargingE status, long id, int totalDiffStatus, int samePin)
        {
            Imei = imei;
            OldPin = oldPin;
            Status = status;
            Id = id;
            TotalDiffStatus = totalDiffStatus;
            SamePin = samePin;
        }
    }

    class CheckCharging
    {
        public static Dictionary<string, Charging> LastPercentPin = new Dictionary<string, Charging>();

        private DockModels dockModels = new DockModels();
        private BookingModel bookingModel = new BookingModel();

        public void ProcessCharging(string imei, double newPin)
        {
            try
            {
                Task.Run(() => CheckCharge(imei, newPin));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async void CheckCharge(string imei, double newPin)
        {
            try
            {
                Utilities.WriteDebugLog("CheckCharge", $"1: Imei: {imei} | Pin New: {newPin}");
                var charging = LastPercentPin[imei];

                var deviantPin = newPin - charging.OldPin;

                Utilities.WriteDebugLog("CheckCharge", $"2: Imei: {imei} | Pin Old: {charging.OldPin} - Status: {charging.Status} | Chênh lệch: {deviantPin} | Total diff : {charging.TotalDiffStatus} | Total Same: {charging.SamePin}");

                DockModels.ChargingE status = DockModels.ChargingE.NotCharging;

                if (deviantPin > 0)
                {
                    status = DockModels.ChargingE.IsCharging;
                }
                else if(deviantPin == 0)
                {
                    status = charging.Status;
                    charging.SamePin += 1;
                }
                // Chênh lệch < 0
                else
                {
                    status = DockModels.ChargingE.NotCharging;
                }

                Utilities.WriteDebugLog("CheckCharge", $"3: Imei: {imei} | Chênh lệch: {deviantPin} | Status: {status} | Total Same: {charging.SamePin}");

                if (charging.Status != status)
                {

                    if (charging.Status == DockModels.ChargingE.NotCharging && status == DockModels.ChargingE.IsCharging)
                    {
                        // Update Status Charging
                        dockModels.UpdateCharging(imei, status);
                        Utilities.WriteDebugLog("CheckCharge", $"4: Imei: {imei} | Status cũ: {charging.Status} | Status mới: {status} => UPDATE status: {status}");

                        charging.TotalDiffStatus = 0;
                        charging.SamePin = 0;
                    }
                    else
                    {
                        if (charging.TotalDiffStatus >= 2)
                        {
                            // Update Status Charging
                            dockModels.UpdateCharging(imei, status);
                            Utilities.WriteDebugLog("CheckCharge", $"5: Imei: {imei} | Số lần khác status {charging.TotalDiffStatus} => UPDATE status: {status}");

                            charging.TotalDiffStatus = 0;
                            charging.SamePin = 0;
                        }
                        else
                        {
                            charging.TotalDiffStatus += 1;
                        }
                    }
                }
                else // (Status sạc giống nhau)
                {
                    // Not Update
                    if (charging.SamePin >= 2)
                    {
                        status = DockModels.ChargingE.NotCharging;
                        dockModels.UpdateCharging(imei, status);
                        Utilities.WriteDebugLog("CheckCharge", $"6: Imei: {imei} | Số lần cùng chỉ sổ pin {charging.SamePin} => UPDATE status: {status}");

                        charging.TotalDiffStatus = 0;
                        charging.SamePin = 0;
                    }
                }

                LastPercentPin[imei] = new Charging(imei, newPin, status, charging.Id, charging.TotalDiffStatus, charging.SamePin);

            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("CheckCharge", $"Error: {e.Message}");
                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, "CheckCharge (Error) | Tự động kiểm tra trạng thái sạc (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "", MongoUtilities.EnumMongoLogType.Start, $"Error: {e.Message}", true);
                }
            }

            await Task.Delay(1);
        }
    }
}
