﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using MongoDB.Bson.IO;
using ProtoBuf;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.MinaCores;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace TN.GO.LockServices.RabbitMQ
{
    class RabbitMqReceive
    {
        public bool IsSendThread { get; set; }

        public string StrAddress { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Queue { get; set; }
        public int Port { get; set; }

        private IConnection _connection;
        private IModel _model;
        private ConnectionFactory _factory = null;
        //private EventingBasicConsumer _consumer = null;
        private QueueingBasicConsumer _consumer = null;

        //private bool connect = true;
        private bool RunLoop = true;
        private bool _rabbitConnection = true;
        bool DisplayConnectRBMQFailer = false;
        
        public RabbitMqReceive()
        {

        }

        public RabbitMqReceive(string strAddress, string user, string password, int port, string queue)
        {
            StrAddress = strAddress;
            User = user;
            Password = password;
            Port = port;
            Queue = queue;
        }

        public void Initial()
        {
            try
            {
                _factory = new ConnectionFactory();
                _factory.HostName = StrAddress;
                _factory.UserName = User;
                _factory.Password = Password;
                _factory.Port = Port;
                _factory.Protocol = Protocols.DefaultProtocol;
                _factory.AutomaticRecoveryEnabled = true;

                _connection = _factory.CreateConnection();
                _model = _connection.CreateModel();
                _consumer = new QueueingBasicConsumer(_model);

                _model.BasicConsume(Queue, true, _consumer);
                _model.BasicQos(0, 1, true);
                _rabbitConnection = true;
                DisplayConnectRBMQFailer = false;
            }
            catch (Exception e)
            {
                Disconnect();
                if (_rabbitConnection)
                {
                    _rabbitConnection = false;
                    if (!DisplayConnectRBMQFailer)
                    {
                        Utilities.WriteErrorLog("RabbitMQReceive.Initial", $"Error : Connect to RBMQ fail! | Message : {e.Message}");
                        DisplayConnectRBMQFailer = true;
                    }
                }
                else
                {
                    if (!DisplayConnectRBMQFailer)
                    {
                        Utilities.WriteErrorLog("RabbitMQReceive.Initial", $"Error : Connect to RBMQ fail! | Message : {e.Message}");
                        DisplayConnectRBMQFailer = true;
                    }
                }
            }
        }

        public void Disconnect()
        {
            if (_model != null && _connection != null)
            {
                RunLoop = false;
                _model.Close(200, "Disconnected");
                if (_connection.IsOpen)
                {
                    _connection.Close();
                }
            }
        }

        public void GetMessageRuntime()
        {
            try
            {
                while (RunLoop)
                {
                    GetMessageNew();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void GetMessageNew()
        {
            if (_connection != null && _connection.IsOpen)
            {
                try
                {
                    var message = "";
                    try
                    {
                        BasicDeliverEventArgs e = null;
                        if (_consumer.Queue.Dequeue(100, out e) == false)
                            return;
                        if (e == null) return;
                        byte[] body = e.Body;
                        using (var stream = new MemoryStream(body))
                        {
                            message = Serializer.Deserialize<string>(stream);
                        }

                        Utilities.WriteOperationLog("RabbitMQ.Receive", $"Message: {message}");

                        // CODE

                        new ProcessD0Rabbit().ProcessDockInD0Queue(message);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
                catch (Exception e)
                {
                    if (e is AlreadyClosedException || e is EndOfStreamException)
                    {
                        Initial();
                        return;
                    }
                    return;
                }
            }
            else
            {
                Initial();
            }
        }

    }

    class ProcessD0Rabbit
    {
        private DockModels dockModel = new DockModels();
        private BookingModel bookingModel = new BookingModel();
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private string rfidCode = AppSettings.DefaultRFID;

        public void ProcessDockInD0Queue(string message)
        {
            try
            {
                Task.Run(() =>
                {
                    HandlerDockInD0(message);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessDockInD0Queue", $"Error: {e.Message}");
            }
        }

        private async void HandlerDockInD0(string message)
        {
            var dataD0Rabbit = JsonConvert.DeserializeObject<DataD0Rabbit>(message);

            await DockInD0(dataD0Rabbit);
        }

        private async Task DockInD0(DataD0Rabbit data)
        {
            try
            {

                IoSession session = null;

                if (JobCheckConnection.DataConnection.TryGetValue(data.Imei, out var value))
                {
                    session = value.Session;
                }

                var dock = dockModel.GetOneDock(data.Imei);
                if (dock != null && dock.Id > 0)
                {
                    dock.LastConnectionTime = DateTime.Now;
                    dock.ConnectionStatus = true;
                    dock.UpdatedDate = DateTime.Now;

                    if (data.Lat != 0 && data.Lng != 0)
                    {
                        if (String.IsNullOrEmpty(dock.RFID))
                        {
                            // Gửi lệnh khóa RFID
                            var messageRFID = $"*CMDS,NP,{data.Imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                            TNSessionMap.SendMessage(session, data.Imei, messageRFID, data.RequestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                            dock.RFID = rfidCode;
                        }

                        dock.LastGPSTime = DateTime.Now;
                        dock.Lat = data.Lat;
                        dock.Long = data.Lng;

                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);
                        dockModel.UpdateBike(dock.Id, dock.Lat, dock.Long, data.RequestId, dock.IMEI);

                        Utilities.WriteOperationLog("ProcessMessageD0.Update", $"RequestId: {data.RequestId} | Imei: {data.Imei} | D0 - POSITIONING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lat: {data.Lat} - Long: {data.Lng} | Lock Status: {dock.LockStatus}");

                        if (AppSettings.TurnOnLogD0)
                        {
                            mongoUtilities.AddLog(0, "D0 (Update) | Vị trí (Cập nhật khóa)", data.RequestId, data.Imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {data.Imei} | D0 - POSITIONING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lat: {data.Lat} - Long: {data.Lng} | Lock Status: {dock.LockStatus}");
                        }
                    }
                    else
                    {
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);
                        Utilities.WriteOperationLog("ProcessMessageD0.Update", $"RequestId: {data.RequestId} | Imei: {data.Imei} | D0 - POSITIONING COMMAND | Last GPS Time: {dock.LastGPSTime} | Lock Status: {dock.LockStatus}");
                    }

                    var booking = bookingModel.GetOneBooking(dock.Id);
                    if (booking == null)
                    {
                        // Gửi gói tin tắt định vị tự động D1
                        var messageD1Off = $"*CMDS,NP,{data.Imei},{DateTime.Now:yyMMddHHmmss},D1,0#<LF>";
                        TNSessionMap.SendMessage(session, data.Imei, messageD1Off, data.RequestId, true, "Turn Off D1", "Tắt định vị tự động", MongoUtilities.EnumMongoLogType.Center);

                        Utilities.WriteOperationLog("ProcessMessageD0.TurnOffD0", $"RequestId: {data.RequestId} | Imei: {data.Imei} | Id: {dock.Id} | D0 - LOCK COMMAND | Tắt định vị tự động - Xe không trong chuyến");

                        if (JobCheckWarning.DATACHECKWARNINGS.TryGetValue(data.Imei, out var warning))
                        {
                            if (warning.DataGps.Count >= 50)
                            {
                                warning.DataGps.RemoveAt(0);
                            }
                            warning.DataGps.Add(new CheckGps(data.Lat, data.Lng, DateTime.Now));
                        }
                        else
                        {
                            var checkWarning = new CheckWarningEntity(data.Imei, data.Lat, data.Lng, DateTime.Now);
                            JobCheckWarning.DATACHECKWARNINGS.Add(data.Imei, checkWarning);
                        }
                    }
                    else
                    {
                        Utilities.WriteOperationLog("ProcessMessageD0.TurnOffD0", $"RequestId: {data.RequestId} | Imei: {data.Imei} | Id: {dock.Id} | D0 - LOCK COMMAND | Giữ định vị tự động - Xe đang trong chuyến");
                    }
                }
                else
                {
                    dock = new Dock(data.Imei, 0, data.Lat, data.Lng, true);
                    dockModel.InsertDock(dock);


                    Utilities.WriteOperationLog("ProcessMessageD0.Insert", $"RequestId: {data.RequestId} | Imei: {data.Imei} | D0 - POSITIONING COMMAND | Dock: {dock.ToString()}");

                    if (AppSettings.TurnOnLogD0)
                    {
                        mongoUtilities.AddLog(0, "D0 (Insert) | Vị trí (Thêm mới khóa)", data.RequestId, data.Imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {data.Imei} | D0 - POSITIONING COMMAND | Dock: {dock.ToString()}");

                    }
                }

                SqlHelper.InsertGPSTask(data.Imei, data.Lat, data.Lng, data.RequestId);

                new WarningProcess().ProcessWarningGps(data.Imei, data.RequestId, data.Lat, data.Lng, dock.Id);

            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("DockInD0", $"RequestId: {data.RequestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
