﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ProtoBuf;
using RabbitMQ.Client;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.RabbitMQ
{
    class RabbitMqPush
    {
        public IModel _model;
        public ConnectionFactory _factory = null;
        public IConnection _connection = null;

        //public string StrConnection { get; set; }
        public string StrAddress { get; set; }
        public string Exchanges { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }

        /// <summary>
        /// Khởi tạo connect với RabbitMQ
        /// </summary>
        /// <param name="strAddress">Địa chỉ IP</param>
        /// <param name="user">Tài khoản</param>
        /// <param name="password">Mật khẩu</param>
        /// <param name="port">Cổng</param>
        public RabbitMqPush(string strAddress, string exchanges, string user, string password, int port)
        {
            StrAddress = strAddress;
            User = user;
            Password = password;
            Port = port;
            Exchanges = exchanges;
            try
            {
                _factory = new ConnectionFactory();
                _factory.HostName = strAddress;
                _factory.UserName = user;
                _factory.Password = password;
                _factory.Protocol = Protocols.DefaultProtocol;
                _factory.Port = port;
                _factory.VirtualHost = "/";
                _connection = _factory.CreateConnection();
                _model = _connection.CreateModel();
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("RabbitMQPush.Initial", $"Error: {e.Message}");
            }
        }

        private bool connected = true;

        /// <summary>
        /// Khởi tạo lại kết nối
        /// </summary>
        public void Reconnect()
        {
            Thread.Sleep(1000);
            try
            {
                Disconnecting();
                _factory = new ConnectionFactory();
                _factory.HostName = StrAddress;
                _factory.UserName = User;
                _factory.Password = Password;
                _factory.Protocol = Protocols.DefaultProtocol;
                _factory.Port = Port;
                _factory.VirtualHost = "/";
                _connection = _factory.CreateConnection();
                _model = _connection.CreateModel();
                if (!connected)
                {
                    connected = true;
                }
            }
            catch (Exception e)
            {
                if (connected)
                {
                    Utilities.WriteErrorLog("RabbitMQPush.Reconnect", $"Error : {e.Message.ToString()}");
                    connected = false;
                }
                else
                {
                    Utilities.WriteErrorLog("RabbitMQPush.Reconnect", "Error : Connect fail !");
                }
            }
        }

        /// <summary>
        /// Ngắt kết nối
        /// </summary>
        public void Disconnecting()
        {
            _model.Close(200, "Disconnected");
            if (_connection.IsOpen)
            {
                _connection.Close();
            }
        }


        byte[] Serialize(string body)
        {
            try
            {
                using (var stream = new MemoryStream())
                {
                    Serializer.Serialize(stream, body);
                    stream.Position = 0;
                    return stream.ToArray();
                }
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("RabbitMQPush.Serialize", $"Error : {ex.Message}");
                return null;
            }
        }

        private bool error = false;
        public void PushMessage(string message)
        {
            try
            {
                if (Serialize(message) != null)
                {
                    _model.BasicPublish(Exchanges, "", null, Serialize(message));
                    Utilities.WriteOperationLog("RabbitMQPush.PushMessage", $"Push message success to Queue:");
                }
                if (error) error = false;
            }
            catch (Exception ex)
            {
                if (!error) error = true;
                Utilities.WriteErrorLog("RabbitMQPush.Serialize", $"Error : {ex.Message}");
                Reconnect();
            }
        }
    }
}
