﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using TN.GO.LockServices.MinaCores;
using TN.GO.LockServices.MinaCores.HandlerCmd;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.JobScheduler
{
    class JobAutoSendD1
    {
        private static IScheduler scheduler;
        public static void Start()
        {
            try
            {
                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
                //Job capture frames

                IJobDetail job = JobBuilder.Create<SendD0Auto>().Build();
                ITrigger restartTrigger = TriggerBuilder.Create()
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(AppSettings.TimeCheckLocation)
                        .RepeatForever())
                    .Build();
                scheduler.ScheduleJob(job, restartTrigger);

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobAutoSendD1_Start", ex.ToString());
                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, $"JobAutoSendD1 (Error) | Tự động kiểm tra vị trí (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobAutoSendD1_Start | Error: {ex.Message}", true);

                }
            }
        }

        public static void ReStart()
        {
            try
            {
                scheduler?.Shutdown();

                Thread.Sleep(1000);

                Start();
            }
            catch (Exception e)
            {
                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, $"JobAutoSendD1 (Error) |  Tự động kiểm tra vị trí (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobAutoSendD1_Restart | Error: {e.Message}", true);

                }
            }
        }

        public static void Stop()
        {
            try
            {
                scheduler?.Shutdown();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobAutoSendD1_Stop", ex.ToString());

                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, $"JobAutoSendD1 (Error) | Tự động kiểm tra vị trí (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobAutoSendD1_Stop | Error: {ex.Message}", true);

                }
            }
        }
    }

    class SendD0Auto : IJob
    {
        public bool IsRunning = false;
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private StationModels StationModel = new StationModels();
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (IsRunning)
                {
                    return;
                }

                IsRunning = true;

                HandlerCmdC0.listRFID = Utilities.ReadRFID("RFID.txt");

                JobScanScheduler.ListStations = StationModel.GetAll();

                foreach (var dataSession in TNSessionMap.MAPIMEI)
                {
                    var requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";
                    // *CMDS,NP,123456789123456,200318123020,D0#<LF>
                    var message = $"*CMDS,NP,{dataSession.Key},{DateTime.Now:yyMMddHHmmss},D0#<LF>";

                    var result = TNSessionMap.SendMessage(dataSession.Value, $"{dataSession.Key}", message, requestId, true, "CheckD0", "Kiểm tra vị trí", MongoUtilities.EnumMongoLogType.Start);
                    var result2 = TNSessionMap.SendMessageStr(dataSession.Value, $"{dataSession.Key}", message, requestId, true, "CheckD0", "Kiểm tra vị trí", MongoUtilities.EnumMongoLogType.Start);

                    var status = "";

                    status = result ? $"Send message {message} successful !" : $"Send message {message} fail !";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Message ARR : {message}");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : {status}");
                        Console.WriteLine("====================================================================================================");
                    }

                    status = result2 ? $"Send message {message} successful !" : $"Send message {message} fail !";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Message STR : {message}");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : {status}");
                        Console.WriteLine("====================================================================================================");
                    }
                }

                IsRunning = false;
            }
            catch (Exception e)
            {
                IsRunning = false;
                Utilities.WriteErrorLog("SendD0Auto.Execute", $"Error: {e.Message}");
                if (AppSettings.TurnOnLogStartSV)
                {
                    mongoUtilities.AddLog(0, "SendD0 (Error) | Kiểm tra vị trí (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "", MongoUtilities.EnumMongoLogType.Start, $"Error: {e.Message}", true);
                }
            }
        }
    }
}
