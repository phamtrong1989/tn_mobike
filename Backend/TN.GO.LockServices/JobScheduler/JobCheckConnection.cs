﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Mina.Core.Session;
using Quartz;
using Quartz.Impl;
using TN.GO.LockServices.Entity.SessionEntity;
using TN.GO.LockServices.MinaCores;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.JobScheduler
{
    class JobCheckConnection
    {
        private static IScheduler scheduler;
        public static Dictionary<string, DataSession> DataConnection = new Dictionary<string, DataSession>();
        public static void Start()
        {
            try
            {
                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
                //Job capture frames

                IJobDetail job = JobBuilder.Create<CheckConnection>().Build();
                ITrigger restartTrigger = TriggerBuilder.Create()
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(AppSettings.TimeJobCheckConnection)
                        .RepeatForever())
                    .Build();
                scheduler.ScheduleJob(job, restartTrigger);

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobCheckConnection_Start", ex.ToString());
                if (AppSettings.TurnOnLogCheckConnect)
                {
                    new MongoUtilities().AddLog(0, $"JobCheckConnection (Error) | Tự động kiểm tra kết nối (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobCheckConnection_Start | Error: {ex.Message}", true);

                }
            }
        }

        public static void ReStart()
        {
            try
            {
                scheduler?.Shutdown();

                Thread.Sleep(1000);

                Start();
            }
            catch (Exception e)
            {
                if (AppSettings.TurnOnLogCheckConnect)
                {
                    new MongoUtilities().AddLog(0, $"JobCheckConnection (Error) |  Tự động kiểm tra kết nối (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobCheckConnection_Restart | Error: {e.Message}", true);

                }
            }
        }

        public static void Stop()
        {
            try
            {
                scheduler?.Shutdown();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("restartJob_Stop", ex.ToString());

                if (AppSettings.TurnOnLogCheckConnect)
                {
                    new MongoUtilities().AddLog(0, $"JobCheckConnection (Error) | Tự động kiểm tra kết nối (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobCheckConnection_Stop | Error: {ex.Message}", true);

                }
            }
        }
    }

    class CheckConnection : IJob
    {
        public bool IsRunning = false;
        private DockModels dockModel = new DockModels();
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private StationModels StationModel = new StationModels();
        //public static long Count = 0;
        public void Execute(IJobExecutionContext context)
        {
            var timeCheck = DateTime.Now;
            try
            {
                if (IsRunning)
                {
                    return;
                }

                IsRunning = true;

                //Count += 1;

                if (AppSettings.ModeRun)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("====================================================================================================");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Total Session Open (IP)   : {TNSessionMap.MAPIP.Count}");
                    Console.WriteLine($"Total Session Open (IMEI) : {TNSessionMap.MAPIMEI.Count}");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("====================================================================================================");
                    Console.WriteLine($"Start Check connection / {AppSettings.TimeJobCheckConnection} s: {JobCheckConnection.DataConnection.Count} Lock | TimeCheck: {AppSettings.TimeCheckConnection} s");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Utilities.WriteOperationLog("CheckConnection", $"Tổng session bắt đầu kiểm tra : {JobCheckConnection.DataConnection.Count}");

                Dictionary<string, DataSession> dataRange = new Dictionary<string, DataSession>();

                JobScanScheduler.ListStations = StationModel.GetAll();

                CheckCharging.LastPercentPin = dockModel.GetOldPinFirst();

                dataRange = JobCheckConnection.DataConnection;

                if (JobCheckConnection.DataConnection.Count > 0)
                {
                    foreach (var data in JobCheckConnection.DataConnection)
                    {
                        IPEndPoint endPoint = data.Value.Session.RemoteEndPoint as IPEndPoint;
                        var totalSecond = (int) (timeCheck - data.Value.LastTime).TotalSeconds;

                        double timeOut = 0;

                        var checkImei = TNSessionMap.MAPIMEI.TryGetValue(Convert.ToInt64(data.Key), out var sessionImei);
                        var CheckIp = TNSessionMap.MAPIP.TryGetValue($"{endPoint.Address}:{endPoint.Port}:{data.Value.Session.Id}", out var sessionIP);

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"Imei: {data.Key} - CheckImei: {checkImei} | IP: {endPoint?.Address} - CheckIP: {CheckIp}");
                        }

                        if (data.Value.LastSendL0 != DateTime.MinValue)
                        {
                            timeOut = data.Value.LastReceiverL0 != DateTime.MinValue ? (data.Value.LastReceiverL0 - data.Value.LastSendL0).TotalSeconds : (DateTime.Now - data.Value.LastSendL0).TotalSeconds;
                        }

                        if (totalSecond >= AppSettings.TimeCheckConnection || !data.Value.Session.Connected || (!checkImei || !CheckIp) || timeOut > AppSettings.TimeOut)
                        {
                            dockModel.UpdateStatus(data.Key, 0);

                            //SessionMap.NewInstance().RemoveSession(data.Value.Item3);
                            
                            Utilities.WriteOperationLog("CheckConnection", $"Tổng session trước khi xóa : {dataRange.Count}");
                            TNSessionMap.RemoveSessionImei(data.Value.Session);
                            TNSessionMap.RemoveSession(data.Value.Session);

                            if (AppSettings.ModeRun)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine($"LOCK {data.Key} DISCONNECT | Last connect: {data.Value.LastTime:yyyy-MM-dd HH:mm:ss.fff} | Id: {data.Value.Session.Id} | EndPoint: {endPoint?.Address} | Code: {data.Value.Key} | Total second: {totalSecond}");
                                Console.ForegroundColor = ConsoleColor.White;

                                Console.WriteLine("----------------------------------------------------------------------------------------------------");
                            }

                            dataRange.Remove(data.Key);

                            Utilities.WriteOperationLog("CheckConnection", $"Tổng session sau khi xóa : {dataRange.Count}");

                            Utilities.WriteOperationLog("CheckConnection", $"LOCK {data.Key} DISCONNECT | Last connect: {data.Value.LastTime:yyyy-MM-dd HH:mm:ss.fff} | EndPoint: {endPoint?.Address}:{endPoint?.Port} | Id: {data.Value.Session.Id} | {data.Value.Key} | Total second: {totalSecond} | CheckImei: {checkImei} | CheckIp: {CheckIp} | Connect: {data.Value.Session.Connected} | TimeOut: {timeOut}");

                            if (AppSettings.TurnOnLogCheckConnect)
                            {
                                mongoUtilities.AddLog(0, "CheckConnection | Kiểm tra kết nối", $"{timeCheck:yyyyMMddHHmmssffffff}", data.Key, MongoUtilities.EnumMongoLogType.Start, $"LOCK {data.Key} DISCONNECT | Last connect: {data.Value.LastTime:yyyy-MM-dd HH:mm:ss.fff} | {data.Value.Key} | Total second: {totalSecond}");

                            }
                        }
                        else
                        {
                            if (AppSettings.ModeRun)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine($"LOCK {data.Key} CONNECTED | Last connect: {data.Value.LastTime:yyyy-MM-dd HH:mm:ss.fff} | Id: {data.Value.Session.Id} | EndPoint: {endPoint?.Address} | Code: {data.Value.Key} | Connect: {data.Value.Session.Connected} | Total second: {totalSecond}");
                                Console.ForegroundColor = ConsoleColor.White;

                                Console.WriteLine("----------------------------------------------------------------------------------------------------");
                            }

                            if (dataRange.TryGetValue(data.Key, out var value))
                            {
                                value.TimeOut = timeOut;
                            }

                            continue;
                        }
                    }
                }

                //JobCheckConnection.DataConnection.Clear();
                JobCheckConnection.DataConnection = dataRange;

                if (AppSettings.ModeRun)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("====================================================================================================");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                IsRunning = false;
            }
            catch (Exception e)
            {
                IsRunning = false;
                Utilities.WriteErrorLog("JobScanDB.Execute", $"Error: {e.Message}");
                if (AppSettings.TurnOnLogCheckConnect)
                {
                    mongoUtilities.AddLog(0, "CheckConnection (Error) | Kiểm tra kết nối (Lỗi)", $"{timeCheck:yyyyMMddHHmmssffffff}", "", MongoUtilities.EnumMongoLogType.Start, $"Error: {e.Message}", true);

                }
            }
        }
    }
}
