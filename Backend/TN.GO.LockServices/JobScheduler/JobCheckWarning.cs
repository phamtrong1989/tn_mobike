﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.JobScheduler
{
    class JobCheckWarning
    {
        private static IScheduler scheduler;
        public static Dictionary<string, CheckWarningEntity> DATACHECKWARNINGS = new Dictionary<string, CheckWarningEntity>();
        public static void Start()
        {
            try
            {
                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
                //Job capture frames

                IJobDetail job = JobBuilder.Create<WarningCheck>().Build();
                ITrigger restartTrigger = TriggerBuilder.Create()
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(AppSettings.TimeCheckLocation)
                        .RepeatForever())
                    .Build();
                scheduler.ScheduleJob(job, restartTrigger);

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobCheckWarning_Start", ex.ToString());
            }
        }

        public static void ReStart()
        {
            try
            {
                scheduler?.Shutdown();

                Thread.Sleep(1000);

                Start();
            }
            catch (Exception)
            {
                //
            }
        }

        public static void Stop()
        {
            try
            {
                scheduler?.Shutdown();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobCheckWarning_Stop", ex.ToString());
            }
        }
    }

    class WarningCheck : IJob
    {
        public bool IsRunning = false;
        private DockModels dockModels = new DockModels();
        private BookingModel bookingModel = new BookingModel();
        private ApiProcess apiProcess = new ApiProcess();
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (IsRunning)
                {
                    return;
                }

                IsRunning = true;

                if (JobCheckWarning.DATACHECKWARNINGS.Count > 0)
                {
                    Dictionary<string, CheckWarningEntity> dataTemp = new Dictionary<string, CheckWarningEntity>();
                    dataTemp = JobCheckWarning.DATACHECKWARNINGS;

                    Utilities.WriteDebugLog("WarningCheckJob.1.0.0", $"Start | DATACHECKWARNINGS Count = {dataTemp.Count}");

                    foreach (var warning in dataTemp)
                    {
                        var dock = dockModels.GetOneDock(warning.Key);
                        var booking = bookingModel.GetOneBooking(dock.Id);
                        var requestId = $"{DateTime.Now:yyyyMMddHHmmssfff}";

                        Utilities.WriteDebugLog("WarningCheckJob.1.1.0", $"{requestId} | Imei: {warning.Key} | DockId: {dock.Id} | Pin: {dock.Battery} | Status: {dock.LockStatus}");

                        // Không trong chuyến
                        if (booking == null)
                        {
                            Utilities.WriteDebugLog("WarningCheckJob.1.1.1", $"{requestId} | Imei: {warning.Key} | Booking is null - Xe không trong chuyến - Tiếp tục WarningCheck");
                            // Đang khóa
                            if (dock.LockStatus)
                            {
                                Utilities.WriteDebugLog("WarningCheckJob.1.1.2", $"{requestId} | Imei: {warning.Key} | Khóa đóng");
                                var gpsFirst = warning.Value.DataGps.FirstOrDefault();
                                var gpsLast = warning.Value.DataGps.LastOrDefault();
                                var met = Utilities.DistanceInMeterM(gpsFirst.Lat, gpsFirst.Lng, gpsLast.Lat, gpsLast.Lng);
                                Utilities.WriteDebugLog("WarningCheckJob.1.1.3", $"{requestId} | Imei: {warning.Key} | GPS1 - {gpsFirst.Time}: https://maps.google.com?q={gpsFirst.Lat},{gpsFirst.Lng} | GPS2 - {gpsLast.Time}: https://maps.google.com?q={gpsLast.Lat},{gpsLast.Lng} | MetCheck: {met}");
                                if (met >= AppSettings.MetWarning)
                                {
                                    booking = bookingModel.GetOneBooking(dock.Id);
                                    if ((DateTime.Now - warning.Value.TimePush).TotalMinutes > AppSettings.TimePushWarning && booking == null)
                                    {
                                        apiProcess.PushBikeWarning(warning.Key, EBikeWarningType.GPSDiChuyenBatHopPhap, DateTime.Now, requestId);

                                        if (JobCheckWarning.DATACHECKWARNINGS.TryGetValue(warning.Key, out var value))
                                        {
                                            value.TimePush = DateTime.Now;
                                        }
                                        Utilities.WriteDebugLog("WarningCheckJob.1.1.4", $"{requestId} | Imei: {warning.Key} | MetCheck: {met} > Config: {AppSettings.MetWarning} | Push Warning : Xe di chuyển bất hợp pháp");
                                    }
                                }
                                else
                                {
                                    Utilities.WriteDebugLog("WarningCheckJob.1.1.4", $"{requestId} | Imei: {warning.Key} | MetCheck: {met} > Config: {AppSettings.MetWarning} | Continue");
                                    continue;
                                }
                            }
                            // Mở khóa
                            else
                            {
                                Utilities.WriteDebugLog("WarningCheckJob.1.2.1", $"{requestId} | Imei: {warning.Key} | Khóa mở");
                                var count = 0;
                                var metCheck = 0.0;
                                var firstGps = warning.Value.DataGps.FirstOrDefault();
                                var countGps = warning.Value.DataGps.Count;

                                Utilities.WriteDebugLog("WarningCheckJob.1.2.2", $"{requestId} | Imei: {warning.Key} | GPS1 - {firstGps.Time}: https://maps.google.com?q={firstGps.Lat},{firstGps.Lng} | Count Data Gps: {countGps}");

                                for (int i = 1; i < countGps; i++)
                                {
                                    var gpsI = warning.Value.DataGps[i];
                                    var met = Utilities.DistanceInMeterM(firstGps.Lat, firstGps.Lng, gpsI.Lat, gpsI.Lng);

                                    if (metCheck == 0)
                                    {
                                        metCheck = met;
                                        continue;
                                    }
                                    else
                                    {
                                        if (Math.Abs((met - metCheck)) > AppSettings.MetCheck)
                                        {
                                            count += 1;
                                        }
                                        metCheck = met;
                                    }

                                    Utilities.WriteDebugLog($"WarningCheckJob.1.2.3.{i}", $"{requestId} | Imei: {warning.Key} | GPSi - {gpsI.Time} : https://maps.google.com?q={gpsI.Lat},{gpsI.Lng} | Met: {met} | MetCheck : {metCheck} | Count: {count}");

                                }

                                if (Math.Round(((double)count / countGps), 2) > 50)
                                {
                                    booking = bookingModel.GetOneBooking(dock.Id);
                                    if ((DateTime.Now - warning.Value.TimePush).TotalMinutes > AppSettings.TimePushWarning && booking == null)
                                    {
                                        apiProcess.PushBikeWarning(warning.Key, EBikeWarningType.GPSDiChuyenBatHopPhap, DateTime.Now, requestId);

                                        if (JobCheckWarning.DATACHECKWARNINGS.TryGetValue(warning.Key, out var value))
                                        {
                                            value.TimePush = DateTime.Now;
                                        }

                                        Utilities.WriteDebugLog("WarningCheckJob.1.2.4", $"{requestId} | Imei: {warning.Key} | Push Warning : Xe di chuyển bất hợp pháp");
                                    }
                                }
                            }
                        }
                        // Trong chuyến
                        else
                        {
                            Utilities.WriteDebugLog("WarningCheckJob", $"{requestId} | Booking is not null - Xe trong chuyến - Hủy bỏ WarningCheck");
                            Utilities.WriteDebugLog("WarningCheckJob", $"{requestId} | Tổng DATACHECKWARNINGS trước khi xóa Key: {warning.Key} = {JobCheckWarning.DATACHECKWARNINGS.Count}");
                            JobCheckWarning.DATACHECKWARNINGS.Remove(warning.Key);
                            Utilities.WriteDebugLog("WarningCheckJob", $"{requestId} | Tổng DATACHECKWARNINGS sau khi xóa Key: {warning.Key} = {JobCheckWarning.DATACHECKWARNINGS.Count}");
                            continue;
                        }
                    }
                }
                else
                {
                    Utilities.WriteDebugLog("WarningCheckJob.1.0.0", $"Start | DATACHECKWARNINGS NULL");
                }
                IsRunning = false;
            }
            catch (Exception e)
            {
                IsRunning = false;
                Utilities.WriteErrorLog("WarningCheckJob.Execute", $"Error: {e.Message}");
            }
        }
    }
}
