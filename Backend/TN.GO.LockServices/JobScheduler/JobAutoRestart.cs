﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using TN.GO.LockServices.MinaCores;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.JobScheduler
{
    class JobAutoRestart
    {
        private static IScheduler scheduler;

        public static void Start()
        {
            try
            {
                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
                //Job capture frames

                IJobDetail job = JobBuilder.Create<RestartSV>().Build();
                ITrigger restartTrigger = TriggerBuilder.Create()
                    //.StartNow()
                    .WithDailyTimeIntervalSchedule
                          (s =>
                             s.WithIntervalInHours(24)
                            .OnEveryDay()
                            .StartingDailyAt(TimeOfDay.HourMinuteAndSecondOfDay(AppSettings.TimeAutoRestart, 0, 0))
                          )
                    .Build();
                scheduler.ScheduleJob(job, restartTrigger);

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobCheckConnection_Start", ex.ToString());
            }
        }

        public static void ReStart()
        {
            try
            {
                scheduler?.Shutdown();

                Thread.Sleep(1000);

                Start();
            }
            catch (Exception)
            {
                //
            }
        }

        public static void Stop()
        {
            try
            {
                scheduler?.Shutdown();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("restartJob_Stop", ex.ToString());
            }
        }
    }

    class RestartSV : IJob
    {
        public bool IsRunning = false;
        public static TNControl tnControl = null;
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (IsRunning)
                {
                    return;
                }

                IsRunning = true;
                if (tnControl != null)
                {
                    new DockModels().UpdateStatus();
                    tnControl.StopServer();

                    TNSessionMap.MAPIMEI.Clear();
                    TNSessionMap.MAPIP.Clear();
                    TNControl.TotalSessions.Clear();
                    Task.Delay(5000);

                    tnControl.Start();
                }
                IsRunning = false;
            }
            catch (Exception e)
            {
                IsRunning = false;
                Utilities.WriteErrorLog("RestartSV.Execute", $"Error: {e.Message}");
            }
        }
    }
}
