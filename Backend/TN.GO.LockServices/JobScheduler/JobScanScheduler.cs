﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.JobScheduler
{
    class JobScanScheduler
    {
        private static IScheduler scheduler;
        public static List<Station> ListStations = new List<Station>();
        private static StationModels StationModel = new StationModels();

        public static void Start()
        {
            try
            {
                ListStations = StationModel.GetAll();

                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
                //Job capture frames

                IJobDetail job = JobBuilder.Create<JobScanDb>().Build();
                ITrigger restartTrigger = TriggerBuilder.Create()
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(AppSettings.TimeJobSchedulerSecond)
                        .RepeatForever())
                    .Build();
                scheduler.ScheduleJob(job, restartTrigger);

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobScanScheduler_Start", ex.ToString());

                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, $"JobScanScheduler (Error) | Quét DB mở khóa (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobScanScheduler_Start | Error: {ex.Message}", true);

                }
            }
        }

        public static void ReStart()
        {
            try
            {
                scheduler?.Shutdown();

                Thread.Sleep(1000);

                Start();
            }
            catch (Exception e)
            {
                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, $"JobScanScheduler (Error) | Quét DB mở khóa (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobScanScheduler_Restart | Error: {e.Message}", true);

                }
            }
        }

        public static void Stop()
        {
            try
            {
                scheduler?.Shutdown();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("restartJob_Stop", ex.ToString());

                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, $"JobScanScheduler (Error) | Quét DB mở khóa (lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "0x0000000000000", MongoUtilities.EnumMongoLogType.End, $"JobScanScheduler_Stop | Error: {ex.Message}", true);

                }
            }
        }
    }

    class JobScanDb : IJob
    {
        private DBProcess dbProcess = new DBProcess();
        public static bool IsRunning = false;
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private StationModels StationModel = new StationModels();
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (IsRunning)
                {
                    return;
                }

                IsRunning = true;

                dbProcess.QueryDbToUnlock();

                IsRunning = false;
            }
            catch (Exception e)
            {
                IsRunning = false;
                Utilities.WriteErrorLog("JobScanDB.Execute", $"Error: {e.Message}");

                if (AppSettings.TurnOnLogSendUnlock)
                {
                    mongoUtilities.AddLog(0, "JobScanUnlock (Error) | Mở khóa (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "", MongoUtilities.EnumMongoLogType.Start, $"Error: {e.Message}", true);
                }
            }
        }
    }
}
