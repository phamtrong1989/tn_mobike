﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.GO.LockServices.MinaCores;
using TN.GO.LockServices.MinaCores.HandlerCmd;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.JobScheduler
{
    class SyncJobScheduler
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNControl tnControl = new TNControl();
        
        public void Start()
        {
            try
            {
                if (AppSettings.ModeRun)
                {
                    Console.OutputEncoding = Encoding.UTF8;
                }
                Utilities.WriteOperationLog("Start", "App Started!");

                RestartSV.tnControl = tnControl;

                if (AppSettings.TurnOnLogStartSV)
                {
                    mongoUtilities.AddLog(0, "Start Services | Bật services ", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "", MongoUtilities.EnumMongoLogType.Start, $"App Started!");
                }

                CheckCharging.LastPercentPin = new DockModels().GetOldPinFirst();

                SqlHelper.OpenConnectMongoDb();

                new DockModels().UpdateStatus();

                if (AppSettings.TurnOnSocket)
                {
                    tnControl.Start();
                }

                if (AppSettings.TurnOnAutoRestart)
                {
                    JobAutoRestart.Start();
                }

                if (AppSettings.TurnOnCheckLocation)
                {
                    JobAutoSendD1.Start();
                }

                if (AppSettings.TurnOnQueryDb)
                {
                    JobScanScheduler.Start();
                }

                JobCheckConnection.Start();

                if (AppSettings.ModeProcessD0)
                {
                    RabbitMqProcess.Start();
                }

                if (AppSettings.TurnOnCheckWarning)
                {
                    JobCheckWarning.Start();
                }

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Start", ex.ToString());

                if (AppSettings.TurnOnLogStartSV)
                {
                    mongoUtilities.AddLog(0, "Start Services (Error) | Bật services (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "", MongoUtilities.EnumMongoLogType.Start, $"Error: {ex.Message}", true);

                }
            } 
        }
        public void Stop()
        {
            try
            {
                new DockModels().UpdateStatus();

                if (AppSettings.TurnOnSocket)
                {
                    tnControl.StopServer();
                }

                if (AppSettings.TurnOnAutoRestart)
                {
                    JobAutoRestart.Stop();
                }

                if (AppSettings.TurnOnQueryDb)
                {
                    JobScanScheduler.Stop();
                }

                if (AppSettings.TurnOnCheckLocation)
                {
                    JobAutoSendD1.Stop();
                }

                JobCheckConnection.Stop();

                
                if (AppSettings.ModeProcessD0)
                {
                    RabbitMqProcess.Stop();
                }

                if (AppSettings.TurnOnCheckWarning)
                {
                    JobCheckWarning.Stop();
                }

                Utilities.WriteOperationLog("ProgramInit_Stop", "Try to stop service...");

                if (AppSettings.TurnOnLogStartSV)
                {
                    mongoUtilities.AddLog(0, "Stop Services | Tắt services", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "", MongoUtilities.EnumMongoLogType.Start, $"Try to stop service...");
                }

                Utilities.CloseLog();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Stop", ex.ToString());

                if (AppSettings.TurnOnLogStartSV)
                {
                    mongoUtilities.AddLog(0, "Stop Services (Error) | Tắt services (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", "", MongoUtilities.EnumMongoLogType.Start, $"Error: {ex.Message}", true);

                }
            }
        }
    }
}
