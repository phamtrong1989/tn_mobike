﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdS5
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private CheckCharging checkCharging = new CheckCharging();
        private PinDockLogging pinDockLogging = new PinDockLogging();

        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageS5(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageS5(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageS5", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageS5(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageS5(command, requestId, session);
        }

        private async Task ProcessMessageS5(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var percent = Utilities.CheckPercent(comm[5].Replace("#", ""));

                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                        var gms = Utilities.CheckGMS(comm[6]);
                        var pin = $"{percent} %";

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : S5 - LOCK STATUS COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER S5 : {string.Join(",", comm)}");

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Pin : {pin}");
                            Console.WriteLine($"Tín hiệu : {gms}");
                            Console.WriteLine($"Chỉ số GPS : {comm[7]}");
                            Console.WriteLine(comm[8] == "0" ? $"Trạng thái : Đang mở khóa" : $"Trạng thái : Đã khóa");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Utilities.WriteOperationLog("MinaOmni.HandCommand", $"RequestId: {requestId} | Imei: {imei} | S5 - LOCK STATUS COMMAND | Thời gian: {time} | Pin: {pin} | GMS: {gms} | GPS: {comm[7]} | Lock (1: Lock \\ 0: Unlock): {comm[8]}");
                        if (AppSettings.TurnOnLogS5)
                        {
                            mongoUtilities.AddLog(0, "S5 | Trạng thái", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Thời gian: {time} | Pin: {pin} | GMS: {gms} | GPS: {comm[7]} | Lock (1: Lock \\ 0: Unlock): {comm[8]}");

                        }

                        pinDockLogging.AddLog(requestId, imei, Convert.ToDouble(percent), Convert.ToInt32(comm[5].Replace("#", "")), false, DateTime.Now);

                        checkCharging.ProcessCharging(imei, percent);

                        dock = dockModel.GetOneDock(imei);
                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LockStatus = comm[8] != "0";
                            dock.LastConnectionTime = DateTime.Now;
                            dock.Battery = percent;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Update", $"RequestId: {requestId} | Imei: {imei} | S5 - LOCK STATUS COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");
                            if (AppSettings.TurnOnLogS5)
                            {
                                mongoUtilities.AddLog(0, "S5 (Update) | Trạng thái (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");

                            }
                        }
                        else
                        {
                            dock = new Dock(imei, percent, 0, 0);
                            dock.LockStatus = comm[8] != "0";
                            dockModel.InsertDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Insert", $"RequestId: {requestId} | Imei: {imei} | S5 - LOCK STATUS COMMAND | Dock: {dock.ToString()}");
                            if (AppSettings.TurnOnLogS5)
                            {
                                mongoUtilities.AddLog(0, "S5 (Insert) | Trạng thái (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageS5", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageS5", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }
            await Task.Delay(1);
        }
    }
}
