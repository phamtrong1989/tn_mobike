﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdI0
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();

        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageI0(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageI0(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageI0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageI0(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageI0(command, requestId, session);
        }

        private async Task ProcessMessageI0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                        var ICCID = comm[5].Replace("#", "");

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : I0 - OBTAIN SIM CARD ICCID CODE COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER I0 : {string.Join(",", comm)}");

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Thời gian : {time}");
                            Console.WriteLine($"Mã ICCID  : {ICCID}");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Utilities.WriteOperationLog("MinaOmni.HandCommand", $"RequestId: {requestId} | Imei: {imei} | I0 - OBTAIN SIM CARD ICCID CODE COMMAND | Thời gian: {time} | ICCID: {ICCID}");
                        if (AppSettings.TurnOnLogI0)
                        {
                            mongoUtilities.AddLog(0, "I0 | Lấy mã ICCID", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | I0 - OBTAIN SIM CARD ICCID CODE COMMAND | Thời gian: {time} | ICCID: {ICCID}");
                        }

                        dock = dockModel.GetOneDock(imei);
                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LastConnectionTime = DateTime.Now;
                            dock.SIM = ICCID;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Update", $"RequestId: {requestId} | Imei: {imei} | I0 - ICCID | Last Connection Time: {dock.LastConnectionTime} | ICCID: {ICCID} | Lock Status: {dock.LockStatus}");

                            if (AppSettings.TurnOnLogI0)
                            {
                                mongoUtilities.AddLog(0, "I0 (Update) | Lấy mã ICCID (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | I0 - ICCID | Last Connection Time: {dock.LastConnectionTime} | ICCID: {ICCID} | Lock Status: {dock.LockStatus}");

                            }
                        }
                        else
                        {
                            dock = new Dock(imei, 0, 0, 0, true, ICCID);
                            dockModel.InsertDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Insert", $"RequestId: {requestId} | Imei: {imei} | I0 - ICCID | Dock: {dock.ToString()}");

                            if (AppSettings.TurnOnLogI0)
                            {
                                mongoUtilities.AddLog(0, "I0 (Insert) | Lấy mã ICCID (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | I0 - ICCID | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageI0", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageI0", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
