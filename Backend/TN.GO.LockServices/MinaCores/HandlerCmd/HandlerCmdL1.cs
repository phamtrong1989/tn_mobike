﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdL1
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private ApiProcess apiProcess = new ApiProcess();
        private BookingModel bookingModel = new BookingModel();

        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageL1(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageL1(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageL1", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageL1(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageL1(command, requestId, session);
        }

        private async Task ProcessMessageL1(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        apiProcess.PushStatus(imei, requestId);

                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                        var timeActive = Utilities.UnixTimeStampToDateTime(comm[6]);
                        var cycle = $"{comm[7].Replace("#", "")} phút";

                        var message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},Re,L1#<LF>";

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : L1 -  LOCK COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER L1 : {string.Join(",", comm)}");

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Thời gian : {time}");
                            Console.WriteLine($"USER ID   : {comm[5]}");
                            Console.WriteLine($"Thời gian hoạt động : {timeActive}");
                            Console.WriteLine($"Thời gian chu kỳ : {cycle}");
                            Console.ForegroundColor = ConsoleColor.White;

                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message ?? "No Message"}");
                        }

                        Utilities.WriteOperationLog("MinaOmni.HandCommand", $"RequestId: {requestId} | Imei: {imei} | L1 - LOCK COMMAND | Thời gian : {time} | User : {comm[5]} | Thời gian hoạt động : {timeActive} | Thời gian đi (Phút) : {cycle} ");

                        if (AppSettings.TurnOnLogL1)
                        {
                            mongoUtilities.AddLog(0, "L1 | Đóng khóa", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | L1 - LOCK COMMAND | Thời gian : {time} | User : {comm[5]} | Thời gian hoạt động : {timeActive} | Thời gian đi (Phút) : {cycle}");
                        }
                        // Gửi phản hồi gói tin L1
                        TNSessionMap.SendMessage(session, imei, message, requestId, true, "L1 (Resend)", "Đóng khóa (Phản hồi L1)", MongoUtilities.EnumMongoLogType.Center);

                        //// Gửi gói tin tắt định vị tự động D1
                        //var messageD1Off = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},D1,0#<LF>";
                        //tnSessionMap.SendMessage(session, imei, messageD1Off, requestId, true, "Turn Off D1", "Tắt định vị tự động", MongoUtilities.EnumMongoLogType.Center);

                        // Gửi gói tin kiểm tra trạng thái khóa
                        var messageS5 = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},S5#<LF>";
                        TNSessionMap.SendMessage(session, imei, messageS5, requestId, true, "Send S5", "Kiểm tra trạng thái", MongoUtilities.EnumMongoLogType.Center);

                        dock = dockModel.GetOneDock(imei);

                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LockStatus = true;
                            dock.LastConnectionTime = DateTime.Now;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Update", $"RequestId: {requestId} | Imei: {imei} | L1 - LOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                            
                            if (AppSettings.TurnOnLogL1)
                            {
                                mongoUtilities.AddLog(0, "L1 (Update) | Đóng khóa (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | L1 - LOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                            }

                            var booking = bookingModel.GetOneBooking(dock.Id);
                            if (booking == null)
                            {
                                // Gửi gói tin tắt định vị tự động D1
                                var messageD1Off = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},D1,0#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageD1Off, requestId, true, "Turn Off D1", "Tắt định vị tự động", MongoUtilities.EnumMongoLogType.Center);

                                Utilities.WriteOperationLog("HandCommand.TurnOffD0", $"RequestId: {requestId} | Imei: {imei} | Id: {dock.Id} | L1 - LOCK COMMAND | Tắt định vị tự động - đã trả xe");
                            }
                            else
                            {
                                Utilities.WriteOperationLog("HandCommand.TurnOffD0", $"RequestId: {requestId} | Imei: {imei} | Id: {dock.Id} | L1 - LOCK COMMAND | Giữ định vị tự động - chưa trả xe");
                            }
                        }
                        else
                        {
                            dock = new Dock(imei, 0, 0, 0, true);
                            dockModel.InsertDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Insert", $"RequestId: {requestId} | Imei: {imei} | L1 - LOCK COMMAND | Dock: {dock.ToString()}");

                            if (AppSettings.TurnOnLogL1)
                            {
                                mongoUtilities.AddLog(0, "L1 (Insert) | Đóng khóa (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | L1 - LOCK COMMAND | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageL1", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageL1", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
