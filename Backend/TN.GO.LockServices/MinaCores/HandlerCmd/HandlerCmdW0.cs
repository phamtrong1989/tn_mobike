﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using MongoDB.Driver.Core.Misc;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdW0
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private ApiProcess apiProcess = new ApiProcess();
        private WarningProcess warningProcess = new WarningProcess();

        private string rfidCode = AppSettings.DefaultRFID;
        public void HandlerMessageW0(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageW0(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageW0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageW0(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageW0(command, requestId, session);
        }

        private async Task ProcessMessageW0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},Re,W0#\n";

                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                        var alarm = comm[5].Replace("#", "");
                        var statusAlarm = "";

                        var warningType = EBikeWarningType.Undefined;

                        if (alarm == "1")
                        {
                            statusAlarm = "Báo động : Chuyển động bất hợp pháp";
                            warningType = EBikeWarningType.DiChuyenBatHopPhap;
                        }
                        else if (alarm == "2")
                        {
                            statusAlarm = "Báo động : đổ xe";
                            warningType = EBikeWarningType.DoXe;
                        }
                        else if (alarm == "6")
                        {
                            statusAlarm = "Báo động : Xe được dựng lên";
                        }

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : W0 - ALARMING COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER W0 : {string.Join(",", comm)}");

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Thời gian : {time}");
                            Console.WriteLine(statusAlarm);

                            Console.ForegroundColor = ConsoleColor.White;

                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message ?? "No Message"}");
                        }

                        Utilities.WriteOperationLog("MinaOmni.HandCommand", $"RequestId: {requestId} | Imei: {imei} | W0 - ALARMING COMMAND | Thời gian: {time} | Status: {statusAlarm}");
                        if (AppSettings.TurnOnLogW0)
                        {
                            mongoUtilities.AddLog(0, "W0 | Cảnh báo", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | W0 - ALARMING COMMAND | Thời gian: {time} | Status: {statusAlarm}");

                        }
                        Utilities.WriteOperationLog("MinaOmni.Resend", $"Message : {message}");
                        TNSessionMap.SendMessage(session, imei, message, requestId, true, "W0 (Resend)", "Cảnh báo (Phản hồi W0)", MongoUtilities.EnumMongoLogType.Center);

                        //new BikeWarningModel().InsertBikeWarningTask(imei, requestId, warningType, 0, 0, 0, false);
                        if (warningType != EBikeWarningType.Undefined)
                        {
                            warningProcess.ProcessWarning(imei, requestId, warningType);
                        }

                        dock = dockModel.GetOneDock(imei);
                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LastConnectionTime = DateTime.Now;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Update", $"RequestId: {requestId} | Imei: {imei} | W0 - ALARMING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                            if (AppSettings.TurnOnLogW0)
                            {
                                mongoUtilities.AddLog(0, "W0 (Update) | Cảnh báo (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | W0 - ALARMING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                            }
                        }
                        else
                        {
                            dock = new Dock(imei, 0, 0, 0);
                            dockModel.InsertDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Insert", $"RequestId: {requestId} | Imei: {imei} | W0 - ALARMING COMMAND | Dock: {dock.ToString()}");

                            if (AppSettings.TurnOnLogW0)
                            {
                                mongoUtilities.AddLog(0, "W0 (Insert) | Cảnh bảo (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | W0 - ALARMING COMMAND | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("HandlerMessageW0", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageW0", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
