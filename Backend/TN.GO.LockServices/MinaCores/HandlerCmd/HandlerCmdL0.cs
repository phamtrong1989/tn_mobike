﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdL0
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private string rfidCode = AppSettings.DefaultRFID;
        private OpenLockRequestModel openLockRequestModel = new OpenLockRequestModel();
        private OpenLockHistoryModel openLockHistoryModel = new OpenLockHistoryModel();

        public void HandlerMessageL0(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageL0(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageL0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageL0(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageL0(command, requestId, session);
        }

        private async Task ProcessMessageL0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                        var timeActive = $"{Utilities.UnixTimeStampToDateTime(comm[7].Replace("#", ""))}";

                        var message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},Re,L0#<LF>";

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : L0 - UNLOCK COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER L0 : {string.Join(",", comm)}");

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($" Thời gian : {time}");
                            Console.WriteLine(comm[5] == "0" ? $"Trạng thái : Mở thành công" : $"Trạng thái : Mở lỗi");
                            Console.WriteLine($" Customer ID : {comm[6]}");
                            Console.WriteLine($" Timestamp : {timeActive}");
                            Console.ForegroundColor = ConsoleColor.White;

                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message ?? "No Message"}");
                        }

                        Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | L0 - UNLOCK COMMAND | Thời gian : {time} | User : {comm[6]} | Thời gian hoạt động : {timeActive} | Lock (1: Lock \\ 0: Unlock) : {comm[5]}");

                        if (AppSettings.TurnOnLogL0)
                        {
                            mongoUtilities.AddLog(0, "L0 | Mở khóa", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | L0 - UNLOCK COMMAND | Thời gian : {time} | User : {comm[6]} | Thời gian hoạt động : {timeActive} | Lock (1: Lock \\ 0: Unlock) : {comm[5]}");

                        }
                        Utilities.WriteOperationLog("MinaOmni.Resend", $"Message : {message}");
                        TNSessionMap.SendMessage(session, imei, message, requestId, true, "L0 (Resend)", "Mở khóa (Phản hồi L0)", MongoUtilities.EnumMongoLogType.Center);

                        var messageD1On = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},D1,{AppSettings.TimeTrackingLocation}#<LF>";
                        TNSessionMap.SendMessage(session, imei, messageD1On, requestId, true, "SendD1On", "Bật D1", MongoUtilities.EnumMongoLogType.Center);

                        var item = openLockRequestModel.GetOne(imei, DateTime.Now);

                        if (item != null)
                        {
                            dock = dockModel.GetOneDock(imei);
                            if (dock != null && dock.Id >= 0)
                            {
                                if (String.IsNullOrEmpty(dock.RFID))
                                {
                                    // Gửi lệnh khóa RFID
                                    var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                    TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                    dock.RFID = rfidCode;
                                }

                                dock.LockStatus = comm[5] != "0";
                                dock.LastConnectionTime = DateTime.Now;
                                dock.ConnectionStatus = true;
                                dockModel.UpdateDock(dock);

                                Utilities.WriteOperationLog("HandCommand.Update", $"RequestId: {requestId} | Imei: {imei} | L0 - UNLOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                                if (AppSettings.TurnOnLogL0)
                                {
                                    mongoUtilities.AddLog(0, "L0 (Update) | Mở khóa (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | L0 - UNLOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                                }
                            }
                            else
                            {
                                dock = new Dock(imei, 0, 0, 0, false);
                                dockModel.InsertDock(dock);

                                Utilities.WriteOperationLog("HandCommand.Insert", $"RequestId: {requestId} | Imei: {imei} | L0 - UNLOCK COMMAND | Dock: {dock.ToString()}");

                                if (AppSettings.TurnOnLogL0)
                                {
                                    mongoUtilities.AddLog(0, "L0 (Insert) | Mở khóa (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | L0 - UNLOCK COMMAND | Dock: {dock.ToString()}");
                                }
                            }

                            var openLock = new OpenLockHistory(item);
                            openLock.Status = 0;

                            try
                            {
                                openLockHistoryModel.InsertOpenLockHistory(openLock);
                                if (AppSettings.TurnOnLogL0)
                                {
                                    mongoUtilities.AddLog(0, "InsertLockHistory | Xóa request mở khóa", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | InsertLockHistory | {openLock.ToString()}");
                                }
                            }
                            catch (Exception e)
                            {
                                Utilities.WriteErrorLog("[InsertLockHistory]", $"[ERROR: RequestId: {requestId} |  {e}]");
                                if (AppSettings.TurnOnLogL0)
                                {
                                    mongoUtilities.AddLog(0, "InsertLockHistory (Error) | Thêm lịch sử mở khóa (Lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | InsertLockHistory | [ERROR: {e}]");
                                }
                            }

                            try
                            {
                                openLockRequestModel.DeleteOpenLockRequest(item.Id);
                                if (AppSettings.TurnOnLogL0)
                                {
                                    mongoUtilities.AddLog(0, "DeleteLockRequest | Xóa request mở khóa", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | DeleteLockRequest | {item.ToString()}");

                                }
                            }
                            catch (Exception e)
                            {
                                Utilities.WriteErrorLog("[DeleteLockRequest]", $"[ERROR: RequestId: {requestId} |  {e}]");
                                if (AppSettings.TurnOnLogL0)
                                {
                                    mongoUtilities.AddLog(0, "DeleteLockRequest (Error) | Xóa request mở khóa (Lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | DeleteLockRequest | [ERROR: {e}]");

                                }
                            }
                        }
                        else
                        {
                            dock = dockModel.GetOneDock(imei);
                            if (dock != null && dock.Id >= 0)
                            {
                                dock.LockStatus = comm[5] != "0";
                                dock.LastConnectionTime = DateTime.Now;
                                dock.ConnectionStatus = true;
                                dockModel.UpdateDock(dock);

                                Utilities.WriteOperationLog("HandCommand.Update", $"RequestId: {requestId} | Imei: {imei} | L0 - UNLOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                                if (AppSettings.TurnOnLogL0)
                                {
                                    mongoUtilities.AddLog(0, "L0 (Update) | Mở khóa (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | L0 - UNLOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                                }
                            }
                            else
                            {
                                dock = new Dock(imei, 0, 0, 0, false);
                                dockModel.InsertDock(dock);

                                Utilities.WriteOperationLog("HandCommand.Insert", $"RequestId: {requestId} | Imei: {imei} | L0 - UNLOCK COMMAND | Dock: {dock.ToString()}");

                                if (AppSettings.TurnOnLogL0)
                                {
                                    mongoUtilities.AddLog(0, "L0 (Insert) | Mở khóa (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | L0 - UNLOCK COMMAND | Dock: {dock.ToString()}");

                                }
                            }
                        }

                        if (JobCheckConnection.DataConnection.TryGetValue(imei, out var value))
                        {
                            value.LastReceiverL0 = DateTime.Now;
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageL0", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageL0", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
