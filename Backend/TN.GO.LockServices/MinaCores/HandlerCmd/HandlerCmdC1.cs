﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdC1
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();

        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageC1(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageC1(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageC1", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageC1(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageC1(command, requestId, session);
        }

        private async Task ProcessMessageC1(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                        var key = comm[5].Replace("#", "");

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : C1 - Management biked number setting");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER C1 : {string.Join(",", comm)}");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Thời gian : {time}");
                            Console.WriteLine($"Key  : {key}");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Utilities.WriteOperationLog("ProcessMessageC1.HandCommand", $"RequestId: {requestId} | Imei: {imei} | C1 - Management biked number setting | Thời gian: {time} | Key: {key}");
                        if (AppSettings.TurnOnLogC1)
                        {
                            mongoUtilities.AddLog(0, "C1 | Cài đặt mã RFID", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | C1 - Management biked number setting | Thời gian: {time} | Key: {key}");
                        }

                        dock = dockModel.GetOneDock(imei);

                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LastConnectionTime = DateTime.Now;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("ProcessMessageC1.Update", $"RequestId: {requestId} | Imei: {imei} | C1 | Last Connection Time: {dock.LastConnectionTime} | Key: {key} | Lock Status: {dock.LockStatus}");
                            if (AppSettings.TurnOnLogC1)
                            {
                                mongoUtilities.AddLog(0, "C1 (Update) | Cài đặt mã RFID (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | C1 | Last Connection Time: {dock.LastConnectionTime} | Key: {key} | Lock Status: {dock.LockStatus}");

                            }
                        }
                        else
                        {
                            dock = new Dock(imei, 0, 0, 0);
                            dockModel.InsertDock(dock);

                            Utilities.WriteOperationLog("ProcessMessageC1.Insert", $"RequestId: {requestId} | Imei: {imei} | C1 | Dock: {dock.ToString()}");

                            if (AppSettings.TurnOnLogC1)
                            {
                                mongoUtilities.AddLog(0, "C1 (Insert) | Cài đặt mã RFID (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | C1 | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageC1", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageC1", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
