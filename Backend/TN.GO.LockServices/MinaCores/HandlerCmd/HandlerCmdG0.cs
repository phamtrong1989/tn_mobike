﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdG0
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageG0(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageG0(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageG0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageG0(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageG0(command, requestId, session);
        }

        private async Task ProcessMessageG0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine(
                                $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : G0 - QUERY FIRMWARE VERSION COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER G0 : {string.Join(",", comm)}");

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Thời gian : {time}");
                            Console.WriteLine($"Phiên bản : {comm[5]}");
                            Console.WriteLine($"Thời gian cập nhật : {comm[6].Replace("#", "")}");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Utilities.WriteOperationLog("ProcessMessageG0.HandCommand",
                            $"RequestId: {requestId} | Imei: {imei} | G0 - QUERY FIRMWARE VERSION COMMAND | Thời gian: {time} | Phiên bản: {comm[5]} | Thời gian cập nhật: {comm[6].Replace("#", "")}");
                        if (AppSettings.TurnOnLogG0)
                        {
                            mongoUtilities.AddLog(0, "G0 | Phần mềm", requestId, imei,
                                MongoUtilities.EnumMongoLogType.Start,
                                $"Imei: {imei} | G0 - QUERY FIRMWARE VERSION COMMAND | Thời gian: {time} | Phiên bản: {comm[5]} | Thời gian cập nhật: {comm[6].Replace("#", "")}");

                        }

                        dock = dockModel.GetOneDock(imei);
                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1",
                                    "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LastConnectionTime = DateTime.Now;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);


                            Utilities.WriteOperationLog("ProcessMessageG0.Update",
                                $"RequestId: {requestId} | Imei: {imei} | G0 - QUERY FIRMWARE | Last Connection Time: {dock.LastConnectionTime} | Model: {comm[5]} | Update: {comm[6]} | Lock Status: {dock.LockStatus}");
                            if (AppSettings.TurnOnLogG0)
                            {
                                mongoUtilities.AddLog(0, "G0 (Update) | Phần mềm (Cập nhật khóa)", requestId, imei,
                                    MongoUtilities.EnumMongoLogType.End,
                                    $"Imei: {imei} | G0 - QUERY FIRMWARE | Last Connection Time: {dock.LastConnectionTime} | Model: {comm[5]} | Update: {comm[6]} | Lock Status: {dock.LockStatus}");

                            }
                        }
                        else
                        {
                            dock = new Dock(imei, 0, 0, 0);
                            dockModel.InsertDock(dock);

                            Utilities.WriteOperationLog("ProcessMessageG0.Insert",
                                $"RequestId: {requestId} | Imei: {imei} | G0 - QUERY FIRMWARE | Dock: {dock.ToString()}");

                            if (AppSettings.TurnOnLogG0)
                            {
                                mongoUtilities.AddLog(0, "G0 (Insert) | Phần mềm (Thêm mới khóa)", requestId, imei,
                                    MongoUtilities.EnumMongoLogType.End,
                                    $"Imei: {imei} | G0 - QUERY FIRMWARE | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageG0", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageG0", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
