﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdC0
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private ApiProcess apiProcess = new ApiProcess();

        private string rfidCode = AppSettings.DefaultRFID;
        //public static List<string> listRFID = Utilities.ReadRFID("RFID.txt");
        public static List<string> listRFID = new List<string>();
        private readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public void HandlerMessageC0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageC0(comm, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageC0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageC0(List<string> comm, string requestId, IoSession session)
        {
            await ProcessMessageC0(comm, requestId, session);
        }

        private async Task ProcessMessageC0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                // *CMDR,NP,861123053783187,000000000000,C0,0,0,0080645D620E8904#
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    //var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var rfid = comm[7].Replace("#", "");

                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                        Utilities.WriteOperationLog("ProcessMessageC0.HandCommand", $"RequestId: {requestId} | Imei: {imei} | C0 - RFID Unlock | Thời gian: {time} | Key: C0 | RFID: {rfid}");

                        if (AppSettings.TurnOnLogC0)
                        {
                            mongoUtilities.AddLog(0, "C0 | Mở khóa với RFID", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | C0 - RFID Unlock | Thời gian: {time} | Key: C0");
                        }

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : C0 - Unlock with RFID");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER C0 : {string.Join(",", comm)}");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Thời gian : {time}");
                            Console.WriteLine($"RFID  : {rfid}");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        var check = apiProcess.PushOpenRfid(imei, requestId, rfid);

                        //var check = AppSettings.TurnOnCheckRfidApi ? apiProcess.PushOpenRfid(imei, requestId, rfid) : listRFID.Contains(rfid);

                        //if (check && AppSettings.TurnOnCheckRfidApi)
                        //{
                            //int uid = 0;

                            //long timestamp = (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;

                            //var message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},L0,0,{uid},{timestamp}#<LF>";
                            //TNSessionMap.SendMessage(session, imei, message, requestId, true, "C0 (RFID Unlock)", "Mở khóa với RFID", MongoUtilities.EnumMongoLogType.Center);


                            dock = dockModel.GetOneDock(imei);
                            if (dock != null && dock.Id >= 0)
                            {
                                if (String.IsNullOrEmpty(dock.RFID))
                                {
                                    // Gửi lệnh khóa RFID
                                    var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                    TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                    dock.RFID = rfidCode;
                                }

                                dock.LastConnectionTime = DateTime.Now;
                                dock.ConnectionStatus = true;
                                dockModel.UpdateDock(dock);

                                Utilities.WriteOperationLog("ProcessMessageC0.Update", $"RequestId: {requestId} | Imei: {imei} | C0 - RFID Unlock | Last Connection Time: {dock.LastConnectionTime} | RFID: {rfid} | Lock Status: {dock.LockStatus}");

                                if (AppSettings.TurnOnLogC0)
                                {
                                    mongoUtilities.AddLog(0, "C0 (Update) | Mở khóa với RFID (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | C0 - RFID Unlock | Last Connection Time: {dock.LastConnectionTime} | RFID: {rfid} | Lock Status: {dock.LockStatus}");

                                }
                            }
                            else
                            {
                                dock = new Dock(imei, 0, 0, 0);
                                dockModel.InsertDock(dock);

                                Utilities.WriteOperationLog("ProcessMessageC0.Insert", $"RequestId: {requestId} | Imei: {imei} | C0 - RFID Unlock | Dock: {dock.ToString()}");

                                if (AppSettings.TurnOnLogC0)
                                {
                                    mongoUtilities.AddLog(0, "C0 (Insert) | Mở khóa với RFID (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | C0 - RFID Unlock | Dock: {dock.ToString()}");

                                }
                            }
                        //}
                        //else
                        //{
                        //    Utilities.WriteOperationLog("ProcessMessageC0.Error", $"RequestId: {requestId} | Imei: {imei} | C0 - RFID Unlock | RFID: {rfid} is not pass filter");
                        //    return;
                        //}

                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageC0", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageC0", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
