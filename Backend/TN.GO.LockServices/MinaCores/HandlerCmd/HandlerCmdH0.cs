﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdH0
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private CheckCharging checkCharging = new CheckCharging();
        private PinDockLogging pinDockLogging = new PinDockLogging();

        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageH0(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageH0(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageH0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageH0(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageH0(command, requestId, session);
        }

        private async Task ProcessMessageH0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;
                    try
                    {
                        var percent = Utilities.CheckPercent(comm[6]);
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                        var pin = $"{percent} %";
                        var gms = Utilities.CheckGMS(comm[7].Replace("#", ""));

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : H0 - HEARTBEAT COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER H0 : {string.Join(",", comm)}");

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Thời gian : {time}");
                            Console.WriteLine(comm[5] == "0" ? $"Trạng thái : Đã mở khóa" : $"Trạng thái : Đã khóa");
                            Console.WriteLine($"Pin : {pin}");
                            Console.WriteLine($"Tín hiệu : {gms}");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Utilities.WriteOperationLog("ProcessMessageH0.HandCommand",
                            $"RequestId: {requestId} | Imei: {imei} | H0 - HEARTBEAT COMMAND | Thời gian : {time} | Pin : {pin} | Lock (1: Lock \\ 0: Unlock) : {comm[5]} | GMS : {gms}");

                        if (AppSettings.TurnOnLogH0)
                        {
                            mongoUtilities.AddLog(0, "H0 | Giữ kết nối", requestId, imei,
                                MongoUtilities.EnumMongoLogType.Start,
                                $"Imei: {imei} | H0 - HEARTBEAT COMMAND | Thời gian : {time} | Pin : {pin} % | Lock (1: Lock \\ 0: Unlock) : {comm[5]} | GMS : {gms}");

                        }

                        pinDockLogging.AddLog(requestId, imei, percent, Convert.ToInt32(comm[6]), false, DateTime.Now);

                        checkCharging.ProcessCharging(imei, percent);

                        dock = dockModel.GetOneDock(imei);

                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1",
                                    "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LockStatus = comm[5] != "0";
                            dock.Battery = percent;
                            dock.LastConnectionTime = DateTime.Now;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("ProcessMessageH0.Update",
                                $"RequestId: {requestId} | Imei: {imei} | H0 - HEARTBEAT COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");

                            if (AppSettings.TurnOnLogH0)
                            {
                                mongoUtilities.AddLog(0, "H0 (Update) | Giữ kết nối (Cập nhật khóa)", requestId, imei,
                                    MongoUtilities.EnumMongoLogType.End,
                                    $"RequestId: {requestId} | Imei: {imei} | H0 - HEARTBEAT COMMAND | Thời gian : {time} | Pin : {pin} % | Lock (1: Lock \\ 0: Unlock) : {comm[5]} | GMS : {gms}");

                            }
                        }
                        else
                        {
                            dock = new Dock(imei, percent, 0, 0);
                            dock.LockStatus = comm[5] != "0";
                            dockModel.InsertDock(dock);

                            var messageS5H0 = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},S5#<LF>";
                            TNSessionMap.SendMessage(session, imei, messageS5H0, requestId, true, "Send S5",
                                "Kiểm tra trạng thái", MongoUtilities.EnumMongoLogType.Center);


                            Utilities.WriteOperationLog("ProcessMessageH0.Insert",
                                $"RequestId: {requestId} | Imei: {imei} | H0 - HEARTBEAT COMMAND | Dock: {dock.ToString()}");

                            if (AppSettings.TurnOnLogH0)
                            {
                                mongoUtilities.AddLog(0, "H0 (Insert) | Giữ kết nối (Thêm mới khóa)", requestId, imei,
                                    MongoUtilities.EnumMongoLogType.End,
                                    $"Imei: {imei} | H0 - HEARTBEAT COMMAND | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageH0", $"RequestId: {requestId} | RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageH0", $"RequestId: {requestId} | RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
