﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.RabbitMQ;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdD0
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private BookingModel bookingModel = new BookingModel();
        private WarningProcess warningProcess = new WarningProcess();
        private int status = 0;
        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageD0(List<string> command, string requestId, IoSession session, RabbitMqPush push)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageD0(command, requestId, session, push);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageD0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageD0(List<string> command, string requestId, IoSession session, RabbitMqPush push)
        {
            await ProcessMessageD0(command, requestId, session, push);
        }

        private async Task ProcessMessageD0(List<string> comm, string requestId, IoSession session, RabbitMqPush push)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                        var message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},Re,D0#<LF>";

                        var data12 = comm.Count == 18 ? comm[17] : comm[16];
                        var data11 = comm.Count == 18 ? comm[16] : "";

                        var data10 = comm[15];
                        var data9 = comm[14];
                        var data8 = comm[13];
                        var data7 = comm[12];
                        var data6 = comm[11];
                        var data5 = Utilities.getLatitudeLongitude(comm[10], false); // LONG
                        var data4 = comm[9];
                        var data3 = Utilities.getLatitudeLongitude(comm[8]); // LAT
                        var data2 = comm[7];
                        var data1 = comm[6];
                        var data0 = comm[5];

                        var location = $@"https://maps.google.com?q={data3},{data5}";

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : D0 - POSITIONING COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER D0 : {string.Join(",", comm)}");

                            Console.ForegroundColor = ConsoleColor.Yellow;

                            Console.WriteLine($"<1> : {data0}");
                            Console.WriteLine($"<2> : {data1} (hhmmss / UTC)");

                            if (data2 == "V" || data2 == "VA")
                            {
                                Console.WriteLine($"<3> : Location status : {data2} = Định vị không hoạt động");
                            }
                            else if (data2 == "A")
                            {
                                Console.WriteLine($"<3> : Location status : {data2} = Định vị hoạt động");
                            }

                            Console.WriteLine($"<4> : {data3} | Vĩ độ ddmm.mmmm");
                            Console.WriteLine($"<5> : {data4} | N (Bán cầu Bắc) | S (Bán cầu Nam)");
                            Console.WriteLine($"<6> : {data5} | Kinh độ dddmm.mmmm");
                            Console.WriteLine($"<7> : {data6} | E (Đông) | W (Tây)");
                            Console.WriteLine($"<8> : {data7} | Vận tốc");
                            Console.WriteLine($"<9> : {data8} | HDOP độ chính sác định vị");
                            Console.WriteLine($"<10> : {data9} | Ngày UTC | ddmmyy");
                            Console.WriteLine($"<11> : {data10} | Độ cao  so với mực nước biển");
                            Console.WriteLine($"<12> : {data11} | Đơn vị chiều cao mét");

                            if (data12 == "N#")
                            {
                                Console.WriteLine("<13> : N = Dữ liệu định vị không hợp lệ");
                            }
                            else if (data12 == "A#")
                            {
                                Console.WriteLine("<13> : A = Định vị tự động");
                            }
                            else if (data12 == "D#")
                            {
                                Console.WriteLine("<13> : D = Khác");
                            }
                            else
                            {
                                Console.WriteLine("<13> : E = Ước lượng");
                            }

                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message ?? "No Message"}");
                        }

                        Utilities.WriteOperationLog("ProcessMessageD0.HandCommand", $"RequestId: {requestId} | Imei: {imei} | D0 - POSITIONING COMMAND | Thời gian: {time} | Trạng thái định vị (A: Active \\ V|VA: Not Active) : {data2} | Lat (Vĩ Độ): {data3} | Long (Kinh độ): {data5} | {data4} - {data6} | Point HDOP: {data8} | Altitude: {data10} | Mode (A: Định vị tự động\\ D: Khác\\ E: Ước lượng\\ N: Dữ liệu lỗi): {data12} | Resend : {status} | {location}");
                        if (AppSettings.TurnOnLogD0)
                        {
                            mongoUtilities.AddLog(0, "D0 | Vị trí", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | D0 - POSITIONING COMMAND | Thời gian: {time} | Trạng thái định vị (A: Active \\ V|VA: Not Active) : {data2} | Lat (Vĩ Độ): {data3} | Long (Kinh độ): {data5} | {data4} - {data6} | Point HDOP: {data8} | Altitude: {data10} | Mode (A: Định vị tự động\\ D: Khác\\ E: Ước lượng\\ N: Dữ liệu lỗi): {data12} | Resend : {status} | {location}");
                        }

                        Utilities.WriteOperationLog("ProcessMessageD0.Resend", $"RequestId: {requestId} | Message : {message}");

                        TNSessionMap.SendMessage(session, imei, message, requestId, true, "D0 (Resend)", "Vị trí (Phản hồi D0)", MongoUtilities.EnumMongoLogType.Center);

                        if (AppSettings.ModeProcessD0)
                        {
                            var dataD0Rabbit = new DataD0Rabbit(imei, data3, data5, requestId);
                            push.PushMessage($"{dataD0Rabbit.ToString()}");
                        }
                        else
                        {
                            dock = dockModel.GetOneDock(imei);
                            if (dock != null && dock.Id > 0)
                            {
                                dock.LastConnectionTime = DateTime.Now;
                                dock.ConnectionStatus = true;
                                dock.UpdatedDate = DateTime.Now;

                                if (data3 != 0 && data5 != 0)
                                {
                                    if (String.IsNullOrEmpty(dock.RFID))
                                    {
                                        // Gửi lệnh khóa RFID
                                        var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                        TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                        dock.RFID = rfidCode;
                                    }

                                    dock.LastGPSTime = DateTime.Now;
                                    dock.Lat = data3;
                                    dock.Long = data5;

                                    dock.ConnectionStatus = true;
                                    dockModel.UpdateDock(dock);
                                    dockModel.UpdateBike(dock.Id, dock.Lat, dock.Long, requestId, dock.IMEI);

                                    Utilities.WriteOperationLog("ProcessMessageD0.Update", $"RequestId: {requestId} | Imei: {imei} | D0 - POSITIONING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lat: {data3} - Long: {data5} | Lock Status: {dock.LockStatus}");

                                    if (AppSettings.TurnOnLogD0)
                                    {
                                        mongoUtilities.AddLog(0, "D0 (Update) | Vị trí (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | D0 - POSITIONING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lat: {data3} - Long: {data5} | Lock Status: {dock.LockStatus}");
                                    }
                                }
                                else
                                {
                                    dock.ConnectionStatus = true;
                                    dockModel.UpdateDock(dock);
                                    Utilities.WriteOperationLog("ProcessMessageD0.Update", $"RequestId: {requestId} | Imei: {imei} | D0 - POSITIONING COMMAND | Last GPS Time: {dock.LastGPSTime} | Lock Status: {dock.LockStatus}");
                                }

                                var booking = bookingModel.GetOneBooking(dock.Id);
                                if (booking == null)
                                {
                                    // Gửi gói tin tắt định vị tự động D1
                                    var messageD1Off = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},D1,0#<LF>";
                                    TNSessionMap.SendMessage(session, imei, messageD1Off, requestId, true, "Turn Off D1", "Tắt định vị tự động", MongoUtilities.EnumMongoLogType.Center);

                                    Utilities.WriteOperationLog("ProcessMessageD0.TurnOffD0", $"RequestId: {requestId} | Imei: {imei} | Id: {dock.Id} | D0 - LOCK COMMAND | Tắt định vị tự động - Xe không trong chuyến");

                                    if (JobCheckWarning.DATACHECKWARNINGS.TryGetValue(imei, out var warning))
                                    {
                                        if (warning.DataGps.Count >= 50)
                                        {
                                            warning.DataGps.RemoveAt(0);
                                        }
                                        warning.DataGps.Add(new CheckGps(data3, data5, DateTime.Now));
                                    }
                                    else
                                    {
                                        var checkWarning = new CheckWarningEntity(imei, data3, data5, DateTime.Now);
                                        JobCheckWarning.DATACHECKWARNINGS.Add(imei, checkWarning);
                                    }
                                }
                                else
                                {
                                    Utilities.WriteOperationLog("ProcessMessageD0.TurnOffD0", $"RequestId: {requestId} | Imei: {imei} | Id: {dock.Id} | D0 - LOCK COMMAND | Giữ định vị tự động - Xe đang trong chuyến");
                                }
                            }
                            else
                            {
                                dock = new Dock(imei, 0, data3, data5, true);
                                dockModel.InsertDock(dock);


                                Utilities.WriteOperationLog("ProcessMessageD0.Insert", $"RequestId: {requestId} | Imei: {imei} | D0 - POSITIONING COMMAND | Dock: {dock.ToString()}");

                                if (AppSettings.TurnOnLogD0)
                                {
                                    mongoUtilities.AddLog(0, "D0 (Insert) | Vị trí (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | D0 - POSITIONING COMMAND | Dock: {dock.ToString()}");

                                }
                            }

                            SqlHelper.InsertGPSTask(imei, data3, data5, requestId);

                            //new BikeWarningModel().InsertBikeWarningTask(imei, requestId, EBikeWarningType.Undefined, data3, data5, dock.Id, true);
                            warningProcess.ProcessWarningGps(imei, requestId, data3, data5, dock.Id);
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageD0", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageD0", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
