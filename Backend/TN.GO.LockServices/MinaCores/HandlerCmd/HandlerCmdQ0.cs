﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Process;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdQ0
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private CheckCharging checkCharging = new CheckCharging();
        private PinDockLogging pinDockLogging = new PinDockLogging();

        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageQ0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageQ0(comm, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageQ0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageQ0(List<string> comm, string requestId, IoSession session)
        {
            await ProcessMessageQ0(comm, requestId, session);
        }

        private async Task ProcessMessageQ0(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var percent = Utilities.CheckPercent(comm[5].Replace("#", ""));
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                        var pin = $"{percent} %";

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Q0 - SIGN IN COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Q0 - IMEI = {imei}");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER Q0 : {string.Join(",", comm)}");


                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Thời gian : {time}");
                            Console.WriteLine($"Pin       : {pin}");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Utilities.WriteOperationLog("MinaOmni.HandCommand", $"RequestId: {requestId} | Imei: {imei} | Q0 - SIGN IN COMMAND | Thời gian : {time} | Pin : {pin}");

                        if (AppSettings.TurnOnLogQ0)
                        {
                            mongoUtilities.AddLog(0, "Q0 | Kết nối sv", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Q0 - SIGN IN COMMAND | Thời gian : {time} | Pin : {pin}");
                        }

                        pinDockLogging.AddLog(requestId, imei, percent, Convert.ToInt32(comm[5].Replace("#", "")), false, DateTime.Now);

                        checkCharging.ProcessCharging(imei, percent);

                        dock = dockModel.GetOneDock(imei);
                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.Battery = percent;
                            dock.LastConnectionTime = DateTime.Now;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Update", $"RequestId: {requestId} | Imei: {imei} | Q0 - SIGN IN COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");

                            if (AppSettings.TurnOnLogQ0)
                            {
                                mongoUtilities.AddLog(0, "Q0 (Update) | Kết nối sv (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | Q0 - SIGN IN COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");
                            }

                        }
                        else
                        {
                            dock = new Dock(imei, percent, 0, 0);
                            dockModel.InsertDock(dock);

                            // Gửi gói tin kiểm tra trạng thái khóa
                            var messageS5Q0 = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},S5#<LF>";
                            TNSessionMap.SendMessage(session, imei, messageS5Q0, requestId, true, "Send S5", "Kiểm tra trạng thái", MongoUtilities.EnumMongoLogType.Center);


                            // Gửi lệnh lấy thông tin mã ICCID của sim
                            var message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},I0#<LF>";
                            TNSessionMap.SendMessage(session, imei, message, requestId, true, "Send I0", "Lấy mã ICCID", MongoUtilities.EnumMongoLogType.Center);

                            // Gửi lệnh khóa RFID
                            var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                            TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);

                            Utilities.WriteOperationLog("HandCommand.Insert", $"RequestId: {requestId} | Imei: {imei} | Q0 - SIGN IN COMMAND | Dock: {dock.ToString()}");

                            if (AppSettings.TurnOnLogQ0)
                            {
                                mongoUtilities.AddLog(0, "Q0 (Insert) | Kết nối sv (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | Q0 - SIGN IN COMMAND | Dock: {dock.ToString()}");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageQ0", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageQ0", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
