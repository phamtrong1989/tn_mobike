﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdD1
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();
        private string rfidCode = AppSettings.DefaultRFID;

        public void HandlerMessageD1(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageD1(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageG0", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageD1(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageD1(command, requestId, session);
        }

        private async Task ProcessMessageD1(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : D1 - TRACKING LOCATION COMMAND");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER S5 : {string.Join(",", comm)}");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Location tracking each : {comm[5].Replace("#", "")} giây");
                        }

                        Utilities.WriteOperationLog("ProcessMessageD1.HandCommand", $"RequestId: {requestId} | Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Thời gian: {time} | Bật tự động gửi vị trí mỗi {comm[5].Replace("#", "")} giây");
                        if (AppSettings.TurnOnLogD1)
                        {
                            mongoUtilities.AddLog(0, "D1 | Định vị tự động", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Thời gian: {time} | Bật tự động gửi vị trí mỗi {comm[5].Replace("#", "")} giây");

                        }
                        dock = dockModel.GetOneDock(imei);
                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LastConnectionTime = DateTime.Now;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("ProcessMessageD1.Update", $"RequestId: {requestId} | Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                            if (AppSettings.TurnOnLogD1)
                            {
                                mongoUtilities.AddLog(0, "D1 (Update) | Định vị tự động (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                            }
                        }
                        else
                        {
                            dock = new Dock(imei, 0, 0, 0);
                            dockModel.InsertDock(dock);

                            Utilities.WriteOperationLog("ProcessMessageD1.Insert", $"RequestId: {requestId} | Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Dock: {dock.ToString()}");
                            if (AppSettings.TurnOnLogD1)
                            {
                                mongoUtilities.AddLog(0, "D1 (Insert) | Định vị tự động (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageD1", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageD1", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }

            await Task.Delay(1);
        }
    }
}
