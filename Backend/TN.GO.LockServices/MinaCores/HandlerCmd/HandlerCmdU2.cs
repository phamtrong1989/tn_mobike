﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores.HandlerCmd
{
    class HandlerCmdU2
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        private DockModels dockModel = new DockModels();

        private string rfidCode = AppSettings.DefaultRFID;
        public void HandlerMessageU2(List<string> command, string requestId, IoSession session)
        {
            try
            {
                Task.Run(() =>
                {
                    AddMessageU2(command, requestId, session);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("HandlerMessageU2", $"Error: RequestId: {requestId} | {e.Message}");
            }
        }

        private async void AddMessageU2(List<string> command, string requestId, IoSession session)
        {
            await ProcessMessageU2(command, requestId, session);
        }

        private async Task ProcessMessageU2(List<string> comm, string requestId, IoSession session)
        {
            try
            {
                if (comm.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = comm[4].ToUpper();
                    var imei = comm[2];
                    var imeiLong = Convert.ToInt64(imei);

                    Dock dock = null;

                    try
                    {
                        var time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : U2 - Notification of upgrade results");
                            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER U2 : {string.Join(",", comm)}");
                        }

                        Utilities.WriteOperationLog("MinaOmni.HandCommand", $"RequestId: {requestId} | Imei: {imei} | U2 - Notification of upgrade results | Thời gian: {time} | Code: {comm[5]} | Status: {comm[6].Replace("#", "")}");

                        if (AppSettings.TurnOnLogU2)
                        {
                            mongoUtilities.AddLog(0, "U2 | Kết quả nâng cấp", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | U2 - Notification of upgrade results | Thời gian: {time} | Code: {comm[5]} | Status: {comm[6].Replace("#", "")}");

                        }

                        dock = dockModel.GetOneDock(imei);
                        if (dock != null && dock.Id > 0)
                        {
                            if (String.IsNullOrEmpty(dock.RFID))
                            {
                                // Gửi lệnh khóa RFID
                                var messageRFID = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                                TNSessionMap.SendMessage(session, imei, messageRFID, requestId, true, "Send C1", "Khóa mở bằng RFID", MongoUtilities.EnumMongoLogType.Center);
                                dock.RFID = rfidCode;
                            }

                            dock.LastConnectionTime = DateTime.Now;
                            dock.ConnectionStatus = true;
                            dockModel.UpdateDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Update", $"RequestId: {requestId} | Imei: {imei} | U2 - Notification of upgrade | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                            if (AppSettings.TurnOnLogU2)
                            {
                                mongoUtilities.AddLog(0, "U2 (Update) | Kết quả nâng cấp (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | U2 - Notification of upgrade | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                            }
                        }
                        else
                        {
                            dock = new Dock(imei, 0, 0, 0);
                            dockModel.InsertDock(dock);

                            Utilities.WriteOperationLog("HandCommand.Insert", $"RequestId: {requestId} | Imei: {imei} | U2 - Notification of upgrade | Dock: {dock.ToString()}");

                            if (AppSettings.TurnOnLogU2)
                            {
                                mongoUtilities.AddLog(0, "U2 (Insert) | Kết quả nâng cấp (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | U2 - Notification of upgrade | Dock: {dock.ToString()}");

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("ProcessMessageU2", $"RequestId: {requestId} | Error Handler : {e.Message}");
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProcessMessageU2", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }
            await Task.Delay(1);
        }
    }
}
