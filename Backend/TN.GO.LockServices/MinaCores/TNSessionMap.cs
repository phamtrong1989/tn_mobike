﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Buffer;
using Mina.Core.Session;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores
{
    class TNSessionMap
    {
        public static IDictionary<string, IoSession> MAPIP = new Dictionary<string, IoSession>();

        public static IDictionary<long, IoSession> MAPIMEI = new Dictionary<long, IoSession>();



        #region SESSION PROCESS WITH IMEI

        public static void AddSession(long key, IoSession session, string requestId = "")
        {
            try
            {
                var keyCheck = MAPIMEI.FirstOrDefault(x => x.Value == session).Key;
                if (keyCheck != 0)
                {
                    if (keyCheck == key)
                    {
                        if(AppSettings.ModeRun)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine($"KEY ADD SESSION = {key} - SUCCESSFUL Key = KeyCheck !");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        MAPIMEI[key] = session;
                        Utilities.WriteOperationLog("MinaOmni.AddSession", $"Imei: {key} | Add session has this key Successful !");

                        if (AppSettings.TurnOnLogSession)
                        {
                            new MongoUtilities().AddLog(0, "AddSession (Imei) | Thêm khóa kết nối", requestId, $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Add session has this key Successful !");

                        }
                    }
                    else
                    {
                        MAPIMEI.Add(key, session);
                        if (AppSettings.ModeRun)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine($"KEY ADD SESSION = {key} - SUCCESSFUL Key != KeyCheck !");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Utilities.WriteOperationLog("MinaOmni.AddSession", $"Imei: {key} | Add session has this key Successful !");

                        if (AppSettings.TurnOnLogSession)
                        {
                            new MongoUtilities().AddLog(0, "AddSession (Imei) | Thêm khóa kết nối", requestId, $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Add session has this key Successful !");

                        }
                    }
                }
                else
                {
                    MAPIMEI.Add(key, session);
                    if (AppSettings.ModeRun)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"KEY ADD SESSION = {key} - SUCCESSFUL KeyCheck = 0 !");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.AddSession", $"Imei: {key} | Add session has this key Successful !");

                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "AddSession (Imei) | Thêm khóa kết nối", requestId, $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Add session has this key Successful !");

                    }
                }
            }
            catch (Exception e)
            {
                //Utilities.WriteErrorLog("MinaOmni.AddSession", $"Imei: {key} | Error : {e.Message}");

                if (AppSettings.TurnOnLogSession)
                {
                    new MongoUtilities().AddLog(0, "AddSession (IMEI-Error) | Thêm khóa kết nối (Lỗi)", requestId, $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error : {e.Message}", true);

                }
            }
        }

        public static IoSession GetSession(long key)
        {
            if (MAPIMEI.TryGetValue(key, out var session))
            {
                return session;
            }
            else
            {
                return null;
            }
        }

        public static bool ConstantKey(long key)
        {
            return MAPIMEI.ContainsKey(key);
        }

        public static void RemoveSession(long imei)
        {
            try
            {
                if (MAPIMEI.TryGetValue(imei, out var session))
                {
                    session.CloseOnFlush();
                    try
                    {
                        session.CloseNow();
                    }
                    catch (Exception)
                    {
                        session.Close(true);
                    }

                    MAPIMEI.Remove(imei);
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Imei: {imei} | Remove session successful !");
                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "RemoveSession | Xóa khóa", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Remove session successful !");

                    }
                }
                else
                {
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Imei: {imei} | Session has not exist with key !");
                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "RemoveSession (IP) | Xóa khóa", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Session has not exist with key !");

                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("TNGO.RemoveSession", $"Imei: {imei} | Error : {e.Message}");
                if (AppSettings.TurnOnLogSession)
                {
                    new MongoUtilities().AddLog(0, "RemoveSession (IP - Error) | Xóa khóa (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Error : {e.Message}", true);

                }
            }
        }

        public static void RemoveSessionImei(IoSession session)
        {
            long key = 0;
            try
            {
                key = MAPIMEI.FirstOrDefault(s => s.Value == session).Key;
                if (key != 0)
                {
                    session.CloseOnFlush();
                    try
                    {
                        session.CloseNow();
                    }
                    catch (Exception)
                    {
                        session.Close(true);
                    }
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Imei: {key} | Tổng sổ session Imei trước khi xóa: {MAPIMEI.Count}");
                    MAPIMEI.Remove(key);
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Imei: {key} | Remove session successful ! | Tổng sổ session Imei sau khi xóa: {MAPIMEI.Count}");

                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "RemoveSession | Xóa khóa", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Remove session successful !");
                    }
                }
                else
                {
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Imei: {key} | Session has not exist with key !");
                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "RemoveSession | Xóa khóa", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Session has not exist with key !");

                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("TNGO.RemoveSession", $"Imei: {key} | Error : {e.Message}");
                if (AppSettings.TurnOnLogSession)
                {
                    new MongoUtilities().AddLog(0, "RemoveSession (Error) | Xóa khóa (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error : {e.Message}", true);

                }
            }
        }
        #endregion

        public static void SessionIdle(IoSession session, IdleStatus status)
        {
            try
            {
                if (status == IdleStatus.ReaderIdle)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"IDLE : {session.GetIdleCount(status)}");
                        Console.WriteLine("READER IDLE");
                    }
                    
                    RemoveSession(session);
                    RemoveSessionImei(session);
                }
                else if (status == IdleStatus.WriterIdle)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"IDLE : {session.GetIdleCount(status)}");
                        Console.WriteLine("WRITER IDLE");
                    }
                }
                else
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"IDLE : {session.GetIdleCount(status)}");
                        Console.WriteLine("BOTH IDLE");
                    }

                    RemoveSession(session);
                    RemoveSessionImei(session);
                }

                Utilities.WriteOperationLog("MinaOmni.SessionIdle", $"Status : {status.ToString()}");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.SessionIdle", $"Error : {e.Message}");
            }
        }


        #region SESSION PROCESS WITH IPADDRESS

        public static void AddSession(string key, IoSession session, string requestId = "")
        {
            try
            {
                var keyCheck = MAPIP.FirstOrDefault(s => s.Key == key);
                if (key != null)
                {
                    if (keyCheck.Key == key)
                    {
                        if (AppSettings.ModeRun)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine($"KEY ADD SESSION = {key} - SUCCESSFUL Key != KeyCheck !");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        MAPIP[keyCheck.Key] = session;
                        Utilities.WriteOperationLog("MinaOmni.AddSession", $"Imei: {key} | Add session has this key Successful !");

                        if (AppSettings.TurnOnLogSession)
                        {
                            new MongoUtilities().AddLog(0, "AddSession (IP) | Thêm khóa kết nối", requestId, $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Add session has this key Successful !");

                        }
                    }
                    else
                    {
                        MAPIP.Add(key, session);
                        if (AppSettings.ModeRun)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine($"KEY ADD SESSION = {key} - SUCCESSFUL Key != KeyCheck !");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Utilities.WriteOperationLog("MinaOmni.AddSession", $"Imei: {key} | Add session has this key Successful !");

                        if (AppSettings.TurnOnLogSession)
                        {
                            new MongoUtilities().AddLog(0, "AddSession (IP) | Thêm khóa kết nối", requestId, $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Add session has this key Successful !");

                        }
                    }
                }
                else
                {
                    MAPIP.Add(key, session);
                    if (AppSettings.ModeRun)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"KEY ADD SESSION = {key} - SUCCESSFUL KeyCheck = null !");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.AddSession", $"Imei: {key} | Add session has this key Successful !");

                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "AddSession (IP) | Thêm khóa kết nối", requestId, $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Add session has this key Successful !");

                    }
                }
            }
            catch (Exception e)
            {
                //Utilities.WriteErrorLog("MinaOmni.AddSession", $"Imei: {key} | Error : {e.Message}");

                if (AppSettings.TurnOnLogSession)
                {
                    new MongoUtilities().AddLog(0, "AddSession (IP - Error) | Thêm khóa kết nối (Lỗi)", requestId, $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error : {e.Message}", true);

                }
            }
        }

        public static IoSession GetSession(string ipAddress)
        {
            if (MAPIP.TryGetValue(ipAddress, out var session))
            {
                return session;
            }
            else
            {
                return null;
            }
        }

        public static bool ConstantKey(string key)
        {
            return MAPIP.ContainsKey(key);
        }

        public static void RemoveSession(IoSession session)
        {
            string key = "";
            try
            {
                IPEndPoint endPoint = session.RemoteEndPoint as IPEndPoint;
                var keyCheck = $"{endPoint?.Address}:{endPoint?.Port}:{session.Id}";

                key = MAPIP.FirstOrDefault(s => s.Key == keyCheck).Key;

                if (!String.IsNullOrEmpty(key))
                {
                    session.CloseOnFlush();
                    try
                    {
                        session.CloseNow();
                    }
                    catch (Exception)
                    {
                        session.Close(true);
                    }

                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Imei: {key} | Tổng số session trước khi xóa: {MAPIP.Count}");
                    MAPIP.Remove(key);
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Imei: {key} | Remove session successful ! | Tổng số session sau khi xóa: {MAPIP.Count}");
                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "RemoveSession (IP) | Xóa khóa", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Remove session successful !");

                    }
                }
                else
                {
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Imei: {key} | Session has not exist with key !");

                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "RemoveSession (IP) | Xóa khóa", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Session has not exist with key !");

                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("TNGO.RemoveSession", $"Imei: {key} | Error : {e.Message}");

                if (AppSettings.TurnOnLogSession)
                {
                    new MongoUtilities().AddLog(0, "RemoveSession (IP - Error) | Xóa khóa (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error : {e.Message}", true);

                }
            }
        }

        public static void RemoveSession(string key)
        {
            try
            {
                if (MAPIP.TryGetValue(key, out var session))
                {
                    session.CloseNow();
                    try
                    {
                        session.CloseOnFlush();
                    }
                    catch (Exception)
                    {
                        session.Close(true);
                    }
                    MAPIP.Remove(key);
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Ip: {key} | Remove session successful !");

                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "RemoveSession (IP) | Xóa khóa", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Remove session successful !");

                    }
                }
                else
                {
                    Utilities.WriteOperationLog("TNGO.RemoveSession", $"Ip: {key} | Session has not exist with key !");
                    if (AppSettings.TurnOnLogSession)
                    {
                        new MongoUtilities().AddLog(0, "RemoveSession (IP) | Xóa khóa", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Session has not exist with key !");

                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.RemoveSession", $"Ip: {key} | Error : {e.Message}");
                if (AppSettings.TurnOnLogSession)
                {
                    new MongoUtilities().AddLog(0, "RemoveSession (IP - Error) | Xóa khóa (Lỗi)", $"{DateTime.Now:yyyyMMddHHmmssffffff}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error : {e.Message}", true);

                }
            }
        }

        #endregion

        public static bool SendMessageArrWithKey(string ipAddress, string key, byte[] messBytes, string requestId)
        {
            var session = GetSession(ipAddress);
            if (session != null)
            {
                try
                {
                    var resultSend = session.Write(IoBuffer.Wrap(messBytes));
                    return resultSend.Done;
                }
                catch (Exception e)
                {

                    if (AppSettings.TurnOnLogSendUnlock)
                    {
                        new MongoUtilities().AddLog(0, "SendMessage (Error) | Gửi message (Lỗi)", $"{requestId}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error: {e.Message} !");

                    }
                    return false;
                }
            }
            else
            {
                Utilities.WriteOperationLog("MinaOmni.Unlock.1", $"Imei: {key} | Error: This lock has not connected the services !");

                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, "SendMessage (Error) | Gửi message (Lỗi)", $"{requestId}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error: This lock has not connected the services !", true);

                }
                return false;
            }
        }

        public static bool SendMessageArrNew(IoSession session, string imei, byte[] messBytes, string requestId)
        {
            try
            {
                var resultSend = session.Write(IoBuffer.Wrap(messBytes), session.RemoteEndPoint);

                return resultSend.Done;
            }
            catch (Exception e)
            {
                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, "SendMessage (Error) | Gửi message (Lỗi)", $"{requestId}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Error: {e.Message} !", true);

                }
                return false;
            }
        }

        public static bool SendMessageArrWithSession(IoSession session, string imei, byte[] messBytes, string requestId)
        {
            try
            {
                var resultSend = session.Write(IoBuffer.Wrap(messBytes));

                return resultSend.Done;
            }
            catch (Exception e)
            {
                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, "SendMessage (Error) | Gửi message (Lỗi)", $"{requestId}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Error: {e.Message} !", true);

                }
                return false;
            }
        }

        public static bool SendMessageStrWithKey(string ipAddress, string key, string message, string requestId)
        {
            var session = GetSession(ipAddress);
            if (session != null)
            {
                try
                {
                    var resultSend = session.Write(message);
                    return resultSend.Done;
                }
                catch (Exception e)
                {
                    if (AppSettings.TurnOnLogSendUnlock)
                    {
                        new MongoUtilities().AddLog(0, "SendMessage (Error) | Gửi message (Lỗi)", $"{requestId}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error: {e.Message} !", true);

                    }
                    return false;
                }
            }
            else
            {
                Utilities.WriteOperationLog("MinaOmni.Unlock.1", $"Imei: {key} | Error: This lock has not connected the services !");

                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, "SendMessage (Error) | Gửi message (Lỗi)", $"{requestId}", $"{key}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {key} | Error: This lock has not connected the services !");

                }
                return false;
            }
        }

        public static bool SendMessageStrWithSession(IoSession session, string imei, string message, string requestId)
        {
            try
            {
                var resultSend = session.Write(message);

                return resultSend.Done;
            }
            catch (Exception e)
            {
                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, "SendMessage (Error) | Gửi message (Lỗi)", $"{requestId}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Error: {e.Message} !", true);

                }
                return false;
            }
        }

        public static bool SendMessage(string ipAddress, string key, string message, string requestId = "", bool isLog = false, string titleLog1 = "", string titleLog2 = "", MongoUtilities.EnumMongoLogType enumMongo = MongoUtilities.EnumMongoLogType.Start)
        {
            var command = Utilities.AddBytes(new byte[] { (byte)0xFF, (byte)0xFF }, Encoding.UTF8.GetBytes(message));
            var result = SendMessageArrWithKey(ipAddress, key, command, requestId);

            if (isLog)
            {
                var status = result ? $"Send message {message} successful !" : $"Send message {message} fail !";
                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, $"{titleLog1} (ARR) | {titleLog2}", $"{requestId}", key, enumMongo, $"Imei: {key} | Status: {status}");
                }
            }

            return result;
        }

        public static bool SendMessageNew(IoSession session, string key, string message, string requestId = "", bool isLog = false, string titleLog1 = "", string titleLog2 = "", MongoUtilities.EnumMongoLogType enumMongo = MongoUtilities.EnumMongoLogType.Start)
        {
            var command = Utilities.AddBytes(new byte[] { (byte)0xFF, (byte)0xFF }, Encoding.UTF8.GetBytes(message));
            var result = SendMessageArrNew(session, key, command, requestId);

            if (isLog)
            {
                var status = result ? $"Send message {message} successful !" : $"Send message {message} fail !";
                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, $"{titleLog1} (ARR) | {titleLog2}", $"{requestId}", key, enumMongo, $"Imei: {key} | Status: {status}");

                }
            }

            return result;
        }

        public static bool SendMessage(IoSession session, string key, string message, string requestId = "", bool isLog = false, string titleLog1 = "", string titleLog2 = "", MongoUtilities.EnumMongoLogType enumMongo = MongoUtilities.EnumMongoLogType.Start)
        {
            var command = Utilities.AddBytes(new byte[] { (byte)0xFF, (byte)0xFF }, Encoding.UTF8.GetBytes(message));
            var result = SendMessageArrWithSession(session, key, command, requestId);

            if (isLog)
            {
                var status = result ? $"Send message {message} successful !" : $"Send message {message} fail !";
                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, $"{titleLog1} (ARR) | {titleLog2}", $"{requestId}", key, enumMongo, $"Imei: {key} | Status: {status}");

                }
            }

            return result;
        }

        public static bool SendMessageStr(string ipAddress, string key, string message, string requestId = "", bool isLog = false, string titleLog1 = "", string titleLog2 = "", MongoUtilities.EnumMongoLogType enumMongo = MongoUtilities.EnumMongoLogType.Start)
        {
            var result = SendMessageStrWithKey(ipAddress, key, message, requestId);
            var status = result ? $"Send message {message} successful !" : $"Send message {message} fail !";

            if (AppSettings.TurnOnLogSendUnlock)
            {
                new MongoUtilities().AddLog(0, $"{titleLog1} (STR) | {titleLog2}", $"{requestId}", key, enumMongo, $"Imei: {key} | Status: {status}");

            }
            return result;
        }

        public static bool SendMessageStr(IoSession session, string key, string message, string requestId = "", bool isLog = false, string titleLog1 = "", string titleLog2 = "", MongoUtilities.EnumMongoLogType enumMongo = MongoUtilities.EnumMongoLogType.Start)
        {
            var result = SendMessageStrWithSession(session, key, message, requestId);
            var status = result ? $"Send message {message} successful !" : $"Send message {message} fail !";

            if (AppSettings.TurnOnLogSendUnlock)
            {
                new MongoUtilities().AddLog(0, $"{titleLog1} (STR) | {titleLog2}", $"{requestId}", key, enumMongo, $"Imei: {key} | Status: {status}");
            }
            return result;
        }
    }
}
