﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using TN.GO.LockServices.Entity;
using TN.GO.LockServices.Entity.SessionEntity;
using TN.GO.LockServices.JobScheduler;
using TN.GO.LockServices.MinaCores.HandlerCmd;
using TN.GO.LockServices.RabbitMQ;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores
{
    class TNSocketHandlerV2
    {
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        public static List<BikeWarningCount> ListBikeWarningCounts = new List<BikeWarningCount>() { };

        public static RabbitMqPush Push = new RabbitMqPush(AppSettings.RMQAddress, AppSettings.RMQExchanges, AppSettings.RMQUser, AppSettings.RMQPassword, AppSettings.RMQPort);

        public void MessageReceived(IoSession session, object message)
        {
            string requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";
            try
            {
                string msg = message != null ? message.ToString() : "";

                if (!String.IsNullOrEmpty(msg) && msg.Length > 0)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"Message : {msg}");
                    }

                    Utilities.WriteOperationLog("MinaOmni.ReceiverMessage", $"RequestID: {requestId} | {msg}");

                    if (msg.Contains("*CMDR") && msg.Contains("#"))
                    {
                        int firstIndex = msg.IndexOf("#", StringComparison.Ordinal);
                        int lastIndex = msg.LastIndexOf("#", StringComparison.Ordinal);

                        if (firstIndex == lastIndex)
                        {
                            int startIndex = msg.IndexOf("*CMDR", StringComparison.Ordinal);
                            string order = msg.Substring(startIndex, lastIndex + 1);
                            HandlerCommand(session, order, requestId);
                        }
                        else
                        {
                            var dataMsg = msg.Split('#').ToList();
                            foreach (var msgItem in dataMsg)
                            {
                                int startIndex = msgItem.IndexOf("*CMDR", StringComparison.Ordinal);
                                string orderItem = msgItem.Substring(startIndex, msgItem.Length);
                                HandlerCommand(session, $"{orderItem}#", requestId);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.MessageReceived", $"Error : {e.Message}");
                if (AppSettings.TurnOnLogHandleCmd)
                {
                    new MongoUtilities().AddLog(0, $"MessageReceived (Error) | Nhận gói tin (Lỗi)", $"{requestId}", "0x0000000000000", MongoUtilities.EnumMongoLogType.Start, $"[ERROR: {e.Message}]", true);

                }
            }
        }

        public void HandlerCommand(IoSession session, string command, string requestId)
        {
            try
            {
                var dataCmd = command.Split(',').ToList();

                if (command.Length < 1 || dataCmd.Count < 4)
                {
                    return;
                }
                else
                {
                    var comCode = dataCmd[4].ToUpper();
                    var imei = dataCmd[2];
                    var imeiLong = Convert.ToInt64(imei);

                    try
                    {
                        #region Add Session

                        TNSessionMap.AddSession(imeiLong, session, requestId);
                        IPEndPoint endPoint = session.RemoteEndPoint as IPEndPoint;
                        TNSessionMap.AddSession($"{endPoint?.Address}:{endPoint?.Port}:{session.Id}", session, requestId);

                        if (JobCheckConnection.DataConnection.TryGetValue(imei, out var value))
                        {
                            JobCheckConnection.DataConnection[imei] = new DataSession(DateTime.Now, comCode, imei, $"{endPoint?.Address}", session, value.LastSendL0, value.LastReceiverL0, value.TimeOut);
                        }
                        else
                        {
                            JobCheckConnection.DataConnection.Add(imei, new DataSession(DateTime.Now, comCode, imei, $"{endPoint?.Address}", session, DateTime.MinValue, DateTime.MinValue, 0));
                        }

                        #endregion

                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine($"MESSAGE FROM IMEI: {imei} | {command}");
                        }

                        Utilities.WriteOperationLog("MinaOmni.ReceiverMessage", $"RequestID: {requestId} | Imei: {imei} | {command}");

                        switch (comCode)
                        {
                            case "Q0":
                                new HandlerCmdQ0().HandlerMessageQ0(dataCmd, requestId, session);
                                break;

                            case "H0":
                                new HandlerCmdH0().HandlerMessageH0(dataCmd, requestId, session);
                                break;

                            case "L1":
                                new HandlerCmdL1().HandlerMessageL1(dataCmd, requestId, session);
                                break;

                            case "L0":
                                new HandlerCmdL0().HandlerMessageL0(dataCmd, requestId, session);
                                break;

                            case "D0":
                                new HandlerCmdD0().HandlerMessageD0(dataCmd, requestId, session, Push);
                                break;

                            case "D1":
                                new HandlerCmdD1().HandlerMessageD1(dataCmd, requestId, session);
                                break;

                            case "S5":
                                new HandlerCmdS5().HandlerMessageS5(dataCmd, requestId, session);
                                break;

                            case "S8":
                                new HandlerCmdS8().HandlerMessageS8(dataCmd, requestId, session);
                                break;

                            case "G0":
                                new HandlerCmdG0().HandlerMessageG0(dataCmd, requestId, session);
                                break;

                            case "W0":
                                new HandlerCmdW0().HandlerMessageW0(dataCmd, requestId, session);
                                break;

                            case "U0":
                                new HandlerCmdU0().HandlerMessageU0(dataCmd, requestId, session);
                                break;

                            case "U1":
                                new HandlerCmdU1().HandlerMessageU1(dataCmd, requestId, session);
                                break;

                            case "U2":
                                new HandlerCmdU2().HandlerMessageU2(dataCmd, requestId, session);
                                break;

                            case "K0":
                                new HandlerCmdK0().HandlerMessageK0(dataCmd, requestId, session);
                                break;

                            case "I0":
                                new HandlerCmdI0().HandlerMessageI0(dataCmd, requestId, session);
                                break;

                            case "M0":
                                new HandlerCmdM0().HandlerMessageM0(dataCmd, requestId, session);
                                break;

                            case "C1":
                                new HandlerCmdU1().HandlerMessageU1(dataCmd, requestId, session);
                                break;

                            case "C0":
                                new HandlerCmdC0().HandlerMessageC0(dataCmd, requestId, session);
                                break;

                            default:
                                Utilities.WriteErrorLog("MinaOmni.MessageReceived", $"RequestId: {requestId} | Code: {comCode}| Error Handler : Com code is not match !");
                                break;
                        }

                    }
                    catch (Exception e)
                    {
                        Utilities.WriteErrorLog("MinaOmni.MessageReceived", $"RequestId: {requestId} | Code: {comCode}| Error Handler : {e.Message}");
                        if (AppSettings.TurnOnLogHandleCmd)
                        {
                            mongoUtilities.AddLog(0, $"{comCode} (Error) | {comCode} (Lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Code: {comCode}| Error : {e.Message}", true);

                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.MessageReceived", $"RequestId: {requestId} | Error Handler : {e.Message}");
            }
        }
    }
}
