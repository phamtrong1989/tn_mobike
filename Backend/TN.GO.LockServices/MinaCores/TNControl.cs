﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Mina.Core.Buffer;
using Mina.Core.Future;
using Mina.Core.Service;
using Mina.Core.Session;
using Mina.Filter.Codec;
using Mina.Filter.Codec.TextLine;
using Mina.Filter.Executor;
using Mina.Filter.KeepAlive;
using Mina.Filter.Logging;
using Mina.Transport.Socket;
using TN.GO.LockServices.Models;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.MinaCores
{
    class TNControl
    {
        private readonly int PORT = AppSettings.PortService;
        public readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private IoAcceptor acceptor;
        //private AsyncSocketAcceptor acceptor;
        private IPAddress ipAddress = null;
        private DockModels dockModel = new DockModels();

        //private TNSocketHandler tnSocketHandler = new TNSocketHandler();

        private TNSocketHandlerV2 tnSocketHandlerV2 = new TNSocketHandlerV2();
        private TNSessionMap tnSessionMap = new TNSessionMap();
        public static List<IoSession> TotalSessions = new List<IoSession>();

        static readonly IoBuffer PING = IoBuffer.Wrap(new Byte[] { 1 });
        static readonly IoBuffer PONG = IoBuffer.Wrap(new Byte[] { 2 });
        //private static readonly Int32 INTERVAL = 5;
        //private static readonly Int32 TIMEOUT = 1;

        public static bool IsStarted = false;

        static Boolean CheckRequest(IoBuffer message)
        {
            var check = true;
            try
            {
                IoBuffer buff = message;
                check = buff.Get() == 1;
                buff.Rewind();

            }
            catch (Exception)
            {
                //
            }

            return check;
        }

        static Boolean CheckResponse(IoBuffer message)
        {
            var check = true;
            try
            {
                IoBuffer buff = message;
                check = buff.Get() == 2;
                buff.Rewind();

            }
            catch (Exception)
            {
                //
            }

            return check;
        }

        class ServerFactory : IKeepAliveMessageFactory
        {
            public Object GetRequest(IoSession session)
            {
                return null;
            }

            public Object GetResponse(IoSession session, Object request)
            {
                return PONG.Duplicate();
            }

            public Boolean IsRequest(IoSession session, Object message)
            {
                if (message is IoBuffer)
                {
                    return CheckRequest((IoBuffer)message);
                }
                return false;
            }

            public Boolean IsResponse(IoSession session, Object message)
            {
                if (message is IoBuffer)
                {
                    return CheckResponse((IoBuffer)message);
                }
                return false;
            }
        }

        public void Start2()
        {
            string _requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";
            try
            {
                acceptor = new AsyncSocketAcceptor(10000);
                acceptor.FilterChain.AddLast("logger", new LoggingFilter());
                acceptor.FilterChain.AddLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Encoding.UTF8)));
                acceptor.FilterChain.AddLast("exceutor", new ExecutorFilter());

                acceptor.Handler = new TNSocketHandler();
                acceptor.SessionConfig.ReadBufferSize = 2048;
                acceptor.SessionConfig.SetIdleTime(IdleStatus.ReaderIdle, 8*60);

                string hostName = Dns.GetHostName();

                ipAddress = !String.IsNullOrEmpty(AppSettings.HostService) ? IPAddress.Parse(AppSettings.HostService) : Dns.GetHostByName(hostName).AddressList[0];

                acceptor.Bind(new IPEndPoint(ipAddress, PORT));
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.StartServer", $"Listening on port: {PORT} | Error: {e.Message}");

                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, "StartServer (Error) | Bật server (Lỗi)", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Listening on port: {PORT} | Error: {e.Message}", true);
                }
            }
        }

        public void Start()
        {
            string _requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";
            try
            {
                acceptor = new AsyncSocketAcceptor(10000);

                IKeepAliveMessageFactory factory = new ServerFactory();
                KeepAliveFilter filter = new KeepAliveFilter(factory, IdleStatus.BothIdle);
                acceptor.FilterChain.AddLast("keep-alive", filter);

                acceptor.FilterChain.AddLast("logger", new LoggingFilter());
                acceptor.FilterChain.AddLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Encoding.UTF8)));
                acceptor.FilterChain.AddLast("exceutor", new ExecutorFilter());

                acceptor.Activated += (s, e) =>
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine("ACTIVATED");
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                };
                acceptor.Deactivated += (s, e) =>
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine("DEACTIVATED");
                        Console.BackgroundColor = ConsoleColor.Black;
                    }

                    Utilities.WriteOperationLog("Control.Deactivated", $"Deactived ! {e}");
                };
                acceptor.SessionCreated += (s, e) =>
                {
                    e.Session.Config.SetIdleTime(IdleStatus.BothIdle, 8 * 60);
                    e.Session.Config.ReadBufferSize = 2048;
                };

                acceptor.SessionOpened += (s, e) =>
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"OPEN SESSION | {e.Session.RemoteEndPoint}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    IPEndPoint endPoint = e.Session.RemoteEndPoint as IPEndPoint;


                    TNSessionMap.AddSession($"{endPoint?.Address}:{endPoint?.Port}:{e.Session.Id}", e.Session, _requestId);

                    TotalSessions.Add(e.Session);

                    Utilities.WriteOperationLog("SessionOpened", $"{endPoint?.Address}:{endPoint?.Port} | Id: {e.Session.Id} | Total Session: {TotalSessions.Count} | Total Session IP:PORT:ID : {TNSessionMap.MAPIP.Count} | Total Session Imei: {TNSessionMap.MAPIMEI.Count}");
                };
                acceptor.SessionClosed += (s, e) =>
                {
                    IPEndPoint endPoint = e.Session.RemoteEndPoint as IPEndPoint;
                    if (AppSettings.ModeRun)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"CLOSE SESSION | {endPoint?.Address}:{endPoint?.Port}:{e.Session.Id}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    TNSessionMap.RemoveSession(e.Session);
                    TNSessionMap.RemoveSessionImei(e.Session);

                    try
                    {
                        TotalSessions.Remove(e.Session);
                    }
                    catch (Exception ex)
                    {
                        Utilities.WriteErrorLog("TotalSessionRemove", $"{endPoint?.Address}:{endPoint?.Port} | Id: {e.Session.Id} | Error: {ex.Message}");
                    }

                    Utilities.WriteOperationLog("SessionClose", $"{endPoint?.Address}:{endPoint?.Port} | Id: {e.Session.Id} | Total Session: {TotalSessions.Count} | Total Session IP:PORT:ID : {TNSessionMap.MAPIP.Count} | Total Session Imei: {TNSessionMap.MAPIMEI.Count}");
                };
                acceptor.SessionIdle += (s, e) =>
                {
                    TNSessionMap.SessionIdle(e.Session, e.IdleStatus);
                };

                acceptor.MessageReceived += (s, e) =>
                {
                    new TNSocketHandlerV2().MessageReceived(e.Session, e.Message);

                    //try
                    //{
                    //    IoBuffer income = (IoBuffer)e.Message;
                    //    IoBuffer outcome = IoBuffer.Allocate(income.Remaining);
                    //    outcome.Put(income);
                    //    outcome.Flip();
                    //    e.Session.Write(outcome);
                    //}
                    //catch (Exception)
                    //{
                    //    //
                    //}
                };

                string hostName = Dns.GetHostName();

                ipAddress = !String.IsNullOrEmpty(AppSettings.HostService) ? IPAddress.Parse(AppSettings.HostService) : Dns.GetHostByName(hostName).AddressList[0];

                acceptor.Bind(new IPEndPoint(ipAddress, PORT));

                IsStarted = true;

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"START ON: {ipAddress} | LISTENING ON PORT : {PORT}");
                }

                Utilities.WriteOperationLog("MinaOmni.StartServer", $"Start on: {ipAddress} | Listening on port: {PORT}");

                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, "StartServer | Bật server", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Start on: {ipAddress} | Listening on port: {PORT}");
                    
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.StartServer", $"Listening on port: {PORT} | Error: {e.Message}");

                IsStarted = false;

                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, "StartServer (Error) | Bật server (Lỗi)", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Listening on port: {PORT} | Error: {e.Message}", true);
                }
            }
        }

        public void StopServer()
        {
            string _requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";
            try
            {
                acceptor?.Unbind();
                Utilities.WriteOperationLog("MinaOmni.StopServer", $"Stop server Mina Omni successful !");

                IsStarted = false;

                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, "StopServer | Tắt server", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Stop server Mina Omni successful !");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.StopServer", $"Error: {e.Message}");

                IsStarted = true;

                if (AppSettings.TurnOnLogStartSV)
                {
                    new MongoUtilities().AddLog(0, "StopServer (Error) | Tắt server (Lỗi)", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Error: {e.Message}", true);
                    
                }
            }
        }

        Dictionary<string, Tuple<string,string>> DataTitle = new Dictionary<string, Tuple<string, string>>()
        {
            {"L0", new Tuple<string, string>("Unlock","Mở khóa")},
            {"S8", new Tuple<string, string>("Ring","Đổ chuông")},
            {"D0", new Tuple<string, string>("Check Location","Kiểm tra vị trí")},
            {"S5", new Tuple<string, string>("Lock Info","Thông tin khóa")},
            {"G0", new Tuple<string, string>("Firmware Info","Thông tin Firmware")},
            {"I0", new Tuple<string, string>("ICCID Code","Mã ICCID")},
            {"M0", new Tuple<string, string>("Bluetooth Mac","Địa chỉ Mac Bluetooth")},
            {"S0", new Tuple<string, string>("Shutdown","Tắt nguồn")},
            {"S1", new Tuple<string, string>("Restart","Khởi động lại")},
            {"C0", new Tuple<string, string>("RFID Unlock","Mở khóa RFID")},
            {"C1", new Tuple<string, string>("Setting RFID","Cài đặt RFID")},
            {"D1", new Tuple<string, string>("Tracking Location ON","Định vị tự động bật")},
            {"D1O", new Tuple<string, string>("Tracking Location OFF","Định vị tự động tắt")}
        };

        public bool Unlock(string imei, string key = "L0", string rfidCode = "000000001A2B3C4D")
        {
            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";
            try
            {
                var message = "";
                var title1 = "Unlock";
                var title2 = "Mở khóa";

                if (DataTitle.TryGetValue(key, out var value))
                {
                    title1 = value.Item1;
                    title2 = value.Item2;
                }

                switch (key)
                {
                    case "L0":
                        int uid = 0;
                        long timestamp = (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
                        message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},L0,0,{uid},{timestamp}#<LF>";
                        break;
                    case "S8":
                        message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},S8,5,0#<LF>";
                        break;
                    case "D0":
                    case "S5":
                    case "G0":
                    case "I0":
                    case "M0":
                    case "S0":
                    case "S1":
                        message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},{key}#<LF>";
                        break;
                    case "C0":
                        message = $"*CMDR,NP,{imei},{DateTime.Now:yyMMddHHmmss},C0,0,0,{rfidCode}#<LF>";
                        break;
                    case "C1":
                        message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,{rfidCode}#<LF>";
                        break;
                    case "D1":
                        message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},D1,{AppSettings.TimeTrackingLocation}#<LF>";
                        break;
                    case "D1O":
                        message = $"*CMDS,NP,{imei},{DateTime.Now:yyMMddHHmmss},D1,0#<LF>";
                        break;
                    default:
                        goto case "L0";
                }

                var session = TNSessionMap.GetSession(Convert.ToInt64(imei));

                IPEndPoint endPoint = session.RemoteEndPoint as IPEndPoint;
                var result4 = TNSessionMap.SendMessage($"{endPoint?.Address}:{endPoint?.Port}:{session.Id}", imei, message, requestId, true,title1, title2, MongoUtilities.EnumMongoLogType.End);
                var result = TNSessionMap.SendMessage(session, imei, message, requestId, true, title1, title2, MongoUtilities.EnumMongoLogType.End);
                var result2 = TNSessionMap.SendMessageStr(session, imei, message, requestId, true, title1, title2, MongoUtilities.EnumMongoLogType.End);
                var result3 = TNSessionMap.SendMessageNew(session, imei, message, requestId, true, title1, title2, MongoUtilities.EnumMongoLogType.End);

                if (key == "S1")
                {
                    TNSessionMap.RemoveSessionImei(session);
                    TNSessionMap.RemoveSession(session);
                }

                var status = "";

                status = result ? $"Send message {message} successful !" : $"Send message {message} fail !";
                Utilities.WriteOperationLog("MinaOmni.Unlock", $"{requestId} | Message ARR | Imei: {imei} | Status: {status}");
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Message ARR : {message}");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Status  ARR : {status}");
                    Console.WriteLine("====================================================================================================");
                }

                status = result2 ? $"Send message {message} successful !" : $"Send message {message} fail !";
                Utilities.WriteOperationLog("MinaOmni.Unlock", $"{requestId} | Message STR | Imei: {imei} | Status: {status}");

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Message STR : {message}");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Status  STR : {status}");
                    Console.WriteLine("====================================================================================================");
                }

                status = result3 ? $"Send message {message} successful !" : $"Send message {message} fail !";
                Utilities.WriteOperationLog("MinaOmni.Unlock", $"{requestId} | Message ARR New | Imei: {imei} | Status: {status}");

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Message ARR New : {message}");
                    Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Status  ARR New : {status}");
                    Console.WriteLine("====================================================================================================");
                }

                status = result4 ? $"Send message {message} successful !" : $"Send message {message} fail !";
                Utilities.WriteOperationLog("MinaOmni.Unlock", $"{requestId} | Message ARR Ip | Imei: {imei} | Status: {status}");

                if (result)
                {
                    return result;
                }
                else if(result2)
                {
                    return result2;
                }
                else if(result3)
                {
                    return result3;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.Unlock", $"{requestId} | Imei: {imei} | Error: {e.Message}");

                if (AppSettings.TurnOnLogSendUnlock)
                {
                    new MongoUtilities().AddLog(0, $"Unlock (Error) | Mở khóa (lỗi)", $"{requestId}", imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | Error: {e.Message}", true);
                    
                }
                return false;
            }
        }

    }
}
