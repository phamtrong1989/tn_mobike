﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.GO.LockServices.Settings
{
    class AppSettings
    {
        // CONNECTION STRING
        public static string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];

        // SERVICES NAME
        public static string ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        public static string ServiceDisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"];
        public static string ServiceDescription = ConfigurationManager.AppSettings["ServiceDescription"];

        //<!--Thời gian giới hạn để mở khóa-->
        public static int LimitTimeOpen = Convert.ToInt32(ConfigurationManager.AppSettings["LimitTimeOpen"]);
        public static int TimeJobSchedulerSecond = Convert.ToInt32(ConfigurationManager.AppSettings["TimeJobSchedulerSecond"]);

        //<!--PORT nhận dữ liệu từ khóa trả về-->
        public static int PortService = Convert.ToInt32(ConfigurationManager.AppSettings["PortService"]);
        public static string HostService = ConfigurationManager.AppSettings["HostService"];

        // <!--Tổng dung lượng pin-->
        public static int TotalPin = Convert.ToInt32(ConfigurationManager.AppSettings["TotalPin"]);

        // <!--Thời gian bật định vị vị trí khóa ( tính theo giây )-->
        public static int TimeTrackingLocation = Convert.ToInt32(ConfigurationManager.AppSettings["TimeTrackingLocation"]);

        public static int TimeCheckConnection = Convert.ToInt32(ConfigurationManager.AppSettings["TimeCheckConnection"]);
        public static int TimeCheckLocation = Convert.ToInt32(ConfigurationManager.AppSettings["TimeCheckLocation"]);
        public static int TimeJobCheckConnection = Convert.ToInt32(ConfigurationManager.AppSettings["TimeJobCheckConnection"]);
        public static int TimeCheckL0 = Convert.ToInt32(ConfigurationManager.AppSettings["TimeCheckL0"]);

        public static int MeterCheck = Convert.ToInt32(ConfigurationManager.AppSettings["MeterCheck"]);

        // <!--Cài đặt xem chạy ở dạng services hay console-->
        public static bool ModeRun = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["ModeRun"]));

        //<!--CONNECTION STRING TO MONGODB-->
        public static string ConnectionMongoDb = ConfigurationManager.AppSettings["ConnectionMongoDb"];
        //!--Tên database MongoDB-->
        public static string DatabaseNameMongo = ConfigurationManager.AppSettings["DatabaseNameMongo"];
        //<!--Tên bảng lưu lịch sử GPS MongoDb-->
        public static string TableGPSNameMongo = ConfigurationManager.AppSettings["TableGPSNameMongo"];

        public static string ApiPushLock = ConfigurationManager.AppSettings["ApiPushLock"];
        public static string TokenApi = ConfigurationManager.AppSettings["TokenApi"];

        public static bool TurnOnQueryDb = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnQueryDb"]));
        public static bool TurnOnSocket = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnSocket"]));
        public static bool TurnOnCheckLocation = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnCheckLocation"]));

        public static bool TurnOnLogSQL = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogSQL"]));
        public static bool TurnOnLogSendUnlock = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogSendUnlock"]));
        public static bool TurnOnLogStartSV = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogStartSV"]));
        public static bool TurnOnLogSession = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogSession"]));
        public static bool TurnOnLogHandleCmd = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogHandleCmd"]));
        public static bool TurnOnLogCheckConnect = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogCheckConnect"]));

        public static bool TurnOnLogQ0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogQ0"]));
        public static bool TurnOnLogH0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogH0"]));
        public static bool TurnOnLogL1 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogL1"]));
        public static bool TurnOnLogL0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogL0"]));
        public static bool TurnOnLogD0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogD0"]));
        public static bool TurnOnLogD1 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogD1"]));
        public static bool TurnOnLogS5 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogS5"]));
        public static bool TurnOnLogS8 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogS8"]));
        public static bool TurnOnLogG0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogG0"]));
        public static bool TurnOnLogW0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogW0"]));
        public static bool TurnOnLogU0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogU0"]));
        public static bool TurnOnLogU1 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogU1"]));
        public static bool TurnOnLogU2 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogU2"]));
        public static bool TurnOnLogK0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogK0"]));
        public static bool TurnOnLogI0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogI0"]));
        public static bool TurnOnLogM0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogM0"]));
        public static bool TurnOnLogC1 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogC1"]));
        public static bool TurnOnLogC0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnLogC0"]));

        public static int TimeCheckBikeWarning = Convert.ToInt32(ConfigurationManager.AppSettings["TimeCheckBikeWarning"]);

        public static int OverStationDelta = Convert.ToInt32(ConfigurationManager.AppSettings["OverStationDelta"])*1000;

        public static double InStationDelta = Convert.ToDouble(ConfigurationManager.AppSettings["InStationDelta"]);

        public static string DefaultRFID = ConfigurationManager.AppSettings["DefaultRFID"];
        public static string ApiPushBikeWarning = ConfigurationManager.AppSettings["ApiPushBikeWarning"];

        public static bool ModeReceiver = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["ModeReceiver"]));
        public static bool ModeProcessD0 = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["ModeProcessD0"]));
        public static bool TurnOnCheckRfidApi = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnCheckRfidApi"]));
        public static bool TurnOnCheckWarning = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnCheckWarning"]));
        public static bool TurnOnAutoRestart = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnAutoRestart"]));

        public static int TotalTime2Warning = Convert.ToInt32(ConfigurationManager.AppSettings["TotalTime2Warning"]);
        public static int TimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
        public static int MetWarning = Convert.ToInt32(ConfigurationManager.AppSettings["MetWarning"]);
        public static int MetCheck = Convert.ToInt32(ConfigurationManager.AppSettings["MetCheck"]);
        public static int TimePushWarning = Convert.ToInt32(ConfigurationManager.AppSettings["TimePushWarning"]);
        public static int TimeAutoRestart = Convert.ToInt32(ConfigurationManager.AppSettings["TimeAutoRestart"]);

        public static string RMQAddress = ConfigurationManager.AppSettings["RMQAddress"];
        public static string RMQUser = ConfigurationManager.AppSettings["RMQUser"];
        public static string RMQPassword = ConfigurationManager.AppSettings["RMQPassword"];
        public static string RMQExchanges = ConfigurationManager.AppSettings["RMQExchanges"];
        public static int RMQPort = Convert.ToInt32(ConfigurationManager.AppSettings["RMQPort"]);

        public static string RMQQueue = ConfigurationManager.AppSettings["RMQQueue"];
    }
}
