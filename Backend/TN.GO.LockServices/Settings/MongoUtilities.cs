﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using TN.GO.LockServices.Entity;

namespace TN.GO.LockServices.Settings
{
    class MongoUtilities
    {
        public enum EnumMongoLogType
        {
            Start = 0,
            Center,
            End
        }

        public enum EnumSystemType
        {
            APIAPP = 0,
            APIBackend,
            DockService
        }

        public class MongoLog
        {
            // Ko nhập
            public ObjectId _id { get; set; }
            // log bắt đầu, log giữa, log kết thúc
            public EnumMongoLogType Type { get; set; }
            public string Content { get; set; }
            public int AccountId { get; set; }
            //Request Id vào đầu function sinh ra 1 string dạng
            string requestId = $"{DateTime.Now:yyyyMMddHHMMssfff}";
            public string LogKey { get; set; }
            public string DeviceKey { get; set; }
            // Function áp dụng
            public string FunctionName { get; set; }
            // format string yyyy-MM-dd HH:mm:ss.fff
            public string CreateTime { get; set; }
            // Loại hệ thống, Bắc => DockService
            public EnumSystemType SystemType { get; set; }
            //DateTimeNow.Ticks
            public long CreateTimeTicks { get; set; }

            public bool IsExeption { get; set; }

            public int Date { get; set; }
        }

        LogSettings _logSettings = new LogSettings("LOG");

        public void AddLog(int accountId, string functionName, string logKey, string deviceKey, EnumMongoLogType type, string content, bool isException = false)
        {
            try
            {
                Task.Run(() => AddLogRun(accountId, functionName, logKey, deviceKey, type, content, isException)).ConfigureAwait(false);
            }
            catch
            {
                //
            }
        }

        private async void AddLogRun(int accountId, string functionName, string logKey, string deviceKey, EnumMongoLogType type, string content, bool isException)
        {
            await MongoAddAsync(_logSettings, new MongoLog
            {
                AccountId = accountId,
                Content = content,
                CreateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"),
                CreateTimeTicks = DateTime.Now.Ticks,
                FunctionName = functionName,
                LogKey = logKey,
                DeviceKey = deviceKey,
                Type = type,
                SystemType = EnumSystemType.DockService,
                IsExeption = isException,
                Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}")
            });
        }

        private async Task MongoAddAsync(LogSettings settings, MongoLog data)
        {
            try
            {
                string dbName = settings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<MongoLog>(settings.MongoCollection);
                await collection.InsertOneAsync(data);
            }
            catch
            {
                //
            }
        }
    }
}
