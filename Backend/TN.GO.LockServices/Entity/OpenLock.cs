﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.GO.LockServices.Entity
{

    public enum ECommandType : byte
    {
        L0 = 0,
        D0 = 1,
        D1 = 2,
        S5 = 3,
        S8 = 4,
        G0 = 5,
        I0 = 6,
        M0 = 7,
        S0 = 8,
        S1 = 9,
        C0 = 10,
        C1 = 11,
        D1O = 12
    }

    public enum LockRequestStatus
    {
        Waiting = 0,
        Success = 1,
        Failure = 2,
        Retrying = 3
    }

    class OpenLock
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public int DockId { get; set; }
        public int BikeId { get; set; }
        public int AccountId { get; set; }
        public string Phone { get; set; }
        public string TransactionCode { get; set; }
        public string IMEI { get; set; }
        public string SerialNumber { get; set; }
        public int Retry { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime OpenTime { get; set; } = DateTime.Now;
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        public ECommandType CommandType { get; set; }
        public string RFID { get; set; }
        public OpenLock()
        {

        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    class OpenLockRequest : OpenLock
    {
        public OpenLockRequest()
        {

        }

        public OpenLockRequest(OpenLockHistory open)
        {
            StationId = open.StationId;
            DockId = open.DockId;
            BikeId = open.BikeId;
            AccountId = open.AccountId;
            Phone = open.Phone;
            TransactionCode = open.TransactionCode;
            IMEI = open.IMEI;
            SerialNumber = open.SerialNumber;
            Retry = open.Retry;
            Status = open.Status;
            CreatedDate = open.CreatedDate;
            OpenTime = open.OpenTime;
            InvestorId = open.InvestorId;
            ProjectId = open.ProjectId;
            CommandType = open.CommandType;
            RFID = open.RFID;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }


    }

    class OpenLockHistory : OpenLock
    {
        public OpenLockHistory()
        {

        }
        public OpenLockHistory(OpenLockRequest open)
        {
            StationId = open.StationId;
            DockId = open.DockId;
            BikeId = open.BikeId;
            AccountId = open.AccountId;
            Phone = open.Phone;
            TransactionCode = open.TransactionCode;
            IMEI = open.IMEI;
            SerialNumber = open.SerialNumber;
            Retry = open.Retry;
            Status = open.Status;
            CreatedDate = open.CreatedDate;
            OpenTime = open.OpenTime;
            InvestorId = open.InvestorId;
            ProjectId = open.ProjectId;
            CommandType = open.CommandType;
            RFID = open.RFID;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
