﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.GO.LockServices.Entity
{
    class DataD0Rabbit
    {
        public string Imei { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string RequestId { get; set; }
        
        public DataD0Rabbit()
        {
            
        }

        public DataD0Rabbit(string imei, double lat, double lng, string requestId)
        {
            Imei = imei;
            Lat = lat;
            Lng = lng;
            RequestId = requestId;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
