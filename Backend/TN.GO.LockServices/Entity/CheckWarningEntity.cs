﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using Newtonsoft.Json;

namespace TN.GO.LockServices.Entity
{
    class CheckWarningEntity
    {
        public string Imeil { get; set; }
        public List<CheckGps> DataGps { get; set; }
        public bool UnlockCheck { get; set; }
        public DateTime TimePush { get; set; }

        public CheckWarningEntity()
        {
            
        }

        public CheckWarningEntity(string imeil, double lat, double lng, DateTime time)
        {
            Imeil = imeil;

            DataGps = new List<CheckGps>(){new CheckGps(lat, lng, time)};
            UnlockCheck = false;
            TimePush = DateTime.MinValue;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    class CheckGps
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public DateTime Time { get; set; }

        public CheckGps()
        {
            
        }

        public CheckGps(double lat, double lng, DateTime time)
        {
            Lat = lat;
            Lng = lng;
            Time = time;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
