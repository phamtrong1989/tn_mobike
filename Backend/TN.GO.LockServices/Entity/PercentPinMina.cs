﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.GO.LockServices.Entity
{
    class PercentPinMina
    {
        public double Min { get; set; }
        public double Max { get; set; }
        public double Percent { get; set; }
        public double LastPercent { get; set; }
        public double Distance { get; set; }


        public PercentPinMina()
        {
            
        }

        public PercentPinMina(double min, double max, double percent, double lastPercent)
        {
            Min = min;
            Max = max;
            Percent = percent;
            LastPercent = lastPercent;
            Distance = max - min;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
