﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.GO.LockServices.Entity
{
    class DockInfo
    {
        public int Id { get; set; }
        public int DockId { get; set; }
        public string L0 { get; set; }
        public string D0 { get; set; }
        public string D1 { get; set; }
        public string S5 { get; set; }
        public string S8 { get; set; }
        public string G0 { get; set; }
        public string I0 { get; set; }
        public string M0 { get; set; }
        public string S0 { get; set; }
        public string S1 { get; set; }
        public string C0 { get; set; }
        public string C1 { get; set; }
        public string D1O { get; set; }
        public DateTime UpdatedDate { get; set; }
	}
}
