﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.GO.LockServices.Entity
{
    class Device
    {
        public string No { get; set; }
        public string IMEI { get; set; }
        public string MAC { get; set; }

        public Device()
        {

        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
