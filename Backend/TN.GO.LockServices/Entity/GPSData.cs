﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace TN.GO.LockServices.Entity
{
    class GPSData
    {
        public long Id { get; set; }
        public string IMEI { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public DateTime CreateTime { get; set; }
        public long CreateTimeTicks { get; set; }
        public int Date { get; set; }

        public GPSData()
        {

        }

        public GPSData(string imei, double lat, double l, DateTime time)
        {
            IMEI = imei;
            Lat = lat;
            Long = l;
            CreateTime = time;
            CreateTimeTicks = time.Ticks;
            Date = Convert.ToInt32($"{time:yyyyMMdd}");
        }

        public BsonDocument ToBsonDocument()
        {
            return new BsonDocument
            {
                {"IMEI", IMEI},
                {"Lat", Lat},
                {"Long", Long},
                {"CreateTime", $"{CreateTime:yyyy-MM-dd HH:mm:ss.ffffff}"},
                {"CreateTimeTicks", CreateTimeTicks},
                {"Date", Date}
            };
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
