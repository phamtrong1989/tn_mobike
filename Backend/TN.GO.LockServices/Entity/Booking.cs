﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace TN.GO.LockServices.Entity
{
    class Booking
    {
        public int Id { get; set; }
        public int StationIn { get; set; }
        public int StationOut { get; set; }
        public int BikeId { get; set; }
        public int DockId { get; set; }
        public int AccountId { get; set; }
        public int CustomerGroupId { get; set; }
        public string TransactionCode { get; set; }
        public byte Vote { get; set; }
        public string Feedback { get; set; }
        public int Status { get; set; }
        public decimal ChargeInAccount { get; set; }
        public decimal ChargeInSubAccount { get; set; }
        public int TicketPriceId { get; set; }
        public DateTime DateOfPayment { get; set; }
        public DateTime CreatedDate { get; set; }
        public int TotalMinutes { get; set; }
        public decimal TotalPrice { get; set; }
        public double KCal { get; set; }
        public double KG { get; set; }
        public double KM { get; set; }
        public DateTime EndTime { get; set; }
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartTime { get; set; }
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        public int TicketPrice_BlockPerMinute { get; set; }
        public int TicketPrice_BlockPerViolation { get; set; }
        public decimal TicketPrice_BlockViolationValue { get; set; }
        public bool TicketPrice_IsDefault { get; set; }
        public int TicketPrice_LimitMinutes { get; set; }
        public byte TicketPrice_TicketType { get; set; }
        public decimal TicketPrice_TicketValue { get; set; }
        public decimal EstimateChargeInAccount { get; set; }
        public decimal EstimateChargeInSubAccount { get; set; }
        public string TicketPrepaidCode { get; set; }
        public int TicketPrepaidId { get; set; }
        public string Note { get; set; }
        public decimal TripPoint { get; set; }
        public DateTime Prepaid_EndDate { get; set; }
        public int Prepaid_MinutesSpent { get; set; }
        public DateTime Prepaid_StartDate { get; set; }
        public DateTime Prepaid_EndTime { get; set; }
        public DateTime Prepaid_StartTime { get; set; }

        public Booking()
        {
            
        }

        public Booking(int id, int stationIn, int stationOut, int bikeId, int dockId, int accountId, int customerGroupId, string transactionCode, byte vote, string feedback, int status, decimal chargeInAccount, decimal chargeInSubAccount, int ticketPriceId, DateTime dateOfPayment, DateTime createdDate, int totalMinutes, decimal totalPrice, double kCal, double kg, double km, DateTime endTime, int investorId, int projectId, DateTime startTime, bool ticketPriceAllowChargeInSubAccount, int ticketPriceBlockPerMinute, int ticketPriceBlockPerViolation, decimal ticketPriceBlockViolationValue, bool ticketPriceIsDefault, int ticketPriceLimitMinutes, byte ticketPriceTicketType, decimal ticketPriceTicketValue, decimal estimateChargeInAccount, decimal estimateChargeInSubAccount, string ticketPrepaidCode, int ticketPrepaidId, string note, decimal tripPoint, DateTime prepaidEndDate, int prepaidMinutesSpent, DateTime prepaidStartDate, DateTime prepaidEndTime, DateTime prepaidStartTime)
        {
            Id = id;
            StationIn = stationIn;
            StationOut = stationOut;
            BikeId = bikeId;
            DockId = dockId;
            AccountId = accountId;
            CustomerGroupId = customerGroupId;
            TransactionCode = transactionCode;
            Vote = vote;
            Feedback = feedback;
            Status = status;
            ChargeInAccount = chargeInAccount;
            ChargeInSubAccount = chargeInSubAccount;
            TicketPriceId = ticketPriceId;
            DateOfPayment = dateOfPayment;
            CreatedDate = createdDate;
            TotalMinutes = totalMinutes;
            TotalPrice = totalPrice;
            KCal = kCal;
            KG = kg;
            KM = km;
            EndTime = endTime;
            InvestorId = investorId;
            ProjectId = projectId;
            StartTime = startTime;
            TicketPrice_AllowChargeInSubAccount = ticketPriceAllowChargeInSubAccount;
            TicketPrice_BlockPerMinute = ticketPriceBlockPerMinute;
            TicketPrice_BlockPerViolation = ticketPriceBlockPerViolation;
            TicketPrice_BlockViolationValue = ticketPriceBlockViolationValue;
            TicketPrice_IsDefault = ticketPriceIsDefault;
            TicketPrice_LimitMinutes = ticketPriceLimitMinutes;
            TicketPrice_TicketType = ticketPriceTicketType;
            TicketPrice_TicketValue = ticketPriceTicketValue;
            EstimateChargeInAccount = estimateChargeInAccount;
            EstimateChargeInSubAccount = estimateChargeInSubAccount;
            TicketPrepaidCode = ticketPrepaidCode;
            TicketPrepaidId = ticketPrepaidId;
            Note = note;
            TripPoint = tripPoint;
            Prepaid_EndDate = prepaidEndDate;
            Prepaid_MinutesSpent = prepaidMinutesSpent;
            Prepaid_StartDate = prepaidStartDate;
            Prepaid_EndTime = prepaidEndTime;
            Prepaid_StartTime = prepaidStartTime;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
