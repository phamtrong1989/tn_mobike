﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.GO.LockServices.Entity
{
    class Station
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int TotalDock { get; set; }
        public int CityId { get; set; }
        public int CreatedUser { get; set; }
        public int DistrictId { get; set; }
        public int Height { get; set; }
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        public int UpdatedUser { get; set; }
        public int Width { get; set; }
        public int Spaces { get; set; }
        public string DisplayName { get; set; }
        public int ManagerUserId { get; set; }
        public double ReturnDistance { get; set; }


        public Station()
        {

        }

        public Station(int id, string name, string address, double lat, double lng, string description, int status, DateTime createdDate, DateTime updatedDate, int totalDock, int cityId, int createdUser, int districtId, int height, int investorId, int projectId, int updatedUser, int width, int spaces, string displayName, int managerUserId, double returnDistance)
        {
            Id = id;
            Name = name;
            Address = address;
            Lat = lat;
            Lng = lng;
            Description = description;
            Status = status;
            CreatedDate = createdDate;
            UpdatedDate = updatedDate;
            TotalDock = totalDock;
            CityId = cityId;
            CreatedUser = createdUser;
            DistrictId = districtId;
            Height = height;
            InvestorId = investorId;
            ProjectId = projectId;
            UpdatedUser = updatedUser;
            Width = width;
            Spaces = spaces;
            DisplayName = displayName;
            ManagerUserId = managerUserId;
            ReturnDistance = returnDistance;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
