﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.GO.LockServices.Entity
{
    class Dock
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public string IMEI { get; set; }
        public string SerialNumber { get; set; }
        public int Status { get; set; }
        public bool LockStatus { get; set; }
        public double Battery { get; set; }
        public bool Charging { get; set; }
        public bool ConnectionStatus { get; set; }
        public DateTime LastConnectionTime { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string SIM { get; set; }
        public int CreatedUser { get; set; }
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        public int UpdatedUser { get; set; }
        public int ModelId { get; set; }
        public int ProducerId { get; set; }
        public int WarehouseId { get; set; }
        public string Note { get; set; }
        public string RFID { get; set; }
        public DateTime? LastGPSTime { get; set; }

        public Dock()
        {

        }

        public Dock(string imei, double battery, double lat, double l, bool lockStatus = true, string sim = "")
        {
            StationId = 1;
            IMEI = imei;
            SerialNumber = null;
            Status = 1;
            LockStatus = lockStatus;
            Battery = battery;
            Charging = false;
            ConnectionStatus = true;
            LastConnectionTime = DateTime.Now;
            Lat = lat;
            Long = l;
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            SIM = null;
            CreatedUser = 0;
            InvestorId = 1;
            ProjectId = 1;
            UpdatedUser = 1;
            ModelId = 1;
            ProducerId = 1;
            WarehouseId = 0;
            Note = "";
            RFID = "";
            LastGPSTime = DateTime.Now;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
