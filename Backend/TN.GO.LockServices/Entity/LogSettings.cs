﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TN.GO.LockServices.Settings;

namespace TN.GO.LockServices.Entity
{
    class LogSettings
    {
        public bool Is { get; set; }
        public string MongoClient { get; set; }
        public string MongoDataBase { get; set; }
        public string MongoCollection { get; set; }
        public bool IsUseMongo { get; set; }
        public string MongoDataBaseLog { get; set; }
        public string MongoCollectionLog { get; set; }
        public int Date { get; set; }

        public LogSettings()
        {
            Is = true;
            MongoClient = AppSettings.ConnectionMongoDb;
            MongoDataBase = "TN-Mobike-{TimeDB}";
            MongoCollection = "PIN";
            IsUseMongo = false;
            MongoDataBaseLog = "PIN-{TimeDB}";
            MongoCollectionLog = "Pin";
            Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}");
        }

        public LogSettings(string dbName)
        {
            Is = true;
            MongoClient = AppSettings.ConnectionMongoDb;
            MongoDataBase = "TN-Mobike-{TimeDB}";
            MongoCollection = dbName;
            IsUseMongo = false;
            MongoDataBaseLog = "LOG-{TimeDB}";
            MongoCollectionLog = "Log";
            Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}"); ;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
