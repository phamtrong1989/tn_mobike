﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace TN.GO.LockServices.Entity
{

    public enum EBikeWarningType
    {
        Undefined,
        DoXe,
        DiChuyenBatHopPhap,
        XeDiQuaXa,
        XeKhongTrongTram,
        XeLoiGPS,
        GPSDiChuyenBatHopPhap
    }

    public class BikeWarning
    {
        public int Id { get; set; }
        public string IMEI { get; set; }
        public string Transaction { get; set; }
        public EBikeWarningType WarningType { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public bool Status { get; set; }
        public string Note { get; set; }
        public DateTime UpdatedDate { get; set; }

        public BikeWarning()
        {
            
        }

        public BikeWarning(string imei, string transaction, EBikeWarningType warningType, double lat, double l, bool status, string note, DateTime updatedDate)
        {
            IMEI = imei;
            Transaction = transaction;
            WarningType = warningType;
            Lat = lat;
            Long = l;
            Status = status;
            Note = note;
            UpdatedDate = updatedDate;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class BikeWarningLog : BikeWarning
    {
        public BikeWarningLog()
        {
            
        }

        public BikeWarningLog(BikeWarning data)
        {
            Id = data.Id;
            IMEI = data.IMEI;
            Transaction = data.Transaction;
            WarningType = data.WarningType;
            Lat = data.Lat;
            Long = data.Long;
            Status = data.Status;
            Note = data.Note;
            UpdatedDate = data.UpdatedDate;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class BikeWarningSendCommand
    {
        public string IMEI { get; set; }
        public EBikeWarningType Type { get; set; }
        public DateTime Time { get; set; }

        public BikeWarningSendCommand()
        {
            
        }

        public BikeWarningSendCommand(string imei, EBikeWarningType type, DateTime time)
        {
            IMEI = imei;
            Type = type;
            Time = time;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class BikeWarningCount
    {
        public string IMEI { get; set; }
        public EBikeWarningType EBikeWarningType { get; set; }
        public int Count { get; set; } = 0;
        public DateTime LastTime { get; set; } = DateTime.Now;

        public BikeWarningCount()
        {
            
        }

        public BikeWarningCount(string imei, EBikeWarningType eBikeWarningType, int count, DateTime lastTime)
        {
            IMEI = imei;
            EBikeWarningType = eBikeWarningType;
            Count = count;
            LastTime = lastTime;
        }

        public BikeWarningCount(BikeWarning data)
        {
            IMEI = data.IMEI;
            EBikeWarningType = data.WarningType;
            Count = 1;
            LastTime = DateTime.Now;
        }


        public override string ToString()
        {
            return base.ToString();
        }
    }
}
