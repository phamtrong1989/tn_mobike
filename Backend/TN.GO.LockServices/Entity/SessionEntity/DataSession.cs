﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;
using Newtonsoft.Json;

namespace TN.GO.LockServices.Entity.SessionEntity
{
    class DataSession
    {
        public DateTime LastTime { get; set; }
        public string Key { get; set; }
        public string Imei { get; set; }
        public string IpAddress { get; set; }
        public IoSession Session { get; set; }
        public DateTime LastSendL0 { get; set; } = DateTime.MinValue;
        public DateTime LastReceiverL0 { get; set; } = DateTime.MinValue;
        public double TimeOut { get; set; } = 0;

        public DataSession()
        {
            
        }

        public DataSession(DateTime lastTime, string key, string imei, string ipAddress, IoSession session, DateTime lastSendL0, DateTime lastReceiverL0, double timeOut)
        {
            LastTime = lastTime;
            Key = key;
            Imei = imei;
            IpAddress = ipAddress;
            Session = session;
            LastSendL0 = lastSendL0;
            LastReceiverL0 = lastReceiverL0;
            TimeOut = timeOut;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
