﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Session;

namespace TN.GO.LockServices.Entity
{
    class Session
    {
        public string Imei { get; set; }
        public string IpAddress { get; set; }
        public IoSession ISession { get; set; }

        public Session(string imei, string ipAddress, IoSession session)
        {
            Imei = imei;
            IpAddress = ipAddress;
            ISession = session;
        }

        public Session()
        {
            
        }
    }
}
