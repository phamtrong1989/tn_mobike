﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.UI.Extensions;

namespace TN.UI.Controllers
{
    public class HomeController : TN.UI.Areas.Base.Controllers.BaseController
    {
        private readonly IStationRepository _iStationRepository;
        private readonly ILogger _logger;
        public HomeController(
            ILogger<HomeController> logger,
            IStationRepository iStationRepository
            )
        {
            controllerName = "Map";
            tableName = "Home";
            _logger = logger;
            _iStationRepository = iStationRepository;
        }
        [Route("Admin")]
        [Route("/")]
        [AuthorizePermission]
        public IActionResult Index()
        {
            _logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", "Bắt đầu trang index");
            return View();
        }
        [AuthorizePermission("Index")]
        public async Task<ResponseModel<List<Station>>> Stations()
        {
            try
            {
                var list = await _iStationRepository.FindByMap(m => (m.Status == Station.EStatus.Active), null);
                foreach(var item in list)
                {
                    item.Docks= item.Docks.Where(m=>m.Status>0).OrderBy(m => m.OrderNumber);
                }
                return new ResponseModel<List<Station>>
                {
                    Data = list.Select(m=> new Station {
                        Model = m.Model,
                        Address=m.Address,
                        IMEI=m.IMEI??"",
                        BookedBike= m.BookedBike,
                        ConnectionStatus=m.ConnectionStatus,
                        CreatedDate=m.CreatedDate,
                        CreatedSystemUserId=m.CreatedSystemUserId,
                        Docks=m.Docks,
                        LastConnectionTime=m.LastConnectionTime,
                        UpdateFlag = m.UpdateFlag,
                        ErrorDock=m.ErrorDock,
                        Id = m.Id,
                        Lat =m.Lat,
                        Lng=m.Lng,
                        Name = m.Name,
                        StationConfigId=m.StationConfigId,
                        Token =m.Token,
                        TotalBike=m.TotalBike,
                        Status=m.Status,
                        TotalDock=m.TotalDock,
                        UpdatedDate=m.UpdatedDate,
                        UpdatedSystemUserId=m.UpdatedSystemUserId,
                        SerialNumber=m.SerialNumber??""
                    }).OrderBy(m=>m.Name).ToList()
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
                return new ResponseModel<List<Station>>
                {
                    Status = false
                };
            }
        }
    }
}