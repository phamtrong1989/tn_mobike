﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Claims;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using TN.Domain.Model;
namespace TN.UI.Extensions
{
    public class RoleType
    {
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

    }
    public class AuthorizePermissionAttribute : TypeFilterAttribute
    {
        public AuthorizePermissionAttribute(string action = null, string controller = null, string area = null)
            : base(typeof(PermissionFilter))
        {
            Arguments = new object[] { new RoleType() {Action=action,Area=area,Controller=controller } };
        }
    }

    public class PermissionFilter : Attribute, IAsyncAuthorizationFilter
    {
        private readonly IUserRepository _IAspNetUsers;
        private readonly UserManager<ApplicationUser> _UserManager;
        private readonly SignInManager<ApplicationUser> _SignInManager;

        readonly RoleType _roleType;

        public PermissionFilter(IUserRepository iAspNetUsers, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleType roleType)
        {
            _IAspNetUsers = iAspNetUsers;
            _UserManager = userManager;
            _SignInManager = signInManager;
            _roleType = roleType;
        }
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            string controller, action, area = "Null", returnUrl;
            int userId;
            ApplicationUser user;
            List<RoleActionModel> listRoleUser;
            controller = _roleType.Controller ?? context.RouteData.Values["controller"].ToString();
            action = _roleType.Action ?? context.RouteData.Values["action"].ToString();
            if (context.RouteData.Values.TryGetValue("area", out object areaObj))
            {
                area = _roleType.Area ?? areaObj.ToString();
            }
            userId = Convert.ToInt32(context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            user = await _UserManager.FindByIdAsync(userId.ToString());
            // Không tồn tại
            if (user==null)
            {
                await _SignInManager.SignOutAsync();
                returnUrl = "/Login" + "?ReturnUrl=" + System.Text.Encodings.Web.UrlEncoder.Default.Encode(context.HttpContext.Request.Path + context.HttpContext.Request.QueryString);
                context.Result = new RedirectResult(returnUrl);
                return;
            }
            // Tài khoản bị khóa
            if(user.IsLock && !user.IsSuperAdmin)
            {
                await _SignInManager.SignOutAsync();
                returnUrl = "/Login" + "?ReturnUrl=" + System.Text.Encodings.Web.UrlEncoder.Default.Encode(context.HttpContext.Request.Path + context.HttpContext.Request.QueryString);
                context.Result = new RedirectResult(returnUrl);
                return;
            }
            // Yêu cầu đăng nhập lại
            if (user.IsReLogin)
            {
                await _SignInManager.RefreshSignInAsync(user);
                user.IsReLogin = false;
                await _UserManager.UpdateAsync(user);
            }
            if(!user.IsSuperAdmin)
            {
                //Check quyền
                listRoleUser = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RoleActionModel>>(context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "RoleActions")?.Value);
                if (listRoleUser.Any(m => m.ActionName == action && m.AreaName == area && m.ControllerName == controller))
                {
                    return;
                }
                context.Result = new ForbidResult();
            }
            return;
        }
    }
}
