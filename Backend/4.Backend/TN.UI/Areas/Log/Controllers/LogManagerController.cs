﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using TN.UI.Extensions;
using Microsoft.AspNetCore.Authorization;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Extensions;
using System.Security.Claims;
using static TN.UI.Extensions.DataUserInfo;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace TN.UI.Areas.Log.Controllers
{
    [Area("Log")]
    [AuthorizePermission]
    public class LogManagerController : Controller
    {
        private readonly RoleManager<ApplicationRole> _RoleManager;
        private readonly ILogger _Logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IBikeRepository _IBikeRepository;
        private readonly IStationConfigRepository _IStationConfigRepository;
        private readonly ILogRepository _ILogRepository;
        private readonly IRoleControllerRepository _IAspNetRoleControllerRepository;

        public LogManagerController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> RoleManager,
            ILogger<LogManagerController> logger,
            IBikeRepository IBikeRepository,
            IStationConfigRepository IStationConfigRepository,
            ILogRepository ILogRepository, IRoleControllerRepository IAspNetRoleControllerRepository
            )
        {
            _Logger = logger;
            _RoleManager = RoleManager;
            _userManager = userManager;
            _IBikeRepository = IBikeRepository;
            _IStationConfigRepository = IStationConfigRepository;
            _ILogRepository = ILogRepository;
            _IAspNetRoleControllerRepository = IAspNetRoleControllerRepository;
        }
        #region [Index]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewData["PageSelectListItem"] = new SelectList(await _IAspNetRoleControllerRepository.Search(m => m.IsShow == true), "Id", "Name");
            ViewData["TypeSelectListItem"] = new SelectList(
                new List<SelectListItem>() {
                    new SelectListItem {Text="Normal",Value=((int)LogType.Normal).ToString() },
                    new SelectListItem {Text="Warning",Value=((int)LogType.Warning).ToString() },
                    new SelectListItem {Text="Danger",Value=((int)LogType.Danger).ToString() }
                }
                , "Value", "Text");
            return View();
        }
        [HttpPost, ActionName("Index")]
        public async Task<IActionResult> IndexPost(int? page, int? limit, string key, int? type, string pageManager, string startTime, string endTime, string ordertype, string orderby)
        {
            ViewData["ListController"] = await _IAspNetRoleControllerRepository.Search(null);
            page = page < 0 ? 1 : page;
            limit = (limit > 100 || limit < 10) ? 10 : limit;
            DateTime? _startTime = Convert.ToDateTime(startTime).Date;
            DateTime? _endTime = Convert.ToDateTime(endTime).Date.AddDays(1);

            var data = await _ILogRepository.SearchPagedList(
                page ?? 1,
                limit ?? 10,
                m => (m.Action.Contains(key) || key == null || m.SystemUser.Contains(key))
                && (m.ObjectType == pageManager || pageManager == null)
                && ((int)m.Type == type || type == null)
                && (startTime == null || m.CreatedDate >= _startTime.Value)
                && (endTime == null || m.CreatedDate <= _endTime.Value),
                OrderByExtention(ordertype, orderby), m=> new Domain.Model.Log {Id=m.Id,Action = m.Action, Object=m.Object,ObjectType= m.ObjectType,Status=m.Status,Type=m.Type,Timestamp=m.Timestamp,SystemUser=m.SystemUser,ObjectId=m.ObjectId,SystemUserId=m.SystemUserId,CreatedDate=m.CreatedDate });
            // returnurl
            data.ReturnUrl = Url.Action("Index", new { page, limit, key, type, pageManager, startTime, endTime, ordertype, orderby });
            return View("IndexAjax", data);
        }
        private Func<IQueryable<TN.Domain.Model.Log>, IOrderedQueryable<TN.Domain.Model.Log>> OrderByExtention(string ordertype, string orderby)
        {
            Func<IQueryable<TN.Domain.Model.Log>, IOrderedQueryable<TN.Domain.Model.Log>> functionOrder = null;
            switch (orderby)
            {
                case "createdDate":
                    functionOrder = ordertype == "asc" ? EntityExtention<TN.Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<TN.Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                default:
                    functionOrder = ordertype == "asc" ? EntityExtention<TN.Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<TN.Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
            }
            return functionOrder;
        }
        #endregion
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var dl = await _ILogRepository.SearchOneAsync(m => m.Id == id);
                if (dl == null)
                {
                    return View("404");
                }
                else if (dl.ObjectType == "BikeManager")
                {
                    var model = new BaseLogModel<BikeModel>
                    {
                        DataBefore = dl.ValueBefore == null ? new BikeModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<BikeModel>(dl.ValueBefore),
                        DataAfter = dl.ValueAfter == null ? new BikeModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<BikeModel>(dl.ValueAfter),
                        Log = dl
                    };
                    return View("Bike", model);
                }
                else if (dl.ObjectType == "StationManager")
                {
                    var model = new BaseLogModel<StationModel>
                    {
                        DataBefore = dl.ValueBefore == null ? new StationModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<StationModel>(dl.ValueBefore),
                        DataAfter = dl.ValueAfter == null ? new StationModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<StationModel>(dl.ValueAfter),
                        Log = dl
                    };
                    return View("Station", model);
                }
                else if (dl.ObjectType == "StationConfigManager")
                {
                    var model = new BaseLogModel<StationConfigModel>
                    {
                        DataBefore = dl.ValueBefore == null ? new StationConfigModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<StationConfigModel>(dl.ValueBefore),
                        DataAfter = dl.ValueAfter == null ? new StationConfigModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<StationConfigModel>(dl.ValueAfter),
                        Log = dl
                    };
                    return View("StationConfig", model);
                }
                else if (dl.ObjectType == "DockManager")
                {
                    var model = new BaseLogModel<DockModel>
                    {
                        DataBefore = dl.ValueBefore == null ? new DockModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<DockModel>(dl.ValueBefore),
                        DataAfter = dl.ValueAfter == null ? new DockModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<DockModel>(dl.ValueAfter),
                        Log = dl
                    };
                    return View("Dock", model);
                }
                else if (dl.ObjectType == "UserManager")
                {
                    var model = new BaseLogModel<UserEditManagerModel>
                    {
                        DataBefore = dl.ValueBefore == null ? new UserEditManagerModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<UserEditManagerModel>(dl.ValueBefore),
                        DataAfter = dl.ValueAfter == null ? new UserEditManagerModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<UserEditManagerModel>(dl.ValueAfter),
                        Log = dl
                    };
                    return View("User", model);
                }
                else if (dl.ObjectType == "AccountManager")
                {
                    var model = new BaseLogModel<AccountModel>
                    {
                        DataBefore = dl.ValueBefore == null ? new AccountModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<AccountModel>(dl.ValueBefore),
                        DataAfter = dl.ValueAfter == null ? new AccountModel() : Newtonsoft.Json.JsonConvert.DeserializeObject<AccountModel>(dl.ValueAfter),
                        Log = dl
                    };
                    return View("Account", model);
                }
                else
                {
                    dl.ValueBefore = "\n" + JToken.Parse(dl.ValueBefore).ToString(Formatting.Indented);
                    dl.ValueAfter= "\n" + JToken.Parse(dl.ValueAfter).ToString(Formatting.Indented);
                    return View("Details", dl);
                }
            }
            catch (Exception)
            {
            }
            return View("404");
        }
    }
}