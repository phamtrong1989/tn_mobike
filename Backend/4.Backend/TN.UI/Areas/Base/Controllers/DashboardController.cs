﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.UI.Extensions;

namespace TN.UI.Areas.Base.Controllers
{
    [Area("Base")]
    //[Authorize]
    public class DashboardController : Controller
    {
        [Route("Admin/AccessDenied")]
        public IActionResult AccessDenied()
        {
            return View();
        }
        [HttpPost("Admin/ChangerMenuType/{i}")]
        public bool ChangeMenuType(int i)
        {
            CookieExtensions.Remove("MenuType");
            CookieExtensions.Set("MenuType", i.ToString(), 1440);
            return true;
        }
    }
}