﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using TN.UI.Extensions;
using Microsoft.AspNetCore.Authorization;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Extensions;
using System.Security.Claims;
using static TN.UI.Extensions.DataUserInfo;
using TN.Domain.Model.Common;
namespace TN.UI.Areas.Bike.Controllers
{
    [Area("Bike")]
    [AuthorizePermission]
    public class BikeManagerController : Base.Controllers.BaseController
    {
        private readonly RoleManager<ApplicationRole> _RoleManager;
        private readonly IRoleControllerRepository _IAspNetRoleController;
        private readonly IAspNetRoleDetailRepository _IAspNetRoleDetails;
        private readonly ILogger _Logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IBikeRepository _IBikeRepository;
        public BikeManagerController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> RoleManager,
            IRoleControllerRepository IAspNetRoleController,
            IAspNetRoleDetailRepository IAspNetRoleDetails,
            ILogger<BikeManagerController> logger,
            IBikeRepository IBikeRepository
            )
        {
            controllerName = "BikeManager";
            tableName = "Bike";
            _Logger = logger;
            _RoleManager = RoleManager;
            _IAspNetRoleController = IAspNetRoleController;
            _IAspNetRoleDetails = IAspNetRoleDetails;
            _userManager = userManager;
            _IBikeRepository = IBikeRepository;
        }

        #region [Index]
        [HttpGet]
        public IActionResult Index()
        {
            ViewData["StatusSelectListItem"] = new SelectList(
                new List<SelectListItem>() {
                    new SelectListItem {Text="Sử dụng",Value="1" },
                    new SelectListItem {Text="Không sử dụng",Value="0" },
                    new SelectListItem {Text="Lỗi",Value="2" }
                }
                , "Value", "Text");
            return View();
        }
        [HttpPost, ActionName("Index")]
        public async Task<IActionResult> IndexPost(int? page, int? limit, string key, int? type, int? status)
        {
            if (page <= 0)
            {
                page = 1;
            }
            if (limit >= 100)
            {
                page = 100;
            }
            var data = await _IBikeRepository.SearchPagedList(
                page ?? 1,
                limit ?? 10,
                m => m.Id >0 && (m.Etag.Contains(key) || key == null || m.IMEI.Contains(key) || m.SerialNumber.Contains(key))
                && (((int)m.Status == status || status == null) && ((int)m.Type == type || type == null)),
                x => x.OrderByDescending(m => m.CreatedDate));
            // returnurl
            data.ReturnUrl = Url.Action("Index", new { page, limit, key, type, status });
            return View("IndexAjax", data);
        }
        #endregion

        #region [Create]
        [HttpGet]
        public IActionResult Create() => View(new BikeModel
        {
            TypeSelectList = new SelectList(ListData.ListBikeType, "Id", "Name"),
        });
        [HttpPost, ActionName("Create")]
        public async Task<ResponseModel> CreatePost(BikeModel use)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var ktEtag = await _IBikeRepository.FindByEtag(use.Etag);
                    if (ktEtag != null)
                    {
                        return new ResponseModel() { Output = 2, Message = "Etag đã tồn tại, vui lòng kiểm tra lại", Type = ResponseTypeMessage.Warning };
                    }
                    else
                    {
                        var dlAdd = new Domain.Model.Bike
                        {
                            Etag = use.Etag,
                            IMEI = use.IMEI,
                            Model = use.Model,
                            SerialNumber = use.SerialNumber,
                            Type = use.Type,
                            Status = use.Status,
                            CreatedSystemUserId = UserId,
                            CreatedDate = DateTime.Now
                        };
                        await _IBikeRepository.AddAsync(dlAdd);
                        await _IBikeRepository.Commit();
                        await AddLog(new LogModel { Action = $"Thêm mới xe '{dlAdd.Etag}'", ObjectId = dlAdd.Id, Type = LogType.Normal, ValueAfter = dlAdd });
                        return new ResponseModel() { Output = 1, Message = "Thêm mới xe thành công", Type = ResponseTypeMessage.Success };
                    }
                }
                {
                    return new ResponseModel() { Output = 0, Message = "Bạn chưa nhập đủ thông tin hoặc thông tin đã tồn tại!", Type = ResponseTypeMessage.Warning };
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại!", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion

        #region [Edit]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var dl = await _IBikeRepository.SearchOneAsync(m => m.Id == id);
            if (dl == null)
            {
                return View("404");
            }
            var model = new BikeModel
            {
                TypeSelectList = new SelectList(ListData.ListBikeType, "Id", "Name"),
                Id = dl.Id,
                Etag = dl.Etag,
                Model = dl.Model,
                Type = dl.Type,
                IMEI = dl.IMEI,
                SerialNumber = dl.SerialNumber,
                Status = dl.Status,
            };
            return View(model);
        }
        [HttpPost, ActionName("Edit")]
        public async Task<ResponseModel> EditPost(BikeModel use, int id, string returnurl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dl = await _IBikeRepository.SearchOneAsync(m => m.Id == id);
                    if (dl == null)
                    {
                        return new ResponseModel() { Output = 0, Message = "Dữ liệu không tồn tại, vui lòng thử lại", Type = ResponseTypeMessage.Warning };
                    }
                    var dlLog = new LogModel
                    {
                        ObjectId = id,
                        Action = $"Cập nhật thông tin xe '{dl.Etag}'",
                        ValueBefore = dl,
                        Type = LogType.Warning
                    };
                    dl.Etag = use.Etag;
                    dl.Model = use.Model;
                    dl.Type = use.Type;
                    dl.IMEI = use.IMEI;
                    dl.SerialNumber = use.SerialNumber;
                    dl.Status = use.Status;
                    dl.UpdatedDate = DateTime.Now;
                    dl.UpdatedSystemUserId = UserId;
                    _IBikeRepository.Update(dl);
                    await _IBikeRepository.Commit();
                    dlLog.ValueAfter = dl;
                    await AddLog(dlLog);
                    return new ResponseModel() { Output = 1, Message = "Cập nhật thông tin xe thành công", Type = ResponseTypeMessage.Success };
                }
                return new ResponseModel() { Output = -2, Message = "Bạn chưa nhập đầy đủ thông tin", Type = ResponseTypeMessage.Warning };
            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion

        #region [Delete]
        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken]
        public async Task<ResponseModel> DeletePost(int id)
        {
            try
            {
                var dl = await _IBikeRepository.SearchOneAsync(m => m.Id == id);
                if (dl == null)
                {
                    return new ResponseModel() { Output = -2, Message = "Dữ liệu không tồn tại", Type = ResponseTypeMessage.Warning };
                }
                _IBikeRepository.Delete(dl);
                await _IBikeRepository.Commit();

                await AddLog(new LogModel
                {
                    ObjectId = id,
                    Action = $"Xóa xe '{dl.Etag}'",
                    ValueBefore = dl,
                    Type = LogType.Danger
                });
                return new ResponseModel() { Output = 1, Message = "Xóa xe thành công", Type = ResponseTypeMessage.Success };
            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
    }
}
