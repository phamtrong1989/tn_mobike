﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System.Linq;
using System.IO;
using System.Net.Http.Headers;
using System.Drawing;
using TN.UI.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using static TN.UI.Extensions.DataUserInfo;

namespace TN.UI.Areas.User.Controllers
{
    [Area("Bike")]
    [AuthorizePermission]
    public class AccountManagerController : Base.Controllers.BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IHostingEnvironment _iHostingEnvironment;
        private readonly ILogger _logger;
        private readonly LogSettings _logSettings;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IOptions<BaseSettings> _baseSettings;
        private readonly IUserRepository _aspNetUsers;
        private readonly ILogRepository _iLogRepository;
        private readonly IAccountRepository _iAccountRepository;

        public AccountManagerController(
            ILogger<UserManagerController> logger,
            IHostingEnvironment hostingEnvironment,
            IOptions<BaseSettings> baseSettings,
            UserManager<ApplicationUser> userManager,
            IUserRepository aspNetUsers,
            RoleManager<ApplicationRole> roleManager,
            IHostingEnvironment iHostingEnvironment,
            ILogRepository iLogRepository,
            IOptions<LogSettings> logSettings,
            IAccountRepository iAccountRepository
        )
        {
            controllerName = "AccountManager";
            tableName = "Account";
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _baseSettings = baseSettings;
            _userManager = userManager;
            _aspNetUsers = aspNetUsers;
            _roleManager = roleManager;
            _iHostingEnvironment = iHostingEnvironment;
            _iLogRepository = iLogRepository;
            _logSettings = logSettings.Value;
            _iAccountRepository = iAccountRepository;
        }
        #region [Index]
        public IActionResult Index()
        {
            ViewData["TypeSelectListItem"] = new SelectList(
                new List<SelectListItem>() {
                    new SelectListItem {Text="Thông thường",Value="1" },
                    new SelectListItem {Text="Vàng",Value="2" },
                    new SelectListItem {Text="Kim cương",Value="3" }
                }
                , "Value", "Text");
            return View();
        }
        [HttpPost, ActionName("Index")]
        public async Task<IActionResult> IndexPost(int? page, int? limit, string key, int? status, int? type)
        {
            page = page < 0 ? 1 : page;
            limit = (limit > 100 || limit < 10) ? 10 : limit;
            var data = await _iAccountRepository.SearchPagedList(
                page ?? 1,
                limit ?? 10,
                m => (m.Username.Contains(key) || key == null || m.LastName.Contains(key) || m.FirstName.Contains(key) || m.Phone.Contains(key)
                || m.Email.Contains(key) || m.Etag.Contains(key) || m.Phone.Contains(key))
                && ((int)m.Status == status || status == null)
                && ((int)m.Type == type || type == null),
                x => x.OrderByDescending(m => m.CreatedDate));
            return View("IndexAjax", data);
        }
        #endregion
        #region [Edit]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var dl = await _iAccountRepository.SearchOneAsync(m => m.Id == id);
            if (dl == null)
            {
                return View("404");
            }
            var model = new AccountModel
            {
                Id = dl.Id,
                Avatar = dl.Avatar,
                Username = dl.Username,
                LastName = dl.LastName,
                FirstName = dl.FirstName,
                Birthday = dl.Birthday,
                Sex = dl.Sex,
                Address = dl.Address,
                Etag = dl.Etag,
                Email = dl.Email,
                Phone = dl.Phone,
                Type = dl.Type,
                Status = dl.Status
            };
            return View(model);
        }
        [HttpPost, ActionName("Edit")]
        public async Task<ResponseModel> EditPost(AccountModel use, int id, string returnurl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dl = await _iAccountRepository.SearchOneAsync(m => m.Id == id);
                    if (dl == null)
                    {
                        return new ResponseModel() { Output = 0, Message = "Dữ liệu không tồn tại, vui lòng thử lại", Type = ResponseTypeMessage.Warning };
                    }
                    //Model log
                    var dlLog = new LogModel
                    {
                        ObjectId = id,
                        Action = $"Cập nhật tài khoản khách hàng '{dl.Username}'",
                        ValueBefore = dl,
                        Type = LogType.Warning
                    };
                    var ktEtag = await _iAccountRepository.SearchOneAsync(m => m.Etag == use.Etag);
                    if (ktEtag == null)
                    {
                        dl.Etag = use.Etag;
                        dl.UpdatedDate = DateTime.Now;
                        dl.UpdatedSystemUserId = UserId;
                        _iAccountRepository.Update(dl);
                        await _iAccountRepository.Commit();
                    }
                    else
                    {
                        return new ResponseModel() { Output = 0, Message = "Thẻ Etag này đã tồn tại, vui lòng thử lại", Type = ResponseTypeMessage.Warning };
                    }
                    
                    // Log
                    dlLog.ValueAfter = dl;
                    await AddLog(dlLog);
                    return new ResponseModel() { Output = 1, Message = "Cập nhật thông tin tài khoản thành công", Type = ResponseTypeMessage.Success };
                }
                return new ResponseModel() { Output = -2, Message = "Bạn chưa nhập đầy đủ thông tin", Type = ResponseTypeMessage.Warning };
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion

        #region [Delete]
        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken]
        public async Task<ResponseModel> DeletePost(int id)
        {
            try
            {
                var dl = await _iAccountRepository.SearchOneAsync(m => m.Id == id);
                if (dl == null)
                {
                    return new ResponseModel() { Output = -2, Message = "Dữ liệu không tồn tại", Type = ResponseTypeMessage.Warning };
                }
                _iAccountRepository.Delete(dl);
                await _iAccountRepository.Commit();

                await AddLog(new LogModel
                {
                    ObjectId = id,
                    Action = $"Xóa tài khoản '{dl.Etag}'",
                    ValueBefore = dl,
                    Type = LogType.Danger
                });
                return new ResponseModel() { Output = 1, Message = "Xóa tài khoản thành công", Type = ResponseTypeMessage.Success };
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
    }
}