﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System.Linq;
using static TN.UI.Extensions.DataUserInfo;
using Microsoft.AspNetCore.Authorization;
using TN.UI.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TN.UI.Areas.Bike.Controllers
{
    [Area("Bike")]
   // [AuthorizePermission]
    public class StationManager : Base.Controllers.BaseController
    {
        private readonly UserManager<ApplicationUser> _UserManager;
        private readonly RoleManager<ApplicationRole> _RoleManager;
        private readonly ILogger _Logger;
        private readonly IHostingEnvironment _HostingEnvironment;
        private readonly IOptions<BaseSettings> _BaseSettings;
        private readonly IUserRepository _AspNetUsers;
        private readonly IStationRepository _IStationRepository;
        private readonly IStationConfigRepository _IStationConfigRepository;
        private readonly IDockRepository _iDockRepository;
        public StationManager(
            ILogger<StationManager> logger,
            IHostingEnvironment hostingEnvironment,
            IOptions<BaseSettings> baseSettings,
            UserManager<ApplicationUser> userManager, IUserRepository aspNetUsers, RoleManager<ApplicationRole> roleManager,
            IStationRepository IStationRepository,
            IStationConfigRepository IStationConfigRepository,
            IDockRepository iDockRepository
            )
        {
            controllerName = "StationManager";
            tableName = "Station";
            _Logger = logger;
            _HostingEnvironment = hostingEnvironment;
            _BaseSettings = baseSettings;
            _UserManager = userManager;
            _AspNetUsers = aspNetUsers;
            _RoleManager = roleManager;
            _IStationRepository = IStationRepository;
            _IStationConfigRepository = IStationConfigRepository;
            _iDockRepository = iDockRepository;
        }

        #region [Index]
        public IActionResult Index()
        {
            ViewData["StatusSelectListItem"] = new SelectList(
                new List<SelectListItem>() {
                    new SelectListItem {Text="Sử dụng",Value="1" },
                    new SelectListItem {Text="Không sử dụng",Value="0" },
                    new SelectListItem {Text="Lỗi",Value="2" }
                }
                , "Value", "Text");
            return View();
        }
        [HttpPost, ActionName("Index")]
        public async Task<IActionResult> IndexPost(int? page, int? limit, string key, int? status, bool? cnn, bool? errorDock)
        {
            page = page < 0 ? 1 : page;
            limit = (limit > 100 || limit < 10) ? 10 : limit;
            var data = await _IStationRepository.SearchPagedList(
                page ?? 1,
                limit ?? 10,
                m => (m.Name.Contains(key) || m.Address.Contains(key) || m.IMEI.Contains(key) || m.SerialNumber.Contains(key) || m.Model.Contains(key) || key == null)
                && (((int)m.Status == status || status == null) && (m.ConnectionStatus == cnn || cnn == null) && (m.ErrorDock.Equals(0) == errorDock || errorDock == null)),
                x => x.OrderByDescending(m => m.CreatedDate));
            // returnurl
            data.ReturnUrl = Url.Action("Index", new { page, limit, key, status, cnn, errorDock });
            return View("IndexAjax", data);
        }
        #endregion

        #region [Create]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            ViewData["StationIdSelectListItem"] = new SelectList(await _IStationConfigRepository.Search(m => m.Id > 0), "Id", "Name");
            var dl = new StationModel();
            return View(dl);
        }
        [HttpPost, ActionName("Create")]
        public async Task<ResponseModel> CreatePost(StationModel use)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var ktIMEI = await _IStationRepository.FindByImei(use.IMEI);
                    if (ktIMEI != null)
                    {
                        return new ResponseModel() { Output = 2, Message = "IMEI này đã tồn tại, vui lòng kiểm tra lại", Type = ResponseTypeMessage.Warning };
                    }
                    else
                    {
                        var dlAdd = new Station
                        {
                            Name = use.Name,
                            Address = use.Address,
                            Model = use.Model,
                            IMEI = use.IMEI,
                            SerialNumber = use.SerialNumber,
                            Status = use.Status ?? Station.EStatus.Active,
                            CreatedDate = DateTime.Now,
                            CreatedSystemUserId = UserId,
                            Lng = Convert.ToDouble(use.Lng.Replace(".", ",")),
                            Lat = Convert.ToDouble(use.Lat.Replace(".", ",")),
                            StationConfigId = use.StationConfigId,
                            UpdatedDate = DateTime.Now,
                            UpdatedSystemUserId = UserId
                        };
                        await _IStationRepository.AddAsync(dlAdd);
                        await _IStationRepository.Commit();
                        await AddLog(new LogModel { Action = $"Thêm mới trạm '{dlAdd.Name}'", ObjectId = dlAdd.Id, Type = LogType.Normal, ValueAfter = dlAdd });
                        return new ResponseModel() { Output = 1, Message = "Thêm mới trạm thành công", Type = ResponseTypeMessage.Success };
                    }
                }
                {
                    return new ResponseModel() { Output = 0, Message = "Bạn chưa điền đủ thông tin hoặc trạm đã tồn tại!", Type = ResponseTypeMessage.Danger };
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại!", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion

        #region [Edit]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            ViewData["StationIdSelectListItem"] = new SelectList(await _IStationConfigRepository.Search(m => m.Id > 0), "Id", "Name");
            var dl = await _IStationRepository.SearchOneAsync(m => m.Id == id);
            if (dl == null)
            {
                return View("404");
            }

            var model = new StationModel
            {
                Id = dl.Id,
                Name = dl.Name,
                Address = dl.Address,
                Model = dl.Model,
                IMEI = dl.IMEI,
                SerialNumber = dl.SerialNumber,
                Status = dl.Status,
                Lat = dl.Lat.ToString().Replace(",","."),
                Lng = dl.Lng.ToString().Replace(",", "."),
                StationConfigId = dl.StationConfigId
            };
            return View(model);
        }
        [HttpPost, ActionName("Edit")]
        public async Task<ResponseModel> EditPost(StationModel use, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dl = await _IStationRepository.SearchOneAsync(m => m.Id == id);

                    if (dl == null)
                    {
                        return new ResponseModel() { Output = 0, Message = "Dữ liệu không tồn tại, vui lòng thử lại", Type = ResponseTypeMessage.Warning };
                    }
                    else
                    {
                        var dlLog = new LogModel
                        {
                            ObjectId = id,
                            Action = $"Cập nhật thông tin trạm '{dl.Name}'",
                            ValueBefore = dl,
                            Type = LogType.Warning
                        };
                        dl.Name = use.Name;
                        dl.Address = use.Address;
                        dl.Model = use.Model;
                        dl.IMEI = use.IMEI;
                        dl.SerialNumber = use.SerialNumber;
                        dl.Status = use.Status ?? Station.EStatus.Inactive;
                        dl.Lng = Convert.ToDouble(use.Lng.Replace(".", ","));
                        dl.Lat = Convert.ToDouble(use.Lat.Replace(".", ","));
                        dl.StationConfigId = use.StationConfigId;
                        dl.UpdatedDate = DateTime.Now;
                        dl.UpdatedSystemUserId = UserId;

                        _IStationRepository.Update(dl);
                        await _IStationRepository.Commit();

                        dlLog.ValueAfter = dl;
                        await AddLog(dlLog);

                        return new ResponseModel() { Output = 1, Message = "Cập nhật thông tin trạm xe thành công", Type = ResponseTypeMessage.Success };
                    }

                }
                return new ResponseModel() { Output = -2, Message = "Bạn chưa nhập đầy đủ thông tin", Type = ResponseTypeMessage.Warning };
            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion

        #region [Delete]
        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken]
        public async Task<ResponseModel> DeletePost(int id)
        {
            try
            {
                var dl = await _IStationRepository.SearchOneAsync(m => m.Id == id);
                if (dl == null)
                {
                    return new ResponseModel() { Output = -2, Message = "Dữ liệu không tồn tại!", Type = ResponseTypeMessage.Warning };
                }
                if (await _IStationRepository.CheckAvaiable(id) == true)
                {
                    return new ResponseModel() { IsUse = true, Output = 2, Message = "Dữ liệu đang được sử dụng (quản lý dock) không thể xóa!", Type = ResponseTypeMessage.Warning };
                }
                else
                {
                    _IStationRepository.Delete(dl);
                    await _IStationRepository.Commit();
                    await AddLog(new LogModel
                    {
                        ObjectId = id,
                        Action = $"Xóa trạm '{dl.Name}'",
                        ValueBefore = dl,
                        Type = LogType.Danger
                    });
                    return new ResponseModel() { Output = 1, Message = "Xóa trạm thành công!", Type = ResponseTypeMessage.Success };
                }

            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
    }
}