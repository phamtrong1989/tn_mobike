﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System.Linq;
using static TN.UI.Extensions.DataUserInfo;
using Microsoft.AspNetCore.Authorization;
using TN.UI.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TN.UI.Areas.Dock.Controllers
{
    [Area("Bike")]
    [AuthorizePermission]
    public class DockManager : Base.Controllers.BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IOptions<BaseSettings> _baseSettings;
        private readonly IStationRepository _iStationRepository;
        private readonly IDockRepository _iDockRepository;
        public DockManager(
            ILogger<DockManager> logger,
            IHostingEnvironment hostingEnvironment,
            IOptions<BaseSettings> baseSettings,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IStationRepository IStationRepository,
            IDockRepository IDockRepository
            )
        {
            controllerName = "DockManager";
            tableName = "Dock";
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _baseSettings = baseSettings;
            _userManager = userManager;
            _roleManager = roleManager;
            _iStationRepository = IStationRepository;
            _iDockRepository = IDockRepository;
        }
        #region [Index]
        public async Task<IActionResult> Index()
        {
            ViewData["StatusSelectListItem"] = new SelectList(
                new List<SelectListItem>() {
                    new SelectListItem {Text="Sử dụng",Value="1" },
                    new SelectListItem {Text="Không sử dụng",Value="0" },
                    new SelectListItem {Text="Lỗi",Value="2" }
                }
                , "Value", "Text");
            ViewData["StationSelectListItem"] = new SelectList(await _iStationRepository.Search(m => m.Id > 0), "Id", "Name");
            return View();
        }
        [HttpPost, ActionName("Index")]
        public async Task<IActionResult> IndexPost(int? page, int? limit, string key, int? status, bool? lockStatus, int? stationId,bool? bikeAlarm)
        {
            page = page < 0 ? 1 : page;
            limit = (limit > 100 || limit < 10) ? 10 : limit;
            var data = await _iDockRepository.SearchPagedList(
                page ?? 1,
                limit ?? 10,
                m => 
                (m.Model.Contains(key) || m.IMEI.Contains(key) || m.SerialNumber.Contains(key) || key == null)
                && (((int)m.Status == status || status == null)
                && (m.StationId == stationId || stationId == null)
                && (m.BikeAlarm == bikeAlarm || bikeAlarm == null)
                && (m.LockStatus == lockStatus || lockStatus == null)),
                x => x.OrderByDescending(m => m.StationId),null,m=>m.Station);
   
            data.ReturnUrl = Url.Action("Index", new { page, limit, key, status, lockStatus, stationId , bikeAlarm });
            return View("IndexAjax", data);
        }
        #endregion
        #region [Create]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var dl = new DockModel
            {
                StationSelectList = new SelectList(await _iStationRepository.Search(m => m.Id > 0), "Id", "Name")
            };
            return View(dl);
        }
        [HttpPost, ActionName("Create")]
        public async Task<ResponseModel> CreatePost(DockModel use)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dlAdd = new Domain.Model.Dock
                    {
                        IMEI = use.IMEI,
                        Model = use.Model,
                        SerialNumber = use.SerialNumber,
                        OrderNumber = use.OrderNumber,
                        Status = use.Status,
                        StationId = use.StationId,
                        CreatedDate = DateTime.Now,
                        CreatedSystemUserId = UserId
                    };
                    await _iDockRepository.AddAsync(dlAdd);
                    await _iDockRepository.Commit();

                    var list = await _iDockRepository.Search(m => m.StationId == dlAdd.StationId);
                    int count = list.Count();
                    var totalDock = await _iStationRepository.SearchOneAsync(m => m.Id == dlAdd.StationId);
                    totalDock.TotalDock = count;
                     _iStationRepository.Update(totalDock);
                    await _iStationRepository.Commit();

                    await AddLog(new LogModel { Action = $"Thêm mới dock '{dlAdd.IMEI}'", ObjectId = dlAdd.Id, Type = LogType.Normal, ValueAfter = dlAdd });
                    return new ResponseModel() { Output = 1, Message = "Thêm mới dock thành công", Type = ResponseTypeMessage.Success };
                }
                {
                    return new ResponseModel() { Output = 1, Message = "Bạn chưa điền đủ thông tin", Type = ResponseTypeMessage.Warning };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
        #region [Edit]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            ViewData["StationSelectListItem"] = new SelectList(await _iStationRepository.Search(m => m.Id > 0), "Id", "Name");
            var dl = await _iDockRepository.SearchOneAsync(m => m.Id == id);
            if (dl == null)
            {
                return View("404");
            }

            var model = new DockModel
            {
                Id = dl.Id,
                IMEI = dl.IMEI,
                Model = dl.Model,
                SerialNumber = dl.SerialNumber,
                OrderNumber = dl.OrderNumber,
                Status = dl.Status,
                StationId = dl.StationId,
                CurrentBikeId = dl.CurrentBikeId,
                StationSelectList = new SelectList(await _iStationRepository.Search(m => m.Id > 0), "Id", "Name")
            };
            return View(model);
        }
        [HttpPost, ActionName("Edit")]
        public async Task<ResponseModel> EditPost(DockModel use, int id, string returnurl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dl = await _iDockRepository.SearchOneAsync(m => m.Id == id);
                    if (dl == null)
                    {
                        return new ResponseModel() { Output = 0, Message = "Dữ liệu không tồn tại, vui lòng thử lại", Type = ResponseTypeMessage.Warning };
                    }
                    var dlLog = new LogModel
                    {
                        ObjectId = id,
                        Action = $"Cập nhật thông tin dock '{dl.IMEI}'",
                        ValueBefore = dl,
                        Type = LogType.Warning
                    };

                    var list1 = await _iDockRepository.Search(m => m.StationId == dl.StationId);
                    int count1 = list1.Count();
                    var oldDock = await _iStationRepository.SearchOneAsync(m => m.Id == dl.StationId);
                    oldDock.TotalDock = count1 - 1;
                    _iStationRepository.Update(oldDock);

                    await _iStationRepository.Commit();

                    dl.IMEI = use.IMEI;
                    dl.Model = use.Model;
                    dl.SerialNumber = use.SerialNumber;
                    dl.OrderNumber = use.OrderNumber;
                    dl.Status = use.Status;
                    dl.StationId = use.StationId;
                    dl.UpdatedDate = DateTime.Now;
                    dl.UpdatedSystemUserId = UserId;
                    _iDockRepository.Update(dl);
                    await _iDockRepository.Commit();

                    var list2 = await _iDockRepository.Search(m => m.StationId == dl.StationId);
                    int count2 = list2.Count();
                    var newDock = await _iStationRepository.SearchOneAsync(m => m.Id == dl.StationId);
                    newDock.TotalDock = count2;
                    _iStationRepository.Update(newDock);
                    await _iStationRepository.Commit();

                    dlLog.ValueAfter = dl;
                    await AddLog(dlLog);
                    return new ResponseModel() { Output = 1, Message = "Cập nhật thông tin dock thành công", Type = ResponseTypeMessage.Success };
                }
                return new ResponseModel() { Output = -2, Message = "Bạn chưa nhập đầy đủ thông tin", Type = ResponseTypeMessage.Warning };
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
        #region [Delete]
        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken]
        public async Task<ResponseModel> DeletePost(int id)
        {
            try
            {
                var dl = await _iDockRepository.SearchOneAsync(m => m.Id == id);
                if (dl == null)
                {
                    return new ResponseModel() { Output = -2, Message = "Dữ liệu không tồn tại", Type = ResponseTypeMessage.Warning };
                }
                else
                {
                    _iDockRepository.Delete(dl);
                    await _iDockRepository.Commit();

                    var list = await _iDockRepository.Search(m => m.StationId == dl.StationId);
                    int count = list.Count();
                    var totalDock = await _iStationRepository.SearchOneAsync(m => m.Id == dl.StationId);
                    totalDock.TotalDock = count;
                    _iStationRepository.Update(totalDock);
                    await _iStationRepository.Commit();

                    await AddLog(new LogModel
                    {
                        ObjectId = id,
                        Action = $"Xóa dock '{dl.IMEI}'",
                        ValueBefore = dl,
                        Type = LogType.Danger
                    });
                    return new ResponseModel() { Output = 1, Message = "Xóa dock thành công", Type = ResponseTypeMessage.Success };
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
    }
}