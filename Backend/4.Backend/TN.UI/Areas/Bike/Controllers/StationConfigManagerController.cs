﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using TN.UI.Extensions;
using Microsoft.AspNetCore.Authorization;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Extensions;
using System.Security.Claims;
using static TN.UI.Extensions.DataUserInfo;
namespace TN.UI.Areas.Bike.Controllers
{
    [Area("Bike")]
    [AuthorizePermission]
    public class StationConfigManagerController : Base.Controllers.BaseController
    {
        private readonly RoleManager<ApplicationRole> _RoleManager;
        private readonly ILogger _Logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IBikeRepository _IBikeRepository;
        private readonly IStationConfigRepository _IStationConfigRepository;
        public StationConfigManagerController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> RoleManager,
            ILogger<BikeManagerController> logger,
            IBikeRepository IBikeRepository,
            IStationConfigRepository IStationConfigRepository
            )
        {
            controllerName = "StationConfigManager";
            tableName = "StationConfig";
            _Logger = logger;
            _RoleManager = RoleManager;
            _userManager = userManager;
            _IBikeRepository = IBikeRepository;
            _IStationConfigRepository = IStationConfigRepository;
        }
        #region [Index]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost, ActionName("Index")]
        public async Task<IActionResult> IndexPost(int? page, int? limit, string key, int? type, int? status)
        {
            if (page <= 0)
            {
                page = 1;
            }
            if (limit >= 100)
            {
                page = 100;
            }
            var data = await _IStationConfigRepository.SearchPagedList(
                page ?? 1,
                limit ?? 10,
                m => (m.ServerIP.Contains(key) || m.Name.Contains(key) || m.ServerPort.ToString().Contains(key) || key == null)
                && ((int)m.Status == status || status == null),
                x => x.OrderByDescending(m => m.CreatedDate));
            // returnurl
            data.ReturnUrl = Url.Action("Index", new { page, limit, key, type, status });
            return View("IndexAjax", data);
        }
        #endregion
        #region [Create]
        [HttpGet]
        public IActionResult Create()
        {
            var dl = new StationConfigModel();
            return View(dl);
        }
        [HttpPost, ActionName("Create")]
        public async Task<ResponseModel> CreatePost(StationConfigModel use)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dlAdd = new StationConfig
                    {
                        Name = use.Name,
                        ServerIP = use.ServerIP,
                        ServerPort = use.ServerPort,
                        IsDefault = use.IsDefault,
                        Note = use.Note,
                        Status = use.Status,
                        CreatedSystemUserId = UserId,
                        CreatedDate = DateTime.Now
                    };
                    await _IStationConfigRepository.AddAsync(dlAdd);
                    await _IStationConfigRepository.Commit();

                    if(dlAdd.IsDefault)
                    {
                        var listDefault = await _IStationConfigRepository.Search(m => m.IsDefault == true && m.Id!=dlAdd.Id);
                        listDefault.ForEach(m => m.IsDefault = false);
                        await _IStationConfigRepository.Commit();
                    }

                    await AddLog(new LogModel { Action = $"Thêm mới cấu hình '{dlAdd.Name}'", ObjectId = dlAdd.Id, Type = LogType.Normal, ValueAfter = dlAdd });
                    return new ResponseModel() { Output = 1, Message = "Thêm mới cấu hình thành công", Type = ResponseTypeMessage.Success };
                }
                return new ResponseModel() { Output = 0, Message = "Bạn chưa nhập đầy đủ thông tin", Type = ResponseTypeMessage.Warning };
            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
        #region [Edit]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var dl = await _IStationConfigRepository.SearchOneAsync(m => m.Id == id);
            if (dl == null)
            {
                return View("404");
            }
            var model = new StationConfigModel
            {
                Id = dl.Id,
                Name = dl.Name,
                ServerIP = dl.ServerIP,
                ServerPort = dl.ServerPort,
                Note = dl.Note,
                IsDefault = dl.IsDefault,
                Status = dl.Status
            };
            return View(model);
        }
        [HttpPost, ActionName("Edit")]
        public async Task<ResponseModel> EditPost(StationConfigModel use, int id, string returnurl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dl = await _IStationConfigRepository.SearchOneAsync(m => m.Id == id);
                    if (dl == null)
                    {
                        return new ResponseModel() { Output = 0, Message = "Dữ liệu không tồn tại, vui lòng thử lại", Type = ResponseTypeMessage.Warning };
                    }
                    var dlLog = new LogModel
                    {
                        ObjectId = id,
                        Action = $"Cập nhật thông tin cấu hình '{dl.Name}'",
                        ValueBefore = dl,
                        Type = LogType.Warning
                    };
                    dl.Name = use.Name;
                    dl.ServerPort = use.ServerPort;
                    dl.ServerIP = use.ServerIP;
                    dl.Note = use.Note;
                    dl.IsDefault = use.IsDefault;
                    dl.Status = use.Status;
                    dl.UpdatedDate = DateTime.Now;
                    dl.UpdatedSystemUserId = UserId;

                    if (dl.IsDefault)
                    {
                        var listDefault = await _IStationConfigRepository.Search(m => m.IsDefault == true && m.Id != dl.Id);
                        listDefault.ForEach(m => m.IsDefault = false);
                        await _IStationConfigRepository.Commit();
                    }

                    _IStationConfigRepository.Update(dl);
                    await _IStationConfigRepository.Commit();
                    dlLog.ValueAfter = dl;
                    await AddLog(dlLog);
                    return new ResponseModel() { Output = 1, Message = "Cập nhật thông tin cấu hình thành công", Type = ResponseTypeMessage.Success };
                }
                return new ResponseModel() { Output = -2, Message = "Bạn chưa nhập đầy đủ thông tin", Type = ResponseTypeMessage.Warning };
            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
        #region [Delete]
        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken]
        public async Task<ResponseModel> DeletePost(int id)
        {
            try
            {
                var dl = await _IStationConfigRepository.SearchOneAsync(m => m.Id == id);
                if (dl == null)
                {
                    return new ResponseModel() { Output = -2, Message = "Dữ liệu không tồn tại", Type = ResponseTypeMessage.Warning };
                }
                if (await _IStationConfigRepository.CheckAvaiable(id) == true)
                {
                    return new ResponseModel() { IsUse = true, Output = 2, Message = "Dữ liệu đang được sử dụng không được xóa!", Type = ResponseTypeMessage.Warning };
                }
                else
                {
                    _IStationConfigRepository.Delete(dl);
                    await _IStationConfigRepository.Commit();
                    await AddLog(new LogModel
                    {
                        ObjectId = id,
                        Action = $"Xóa cấu hình '{dl.Name}'",
                        ValueBefore = dl,
                        Type = LogType.Danger
                    });
                    return new ResponseModel() { Output = 1, Message = "Xóa cấu hình thành công", Type = ResponseTypeMessage.Success };
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(LoggingEvents.GENERATE_ITEMS, "#Trong-[Log]{0}", ex);
            }
            return new ResponseModel() { Output = -1, Message = "Đã xảy ra lỗi, vui lòng F5 trình duyệt và thử lại", Type = ResponseTypeMessage.Danger, Status = false };
        }
        #endregion
    }
}
