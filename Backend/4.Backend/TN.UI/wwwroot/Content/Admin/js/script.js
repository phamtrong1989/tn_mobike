﻿//var firsLatLng = new google.maps.LatLng(10.823418, 106.628370);
var defaultLatLng = { lat: 21.0277644, lng: 105.83415979999995 };
var thisController = GetControllerByUrl(window.location.pathname);
(function ($) {

    $.queryString = (function (paramsArray) {
        let params = {};
        for (let i = 0; i < paramsArray.length; ++i) {
            let param = paramsArray[i]
                .split('=', 2);

            if (param.length !== 2)
                continue;

            params[param[0]] = decodeURIComponent(param[1].replace(/\+/g, " "));
        }
        return params;
    })(window.location.search.substr(1).split('&'));
    alertify.set('notifier', 'position', 'top-right');
    initMenuAdmin();
    initScrollBody();
})(jQuery);
function initMenuAdmin() {
    $("#menuleftadmin li").each(function (i, item) {
        if (thisController != "") {
            var thisa = $(item).find(">a");
            if (thisController == GetControllerByUrl(thisa.attr("href"))) {
                if ($(item).hasClass("cap2")) {
                    $(item).addClass("active");
                    thisa.closest("li>ul").closest("li").addClass("active");
                }
                else {
                    $(item).addClass("active");
                }
                return;
            }
        }
        else {
            $(".isDasb").addClass("active");
            return;
        }
    });
}
function GetControllerByUrl(url) {
    if (url == null) {
        return "";
    }
    var arrpa = url.split('/');
    if (arrpa.length >= 4) {
        return arrpa[3];
    }
    return "";
}
function fixButton() {
    //alert($("#canbtn").outerWidth());
    $("#divbutton").width($("#canbtn").outerWidth());
}
function bindLoading(tag) {
    var $loading = $(tag).waitMe({
        effect: "ios",
        text: 'Đang tải...',
        bg: 'rgba(255,255,255,0.0)',
        color: '#555'
    });
    return $loading;
}
function bindNumRow(tag, page, totalrows, limit) {
    var totalPage = (totalrows % limit) > 0 ? (Math.floor((totalrows / limit)) + 1) : Math.floor(totalrows / limit);
    $(tag).html("Bản ghi từ " + (page == 1 ? 1 : ((page - 1) * limit)).toLocaleString('vi-VN') + " - " + (totalPage == page ? totalrows : (page * limit)).toLocaleString('vi-VN') + " trong tổng " + totalrows.toLocaleString('vi-VN') + " bản ghi (trang " + page.toLocaleString('vi-VN') + "/" + totalPage.toLocaleString('vi-VN') + ")");
}
function bindPagination(tag, funtion, page, totalrows, limit) {
    var phandem = 2;
    var isBack = false, isNext = false;
    var totalPage = (totalrows % limit) > 0 ? (Math.floor((totalrows / limit)) + 1) : Math.floor(totalrows / limit);
    if (page > 1 && page <= totalPage) {
        isBack = true;
    }
    if (page > 1) {
        isBack = true;
    }
    if (page < totalPage) {
        isNext = true;
    }
    var str1 =
        `
                                     <ul class="pagination">
                                        <li class="`+ ((totalPage >= 1 && page != 1) ? "" : "disabled") + `">
                                              <a title='Trang đầu tiên' `+ ((totalPage >= 1 && page != 1) ? "onclick='" + funtion + "(1)'" : "") + ` href="javascript:void(0);">
                                                 <i class="material-icons">first_page</i>
                                              </a>
                                         </li>
                                         <li class="`+ (isBack == true ? "" : "disabled") + `">
                                              <a title='Quay lại trang' class='fixpage'`+ (isBack == false ? "" : "onclick='" + funtion + "(" + (page - 1) + ")'") + ` href="javascript:void(0);">
                                                 <i class="material-icons">chevron_left</i>
                                              </a>
                                         </li>
                                `;
    var str2 = `
                                         <li class="`+ (isNext == true ? "" : "disabled") + `">
                                              <a  title='Trang tiếp theo' class='fixpage' `+ (isNext == false ? "" : "onclick='" + funtion + "(" + (page + 1) + ")'") + ` href="javascript:void(0);" class="waves-effect">
                                                 <i class="material-icons">chevron_right</i>
                                              </a>
                                         </li>
                                        <li class="`+ ((totalPage > page) ? "" : "disabled") + `">
                                              <a title='Trang cuối' `+ ((totalPage > page) ? "onclick='" + funtion + "(" + (page + 1) + ")'" : "") + ` href="javascript:void(0);" class="waves-effect">
                                                 <i class="material-icons">last_page</i>
                                              </a>
                                         </li>
                                     </ul>
                                `;
    var str3 = ``;
    if (totalPage >= 15) {
        for (i = 1; i <= totalPage; i++) {
            //if () {
            //    str3 += `<li class="` + (page == i ? "active" : "") + `"><a  onclick='` + funtion + `(` + i + `)' href="javascript:void(0);">` + i + `</a></li>`;
            //}
            if ((i <= phandem) || (((i - phandem)) <= page && ((i + phandem + 1)) > page) || (i >= (totalPage - phandem))) {
                str3 += `<li class="` + (page == i ? "active" : "") + `"><a  onclick='` + funtion + `(` + i + `)' href="javascript:void(0);">` + i + `</a></li>`;
            }
        }
    }
    else {
        for (i = 1; i <= totalPage; i++) {
            str3 += `<li class="` + (page == i ? "active" : "") + `"><a  onclick='` + funtion + `(` + i + `)' href="javascript:void(0);">` + i + `</a></li>`;
        }
    }
    $(tag).html(str1 + str3 + str2);
}
function jsonToQueryString(json) {
    return '?' +
        Object.keys(json).map(function (key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}
function changeUrl(data) {
    window.history.pushState('page2', 'Title', jsonToQueryString(data));
}

function onCheckAll(e, tag) {
    $(tag).find("input[type='checkbox']").prop("checked", $(e).prop("checked"))
}
function initModal(callBack) {
    $(".btnPopup").on("click", function (e) {
        e.preventDefault();
        var xxModal = "#myModal";
        if ($(this).attr('data-namemodal') != undefined || $(this).attr('data-namemodal') != null) {
            xxModal = $(this).attr('data-namemodal');
        }
        $(xxModal).modal('show');
        callBack($(this));
    });
}
function initPopup() {
    $(".btnPopup").on("click", function (e) {

        e.preventDefault();
        var xxModal = "#myModal";
        if ($(this).attr('data-namemodal') != undefined || $(this).attr('data-namemodal') != null) {
            xxModal = $(this).attr('data-namemodal');
        }
        $(xxModal).modal({ backdrop: 'static', keyboard: false });
        $(xxModal).modal('show');
        $.get($(this).attr("href"), function (data) {
            $(xxModal + ">div>div.modal-content").html(data);
            var $form = $(xxModal + " .modal-content .ajaxValidateForm");
            // Unbind existing validation
            $form.unbind();
            $form.data("validator", null);
            // Check document for changes
            $.validator.unobtrusive.parse(document);
            // Re add validation with changes
            $form.validate($form.data("unobtrusiveValidation").options);
            $.AdminBSB.browser.activate();
            $.AdminBSB.dropdownMenu.activate();
            $.AdminBSB.input.activate();
            $.AdminBSB.select.activate();
            //$.AdminBSB.search.activate();
            //open dialog
        });
    });
}
$('.modal').on('hidden.bs.modal', function (e) {
    $(this).removeData();
});
function imgAvatarError(image) {
    image.onerror = "";
    image.src = "/Content/Admin/images/no-avatar.jpg";
    return true;
}
function initSearchExtension() {
    //$(".searchExtensionForm").width($(".keywidth").outerWidth()-42);
    $(".searchExtension").on("click", function () {
        if ($(".searchExtensionForm").hasClass("open")) {
            $(".searchExtensionForm").removeClass("open");
            $(this).html("<i class='material-icons'>keyboard_arrow_down</i>");
        }
        else {
            $(".searchExtensionForm").addClass("open");
            $(this).html("<i class='material-icons'>keyboard_arrow_up</i>");
        }
    });
}
function getQueryString(isFirst, qr, input) {
    return (isFirst && qr != null) ? qr : input
}
function closeSearchExtension() {
    if ($(".searchExtensionForm").hasClass("open")) {
        $(".searchExtensionForm").removeClass("open");
        $(".searchExtension").html("<i class='material-icons'>keyboard_arrow_down</i>");
    }
}
function logout() {
    $("#formLogout").submit();
}

function initScrollBody() {
    var viewportHeight = $(window).height();
    $('#body').slimScroll({
        height: viewportHeight + 'px',
        color: '#455A64',
        distance: '0',
        allowPageScroll: true,
        alwaysVisible: true
    });
}

function initAjaxSubmit(callBack, ajaxSubmit = ".ajaxSubmit", ajaxValidateForm = ".ajaxValidateForm") {
    closeSearchExtension();
    $(ajaxSubmit).on("click", function () {
        var $this = $(this);
        $this.find(".waves-ripple").remove();
        if ($(ajaxValidateForm).valid()) {
            $this.button('loading');
            $.ajax({
                type: "POST",
                url: $(ajaxValidateForm).attr("action"),
                data: $(ajaxValidateForm).serialize(),
                headers:
                    {
                        "RequestVerificationToken": $(ajaxValidateForm).find("input[name='__RequestVerificationToken']").val()
                    },
                success: function (rs) {
                    if (callBack != null) {
                        callBack(rs, $this);
                    }
                    else {

                        alertify.notify(rs.message, rs.type, 5);
                        setTimeout(function () {
                            $this.button('reset');
                        }, 300);
                        if (rs.output == 1) {
                            $(".modal-footer .close").click();
                            reGetDataGrid();
                        }
                    }
                },
                error: function () {
                    alert("Vui lòng F5 trình duyệt rồi thử lại!");
                },
                complete: function (jqxhr, txt_status) {
                    if (jqxhr.status == 502) {
                        alertify.notify("Phiên làm việc đã kết thúc F5 trình duyệt rồi thử lại.", "warning", 5);
                    }
                }
            });
        }
    });
}

function initAjaxDelete(callBack, btndelete = ".btndelete", ajaxValidateForm = ".ajaxValidateForm") {
    closeSearchExtension();
    $(btndelete).on("click", function () {
        var $this = $(this);
        swal({
            title: "Bạn có muốn thực hiện chức năng này?",
            text: "Nếu bạn thực hiện, dữ liệu này sẽ bị xóa vĩnh viên, bạn có muốn tiếp tục?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Đồng ý, xóa bản ghi này!",
            closeOnConfirm: false
        }, function () {
            $this.button('loading');
            $.ajax({
                type: "POST",
                url: $this.attr("data-action"),
                data: $(ajaxValidateForm).serialize(),
                headers:
                    {
                        "RequestVerificationToken": $(ajaxValidateForm).find("input[name='__RequestVerificationToken']").val()
                    },
                success: function (rs) {
                    if (callBack != null) {
                        callBack(rs, $this);
                    }
                    else {
                        var returnurl = $this.attr("data-returnurl");
                        if (rs.isUse) {
                            swal({
                                title: "Xóa dữ liệu thất bại!",
                                text: rs.message,
                                type: "warning",
                                confirmButtonText: "Đóng!"
                            });
                        }
                        else {
                            if (rs.output == 1) {
                                swal({
                                    title: "Thành công!",
                                    text: "Xóa dữ liệu thành công, click quay lại ngay hoặc tự động quay lại trang danh sách trong 5s!",
                                    type: "success",
                                    confirmButtonText: "Quay lại ngay!"
                                },
                                    function () {
                                        window.location.href = returnurl;
                                    });

                                setTimeout(function () {
                                    window.location.href = returnurl;
                                }, 5000);
                            }
                            else {
                                alertify.notify(rs.message, rs.type, 5);
                            }
                        }
                        setTimeout(function () {
                            $this.button('reset');
                        }, 300);
                    }
                },
                error: function () {
                    alert("Vui lòng F5 trình duyệt rồi thử lại!");
                },
                complete: function (jqxhr, txt_status) {
                    if (jqxhr.status == 502) {
                        alertify.notify("Phiên làm việc đã kết thúc F5 trình duyệt rồi thử lại.", "warning", 5);
                    }
                }
            });
        });
    });
}
function intitFileUpload(callBack, ajaxValidateForm = ".ajaxValidateForm") {
    $(ajaxValidateForm + " .chosefileupload").change(function () {
        var $this = $(ajaxValidateForm + " #btnChosefile");
        $this.button('loading');
        var fileUpload = document.getElementById("file");
        var data = new FormData();
        for (var i = 0; i < fileUpload.files.length; i++) {
            var file = fileUpload.files[i];
            data.append(file.name, file);
        }
        $.ajax({
            type: "POST",
            url: $(".ajaxValidateForm .chosefileupload").attr("data-url"),
            contentType: false,
            processData: false,
            data: data,
            dataType: "html",
            headers: {
                "RequestVerificationToken": $(".ajaxValidateForm").find("input[name='__RequestVerificationToken']").val()
            },
            success: function (rs) {
                rs = JSON.parse(rs);
                callBack(rs, $this);
            },
            error: function () {
                alert("Vui lòng F5 trình duyệt rồi thử lại!");
            },
            complete: function (jqxhr, txt_status) {
                if (jqxhr.status == 502) {
                    alertify.notify("Phiên làm việc đã kết thúc F5 trình duyệt rồi thử lại.", "warning", 5);
                }
            }
        });
    });
}
function dataGridView(isFirst, page = 1, data, urlAjax, isChangeUrl = true, callBackSetValue = null, tagBindTag = "#dataBindGrid", tagLoading = ".card-loading", tagCheckAll = "#checkAll", callBack = null) {
    var $loading = bindLoading(tagLoading);
    if (!isFirst && isChangeUrl == true) {
        changeUrl(data);
    }
    else {
        if (callBackSetValue != null) {
            callBackSetValue(data);
        }
    }
    $(tagCheckAll).prop("checked", false);
    if (callBack == null) {
        $.ajax({
            method: 'POST',
            url: urlAjax,
            data: data
        }).done(function (data, statusText, xhdr) {
            $(tagBindTag).html(data);
            $loading.waitMe('hide');
        }).fail(function (xhdr, statusText, errorText) {

        });
    }
    else {
        callBack(isFirst, page, data, urlAjax);
    }
}
