﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;

namespace TN.API.Controllers
{
  
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : Controller<OpenLockRequest, IDeviceService>
    {
        public DeviceController(IDeviceService service) : base(service)
        {
        }
       
        [HttpPost("OpenLock")]
        [Authorize]
        public async Task<object> OpenLock([FromBody] OpenLockRequestCommand cm)
        {
            if (!ModelState.IsValid)
            {
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            }    
            return  await Service.OpenLock(cm);
        }

        [HttpGet("RequestInfo/{id}")]
        [Authorize]
        public async Task<object> RequestInfo(int id) => await Service.RequestInfo(id);

        [HttpGet("LastRequestInfo")]
        [Authorize]
        public async Task<object> LastRequestInfo() => await Service.LastRequestInfo();
    }
}
