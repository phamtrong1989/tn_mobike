﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Services;
using TN.Domain.Seedwork;

namespace TN.API.Controllers
{
    public abstract class Controller<TModel, TService> : Controller
            where TService : IService<TModel>
    {
        protected readonly TService Service;

        protected Controller(TService service)
        {
            Service = service;
        }

        public int UserId
        {
            get
            {
                return Convert.ToInt32(User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value);
            }
        }
    }
}
