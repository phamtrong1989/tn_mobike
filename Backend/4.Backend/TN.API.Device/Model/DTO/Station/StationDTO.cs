﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using static TN.Domain.Model.Bike;

namespace TN.API.Model
{
    public class StationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int TotalDock { get; set; }
        public int ErrorDock { get; set; }
        public int TotalBike { get; set; }
        public int BookedBike { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Distance { get; set; }

        public virtual List<DockDTO> Docks { get; set; }

        public StationDTO(Station station)
        {
           
        }

        public StationDTO(Station station, int distance)
        {
           
        }
    }

    public class DockDTO
    {
        public int Id { get; set; }
        public int OrderNumber { get; set; }
        public EBikeStatus Status { get; set; }
        public bool LockStatus { get; set; }
        public BikeDTO CurrentBike { get; set; }

        public DockDTO(Dock dock)
        {
           
        }
    }

    public class BikeDTO
    {
        public int Id { get; set; }
        public string Etag { get; set; }
        public Bike.EBikeStatus Status { get; set; }
        public Bike.EBikeType Type { get; set; }

        public BikeDTO(Bike bike)
        {
        }
    }
}
