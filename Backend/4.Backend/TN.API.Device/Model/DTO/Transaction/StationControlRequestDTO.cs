﻿using TN.Domain.Model;

namespace TN.API.Model
{
    public class StationControlRequestDTO
    {
        public int Id { get; set; }
        public int TransactionId { get; set; }
        public int Money { get; set; }
        public int Points { get; set; }
        public int Acc_Points { get; set; }
        public EAccountType Acc_Type { get; set; }

        public StationControlRequestDTO(int id, int transactionId, int money, int points, int acc_points, EAccountType acc_type)
        {
            this.Id = id;
            this.TransactionId = transactionId;
            this.Money = money;
            this.Points = points;
            this.Acc_Points = acc_points;
            this.Acc_Type = acc_type;
        }
    }
}
