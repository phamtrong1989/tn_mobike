﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Model
{
    public class FeedbackDTO
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public Feedback.EFeedbackStatus StatusId { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Rating { get; set; }


        public FeedbackDTO(Feedback Feedback)
        {
            Id = Feedback.Id;
            AccountId = Feedback.AccountId;
            Rating = Feedback.Rating;
            StatusId = Feedback.Status;
            Message = Feedback.Message;
            CreatedDate = Feedback.CreatedDate;
        }
    }
}
