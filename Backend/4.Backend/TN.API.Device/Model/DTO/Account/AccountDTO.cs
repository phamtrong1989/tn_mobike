﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Model
{
    public class AccountDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public ESex Sex { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Code { get; set; }
        public int Points { get; set; }
        public EAccountStatus Status { get; set; }
        public EAccountType Type { get; set; }
        public string Token { get; set; }
        public DateTime ExpTocken { get; set; }

        public AccountDTO(Account account)
        {
            Id = account.Id;
            Username = account.Username;
            Avatar = account.Avatar;
            Email = account.Email;
            Sex = account.Sex;
            FirstName = account.FirstName;
            LastName = account.LastName;
            Birthday = account.Birthday;
            Phone = account.Phone;
            Address = account.Address;
            Status = account.Status;
            Type = account.Type;
            Code = account.Code;
        }

        public AccountDTO(Account account, string token)
        {
            Id = account.Id;
            Username = account.Username;
            Avatar = account.Avatar;
            Email = account.Email;
            Sex = account.Sex;
            FirstName = account.FirstName;
            LastName = account.LastName;
            Birthday = account.Birthday;
            Phone = account.Phone;
            Address = account.Address;
            Status = account.Status;
            Type = account.Type;
            Token = token;
            Code = account.Code;
        }
    }
}
