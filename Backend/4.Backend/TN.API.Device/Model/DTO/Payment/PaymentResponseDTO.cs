﻿namespace TN.API.Model
{
    public class PaymentResponseDTO
    {
        public int Status { get; set; }
        public string Error { get; set; }
        public int Data { get; set; }
        public string Smartcontract { get; set; }
    }
    public class PaymentResponseDTO<T> where T : class
    {
        public int Status { get; set; }
        public string Error { get; set; }
        public T Data { get; set; }
        public string Smartcontract { get; set; }
    }
}
