﻿namespace TN.API.Model
{
    public class PaymentOrderCommand
    {
        public int TicketTransactionId { get; set; }
        public int Accountid { get; set; }
        public string Cmd { get; set; }
        public string Merchantcode { get; set; }
        public double Total { get; set; }
        public string Currency { get; set; }
        public string Billid { get; set; }
        public string Smartcontract { get; set; }
    }
}
