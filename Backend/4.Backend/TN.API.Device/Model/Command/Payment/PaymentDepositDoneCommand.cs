﻿namespace TN.API.Model
{
    public class PaymentDepositDoneCommand
    {
        public int amount { get; set; }
        public string message { get; set; }
        public string payment_type { get; set; }
        public string reference_number { get; set; }
        public int status { get; set; }
        public int trans_ref_no { get; set; }
        public int website_id { get; set; }
        public string signature { get; set; }
    }
}
