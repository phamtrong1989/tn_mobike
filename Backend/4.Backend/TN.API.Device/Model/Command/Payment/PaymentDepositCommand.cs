﻿namespace TN.API.Model
{
    public class PaymentDepositCommand
    {
        public int Accountid { get; set; }
        public string Cmd { get; set; }
        public string Merchantcode { get; set; }
        public int Total { get; set; }
        public string Currency { get; set; }
        public string Code { get; set; }
    }
}
