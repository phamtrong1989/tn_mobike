﻿namespace TN.API.Model
{
    public class GetTransactionHistoryCommand
    {
        public int AccountId { get; set; }
        public int TicketTransactionId { get; set; }
    }
}
