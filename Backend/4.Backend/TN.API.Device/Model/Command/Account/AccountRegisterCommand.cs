﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class AccountRegisterCommand
    {
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public ESex Sex { get; set; }
        [Required]
        public DateTime Birthday { get; set; }
        [Required]
        public byte[] AvatarData { get; set; }
        public string CodeChiaSe { get; set; }
    }
}
