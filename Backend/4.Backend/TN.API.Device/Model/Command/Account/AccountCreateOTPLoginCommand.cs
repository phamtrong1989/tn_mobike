﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountCreateOTPLoginCommand
    {
        [Required]
        public string Phone { get; set; }
    }
}
