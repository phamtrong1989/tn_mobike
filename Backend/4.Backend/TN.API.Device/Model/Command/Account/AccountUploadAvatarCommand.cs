﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountUploadAvatarCommand
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public byte[] AvatarData { get; set; }
    }
}
