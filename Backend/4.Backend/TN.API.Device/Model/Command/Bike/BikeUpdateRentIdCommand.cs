﻿using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Model;
using static TN.Domain.Model.Account;

namespace TN.API.Model
{
    public class BikeUpdateRentIdCommand
    {
        [Required]
        public int Id { get; set; }
    }

    public class OpenLockRequestCommand
    {
        [MaxLength(50)]
        [Required]
        public string IMEI { get; set; }

        [MaxLength(50)]
        [Required]
        public string Phone { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public long TicketId { get; set; }

        public int StationId { get; set; }
    }
}
