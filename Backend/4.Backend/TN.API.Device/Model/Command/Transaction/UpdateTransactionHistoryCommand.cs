﻿namespace TN.API.Model
{
    public class UpdateTransactionHistoryCommand
    {
        public int AccountId { get; set; }
        public int TicketTransactionId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
    }
}
