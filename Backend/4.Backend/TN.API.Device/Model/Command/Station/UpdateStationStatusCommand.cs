﻿namespace TN.API.Model
{
    public class UpdateStationStatusCommand
    {
        public string DockStatusStr { get; set; }
        public string OtherStatusStr { get; set; }
    }
}
