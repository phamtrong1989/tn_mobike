﻿using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using TN.API.Model;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;

namespace TN.API.Services
{
    public interface IDeviceService : IService<OpenLockRequest>
    {
        Task<object> OpenLock(OpenLockRequestCommand cm);
        Task<object> RequestInfo(int id);
        Task<object> LastRequestInfo();
    }
   
    public class DeviceService : IDeviceService
    {
        private readonly IDockRepository _dockRepository;
        private readonly IOpenLockRequestRepository _iOpenLockRequestRepository;
        private readonly IOpenLockHistoryRepository _iOpenLockHistoryRepository;

        public IConfigurationRoot Configuration { get; }

        public DeviceService(
            IDockRepository DockRepository,
            IOpenLockHistoryRepository iOpenLockHistoryRepository,
            IOpenLockRequestRepository iOpenLockRequestRepository)
        {
            _dockRepository = DockRepository;
            _iOpenLockRequestRepository = iOpenLockRequestRepository;
            _iOpenLockHistoryRepository = iOpenLockHistoryRepository;
        }

        public async Task<object> OpenLock(OpenLockRequestCommand cm)
        {
            try
            {
                var kt = await _iOpenLockRequestRepository.SearchOneAsync(x =>
                    x.AccountId == UserInfo.UserId
                    && x.IMEI == cm.IMEI
                    && x.StationId == cm.StationId
                    && (x.Status == LockRequestStatus.Waiting || x.Status == LockRequestStatus.Retrying || x.Status == LockRequestStatus.Success));

                if (kt == null)
                {

                    kt = new OpenLockRequest
                    {
                        CreatedDate = DateTime.Now,
                        IMEI = cm.IMEI,
                        OpenTime = null,
                        Phone = cm.Phone,
                        Retry = 0,
                        StationId = cm.StationId,
                        Status = LockRequestStatus.Waiting,
                        BikeId = cm.BikeId,
                        DockId = cm.DockId
                    };

                    var dlAddHistory = new OpenLockHistory
                    {
                        CreatedDate = kt.CreatedDate,
                        IMEI = kt.IMEI,
                        OpenTime = kt.OpenTime,
                        Phone = kt.Phone,
                        Retry = kt.Retry,
                        StationId = kt.StationId,
                        Status = kt.Status,
                        BikeId = kt.BikeId,
                        DockId = kt.DockId
                    };

                    await _iOpenLockRequestRepository.AddAsync(kt);
                    await _iOpenLockRequestRepository.Commit();

                    await _iOpenLockHistoryRepository.AddAsync(dlAddHistory);
                    await _iOpenLockHistoryRepository.Commit();

                    Utility.Log.Debug("DeviceService_OpenLock", $"Tạo mới lệnh {dlAddHistory.Id}, {cm.IMEI}, {cm.DockId}, {cm.Phone}");
                }
                else
                {
                    Utility.Log.Debug("DeviceService_OpenLock", $"Cập nhật lệnh {kt.Id}, {cm.IMEI}, {cm.DockId}, {cm.Phone}");
                }   
                
                return new ApiResponse<OpenLockRequest>()
                {
                    Data = kt,
                    ErrorCode = ApiResponse<OpenLockRequest>.Code.Ok,
                    Message = "Gửi yêu cầu mờ khóa thành công",
                    Status = true,
                    TotalRecord = 1
                };

            }
            catch (Exception ex)
            {
                Utility.Log.Error("DeviceService_OpenLock", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }

        public async Task<object> RequestInfo(int id)
        {
            try
            {
                var kt = await _iOpenLockRequestRepository.SearchOneAsync(x => x.Id == id);
                var outStatus = kt == null ? ApiResponse<OpenLockRequest>.Code.OpenLockRequestNotExist : ApiResponse<OpenLockRequest>.Code.Ok;
                return new ApiResponse<OpenLockRequest>()
                {
                    Data = kt,
                    ErrorCode = outStatus,
                    Status = true,
                    TotalRecord = 1,
                    Message = outStatus.GetDisplayName()
                };
            }
            catch (Exception ex)
            {
                Utility.Log.Error("DeviceService_RequestInfo", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }

        public async Task<object> LastRequestInfo()
        {
            try
            {
                var kt = await _iOpenLockRequestRepository.SearchOneAsync(x => x.AccountId == UserInfo.UserId);
                var outStatus = kt == null ? ApiResponse<OpenLockRequest>.Code.OpenLockRequestNotExist : ApiResponse<OpenLockRequest>.Code.Ok;
                return new ApiResponse<OpenLockRequest>()
                {
                    Data = kt,
                    ErrorCode = outStatus,
                    Status = true,
                    TotalRecord = 1,
                    Message = outStatus.GetDisplayName()
                };
            }
            catch (Exception ex)
            {
                Utility.Log.Error("DeviceService_LastRequestInfo", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
    }
}