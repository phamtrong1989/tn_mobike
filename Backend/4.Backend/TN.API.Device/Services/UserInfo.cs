﻿using System;
using System.IdentityModel.Tokens.Jwt;

namespace TN.API
{
    public class UserInfo
    {
        public static int UserId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(AppHttpContext.Current.User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value);
                }
                catch
                {
                    return 0;
                }
            }
        }

        //public static int ObjectId
        //{
        //    get
        //    {
        //        return Convert.ToInt32(AppHttpContext.Current.User.FindFirst("ObjectId")?.Value);
        //    }
        //}

        //public static bool IsSuperadmin
        //{
        //    get
        //    {
        //        if(AppHttpContext.Current.User.FindFirst("IsSuperadmin")==null)
        //        {
        //            return false;
        //        }
        //        return Convert.ToBoolean(AppHttpContext.Current.User.FindFirst("IsSuperadmin")?.Value);
        //    }
        //}
        //public static RoleManagerType RoleType
        //{
        //    get
        //    {
        //        if (AppHttpContext.Current.User.FindFirst("RoleType") == null)
        //        {
        //            return RoleManagerType.Default;
        //        }
        //        return (RoleManagerType)Convert.ToInt32(AppHttpContext.Current.User.FindFirst("RoleType")?.Value);
        //    }
        //}
    }
}
