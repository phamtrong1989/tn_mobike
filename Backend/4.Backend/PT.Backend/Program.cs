﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace PT.Backend
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateWebHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                // .UseSetting("https_port", "443")
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureLogging((hostingContext, config) => { config.ClearProviders(); })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.SetBasePath(hostingContext.HostingEnvironment.ContentRootPath);
                    config.AddJsonFile("appsettings.json", true, true);
                    config.AddJsonFile("appsettings.Base.json", true, true);
                    config.AddJsonFile("appsettings.Email.json", true, true);
                    config.AddJsonFile("appsettings.Authorize.json", true, true);
                    config.AddJsonFile("appsettings.NotificationTemp.json", true, true);
                    config.AddJsonFile("appsettings.Log.json", true, true);
                    config.AddJsonFile("appsettings.Api.json", true, true);

                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
        }
    }
}