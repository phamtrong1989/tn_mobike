﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using System.Collections.Generic;
using System.Reflection;
using TN.Infrastructure.Interfaces;
using System.Linq;
using TN.Domain.Model.Common;
using TN.Infrastructure.Repositories;

namespace TN.Backend.Areas.Manage
{

    [Area("Admin")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class RoleControllersController : BaseController<RoleController, IRoleControllerService>
    {
        private readonly IRoleControllerRepository _iRoleControllerRepository;
        private readonly IRoleGroupRepository _iRoleGroupRepository;
        public RoleControllersController(
            IRoleControllerService service,
            IRoleControllerRepository iRoleControllerRepository,
            IRoleGroupRepository iRoleGroupRepository)
          : base(service)
        {
            _iRoleControllerRepository = iRoleControllerRepository;
            _iRoleGroupRepository = iRoleGroupRepository;
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int pageIndex,
            int pageSize,
            string key,
            int? groupId,
            string areaId,
            string sortby,
            string sorttype) => await Service.SearchPageAsync(pageIndex, pageSize, key, groupId, areaId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(string id) => await Service.GetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("AutoCreate")]
        public async Task<object> AutoCreate()
        {
            try
            {
                var getList = await GetControllers();
                var group = await _iRoleGroupRepository.SearchOneAsync(x => x.Id > 0);

                foreach (var item in getList)
                {
                    await Create(new RoleControllerCommand
                    {
                        AreaId = item.Area,
                        GroupId = group.Id,
                        Name = item.Controller,
                        Id = item.Controller,
                        IsShow = true,
                        Order = 1,
                        Href = ""
                    });
                }
                return new ApiResponseData<object>() { Status = 1 };
            }
            catch
            {
                return new ApiResponseData<object>();
            }
        }

        private async Task<List<ControllerActions>> GetControllers()
        {
            var listController = await _iRoleControllerRepository.Search();

            var asm = Assembly.GetExecutingAssembly();
            var controlleractionlist = asm.GetTypes()
                    .Where(type => typeof(ControllerBase).IsAssignableFrom(type))
                    .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                    .Select(x => new
                    {
                        Controller = x.DeclaringType.Name,
                        Action = x.Name,
                        Area = x.DeclaringType.CustomAttributes.Where(c => c.AttributeType == typeof(AreaAttribute))

                    }).ToList();

            var list = new List<ControllerActions>();

            foreach (var item in controlleractionlist)
            {
                if (item.Area.Any())
                {
                    list.Add(new ControllerActions()
                    {
                        Controller = item.Controller,
                        Action = item.Action,
                        Area = item.Area.Select(v => v.ConstructorArguments[0].Value.ToString()).FirstOrDefault()
                    });
                }
                else
                {
                    list.Add(new ControllerActions()
                    {
                        Controller = item.Controller,
                        Action = item.Action,
                        Area = null,
                    });
                }
            }

            return list
                .Where(x => x.Controller != "RoleAreas" && x.Controller != "RoleControllers" && x.Controller != "RoleGroups" && x.Controller != "BaseController")
                .Where(x => !listController.Any(m => m.Id + "Controller" == x.Controller)).Select(x => new ControllerActions
                {
                    Action = x.Action,
                    Area = x.Area,
                    Controller = x.Controller?.Replace("Controller", "")
                }).ToList();
        }

        [AuthorizePermission("Index")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] RoleControllerCommand value) => await Service.Create(value);

        [AuthorizePermission("Index")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] RoleControllerCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(string id) => await Service.Delete(id);

        #region [RoleAcction]
        [AuthorizePermission("Index")]
        [HttpGet("RoleActionSearchPaged")]
        public async Task<object> RoleActionSearchPaged(int? pageIndex, int? pageSize, string key, string controllerId, string sortby, string sorttype) => await Service.RoleActionSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, key, controllerId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("RoleActionGetById/{id}")]
        public async Task<object> RoleActionGetById(int id) => await Service.RoleActionGetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("RoleActionCreate")]
        public async Task<object> RoleAccionCreate([FromBody] RoleActionCommand value) => await Service.RoleActionCreate(value);

        [AuthorizePermission("Index")]
        [HttpPut("RoleActionEdit")]
        public async Task<object> RoleAccionEdit([FromBody] RoleActionCommand value) => await Service.RoleActionEdit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("RoleActionDelete/{id}")]
        public async Task<object> Delete(int id) => await Service.RoleActionDelete(id);
        #endregion
    }
}