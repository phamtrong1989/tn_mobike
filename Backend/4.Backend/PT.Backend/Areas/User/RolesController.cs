﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Backend.Controllers;
using PT.Base;
using PT.Base.Command;
using PT.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.Backend.Areas.User
{

    [Area("Admin")]
    [Route("api/[area]/[controller]")]
    [AuthorizePermission]
    [ApiController]
    public class RolesController : BaseController<ApplicationRole, IRolesService>
    { 

        public RolesController(IRolesService service)
          : base(service)
        {
        }

        [HttpGet("Search")]
        public async Task<object> Search(int? pageIndex, int? PageSize, string key, RoleManagerType? type, string sortby, string sorttype) 
            => await Service.SearchPageAsync(pageIndex ?? 1, PageSize ?? 100, key, type, sortby, sorttype);

        [HttpGet("GetById/{id}/{importDetails}")]
        public async Task<ApiResponseData<ApplicationRole>> GetById(int id, bool importDetails) 
            => await Service.GetById(id, importDetails);

        [HttpPost("Create")]
        public async Task<object> Create([FromBody] RoleCommand value) => await Service.Create(value);

        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] RoleCommand value) => await Service.Edit(value);

        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

    }
}