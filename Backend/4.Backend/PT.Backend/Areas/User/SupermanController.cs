﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using PT.Base;
//using TN.Base;
//using TN.Base.Command;
//using TN.Base.Services;
//using TN.Domain.Model;
//using TN.Backend.Controllers;
//using TN.Infrastructure.Interfaces;
//using TN.Domain.Model.Common;

//namespace TN.Backend.Areas.Manage
//{

//    [Area("Admin")]
//    [Route("api/[area]/[controller]")]
//    [ApiController]
//    public class SupermanController : BaseController<RoleGroup, IRoleGroupService>
//    {
//        private readonly IDockRepository _iDockRepository;
//        private readonly IOpenLockRequestRepository _iOpenLockRequestRepository;
//        public SupermanController(IRoleGroupService service, IDockRepository iDockRepository, IOpenLockRequestRepository iOpenLockRequestRepository)
//          : base(service)
//        {
//            _iDockRepository =  iDockRepository;
//            _iOpenLockRequestRepository = iOpenLockRequestRepository;
//        }

//        [AuthorizePermission("TrongDZ")]
//        [HttpPut("DockOpenAll/{projectId}")]
//        public async Task<object> DockOpenAll(int projectId)
//        {
//            try
//            {
//                var list = await _iDockRepository.Search(x => x.ProducerId == projectId && x.Status == Bike.EBikeStatus.Active);
//                foreach (var ktDock in list)
//                {
//                    var kt = new OpenLockRequest
//                    {
//                        CreatedDate = DateTime.Now,
//                        IMEI = ktDock.IMEI,
//                        OpenTime = null,
//                        Retry = 0,
//                        StationId = ktDock.StationId,
//                        Status = LockRequestStatus.Waiting,
//                        DockId = ktDock.Id,
//                        AccountId = 0,
//                        TransactionCode = null,
//                        SerialNumber = ktDock.SerialNumber,
//                        InvestorId = ktDock.InvestorId,
//                        ProjectId = ktDock.ProjectId
//                    };
//                    await _iOpenLockRequestRepository.AddAsync(kt);
//                    await _iOpenLockRequestRepository.Commit();
//                }
//                return new ApiResponseData<object>() { Status = 1, Message = $"Gửi lệnh mở {list.Count} khóa thành công" };
//            }
//            catch
//            {
//                return new ApiResponseData<object>() { Status = 0};
//            }
//        }
//    }
//}