﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Backend.Controllers;
using PT.Base;
using PT.Base.Command;
using PT.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.Backend.Areas.User
{
    [Area("Admin")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class UsersController : BaseController<ApplicationUser, IUsersService>
    {
        public UsersController(IUsersService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(
            int pageIndex,
            int pageSize,
            string key,
            int? roleId,
            bool? isLock,
            bool? isRetired,
            string code,
            string sortby,
            string sorttype
       )
            => await Service.SearchPagedAsync(pageIndex, pageSize, key, roleId, isLock, isRetired, code,  sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<ApiResponseData<ApplicationUser>> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] ManagerRegisterCommand value) => await Service.Create(value);

        [AuthorizePermission("Index")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] ManagerRegisterCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        [HttpPost("UploadImage")]
        [AuthorizePermission("Index")]
        public async Task<object> UploadImage() => await Service.UploadImage(Request);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByUser")]
        public async Task<object> SearchByUser(string key) => await Service.SearchByUser(key);

    }
}