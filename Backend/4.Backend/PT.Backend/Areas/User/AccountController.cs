﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TN.Backend.Controllers;
using PT.Base.Models;
using PT.Base.Services;
using PT.Domain.Model;
using TN.Domain.Model;
using PT.Base;

namespace TN.Backend.Areas.User
{
    [AppVerification]
    [Area("Admin")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class AccountController : BaseController<ApplicationUser, IAccountService>
    {
        private readonly IOptions<AuthorizeSettings> _authorizeSettings;
        public AccountController(IAccountService sevice, IOptions<AuthorizeSettings> authorizeSettings) :base(sevice)
        {
            _authorizeSettings = authorizeSettings;
        }

        [HttpPost("Login")]
        public async Task<object> LoginAsync([FromBody] LoginCommand value) => await Service.LoginAsync(value);

        [HttpGet("GetProfiles")]
        [Authorize]
        public async Task<object> GetProfiles() => await Service.GetProfiles();

        [HttpPut("UpdateProfiles")]
        [Authorize]
        public async Task<object> UpdateProfiles([FromBody] AccountProfilesCommand value) => await Service.UpdateProfiles(value);

        [HttpPut("ChangePassword")]
        [Authorize]
        public async Task<object> ChangePassword([FromBody] ChangePasswordCommand value) => await Service.ChangePassword(value);
    }
}