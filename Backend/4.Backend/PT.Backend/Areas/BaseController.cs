﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IdentityModel.Tokens.Jwt;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Controllers
{
    public abstract class BaseController<TModel, TService> : Controller
            where TService : IService<TModel>
    {
        protected readonly TService Service;

        protected BaseController(TService service)
        {
            Service = service;
        }
        public int UserId
        {
            get
            {
                return Convert.ToInt32(User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value);
            }
        }
        public RoleManagerType RoleType
        {
            get
            {
                if(User.FindFirst("RoleType")==null)
                {
                    return RoleManagerType.Admin;
                }
                return (RoleManagerType)Convert.ToInt32(User.FindFirst("RoleType")?.Value);
            }
        }
    }
}
