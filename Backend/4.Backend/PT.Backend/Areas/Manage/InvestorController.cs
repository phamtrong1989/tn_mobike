using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using PT.Base;
using PT.Base.Command;
using Microsoft.AspNetCore.Authorization;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class InvestorsController : BaseController<Investor, IInvestorService>
    { 
        public InvestorsController(IInvestorService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? pageSize, string key, EInvestorStatus? status, string sortby, string sorttype) 
            => await Service.SearchPageAsync(pageIndex ?? 1, pageSize ?? 100, key, status, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);
       
        [AuthorizePermission("Index")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] InvestorCommand value) => await Service.Create(value);

        [AuthorizePermission("Index")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] InvestorCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        #region [Log]
        [Authorize]
        [HttpGet("LogSearchPaged")]
        public async Task<object> LogSearchPaged(int? pageIndex, int? pageSize, int projectid, string sortby, string sorttype) => await Service.LogSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, projectid, sortby, sorttype);
        #endregion
    }
}