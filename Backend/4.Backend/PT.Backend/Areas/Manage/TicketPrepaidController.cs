using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using PT.Base;
using System;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class TicketPrepaidsController : BaseController<TicketPrepaid, ITicketPrepaidService>
    { 
        public TicketPrepaidsController(ITicketPrepaidService service)
          : base(service)
        {
        }


        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(
            int pageIndex,
            int pageSize,
            string key,
            ETicketType? ticketPrice_TicketType,
            int? accountId,
            int? projectId,
            int? customerGroupId,
            DateTime? sellStart,
            DateTime? sellEnd,
            string sortby,
            string sorttype) 
            => await Service.SearchPageAsync(pageIndex, pageSize, key, ticketPrice_TicketType, accountId, projectId, customerGroupId, sellStart, sellEnd, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] TicketPrepaidCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] TicketPrepaidEditCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpGet("SearchTicketPriceByProject")]
        public async Task<object> SearchTicketPriceByProject(int projectId, int accountId) => await Service.SearchTicketPriceByProject(projectId, accountId);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await Service.SearchByAccount(key);

        [AuthorizePermission("Index")]
        [HttpPost("Report")]
        public async Task<string> Report(
            string key,
            ETicketType? ticketPrice_TicketType,
            int? accountId,
            int? projectId,
            int? customerGroupId,
            DateTime? sellStart,
            DateTime? sellEnd
        )
        => await Service.ExportReport(key, ticketPrice_TicketType, accountId, projectId, customerGroupId, sellStart, sellEnd);
    }
}