using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using TN.Base.Command;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]

    public class DashboardController : BaseController<Station, IDashboardService>
    {
        private readonly IBookingService _iBookingService;
        private readonly IBikeService _iBikeService;
        public DashboardController(IDashboardService service, IBookingService iBookingService, IBikeService iBikeService)
          : base(service)
        {
            _iBookingService = iBookingService; 
            _iBikeService = iBikeService;
        }

        [AuthorizePermission("Index")]
        [HttpGet("BikeSearchPaged")]
        public async Task<object> BikeSearchPaged(
             int pageIndex,
             int pagesize,
             EDockBookingStatus? bookingStatus,
             string key,
             int? projectId,
             Bike.EBikeType? type,
             bool? connectionStatus,
             int? battery,
             bool? lockStatus,
             bool? charging,
             int? stationId,
             string sortby,
             string sorttype)
        => await _iBikeService.SearchPageAsync(pageIndex, pagesize, bookingStatus, key, null, null, projectId, type, Bike.EBikeStatus.Active, connectionStatus, battery, lockStatus, charging, stationId, sortby, sorttype);

        [HttpGet("Stations/{projectId}")]
        [Authorize]
        public async Task<object> Stations(int projectId) => await Service.Stations(projectId);

        [HttpGet("Bookings/{projectId}")]
        [Authorize]
        public async Task<object> Bookings(int projectId) => await Service.Bookings(projectId);

        [HttpGet("BikeNotBookings/{projectId}")]
        [Authorize]
        public async Task<object> BikeNotBookings(int projectId) => await Service.BikeNotBookings(projectId);

        [HttpGet("DataInfoAccountOnline")]
        [Authorize]
        public async Task<object> DataInfoAccountOnline() => await Service.DataInfoAccountOnline();
        
        [HttpGet("BikeGetById/{id}")]
        [AuthorizePermission("Index")]
        public async Task<object> BikeGetById(int id) => await _iBikeService.GetById(id);

        [HttpGet("GetByIMEI")]
        [AuthorizePermission("Index")]
        public object GetByIMEI(string imei, DateTime start) => Service.GPSByIMEI(imei, start, DateTime.Now);

        [HttpPost("BookingCheckOut")]
        [AuthorizePermission("End")]
        public async Task<object> BookingCheckOut(BookingEndCommand cm) => await _iBookingService.BookingCheckOut(cm, 0, 0);

        [HttpPost("BookingCancel")]
        [AuthorizePermission("Cancel")]
        public async Task<object> BookingCancel(BookingCancelCommand cm) => await _iBookingService.BookingCancel(cm);

        [HttpPost("OpenLock")]
        [AuthorizePermission("Open")]
        public async Task<object> OpenLock(BookingOpenLockCommand cm) => await _iBookingService.OpenLock(cm);
       
        [HttpGet("BookingCalculate/{id}")]
        [AuthorizePermission("Index")]
        public async Task<object> BookingCalculate(int id) => await _iBookingService.BookingCalculate(id);

        [HttpPost("ReCall")]
        [AuthorizePermission("End")]
        public async Task<object> ReCall(ReCallCommand cm) => await _iBookingService.ReCall(cm);

        [HttpGet("EventSystemSearchPaged")]
        [AuthorizePermission("Index")]
        public object EventSystemSearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            bool? isFlag,
            bool? isProcessed,
            EEventSystemType? type,
            EEventSystemGroup? group,
            EEventSystemWarningType? warningType,
            DateTime? start,
            DateTime? end,
            int? projectId,
            string transactionCode, string plate
            ) =>  Service.EventSystemSearchPageAsync(pageIndex, pageSize, key, isFlag, isProcessed, type, group, warningType, start, end, projectId, transactionCode, plate);

        [AuthorizePermission("Index")]
        [HttpPost("EventSystemChangeIsFlag/{id}/{isFlag}")]
        public object EventSystemChangeIsFlag(long id, bool isFlag) =>  Service.EventSystemChangeIsFlag(id, isFlag);
        
        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await _iBookingService.SearchByAccount(key);

        [AuthorizePermission("Index")]
        [HttpGet("GPSByDock")]
        public async Task<object> GPSByDock(int id, DateTime date, string startTime, string endTime) => await _iBikeService.GPSByDock(id, date, startTime, endTime, true, false);

        [AuthorizePermission("Index")]
        [HttpGet("LogSearchPaged")]
        public async Task<object> LogSearchPaged(
            int pageIndex,
            int pageSize,
            string key,
            string @object,
            int? systemUserId,
            DateTime? start,
            DateTime? end
            )
       => await Service.LogSearchPageAsync(pageIndex, pageSize, key, @object, systemUserId, start, end);


        [AuthorizePermission("Index")]
        [HttpGet("SearchByUser")]
        public async Task<object> SearchByUser(string key) => await _iBookingService.SearchByUser(key);

    }
}