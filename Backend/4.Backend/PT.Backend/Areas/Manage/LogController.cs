using Microsoft.AspNetCore.Mvc;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{

    [Area("Admin")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class LogController : BaseController<Log, ILogService>
    {
        public LogController(ILogService service)
          : base(service)
        {
        }


        [AuthorizePermission("Index")]
        [HttpGet("MongoSearchPaged")]
        public object MongoSearchPageAsync(
            int pageIndex,
            int pageSize, int? accountId, string logKey, string functionName, EnumMongoLogType? type, DateTime? date, EnumSystemType? systemType,
            string deviceKey, bool? isExeption, string sortby, string sorttype)
            => Service.MongoSearchPageAsync(pageIndex, pageSize, accountId, logKey, functionName, type, date, systemType, deviceKey, isExeption, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await Service.SearchByAccount(key);
    }
}