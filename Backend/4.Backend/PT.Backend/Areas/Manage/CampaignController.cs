using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TN.Base;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using PT.Base;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class CampaignsController : BaseController<Campaign, ICampaignService>
    {
        public CampaignsController(ICampaignService service) : base(service)
        {
        }


        #region [Campaign]
        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int pageIndex, int pageSize, string key, int? projectId, ECampaignStatus? status, ECampaignType? type, string sortby, string sorttype)
            => await Service.SearchPageAsync(pageIndex, pageSize, key, projectId, status, type, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id)
        {
            return await Service.GetById(id);
        }

        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] CampaignCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] CampaignCommand value) => await Service.Edit(value);

        [AuthorizePermission("Delete")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);
        #endregion

        #region [VoucherCode]
        [AuthorizePermission("Index")]
        [HttpGet("VoucherCodeSearchPaged")]
        public async Task<object> VoucherCodeSearchPaged(int pageIndex, int pageSize, string key, int? campaignId, string sortby, string sorttype) => await Service.VoucherCodeSearchPageAsync(pageIndex, pageSize, key, campaignId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("VoucherCodeGetById/{id}")]
        public async Task<object> VoucherCodeGetById(int id) => await Service.VoucherCodeGetById(id);

        [AuthorizePermission("Edit")]
        [HttpPost("VoucherCodeCreate")]
        public async Task<object> VoucherCodeCreate([FromBody] VoucherCodeCommand value) => await Service.VoucherCodeCreate(value);

        [AuthorizePermission("Edit")]
        [HttpPut("VoucherCodeEdit")]
        public async Task<object> VoucherCodeEdit([FromBody] VoucherCodeEditCommand value) => await Service.VoucherCodeEdit(value);

        [AuthorizePermission("Edit")]
        [HttpDelete("VoucherCodeDelete/{id}")]
        public async Task<object> VoucherCodeDelete(int id) => await Service.VoucherCodeDelete(id);
        #endregion

        #region [Log]
        [AuthorizePermission("Index")]
        [HttpGet("LogSearchPaged")]
        public async Task<object> LogSearchPaged(int? pageIndex, int? pageSize, int campaignId, string sortby, string sorttype) => await Service.LogSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, campaignId, sortby, sorttype);
        #endregion

        #region [DepositEvent]
        [AuthorizePermission("Index")]
        [HttpGet("DepositEventSearchPaged")]
        public async Task<object> DepositEventSearchPaged(int? pageIndex, int? pageSize, int campaignId, string sortby, string sorttype) => await Service.DepositEventSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, campaignId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("DepositEventGetById/{id}")]
        public async Task<object> DepositEventGetById(int id) => await Service.DepositEventGetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("DepositEventCreate")]
        public async Task<object> DepositEventCreate([FromBody] DepositEventCommand value) => await Service.DepositEventCreate(value);

        [AuthorizePermission("Index")]
        [HttpPut("DepositEventEdit")]
        public async Task<object> DepositEventEdit([FromBody] DepositEventCommand value) => await Service.DepositEventEdit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("DepositEventDelete/{id}")]
        public async Task<object> DepositEventDelete(int id) => await Service.DepositEventDelete(id);
        #endregion

        #region [CampaignResult]
        [AuthorizePermission("Index")]
        [HttpGet("CampaignResultSearchPaged")]
        public async Task<object> CampaignResultSearchPaged(
            int pageIndex,
            int pageSize,
            string key,
            string sortby,
            string sorttype,

            DateTime? from,
            DateTime? to,
            int? projectId,
            int? campaignId,
            int? customerGroupId,
            ECampaignType? type
            ) => await Service.CampaignResultSearchPageAsync(pageIndex, pageSize, key, sortby, sorttype, from, to, projectId, campaignId, customerGroupId, type);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByCampaign")]
        public async Task<object> SearchByCampaign(string key) => await Service.SearchByCampaign(key);
        #endregion

        #region [DiscountCode]
        [AuthorizePermission("Index")]
        [HttpGet("DiscountCodeSearchPaged")]
        public async Task<object> DiscountCodeSearchPaged(int pageIndex, int pageSize, int campaignId, string sortby, string sorttype) => await Service.DiscountCodeSearchPageAsync(pageIndex, pageSize, campaignId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("DiscountCodeGetById/{id}")]
        public async Task<object> DiscountCodeGetById(int id) => await Service.DiscountCodeGetById(id);

        [AuthorizePermission("Edit")]
        [HttpPost("DiscountCodeCreate")]
        public async Task<object> DiscountCodeCreate([FromBody] DiscountCodeCommand value) => await Service.DiscountCodeCreate(value);

        [AuthorizePermission("Edit")]
        [HttpPut("DiscountCodeEdit")]
        public async Task<object> DiscountCodeEdit([FromBody] DiscountCodeCommand value) => await Service.DiscountCodeEdit(value);

        [AuthorizePermission("Edit")]
        [HttpDelete("DiscountCodeDelete/{id}")]
        public async Task<object> DiscountCodeDelete(int id) => await Service.DiscountCodeDelete(id);
        #endregion

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await Service.SearchByAccount(key);
    }
}