using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using PT.Base;
using PT.Base.Command;
using Microsoft.AspNetCore.Authorization;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ProjectsController : BaseController<Project, IProjectService>
    { 
        public ProjectsController(IProjectService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? pageSize, string key, EProjectStatus? status, string sortby, string sorttype)  => await Service.SearchPageAsync(pageIndex ?? 1, pageSize ?? 100, key, status, sortby, sorttype);
      
        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] ProjectCommand value) => await Service.Create(value);

        [AuthorizePermission("Index")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] ProjectCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpGet("ProjectResourceGetById/{projectId}/{language}")]
        public async Task<object> ProjectResourceGetById(int projectId, string language) => await Service.ProjectResourceGetById(projectId, language);

        [AuthorizePermission("Index")]
        [HttpPut("ProjectResourceEdit")]
        public async Task<object> ProjectResourceEdit([FromBody] ProjectResourceCommand value) => await Service.ProjectResourceEdit(value);

        [AuthorizePermission("Index")]
        [HttpGet("CustomerGroupResourceGetById/{customerGroupId}/{language}")]
        public async Task<object> CustomerGroupResourceGetById(int customerGroupId, string language) => await Service.CustomerGroupResourceGetById(customerGroupId, language);

        [AuthorizePermission("Index")]
        [HttpPut("CustomerGroupResourceEdit")]
        public async Task<object> CustomerGroupResourceEdit([FromBody] CustomerGroupResourceCommand value) => await Service.CustomerGroupResourceEdit(value);

        [AuthorizePermission("Index")]
        [HttpPut("HeadquartersEdit")]
        public async Task<object> HeadquartersEdit([FromBody] HeadquartersEditCommand value) => await Service.HeadquartersEdit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        #region [CustomerGroup]
        [AuthorizePermission("Index")]
        [HttpGet("CustomerGroupSearchPaged")]
        public async Task<object> CustomerGroupSearchPaged(int? pageIndex, int? pageSize, int projectId, string sortby, string sorttype) => await Service.CustomerGroupSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, projectId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("CustomerGroupGetById/{id}")]
        public async Task<object> CustomerGroupGetById(int id) => await Service.CustomerGroupGetById(id);
       
        [AuthorizePermission("Index")]
        [HttpPost("CustomerGroupCreate")]
        public async Task<object> CustomerGroupCreate([FromBody] CustomerGroupCommand value) => await Service.CustomerGroupCreate(value);

        [AuthorizePermission("Index")]
        [HttpPut("CustomerGroupEdit")]
        public async Task<object> CustomerGroupEdit([FromBody] CustomerGroupCommand value) => await Service.CustomerGroupEdit(value);

        [AuthorizePermission]
        [HttpDelete("CustomerGroupDelete/{id}")]
        public async Task<object> CustomerGroupDelete(int id) => await Service.CustomerGroupDelete(id);
        #endregion

        #region [TicketPrice]
        [AuthorizePermission("Index")]
        [HttpGet("TicketPriceSearchPaged")]
        public async Task<object> TicketPriceSearchPaged(int pageIndex, int pageSize, int? tenantId, int? customerGroupId, string sortby, string sorttype)
           => await Service.TicketPriceSearchPageAsync(pageIndex, pageSize, tenantId, customerGroupId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("TicketPriceGetById/{id}")]
        public async Task<object> TicketPriceGetById(int id) => await Service.TicketPriceGetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("TicketPriceCreate")]
        public async Task<object> TicketPriceCreate([FromBody] TicketPriceCommand value) => await Service.TicketPriceCreate(value);

        [AuthorizePermission("Index")]
        [HttpPut("TicketPriceEdit")]
        public async Task<object> TicketPriceEdit([FromBody] TicketPriceCommand value) => await Service.TicketPriceEdit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("TicketPriceDelete/{id}")]
        public async Task<object> TicketPriceDelete(int id) => await Service.TicketPriceDelete(id);
        #endregion

        #region [TenantAccount]
        [AuthorizePermission("Index")]
        [HttpGet("ProjectAccountSearchPaged")]
        public async Task<object> ProjectAccountSearchPaged(int pageIndex, int pageSize, int? projectId, int? customerGroupId, EAccountStatus? status, EAccountType? type, ESex? sex, string key, int? id, string sortby, string sorttype)
            => await Service.ProjectAccountSearchPageAsync(pageIndex, pageSize, projectId, customerGroupId, status, type, sex, key, id, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("ProjectAccountGetById/{id}")]
        public async Task<object> ProjectAccountGetById(int id) => await Service.ProjectAccountGetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("ProjectAccountCreate")]
        public async Task<object> ProjectAccountCreate([FromBody] ProjectAccountCommand value) => await Service.ProjectAccountCreate(value);

        [AuthorizePermission("Index")]
        [HttpPut("ProjectAccountEdit")]
        public async Task<object> ProjectAccountEdit([FromBody] ProjectAccountCommand value) => await Service.ProjectAccountEdit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("ProjectAccountDelete/{id}")]
        public async Task<object> ProjectAccountDelete(int id) => await Service.ProjectAccountDelete(id);
        #endregion

        #region [Account]
        [AuthorizePermission("Index")]
        [HttpGet("AccountGetByPhone/{phone}")]
        public async Task<object> AccountGetByPhone(string phone) => await Service.AccountGetByPhone(phone);
        #endregion

        #region [Log]
        [Authorize]
        [HttpGet("LogSearchPaged")]
        public async Task<object> LogSearchPaged(int? pageIndex, int? pageSize, int projectid, string sortby, string sorttype) => await Service.LogSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, projectid, sortby, sorttype);
        #endregion
    }
}