using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using PT.Base;
using static TN.Domain.Model.Bike;
using Microsoft.AspNetCore.Authorization;
using System;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class DocksController : BaseController<Dock, IDockService>
    {
        public DocksController(IDockService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(
            int pageIndex,
            int pageSize,
            string key,
            int? projectId,
            int? investorId,
            int? modelId,
            int? producerId,
            int? warehouseId,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            bool? charging,
            EBikeStatus? status,
            int? battery,
            bool? lockStatus,
            string sortby,
            string sorttype
          )
            => await Service.SearchPageAsync(pageIndex, pageSize, key, projectId, investorId, modelId, producerId, warehouseId, bookingStatus, connectionStatus, charging, status, battery, lockStatus, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] DockCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] DockCommand value) => await Service.Edit(value);


        [AuthorizePermission("Delete")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        [AuthorizePermission("Index")]
        [HttpGet("BikeSearch")]
        public async Task<object> BikeSearch(string key) => await Service.BikeSearch(key);

        [AuthorizePermission("OpenDock")]
        [HttpPost("OpenLock")]
        public async Task<object> OpenLock([FromBody] DockOpenManualCommand value) => await Service.OpenLock(value);

        #region [Log]
        [Authorize]
        [HttpGet("LogSearchPaged")]
        public async Task<object> LogSearchPaged(int? pageIndex, int? pageSize, int objectId, string sortby, string sorttype) => await Service.LogSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, objectId, sortby, sorttype);
        #endregion

        [AuthorizePermission("Index")]
        [HttpPost("Report")]
        public async Task<FileContentResult> Report(
            string key,
            int? projectId,
            int? investorId,
            int? modelId,
            int? producerId,
            int? warehouseId,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            bool? charging,
            EBikeStatus? status,
            int? battery,
            bool? lockStatus
        )
        => await Service.ExportReport(key, projectId, investorId, modelId, producerId, warehouseId, bookingStatus, connectionStatus, charging, status, battery, lockStatus);
    }
}