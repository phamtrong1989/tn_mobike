using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using PT.Base;
using System;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class NotificationJobsController : BaseController<NotificationJob, INotificationJobService>
    { 
        public NotificationJobsController(INotificationJobService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int pageIndex, int pageSize, string key, int? accountId, bool? isSend, DateTime? start, DateTime? end, string sortby, string sorttype) 
            => await Service.SearchPageAsync(pageIndex, pageSize, key, accountId, isSend, start, end,   sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] NotificationJobCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] NotificationJobCommand value) => await Service.Edit(value);

        [AuthorizePermission("Delete")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);


        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await Service.SearchByAccount(key);

    }
}