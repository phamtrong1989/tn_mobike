﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TN.Backend.Controllers;
using PT.Base;
using PT.Base.Command;
using PT.Base.Services;
using PT.Domain.Model;

namespace TN.Backend.Areas.User
{

    
    [Area("Admin")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class InitDataController : BaseController<object, IInitDataService>
    {
        public InitDataController(IInitDataService service) : base(service){}

        [Authorize]
        [HttpPost("PullData/{v}")]
        public async Task<object> PullData(string v) => await this.Service.PullDataAsync(v);

        [HttpPost("CurrentVersion")]
        public async Task<object> CurrentVersion() => await Service.CurrentVersion();
    }
}