﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{
    [Area("work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    [Authorize]

    public class ChartReportController : BaseController<Station, IChartReportService>
    {
        public ChartReportController(IChartReportService service)
          : base(service)
        {
        }

        [HttpGet("GetInfoBoxData")]
        public async Task<object> GetInfoBoxData() => await Service.GetInfoBoxData();

        [HttpGet("CommonInfo")]
        public async Task<object> CommonInfo(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.CommonInfo(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("GrowthChartData")]
        public async Task<object> GrowthChartData(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.GrowthChartData(projectId, typeTimeView, fromDate, toDate);

        #region [Customer Chart]
        [HttpGet("CustomerChartQuantity")]
        public async Task<object> CustomerChartQuantity(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.CustomerChartQuantity(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("CustomerChartQuantityAverage")]
        public async Task<object> CustomerChartQuantityAverage(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.CustomerChartQuantityAverage(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("CustomerChartRangeAgeTrend")]
        public async Task<object> CustomerChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.CustomerChartRangeAgeTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("CustomerChartGenderTrend")]
        public async Task<object> CustomerChartGenderTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.CustomerChartGenderTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("CustomerChartDayOfWeekTrend")]
        public async Task<object> CustomerChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.CustomerChartDayOfWeekTrend(projectId, typeTimeView, fromDate, toDate);
        #endregion

        #region [Finance Chart]
        [HttpGet("FinanceChartRechargeSum")]
        public async Task<object> FinanceChartRechargeSum(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.FinanceChartRechargeSum(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("FinanceChartRechargeSumAverage")]
        public async Task<object> FinanceChartRechargeSumAverage(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.FinanceChartRechargeSumAverage(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("FinanceChartRangeAgeTrend")]
        public async Task<object> FinanceChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.FinanceChartRangeAgeTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("FinanceChartRangeHourTrend")]
        public async Task<object> FinanceChartRangeHourTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.FinanceChartRangeHourTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("FinanceChartRechargeChanelTrend")]
        public async Task<object> FinanceChartRechargeChanelTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.FinanceChartRechargeChanelTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("FinanceChartRechargeLevelTrend")]
        public async Task<object> FinanceChartRechargeLevelTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.FinanceChartRechargeLevelTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("FinanceChartDayOfWeekTrend")]
        public async Task<object> FinanceChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.FinanceChartDayOfWeekTrend(projectId, typeTimeView, fromDate, toDate);
        #endregion

        #region [Transaction Chart]
        [HttpGet("TransactionChartQuantity")]
        public async Task<object> TransactionChartQuantity(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.TransactionChartQuantity(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("TransactionChartQuantityAverage")]
        public async Task<object> TransactionChartQuantityAverage(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.TransactionChartQuantityAverage(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("TransactionChartTypeTicketTrend")]
        public async Task<object> TransactionChartTypeTicketTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.TransactionChartTypeTicketTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("TransactionChartRangeHourTrend")]
        public async Task<object> TransactionChartRangeHourTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.TransactionChartRangeHourTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("TransactionChartRangeAgeTrend")]
        public async Task<object> TransactionChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.TransactionChartRangeAgeTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("TransactionChartTripTimeTrend")]
        public async Task<object> TransactionChartTripTimeTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.TransactionChartTripTimeTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("TransactionChartTripDistanceTrend")]
        public async Task<object> TransactionChartTripDistanceTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.TransactionChartTripDistanceTrend(projectId, typeTimeView, fromDate, toDate);

        [HttpGet("TransactionChartDayOfWeekTrend")]
        public async Task<object> TransactionChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.TransactionChartDayOfWeekTrend(projectId, typeTimeView, fromDate, toDate);
        #endregion

        #region [Station Chart]
        [HttpGet("GetStations")]
        public async Task<object> GetStations(int? projectId) => await Service.GetStations(projectId);

        [HttpGet("StationChartData")]
        public async Task<object> StationChartData(int? projectId, string typeTimeView, string typeView, int stationId, DateTime fromDate, DateTime toDate) => await Service.StationChartData(projectId, typeTimeView, typeView, stationId, fromDate, toDate);
        #endregion

        #region [Bike Chart]
        [HttpGet("BikeChartData")]
        public async Task<object> BikeChartData(int? projectId, string typeTimeView, DateTime fromDate, DateTime toDate) => await Service.BikeChartData(projectId, typeTimeView, fromDate, toDate);
        #endregion
    }
}