﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ReportTransactionController : Controller
    {
        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IBikeRepository _iBikeRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;


        public ReportTransactionController(
            ILogger<ReportTransactionController> logger,
            ILogRepository iLogRepository,
            IWebHostEnvironment iWebHostEnvironment,

            IBikeRepository iBikeRepository,
            IUserRepository iUserRepository,
            IDockRepository iDockRepository,
            IAccountRepository iAccountRepository,
            IProjectRepository iProjectRepository,
            IStationRepository iStationRepository,
            IBookingRepository iBookingRepository,
            IInvestorRepository iInvestorRepository,
            ITransactionRepository iTransactionRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IWalletTransactionRepository iWalletTransactionRepository
            ) : base()
        {

            _logger = logger;
            _iLogRepository = iLogRepository;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iBikeRepository = iBikeRepository;
            _iUserRepository = iUserRepository;
            _iDockRepository = iDockRepository;
            _iAccountRepository = iAccountRepository;
            _iProjectRepository = iProjectRepository;
            _iStationRepository = iStationRepository;
            _iBookingRepository = iBookingRepository;
            _iInvestorRepository = iInvestorRepository;
            _iTransactionRepository = iTransactionRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
        }

        #region [Transaction]
        [AuthorizePermission("Index")]
        [HttpPost("Transaction")]
        public async Task<string> Transaction(DateTime? from, DateTime? to, int? investorId, int? projectId, int? customerGroupId, EBookingStatus? status, int? stationIn, int? stationOut, int? accountId, ETicketType? ticketPrice_TicketType, string key)
        {
            var transactions = await _iTransactionRepository.Search(
                x => (key == null || x.TransactionCode.Contains(key))
                    && (investorId == null || x.InvestorId == investorId)
                    && (projectId == null || x.ProjectId == projectId)
                    && (stationIn == null || x.StationIn == stationIn)
                    && (accountId == null || x.AccountId == accountId)
                    && (customerGroupId == null || x.CustomerGroupId == customerGroupId)
                    && (ticketPrice_TicketType == null || x.TicketPrice_TicketType == ticketPrice_TicketType)
                    && (from == null || x.CreatedDate >= from)
                    && (to == null || x.CreatedDate < to.Value.AddDays(1))
                    && x.Status != EBookingStatus.Debt);

            var accounts = await _iAccountRepository.Search(
                x => transactions.Select(y => y.AccountId).Distinct().Contains(x.Id),
                null,
                x => new Account
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Code = x.Code,
                    RFID = x.RFID,
                    ManagerUserId = x.ManagerUserId,
                    SubManagerUserId = x.SubManagerUserId
                });

            var customerGroups = await _iCustomerGroupRepository.Search(
                x => transactions.Select(x => x.CustomerGroupId).Distinct().Contains(x.Id),
                null,
                x => new CustomerGroup
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var bikes = await _iBikeRepository.Search(
                x => transactions.Select(x => x.BikeId).Distinct().Contains(x.Id),
                null,
                x => new Bike
                {
                    Id = x.Id,
                    Plate = x.Plate
                });

            var docks = await _iDockRepository.Search(
                x => transactions.Select(x => x.DockId).Distinct().Contains(x.Id),
                null,
                x => new Dock
                {
                    Id = x.Id,
                    IMEI = x.IMEI,
                    SerialNumber = x.SerialNumber
                });

            var stations = await _iStationRepository.Search(
                x => transactions.Select(y => y.StationIn).Distinct().Contains(x.Id)
                    || transactions.Select(y => y.StationOut).Distinct().Contains(x.Id),
                null,
                x => new Station
                {
                    Id = x.Id,
                    Name = x.Name,
                    DisplayName = x.DisplayName
                });

            var users = await _iUserRepository.Search(
                x => accounts.Select(y => y.ManagerUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.SubManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            var projects = await _iProjectRepository.Search(
                x => transactions.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => transactions.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            foreach (var item in transactions)
            {
                item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                item.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);

                if (item.Account != null)
                {
                    item.Account.ManagerUserCode = users.FirstOrDefault(x => x.Id == item.Account.ManagerUserId)?.Code;
                    item.Account.SubManagerUserCode = users.FirstOrDefault(x => x.Id == item.Account.SubManagerUserId)?.Code;
                }

                item.Project = projects.FirstOrDefault(x => x.Id == item.ProjectId);
                item.Investor = investors.FirstOrDefault(x => x.Id == item.ProjectId);
                item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                item.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                item.ObjStationIN = stations.FirstOrDefault(x => x.Id == item.StationIn);
                item.ObjStationOut = stations.FirstOrDefault(x => x.Id == item.StationOut);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("Chuyến đi hoàn thành");
                ExcelWorksheet worksheet = MyExcel.Workbook.Worksheets[0];

                FormatTransactionExcel(worksheet, transactions, from, to);

                MyExcel.Save();
            }
            ////
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatTransactionExcel(ExcelWorksheet worksheet, List<Transaction> transactions, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1, 6}, {2, 24}, {3, 14}, {4, 14}, {5, 14},
                {6, 13}, {7, 25},{8, 15}, {9, 18}, {10, 25},
                {11, 25}, {12, 17},{13, 17}, {14, 10}, {15, 20},
                {16, 13}, {17, 11},{18, 11}, {19, 11}, {20, 11},
                {21, 11}, {22, 15},{23, 15}, {24, 30}, {25, 30},
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 23, 22, 16, 15, 13, 12, 9, 8, 7, 5, 4, 3 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 14, 17, 18, 19, 20, 21 });
            SetNumberAsText(worksheet, new int[] { 3 });

            // Title
            worksheet.Cells["B2:Y2"].Merge = true;
            worksheet.Cells["B2:Y2"].Style.Font.Bold = true;
            worksheet.Cells["B2:Y2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B2:Y2"].Value = $"BÁO CÁO CHUYẾN ĐI HOÀN THÀNH";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:Y3"].Merge = true;
            worksheet.Cells["B3:Y3"].Style.Font.Italic = true;
            worksheet.Cells["B3:Y3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:Y3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:Y5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:Y5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:Y5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:Y5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells[5, 1].Value = "STT";
            worksheet.Cells[5, 2].Value = "Tên KH";
            worksheet.Cells[5, 3].Value = "SĐT";
            worksheet.Cells[5, 4].Value = "Mã KH";
            worksheet.Cells[5, 5].Value = "Mã RFID";
            worksheet.Cells[5, 6].Value = "Nhóm KH";
            worksheet.Cells[5, 7].Value = "Mã chuyến đi";
            worksheet.Cells[5, 8].Value = "Biển số xe";
            worksheet.Cells[5, 9].Value = "IMEI khóa";
            worksheet.Cells[5, 10].Value = "Từ trạm";
            worksheet.Cells[5, 11].Value = "Đến trạm";
            worksheet.Cells[5, 12].Value = "Thời gian bắt đầu";
            worksheet.Cells[5, 13].Value = "Thời gian kết thúc";
            worksheet.Cells[5, 14].Value = "Số phút thuê";
            worksheet.Cells[5, 15].Value = "Trạng thái";
            worksheet.Cells[5, 16].Value = "Loại vé";
            worksheet.Cells[5, 17].Value = "Giá vé";
            worksheet.Cells[5, 18].Value = "Phí điểm gốc";
            worksheet.Cells[5, 19].Value = "Phí điểm KM";
            worksheet.Cells[5, 20].Value = "Tổng phí";
            worksheet.Cells[5, 21].Value = "Tích điểm";
            worksheet.Cells[5, 22].Value = "Mã CTV/NV";
            worksheet.Cells[5, 23].Value = "Mã Quản lý";
            worksheet.Cells[5, 24].Value = "Đơn vị đầu tư";
            worksheet.Cells[5, 25].Value = "Dự án";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1; // row start loop data table

            if (transactions.Any())
            {
                foreach (var item in transactions)
                {
                    worksheet.Cells[rs, 1].Value = stt;
                    worksheet.Cells[rs, 2].Value = item.Account?.FullName;
                    worksheet.Cells[rs, 3].Value = item.Account?.Phone;
                    worksheet.Cells[rs, 4].Value = item.Account?.Code;
                    worksheet.Cells[rs, 5].Value = item.Account?.RFID;
                    worksheet.Cells[rs, 6].Value = item.CustomerGroup?.Name;
                    worksheet.Cells[rs, 7].Value = item.TransactionCode;
                    worksheet.Cells[rs, 8].Value = item.Bike?.Plate;
                    worksheet.Cells[rs, 9].Value = item.Dock?.IMEI;
                    worksheet.Cells[rs, 10].Value = item.ObjStationIN?.DisplayName;
                    worksheet.Cells[rs, 11].Value = item.ObjStationOut?.DisplayName;
                    worksheet.Cells[rs, 12].Value = item.StartTime.ToString("dd/MM/yyyy HH:mm");
                    worksheet.Cells[rs, 13].Value = item.EndTime?.ToString("dd/MM/yyyy HH:mm");
                    worksheet.Cells[rs, 14].Value = item.TotalMinutes;
                    worksheet.Cells[rs, 15].Value = item.Status.ToEnumGetDisplayName();
                    worksheet.Cells[rs, 16].Value = item.TicketPrice_TicketType.ToEnumGetDisplayName();
                    worksheet.Cells[rs, 17].Value = item.TicketPrice_TicketValue;
                    worksheet.Cells[rs, 18].Value = item.ChargeInAccount;
                    worksheet.Cells[rs, 19].Value = item.ChargeInSubAccount;
                    worksheet.Cells[rs, 20].Value = item.TotalPrice;
                    worksheet.Cells[rs, 21].Value = item.TripPoint;
                    worksheet.Cells[rs, 22].Value = item.Account?.SubManagerUserCode;
                    worksheet.Cells[rs, 23].Value = item.Account?.ManagerUserCode;
                    worksheet.Cells[rs, 24].Value = item.Investor?.Name;
                    worksheet.Cells[rs, 25].Value = item.Project?.Name;
                    //
                    stt++;
                    rs++;
                }
            }

            SetBorderTable(worksheet, $"A5:Y{rs - 1}");
            worksheet.Cells[$"A6:Y{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [Booking]
        [AuthorizePermission("Index")]
        [HttpPost("Booking")]
        public async Task<string> Booking(string key, int? investorId, int? projectId, int? customerGroupId, int? stationIn, int? accountId, ETicketType? ticketPrice_TicketType)
        {
            var bookings = await _iBookingRepository.Search(
                x => (key == null || x.TransactionCode.Contains(key))
                    && (investorId == null || x.InvestorId == investorId)
                    && (projectId == null || x.ProjectId == projectId)
                    && (stationIn == null || x.StationIn == stationIn)
                    && (accountId == null || x.AccountId == accountId)
                    && (customerGroupId == null || x.CustomerGroupId == customerGroupId)
                    && (ticketPrice_TicketType == null || x.TicketPrice_TicketType == ticketPrice_TicketType)
                );

            var accounts = await _iAccountRepository.Search(
                x => bookings.Select(y => y.AccountId).Distinct().Contains(x.Id),
                null,
                x => new Account
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Code = x.Code,
                    RFID = x.RFID,
                    ManagerUserId = x.ManagerUserId,
                    SubManagerUserId = x.SubManagerUserId
                });

            var customerGroups = await _iCustomerGroupRepository.Search(
                x => bookings.Select(x => x.CustomerGroupId).Distinct().Contains(x.Id),
                null,
                x => new CustomerGroup
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var bikes = await _iBikeRepository.Search(
                x => bookings.Select(x => x.BikeId).Distinct().Contains(x.Id),
                null,
                x => new Bike
                {
                    Id = x.Id,
                    Plate = x.Plate
                });

            var docks = await _iDockRepository.Search(
                x => bookings.Select(x => x.DockId).Distinct().Contains(x.Id),
                null,
                x => new Dock
                {
                    Id = x.Id,
                    IMEI = x.IMEI,
                    SerialNumber = x.SerialNumber
                });

            var stations = await _iStationRepository.Search(
                x => bookings.Select(y => y.StationIn).Distinct().Contains(x.Id)
                    || bookings.Select(y => y.StationOut).Distinct().Contains(x.Id),
                null,
                x => new Station
                {
                    Id = x.Id,
                    Name = x.Name,
                    DisplayName = x.DisplayName
                });

            var users = await _iUserRepository.Search(
                x => accounts.Select(y => y.ManagerUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.SubManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            var projects = await _iProjectRepository.Search(
                x => bookings.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => bookings.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            foreach (var item in bookings)
            {
                item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                item.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);

                if (item.Account != null)
                {
                    item.Account.ManagerUserCode = users.FirstOrDefault(x => x.Id == item.Account.ManagerUserId)?.Code;
                    item.Account.SubManagerUserCode = users.FirstOrDefault(x => x.Id == item.Account.SubManagerUserId)?.Code;
                }

                item.Project = projects.FirstOrDefault(x => x.Id == item.ProjectId);
                item.Investor = investors.FirstOrDefault(x => x.Id == item.ProjectId);
                item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                item.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                item.ObjStationIn = stations.FirstOrDefault(x => x.Id == item.StationIn);
                item.ObjStationOut = stations.FirstOrDefault(x => x.Id == item.StationOut);
                item.TotalPrice = _iTransactionRepository
                                    .ToTotalPrice(item.TicketPrice_TicketType,
                                                    item.StartTime,
                                                    DateTime.Now,
                                                    item.TicketPrice_TicketValue,
                                                    item.TicketPrice_LimitMinutes,
                                                    item.TicketPrice_BlockPerMinute,
                                                    item.TicketPrice_BlockPerViolation,
                                                    item.TicketPrice_BlockViolationValue,
                                                    item.Prepaid_EndTime ?? DateTime.Now,
                                                    item.Prepaid_MinutesSpent,
                                                    new DiscountCode { Percent = item.DiscountCode_Percent ?? 0, Point = item.DiscountCode_Point ?? 0, Type = item.DiscountCode_Type ?? EDiscountCodeType.Point }
                                                )?.Price ?? 0;
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("Chuyến đi đang thuê");
                ExcelWorksheet worksheet = MyExcel.Workbook.Worksheets[0];

                FormatBookingExcel(worksheet, bookings);

                MyExcel.Save();
            }
            ////
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatBookingExcel(ExcelWorksheet worksheet, IEnumerable<Booking> bookings)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Style Columns
            worksheet.Column(14).Style.Font.Bold = true;
            worksheet.Column(20).Style.Font.Bold = true;

            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 24}, {3 , 14}, {4 , 14}, {5 , 14},
                {6 , 13}, {7 , 25}, {8 , 15}, {9 , 18}, {10, 25},
                {11, 25}, {12, 17}, {13, 17}, {14, 10}, {15, 20},
                {16, 13}, {17, 12}, {18, 12}, {19, 12}, {20, 12},
                {21, 12}, {22, 15}, {23, 15}, {24, 30}, {25, 30},
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 23, 22, 16, 15, 13, 12, 9, 8, 7, 5, 4, 3 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 14, 17, 18, 19, 20, 21 });
            SetNumberAsText(worksheet, new int[] { 3 });

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO CHUYẾN ĐI ĐANG THUÊ";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:D3"].Value = $"Ghi nhận tại thời điểm {DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:Y5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:Y5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:Y5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:Y5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells[5, 1].Value = "STT";
            worksheet.Cells[5, 2].Value = "Tên KH";
            worksheet.Cells[5, 3].Value = "SĐT";
            worksheet.Cells[5, 4].Value = "Mã KH";
            worksheet.Cells[5, 5].Value = "Mã RFID";
            worksheet.Cells[5, 6].Value = "Nhóm KH";
            worksheet.Cells[5, 7].Value = "Mã chuyến đi";
            worksheet.Cells[5, 8].Value = "Biển số xe";
            worksheet.Cells[5, 9].Value = "IMEI khóa";
            worksheet.Cells[5, 10].Value = "Từ trạm";
            worksheet.Cells[5, 11].Value = "Đến trạm";
            worksheet.Cells[5, 12].Value = "Thời gian bắt đầu";
            worksheet.Cells[5, 13].Value = "Thời gian kết thúc";
            worksheet.Cells[5, 14].Value = "Số phút thuê";
            worksheet.Cells[5, 15].Value = "Trạng thái";
            worksheet.Cells[5, 16].Value = "Loại vé";
            worksheet.Cells[5, 17].Value = "Giá vé";
            worksheet.Cells[5, 18].Value = "Phí điểm gốc";
            worksheet.Cells[5, 19].Value = "Phí điểm KM";
            worksheet.Cells[5, 20].Value = "Tổng phí";
            worksheet.Cells[5, 21].Value = "Tích điểm";
            worksheet.Cells[5, 22].Value = "Mã CTV/NV";
            worksheet.Cells[5, 23].Value = "Mã Quản lý";
            worksheet.Cells[5, 24].Value = "Đơn vị đầu tư";
            worksheet.Cells[5, 25].Value = "Dự án";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1; // row start loop data table

            if (bookings.Any())
            {
                foreach (var item in bookings)
                {
                    worksheet.Cells[rs, 1].Value = stt;
                    worksheet.Cells[rs, 2].Value = item.Account?.FullName;
                    worksheet.Cells[rs, 3].Value = item.Account?.Phone;
                    worksheet.Cells[rs, 4].Value = item.Account?.Code;
                    worksheet.Cells[rs, 5].Value = item.Account?.RFID;
                    worksheet.Cells[rs, 6].Value = item.CustomerGroup?.Name;
                    worksheet.Cells[rs, 7].Value = item.TransactionCode;
                    worksheet.Cells[rs, 8].Value = item.Bike?.Plate;
                    worksheet.Cells[rs, 9].Value = item.Dock?.IMEI;
                    worksheet.Cells[rs, 10].Value = item.ObjStationIn?.DisplayName;
                    worksheet.Cells[rs, 11].Value = item.ObjStationOut?.DisplayName;
                    worksheet.Cells[rs, 12].Value = item.StartTime.ToString("dd/MM/yyyy HH:mm");
                    worksheet.Cells[rs, 13].Value = item.EndTime?.ToString("dd/MM/yyyy HH:mm");
                    worksheet.Cells[rs, 14].Value = Convert.ToInt32((DateTime.Now - item.StartTime).TotalMinutes);
                    worksheet.Cells[rs, 15].Value = item.Status.ToEnumGetDisplayName();
                    worksheet.Cells[rs, 16].Value = item.TicketPrice_TicketType.ToEnumGetDisplayName();
                    worksheet.Cells[rs, 17].Value = item.TicketPrice_TicketValue;
                    worksheet.Cells[rs, 18].Value = item.ChargeInAccount;
                    worksheet.Cells[rs, 19].Value = item.ChargeInSubAccount;
                    worksheet.Cells[rs, 20].Value = item.TotalPrice;
                    worksheet.Cells[rs, 21].Value = item.TripPoint;
                    worksheet.Cells[rs, 22].Value = item.Account?.SubManagerUserCode;
                    worksheet.Cells[rs, 23].Value = item.Account?.ManagerUserCode;
                    worksheet.Cells[rs, 24].Value = item.Investor?.Name;
                    worksheet.Cells[rs, 25].Value = item.Project?.Name;
                    //
                    stt++;
                    rs++;
                }
            }

            SetBorderTable(worksheet, $"A5:Y{rs - 1}");
            worksheet.Cells[$"N6:N{rs}"].Style.Font.Color.SetColor(System.Drawing.Color.Red);
            worksheet.Cells[$"T6:T{rs}"].Style.Font.Color.SetColor(System.Drawing.Color.Red);

            worksheet.Cells[$"A6:Y{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [TransactionDebt]
        [AuthorizePermission("Index")]
        [HttpPost("TransactionDebt")]
        public async Task<string> TransactionDebt(string key, int? investorId, int? projectId, int? customerGroupId, EBookingStatus? status, int? stationIn, int? stationOut, int? accountId, ETicketType? ticketPrice_TicketType, DateTime? from, DateTime? to)
        {
            var transactions = await _iTransactionRepository.Search(
                x => (key == null || x.TransactionCode.Contains(key))
                    && (investorId == null || x.InvestorId == investorId)
                    && (projectId == null || x.ProjectId == projectId)
                    && (stationIn == null || x.StationIn == stationIn)
                    && (accountId == null || x.AccountId == accountId)
                    && (customerGroupId == null || x.CustomerGroupId == customerGroupId)
                    && (ticketPrice_TicketType == null || x.TicketPrice_TicketType == ticketPrice_TicketType)
                    && (from == null || x.CreatedDate >= from)
                    && (to == null || x.CreatedDate < to.Value.AddDays(1))
                    && (x.Status == EBookingStatus.Debt));

            var walletTransactions = await _iWalletTransactionRepository.Search(
                x => transactions.Select(y => y.Id).Contains(x.TransactionId),
                null,
                x => new WalletTransaction
                {
                    Id = x.Id,
                    TransactionId = x.TransactionId,
                    Wallet_Balance = x.Wallet_Balance,
                    Wallet_SubBalance = x.Wallet_SubBalance
                });

            var accounts = await _iAccountRepository.Search(
                x => transactions.Select(y => y.AccountId).Distinct().Contains(x.Id),
                null,
                x => new Account
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Code = x.Code,
                    RFID = x.RFID,
                    ManagerUserId = x.ManagerUserId,
                    SubManagerUserId = x.SubManagerUserId
                });

            var customerGroups = await _iCustomerGroupRepository.Search(
                x => transactions.Select(x => x.CustomerGroupId).Distinct().Contains(x.Id),
                null,
                x => new CustomerGroup
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var bikes = await _iBikeRepository.Search(
                x => transactions.Select(x => x.BikeId).Distinct().Contains(x.Id),
                null,
                x => new Bike
                {
                    Id = x.Id,
                    Plate = x.Plate
                });

            var docks = await _iDockRepository.Search(
                x => transactions.Select(x => x.DockId).Distinct().Contains(x.Id),
                null,
                x => new Dock
                {
                    Id = x.Id,
                    IMEI = x.IMEI,
                    SerialNumber = x.SerialNumber
                });

            var stations = await _iStationRepository.Search(
                x => transactions.Select(y => y.StationIn).Distinct().Contains(x.Id)
                    || transactions.Select(y => y.StationOut).Distinct().Contains(x.Id),
                null,
                x => new Station
                {
                    Id = x.Id,
                    Name = x.Name,
                    DisplayName = x.DisplayName
                });

            var users = await _iUserRepository.Search(
                x => accounts.Select(y => y.ManagerUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.SubManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            var projects = await _iProjectRepository.Search(
                x => transactions.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => transactions.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            foreach (var item in transactions)
            {
                item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                item.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);

                if (item.Account != null)
                {
                    item.Account.ManagerUserCode = users.FirstOrDefault(x => x.Id == item.Account.ManagerUserId)?.Code;
                    item.Account.SubManagerUserCode = users.FirstOrDefault(x => x.Id == item.Account.SubManagerUserId)?.Code;
                }

                item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                item.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                item.Project = projects.FirstOrDefault(x => x.Id == item.ProjectId);
                item.Investor = investors.FirstOrDefault(x => x.Id == item.ProjectId);
                item.ObjStationIN = stations.FirstOrDefault(x => x.Id == item.StationIn);
                item.ObjStationOut = stations.FirstOrDefault(x => x.Id == item.StationOut);
                item.Wallet_AccountBlance = walletTransactions.FirstOrDefault(x => x.TransactionId == item.Id).Wallet_Balance;
                item.Wallet_SubAccountBlance = walletTransactions.FirstOrDefault(x => x.TransactionId == item.Id).Wallet_SubBalance;
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("Chuyến đi nợ cước");
                ExcelWorksheet worksheet = MyExcel.Workbook.Worksheets[0];

                FormatTransactionDebtExcel(worksheet, transactions, from, to);

                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatTransactionDebtExcel(ExcelWorksheet worksheet, List<Transaction> transactions, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Style Columns
            worksheet.Column(26).Style.Font.Bold = true;

            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 24}, {3 , 14}, {4 , 14}, {5 , 14},
                {6 , 13}, {7 , 25}, {8 , 15}, {9 , 18}, {10, 25},
                {11, 25}, {12, 17}, {13, 17}, {14, 10}, {15, 20},
                {16, 13}, {17, 11}, {18, 11}, {19, 11}, {20, 11},
                {21, 11}, {22, 11}, {23, 11}, {24, 11}, {25, 11},
                {26, 11}, {27, 15}, {28, 15}, {29, 30}, {30, 30}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 28, 27, 16, 15, 13, 12, 9, 8, 7, 5, 4, 3 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });
            SetNumberAsText(worksheet, new int[] { 3 });

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO CHUYẾN ĐI NỢ CƯỚC";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:D3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:A6"].Merge = true;
            worksheet.Cells["A5:A6"].Value = "STT";

            worksheet.Cells["B5:B6"].Merge = true;
            worksheet.Cells["B5:B6"].Value = "Tên KH";

            worksheet.Cells["C5:C6"].Merge = true;
            worksheet.Cells["C5:C6"].Value = "SĐT";

            worksheet.Cells["D5:D6"].Merge = true;
            worksheet.Cells["D5:D6"].Value = "Mã KH";

            worksheet.Cells["E5:E6"].Merge = true;
            worksheet.Cells["E5:E6"].Value = "Mã RFID";

            worksheet.Cells["F5:F6"].Merge = true;
            worksheet.Cells["F5:F6"].Value = "Nhóm KH";

            worksheet.Cells["G5:G6"].Merge = true;
            worksheet.Cells["G5:G6"].Value = "Mã chuyến đi";

            worksheet.Cells["H5:H6"].Merge = true;
            worksheet.Cells["H5:H6"].Value = "Biển số xe";

            worksheet.Cells["I5:I6"].Merge = true;
            worksheet.Cells["I5:I6"].Value = "IMEI khóa";

            worksheet.Cells["J5:J6"].Merge = true;
            worksheet.Cells["J5:J6"].Value = "Từ trạm";

            worksheet.Cells["K5:K6"].Merge = true;
            worksheet.Cells["K5:K6"].Value = "Đến trạm";

            worksheet.Cells["L5:L6"].Merge = true;
            worksheet.Cells["L5:L6"].Value = "Thời gian bắt đầu";

            worksheet.Cells["M5:M6"].Merge = true;
            worksheet.Cells["M5:M6"].Value = "Thời gian kết thúc";

            worksheet.Cells["N5:N6"].Merge = true;
            worksheet.Cells["N5:N6"].Value = "Số phút thuê";

            worksheet.Cells["O5:O6"].Merge = true;
            worksheet.Cells["O5:O6"].Value = "Trạng thái";

            worksheet.Cells["P5:P6"].Merge = true;
            worksheet.Cells["P5:P6"].Value = "Loại vé";

            worksheet.Cells["Q5:Q6"].Merge = true;
            worksheet.Cells["Q5:Q6"].Value = "Giá vé";

            worksheet.Cells["R5:R6"].Merge = true;
            worksheet.Cells["R5:R6"].Value = "Tổng phí";

            worksheet.Cells["S5:S6"].Merge = true;
            worksheet.Cells["S5:S6"].Value = "Tích điểm";

            worksheet.Cells["T5:U5"].Merge = true;
            worksheet.Cells["T5:U5"].Value = "Tồn trước giao dịch";
            worksheet.Cells[6, 20].Value = "Tài khoản gốc";
            worksheet.Cells[6, 21].Value = "Tài khoản KM";

            worksheet.Cells["V5:W5"].Merge = true;
            worksheet.Cells["V5:W5"].Value = "Giao dịch ước tính";
            worksheet.Cells[6, 22].Value = "Phí điểm gốc"; // Ước tính
            worksheet.Cells[6, 23].Value = "Phí điểm KM";

            worksheet.Cells["X5:Y5"].Merge = true;
            worksheet.Cells["X5:Y5"].Value = "Giao dịch thực tế";
            worksheet.Cells[6, 24].Value = "Phí điểm gốc"; // Thực tế
            worksheet.Cells[6, 25].Value = "Phí điểm KM";

            worksheet.Cells["Z5:Z6"].Merge = true;
            worksheet.Cells["Z5:Z6"].Value = "Số tiền nợ cước";

            worksheet.Cells["AA5:AA6"].Merge = true;
            worksheet.Cells["AA5:AA6"].Value = "Mã CTV/NV";

            worksheet.Cells["AB5:AB6"].Merge = true;
            worksheet.Cells["AB5:AB6"].Value = "Mã Quản lý";

            worksheet.Cells["AC5:AC6"].Merge = true;
            worksheet.Cells["AC5:AC6"].Value = "Đơn vị đầu tư";

            worksheet.Cells["AD5:AD6"].Merge = true;
            worksheet.Cells["AD5:AD6"].Value = "Dự án";

            worksheet.Cells["A5:AD5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:AD5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:AD5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:AD5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["T6:Y6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["T6:Y6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["T6:Y6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["T6:Y6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            // Binding data 
            var rs = 7; // row start loop data table

            if (transactions.Any())
            {
                foreach (var item in transactions)
                {
                    worksheet.Cells[rs, 1].Value = rs - 6;
                    worksheet.Cells[rs, 2].Value = item.Account?.FullName;
                    worksheet.Cells[rs, 3].Value = item.Account?.Phone;
                    worksheet.Cells[rs, 4].Value = item.Account?.Code;
                    worksheet.Cells[rs, 5].Value = item.Account?.RFID;
                    worksheet.Cells[rs, 6].Value = item.CustomerGroup?.Name;
                    worksheet.Cells[rs, 7].Value = item.TransactionCode;
                    worksheet.Cells[rs, 8].Value = item.Bike?.Plate;
                    worksheet.Cells[rs, 9].Value = item.Dock?.IMEI;
                    worksheet.Cells[rs, 10].Value = item.ObjStationIN?.DisplayName;
                    worksheet.Cells[rs, 11].Value = item.ObjStationOut?.DisplayName;
                    worksheet.Cells[rs, 12].Value = item.StartTime.ToString("dd/MM/yyyy HH:mm");
                    worksheet.Cells[rs, 13].Value = item.EndTime?.ToString("dd/MM/yyyy HH:mm");
                    worksheet.Cells[rs, 14].Value = item.TotalMinutes;
                    worksheet.Cells[rs, 15].Value = item.Status.ToEnumGetDisplayName();
                    worksheet.Cells[rs, 16].Value = item.TicketPrice_TicketType.ToEnumGetDisplayName();
                    worksheet.Cells[rs, 17].Value = item.TicketPrice_TicketValue;
                    worksheet.Cells[rs, 18].Value = item.TotalPrice;
                    worksheet.Cells[rs, 19].Value = item.TripPoint;
                    worksheet.Cells[rs, 20].Value = item.Wallet_AccountBlance;
                    worksheet.Cells[rs, 21].Value = item.Wallet_SubAccountBlance;
                    worksheet.Cells[rs, 22].Value = item.EstimateChargeInAccount;
                    worksheet.Cells[rs, 23].Value = item.EstimateChargeInSubAccount;
                    worksheet.Cells[rs, 24].Value = item.ChargeInAccount;
                    worksheet.Cells[rs, 25].Value = item.ChargeInSubAccount;
                    worksheet.Cells[rs, 26].Value = item.ChargeDebt;
                    worksheet.Cells[rs, 27].Value = item.Account?.SubManagerUserCode;
                    worksheet.Cells[rs, 28].Value = item.Account?.ManagerUserCode;
                    worksheet.Cells[rs, 29].Value = item.Investor?.Name;
                    worksheet.Cells[rs, 30].Value = item.Project?.Name;
                    //
                    rs++;
                }
            }
            //
            SetBorderTable(worksheet, $"A5:AD{rs - 1}");
            worksheet.Cells[$"A7:AD{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells[$"Z7:Z{rs}"].Style.Font.Color.SetColor(System.Drawing.Color.Red);
        }
        #endregion

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string, string> InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
    }
}