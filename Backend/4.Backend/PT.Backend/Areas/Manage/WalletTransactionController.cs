using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;

namespace TN.Backend.Areas.Manage
{    
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class WalletTransactionsController : BaseController<WalletTransaction, IWalletTransactionService>
    { 
        public WalletTransactionsController(IWalletTransactionService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int pageIndex,
            int pageSize,
            string key,
            int? projectId,
            int? accountId,
            EPaymentGroup? paymentGroup,
            EWalletTransactionType? type,
            EWalletTransactionStatus? status,
            DateTime? start,
            DateTime? end,
            string sortby,
            string sorttype
        ) 
        => await Service.SearchPageAsync(pageIndex, pageSize, key, projectId, accountId, paymentGroup, type, status, start, end, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Add")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] WalletTransactionCommand value) => await Service.Create(value);

        [AuthorizePermission("Arrears")]
        [HttpPost("Arrears")]
        public async Task<object> Arrears([FromBody] WalletTransactionArrearsCommand value) => await Service.Arrears(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] WalletTransactionEditCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await Service.SearchByAccount(key);

        [HttpPost("Upload")]
        [AuthorizePermission("Edit")]
        public async Task<object> Upload() => await Service.Upload(Request);

        [AuthorizePermission("Share")]
        [HttpPost("Gift")]
        public async Task<object> Gift([FromBody] WalletTransactionGiftCommand value) => await Service.Gift(value);
    }
}