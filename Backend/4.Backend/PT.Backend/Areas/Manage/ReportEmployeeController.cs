﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ReportEmployeeController : Controller
    {
        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        private readonly IUserRepository _iUserRepository;

        public ReportEmployeeController(
            ILogger<ReportEmployeeController> logger,
            ILogRepository iLogRepository,
            IWebHostEnvironment iWebHostEnvironment,

            IUserRepository iUserRepository
            ) : base()
        {

            _logger = logger;
            _iLogRepository = iLogRepository;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iUserRepository = iUserRepository;
        }

        #region [NVVTS]
        [AuthorizePermission("Index")]
        [HttpPost("NVVTS")]
        public async Task<string> NVVTS(
            DateTime? from,
            DateTime? to)
        {
            var employees = await _iUserRepository.Search(
                x => (x.RoleType == RoleManagerType.BOD_VTS || x.RoleType == RoleManagerType.KTT_VTS || x.RoleType == RoleManagerType.CSKH_VTS || x.RoleType == RoleManagerType.Coordinator_VTS)
                    && (from == null || x.CreatedDate >= from)
                    && (to == null || x.CreatedDate < to.Value.AddDays(1))
                );

            var admins = await _iUserRepository.Search(
                x => employees.Select(y => y.CreatedUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Code = x.Code
                });

            foreach (var item in employees)
            {
                item.CreatedUserObject = admins.FirstOrDefault(x => x.Id == item.CreatedUserId);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC NVVTS");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelNVVTS(workSheet, employees, from, to);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelNVVTS(ExcelWorksheet worksheet, List<ApplicationUser> items, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 25}, {3 , 15}, {4 , 15}, {5 , 38},
                {6 , 10}, {7 , 15}, {8 , 15}, {9 , 15}, {10, 15},
                {11, 40}, {12, 40}, {13, 40}, {14, 15}, {15, 15},
                {16, 25}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 3, 4, 7, 9, 10, 15 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:F2"].Merge = true;
            worksheet.Cells["B2:F2"].Style.Font.Bold = true;
            worksheet.Cells["B2:F2"].Value = $"BÁO CÁO THÔNG TIN NHÂN VIÊN VTS";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:F3"].Merge = true;
            worksheet.Cells["B3:F3"].Style.Font.Italic = true;
            worksheet.Cells["B3:F3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:P5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên NV";
            worksheet.Cells["C5"].Value = "SĐT";
            worksheet.Cells["D5"].Value = "Mã NV";
            worksheet.Cells["E5"].Value = "Chức danh";
            worksheet.Cells["F5"].Value = "Giới tính";
            worksheet.Cells["G5"].Value = "Ngày sinh";
            worksheet.Cells["H5"].Value = "Trình độ";
            worksheet.Cells["I5"].Value = "Số CCCD/HC";
            worksheet.Cells["J5"].Value = "Ngày cấp";
            worksheet.Cells["K5"].Value = "Nơi cấp";
            worksheet.Cells["L5"].Value = "Địa chỉ (CCCD)";
            worksheet.Cells["M5"].Value = "Địa chỉ thường trú";
            worksheet.Cells["N5"].Value = "Trạng thái hoạt động";
            worksheet.Cells["O5"].Value = "Ngày tạo TK";
            worksheet.Cells["P5"].Value = "Người tạo TK";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.FullName;
                worksheet.Cells[rs, 3].Value = item.PhoneNumber;
                worksheet.Cells[rs, 4].Value = item.Code;
                worksheet.Cells[rs, 5].Value = item.RoleType.ToEnumGetDisplayName();
                worksheet.Cells[rs, 6].Value = item.Sex.ToEnumGetDisplayName();
                worksheet.Cells[rs, 7].Value = item.Birthday?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 8].Value = item.Education;
                worksheet.Cells[rs, 9].Value = item.IdentificationID;
                worksheet.Cells[rs, 10].Value = item.IdentificationSupplyDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 11].Value = item.IdentificationSupplyAdress;
                worksheet.Cells[rs, 12].Value = item.IdentificationAdress;
                worksheet.Cells[rs, 13].Value = item.Address;
                worksheet.Cells[rs, 14].Value = item.IsLock ? "Khóa" : "Hoạt động";
                worksheet.Cells[rs, 15].Value = item.CreatedDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 16].Value = $"{item.CreatedUserObject?.FullName} ({item.CreatedUserObject?.Code})";
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:P{rs - 1}");
            worksheet.Cells[$"A6:P{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [CTVVTS]
        [AuthorizePermission("Index")]
        [HttpPost("CTVVTS")]
        public async Task<string> CTVVTS(
            DateTime? from,
            DateTime? to
        )
        {
            var ctvs = await _iUserRepository.Search(
                x => x.RoleType == RoleManagerType.CTV_VTS
                    && (from == null || x.CreatedDate >= from)
                    && (to == null || x.CreatedDate < to.Value.AddDays(1))
                );

            var nvqls = await _iUserRepository.Search(
                x => ctvs.Select(y => y.ManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Code = x.Code,
                    PhoneNumber = x.PhoneNumber
                });

            foreach (var item in ctvs)
            {
                item.ManagerUser = nvqls.FirstOrDefault(x => x.Id == item.ManagerUserId);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC CTV VTS");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelCTVVTS(workSheet, ctvs, from, to);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelCTVVTS(ExcelWorksheet worksheet, List<ApplicationUser> items, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 25}, {3 , 15}, {4 , 15}, {5 , 10},
                {6 , 15}, {7 , 15}, {8 , 15}, {9 , 15}, {10, 25},
                {11, 30}, {12, 30}, {13, 15}, {14, 15}, {15, 25},
                {16, 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 3, 4, 5, 6, 8, 9, 13, 14, 16 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:F2"].Merge = true;
            worksheet.Cells["B2:F2"].Style.Font.Bold = true;
            worksheet.Cells["B2:F2"].Value = $"BÁO CÁO THÔNG TIN CỘNG TÁC VIÊN VTS";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:F3"].Merge = true;
            worksheet.Cells["B3:F3"].Style.Font.Italic = true;
            worksheet.Cells["B3:F3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:P5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên NV";
            worksheet.Cells["C5"].Value = "SĐT";
            worksheet.Cells["D5"].Value = "Mã NV";
            worksheet.Cells["E5"].Value = "Giới tính";
            worksheet.Cells["F5"].Value = "Ngày sinh";
            worksheet.Cells["G5"].Value = "Trình độ";
            worksheet.Cells["H5"].Value = "Số CCCD/HC";
            worksheet.Cells["I5"].Value = "Ngày cấp";
            worksheet.Cells["J5"].Value = "Nơi cấp";
            worksheet.Cells["K5"].Value = "Địa chỉ (CCCD)";
            worksheet.Cells["L5"].Value = "Địa chỉ thường trú";
            worksheet.Cells["M5"].Value = "Trạng thái hoạt động";
            worksheet.Cells["N5"].Value = "Ngày tạo TK";
            worksheet.Cells["O5"].Value = "NV Quản lý";
            worksheet.Cells["P5"].Value = "SĐT NV Quản lý";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.FullName;
                worksheet.Cells[rs, 3].Value = item.PhoneNumber;
                worksheet.Cells[rs, 4].Value = item.Code;
                worksheet.Cells[rs, 5].Value = item.Sex.ToEnumGetDisplayName();
                worksheet.Cells[rs, 6].Value = item.Birthday?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 7].Value = item.Education;
                worksheet.Cells[rs, 8].Value = item.IdentificationID;
                worksheet.Cells[rs, 9].Value = item.IdentificationSupplyDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 10].Value = item.IdentificationSupplyAdress;
                worksheet.Cells[rs, 11].Value = item.IdentificationAdress;
                worksheet.Cells[rs, 12].Value = item.Address;
                worksheet.Cells[rs, 13].Value = item.IsLock ? "Khóa" : "Hoạt động";
                worksheet.Cells[rs, 14].Value = item.CreatedDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 15].Value = $"{item.ManagerUser?.FullName} ({item.ManagerUser?.Code})";
                worksheet.Cells[rs, 16].Value = item.ManagerUser?.PhoneNumber;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:P{rs - 1}");
            worksheet.Cells[$"A6:P{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string, string> InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
    }
}