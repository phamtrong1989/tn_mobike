using Microsoft.AspNetCore.Mvc;
using PT.Base;
using System;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ImportBillsController : BaseController<ImportBill, IImportBillService>
    {
        public ImportBillsController(IImportBillService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? PageSize, string key, string sortby, string sorttype,

            EImportBillStatus? status,
            int? warehouseId,
            DateTime? from,
            DateTime? to)
            => await Service.SearchPageAsync(pageIndex ?? 1, PageSize ?? 100, key, sortby, sorttype, status, warehouseId, from, to);


        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Create")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] ImportBillCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] ImportBillCommand value) => await Service.Edit(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Approve")]
        public async Task<object> Approve([FromBody] ImportBillCommand value) => await Service.Approve(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Cancel")]
        public async Task<object> Cancel([FromBody] ImportBillCommand value) => await Service.Cancel(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Denied")]
        public async Task<object> Denied([FromBody] ImportBillCommand value) => await Service.Denied(value);

        [AuthorizePermission("Index")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        [HttpPost("Upload")]
        [AuthorizePermission("Edit")]
        public async Task<object> Upload() => await Service.Upload(Request);

        [HttpGet("View")]
        public object View(string pathData, bool isPrivate) => Service.View(pathData, isPrivate);

        #region [Import Bill Item]
        [AuthorizePermission("Edit")]
        [HttpGet("ImportBillItemSearchPaged")]
        public async Task<object> ImportBillItemSearchPaged(int? pageIndex, int? PageSize, int importBillId, string sortby, string sorttype)
        => await Service.ImportBillItemSearchPageAsync(pageIndex ?? 1, PageSize ?? 100, importBillId, sortby, sorttype);

        [AuthorizePermission("Edit")]
        [HttpGet("ImportBillItemGetById/{id}")]
        public async Task<object> ImportBillItemGetById(int id) => await Service.ImportBillItemGetById(id);

        [AuthorizePermission("Edit")]
        [HttpGet("SearchBySupplies")]
        public async Task<object> SearchBySupplies(string key) => await Service.SearchBySupplies(key);

        [AuthorizePermission("Edit")]
        [HttpPost("ImportBillItemCreate")]
        public async Task<object> ImportBillItemCreate([FromBody] ImportBillItemCommand value) => await Service.ImportBillItemCreate(value);

        [AuthorizePermission("Edit")]
        [HttpPut("ImportBillItemEdit")]
        public async Task<object> ImportBillItemEdit([FromBody] ImportBillItemCommand value) => await Service.ImportBillItemEdit(value);

        [AuthorizePermission("Edit")]
        [HttpDelete("ImportBillItemDelete/{id}")]
        public async Task<object> ImportBillItemDelete(int id) => await Service.ImportBillItemDelete(id);
        #endregion
    }
}