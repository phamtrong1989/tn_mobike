﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{
    [Area("work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    [Authorize]

    public class TroubleshootingCenterController : BaseController<Station, ITroubleshootingCenterService>
    {
        public TroubleshootingCenterController(ITroubleshootingCenterService service)
          : base(service)
        {
        }

        [HttpGet("GetAnalyticRevenue")]
        public async Task<object> GetAnalyticRevenue(string type, int? projectId, DateTime statisticDate) => await Service.GetAnalyticRevenue(type, projectId, statisticDate);

        [HttpGet("GetAnalyticCashFlow")]
        public async Task<object> GetAnalyticCashFlow(string type, int? projectId, DateTime statisticDate) => await Service.GetAnalyticCashFlow(type, projectId, statisticDate);

        [HttpGet("GetAnalyticCustomer")]
        public async Task<object> GetAnalyticCustomer(string type, int? projectId, DateTime statisticDate) => await Service.GetAnalyticCustomer(type, projectId, statisticDate);

        [HttpGet("GetAnalyticTransaction")]
        public async Task<object> GetAnalyticTransaction(string type, int? projectId, DateTime statisticDate) => await Service.GetAnalyticTransaction(type, projectId, statisticDate);

        [HttpGet("GetAnalyticBike")]
        public async Task<object> GetAnalyticBike(int? projectId) => await Service.GetAnalyticBike(projectId);

        [HttpGet("GetAnalyticDock")]
        public async Task<object> GetAnalyticDock(int? projectId) => await Service.GetAnalyticDock(projectId);
    }
}