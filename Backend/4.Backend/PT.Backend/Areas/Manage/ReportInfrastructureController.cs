﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using static TN.Domain.Model.Bike;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ReportInfrastructureController : Controller
    {
        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IBikeRepository _iBikeRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IEntityBaseRepository<City> _cityRepository;
        private readonly IEntityBaseRepository<District> _districtRepository;
        private readonly IEntityBaseRepository<Producer> _producerRepository;
        private readonly IEntityBaseRepository<Model> _modelRepository;
        private readonly IEntityBaseRepository<Supplies> _suppliesRepository;
        private readonly IEntityBaseRepository<Warehouse> _warehouseRepository;
        private readonly IEntityBaseRepository<BikeInfo> _bikeInfoRepository;
        private readonly IEntityBaseRepository<BikeReport> _bikeReportRepository;
        private readonly IEntityBaseRepository<Domain.Model.Color> _colorRepository;
        private readonly IEntityBaseRepository<SuppliesWarehouse> _suppliesWarehouseRepository;
        private readonly IEntityBaseRepository<MaintenanceRepair> _maintainRepairRepository;
        private readonly IEntityBaseRepository<MaintenanceRepairItem> _maintainRepairItemRepository;


        public ReportInfrastructureController(
            ILogger<ReportInfrastructureController> logger,
            ILogRepository iLogRepository,
            IWebHostEnvironment iWebHostEnvironment,

            IBikeRepository iBikeRepository,
            IUserRepository iUserRepository,
            IDockRepository iDockRepository,
            IProjectRepository iProjectRepository,
            IStationRepository iStationRepository,
            IBookingRepository iBookingRepository,
            IInvestorRepository iInvestorRepository,
            ITransactionRepository transactionRepository,
            IEntityBaseRepository<City> cityRepository,
            IEntityBaseRepository<District> districtRepository,
            IEntityBaseRepository<Producer> producerRepository,
            IEntityBaseRepository<Model> modelRepository,
            IEntityBaseRepository<Supplies> suppliesRepository,
            IEntityBaseRepository<Warehouse> warehouseRepository,
            IEntityBaseRepository<Domain.Model.Color> colorRepository,
            IEntityBaseRepository<BikeInfo> bikeInfoRepository,
            IEntityBaseRepository<BikeReport> bikeReportRepository,
            IEntityBaseRepository<SuppliesWarehouse> suppliesWarehouseRepository,
            IEntityBaseRepository<MaintenanceRepair> maintainRepairRepository,
            IEntityBaseRepository<MaintenanceRepairItem> maintainRepairItemRepository
            ) : base()
        {

            _logger = logger;
            _iLogRepository = iLogRepository;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iBikeRepository = iBikeRepository;
            _iUserRepository = iUserRepository;
            _iProjectRepository = iProjectRepository;
            _iInvestorRepository = iInvestorRepository;
            _iDockRepository = iDockRepository;
            _iStationRepository = iStationRepository;
            _iBookingRepository = iBookingRepository;
            _cityRepository = cityRepository;
            _districtRepository = districtRepository;
            _producerRepository = producerRepository;
            _modelRepository = modelRepository;
            _suppliesRepository = suppliesRepository;
            _colorRepository = colorRepository;
            _warehouseRepository = warehouseRepository;
            _transactionRepository = transactionRepository;
            _bikeInfoRepository = bikeInfoRepository;
            _bikeReportRepository = bikeReportRepository;
            _suppliesWarehouseRepository = suppliesWarehouseRepository;
            _maintainRepairRepository = maintainRepairRepository;
            _maintainRepairItemRepository = maintainRepairItemRepository;
        }

        #region [Station]
        [AuthorizePermission("Index")]
        [HttpPost("Station")]
        public async Task<string> Station(string key, int? investorId, int? cityId, int? districtId, ESationStatus? status, int? projectId)
        {
            var stations = await _iStationRepository.Search(
                x => (cityId == null || x.CityId == cityId)
                    && (status == null || x.Status == status)
                    && (projectId == null || x.ProjectId == projectId)
                    && (investorId == null || x.InvestorId == investorId)
                    && (districtId == null || x.DistrictId == districtId)
                    && (key == null || x.Name.Contains(key) || x.DisplayName.Contains(key) || x.Address.Contains(key))
                );

            var numBikeInStation = await _iStationRepository.CountAvailableBikeInStation();

            var users = await _iUserRepository.Search(
                x => stations.Select(y => y.ManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    ManagerUserId = x.ManagerUserId,
                    Code = x.Code,
                    PhoneNumber = x.PhoneNumber
                });

            var projects = await _iProjectRepository.Search(
                x => stations.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => stations.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var districts = await _districtRepository.Search(
                x => stations.Select(y => y.DistrictId).Contains(x.Id),
                null,
                x => new District
                {
                    Id = x.Id,
                    Name = x.Name,
                    CityId = x.CityId
                });

            var cities = await _cityRepository.Search(
                x => districts.Select(y => y.CityId).Contains(x.Id),
                null,
                x => new City
                {
                    Id = x.Id,
                    Name = x.Name,
                });

            foreach (var station in stations)
            {
                station.Project = projects.FirstOrDefault(x => x.Id == station.ProjectId);
                station.Investor = investors.FirstOrDefault(x => x.Id == station.InvestorId);
                station.District = districts.FirstOrDefault(x => x.Id == station.DistrictId);
                station.City = cities.FirstOrDefault(x => x.Id == station.CityId);
                station.NumElectricBikeAvailable = numBikeInStation.FirstOrDefault(x => x.StationId == station.Id && x.Type == EBikeType.XeDapDien)?.Count;
                station.NumMechanicBikeAvailable = numBikeInStation.FirstOrDefault(x => x.StationId == station.Id && x.Type == EBikeType.XeDap)?.Count;
                station.ManagerUserObject = users.FirstOrDefault(x => x.Id == station.ManagerUserId);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Trạm xe");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelStation(workSheet, stations);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelStation(ExcelWorksheet worksheet, List<Station> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 25}, {3 , 25}, {4 , 20}, {5 , 40},
                {6 , 20}, {7 , 20}, {8 , 40}, {9 , 15}, {10, 15},
                {11, 14}, {12, 14}, {13, 15}, {14, 10}, {15, 10}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 9, 10, 11, 12 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;
            worksheet.Column(9).Style.Font.Bold = true;
            worksheet.Column(10).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO THÔNG TIN TRẠM XE";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:O5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:O5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:O5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên trạm";
            worksheet.Cells["C5"].Value = "Thông tin hiển thị";
            worksheet.Cells["D5"].Value = "Tỉnh/TP";
            worksheet.Cells["E5"].Value = "Địa chỉ";
            worksheet.Cells["F5"].Value = "Dự án";
            worksheet.Cells["G5"].Value = "Đơn vị đầu tư";
            worksheet.Cells["H5"].Value = "Thông tin thiết kế";
            worksheet.Cells["I5"].Value = "Số lượng xe đạp điện online tại trạm";
            worksheet.Cells["J5"].Value = "Số lượng xe đạp cơ online tại trạm";
            worksheet.Cells["K5"].Value = "Quản lý trạm";
            worksheet.Cells["L5"].Value = "SĐT";
            worksheet.Cells["M5"].Value = "Kích thước (r x d)(m)";
            worksheet.Cells["N5"].Value = "Khoảng cách trả xe";
            worksheet.Cells["O5"].Value = "Số chỗ đỗ thiết kế";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Name;
                worksheet.Cells[rs, 3].Value = item.DisplayName;
                worksheet.Cells[rs, 4].Value = $"{item.District?.Name} - {item.City?.Name}";
                worksheet.Cells[rs, 5].Value = item.Address;
                worksheet.Cells[rs, 6].Value = item.Project?.Name;
                worksheet.Cells[rs, 7].Value = item.Investor?.Name;
                worksheet.Cells[rs, 8].Value = item.Description;

                worksheet.Cells[rs, 9].Value = item.NumElectricBikeAvailable ?? 0;
                if (item.NumElectricBikeAvailable != null && item.NumElectricBikeAvailable > 0)
                {
                    worksheet.Cells[rs, 9].Style.Font.Color.SetColor(System.Drawing.Color.Green);
                }
                else
                {
                    worksheet.Cells[rs, 9].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                }

                worksheet.Cells[rs, 10].Value = item.NumMechanicBikeAvailable ?? 0;
                if (item.NumMechanicBikeAvailable != null && item.NumMechanicBikeAvailable > 0)
                {
                    worksheet.Cells[rs, 10].Style.Font.Color.SetColor(System.Drawing.Color.Green);
                }
                else
                {
                    worksheet.Cells[rs, 10].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                }

                worksheet.Cells[rs, 11].Value = item.ManagerUserObject?.Code;
                worksheet.Cells[rs, 12].Value = item.ManagerUserObject?.PhoneNumber;
                worksheet.Cells[rs, 13].Value = $"{item.Width} x {item.Height}";
                worksheet.Cells[rs, 14].Value = item.ReturnDistance;
                worksheet.Cells[rs, 15].Value = item.Spaces;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:O{rs - 1}");
            worksheet.Cells[$"A6:O{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [Dock]
        [AuthorizePermission("Index")]
        [HttpPost("Dock")]
        public async Task<string> Dock(
            string key,
            int? projectId,
            int? investorId,
            int? modelId,
            int? producerId,
            int? warehouseId,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            bool? charging,
            EBikeStatus? status,
            int? battery,
            bool? lockStatus
            )
        {
            var docks = await _iDockRepository.SearchToReport(
                bookingStatus,
                x => (projectId == null || x.InvestorId == projectId)
                    && (investorId == null || x.InvestorId == investorId)
                    && (modelId == null || x.ModelId == modelId)
                    && (producerId == null || x.ProducerId == producerId)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                    && (connectionStatus == null || x.ConnectionStatus == connectionStatus)
                    && (charging == null || x.Charging == charging)
                    && (lockStatus == null || x.LockStatus == lockStatus)
                    && (battery == null || x.Battery <= battery)
                    && (status == null || x.Status == status)
                    && (key == null || x.IMEI.Contains(key) || x.SerialNumber.Contains(key) || x.SIM.Contains(key))
                );

            var producers = await _producerRepository.Search(
                x => docks.Select(x => x.ProducerId).Distinct().Contains(x.Id),
                null,
                x => new Producer
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var models = await _modelRepository.Search(
                x => docks.Select(x => x.ModelId).Distinct().Contains(x.Id),
                null,
                x => new Model
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var bikes = await _iBikeRepository.Search(
                x => docks.Select(x => x.Id).Contains(x.DockId),
                null,
                x => new Bike
                {
                    Id = x.Id,
                    Plate = x.Plate
                });

            var stations = await _iStationRepository.Search(
                x => docks.Select(x => x.StationId).Distinct().Contains(x.Id),
                null,
                x => new Station
                {
                    Id = x.Id,
                    Name = x.Name,
                    ManagerUserId = x.ManagerUserId,
                    ProjectId = x.ProjectId,
                    InvestorId = x.InvestorId
                });

            var users = await _iUserRepository.Search(
                x => stations.Select(y => y.ManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    ManagerUserId = x.ManagerUserId,
                    Code = x.Code,
                });

            var projects = await _iProjectRepository.Search(
                x => stations.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => stations.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var books = await _iBookingRepository.Search(x => docks.Select(m => m.Id).Contains(x.DockId) && x.Status == EBookingStatus.Start);

            foreach (var dock in docks)
            {
                var station = stations.FirstOrDefault(x => x.Id == dock.StationId);
                station.ManagerUserObject = users.FirstOrDefault(x => x.Id == station.ManagerUserId);

                dock.Project = projects.FirstOrDefault(x => x.Id == dock.ProjectId);
                dock.Investor = investors.FirstOrDefault(x => x.Id == dock.InvestorId);
                dock.Station = station;
                dock.Producer = producers.FirstOrDefault(x => x.Id == dock.ProducerId);
                dock.Model = models.FirstOrDefault(x => x.Id == dock.ModelId);
                dock.Bike = bikes.FirstOrDefault(x => x.DockId == dock.Id);
                dock.BookingStatus = books.Any(x => x.DockId == dock.Id) ? EDockBookingStatus.Moving : EDockBookingStatus.Free;
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Khóa/Sim");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelDock(workSheet, docks);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelDock(ExcelWorksheet worksheet, List<Dock> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 20}, {4 , 25}, {5 , 15},
                {6 , 22}, {7 , 15}, {8 , 25}, {9 , 10}, {10, 10},
                {11, 15}, {12, 20}, {13, 15}, {14, 15}, {15, 20},
                {16, 20}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2, 3, 5, 6, 7, 9, 10, 11, 12, 13, 14 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO QUẢN LÝ KHÓA - SIM";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:P5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Serial khóa";
            worksheet.Cells["C5"].Value = "IMEI";
            worksheet.Cells["D5"].Value = "Hãng sản xuất";
            worksheet.Cells["E5"].Value = "Mẫu mã";
            worksheet.Cells["F5"].Value = "Số SIM";
            worksheet.Cells["G5"].Value = "Biển số xe gắn khóa";
            worksheet.Cells["H5"].Value = "Đang tại trạm";
            worksheet.Cells["I5"].Value = "Trạng thái khóa/mở";
            worksheet.Cells["J5"].Value = "Pin (%)";
            worksheet.Cells["K5"].Value = "Trạng thái sạc pin";
            worksheet.Cells["L5"].Value = "Thời gian kết nối gần nhất";
            worksheet.Cells["M5"].Value = "Trạng thái giao dịch xe";
            worksheet.Cells["N5"].Value = "Nhân viên quản lý trạm";
            worksheet.Cells["O5"].Value = "Dự án";
            worksheet.Cells["P5"].Value = "Đơn vị đầu tư";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.SerialNumber;
                worksheet.Cells[rs, 3].Value = item.IMEI;
                worksheet.Cells[rs, 4].Value = item.Producer?.Name;
                worksheet.Cells[rs, 5].Value = item.Model?.Name;
                worksheet.Cells[rs, 6].Value = item.SIM;
                worksheet.Cells[rs, 7].Value = item.Bike?.Plate;
                worksheet.Cells[rs, 8].Value = item.Station?.Name;
                worksheet.Cells[rs, 9].Value = item.LockStatus ? "KHÓA" : "MỞ";
                worksheet.Cells[rs, 10].Value = item.Battery;
                worksheet.Cells[rs, 11].Value = item.Charging ? "ĐANG SẠC" : "KHÔNG SẠC";
                worksheet.Cells[rs, 12].Value = item.LastConnectionTime?.ToString("HH:mm:ss dd/MM/yyyy");

                if (item.BookingStatus == EDockBookingStatus.None || item.BookingStatus == EDockBookingStatus.Free)
                {
                    worksheet.Cells[rs, 13].Value = "CHƯA THUÊ";
                }
                else if (item.BookingStatus == EDockBookingStatus.Moving)
                {
                    worksheet.Cells[rs, 13].Value = "ĐANG THUÊ";
                }
                else
                {
                    worksheet.Cells[rs, 13].Value = "";
                }
                worksheet.Cells[rs, 14].Value = item.Station?.ManagerUserObject?.Code;
                worksheet.Cells[rs, 15].Value = item.Project?.Name;
                worksheet.Cells[rs, 16].Value = item.Investor?.Name;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:P{rs - 1}");
            worksheet.Cells[$"A6:P{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [Bike]
        [AuthorizePermission("Index")]
        [HttpPost("Bike")]
        public async Task<string> Bike(
            EDockBookingStatus? bookingStatus,
            string key,
            int? warehouseId,
            int? modelId,
            int? projectId,
            Bike.EBikeType? type,
            Bike.EBikeStatus? status
            )
        {
            var bikes = await _iBikeRepository.SearchToReport(
                    key,
                    bookingStatus,
                    x => (projectId == null || x.ProjectId == projectId)
                    && (status == null || x.Status == status)
                    && (type == null || x.Type == type)
                    && (modelId == null || x.ModelId == modelId)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                );

            var docks = await _iDockRepository.Search(
                x => bikes.Select(x => x.DockId).Distinct().Contains(x.Id),
                null,
                x => new Dock
                {
                    Id = x.Id,
                    IMEI = x.IMEI
                });

            var producers = await _producerRepository.Search(
                x => bikes.Select(x => x.ProducerId).Distinct().Contains(x.Id),
                null,
                x => new Producer
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var colors = await _colorRepository.Search(
                x => bikes.Select(x => x.ColorId).Distinct().Contains(x.Id),
                null,
                x => new Domain.Model.Color
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var models = await _modelRepository.Search(
                x => bikes.Select(x => x.ModelId).Distinct().Contains(x.Id),
                null,
                x => new Model
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var transactionGroupByBike = await _transactionRepository.CountTotalMinuteRentGroupByBikeAsync();

            var stations = await _iStationRepository.Search(
                x => bikes.Select(x => x.StationId).Distinct().Contains(x.Id),
                null,
                x => new Station
                {
                    Id = x.Id,
                    Name = x.Name,
                    ManagerUserId = x.ManagerUserId,
                    ProjectId = x.ProjectId,
                    InvestorId = x.InvestorId
                });

            var users = await _iUserRepository.Search(
                x => stations.Select(y => y.ManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    ManagerUserId = x.ManagerUserId,
                    Code = x.Code,
                });

            var projects = await _iProjectRepository.Search(
                x => stations.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => stations.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var bikeInfos = await _bikeInfoRepository.Search(
                x => bikes.Select(y => y.Id).Distinct().Contains(x.BikeId));

            foreach (var bike in bikes)
            {
                var station = stations.FirstOrDefault(x => x.Id == bike.StationId);
                if (station != null)
                {
                    station.ManagerUserObject = users.FirstOrDefault(x => x.Id == station.ManagerUserId);
                }
                bike.Station = station;

                bike.Dock = docks.FirstOrDefault(x => x.Id == bike.DockId);
                bike.Project = projects.FirstOrDefault(x => x.Id == bike.ProjectId);
                bike.Investor = investors.FirstOrDefault(x => x.Id == bike.InvestorId);
                bike.Producer = producers.FirstOrDefault(x => x.Id == bike.ProducerId);
                bike.Color = colors.FirstOrDefault(x => x.Id == bike.ColorId);
                bike.Model = models.FirstOrDefault(x => x.Id == bike.ModelId);
                bike.BikeInfo = bikeInfos.FirstOrDefault(x => x.BikeId == bike.Id);
                bike.NumMinuteRent = transactionGroupByBike.FirstOrDefault(x => x.BikeId == bike.Id)?.TotalMinutesRent;
                bike.BikeReport = await _bikeReportRepository.SearchOneAsync(x => x.BikeId == bike.Id && x.Status == false);
            }

            var employees = await _iUserRepository.Search(
                x => x.RoleType == RoleManagerType.Coordinator_VTS,
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            var suppliess = await _suppliesRepository.GetAll();

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Xe đạp");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelBike(workSheet, bikes, suppliess, employees);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelBike(ExcelWorksheet worksheet, List<Bike> items, IEnumerable<Supplies> suppliess, IEnumerable<ApplicationUser> employees)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 15}, {4 , 20}, {5, 15},
                {6 , 25}, {7 , 10}, {8 , 15}, {9 , 15}, {10, 15},
                {11, 16}, {12, 13}, {13, 15}, {14, 15}, {15, 20},
                {16, 30}, {17, 25}, {18, 25}, {19, 50}, {20, 35},
                {21, 15}, {22, 30}, {23, 30}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO QUẢN LÝ XE";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:W5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:W5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:W5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:W5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Biển số";
            worksheet.Cells["C5"].Value = "Serial";
            worksheet.Cells["D5"].Value = "IMEI khóa";
            worksheet.Cells["E5"].Value = "Loại xe";
            worksheet.Cells["F5"].Value = "Nhà sản xuất";
            worksheet.Cells["G5"].Value = "Màu sắc";
            worksheet.Cells["H5"].Value = "Kiểu mẫu";
            worksheet.Cells["I5"].Value = "Ngày sản xuất";
            worksheet.Cells["J5"].Value = "Ngày đưa vào khai thác";
            worksheet.Cells["K5"].Value = "Trạng thái";
            worksheet.Cells["L5"].Value = "Lũy kế số phút đã đi";
            worksheet.Cells["M5"].Value = "Thời gian bảo dưỡng gần nhất";
            worksheet.Cells["N5"].Value = "Tổng số giờ đã sử dụng cần bảo dưỡng (trên tổng 4800 giờ)";
            worksheet.Cells["O5"].Value = "Thời gian cần bảo dưỡng (định kỳ 4 tháng)";
            worksheet.Cells["P5"].Value = "Xe bị sự cố";
            worksheet.Cells["Q5"].Value = "Mô tả trước khi bảo dưỡng sửa chữa";
            worksheet.Cells["R5"].Value = "Mô tả sau khi bảo dưỡng sửa chữa";
            worksheet.Cells["S5"].Value = "Thông tin vật tư thay thế";
            worksheet.Cells["T5"].Value = "Trạm xe đang online";
            worksheet.Cells["U5"].Value = "Nhân viên quản lý trạm";
            worksheet.Cells["V5"].Value = "Đơn vị đầu tư";
            worksheet.Cells["W5"].Value = "Dự án";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Plate;
                worksheet.Cells[rs, 3].Value = item.SerialNumber;
                worksheet.Cells[rs, 4].Value = item.Dock?.IMEI;
                worksheet.Cells[rs, 5].Value = item.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 6].Value = item.Producer?.Name;
                worksheet.Cells[rs, 7].Value = item.Color?.Name;
                worksheet.Cells[rs, 8].Value = item.Model?.Name;
                worksheet.Cells[rs, 9].Value = item.ProductionDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 10].Value = item.StartDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 11].Value = item.Status.ToEnumGetDisplayName();
                worksheet.Cells[rs, 12].Value = item.NumMinuteRent;
                worksheet.Cells[rs, 13].Value = item.BikeInfo?.LastMaintenanceTime?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 14].Value = item.BikeInfo?.TotalMinutes / 60;

                //
                if (item.BikeInfo?.LastMaintenanceTime != null)
                {
                    worksheet.Cells[rs, 15].Value = item.BikeInfo?.LastMaintenanceTime?.AddMonths(4).ToString("dd/MM/yyyy");
                }
                else if (item.StartDate != null)
                {
                    worksheet.Cells[rs, 15].Value = item.StartDate?.AddMonths(4).ToString("dd/MM/yyyy");
                }
                else
                {
                    worksheet.Cells[rs, 15].Value = "Xe chưa hoạt động";
                }
                //
                worksheet.Cells[rs, 16].Value = item.BikeReport?.Type.ToEnumGetDisplayName();
                //
                try
                {
                    if (item.BikeInfo?.MaintenanceData != null)
                    {
                        var maintainRepair = JsonConvert.DeserializeObject<MaintenanceRepair>(item.BikeInfo?.MaintenanceData);
                    }
                    else
                    {
                        worksheet.Cells[rs, 17].Value = "";
                        worksheet.Cells[rs, 18].Value = "";
                    }

                    if (item.BikeInfo?.MaintenanceDataItem != null)
                    {
                        var maintainRepairItem = JsonConvert.DeserializeObject<MaintenanceRepairItem>(item.BikeInfo?.MaintenanceDataItem);
                        var supplies = suppliess.FirstOrDefault(x => x.Id == maintainRepairItem.SuppliesId);
                        var employee = employees.FirstOrDefault(x => x.Id == maintainRepairItem.EmployeeId);
                        worksheet.Cells[rs, 19].Value = $"Mã: {supplies?.Code} | Tên: {supplies?.Name} | Slg: {maintainRepairItem?.SuppliesQuantity} | Ngày: {maintainRepairItem.CompleteDate?.ToString("dd/MM/yyyy")} | NV: {employee.Code}";
                    }
                    else
                    {
                        worksheet.Cells[rs, 19].Value = "";
                    }

                }
                catch (Exception)
                {
                    worksheet.Cells[rs, 17].Value = "ERROR";
                    worksheet.Cells[rs, 18].Value = "ERROR";
                    worksheet.Cells[rs, 19].Value = "ERROR";
                }

                worksheet.Cells[rs, 20].Value = item.Station?.Name;
                worksheet.Cells[rs, 21].Value = item.Station?.ManagerUserObject?.Code;
                worksheet.Cells[rs, 22].Value = item.Investor?.Name;
                worksheet.Cells[rs, 23].Value = item.Project?.Name;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:W{rs - 1}");
            worksheet.Cells[$"A6:W{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [Supplies]
        [AuthorizePermission("Index")]
        [HttpPost("Supplies")]
        public async Task<string> Supplies(string key, int? type)
        {
            var suppliess = await _suppliesRepository.Search(
                x => (key == null || x.Name.Contains(key) || x.Code.Contains(key) || x.Descriptions.Contains(key))
                    && (type == null || x.Type == (ESuppliesType)type)
                );

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Vật tư");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelSupplies(workSheet, suppliess);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelSupplies(ExcelWorksheet worksheet, List<Supplies> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1, 6}, {2, 12}, {3, 35}, {4, 13}, {5, 50}
            };

            SetWidthColumns(worksheet, configWidth);
            //SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 9, 10, 11, 12 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO THÔNG TIN CHI TIẾT VẬT TƯ";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:E5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã vật tư";
            worksheet.Cells["C5"].Value = "Tên vật tư";
            worksheet.Cells["D5"].Value = "Loại vật tư";
            worksheet.Cells["E5"].Value = "Mô tả";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Code;
                worksheet.Cells[rs, 3].Value = item.Name;
                worksheet.Cells[rs, 4].Value = item.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.Descriptions;

                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:E{rs - 1}");
            worksheet.Cells[$"A6:E{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [MaintainRepair]
        [AuthorizePermission("Index")]
        [HttpPost("MaintainRepair")]
        public async Task<string> MaintainRepair(
            DateTime? from,
            DateTime? to,
            string key,
            EMaintenanceRepairStatus? status,
            int? warehouseId
        )
        {
            var maintainRepairs = await _maintainRepairRepository.Search(
                x => (key == null || x.Name.Contains(key) || x.Code.Contains(key))
                    && (status == null || x.Status == status)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                );

            var maintainRepairItems = await _maintainRepairItemRepository.Search(
                x => maintainRepairs.Select(y => y.Id).Contains(x.MaintenanceRepairId)
                    && (from == null || x.CompleteDate >= from)
                    && (to == null || x.CompleteDate < to.Value.AddDays(1))
                    ,
                    null
                    ,
                    x => new MaintenanceRepairItem
                    {
                        Id = x.Id,
                        MaintenanceRepairId = x.MaintenanceRepairId,
                        SuppliesId = x.SuppliesId,
                        SuppliesCode = x.SuppliesCode,
                        SuppliesQuantity = x.SuppliesQuantity,
                        CompleteDate = x.CompleteDate,
                        EmployeeId = x.EmployeeId,
                        BikeId = x.BikeId,
                        ProjectId = x.ProjectId,
                        InvestorId = x.InvestorId
                    }
                );

            var suppliess = await _suppliesRepository.Search(
                x => maintainRepairItems.Select(x => x.SuppliesId).Contains(x.Id),
                null,
                x => new Supplies
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var bikes = await _iBikeRepository.Search(
                x => maintainRepairItems.Select(x => x.BikeId).Contains(x.Id),
                null,
                x => new Bike
                {
                    Id = x.Id,
                    Plate = x.Plate,
                    Type = x.Type,
                    DockId = x.DockId,
                    ProducerId = x.ProducerId,
                    ColorId = x.ColorId
                });

            var producers = await _producerRepository.Search(
                x => bikes.Select(x => x.ProducerId).Distinct().Contains(x.Id),
                null,
                x => new Producer
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var docks = await _iDockRepository.Search(
                x => bikes.Select(x => x.DockId).Contains(x.Id),
                null,
                x => new Dock
                {
                    Id = x.Id,
                    SerialNumber = x.SerialNumber,
                    IMEI = x.IMEI
                });

            var employees = await _iUserRepository.Search(
                x => maintainRepairItems.Select(y => y.EmployeeId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code,
                });

            var projects = await _iProjectRepository.Search(
                x => maintainRepairItems.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => maintainRepairItems.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var colors = await _colorRepository.Search(
                x => bikes.Select(y => y.ColorId).Distinct().Contains(x.Id),
                null,
                x => new Domain.Model.Color
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            foreach (var item in maintainRepairItems)
            {
                item.Bike = new Bike { };
                item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                item.Bike.Color = colors.FirstOrDefault(x => x.Id == item.Bike?.ColorId);
                item.Bike.Dock = docks.FirstOrDefault(x => x.Id == item.Bike?.DockId);
                item.Bike.Producer = producers.FirstOrDefault(x => x.Id == item.Bike?.ProducerId);
                item.Supplies = suppliess.FirstOrDefault(x => x.Id == item.SuppliesId);
                item.Project = projects.FirstOrDefault(x => x.Id == item.ProjectId);
                item.Investor = investors.FirstOrDefault(x => x.Id == item.InvestorId);
                item.Employee = employees.FirstOrDefault(x => x.Id == item.EmployeeId);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Bảo dưỡng - Sửa chữa");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelMaintainRepair(workSheet, maintainRepairItems, from, to);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelMaintainRepair(ExcelWorksheet worksheet, List<MaintenanceRepairItem> items, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 20}, {4 , 15}, {5, 25},
                {6 , 12}, {7 , 15}, {8 , 20}, {9 , 10}, {10, 15},
                {11, 15}, {12, 20}, {13, 20}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2, 3, 4, 5, 6, 7, 10, 11, 12 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO BẢO DƯỠNG - SỬA CHỮA";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["B3:D3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:M5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Serial khóa";
            worksheet.Cells["C5"].Value = "IMEI";
            worksheet.Cells["D5"].Value = "Loại xe";
            worksheet.Cells["E5"].Value = "Nhà sản xuất";
            worksheet.Cells["F5"].Value = "Màu sắc";
            worksheet.Cells["G5"].Value = "Mã vật tư";
            worksheet.Cells["H5"].Value = "Tên vật tư";
            worksheet.Cells["I5"].Value = "Số lượng";
            worksheet.Cells["J5"].Value = "Thời gian sửa chữa/bảo dưỡng";
            worksheet.Cells["K5"].Value = "Nhân viên thực hiện";
            worksheet.Cells["L5"].Value = "Đơn vị đầu tư";
            worksheet.Cells["M5"].Value = "Dự án";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Bike?.Dock?.SerialNumber;
                worksheet.Cells[rs, 3].Value = item.Bike?.Dock?.IMEI;
                worksheet.Cells[rs, 4].Value = item.Bike?.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.Bike?.Producer?.Name;
                worksheet.Cells[rs, 6].Value = item.Bike?.Color?.Name;
                worksheet.Cells[rs, 7].Value = item.Supplies?.Code;
                worksheet.Cells[rs, 8].Value = item.Supplies?.Name;
                worksheet.Cells[rs, 9].Value = item.SuppliesQuantity;
                worksheet.Cells[rs, 10].Value = item.CompleteDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 11].Value = item.Employee?.Code;
                worksheet.Cells[rs, 12].Value = item.Investor?.Name;
                worksheet.Cells[rs, 13].Value = item.Project?.Name;

                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:M{rs - 1}");
            worksheet.Cells[$"A6:M{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [SuppliesInWarehouse]
        [AuthorizePermission("Index")]
        [HttpPost("SuppliesInWarehouse")]
        public async Task<string> SuppliesInWarehouse(string key, int? employeeId, EWarehouseType? type)
        {
            var warehouses = await _warehouseRepository.Search(
                x => (key == null || x.Name.Contains(key))
                    && (employeeId == null || x.EmployeeId == employeeId)
                    && (type == null || x.Type == type)
                ,
                null,
                x => new Warehouse
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var suppliesWarehouses = await _suppliesWarehouseRepository.Search(
                x => warehouses.Select(y => y.Id).Contains(x.WarehouseId)
                );

            var suppliess = await _suppliesRepository.Search(x => suppliesWarehouses.Select(y => y.SuppliesId).Distinct().Contains(x.Id)
                ,
                null,
                x => new Supplies
                {
                    Id = x.Id,
                    Code = x.Code,
                    Name = x.Name
                });

            foreach (var item in suppliesWarehouses)
            {
                item.Warehouse = warehouses.FirstOrDefault(x => x.Id == item.WarehouseId);
                item.Supplies = suppliess.FirstOrDefault(x => x.Id == item.SuppliesId);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Vật tư tồn kho");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelSuppliesInWarehouse(workSheet, suppliesWarehouses);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelSuppliesInWarehouse(ExcelWorksheet worksheet, List<SuppliesWarehouse> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1, 6}, {2, 15}, {3, 35}, {4, 15}, {5, 15}, {6, 30}
            };

            SetWidthColumns(worksheet, configWidth);
            //SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO TỒN KHO VẬT TƯ";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:F5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã vật tư";
            worksheet.Cells["C5"].Value = "Tên vật tư";
            worksheet.Cells["D5"].Value = "Loại vật tư";
            worksheet.Cells["E5"].Value = "Số lượng tồn kho";
            worksheet.Cells["F5"].Value = "Tên kho";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Supplies?.Code;
                worksheet.Cells[rs, 3].Value = item.Supplies?.Name;
                worksheet.Cells[rs, 4].Value = item.Supplies?.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.Quantity;
                worksheet.Cells[rs, 6].Value = item.Warehouse?.Name;

                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:F{rs - 1}");
            worksheet.Cells[$"A6:F{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string, string> InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
    }
}