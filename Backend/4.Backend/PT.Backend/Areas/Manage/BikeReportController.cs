using Microsoft.AspNetCore.Mvc;
using PT.Base;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class BikeReportsController : BaseController<BikeReport, IBikeReportService>
    {
        private readonly ICustomerComplaintService _iCustomerComplaintService;
        public BikeReportsController(IBikeReportService service, ICustomerComplaintService iCustomerComplaintService)
          : base(service)
        {
            _iCustomerComplaintService = iCustomerComplaintService;
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(
            int? pageIndex,
            int? PageSize,
            string key,
            int? accountId,
            int? investorId,
            int? projectId,
            EBikeReportType? type,
            bool? status,
            string from,
            string to,
            string sortby,
            string sorttype)
            => await Service.SearchPageAsync(pageIndex ?? 1, PageSize ?? 100, key, accountId, investorId, projectId, type, status, from, to, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) =>   await Service.GetById(id);
        
        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] BikeReportCommand value, bool? status) => await Service.Edit(value, status);

        [HttpPost("Upload")]
        [AuthorizePermission("Edit")]
        public async Task<object> Upload() => await Service.Upload(Request);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await _iCustomerComplaintService.SearchByAccount(key);
    }
}