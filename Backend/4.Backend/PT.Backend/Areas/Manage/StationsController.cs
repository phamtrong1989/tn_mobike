using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using TN.Base;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]

    public class StationsController : BaseController<Station, IStationService>
    {
        private readonly ICustomersService _iCustomersService;
        public StationsController(IStationService service, ICustomersService iCustomersService)
          : base(service)
        {
            _iCustomersService = iCustomersService;
        }

        [HttpGet("SearchPaged")]
        [AuthorizePermission]
        public async Task<object> SearchPaged(int pageIndex, int pageSize, string key, int? investorId, int? cityId, int? districtId, ESationStatus? status, int? projectId, string sortby, string sorttype)
            => await Service.SearchPageAsync(pageIndex, pageSize, key, investorId, cityId, districtId, status, projectId, sortby, sorttype);


        [HttpGet("GetById/{id}")]
        [AuthorizePermission]
        public async Task<object> GetById(int id) => await Service.GetById(id);
      
        [HttpPost("Create")]
        [AuthorizePermission("Edit")]
        public async Task<object> Create([FromBody] StationCommand value) => await Service.Create(value);

        [HttpPut("Edit")]
        [AuthorizePermission("Edit")]
        public async Task<object> Edit([FromBody] StationCommand value) => await Service.Edit(value);

        [HttpDelete("Delete/{id}")]
        [AuthorizePermission("Delete")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByUser")]
        public async Task<object> SearchByUser(string key) => await _iCustomersService.SearchByUser(key);

        [AuthorizePermission("Index")]
        [HttpPost("Report")]
        public async Task<FileContentResult> Report(
            string key,
            int? investorId,
            int? cityId,
            int? districtId,
            ESationStatus? status,
            int? projectId
        )
            => await Service.ExportReport(key, investorId, cityId, districtId, status, projectId);

        [AuthorizePermission("Index")]
        [HttpGet("StationResourceGetById/{stationId}/{language}")]
        public async Task<object> StationResourceGetById(int stationId, string language) => await Service.StationResourceGetById(stationId, language);

        [AuthorizePermission("Index")]
        [HttpPut("StationResourceEdit")]
        public async Task<object> StationResourceEdit([FromBody] StationResourceCommand value) => await Service.StationResourceEdit(value);
    }
}