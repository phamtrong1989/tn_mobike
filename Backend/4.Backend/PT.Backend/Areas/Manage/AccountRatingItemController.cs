using Microsoft.AspNetCore.Mvc;
using PT.Base;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class AccountRatingItemsController : BaseController<AccountRatingItem, IAccountRatingItemService>
    {
        public AccountRatingItemsController(IAccountRatingItemService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? PageSize, string key, string sortby, string sorttype, string code)
        => await Service.SearchPageAsync(pageIndex ?? 1, PageSize ?? 100, key, sortby, sorttype, code);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] AccountRatingItemCommand value) => await Service.Create(value);

        [AuthorizePermission("Index")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] AccountRatingItemCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);
    }
}