using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using TN.Base.Command;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]

    public class DashboardRFIDController : BaseController<Station, IDashboardService>
    {
        private readonly IBookingService _iBookingService;
        private readonly IBikeService _iBikeService;
        public DashboardRFIDController(IDashboardService service, IBookingService iBookingService, IBikeService iBikeService)
          : base(service)
        {
            _iBookingService = iBookingService;
            _iBikeService = iBikeService;
        }

        [HttpGet("Stations/{projectId}")]
        [Authorize]
        public async Task<object> Stations(int projectId) => await Service.Stations(projectId);

        [HttpGet("RFIDBookings/{projectId}")]
        [AuthorizePermission("Index")]
        public async Task<object> RFIDBookings(int projectId) => await Service.RFIDBookings(projectId);

        [HttpGet("BikeGetById/{id}")]
        [AuthorizePermission("Index")]
        public async Task<object> BikeGetById(int id) => await _iBikeService.GetById(id);

        [HttpGet("GetByIMEI")]
        [AuthorizePermission("Index")]
        public object GetByIMEI(string imei, DateTime start) => Service.GPSByIMEI(imei, start, DateTime.Now);

        [HttpPost("OpenLock")]
        [AuthorizePermission("Open")]
        public async Task<object> OpenLock(BookingOpenLockCommand cm) => await _iBookingService.OpenLock(cm);

        [HttpPost("RFIDBookingCheckOut")]
        [AuthorizePermission("End")]
        public async Task<object> RFIDBookingCheckOut(RFIDBookingEndCommand cm) => await _iBookingService.RFIDBookingCheckOut(cm);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await _iBookingService.SearchByAccount(key);

        [AuthorizePermission("Index")]
        [HttpGet("GPSByDock")]
        public async Task<object> GPSByDock(int id, DateTime date, string startTime, string endTime) => await _iBikeService.GPSByDock(id, date, startTime, endTime, true, false);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByUser")]
        public async Task<object> SearchByUser(string key) => await _iBookingService.SearchByUser(key);

    }
}