using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using PT.Base;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class CustomerComplaintsController : BaseController<CustomerComplaint, ICustomerComplaintService>
    {
        public CustomerComplaintsController(ICustomerComplaintService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            ECustomerComplaintType? bikeReportType,
            ECustomerComplaintStatus? status,
            ECustomerComplaintPattern? pattern,
            int? accountId,
            DateTime? start,
            DateTime? end,
            string sortby,
            string sorttype
            )
            => await Service.SearchPageAsync(pageIndex, pageSize, key, bikeReportType, status, pattern, accountId, start, end, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] CustomerComplaintCommand value, ECustomerComplaintStatus? status) => await Service.Create(value, status);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] CustomerComplaintCommand value, ECustomerComplaintStatus? status) => await Service.Edit(value, status);

        [AuthorizePermission("Delete")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await Service.SearchByAccount(key);

        [HttpPost("Upload")]
        [AuthorizePermission("Edit")]
        public async Task<object> Upload() => await Service.Upload(Request);


        [AuthorizePermission("Index")]
        [HttpPost("Report")]
        public async Task<string> Report(
            string key,
            ECustomerComplaintType? bikeReportType,
            ECustomerComplaintStatus? status,
            ECustomerComplaintPattern? pattern,
            int? accountId,
            DateTime? start,
            DateTime? end
        )
        => await Service.ExportReport(key, bikeReportType, status, pattern, accountId, start, end);
    }
}