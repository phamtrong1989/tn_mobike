using Microsoft.AspNetCore.Mvc;
using PT.Base;
using PT.Base.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class CustomersController : BaseController<Account, ICustomersService>
    {
        private readonly ITicketPrepaidService _iTicketPrepaidService;
        private readonly IWalletTransactionService _iWalletTransactionService;

        public CustomersController(ICustomersService service, ITicketPrepaidService iTicketPrepaidService, IWalletTransactionService iWalletTransactionService)
          : base(service)
        {
            _iTicketPrepaidService = iTicketPrepaidService;
            _iWalletTransactionService = iWalletTransactionService;
        }

        #region [Account]
        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(
            int? pageIndex,
            int? pageSize,
            int? customerGroupId,
            EAccountVerifyStatus? verifyStatus,
            EAccountStatus? status,
            EAccountType? type,
            ESex? sex,
            string key,
            int? id,
            int? userId,
            string sortby,
            string sorttype
        )
            => await Service.SearchPageAsync(pageIndex ?? 1, pageSize ?? 100, key, customerGroupId, verifyStatus, status, type, sex, id, userId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id, Request);


        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] AccountEditCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] AccountEditCommand value) => await Service.Edit(value);

        [AuthorizePermission("ChangeCustomerGroup")]
        [HttpPut("EditCustomerGroup/{accountId}")]
        public async Task<object> EditCustomerGroup(int accountId, [FromBody] List<EditCustomerGroupCommand> model) => await Service.EditCustomerGroup(accountId, model, Request);

        [AuthorizePermission("VerifyAccount")]
        [HttpPut("VerifyEdit")]
        public async Task<object> VerifyEdit([FromBody] AccountVerifyCommand value) => await Service.VerifyEdit(value);

        [HttpPut("ChangePassword")]
        [AuthorizePermission("ChangePassword")]
        public async Task<object> ChangePassword([FromBody] AccountChangePasswordCommand value) => await Service.ChangePassword(value);

        [HttpPut("ConnectEmployee")]
        [AuthorizePermission("ConnectEmployee")]
        public async Task<object> ConnectEmployee([FromBody] AccountEditCommand2 value) => await Service.ConnectEmployee(value);
        #endregion

        #region [WalletTransaction]
        [AuthorizePermission("AddPoint")]
        [HttpPost("WalletTransactionAdd")]
        public async Task<object> WalletTransactionAdd([FromBody] WalletTransactionCommand value) => await _iWalletTransactionService.Create(value);

        [AuthorizePermission("Arrears")]
        [HttpPost("WalletTransactionArrears")]
        public async Task<object> WalletTransactionArrears([FromBody] WalletTransactionArrearsCommand value) => await _iWalletTransactionService.Arrears(value);

        [AuthorizePermission("Index")]
        [HttpGet("WalletTransactionSearchPaged")]
        public async Task<object> WalletTransactionSearchPaged(int? pageIndex, int? pageSize, int id, string sortby, string sorttype) => await Service.WalletTransactionSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, id, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("FeedbackSearchPaged")]
        public async Task<object> FeedbackSearchPaged(int? pageIndex, int? pageSize, int accountId, string sortby, string sorttype) => await Service.FeedbackSearchPage(pageIndex ?? 1, pageSize ?? 100, accountId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("TransactionSearchPaged")]
        public async Task<object> TransactionSearchPaged(int? pageIndex, int? pageSize, int accountId, string sortby, string sorttype) => await Service.TransactionSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, accountId, sortby, sorttype);
        #endregion

        #region [Log]
        [AuthorizePermission]
        [HttpGet("LogSearchPaged")]
        public async Task<object> LogSearchPaged(int? pageIndex, int? pageSize, int accountId, string sortby, string sorttype) => await Service.LogSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, accountId, sortby, sorttype);
        #endregion

        #region [TicketPrepaid]
        [AuthorizePermission("Index")]
        [HttpGet("TicketPrepaidGetById/{id}")]
        public async Task<object> TicketPrepaidGetById(int id)
        {
            return await _iTicketPrepaidService.GetById(id);
        }

        [AuthorizePermission("AddPrepaid")]
        [HttpPost("TicketPrepaidCreate")]
        public async Task<object> TicketPrepaidCreate([FromBody] TicketPrepaidCommand value) => await _iTicketPrepaidService.Create(value);

        [AuthorizePermission("AddPrepaid")]
        [HttpPut("TicketPrepaidEdit")]
        public async Task<object> TicketPrepaidEdit([FromBody] TicketPrepaidEditCommand value) => await _iTicketPrepaidService.Edit(value);

        [AuthorizePermission("Index")]
        [HttpGet("SearchTicketPriceByProject")]
        public async Task<object> SearchTicketPriceByProject(int projectId, int accountId) => await _iTicketPrepaidService.SearchTicketPriceByProject(projectId, accountId);

        [AuthorizePermission("Index")]
        [HttpGet("TicketPrepaidSearchPaged")]
        public async Task<object> TicketPrepaidSearchPaged(
            int pageIndex,
            int pageSize,
            string key,
            ETicketType? ticketPrice_TicketType,
            int? accountId,
            int? projectId,
            int? customerGroupId,
            string sortby,
            string sorttype)
            => await _iTicketPrepaidService.SearchPageAsync(pageIndex, pageSize, key, ticketPrice_TicketType, accountId, projectId, customerGroupId, null, null, sortby, sorttype);
        #endregion

        #region [Gift]
        [AuthorizePermission("AddGiftCode")]
        [HttpPost("AddGiftCard")]
        public async Task<object> AddGiftCard(string code, int accountId) => await _iWalletTransactionService.AddGiftCard(code, accountId);

        [AuthorizePermission("Index")]
        [HttpGet("GiftCardGetData/{accountId}")]
        public async Task<object> GiftCardGetData(int accountId) => await _iWalletTransactionService.GiftCardGetData(accountId);

        [AuthorizePermission("Edit")]
        [HttpPost("RedeemPointAdd/{accountId}/{id}")]
        public async Task<object> RedeemPointAdd(int accountId, int id) => await _iWalletTransactionService.RedeemPointAdd(accountId, id);
        #endregion

        [HttpPost("Upload")]
        [AuthorizePermission("WalletTransactionAdd")]
        public async Task<object> Upload() => await Service.Upload(Request);

        [HttpGet("View")]
        //[AuthorizePermission("Index")]
        public IActionResult View(string pathData, bool isPrivate)
        {
            var data = Service.View(pathData, isPrivate);
            return File(data, "image/jpeg");
        }

        [AuthorizePermission("SendPoint")]
        [HttpPost("WalletTransactionGift")]
        public async Task<object> WalletTransactionGift([FromBody] WalletTransactionGiftCommand value) => await _iWalletTransactionService.Gift(value);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await _iWalletTransactionService.SearchByAccount(key);
 
        [AuthorizePermission("Index")]
        [HttpGet("SearchByUser")]
        public async Task<object> SearchByUser(string key) => await Service.SearchByUser(key);

        #region [BlackList]
        [AuthorizePermission("Index")]
        [HttpGet("AccountBlackListSearchPaged")]
        public async Task<object> AccountBlackListSearchPaged(int pageIndex, int pageSize, int accountId) => await Service.AccountBlackListSearchPageAsync(pageIndex, pageSize, accountId);

        [AuthorizePermission("Index")]
        [HttpGet("AccountBlackListGetById/{id}")]
        public async Task<object> AccountBlackListGetById(int id) => await Service.AccountBlackListGetById(id);
       
        [AuthorizePermission("Index")]
        [HttpPost("AccountBlackListCreate")]
        public async Task<object> Create([FromBody] AccountBlackListCommand value) => await Service.AccountBlackListCreate(value);

        [AuthorizePermission("Index")]
        [HttpPut("AccountBlackListEdit")]
        public async Task<object> Edit([FromBody] AccountBlackListCommand value) => await Service.AccountBlackListEdit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("AccountBlackListDelete/{id}")]
        public async Task<object> Delete(int id) => await Service.AccountBlackListDelete(id);
        #endregion
    }
}