﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ReportCategoryController : Controller
    {
        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IUserRepository _iUserRepository;

        private readonly IColorRepository _iColorRepository;
        private readonly IModelRepository _iModelRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IProducerRepository _iProducerRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly IRedeemPointRepository _iRedeemPointRepository;



        public ReportCategoryController(
            ILogger<ReportCategoryController> logger,
            ILogRepository iLogRepository,
            IWebHostEnvironment iWebHostEnvironment,

            IUserRepository iUserRepository,
            IColorRepository iColorRepository,
            IModelRepository iModelRepository,
            IProjectRepository iProjectRepository,
            IProducerRepository iProducerRepository,
            IInvestorRepository iInvestorRepository,
            IRedeemPointRepository iRedeemPointRepository
            ) : base()
        {

            _logger = logger;
            _iLogRepository = iLogRepository;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iUserRepository = iUserRepository;
            _iColorRepository = iColorRepository;
            _iModelRepository = iModelRepository;
            _iProjectRepository = iProjectRepository;
            _iInvestorRepository = iInvestorRepository;
            _iProducerRepository = iProducerRepository;
            _iRedeemPointRepository = iRedeemPointRepository;
        }

        #region [Project]
        [AuthorizePermission("Index")]
        [HttpPost("Project")]
        public async Task<string> Project(EProjectStatus? status)
        {
            var projects = await _iProjectRepository.Search(
                x => (status == null || x.Status == status)
                );

            var users = await _iUserRepository.Search(
                x => projects.Select(y => y.CreatedUser).Distinct().Contains(x.Id)
                    || projects.Select(y => y.UpdatedUser ?? 0).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            foreach (var item in projects)
            {
                item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC DS Dự án");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelProject(workSheet, projects);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelProject(ExcelWorksheet worksheet, List<Project> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 20}, {3 , 30}, {4 , 15}, {5 , 15},
                {6 , 15}, {7 , 15}, {8 , 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 4, 5, 6, 7, 8 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 14, 15 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["A2:H2"].Merge = true;
            worksheet.Cells["A2:H2"].Style.Font.Bold = true;
            worksheet.Cells["A2:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:H2"].Value = $"BÁO CÁO DANH SÁCH DỰ ÁN";

            worksheet.Cells["A3:H3"].Merge = true;
            worksheet.Cells["A3:H3"].Style.Font.Italic = true;
            worksheet.Cells["A3:H3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:H3"].Value = $"Ngày lập báo cáo {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:H5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã dự án";
            worksheet.Cells["C5"].Value = "Tên dự án";
            worksheet.Cells["D5"].Value = "Trạng thái";
            worksheet.Cells["E5"].Value = "Người tạo";
            worksheet.Cells["F5"].Value = "Ngày tạo";
            worksheet.Cells["G5"].Value = "Người cập nhật";
            worksheet.Cells["H5"].Value = "Ngày cập nhật";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Code;
                worksheet.Cells[rs, 3].Value = item.Name;
                worksheet.Cells[rs, 4].Value = item.Status.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.CreatedUserObject?.Code;
                worksheet.Cells[rs, 6].Value = item.CreatedDate.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 7].Value = item.UpdatedUserObject?.Code;
                worksheet.Cells[rs, 8].Value = item.UpdatedDate?.ToString("dd/MM/yyyy");
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:H{rs - 1}");
            worksheet.Cells[$"A6:H{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [Investor]
        [AuthorizePermission("Index")]
        [HttpPost("Investor")]
        public async Task<string> Investor(EInvestorStatus? status)
        {
            var investors = await _iInvestorRepository.Search(
                x => (status == null || x.Status == status)
                );

            var users = await _iUserRepository.Search(
                x => investors.Select(y => y.CreatedUser).Distinct().Contains(x.Id)
                    || investors.Select(y => y.UpdatedUser ?? 0).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            foreach (var item in investors)
            {
                item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Đơn vị đầu tư");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelInvestor(workSheet, investors);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelInvestor(ExcelWorksheet worksheet, List<Investor> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 22}, {3 , 50}, {4 , 15}, {5 , 15},
                {6 , 15}, {7 , 15}, {8 , 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 4, 5, 6, 7, 8 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 14, 15 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["A2:H2"].Merge = true;
            worksheet.Cells["A2:H2"].Style.Font.Bold = true;
            worksheet.Cells["A2:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:H2"].Value = $"BÁO CÁO ĐƠN VỊ ĐẦU TƯ";

            worksheet.Cells["A3:H3"].Merge = true;
            worksheet.Cells["A3:H3"].Style.Font.Italic = true;
            worksheet.Cells["A3:H3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:H3"].Value = $"Ngày lập báo cáo {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:H5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã đơn vị";
            worksheet.Cells["C5"].Value = "Tên đơn vị";
            worksheet.Cells["D5"].Value = "Trạng thái";
            worksheet.Cells["E5"].Value = "Người tạo";
            worksheet.Cells["F5"].Value = "Ngày tạo";
            worksheet.Cells["G5"].Value = "Người cập nhật";
            worksheet.Cells["H5"].Value = "Ngày cập nhật";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Code;
                worksheet.Cells[rs, 3].Value = item.Name;
                worksheet.Cells[rs, 4].Value = item.Status.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.CreatedUserObject?.Code;
                worksheet.Cells[rs, 6].Value = item.CreatedDate.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 7].Value = item.UpdatedUserObject?.Code;
                worksheet.Cells[rs, 8].Value = item.UpdatedDate?.ToString("dd/MM/yyyy");
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:H{rs - 1}");
            worksheet.Cells[$"A6:H{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [Color]
        [AuthorizePermission("Index")]
        [HttpPost("Color")]
        public async Task<string> Color()
        {
            var colors = await _iColorRepository.Search();

            var users = await _iUserRepository.Search(
                x => colors.Select(y => y.CreatedUser).Distinct().Contains(x.Id)
                    || colors.Select(y => y.UpdatedUser ?? 0).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            foreach (var item in colors)
            {
                item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC DS Mã Màu");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelColor(workSheet, colors);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelColor(ExcelWorksheet worksheet, List<Domain.Model.Color> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 15}, {4 , 15}, {5 , 15},
                {6 , 15}, {7 , 15}, {8 , 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2, 3, 4, 5, 6, 7, 8 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 14, 15 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["A2:H2"].Merge = true;
            worksheet.Cells["A2:H2"].Style.Font.Bold = true;
            worksheet.Cells["A2:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:H2"].Value = $"BÁO CÁO DANH SÁCH MÃ MÀU";

            worksheet.Cells["A3:H3"].Merge = true;
            worksheet.Cells["A3:H3"].Style.Font.Italic = true;
            worksheet.Cells["A3:H3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:H3"].Value = $"Ngày lập báo cáo {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:H5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã màu";
            worksheet.Cells["C5"].Value = "Màu";
            worksheet.Cells["D5"].Value = "Tên màu";
            worksheet.Cells["E5"].Value = "Người tạo";
            worksheet.Cells["F5"].Value = "Ngày tạo";
            worksheet.Cells["G5"].Value = "Người cập nhật";
            worksheet.Cells["H5"].Value = "Ngày cập nhật";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;

                worksheet.Cells[rs, 2].Value = item.Code;

                System.Drawing.Color colFromHex = System.Drawing.ColorTranslator.FromHtml(item.Code);
                worksheet.Cells[rs, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[rs, 3].Style.Fill.BackgroundColor.SetColor(colFromHex);

                worksheet.Cells[rs, 4].Value = item.Name;
                worksheet.Cells[rs, 5].Value = item.CreatedUserObject?.Code;
                worksheet.Cells[rs, 6].Value = item.CreatedDate.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 7].Value = item.UpdatedUserObject?.Code;
                worksheet.Cells[rs, 8].Value = item.UpdatedDate?.ToString("dd/MM/yyyy");
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:H{rs - 1}");
            worksheet.Cells[$"A6:H{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [Model]
        [AuthorizePermission("Index")]
        [HttpPost("Model")]
        public async Task<string> Model(int? producerId)
        {
            var models = await _iModelRepository.Search(
                    x => (producerId == null || x.ProducerId == producerId)
                );

            var producers = await _iProducerRepository.Search(
                    x => models.Select(y => y.ProducerId).Distinct().Contains(x.Id),
                    null,
                    x => new Producer
                    {
                        Id = x.Id,
                        Name = x.Name
                    }
                );

            var users = await _iUserRepository.Search(
                x => models.Select(y => y.CreatedUser).Distinct().Contains(x.Id)
                    || models.Select(y => y.UpdatedUser ?? 0).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            foreach (var item in models)
            {
                item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                item.Producer = producers.FirstOrDefault(x => x.Id == item.ProducerId);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Mẫu thiết bị");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelModel(workSheet, models);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelModel(ExcelWorksheet worksheet, List<Model> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 45}, {4 , 15}, {5 , 15},
                {6 , 15}, {7 , 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 4 , 5, 6, 7 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 14, 15 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["A2:G2"].Merge = true;
            worksheet.Cells["A2:G2"].Style.Font.Bold = true;
            worksheet.Cells["A2:G2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:G2"].Value = $"BÁO CÁO DANH SÁCH MẪU THIẾT BỊ";

            worksheet.Cells["A3:G3"].Merge = true;
            worksheet.Cells["A3:G3"].Style.Font.Italic = true;
            worksheet.Cells["A3:G3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:G3"].Value = $"Ngày lập báo cáo {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:G5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên mẫu";
            worksheet.Cells["C5"].Value = "Nhà sản xuất";
            worksheet.Cells["D5"].Value = "Người tạo";
            worksheet.Cells["E5"].Value = "Ngày tạo";
            worksheet.Cells["F5"].Value = "Người cập nhật";
            worksheet.Cells["G5"].Value = "Ngày cập nhật";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Name;
                worksheet.Cells[rs, 3].Value = item.Producer?.Name;
                worksheet.Cells[rs, 4].Value = item.CreatedUserObject?.Code;
                worksheet.Cells[rs, 5].Value = item.CreatedDate.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 6].Value = item.UpdatedUserObject?.Code;
                worksheet.Cells[rs, 7].Value = item.UpdatedDate?.ToString("dd/MM/yyyy");
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:G{rs - 1}");
            worksheet.Cells[$"A6:G{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [Producer]
        [AuthorizePermission("Index")]
        [HttpPost("Producer")]
        public async Task<string> Producer()
        {
            var producers = await _iProducerRepository.Search();

            var users = await _iUserRepository.Search(
                x => producers.Select(y => y.CreatedUser).Distinct().Contains(x.Id)
                    || producers.Select(y => y.UpdatedUser ?? 0).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            foreach (var item in producers)
            {
                item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC NSX");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelProducer(workSheet, producers);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};
            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelProducer(ExcelWorksheet worksheet, List<Producer> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 45}, {3 , 15}, {4 , 15}, {5 , 15},
                {6 , 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 3, 4, 5, 6 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 14, 15 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["A2:F2"].Merge = true;
            worksheet.Cells["A2:F2"].Style.Font.Bold = true;
            worksheet.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:F2"].Value = $"BÁO CÁO DANH SÁCH NHÀ SẢN XUẤT";

            worksheet.Cells["A3:F3"].Merge = true;
            worksheet.Cells["A3:F3"].Style.Font.Italic = true;
            worksheet.Cells["A3:F3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:F3"].Value = $"Ngày lập báo cáo {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:F5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên nhà sản xuất";
            worksheet.Cells["C5"].Value = "Người tạo";
            worksheet.Cells["D5"].Value = "Ngày tạo";
            worksheet.Cells["E5"].Value = "Người cập nhật";
            worksheet.Cells["F5"].Value = "Ngày cập nhật";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Name;
                worksheet.Cells[rs, 3].Value = item.CreatedUserObject?.Code;
                worksheet.Cells[rs, 4].Value = item.CreatedDate.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 5].Value = item.UpdatedUserObject?.Code;
                worksheet.Cells[rs, 6].Value = item.UpdatedDate?.ToString("dd/MM/yyyy");
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:F{rs - 1}");
            worksheet.Cells[$"A6:F{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [RedeemPoint]
        [AuthorizePermission("Index")]
        [HttpPost("RedeemPoint")]
        public async Task<string> RedeemPoint()
        {
            var redeemPoints = await _iRedeemPointRepository.Search();

            var users = await _iUserRepository.Search(
                x => redeemPoints.Select(y => y.CreatedUser).Distinct().Contains(x.Id)
                    || redeemPoints.Select(y => y.UpdatedUser ?? 0).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            foreach (var item in redeemPoints)
            {
                item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Cấu hình đổi điểm");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelRedeemPoint(workSheet, redeemPoints);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};
            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelRedeemPoint(ExcelWorksheet worksheet, List<RedeemPoint> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 22}, {3 , 15}, {4 , 15}, {5 , 20},
                {6 , 15}, {7 , 15}, {8 , 15}, {9 , 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] {3, 4, 6, 7, 8, 9 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 3, 4 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["A2:I2"].Merge = true;
            worksheet.Cells["A2:I2"].Style.Font.Bold = true;
            worksheet.Cells["A2:I2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:I2"].Value = $"BÁO CÁO CẤU HÌNH ĐỔI ĐIỂM";

            worksheet.Cells["A3:I3"].Merge = true;
            worksheet.Cells["A3:I3"].Style.Font.Italic = true;
            worksheet.Cells["A3:I3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:I3"].Value = $"Ngày lập báo cáo {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:I5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên";
            worksheet.Cells["C5"].Value = "Điểm tích lũy";
            worksheet.Cells["D5"].Value = "Điểm thanh toán";
            worksheet.Cells["E5"].Value = "Ghi chú";
            worksheet.Cells["F5"].Value = "Người tạo";
            worksheet.Cells["G5"].Value = "Ngày tạo";
            worksheet.Cells["H5"].Value = "Người cập nhật";
            worksheet.Cells["I5"].Value = "Ngày cập nhật";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Name;
                worksheet.Cells[rs, 3].Value = item.TripPoint;
                worksheet.Cells[rs, 4].Value = item.ToPoint;
                worksheet.Cells[rs, 5].Value = item.Note;
                worksheet.Cells[rs, 6].Value = item.CreatedUserObject?.Code;
                worksheet.Cells[rs, 7].Value = item.CreatedDate.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 8].Value = item.UpdatedUserObject?.Code;
                worksheet.Cells[rs, 9].Value = item.UpdatedDate?.ToString("dd/MM/yyyy");
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:I{rs - 1}");
            worksheet.Cells[$"A6:I{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string, string> InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
    }
}