using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using System;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ExportBillsController : BaseController<ExportBill, IExportBillService>
    {
        public ExportBillsController(IExportBillService service)
          : base(service)
        {
        }

        #region [ExportBill]
        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(
            int pageIndex,
            int pageSize,
            string key,
            EExportBillStatus? status,
            int? warehouseId,
            int? toWarehouseId,
            DateTime? start,
            DateTime? end,
            EExportBillCompleteStatus? completeStatus,
            string sortby,
            string sorttype
            ) => await Service.SearchPageAsync(pageIndex, pageSize, key, status, warehouseId, toWarehouseId, start, end, completeStatus, sortby, sorttype);


        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] ExportBillCommand value) => await Service.Create(value);

        [AuthorizePermission("Index")]
        [HttpPut("Approve")]
        public async Task<object> Approve([FromBody] ExportBillApproveCommand value) => await Service.Approve(value);

        [AuthorizePermission("Index")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] ExportBillCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpPut("Cancel/{id}")]
        public async Task<object> Cancel(int id) => await Service.Cancel(id);

        [HttpPost("Upload")]
        [AuthorizePermission("Edit")]
        public async Task<object> Upload() => await Service.Upload(Request);

        [HttpGet("View")]
        public object View(string pathData, bool isPrivate) => Service.View(pathData, isPrivate);

        [AuthorizePermission("Edit")]
        [HttpPut("CompleteApprove/{id}/{completeStatus}")]
        public async Task<object> CompleteApprove(int id, EExportBillCompleteStatus completeStatus) => await Service.CompleteApprove(id, completeStatus);

        [AuthorizePermission("Edit")]
        [HttpPut("Refuse/{id}")]
        public async Task<object> Refuse(int id) => await Service.Refuse(id);
        #endregion

        #region [ExportBillItem]
        [AuthorizePermission("Index")]
        [HttpGet("ExportBillItemSearchPaged")]
        public async Task<object> ExportBillItemSearchPaged(int pageIndex, int pageSize, int exportBillId) => await Service.ExportBillItemSearchPageAsync(pageIndex, pageSize, exportBillId);

        [AuthorizePermission("Index")]
        [HttpGet("ExportBillItemGetById/{id}")]
        public async Task<object> ExportBillItemGetById(int id) => await Service.ExportBillItemGetById(id);
       
        [AuthorizePermission("Edit")]
        [HttpPost("ExportBillItemCreate")]
        public async Task<object> ExportBillItemCreate([FromBody] ExportBillItemCommand value) => await Service.ExportBillItemCreate(value);

        [AuthorizePermission("Edit")]
        [HttpPut("ExportBillItemEdit")]
        public async Task<object> ExportBillItemEdit([FromBody] ExportBillItemCommand value) => await Service.ExportBillItemEdit(value);

        [AuthorizePermission("Edit")]
        [HttpDelete("ExportBillItemDelete/{id}")]
        public async Task<object> ExportBillItemDelete(int id) => await Service.ExportBillItemDelete(id);
        #endregion

        [Authorize]
        [HttpGet("SuppliesSearchKey")]
        public async Task<object> SuppliesSearchKey(string key, int warehouseId) => await Service.SuppliesSearchKey(key, warehouseId);


    }
}