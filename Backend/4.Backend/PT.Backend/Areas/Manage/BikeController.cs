using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using TN.Base;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class BikesController : BaseController<Bike, IBikeService>
    {
        private readonly IDockService _iDockService;
        public BikesController(IBikeService service, IDockService iDockService)
          : base(service)
        {
            _iDockService = iDockService;
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(
            int pageIndex,
            int pageSize,
            EDockBookingStatus? bookingStatus,
            string key,
            int? warehouseId,
            int? modelId,
            int? projectId,
            Bike.EBikeType? type,
            Bike.EBikeStatus? status,
            bool? connectionStatus,
            int? battery,
            int? stationId,
            bool? lockStatus,
            bool? charging,
            string sortby,
            string sorttype)
            => await Service.SearchPageAsync(pageIndex, pageSize, bookingStatus, key, warehouseId, modelId, projectId, type, status, connectionStatus, battery, lockStatus, charging, stationId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("OpenDock")]
        [HttpPost("OpenLock")]
        public async Task<object> OpenLock([FromBody] DockOpenManualCommand value) => await _iDockService.OpenLock(value);

        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] BikeCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] BikeCommand value) => await Service.Edit(value);

        [AuthorizePermission("Delete")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        #region [Log]
        [Authorize]
        [HttpGet("LogSearchPaged")]
        public async Task<object> LogSearchPaged(int? pageIndex, int? pageSize, int objectId, string sortby, string sorttype) => await Service.LogSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, objectId, sortby, sorttype);
        #endregion

        [Authorize]
        [HttpGet("DockSearch")]
        public async Task<object> DockSearch(string key, int projectId, int? investorId) => await Service.DockSearch(key, projectId, investorId);

        [AuthorizePermission("Control")]
        [HttpPut("EditLocation")]
        public async Task<object> EditLocation([FromBody] BikeEditLocationCommand value) => await Service.EditLocation(value);

        [AuthorizePermission("Index")]
        [HttpGet("GPSByDock")]
        public async Task<object> GPSByDock(int id, DateTime date, string startTime, string endTime) => await Service.GPSByDock(id, date, startTime, endTime, true, false);

        [AuthorizePermission("Index")]
        [HttpPost("Report")]
        public async Task<FileContentResult> Report(
            string key,
            int? warehouseId,
            int? modelId,
            int? projectId,
            Bike.EBikeType? type,
            Bike.EBikeStatus? status,
            EDockBookingStatus? bookingStatus
        )
        => await Service.ExportReport(key, warehouseId, modelId, projectId, type, status, bookingStatus);
    }
}