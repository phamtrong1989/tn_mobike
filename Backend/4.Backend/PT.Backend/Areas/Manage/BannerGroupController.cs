using Microsoft.AspNetCore.Mvc;
using PT.Base;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class BannerGroupsController : BaseController<BannerGroup, IBannerGroupService>
    {
        public BannerGroupsController(IBannerGroupService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? PageSize, string key, string sortby, string sorttype)
            => await Service.SearchPageAsync(pageIndex ?? 1, PageSize ?? 100, key, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Edit")]
        [HttpGet("GetBannerGroups")]
        public async Task<object> GetBannerGroups() => await Service.GetBannerGroups();

        #region [Banner]
        [AuthorizePermission("Index")]
        [HttpGet("BannerSearchPaged")]
        public async Task<object> BannerSearchPaged(int? pageIndex, int? pageSize, int bannerGroupId)
            => await Service.BannerSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, bannerGroupId);

        [AuthorizePermission("Index")]
        [HttpGet("BannerGetById/{id}")]
        public async Task<object> BannerGetById(int id) => await Service.BannerGetById(id);

        [AuthorizePermission("Edit")]
        [HttpPost("BannerCreate")]
        public async Task<object> BannerCreate([FromBody] BannerCommand value) => await Service.BannerCreate(value);

        [AuthorizePermission("Edit")]
        [HttpPut("BannerEdit")]
        public async Task<object> BannerEdit([FromBody] BannerCommand value) => await Service.BannerEdit(value);

        [AuthorizePermission("Delete")]
        [HttpDelete("BannerDelete/{id}")]
        public async Task<object> BannerDelete(int id) => await Service.BannerDelete(id);

        [AuthorizePermission("Edit")]
        [HttpPost("Upload")]
        public async Task<object> Upload() => await Service.Upload(Request);
        #endregion
    }
}