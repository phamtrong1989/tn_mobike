﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ReportCustomerController : Controller
    {
        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IUserRepository _iUserRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;


        public ReportCustomerController(
            ILogger<ReportCustomerController> logger,
            ILogRepository iLogRepository,
            IWebHostEnvironment iWebHostEnvironment,

            IAccountRepository iAccountRepository,
            IWalletRepository iWalletRepository,
            IWalletTransactionRepository iWalletTransactionRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IUserRepository iUserRepository,
            IProjectAccountRepository iProjectAccountRepository,
            IProjectRepository iProjectRepository,
            IInvestorRepository iInvestorRepository
            ) : base()
        {

            _logger = logger;
            _iLogRepository = iLogRepository;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iAccountRepository = iAccountRepository;
            _iWalletRepository = iWalletRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iUserRepository = iUserRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iProjectRepository = iProjectRepository;
            _iInvestorRepository = iInvestorRepository;
        }

        #region [Customer]
        [AuthorizePermission("Index")]
        [HttpPost("Customer")]
        public async Task<string> Customer(
            DateTime? from,
            DateTime? to,
            string key,
            EAccountVerifyStatus? verifyStatus,
            EAccountStatus? status,
            EAccountType? type,
            ESex? sex)
        {
            var accounts = await _iAccountRepository.Search(
                x => (key == null || x.Address.Contains(key) || x.Phone == key || x.FirstName == key || x.LastName == key || x.Email == key || x.Code == key | x.ShareCode == key)
                    && (verifyStatus == null || x.VerifyStatus == verifyStatus)
                    && (status == null || x.Status == status)
                    && (type == null || x.Type == type)
                    && (sex == null || x.Sex == sex)
                    && (from == null || x.CreatedDate >= from)
                    && (to == null || x.CreatedDate < to.Value.AddDays(1)));

            var users = await _iUserRepository.Search(
                x => accounts.Select(y => y.ManagerUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.SubManagerUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.VerifyUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.UpdatedSystemUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    ManagerUserId = x.ManagerUserId,
                    Code = x.Code
                });

            var projectAccounts = await _iProjectAccountRepository.Search(
                x => accounts.Select(y => y.Id).Distinct().Contains(x.AccountId),
                null,
                x => new ProjectAccount
                {
                    Id = x.Id,
                    ProjectId = x.ProjectId,
                    InvestorId = x.InvestorId,
                    AccountId = x.AccountId
                });

            var projects = await _iProjectRepository.Search(
                x => projectAccounts.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => projectAccounts.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var wallets = await _iWalletRepository.Search(
                x => accounts.Select(y => y.Id).Contains(x.AccountId),
                null,
                x => new Wallet
                {
                    Id = x.Id,
                    AccountId = x.AccountId,
                    Balance = x.Balance,
                    SubBalance = x.SubBalance
                });

            foreach (var account in accounts)
            {
                account.Projects = projects.Where(x => projectAccounts.Where(y => y.AccountId == account.Id)
                                                                      .Select(y => y.ProjectId)
                                                                      .Distinct()
                                                                      .Contains(x.Id));

                account.Investors = investors.Where(x => projectAccounts.Where(y => y.AccountId == account.Id)
                                                                        .Select(y => y.InvestorId)
                                                                        .Distinct()
                                                                        .Contains(x.Id));

                account.ManagerUserCode = users.FirstOrDefault(x => x.Id == account.ManagerUserId)?.Code;
                account.SubManagerUserCode = users.FirstOrDefault(x => x.Id == account.SubManagerUserId)?.Code;
                account.VerifyUserCode = users.FirstOrDefault(x => x.Id == account.VerifyUserId)?.Code;
                account.UpdateUserCode = users.FirstOrDefault(x => x.Id == account.UpdatedSystemUserId)?.Code;

                account.Wallet = wallets.FirstOrDefault(x => x.AccountId == account.Id);
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC TTKH");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelCustomer(workSheet, accounts, from, to);
                MyExcel.Save();
            }

            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelCustomer(ExcelWorksheet worksheet, List<Account> items, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 13}, {3 , 23}, {4 , 10}, {5 , 14},
                {6 , 15}, {7 , 14}, {8 , 30}, {9 , 20}, {10, 15},
                {11, 14}, {12, 14}, {13, 15}, {14, 15}, {15, 15},
                {16, 15}, {17, 15}, {18, 15}, {19, 30}, {20, 30},
                {21, 15}, {22, 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2, 4, 5, 7, 9, 10, 11, 12, 13, 16, 17, 18 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 14, 15 });
            SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:F2"].Merge = true;
            worksheet.Cells["B2:F2"].Style.Font.Bold = true;
            worksheet.Cells["B2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B2:F2"].Value = $"BÁO CÁO THÔNG TIN KHÁCH HÀNG";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:F3"].Merge = true;
            worksheet.Cells["B3:F3"].Style.Font.Italic = true;
            worksheet.Cells["B3:F3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:F3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:V5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:V5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:V5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:V5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã KH";
            worksheet.Cells["C5"].Value = "Tên KH";
            worksheet.Cells["D5"].Value = "Giới tính";
            worksheet.Cells["E5"].Value = "Ngày sinh";
            worksheet.Cells["F5"].Value = "Số CCCD/HC";
            worksheet.Cells["G5"].Value = "SĐT";
            worksheet.Cells["H5"].Value = "Email";
            worksheet.Cells["I5"].Value = "Trạng thái xác thực";
            worksheet.Cells["J5"].Value = "Mã RFID";
            worksheet.Cells["K5"].Value = "Loại TK";
            worksheet.Cells["L5"].Value = "Ngày tạo TK";
            worksheet.Cells["M5"].Value = "Trạng thái TK";
            worksheet.Cells["N5"].Value = "Điểm gốc";
            worksheet.Cells["O5"].Value = "Điểm khuyến mại";
            worksheet.Cells["P5"].Value = "Mã người xác thực";
            worksheet.Cells["Q5"].Value = "Mã CTV/NV";
            worksheet.Cells["R5"].Value = "Mã Quản lý";
            worksheet.Cells["S5"].Value = "Đơn vị đầu tư";
            worksheet.Cells["T5"].Value = "Dự án";
            worksheet.Cells["U5"].Value = "Ngày cập nhật";
            worksheet.Cells["V5"].Value = "Người cập nhật";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Code;
                worksheet.Cells[rs, 3].Value = item.FullName;
                worksheet.Cells[rs, 4].Value = item.Sex.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.Birthday?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 6].Value = item.IdentificationID;
                worksheet.Cells[rs, 7].Value = item.Phone;
                worksheet.Cells[rs, 8].Value = item.Email;
                worksheet.Cells[rs, 9].Value = item.VerifyStatus.ToEnumGetDisplayName();
                worksheet.Cells[rs, 10].Value = item.RFID;
                worksheet.Cells[rs, 11].Value = item.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 12].Value = item.CreatedDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 13].Value = item.Status.ToEnumGetDisplayName();
                worksheet.Cells[rs, 14].Value = item.Wallet?.Balance ?? 0;
                worksheet.Cells[rs, 15].Value = item.Wallet?.SubBalance ?? 0;
                worksheet.Cells[rs, 16].Value = item.VerifyUserCode;
                worksheet.Cells[rs, 17].Value = item.SubManagerUserCode;
                worksheet.Cells[rs, 18].Value = item.ManagerUserCode;
                worksheet.Cells[rs, 19].Value = string.Join("\r\n ", item.Investors?.Select(x => x.Name));
                worksheet.Cells[rs, 20].Value = string.Join("\r\n ", item.Projects?.Select(x => x.Name));
                worksheet.Cells[rs, 21].Value = item.UpdatedSystemDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 22].Value = item.UpdateUserCode;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:V{rs - 1}");
            worksheet.Cells[$"A6:V{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        #region [CustomerTransaction]
        [AuthorizePermission("Index")]
        [HttpPost("CustomerTransaction")]
        public async Task<string> CustomerTransaction(DateTime? from, DateTime? to)
        {
            var walletTransactions = await _iWalletTransactionRepository.Search(
                x => (from == null || x.CreatedDate >= from)
                    && (to == null || x.CreatedDate < to.Value.AddDays(1))
                    && x.Status == EWalletTransactionStatus.Done

                    && (x.Type == EWalletTransactionType.Deposit            //
                        || x.Type == EWalletTransactionType.Manually        // Nạp tiền
                        || x.Type == EWalletTransactionType.Money           //

                        || x.Type == EWalletTransactionType.Withdraw        //
                        || x.Type == EWalletTransactionType.Paid            // Trừ tiền
                        || x.Type == EWalletTransactionType.WithdrawPrepaid //
                        || x.Type == EWalletTransactionType.BuyPrepaid),    //
                null,
                x => new WalletTransaction
                {
                    Id = x.Id,
                    AccountId = x.AccountId,
                    WalletId = x.WalletId,
                    Type = x.Type,
                    Status = x.Status,
                    CreatedDate = x.CreatedDate,
                    Amount = x.Amount,
                    SubAmount = x.SubAmount,
                    Wallet_Balance = x.Wallet_Balance,
                    Wallet_SubBalance = x.Wallet_SubBalance
                });

            var accounts = await _iAccountRepository.Search(
                x => x.Status == EAccountStatus.Active,
                null,
                x => new Account
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Code = x.Code,
                    RFID = x.RFID,
                });

            var projectAccounts = await _iProjectAccountRepository.Search(
                x => accounts.Select(y => y.Id).Contains(x.AccountId),
                null,
                x => new ProjectAccount
                {
                    Id = x.Id,
                    AccountId = x.AccountId,
                    CustomerGroupId = x.CustomerGroupId
                });

            //var projects = await _iProjectRepository.Search(
            //    x => projectAccounts.Select(y => y.ProjectId).Distinct().Contains(x.Id),
            //    null,
            //    x => new Project
            //    {
            //        Id = x.Id,
            //        Name = x.Name
            //    });

            var customerGroups = await _iCustomerGroupRepository.Search(
                    x => projectAccounts.Select(x => x.CustomerGroupId).Distinct().Contains(x.Id),
                    null,
                    x => new CustomerGroup
                    {
                        Id = x.Id,
                        Name = x.Name
                    });

            foreach (var account in accounts)
            {
                var groupIds = projectAccounts.Where(x => x.AccountId == account.Id).Select(x => x.CustomerGroupId).Distinct();
                account.CustomerGroups = customerGroups.Where(x => groupIds.Contains(x.Id));
                //
                //// Tổng điểm nạp TK gốc
                account.AmountPlus = walletTransactions.Where(x => x.AccountId == account.Id
                                                                    && (x.Type == EWalletTransactionType.Deposit
                                                                        || x.Type == EWalletTransactionType.Manually
                                                                        || x.Type == EWalletTransactionType.Money))
                                                        .Sum(x => x.Amount);

                account.PaymentGroups = walletTransactions.Where(x => x.AccountId == account.Id
                                                                    && (x.Type == EWalletTransactionType.Deposit
                                                                        || x.Type == EWalletTransactionType.Manually
                                                                        || x.Type == EWalletTransactionType.Money))
                                                          .Select(x => x.PaymentGroup).Distinct();
                //
                //// Tổng điểm trừ TK gốc
                account.AmountMinus = walletTransactions.Where(x => x.AccountId == account.Id
                                                                    && (x.Type == EWalletTransactionType.Withdraw
                                                                        || x.Type == EWalletTransactionType.Paid
                                                                        || x.Type == EWalletTransactionType.WithdrawPrepaid
                                                                        || x.Type == EWalletTransactionType.BuyPrepaid))
                                                        .Sum(x => x.Amount);
                //// Tổng điểm trừ TK thưởng
                account.SubAmountMinus = walletTransactions.Where(x => x.AccountId == account.Id
                                                                    && (x.Type == EWalletTransactionType.Withdraw
                                                                        || x.Type == EWalletTransactionType.Paid
                                                                        || x.Type == EWalletTransactionType.WithdrawPrepaid
                                                                        || x.Type == EWalletTransactionType.BuyPrepaid))
                                                        .Sum(x => x.SubAmount);

                // Tồn tài khoản tại thời điểm giao dịch cuối cùng tính đến ngày giới hạn báo cáo
                var transactionsInDay = walletTransactions.Where(x => x.AccountId == account.Id);
                if (transactionsInDay.Any())
                {
                    var lastestTransaction = transactionsInDay.OrderByDescending(x => x.CreatedDate).FirstOrDefault();

                    if (lastestTransaction == null)
                    {
                        account.Wallet_Balance = (await _iWalletRepository.SearchOneAsync(x => x.AccountId == account.Id))?.Balance ?? 0;
                        account.Wallet_SubBalance = (await _iWalletRepository.SearchOneAsync(x => x.AccountId == account.Id))?.SubBalance ?? 0;
                    }
                    else
                    {
                        //Giao dịch cộng tiền
                        if (lastestTransaction.Type == EWalletTransactionType.Deposit
                            || lastestTransaction.Type == EWalletTransactionType.Manually
                            || lastestTransaction.Type == EWalletTransactionType.Money)
                        {
                            account.Wallet_Balance = lastestTransaction.Wallet_Balance + lastestTransaction.Amount;
                            account.Wallet_SubBalance = lastestTransaction.Wallet_SubBalance + lastestTransaction.SubAmount;
                        }
                        //Giao dịch trừ tiền
                        else if (lastestTransaction.Type == EWalletTransactionType.Withdraw
                          || lastestTransaction.Type == EWalletTransactionType.Paid
                          || lastestTransaction.Type == EWalletTransactionType.WithdrawPrepaid
                          || lastestTransaction.Type == EWalletTransactionType.BuyPrepaid)
                        {
                            account.Wallet_Balance = lastestTransaction.Wallet_Balance - lastestTransaction.Amount < 0 ? 0 : lastestTransaction.Wallet_Balance - lastestTransaction.Amount;
                            account.Wallet_SubBalance = lastestTransaction.Wallet_SubBalance - lastestTransaction.SubAmount < 0 ? 0 : lastestTransaction.Wallet_SubBalance - lastestTransaction.SubAmount;
                        }
                    }
                }
                else
                {
                    var lastestTransaction = (await _iWalletTransactionRepository.SearchTop(
                        1,
                        x => x.AccountId == account.Id
                            && x.CreatedDate < from
                            && x.Status == EWalletTransactionStatus.Done

                            && (x.Type == EWalletTransactionType.Deposit            //
                                || x.Type == EWalletTransactionType.Manually        // Nạp tiền
                                || x.Type == EWalletTransactionType.Money           //

                                || x.Type == EWalletTransactionType.Withdraw        //
                                || x.Type == EWalletTransactionType.Paid            // Trừ tiền
                                || x.Type == EWalletTransactionType.WithdrawPrepaid //
                                || x.Type == EWalletTransactionType.BuyPrepaid),    //
                        EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                        x => new WalletTransaction
                        {
                            Id = x.Id,
                            AccountId = x.AccountId,
                            WalletId = x.WalletId,
                            Type = x.Type,
                            Status = x.Status,
                            CreatedDate = x.CreatedDate,
                            Amount = x.Amount,
                            SubAmount = x.SubAmount,
                            Wallet_Balance = x.Wallet_Balance,
                            Wallet_SubBalance = x.Wallet_SubBalance
                        })).FirstOrDefault();

                    if (lastestTransaction == null)
                    {
                        account.Wallet_Balance = (await _iWalletRepository.SearchOneAsync(x => x.AccountId == account.Id))?.Balance ?? 0;
                        account.Wallet_SubBalance = (await _iWalletRepository.SearchOneAsync(x => x.AccountId == account.Id))?.SubBalance ?? 0;
                    }
                    else
                    {
                        //Giao dịch cộng tiền
                        if (lastestTransaction.Type == EWalletTransactionType.Deposit
                            || lastestTransaction.Type == EWalletTransactionType.Manually
                            || lastestTransaction.Type == EWalletTransactionType.Money)
                        {
                            account.Wallet_Balance = lastestTransaction.Wallet_Balance + lastestTransaction.Amount;
                            account.Wallet_SubBalance = lastestTransaction.Wallet_SubBalance + lastestTransaction.SubAmount;
                        }
                        //Giao dịch trừ tiền
                        else if (lastestTransaction.Type == EWalletTransactionType.Withdraw
                          || lastestTransaction.Type == EWalletTransactionType.Paid
                          || lastestTransaction.Type == EWalletTransactionType.WithdrawPrepaid
                          || lastestTransaction.Type == EWalletTransactionType.BuyPrepaid)
                        {
                            account.Wallet_Balance = lastestTransaction.Wallet_Balance - lastestTransaction.Amount < 0 ? 0 : lastestTransaction.Wallet_Balance - lastestTransaction.Amount;
                            account.Wallet_SubBalance = lastestTransaction.Wallet_SubBalance - lastestTransaction.SubAmount < 0 ? 0 : lastestTransaction.Wallet_SubBalance - lastestTransaction.SubAmount;
                        }
                    }
                }
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC TK Online");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelCustomerTransaction(workSheet, accounts, from, to);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath.Item1, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/x-msdownload")
            //{
            //    FileDownloadName = Path.GetFileName(filePath.Item1)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelCustomerTransaction(ExcelWorksheet worksheet, List<Account> items, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1, 6}, {2, 23}, {3, 14}, {4, 13}, {5, 15},
                {6, 15}, {7, 15},{8, 15}, {9, 15}, {10, 15},
                {11, 15}, {12, 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 3, 4, 5 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 7, 9, 10, 11, 12 });
            SetNumberAsText(worksheet, new int[] { 3 });

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO TỒN TÀI KHOẢN ONLINE";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:L5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên KH";
            worksheet.Cells["C5"].Value = "Số ĐT";
            worksheet.Cells["D5"].Value = "Mã KH";
            worksheet.Cells["E5"].Value = "Mã RFID";
            worksheet.Cells["F5"].Value = "Nhóm KH";
            worksheet.Cells["G5"].Value = "Tổng tiền nạp thành công";
            worksheet.Cells["H5"].Value = "Cổng nạp tiền";
            worksheet.Cells["I5"].Value = "Tổng điểm trừ TK gốc";
            worksheet.Cells["J5"].Value = "Tổng điểm trừ TK điểm thưởng";
            worksheet.Cells["K5"].Value = "Tồn điểm TK gốc";
            worksheet.Cells["L5"].Value = "Tồn điểm TK điểm thưởng";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.FullName;
                worksheet.Cells[rs, 3].Value = item.Phone;
                worksheet.Cells[rs, 4].Value = item.Code;
                worksheet.Cells[rs, 5].Value = item.RFID;
                worksheet.Cells[rs, 6].Value = string.Join("\r\n ", item.CustomerGroups?.Select(x => x.Name));
                worksheet.Cells[rs, 7].Value = item.AmountPlus;
                worksheet.Cells[rs, 8].Value = string.Join("\r\n ", item.PaymentGroups?.Select(x => x.ToEnumGetDisplayName()));
                worksheet.Cells[rs, 9].Value = item.AmountMinus;
                worksheet.Cells[rs, 10].Value = item.SubAmountMinus;
                worksheet.Cells[rs, 11].Value = item.Wallet_Balance;
                worksheet.Cells[rs, 12].Value = item.Wallet_SubBalance;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:L{rs - 1}");
            worksheet.Cells[$"A6:L{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string,string>  InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
    }
}