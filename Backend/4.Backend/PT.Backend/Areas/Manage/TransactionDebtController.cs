using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using TN.Base.Command;

namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class TransactionDebtController : BaseController<Transaction, ITransactionService>
    { 
        public TransactionDebtController(ITransactionService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? pageSize, string key, int? investorId, int? projectId, int? customerGroupId, EBookingStatus? status, int? stationIn, int? stationOut, string from, string to, int? accountId, ETicketType? ticketPrice_TicketType, string sortby, string sorttype)
            => await Service.SearchPageAsync(pageIndex ?? 1, pageSize ?? 100, key,  investorId,  projectId,  customerGroupId,  status, stationIn,  stationOut,  from, to, accountId, ticketPrice_TicketType, true, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);
      
        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await Service.SearchByAccount(key);

        [AuthorizePermission("Refund")]
        [HttpPost("Refund")]
        public async Task<object> Refund([FromBody] TransctionRefundCommand cm) => await Service.Refund(cm.Id, cm.Note);
    }
}