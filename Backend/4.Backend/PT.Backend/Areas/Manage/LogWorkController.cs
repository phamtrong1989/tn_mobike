﻿using Microsoft.AspNetCore.Mvc;
using PT.Base;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Models.Command;
using TN.Base.Services;
using TN.Domain.Model.Work;

namespace TN.Backend.Areas.Manage
{

    [Area("work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class LogWorkController : BaseController<LogWork, ILogWorkService>
    {
        public LogWorkController(ILogWorkService service)
          : base(service)
        {
        }

        //[AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? userId,
            EShift? shift,
            bool? isLate,
            string from,
            string to,
            string sortby,
            string sorttype
        )
        => await Service.SearchPageAsync(pageIndex, pageSize, key, userId, shift, isLate, from, to, sortby, sorttype);

        //[AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        //[AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] LogWorkCommand value) => await Service.Create(value);

        //[AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] LogWorkCommand value) => await Service.Edit(value);

        //[AuthorizePermission("Index")]
        [HttpGet("SearchByUser")]
        public async Task<object> SearchByUser(string key) => await Service.SearchByUser(key);
    }
}