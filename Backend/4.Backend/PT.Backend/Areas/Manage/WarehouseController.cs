using Microsoft.AspNetCore.Mvc;
using PT.Base;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class WarehousesController : BaseController<Warehouse, IWarehouseService>
    {
        public WarehousesController(IWarehouseService service)
          : base(service)
        {
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? pageSize, string key, string sortby, string sorttype, int? employeeId, EWarehouseType? type)
            => await Service.SearchPageAsync(pageIndex ?? 1, pageSize ?? 100, key, sortby, sorttype, employeeId, type);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] WarehouseCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] WarehouseCommand value) => await Service.Edit(value);

        [AuthorizePermission("Delete")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByEmployee")]
        public async Task<object> SearchByEmployee(string key) => await Service.SearchByEmployee(key);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByEmployeeFree")]
        public async Task<object> SearchByEmployeeFree(string key) => await Service.SearchByEmployeeFree(key);

        [AuthorizePermission("Index")]
        [HttpGet("SuppliesSearchPaged")]
        public async Task<object> SuppliesSearchPaged(
            int? pageIndex,
            int? pageSize,
            string key,
            int warehouseId,
            ESuppliesType? type)
            => await Service.SuppliesSearchPageAsync(pageIndex ?? 1, pageSize ?? 100, key, warehouseId, type);

        [AuthorizePermission("Index")]
        [HttpPost("Report")]
        public async Task<FileContentResult> Report(
            string key,
            int? employeeId,
            EWarehouseType? type
        )
        => await Service.ExportReport(key, employeeId, type);

    }
}