﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ReportCampaignController : Controller
    {

        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IAccountRepository _iAccountRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly ICampaignRepository _iCampaignRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IVoucherCodeRepository _iVoucherCodeRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IDepositEventRepository _iDepositEventRepository;

        public ReportCampaignController(
            ILogger<ReportCampaignController> logger,
            ILogRepository iLogRepository,
            IWebHostEnvironment iWebHostEnvironment,

            IAccountRepository iAccountRepository,
            IWalletTransactionRepository iWalletTransactionRepository,
            ICampaignRepository iCampaignRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IUserRepository iUserRepository,
            IVoucherCodeRepository iVoucherCodeRepository,
            IProjectRepository iProjectRepository,
            IDepositEventRepository iDepositEventRepository
            ) : base()
        {

            _logger = logger;
            _iLogRepository = iLogRepository;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iAccountRepository = iAccountRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iCampaignRepository = iCampaignRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iUserRepository = iUserRepository;
            _iVoucherCodeRepository = iVoucherCodeRepository;
            _iProjectRepository = iProjectRepository;
            _iDepositEventRepository = iDepositEventRepository;
        }

        #region [Campaign]
        [AuthorizePermission("Index")]
        [HttpPost("Campaign")]
        public async Task<string> Campaign(string key, int? projectId, ECampaignStatus? status, ECampaignType? type, DateTime? from, DateTime? to)
        {
            var campaigns = await _iCampaignRepository.Search(
                x => (key == null || x.Name.Contains(key) || x.Description.Contains(key))
                    && (status == null || x.Status == status)
                    && (projectId == null || x.ProjectId == projectId)
                    && (type == null || x.Type == type)
                    && (from == null || x.CreatedDate >= from)
                    && (to == null || x.CreatedDate < to.Value.AddDays(1)));

            var projects = await _iProjectRepository.Search(
                x => campaigns.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var campaignDeposit = campaigns.Where(x => x.Type == ECampaignType.DepositEvent);
            var campaignVoucher = campaigns.Where(x => x.Type == ECampaignType.GiftCode);

            var depositEvents = await _iDepositEventRepository.Search(
                x => campaignDeposit.Select(x => x.Id).Contains(x.CampaignId),
                null,
                x => new DepositEvent
                {
                    Id = x.Id,
                    Type = x.Type,
                    Used = x.Used,
                    Point = x.Point,
                    Limit = x.Limit,
                    Percent = x.Percent,
                    CampaignId = x.CampaignId,
                    StartAmount = x.StartAmount,
                    CustomerGroupId = x.CustomerGroupId,
                    TransactionCode = x.TransactionCode,
                });

            var voucherCodes = await _iVoucherCodeRepository.Search(
                x => campaignVoucher.Select(x => x.Id).Contains(x.CampaignId),
                null,
                x => new VoucherCode
                {
                    Id = x.Id,
                    Code = x.Code,
                    Point = x.Point,
                    Limit = x.Limit,
                    CampaignId = x.CampaignId,
                    CurentLimit = x.CurentLimit,
                    CustomerGroupId = x.CustomerGroupId
                });

            var customerDeposit = await _iCustomerGroupRepository.Search(
                x => depositEvents.Select(x => x.CustomerGroupId).Distinct().Contains(x.Id),
                null,
                x => new CustomerGroup
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var customerVoucher = await _iCustomerGroupRepository.Search(
                x => voucherCodes.Select(x => x.CustomerGroupId).Distinct().Contains(x.Id),
                null,
                x => new CustomerGroup
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var walletTransactions = await _iWalletTransactionRepository.Search(
                x => depositEvents.Select(y => y.Id).Contains(x.DepositEventId)
                    || voucherCodes.Select(y => y.Id).Contains(x.VoucherCodeId),
                null,
                x => new WalletTransaction
                {
                    Id = x.Id,
                    TransactionCode = x.TransactionCode,
                    AccountId = x.AccountId,
                    Amount = x.Amount,
                    SubAmount = x.SubAmount,
                    DepositEventId = x.DepositEventId,
                    VoucherCodeId = x.VoucherCodeId
                });

            var accounts = await _iAccountRepository.Search(
                x => walletTransactions.Select(y => y.AccountId).Distinct().Contains(x.Id),
                null,
                x => new Account
                {
                    Id = x.Id,
                    ManagerUserId = x.ManagerUserId,
                    SubManagerUserId = x.SubManagerUserId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    RFID = x.RFID
                });

            var users = await _iUserRepository.Search(
                x => accounts.Select(y => y.ManagerUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.SubManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            foreach (var item in campaigns)
            {
                if (item.ProjectId > 0)
                {
                    item.Project = projects.FirstOrDefault(x => x.Id == item.ProjectId);
                }

                if (item.Type == ECampaignType.DepositEvent)
                {
                    item.DepositEvents = depositEvents.Where(x => x.CampaignId == item.Id)
                                                      .Select(x => new DepositEvent
                                                      {
                                                          Id = x.Id,
                                                          CampaignId = x.CampaignId,
                                                          CustomerGroupId = x.CustomerGroupId,
                                                          StartAmount = x.StartAmount,
                                                          TransactionCode = x.TransactionCode,
                                                          Type = x.Type,
                                                          Point = x.Point,
                                                          Percent = x.Percent,
                                                          Limit = x.Limit,
                                                          CustomerGroup = customerDeposit.FirstOrDefault(y => y.Id == x.CustomerGroupId),
                                                          WalletTransactions = walletTransactions.Where(y => y.DepositEventId == x.Id)
                                                                                                 .Select(y => new WalletTransaction
                                                                                                 {
                                                                                                     Id = y.Id,
                                                                                                     TransactionCode = y.TransactionCode,
                                                                                                     Amount = y.Amount,
                                                                                                     SubAmount = y.SubAmount,
                                                                                                     AccountId = y.AccountId,
                                                                                                     Account = accounts.Where(a => a.Id == y.AccountId)
                                                                                                                        .Select(a => new Account
                                                                                                                        {
                                                                                                                            Id = a.Id,
                                                                                                                            ManagerUserId = a.ManagerUserId,
                                                                                                                            SubManagerUserId = a.SubManagerUserId,
                                                                                                                            FullName = a.FullName,
                                                                                                                            Phone = a.Phone,
                                                                                                                            RFID = a.RFID,
                                                                                                                            SubManagerUserCode = users.FirstOrDefault(u => u.Id == a.SubManagerUserId)?.Code,
                                                                                                                            ManagerUserCode = users.FirstOrDefault(u => u.Id == a.ManagerUserId)?.Code
                                                                                                                        }).FirstOrDefault()
                                                                                                 })
                                                      });
                }

                if (item.Type == ECampaignType.GiftCode)
                {
                    item.VoucherCodes = voucherCodes.Where(x => x.CampaignId == item.Id)
                                                    .Select(x => new VoucherCode
                                                    {
                                                        Id = x.Id,
                                                        CampaignId = x.CampaignId,
                                                        CustomerGroupId = x.CustomerGroupId,
                                                        Code = x.Code,
                                                        Point = x.Point,
                                                        Limit = x.Limit,
                                                        CurentLimit = x.CurentLimit,
                                                        CustomerGroup = customerVoucher.FirstOrDefault(y => y.Id == x.CustomerGroupId),
                                                        WalletTransactions = walletTransactions.Where(y => y.VoucherCodeId == x.Id)
                                                                                                 .Select(y => new WalletTransaction
                                                                                                 {
                                                                                                     Id = y.Id,
                                                                                                     TransactionCode = y.TransactionCode,
                                                                                                     Amount = y.Amount,
                                                                                                     SubAmount = y.SubAmount,
                                                                                                     AccountId = y.AccountId,
                                                                                                     Account = accounts.Where(a => a.Id == y.AccountId)
                                                                                                                        .Select(a => new Account
                                                                                                                        {
                                                                                                                            Id = a.Id,
                                                                                                                            ManagerUserId = a.ManagerUserId,
                                                                                                                            SubManagerUserId = a.SubManagerUserId,
                                                                                                                            FullName = a.FullName,
                                                                                                                            Phone = a.Phone,
                                                                                                                            RFID = a.RFID,
                                                                                                                            SubManagerUserCode = users.FirstOrDefault(u => u.Id == a.SubManagerUserId)?.Code,
                                                                                                                            ManagerUserCode = users.FirstOrDefault(u => u.Id == a.ManagerUserId)?.Code
                                                                                                                        }).FirstOrDefault()
                                                                                                 })
                                                    });
                }
            }

            var path = InitPath();
            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC CT KM");
                MyExcel.Workbook.Worksheets.Add("BC KQ CT KM");
                ExcelWorksheet worksheet0 = MyExcel.Workbook.Worksheets[0];
                ExcelWorksheet worksheet1 = MyExcel.Workbook.Worksheets[1];

                FormatCampaignExcel(worksheet0, campaigns, from, to);
                FormatCampaignDetailExcel(worksheet1, campaigns, from, to);

                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatCampaignExcel(ExcelWorksheet worksheet, List<Campaign> campaigns, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1, 6}, {2, 40}, {3, 25}, {4, 20}, {5, 50},
                {6, 15}, {7, 15},{8, 15}, {9, 20}, {10, 20},
                {11, 25}, {12, 12},{13, 12}, {14, 25},{15, 25}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 13, 12, 9, 8, 7, 6, 4, 3 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Right, new int[] { 10 });
            SetNumberAsCurrency(worksheet, new int[] { 12, 13 });
            //SetNumberAsText(worksheet, new int[] { 3 });

            // Title
            worksheet.Cells["B2:O2"].Merge = true;
            worksheet.Cells["B2:O2"].Style.Font.Bold = true;
            worksheet.Cells["B2:O2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B2:O2"].Value = $"BÁO CÁO CHƯƠNG TRÌNH KHUYẾN MẠI";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:O3"].Merge = true;
            worksheet.Cells["B3:O3"].Style.Font.Italic = true;
            worksheet.Cells["B3:O3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:O3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:O5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:O5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:O5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells[5, 1].Value = "STT";
            worksheet.Cells[5, 2].Value = "Tên chương trình KM";
            worksheet.Cells[5, 3].Value = "Dự án";
            worksheet.Cells[5, 4].Value = "Loại gói";
            worksheet.Cells[5, 5].Value = "Mô tả gói";
            worksheet.Cells[5, 6].Value = "Thời gian bắt đầu";
            worksheet.Cells[5, 7].Value = "Thời gian kết thúc";
            worksheet.Cells[5, 8].Value = "Nhóm KH";
            worksheet.Cells[5, 9].Value = "Mã KM";
            worksheet.Cells[5, 10].Value = "Mức KM";
            worksheet.Cells[5, 11].Value = "Điều kiện";
            worksheet.Cells[5, 12].Value = "Số lượng mã phát hành";
            worksheet.Cells[5, 13].Value = "Số lượng mã đã dùng";
            worksheet.Cells[5, 14].Value = "Nội dung tin nhắn";
            worksheet.Cells[5, 15].Value = "Cách thức truyền thông";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1; // row start loop data table

            if (campaigns.Any())
            {
                foreach (var campaign in campaigns)
                {
                    if (campaign.DepositEvents != null && campaign.DepositEvents.Any())
                    {
                        foreach (var deposit in campaign.DepositEvents)
                        {
                            worksheet.Cells[rs, 1].Value = stt;
                            worksheet.Cells[rs, 2].Value = campaign.Name;
                            worksheet.Cells[rs, 3].Value = campaign.ProjectId == 0 ? "Tất cả" : campaign.Project?.Name;
                            worksheet.Cells[rs, 4].Value = campaign.Type.ToEnumGetDisplayName();
                            worksheet.Cells[rs, 5].Value = campaign.Description;
                            worksheet.Cells[rs, 6].Value = campaign.StartDate.ToString("dd/MM/yyyy");
                            worksheet.Cells[rs, 7].Value = campaign.EndDate.ToString("dd/MM/yyyy");

                            worksheet.Cells[rs, 8].Value = deposit.CustomerGroupId != 0 ? deposit.CustomerGroup.Name : "Tất cả";
                            worksheet.Cells[rs, 9].Value = deposit.TransactionCode;

                            var depositValue = string.Empty;
                            if (deposit.Type == EDepositEventType.Point)
                            {
                                depositValue = $"{(int)deposit.Point} điểm";
                            }
                            else if (deposit.Type == EDepositEventType.Percent)
                            {
                                depositValue = $"{deposit.Percent} % điểm nạp";
                            }
                            worksheet.Cells[rs, 10].Value = depositValue;
                            worksheet.Cells[rs, 11].Value = $"Nạp ít nhất {(int)deposit.StartAmount} điểm";
                            worksheet.Cells[rs, 12].Value = deposit.Limit;
                            worksheet.Cells[rs, 13].Value = deposit.Used;
                            worksheet.Cells[rs, 14].Value = "";
                            worksheet.Cells[rs, 15].Value = "";
                            //
                            stt++;
                            rs++;
                        }
                    }
                    //
                    if (campaign.VoucherCodes != null && campaign.VoucherCodes.Any())
                    {
                        foreach (var voucher in campaign.VoucherCodes)
                        {
                            worksheet.Cells[rs, 1].Value = stt;
                            worksheet.Cells[rs, 2].Value = campaign.Name;
                            worksheet.Cells[rs, 3].Value = campaign.ProjectId == 0 ? "Tất cả" : campaign.Project?.Name;
                            worksheet.Cells[rs, 4].Value = campaign.Type.ToEnumGetDisplayName();
                            worksheet.Cells[rs, 5].Value = campaign.Description;
                            worksheet.Cells[rs, 6].Value = campaign.StartDate.ToString("dd/MM/yyyy");
                            worksheet.Cells[rs, 7].Value = campaign.EndDate.ToString("dd/MM/yyyy");

                            worksheet.Cells[rs, 8].Value = voucher.CustomerGroupId != 0 ? voucher.CustomerGroup.Name : "Tất cả";
                            worksheet.Cells[rs, 9].Value = voucher.Code;
                            worksheet.Cells[rs, 10].Value = voucher.Point > 0 ? $"{(int)voucher.Point} điểm" : "";
                            worksheet.Cells[rs, 11].Value = "Số lượng giới hạn";
                            worksheet.Cells[rs, 12].Value = voucher.Limit;
                            worksheet.Cells[rs, 13].Value = voucher.Limit - voucher.CurentLimit;
                            worksheet.Cells[rs, 14].Value = "";
                            worksheet.Cells[rs, 15].Value = "";
                            //
                            stt++;
                            rs++;
                        }
                    }
                }
            }

            SetBorderTable(worksheet, $"A5:O{rs - 1}");
            worksheet.Cells[$"A6:O{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        private void FormatCampaignDetailExcel(ExcelWorksheet worksheet, List<Campaign> campaigns, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1, 6}, {2, 40}, {3, 25}, {4, 20}, {5, 50},
                {6, 15}, {7, 15},{8, 15}, {9, 20}, {10, 20},
                {11, 25}, {12, 12},{13, 24}, {14, 14}, {15, 14},
                {16, 12}, {17, 12},{18, 15}, {19, 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 19, 18, 15, 14, 12, 9, 8, 7, 6, 4, 3 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Right, new int[] { 10 });
            SetNumberAsCurrency(worksheet, new int[] { 12, 16, 17 });
            //SetNumberAsText(worksheet, new int[] { 3 });

            // Title
            worksheet.Cells["B2:E2"].Merge = true;
            worksheet.Cells["B2:E2"].Style.Font.Bold = true;
            worksheet.Cells["B2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B2:E2"].Value = $"BÁO CÁO KẾT QUẢ CHƯƠNG TRÌNH KHUYẾN MẠI";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:E3"].Merge = true;
            worksheet.Cells["B3:E3"].Style.Font.Italic = true;
            worksheet.Cells["B3:E3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:E3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:S5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:S5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:S5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:S5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells[5, 1].Value = "STT";
            worksheet.Cells[5, 2].Value = "Tên chương trình KM";
            worksheet.Cells[5, 3].Value = "Dự án";
            worksheet.Cells[5, 4].Value = "Loại gói";
            worksheet.Cells[5, 5].Value = "Mô tả gói";
            worksheet.Cells[5, 6].Value = "Thời gian bắt đầu";
            worksheet.Cells[5, 7].Value = "Thời gian kết thúc";
            worksheet.Cells[5, 8].Value = "Nhóm KH";
            worksheet.Cells[5, 9].Value = "Mã KM";
            worksheet.Cells[5, 10].Value = "Mức KM";
            worksheet.Cells[5, 11].Value = "Điều kiện";
            worksheet.Cells[5, 12].Value = "Số lượng mã phát hành";
            worksheet.Cells[5, 13].Value = "Tên KH";
            worksheet.Cells[5, 14].Value = "SĐT";
            worksheet.Cells[5, 15].Value = "Mã RFID";
            worksheet.Cells[5, 16].Value = "Số tiền nạp";
            worksheet.Cells[5, 17].Value = "Số điểm KM";
            worksheet.Cells[5, 18].Value = "Mã CTV/NV";
            worksheet.Cells[5, 19].Value = "Mã Quản lý";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1; // row start loop data table

            if (campaigns.Any())
            {
                foreach (var campaign in campaigns)
                {
                    if (campaign.DepositEvents != null && campaign.DepositEvents.Any())
                    {
                        foreach (var deposit in campaign.DepositEvents)
                        {
                            if (deposit.WalletTransactions != null && deposit.WalletTransactions.Any())
                            {
                                foreach (var walletTransaction in deposit.WalletTransactions)
                                {
                                    worksheet.Cells[rs, 1].Value = stt;
                                    worksheet.Cells[rs, 2].Value = campaign.Name;
                                    worksheet.Cells[rs, 3].Value = campaign.ProjectId == 0 ? "Tất cả" : campaign.Project?.Name;
                                    worksheet.Cells[rs, 4].Value = campaign.Type.ToEnumGetDisplayName();
                                    worksheet.Cells[rs, 5].Value = campaign.Description;
                                    worksheet.Cells[rs, 6].Value = campaign.StartDate.ToString("dd/MM/yyyy");
                                    worksheet.Cells[rs, 7].Value = campaign.EndDate.ToString("dd/MM/yyyy");

                                    worksheet.Cells[rs, 8].Value = deposit.CustomerGroupId != 0 ? deposit.CustomerGroup.Name : "Tất cả";
                                    worksheet.Cells[rs, 9].Value = deposit.TransactionCode;

                                    var depositValue = string.Empty;
                                    if (deposit.Type == EDepositEventType.Point)
                                    {
                                        depositValue = $"{(int)deposit.Point} điểm";
                                    }
                                    else if (deposit.Type == EDepositEventType.Percent)
                                    {
                                        depositValue = $"{deposit.Percent} % điểm nạp";
                                    }
                                    worksheet.Cells[rs, 10].Value = depositValue;
                                    worksheet.Cells[rs, 11].Value = $"Nạp ít nhất {(int)deposit.StartAmount} điểm";
                                    worksheet.Cells[rs, 12].Value = deposit.Limit;
                                    worksheet.Cells[rs, 13].Value = walletTransaction.Account?.FullName;
                                    worksheet.Cells[rs, 14].Value = walletTransaction.Account?.Phone;
                                    worksheet.Cells[rs, 15].Value = walletTransaction.Account?.RFID; ;
                                    worksheet.Cells[rs, 16].Value = walletTransaction.Amount;
                                    worksheet.Cells[rs, 17].Value = walletTransaction.SubAmount;
                                    worksheet.Cells[rs, 18].Value = walletTransaction.Account?.SubManagerUserCode;
                                    worksheet.Cells[rs, 19].Value = walletTransaction.Account?.ManagerUserCode;
                                    //
                                    stt++;
                                    rs++;
                                }
                            }
                        }
                    }
                    //
                    if (campaign.VoucherCodes != null && campaign.VoucherCodes.Any())
                    {
                        foreach (var voucher in campaign.VoucherCodes)
                        {
                            if (voucher.WalletTransactions != null && voucher.WalletTransactions.Any())
                            {
                                foreach (var walletTransaction in voucher.WalletTransactions)
                                {
                                    worksheet.Cells[rs, 1].Value = stt;
                                    worksheet.Cells[rs, 2].Value = campaign.Name;
                                    worksheet.Cells[rs, 3].Value = campaign.ProjectId == 0 ? "Tất cả" : campaign.Project?.Name;
                                    worksheet.Cells[rs, 4].Value = campaign.Type.ToEnumGetDisplayName();
                                    worksheet.Cells[rs, 5].Value = campaign.Description;
                                    worksheet.Cells[rs, 6].Value = campaign.StartDate.ToString("dd/MM/yyyy");
                                    worksheet.Cells[rs, 7].Value = campaign.EndDate.ToString("dd/MM/yyyy");

                                    worksheet.Cells[rs, 8].Value = voucher.CustomerGroupId != 0 ? voucher.CustomerGroup.Name : "Tất cả";
                                    worksheet.Cells[rs, 9].Value = voucher.Code;
                                    worksheet.Cells[rs, 10].Value = voucher.Point > 0 ? $"{(int)voucher.Point} điểm" : "";
                                    worksheet.Cells[rs, 11].Value = "Số lượng giới hạn";
                                    worksheet.Cells[rs, 12].Value = voucher.Limit;

                                    worksheet.Cells[rs, 13].Value = walletTransaction.Account?.FullName;
                                    worksheet.Cells[rs, 14].Value = walletTransaction.Account?.Phone;
                                    worksheet.Cells[rs, 15].Value = walletTransaction.Account?.RFID; ;
                                    worksheet.Cells[rs, 16].Value = walletTransaction.Amount;
                                    worksheet.Cells[rs, 17].Value = walletTransaction.SubAmount;
                                    worksheet.Cells[rs, 18].Value = walletTransaction.Account?.SubManagerUserCode;
                                    worksheet.Cells[rs, 19].Value = walletTransaction.Account?.ManagerUserCode;
                                    //
                                    stt++;
                                    rs++;
                                }
                            }
                        }
                    }
                }
            }

            SetBorderTable(worksheet, $"A5:S{rs - 1}");
            worksheet.Cells[$"A6:S{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string, string> InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
    }
}