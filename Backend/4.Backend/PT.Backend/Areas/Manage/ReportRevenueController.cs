﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ReportRevenueController : Controller
    {
        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IUserRepository _iUserRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;


        public ReportRevenueController(
            ILogger<ReportCategoryController> logger,
            ILogRepository iLogRepository,
            IWebHostEnvironment iWebHostEnvironment,

            IUserRepository iUserRepository,
            IAccountRepository iAccountRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IProjectAccountRepository iProjectAccountRepository,
            IWalletTransactionRepository iWalletTransactionRepository
            ) : base()
        {

            _logger = logger;
            _iLogRepository = iLogRepository;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iUserRepository = iUserRepository;
            _iAccountRepository = iAccountRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
        }

        #region [Revenue]
        [AuthorizePermission("Index")]
        [HttpPost("Revenue")]
        public async Task<string> Revenue(DateTime? from, DateTime? to)
        {
            var data = await _iWalletTransactionRepository.Search(
                    x => (from == null || x.CreatedDate >= from)
                        && (to == null || x.CreatedDate < to.Value.AddDays(1))
                        && x.Status == EWalletTransactionStatus.Done
                        && (x.Type == EWalletTransactionType.Manually
                            || x.Type == EWalletTransactionType.Money
                            || x.Type == EWalletTransactionType.Deposit
                           )
                    );

            var accounts = await _iAccountRepository.Search(
                x => data.Select(y => y.AccountId).Contains(x.Id),
                null,
                x => new Account
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    RFID = x.RFID,
                    Code = x.Code,
                    ManagerUserId = x.ManagerUserId,
                    SubManagerUserId = x.SubManagerUserId
                });

            var users = await _iUserRepository.Search(
                x => accounts.Select(y => y.ManagerUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.SubManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    ManagerUserId = x.ManagerUserId,
                    Code = x.Code
                });

            var projectAccounts = await _iProjectAccountRepository.Search(
                x => accounts.Select(y => y.Id).Contains(x.AccountId),
                null,
                x => new ProjectAccount
                {
                    Id = x.Id,
                    AccountId = x.AccountId,
                    CustomerGroupId = x.CustomerGroupId
                });

            var customerGroups = await _iCustomerGroupRepository.Search(
                x => projectAccounts.Select(x => x.CustomerGroupId).Distinct().Contains(x.Id),
                null,
                x => new CustomerGroup
                {
                    Id = x.Id,
                    Name = x.Name
                });

            foreach (var item in data)
            {
                item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);

                var groupIds = projectAccounts.Where(x => x.AccountId == item.Id).Select(x => x.CustomerGroupId).Distinct();
                item.CustomerGroups = customerGroups.Where(x => groupIds.Contains(x.Id));

                if (item.Account != null)
                {
                    item.Account.ManagerUserCode = users.FirstOrDefault(x => x.Id == item.Account.ManagerUserId)?.Code;
                    item.Account.SubManagerUserCode = users.FirstOrDefault(x => x.Id == item.Account.SubManagerUserId)?.Code;
                }
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("Doanh thu");
                ExcelWorksheet myWorksheet = MyExcel.Workbook.Worksheets[0];                

                FormatExcelRevenue(myWorksheet, data, from, to);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelRevenue(ExcelWorksheet worksheet, List<WalletTransaction> items, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 23}, {3 , 14}, {4 , 13}, {5 , 13},
                {6 , 13}, {7 , 20}, {8 , 13}, {9 , 24}, {10, 15},
                {11, 13}, {12, 13}, {13, 15}, {14, 15}, {15, 25},
                {16, 25}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 14, 13, 7, 6, 5, 4, 3 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 8, 11, 12 });
            SetNumberAsText(worksheet, new int[] { 3 });

            // Title
            worksheet.Cells["A2:P2"].Merge = true;
            worksheet.Cells["A2:P2"].Style.Font.Bold = true;
            worksheet.Cells["A2:P2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:P2"].Value = $"BÁO CÁO DOANH THU NẠP TIỀN";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["A3:P3"].Merge = true;
            worksheet.Cells["A3:P3"].Style.Font.Italic = true;
            worksheet.Cells["A3:P3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:P3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:P5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên KH";
            worksheet.Cells["C5"].Value = "SĐT";
            worksheet.Cells["D5"].Value = "Mã KH";
            worksheet.Cells["E5"].Value = "Mã RFID";
            worksheet.Cells["F5"].Value = "Nhóm KH";
            worksheet.Cells["G5"].Value = "Thời gian nạp";
            worksheet.Cells["H5"].Value = "Số tiền nạp";
            worksheet.Cells["I5"].Value = "Loại giao dịch";
            worksheet.Cells["J5"].Value = "Cổng giao dịch";
            worksheet.Cells["K5"].Value = "TK gốc trước nạp";
            worksheet.Cells["L5"].Value = "TK KM trước nạp";
            worksheet.Cells["M5"].Value = "Mã CTV/NV";
            worksheet.Cells["N5"].Value = "Mã Quản lý";
            worksheet.Cells["O5"].Value = "Mã Giao dịch";
            worksheet.Cells["P5"].Value = "Mã Đơn hàng";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Account?.FullName;
                worksheet.Cells[rs, 3].Value = item.Account?.Phone;
                worksheet.Cells[rs, 4].Value = item.Account?.Code;
                worksheet.Cells[rs, 5].Value = item.Account?.RFID;
                worksheet.Cells[rs, 6].Value = string.Join("\r\n ", item.CustomerGroups?.Select(x => x.Name));
                worksheet.Cells[rs, 7].Value = item.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
                worksheet.Cells[rs, 8].Value = item.Price;
                worksheet.Cells[rs, 9].Value = item.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 10].Value = item.PaymentGroup.ToEnumGetDisplayName();
                worksheet.Cells[rs, 11].Value = item.Wallet_Balance;
                worksheet.Cells[rs, 12].Value = item.Wallet_SubBalance;
                worksheet.Cells[rs, 13].Value = item.Account?.SubManagerUserCode;
                worksheet.Cells[rs, 14].Value = item.Account?.ManagerUserCode;
                worksheet.Cells[rs, 15].Value = item.TransactionCode;
                worksheet.Cells[rs, 16].Value = item.OrderIdRef;
                //
                rs++;
                stt++;
            }
            //
            SetBorderTable(worksheet, $"A5:P{rs - 1}");
            worksheet.Cells[$"A6:P{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string, string> InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
    }
}