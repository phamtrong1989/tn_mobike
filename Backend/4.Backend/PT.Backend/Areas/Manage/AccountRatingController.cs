using Microsoft.AspNetCore.Mvc;
using PT.Base;
using System.Threading.Tasks;
using TN.Backend.Controllers;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class AccountRatingsController : BaseController<AccountRating, IAccountRatingService>
    {
        private readonly IAccountRatingItemService _accountRatingItemService;
        private readonly IWalletTransactionService _iWalletTransactionService;

        public AccountRatingsController(IAccountRatingService service, IAccountRatingItemService accountRatingItemService, IWalletTransactionService iWalletTransactionService)
          : base(service)
        {
            _accountRatingItemService = accountRatingItemService;
            _iWalletTransactionService = iWalletTransactionService;
        }

        #region [AccountRating]
        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? PageSize, string key, string sortby, string sorttype, EAccountRatingType? type, EAccountRatingGroup? group)
        => await Service.SearchPageAsync(pageIndex ?? 1, PageSize ?? 100, key, sortby, sorttype, type, group);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] AccountRatingCommand value) => await Service.Create(value);

        [AuthorizePermission("Index")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] AccountRatingCommand value) => await Service.Edit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("Delete/{id}")]
        public async Task<object> Delete(int id) => await Service.Delete(id);
        #endregion

        #region [AccountRaingItem]
        [AuthorizePermission("Index")]
        [HttpGet("SearchPagedRankItem")]
        public async Task<object> SearchPagedRankItem(int? pageIndex, int? PageSize, string key, string sortby, string sorttype, string code)
        => await _accountRatingItemService.SearchPageAsync(pageIndex ?? 1, PageSize ?? 100, key, sortby, sorttype, code);

        [AuthorizePermission("Index")]
        [HttpGet("GetByIdRankItem/{id}")]
        public async Task<object> GetByIdRankItem(int id) => await _accountRatingItemService.GetById(id);

        [AuthorizePermission("Index")]
        [HttpPost("CreateRankItem")]
        public async Task<object> CreateRankItem([FromBody] AccountRatingItemCommand value) => await _accountRatingItemService.Create(value);

        [AuthorizePermission("Index")]
        [HttpPut("EditRankItem")]
        public async Task<object> EditRankItem([FromBody] AccountRatingItemCommand value) => await _accountRatingItemService.Edit(value);

        [AuthorizePermission("Index")]
        [HttpDelete("DeleteRankItem/{id}")]
        public async Task<object> DeleteRankItem(int id) => await _accountRatingItemService.Delete(id);

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await _accountRatingItemService.SearchByAccount(key);

        [AuthorizePermission("AddPoint")]
        [HttpPost("WalletTransactionAdd")]
        public async Task<object> WalletTransactionAdd([FromBody] WalletTransactionCommand value) => await _iWalletTransactionService.Create(value);
        #endregion
    }
}