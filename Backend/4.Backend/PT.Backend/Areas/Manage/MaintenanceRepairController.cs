using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TN.Base;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
using PT.Base;

namespace TN.Backend.Areas.Manage
{
    
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class MaintenanceRepairsController : BaseController<MaintenanceRepair, IMaintenanceRepairService>
    { 
        public MaintenanceRepairsController(IMaintenanceRepairService service)
          : base(service)
        {
        }


        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(
            int pageIndex,
            int pageSize,
            string key,
            EMaintenanceRepairStatus? status,
            DateTime? start,
            DateTime? end,
            int? warehouseId,
            string sortby,
            string sorttype
        ) => await Service.SearchPageAsync(pageIndex, pageSize, key, status, start, end, warehouseId, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id) => await Service.GetById(id);
      
        [AuthorizePermission("Edit")]
        [HttpPost("Create")]
        public async Task<object> Create([FromBody] MaintenanceRepairCommand value) => await Service.Create(value);

        [AuthorizePermission("Edit")]
        [HttpPut("Edit")]
        public async Task<object> Edit([FromBody] MaintenanceRepairCommand value) => await Service.Edit(value);

        [AuthorizePermission("Edit")]
        [HttpPost("Cancel/{id}")]
        public async Task<object> Cancel(int id) => await Service.Cancel(id);

        [HttpPost("Upload")]
        [AuthorizePermission("Edit")]
        public async Task<object> Upload() => await Service.Upload(Request);

        [AuthorizePermission("Edit")]
        [HttpPost("Approve/{id}/{endDate}")]
        public async Task<object> Approve(int id, DateTime endDate) => await Service.Approve(id, endDate);

        #region [MaintenanceRepairItem]
        [AuthorizePermission("Index")]
        [HttpGet("MaintenanceRepairItemSearchPaged")]
        public async Task<object> MaintenanceRepairItemSearchPaged(int pageIndex, int pageSize, int maintenanceRepairId) => await Service.MaintenanceRepairItemSearchPageAsync(pageIndex, pageSize, maintenanceRepairId);

        [AuthorizePermission("Edit")]
        [HttpGet("MaintenanceRepairItemGetById/{id}")]
        public async Task<object> MaintenanceRepairItemGetById(int id) => await Service.MaintenanceRepairItemGetById(id);
  
        [AuthorizePermission("Edit")]
        [HttpPost("MaintenanceRepairItemCreate")]
        public async Task<object> MaintenanceRepairItemCreate([FromBody] MaintenanceRepairItemCommand value) => await Service.MaintenanceRepairItemCreate(value);

        [AuthorizePermission("Edit")]
        [HttpPut("MaintenanceRepairItemEdit")]
        public async Task<object> MaintenanceRepairItemEdit([FromBody] MaintenanceRepairItemCommand value) => await Service.MaintenanceRepairItemEdit(value);

        [AuthorizePermission("Edit")]
        [HttpDelete("MaintenanceRepairItemDelete/{id}")]
        public async Task<object> MaintenanceRepairItemDelete(int id) => await Service.MaintenanceRepairItemDelete(id);

        [AuthorizePermission("Index")]
        [HttpGet("SuppliesSearchKey")]
        public async Task<object> SuppliesSearchKey(string key, int warehouseId) => await Service.SuppliesSearchKey(key, warehouseId);

        [AuthorizePermission("Index")]
        [HttpGet("BikesSearchKey")]
        public async Task<object> BikesSearchKey(string key) => await Service.BikesSearchKey(key);
        #endregion

        [AuthorizePermission("Index")]
        [HttpPost("Report")]
        public async Task<FileContentResult> Report(
            DateTime? from,
            DateTime? to,
            string key,
            EMaintenanceRepairStatus? status,
            int? warehouseId
        )
        => await Service.ExportReport(from, to, key, status, warehouseId);
    }
}