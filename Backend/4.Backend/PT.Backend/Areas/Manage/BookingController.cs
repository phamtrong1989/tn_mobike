using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PT.Base;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;
namespace TN.Backend.Areas.Manage
{

    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class BookingsController : BaseController<Booking, IBookingService>
    { 
        public BookingsController(IBookingService service)
          : base(service)
        {
        }


        [AuthorizePermission("Index")]
        [HttpGet("SearchPaged")]
        public async Task<object> SearchPaged(int? pageIndex, int? pageSize, string key, int? investorId, int? projectId, int? customerGroupId,  int? stationIn, int? accountId, string from, string to, ETicketType? ticketPrice_TicketType, string sortby, string sorttype)
            => await Service.SearchPageAsync(pageIndex ?? 1, pageSize ?? 100, key, investorId, projectId, customerGroupId, stationIn, accountId, from,to, ticketPrice_TicketType, sortby, sorttype);

        [AuthorizePermission("Index")]
        [HttpGet("GetById/{id}")]
        public async Task<object> GetById(int id)
        {
           return  await Service.GetById(id);
        }

        [AuthorizePermission("Index")]
        [HttpGet("SearchByAccount")]
        public async Task<object> SearchByAccount(string key) => await Service.SearchByAccount(key);
    }
}