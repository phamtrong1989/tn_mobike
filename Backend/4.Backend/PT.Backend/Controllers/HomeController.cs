﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace TN.Backend.Controllers
{


    public class HomeController : ControllerBase
    {
        private readonly IWebHostEnvironment _iHostingEnvironment;

        public HomeController(IWebHostEnvironment iHostingEnvironment)
        {
            _iHostingEnvironment = iHostingEnvironment;
        }

        [HttpGet("Index")]
        public object Index()
        {
            return 0;
        }

        [HttpGet("data/image")]
        [ResponseCache(VaryByHeader = "User-Agent", Duration = 30000000)]
        public IActionResult Image(string path, int size, bool? s)
        {
            try
            {
                path = $"{_iHostingEnvironment.WebRootPath}\\{path}";
                var image = System.Drawing.Image.FromFile(path);
                var a = ResizeImage(image, size, s ?? false);
                return File(CopyImageToByteArray(a, GetImageFormat(a)), "image/jpeg");
            }
            catch
            {
                return Content(" ");
            }
        }

        private Bitmap ResizeImage(Image image, int maxSideSize, bool makeItSquare)
        {
            int newWidth;
            int newHeight;

            var oldWidth = image.Width;
            var oldHeight = image.Height;
            Bitmap newImage;
            if (makeItSquare)
            {
                var smallerSide = oldWidth >= oldHeight ? oldHeight : oldWidth;
                var coeficient = maxSideSize / (double) smallerSide;
                newWidth = Convert.ToInt32(coeficient * oldWidth);
                newHeight = Convert.ToInt32(coeficient * oldHeight);
                var tempImage = new Bitmap(image, newWidth, newHeight);
                var cropX = (newWidth - maxSideSize) / 2;
                var cropY = (newHeight - maxSideSize) / 2;
                newImage = new Bitmap(maxSideSize, maxSideSize);
                var tempGraphic = Graphics.FromImage(newImage);
                tempGraphic.SmoothingMode = SmoothingMode.AntiAlias;
                tempGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                tempGraphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                tempGraphic.DrawImage(tempImage, new Rectangle(0, 0, maxSideSize, maxSideSize), cropX, cropY,
                    maxSideSize, maxSideSize, GraphicsUnit.Pixel);
            }
            else
            {
                var maxSide = oldWidth >= oldHeight ? oldWidth : oldHeight;

                if (maxSide > maxSideSize)
                {
                    var coeficient = maxSideSize / (double) maxSide;
                    newWidth = Convert.ToInt32(coeficient * oldWidth);
                    newHeight = Convert.ToInt32(coeficient * oldHeight);
                }
                else
                {
                    newWidth = oldWidth;
                    newHeight = oldHeight;
                }

                newImage = new Bitmap(image, newWidth, newHeight);
            }

            return newImage;
        }

        public ImageFormat GetImageFormat(Image img)
        {
            if (img.RawFormat.Equals(ImageFormat.Png))
                return ImageFormat.Png;
            if (img.RawFormat.Equals(ImageFormat.Gif))
                return ImageFormat.Gif;
            if (img.RawFormat.Equals(ImageFormat.Icon))
                return ImageFormat.Icon;
            return ImageFormat.Jpeg;
        }

        private byte[] CopyImageToByteArray(Image theImage, ImageFormat type)
        {
            using var memoryStream = new MemoryStream();
            theImage.Save(memoryStream, type);
            return memoryStream.ToArray();
        }
    }
}