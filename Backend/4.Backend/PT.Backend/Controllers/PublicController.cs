using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Backend.Controllers;

namespace TN.Backend.Areas.Manage
{
    [Area("Work")]
    [Route("api/[area]/[controller]")]
    [ApiController]

    public class PublicController : BaseController<Station, IDashboardService>
    {
        public PublicController(IDashboardService service)
          : base(service)
        {
        }

        [HttpGet("Stations/{projectId}")]
        public async Task<object> Stations(int projectId) => await Service.Stations(projectId);

        [HttpGet("Bikes")]
        public async Task<object> Bikes() => await Service.Bikes();
    }
}