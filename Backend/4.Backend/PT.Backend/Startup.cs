﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PT.Base;
using PT.Base.Services;
using PT.Domain.Model;
using Serilog;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure;
using TN.Infrastructure.Interfaces;
using TN.Infrastructure.Repositories;
using TN.Infrastructure.Repositories.Work;
using TN.Shared;
using Log = Serilog.Log;

namespace PT.Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.RollingFile(Path.Combine(env.ContentRootPath, "logs/log-{Date}.txt"))
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDataProtection().SetApplicationName("mobike_manager");
            services.AddRouting();
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContextPool<ApplicationContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("TN.Infrastructure")));

            services.AddIdentity<ApplicationUser, ApplicationRole>().AddEntityFrameworkStores<ApplicationContext>().AddDefaultTokenProviders();
            services.AddSignalR();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        if (context.HttpContext.Request.Host.Host != "localhost" && (context.HttpContext.Request.Path.StartsWithSegments("/hubs/category/negotiate") || context.HttpContext.Request.Path.StartsWithSegments("/hubs/eventSystem/negotiate")))
                        {
                            var accessToken = context.Request.Query["access_token"].ToString();
                        }
                        else if (context.HttpContext.Request.Host.Host != "localhost" && (context.HttpContext.Request.Path.StartsWithSegments("/hubs/category") || context.HttpContext.Request.Path.StartsWithSegments("/hubs/eventSystem")))
                        {
                            var accessToken = context.Request.Query["access_token"].ToString();
                            if (!string.IsNullOrEmpty(context.Request.Headers["Authorization"].ToString()))
                            {
                                accessToken = context.Request.Headers["Authorization"].ToString();
                            }

                            if (string.IsNullOrEmpty(accessToken))
                            {
                                context.Response.StatusCode = 401;
                            }
                            else
                            {
                                if (accessToken.StartsWith("Bearer"))
                                {
                                    accessToken = accessToken.Replace("Bearer ", "");
                                }

                                var validate = TN.Utility.Function.ValidateToken(accessToken, Configuration["ApiSettings:TokenIssuer"], Configuration["ApiSettings:TokenAudience"], Configuration["ApiSettings:TokenKey"]);
                                if (validate > 0)
                                {
                                    context.Token = accessToken;
                                }
                                else
                                {
                                    context.Response.StatusCode = 401;
                                }
                            }
                        }
                        return Task.CompletedTask;
                    }
                };
                options.TokenValidationParameters =
                    new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["ApiSettings:TokenIssuer"],
                        ValidAudience = Configuration["ApiSettings:TokenAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["ApiSettings:TokenKey"]))
                    };
            });

            // Add Custom Claims processor
            services.Configure<BaseSettings>(Configuration.GetSection("BaseSettings"));
            services.Configure<LogSettings>(Configuration.GetSection("LogSettings"));
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<AuthorizeSettings>(Configuration.GetSection("AuthorizeSettings"));
            services.Configure<ApiSettings>(Configuration.GetSection("ApiSettings"));
            services.Configure<List<NotificationTemp>>(Configuration.GetSection("NotificationTempSettings"));

            services.AddMemoryCache();
            //services.AddHttpsRedirection(options =>
            //{
            //    options.HttpsPort = 443;
            //});

            var supportedCultures = ListData.ListLanguage.Select(x => new CultureInfo(x.Id)).ToArray();
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("en");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            // Ngôn ngữ End
            // Add application services.
            services.AddScoped(typeof(IEntityBaseRepository<>), typeof(EntityBaseRepository<>));

            services.AddScoped<IEmailSenderRepository, EmailSenderRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRoleControllerRepository, RoleControllerRepository>();
            services.AddScoped<ILogRepository, LogRepository>();
            services.AddScoped<IEmailSenderRepository, EmailSenderRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IRoleDetailRepository, RoleDetailRepository>();
            services.AddScoped<IParameterRepository, ParameterRepository>();
            services.AddScoped<ICustomerGroupRepository, CustomerGroupRepository>();
            services.AddScoped<IStationRepository, StationRepository>();
            services.AddScoped<IBikeRepository, BikeRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<ITicketPriceRepository, TicketPriceRepository>();
            services.AddScoped<IProjectAccountRepository, ProjectAccountRepository>();
            services.AddScoped<IDockRepository, DockRepository>();
            services.AddScoped<ICampaignRepository, CampaignRepository>();
            services.AddScoped<IVoucherCodeRepository, VoucherCodeRepository>();
            services.AddScoped<IRoleGroupRepository, RoleGroupRepository>();
            services.AddScoped<IRoleActionRepository, RoleActionRepository>();
            services.AddScoped<IBookingRepository, BookingRepository>();
            services.AddScoped<IRoleAreaRepository, RoleAreaRepository>();
            services.AddScoped<IWalletTransactionRepository, WalletTransactionRepository>();
            services.AddScoped<IWalletRepository, WalletRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<IFeedbackRepository, FeedbackRepository>();
            services.AddScoped<ITicketPriceRepository, TicketPriceRepository>();
            services.AddScoped<IInvestorRepository, InvestorRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IDistrictRepository, DistrictRepository>();
            services.AddScoped<IColorRepository, ColorRepository>();
            services.AddScoped<IModelRepository, ModelRepository>();
            services.AddScoped<IProducerRepository, ProducerRepository>();
            services.AddScoped<IWarehouseRepository, WarehouseRepository>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();
            services.AddScoped<ITicketPrepaidRepository, TicketPrepaidRepository>();
            services.AddScoped<IDistrictRepository, DistrictRepository>();
            services.AddScoped<IRedeemPointRepository, RedeemPointRepository>();
            services.AddScoped<IGPSDataRepository, GPSDataRepository>();
            services.AddScoped<IOpenLockRequestRepository, OpenLockRequestRepository>();
            services.AddScoped<IOpenLockHistoryRepository, OpenLockHistoryRepository>();
            services.AddScoped<IDepositEventRepository, DepositEventRepository>();
            services.AddScoped<IContentPageRepository, ContentPageRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<INotificationJobRepository, NotificationJobRepository>();
            services.AddScoped<ISuppliesRepository, SuppliesRepository>();
            services.AddScoped<IRedeemPointRepository, RedeemPointRepository>();
            services.AddScoped<IImportBillRepository, ImportBillRepository>();
            services.AddScoped<IExportBillRepository, ExportBillRepository>();
            services.AddScoped<ICustomerComplaintRepository, CustomerComplaintRepository>();
            services.AddScoped<IExportBillItemRepository, ExportBillItemRepository>();
            services.AddScoped<IMaintenanceRepairRepository, MaintenanceRepairRepository>();
            services.AddScoped<IMaintenanceRepairItemRepository, MaintenanceRepairItemRepository>();
            services.AddScoped<ISuppliesWarehouseRepository, SuppliesWarehouseRepository>();
            services.AddScoped<IAppVersionRepository, AppVersionRepository>();
            services.AddScoped<IEmailBoxRepository, EmailBoxRepository>();
            services.AddScoped<IEmailManageRepository, EmailManageRepository>();
            services.AddScoped<IBookingFailRepository, BookingFailRepository>();
            services.AddScoped<IRFIDBookingRepository, RFIDBookingRepository>();
            services.AddScoped<IAccountBlackListRepository, AccountBlackListRepository>();
            services.AddScoped<ICyclingWeekRepository, CyclingWeekRepository>();
            services.AddScoped<IBannerRepository, BannerRepository>();
            services.AddScoped<IBannerGroupRepository, BannerGroupRepository>();

            services.AddScoped<ICustomerGroupResourceRepository, CustomerGroupResourceRepository>();
            services.AddScoped<IProjectResourceRepository, ProjectResourceRepository>();
            services.AddScoped<IStationResourceRepository, StationResourceRepository>();
            services.AddScoped<IDiscountCodeRepository, DiscountCodeRepository>();
            services.AddScoped<IAccountRatingRepository, AccountRatingRepository>();
            services.AddScoped<IAccountRatingItemRepository, AccountRatingItemRepository>();

            //  services.AddScoped<IChartsTopRepository, ChartsTopRepository>();
            //
            services.AddScoped<IRolesService, RolesService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<ICustomerGroupService, CustomerGroupService>();
            services.AddScoped<IInitDataService, InitDataService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ICustomersService, CustomersService>();
            services.AddScoped<IStationService, StationService>();
            services.AddScoped<IBikeService, BikeService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IDockService, DockService>();
            services.AddScoped<ICampaignService, CampaignService>();
            services.AddScoped<IRoleGroupService, RoleGroupService>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<IRoleAreaService, RoleAreaService>();
            services.AddScoped<IRoleControllerService, RoleControllerService>();
            services.AddScoped<IWalletTransactionService, WalletTransactionService>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IFeedbackService, FeedbackService>();
            services.AddScoped<ITicketPriceService, TicketPriceService>();
            services.AddScoped<IProjectAccountService, ProjectAccountService>();
            services.AddScoped<IBikeReportRepository, BikeReportRepository>();
            services.AddScoped<IDataInfoRepository, DataInfoRepository>();

            //services.AddScoped<IInfrastructurDashboardService, InfrastructurDashboardService>();
            services.AddScoped<IInvestorService, InvestorService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IColorService, ColorService>();
            services.AddScoped<IDistrictService, DistrictService>();
            services.AddScoped<IModelService, ModelService>();
            services.AddScoped<IProducerService, ProducerService>();
            services.AddScoped<ILanguageService, LanguageService>();
            services.AddScoped<ILanguageService, LanguageService>();
            services.AddScoped<IDashboardService, DashboardService>();
            services.AddScoped<ITicketPrepaidService, TicketPrepaidService>();
            services.AddScoped<ILogService, LogService>();
            services.AddScoped<IWarehouseService, WarehouseService>();
            services.AddScoped<IContentPageService, ContentPageService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<INotificationJobService, NotificationJobService>();
            services.AddScoped<ISuppliesService, SuppliesService>();
            services.AddScoped<IRedeemPointService, RedeemPointService>();
            services.AddScoped<IBikeReportService, BikeReportService>();
            services.AddScoped<IImportBillService, ImportBillService>();
            services.AddScoped<ICustomerComplaintService, CustomerComplaintService>();
            services.AddScoped<IExportBillService, ExportBillService>();
            services.AddScoped<IMaintenanceRepairService, MaintenanceRepairService>();
            services.AddScoped<ITroubleshootingCenterService, TroubleshootingCenterService>();
            services.AddScoped<ICollaboratorsSevice, CollaboratorsSevice>();
            services.AddScoped<ILogWorkService, LogWorkService>();
            services.AddScoped<IChartReportService, ChartReportService>();
            services.AddScoped<IBannerGroupService, BannerGroupService>();
            services.AddScoped<IDashboardService, DashboardService>();
            services.AddScoped<IAccountRatingService, AccountRatingService>();
            services.AddScoped<IAccountRatingItemService, AccountRatingItemService>();

            services.AddScoped<IVehicleRecallPriceRepository, VehicleRecallPriceRepository>();
            //Gzip
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal);
            services.Configure<FormOptions>(options => { options.MultipartBodyLengthLimit = 209_715_200; });
            services.AddDistributedMemoryCache();
            services.AddResponseCompression();
            services.AddCors();
            //services.AddSwaggerGen();
            //services.AddSwaggerDocumentation();
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                 {
                     options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                     options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                     options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                 }
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
            AppHttpContext.Services = app.ApplicationServices;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.UseStaticFiles();
            //  app.UseSwaggerDocumentation();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(builder =>
            {
                builder
                    .WithOrigins(Configuration["ApiSettings:UseCors"].Split(','))
                    .WithMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            });

            app.UseEndpoints(e =>
            {
                e.MapHub<CategoryHub>("/hubs/category");
                e.MapHub<EventSystemHub>("/hubs/eventSystem");
                e.MapControllers();
            });
        }
    }
}
