﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Monitor.Settings;

namespace TN.Monitor
{
    class SyncJobScheduler
    {
        public void Start()
        {
            try
            {
                new MonitorProcess().Monitor();
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProgramInit_Start", e.ToString());
            }
        }

        public void Stop()
        {
            try
            {
                Utilities.CloseLog();
                Environment.Exit(0);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("ProgramInit_Stop", e.ToString());
            }
        }
    }
}
