﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TN.Monitor.Entity;
using TN.Monitor.Settings;

namespace TN.Monitor
{
    class ProcessInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        public string Ram { get; set; }
        public string Physical { get; set; }
        public ProcessInfo()
        {

        }

        public ProcessInfo(string id, string name, string status, string username, string description, string ram, string physical)
        {
            Id = id;
            Name = name;
            Status = status;
            Username = username;
            Description = description;
            Ram = ram;
            Physical = physical;
        }
    }

    class MonitorProcess
    {
        #region Obtain memory information API
        [DllImport("kernel32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GlobalMemoryStatusEx(ref MEMORY_INFO mi);

        //Define the information structure of memory
        [StructLayout(LayoutKind.Sequential)]
        private struct MEMORY_INFO
        {
            public uint dwLength; //Current structure size
            public uint dwMemoryLoad; //Current memory utilization
            public ulong ullTotalPhys; //Total physical memory size
            public ulong ullAvailPhys; //Available physical memory size
            public ulong ullTotalPageFile; //Total Exchange File Size
            public ulong ullAvailPageFile; //Total Exchange File Size
            public ulong ullTotalVirtual; //Total virtual memory size
            public ulong ullAvailVirtual; //Available virtual memory size
            public ulong ullAvailExtendedVirtual; //Keep this value always zero
        }
        #endregion

        public void Monitor()
        {
            try
            {
                Task.Run(MonitorSystem);
            }
            catch (Exception)
            {
                //
            }
        }

        private async void MonitorSystem()
        {
            MonitorCpu monitorCpu = new MonitorCpu
            {
                CategoryNameCpu = "Processor",
                CategoryNameRam = "Memory",
                CounterNameCpu = "% Processor Time",
                CounterNameRam = "Available MBytes",
                InstanceNameCpu = "_Total"
            };

            await MonitorCpuRam(monitorCpu);
        }

        private static DateTime lastTimeSendMail = DateTime.MinValue;

        private async Task MonitorCpuRam(MonitorCpu monitor)
        {
            try
            {
                var totalRamStr = FormatSize(GetTotalPhys());
                var totalRam = Convert.ToDouble(totalRamStr.Split(' ').FirstOrDefault());
                PerformanceCounter processorTimeCounter = new PerformanceCounter(monitor.CategoryNameCpu, monitor.CounterNameCpu, monitor.InstanceNameCpu);
                PerformanceCounter memoryUsage = new PerformanceCounter(monitor.CategoryNameRam, monitor.CounterNameRam);

                while (true)
                {
                    var valueCpu = processorTimeCounter.NextValue();

                    var valueRamAvailable = memoryUsage.NextValue();

                    var valueRam = Math.Round((100 - ((valueRamAvailable / 1024) / totalRam) * 100), 2);

                    if (valueRam >= AppSettings.WarningRam || valueCpu >= AppSettings.WarningCpu)
                    {
                        var message = $"SERVER: {AppSettings.Server} | Total CPU used: {valueCpu} | Total RAM used: {valueRam}";
                        //new SMTPEmail().SendEmail(message);
                        GetAllProcess(message);
                    }

                    Utilities.WriteOperationLog("Monitor", $"SERVER: {AppSettings.Server} | Total CPU used: {valueCpu} % | Total RAM used: {valueRam} %");

                    Console.WriteLine($"SERVER: {AppSettings.Server} | Total CPU used: {valueCpu} % | Total RAM used: {valueRam} %");

                    await Task.Delay(10000);
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MonitorSystem", $"Error: {e.Message}");
            }
        }

        public void GetAllProcess(string message)
        {
            try
            {
                Task.Run(() => PerformanceProcess(message));
            }
            catch (Exception)
            {
                //
            }
        }

        public async void PerformanceProcess(string message)
        {
            await PerformanceAll(message);
        }

        private static List<NewLine> listDataProcess = new List<NewLine>();

        private void CheckOneProcess(Process process)
        {
            double cpu = 0;
            
            var ram = BytesToReadableValue(process.WorkingSet64);
            NewLine newLine = new NewLine(process.ProcessName, $"{cpu}", ram);
            if (newLine != null)
            {
                listDataProcess.Add(newLine);
            }
        }

        public async Task PerformanceAll(string message)
        {
            try
            {
                //double f = 1024.0;

                var ListProcess = Process.GetProcesses();
                var result = Parallel.ForEach(ListProcess, CheckOneProcess);

                if (result.IsCompleted)
                {
                    var list = listDataProcess.OrderByDescending(x => x.RAM).ToList();
                    foreach (var line in list)
                    {
                        message += line.ToString();
                    }

                    var time = DateTime.Now;
                    var totalMin = (time - lastTimeSendMail).TotalMinutes;

                    Utilities.WriteOperationLog("Monitor Detail", $"{message}");

                    if (totalMin > AppSettings.TimeSend)
                    {
                        new SMTPEmail().SendEmail(message);

                        lastTimeSendMail = DateTime.Now;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception)
            {
                //
            }

            await Task.Delay(1);
        }

        private string GetProcessInstanceName(int pid)
        {
            PerformanceCounterCategory cat = new PerformanceCounterCategory("Process");

            string[] instances = cat.GetInstanceNames();

            var result = "";
            foreach (string instance in instances)
            {

                using (PerformanceCounter cnt = new PerformanceCounter("Process", "ID Process", instance, true))
                {
                    int val = (int)cnt.RawValue;
                    if (val == pid)
                    {
                        result = instance;
                    }
                }
            }

            return result;
        }

        public string BytesToReadableValue(long number)
        {
            List<string> suffixes = new List<string> { " B", " KB", " MB", " GB", " TB", " PB" };

            for (int i = 0; i < suffixes.Count; i++)
            {
                long temp = number / (int)Math.Pow(1024, i + 1);

                if (temp == 0)
                {
                    return (number / (int)Math.Pow(1024, i)) + suffixes[i];
                }
            }

            return number.ToString();
        }

        private string FormatSize(double size)
        {
            double d = (double)size;
            int i = 0;
            while ((d > 1024) && (i < 5))
            {
                d /= 1024;
                i++;
            }
            string[] unit = { "B", "KB", "MB", "GB", "TB" };
            return ($"{Math.Round(d, 2)} {unit[i]}");
        }

        private ulong GetTotalPhys()
        {
            MEMORY_INFO mi = GetMemoryStatus();
            return mi.ullTotalPhys;
        }

        private MEMORY_INFO GetMemoryStatus()
        {
            MEMORY_INFO mi = new MEMORY_INFO();
            mi.dwLength = (uint)Marshal.SizeOf(mi);
            GlobalMemoryStatusEx(ref mi);
            return mi;
        }
    }

    class NewLine
    {
        public string Name { get; set; }
        public string CPU { get; set; }
        public double RAM { get; set; }
        public string END { get; set; }

        public NewLine()
        {

        }

        public NewLine(string name, string cpu, string ram)
        {
            Name = name;
            CPU = cpu;
            try
            {
                var data = ram.Split(' ');
                RAM = Convert.ToDouble(data.FirstOrDefault());
                END = data.LastOrDefault();
            }
            catch (Exception)
            {
                //
            }
        }

        public override string ToString()
        {
            //return $" <br /> Name: {Name} | CPU: {CPU} % | RAM: {RAM} {END}";
            return $" <br /> Name: {Name} | RAM: {RAM} {END}";
        }
    }
}
