﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TN.Monitor.Entity;
using TN.Monitor.Settings;

namespace TN.Monitor
{
    class SMTPEmail
    {

        private async Task Send(EmailObject email, string sendTo, string emailTemplate)
        {
            try
            {
                ResponseSend responseSend;
                SmtpClient client = new SmtpClient(email.Host, email.Port) {EnableSsl = true};
                MailMessage message = new MailMessage();

                message.From = new MailAddress(email.EmailHost, email.DisplayName);
                message.Body = emailTemplate;
                message.Subject = email.Title;
                message.IsBodyHtml = true;

                var multi = sendTo.Split(';');
                foreach (var em in multi)
                {
                    message.To.Add(new MailAddress(em));
                }

                NetworkCredential myCredential = new NetworkCredential(email.EmailHost, email.Password, "");
                client.Credentials = myCredential;
                try
                {
                    client.Send(message);
                    responseSend = new ResponseSend(200, "Send email success!");
                    Utilities.WriteOperationLog($"[SEND EMAIL ({responseSend.Status}) ] : ",
                        $"[SEND EMAIL TO {sendTo} SUCCESSFUL !] [ MESSAGE : {emailTemplate} ]");
                }
                catch (Exception e)
                {
                    responseSend = new ResponseSend(404, "Send email fail!");
                    Utilities.WriteErrorLog($"[SEND EMAIL ({responseSend.Status})] : ",
                        $"[SEND EMAIL TO {sendTo} FAIL. TRY AGAIN! ] [MESSAGE : {emailTemplate} ] | Error: {e.Message}");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[SEND EMAIL", $"ERROR: {e.Message}");
            }

            await Task.Delay(1);
        }

        private async void SendMails(string message)
        {
            EmailObject email = new EmailObject();
            email.Host = "smtp.gmail.com";
            email.Port = 587;
            email.Title = "Cảnh báo CPU - RAM TNGO";
            email.DisplayName = "TNGO WARNING";
            email.EmailHost = "toilatieuanhhung2@gmail.com";
            email.Password = "phamtienbac";


            await Send(email, AppSettings.Emails, message);
        }

        public void SendEmail(string message)
        {
            try
            {
                Task.Run(() => SendMails(message));
            }
            catch (Exception)
            {
                //
            }
        }
    }
}
