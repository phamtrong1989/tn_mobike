﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.Monitor.Entity
{
    class MonitorCpu
    {
        public string CategoryNameCpu { get; set; }
        public string CounterNameCpu { get; set; }
        public string InstanceNameCpu { get; set; }
        public string CategoryNameRam { get; set; }
        public string CounterNameRam { get; set; }

        public MonitorCpu()
        {

        }

        public MonitorCpu(string categoryNameCpu, string counterNameCpu, string instanceNameCpu, string categoryNameRam, string counterNameRam)
        {
            CategoryNameCpu = categoryNameCpu;
            CounterNameCpu = counterNameCpu;
            InstanceNameCpu = instanceNameCpu;
            CategoryNameRam = categoryNameRam;
            CounterNameRam = counterNameRam;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
