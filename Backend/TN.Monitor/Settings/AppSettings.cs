﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Monitor.Settings
{
    class AppSettings
    {
        // SERVICES NAME
        public static string ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        public static string ServiceDisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"];
        public static string ServiceDescription = ConfigurationManager.AppSettings["ServiceDescription"];

        public static int WarningCpu = Convert.ToInt32(ConfigurationManager.AppSettings["WarningCpu"]);
        public static int WarningRam = Convert.ToInt32(ConfigurationManager.AppSettings["WarningRam"]);
        public static int TimeSend = Convert.ToInt32(ConfigurationManager.AppSettings["TimeSend"]);
        public static string Emails = ConfigurationManager.AppSettings["Emails"];
        public static string ProcessName = ConfigurationManager.AppSettings["ProcessName"];
        public static string Server = ConfigurationManager.AppSettings["Server"];
    }
}
