﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TN.Mobike.LookOmniServices.Core;
using TN.Mobike.LookOmniServices.Core.OmniMina;
using TN.Mobike.LookOmniServices.Job;
using TN.Mobike.LookOmniServices.Settings;
using Topshelf;

namespace TN.Mobike.LookOmniServices
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TopshelfExitCode exitCode = HostFactory.Run(x =>
                {
                    x.Service<SyncJobScheduler>(s =>
                    {
                        s.ConstructUsing(name => new SyncJobScheduler());
                        s.WhenStarted(cb => cb.Start());
                        s.WhenStopped(cb => cb.Stop());
                        s.WhenShutdown(cb => cb.Stop());
                    });
                    x.RunAsLocalSystem();

                    //#if DEBUG
                    //                    x.RunAsPrompt();
                    //#else
                    //                                    x.RunAsLocalSystem();                            
                    //#endif

                    x.SetServiceName(AppSettings.ServiceName);
                    x.SetDisplayName(AppSettings.ServiceDisplayName);
                    x.SetDescription(AppSettings.ServiceDescription);

                });

            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("Program_Main", ex.ToString());
            }
        }
    }
}
