﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Mobike.LookOmniServices.Entity;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Model
{
    class OpenLockRequestModel
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public List<OpenLockRequest> GetAllOpenLockRequests(DateTime nowTime)
        {
            List<OpenLockRequest> list = new List<OpenLockRequest>();
            list = sqlHelper.GetAll<OpenLockRequest>(
                $"Where CreatedDate >= '{DateTime.Now.AddSeconds(-AppSettings.LimitTimeOpen):yyyy-MM-dd HH:mm:ss.ffffff}' AND CreatedDate <= '{DateTime.Now:yyyy-MM-dd HH:mm:ss.ffffff}'");
            return list;
        }

        public void DeleteOpenLockRequest(int id)
        {
            sqlHelper.DeleteRecord<OpenLockRequest>($"id = {id}");
        }
    }
}
