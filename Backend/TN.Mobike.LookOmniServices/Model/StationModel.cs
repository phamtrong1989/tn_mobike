﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Mobike.LookOmniServices.Entity;

namespace TN.Mobike.LookOmniServices.Model
{
    class StationModel
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public List<Station> GetAll()
        {
            var list = sqlHelper.GetAll<Station>();
            return list;
        }
    }
}
