﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Mobike.LookOmniServices.Entity;

namespace TN.Mobike.LookOmniServices.Model
{
    class OpenLockHistoryModel
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public void InsertOpenLockHistory(OpenLockHistory openLock)
        {
            sqlHelper.InsertOne(openLock);
        }
    }
}
