﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TN.Mobike.LookOmniServices.Entity;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Model
{
    class DockModel
    {
        private SqlHelper sqlHelper = new SqlHelper();

        public Dock GetOneDock(string imei)
        {
            var dock = (Dock)sqlHelper.GetOne<Dock>($"IMEI = '{imei}'");

            return dock;
        }

        public void UpdateDock(Dock dock)
        {
            dock.UpdatedDate = DateTime.Now;

            sqlHelper.UpdateOne(dock, $"id = {dock.Id}");
        }

        public void InsertDock(Dock dock)
        {
            dock.CreatedDate = DateTime.Now;
            dock.UpdatedDate = DateTime.Now;

            sqlHelper.InsertOne<Dock>(dock);
        }

        // update Bike set Lat = 1, Long = 1, StationId = 1 Where DockId = 1

        public void UpdateBike(int dockId, int stationId, double lat, double l)
        {
            try
            {
                if (sqlHelper.CheckExist<Bike>($"DockId = {dockId}"))
                {
                    using (var db = sqlHelper.GetConnection())
                    {
                        var query = $"update Bike set Lat = {lat}, Long = {l}, StationId = {stationId} Where DockId = {dockId}";
                        db.Execute(query);
                    }
                    Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update Bike has DockId = {dockId} successful !");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update Bike has DockId = {dockId} fail ! : {e}");
            }
        }



        public void UpdateStation(string imei, int stationId)
        {
            try
            {
                using (var db = sqlHelper.GetConnection())
                {
                    var query = $"update Dock SET StationId = {stationId} WHERE IMEI = '{imei}'";
                    db.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update All record table Dock connection false successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update All record table Dock fail ! : {e}");
            }
        }

        public void UpdateStatus()
        {
            try
            {
                using (var db = sqlHelper.GetConnection())
                {
                    var query = "update Dock SET ConnectionStatus = 0 WHERE ConnectionStatus = 1";
                    db.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update All record table Dock connection false successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update All record table Dock fail ! : {e}");
            }
        }

        public void UpdateStatus(string imei, int status)
        {
            try
            {
                using (var db = sqlHelper.GetConnection())
                {
                    var query = $"update Dock SET ConnectionStatus = {status} WHERE IMEI = '{imei}'";
                    db.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update record table Dock connection false Imei {imei} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.Dock.UpdateStatus ]", $"Update record table Dock connection false Imei {imei} fail ! : {e}");
            }
        }
    }
}
