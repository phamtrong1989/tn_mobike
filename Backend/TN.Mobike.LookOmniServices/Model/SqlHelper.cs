﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MongoDB.Bson;
using MongoDB.Driver;
using TN.Mobike.LookOmniServices.Entity;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Model
{
    class SqlHelper
    {
        #region SQL

        public static bool CheckConnection(string connectionString = "")
        {
            var cnn = connectionString == "" ? AppSettings.ConnectionString : connectionString;
            using (SqlConnection connection = new SqlConnection(cnn))
            {
                try
                {
                    connection.Open();
                    Console.WriteLine("Connection SQL DONE !");
                    return true;
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Connection SQL FAIL !");
                    Utilities.WriteErrorLog("Check-Connection", $"{cnn} | Error: {e}");
                    return false;
                }
            }
        }

        public SqlConnection GetConnection(string connection = "")
        {
            var cnn = connection == "" ? AppSettings.ConnectionString : connection;
            var sqlConnection = new SqlConnection(cnn);
            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            return sqlConnection;
        }

        public object GetOne<T>(string where = "", string conn = "")
        {
            try
            {
                using (var db = GetConnection(conn))
                {
                    var query = $"SELECT TOP(1) * FROM {typeof(T).Name} Where {where}";
                    var list = db.Query<T>(query).FirstOrDefault();

                    Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.GetOne ]", $"Get one record from table {typeof(T).Name} - {where} - successful !");

                    return list;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.GetOne ]", $"Get one record from table {typeof(T).Name} - {where} -  fail ! : {e}");
                return null;
            }
        }

        public List<T> GetAll<T>(string where = "")
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = $"SELECT * FROM {typeof(T).Name} {where}";
                    var list = db.Query<T>(query).ToList();

                    //Utilities.WriteOperationLog("[ SqlHelper.GetAll ]", $"Get all record from table {typeof(T).Name} successful !");

                    return list;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ SqlHelper.GetAll ]", $"Get all record from table {typeof(T).Name} fail ! : {e}");
                return null;
            }
        }

        public void UpdateOne<T>(T obj, string where)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>(where, false);
                    db.Execute(query, obj);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.UpdateOne ]", $"Update record table {typeof(T).Name} has {where} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.UpdateOne ]", $"Update record table {typeof(T).Name} has {where} fail ! : {e}");
            }
        }

        public void InsertMany<T>(List<T> list)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>("");
                    db.Execute(query, list);
                }

                Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.InsertMany ]", $"Insert {list.Count} record to table {typeof(T).Name} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.InsertMany ]", $"Insert {list.Count} record to table {typeof(T).Name}  fail ! : {e}");
            }
        }

        public void InsertOne<T>(T obj)
        {
            try
            {
                using (var db = GetConnection())
                {
                    var query = GetQuery<T>("");
                    db.Execute(query, obj);
                }

                Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.InsertOne ]", $"Insert record to table {typeof(T).Name} successful [ {obj.ToString()} ]!");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.InsertOne ]", $"Insert record to table {typeof(T).Name} fail! : {e} -- [ {obj.ToString()} ]");
            }
        }

        public void DeleteRecord<T>(string where)
        {
            try
            {
                using (var connection = GetConnection())
                {
                    var query = $"DELETE FROM {typeof(T).Name} WHERE {where}";
                    connection.Execute(query);
                }
                Utilities.WriteOperationLog($"[ SqlHelper.{typeof(T).Name}.DeleteRecord ]", $"Delete record {typeof(T).Name} Where {where} successful !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.DeleteRecord ]", $"[ ERROR: {e} ]");
            }
        }

        public bool CheckExist<T>(string where = "")
        {
            try
            {
                using (var connection = GetConnection())
                {
                    var query = $"SELECT * FROM {typeof(T).Name} Where {where}";
                    var result = connection.Query(query).Any();

                    return result;
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog($"[ SqlHelper.{typeof(T).Name}.CheckExist ]", $"[ ERROR: {e} ]");
                return false;
            }
        }

        #endregion


        #region MongoDB

        public static IMongoCollection<BsonDocument> collectionGPS = null;

        public static void OpenConnectMongoDb()
        {
            try
            {
                MongoClient dbClient = new MongoClient(AppSettings.ConnectionMongoDb);
                var database = dbClient.GetDatabase($"{AppSettings.DatabaseNameMongo}-{DateTime.Now:yyyyMM}");
                collectionGPS = database.GetCollection<BsonDocument>(AppSettings.TableGPSNameMongo);

                Utilities.WriteOperationLog("[SqlHelper.OpenConnectMongoDb]", $"OPEN CONNECT TO {AppSettings.ConnectionMongoDb} SUCCESSFUL !");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[SqlHelper.OpenConnectMongoDb]", $"[ERROR: {e}]");
            }
        }

        public static void InsertGPS(string imei, double lat, double l, string requestId = "")
        {

            try
            {
                if (collectionGPS == null)
                {
                    OpenConnectMongoDb();
                }
                else
                {
                    GPSData gps = new GPSData(imei, lat, l, DateTime.Now);
                    collectionGPS.InsertOneAsync(gps.ToBsonDocument());

                    Utilities.WriteOperationLog("[SqlHelper.InsertGPS]", $"INSERT SUCCESS GPSData: {gps.ToString()}");

                    new MongoUtilities().AddLog(0, "D0 (InsertGPS) | Vị trí (Thêm GPS)", requestId ,imei, MongoUtilities.EnumMongoLogType.End, $"INSERT SUCCESS GPSData: {gps.ToString()}");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[SqlHelper.InsertGPS]", $"[ERROR: {e}]");
                new MongoUtilities().AddLog(0, "D0 (InsertGPS.Error) | Vị trí (Thêm GPS.Lỗi)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"[ERROR: {e}]");
            }
        }

        #endregion


        #region Gennerate

        public string GetQuery<T>(string where, bool isInsert = true)
        {
            var listProperties = typeof(T).GetProperties();
            if (isInsert)
            {
                var propertiesString = "";
                var propertiesValues = "";
                foreach (var prpInfo in listProperties)
                {
                    if (prpInfo == listProperties.Last())
                    {
                        propertiesString += $"{prpInfo.Name.ToLower()}";
                        propertiesValues += $"@{prpInfo.Name.ToLower()}";
                    }
                    else if (prpInfo.Name.ToLower() == "id")
                    {
                        continue;
                    }
                    else
                    {
                        propertiesString += $"{prpInfo.Name.ToLower()},";
                        propertiesValues += $"@{prpInfo.Name.ToLower()},";
                    }
                }
                return $"INSERT INTO {typeof(T).Name}({propertiesString}) VALUES ({propertiesValues})";
            }
            else
            {
                var updateValues = "";
                foreach (var prpInfo in listProperties)
                {
                    if (prpInfo == listProperties.Last())
                    {
                        updateValues += $"{prpInfo.Name.ToLower()} = @{prpInfo.Name.ToLower()}";
                    }
                    else if (prpInfo.Name.ToLower() == "id")
                    {
                        continue;
                    }
                    else
                    {
                        updateValues += $"{prpInfo.Name.ToLower()} = @{prpInfo.Name.ToLower()},";
                    }
                }
                return $"UPDATE {typeof(T).Name} SET {updateValues} WHERE {where}";
            }
        }

        #endregion
    }
}
