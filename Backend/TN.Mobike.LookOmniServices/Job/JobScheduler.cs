﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using TN.Mobike.LookOmniServices.Core;
using TN.Mobike.LookOmniServices.Entity;
using TN.Mobike.LookOmniServices.Model;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Job
{
    class JobScheduler
    {
        private static Task<IScheduler> _scheduler;
        public static List<Station> ListStations = new List<Station>();
        private static StationModel StationModel = new StationModel();
        public static void Start()
        {
            try
            {
                ListStations = StationModel.GetAll();

                StdSchedulerFactory factory = new StdSchedulerFactory();
                _scheduler = factory.GetScheduler();
                _scheduler.Result.Start();

                IJobDetail job = JobBuilder.Create<JobScanDb>()
                    //.WithIdentity("Job1", "Group1")
                    .Build();

                ITrigger trigger = TriggerBuilder.Create()
                    //.WithIdentity("Trigger1", "Group1")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(AppSettings.TimeJobSchedulerSecond)
                        .RepeatForever())
                    .Build();
                _scheduler.Result.ScheduleJob(job, trigger);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("JobSyncScheduler_Start", e.ToString());
            }
        }

        public static void Stop()
        {
            try
            {
                _scheduler.Result?.Shutdown();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobSyncScheduler_Stop", ex.ToString());
            }
        }
    }

    class JobScanDb : IJob
    {
        private DBProcess dbProcess = new DBProcess();
        public static bool IsRunning = false;

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                if (IsRunning)
                {
                    return;
                }

                IsRunning = true;

                dbProcess.QueryDbToUnlock();

                await Task.Delay(1);

                IsRunning = false;
            }
            catch (Exception e)
            {
                IsRunning = false;
                Utilities.WriteErrorLog("JobScanDB.Execute", $"Error: {e.Message}");
            }
        }
    }
}
