﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TN.Mobike.LookOmniServices.Core;
using TN.Mobike.LookOmniServices.Core.OmniMina;
using TN.Mobike.LookOmniServices.Model;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Job
{
    class SyncJobScheduler
    {
        DockModel dockModel = new DockModel();
        public void Start()
        {
            try
            {
                Console.OutputEncoding = Encoding.UTF8;
                Utilities.WriteOperationLog("Start", "App Started!");

                Task.Run(dockModel.UpdateStatus);

                Task.Run(SqlHelper.OpenConnectMongoDb);

                if (AppSettings.TurnOnSocket)
                {
                    //Thread threadSocketLock = new Thread(MinaControl.StartServer);
                    //threadSocketLock.Start();

                    Task.Run(MinaControl.StartServer);
                }

                if (AppSettings.TurnOnSignalR)
                {
                    //Thread threadSignal = new Thread(SignHub.ReceiverToUnlock);
                    //threadSignal.Start();

                    //Task.Run(SignHub.ReceiverToUnlock);
                }

                if (AppSettings.TurnOnQueryDb)
                {
                    JobScheduler.Start();
                }

                JobCheckConnection.Start();

                //MinaControl.StartServer();

                //while (true)
                //{
                //    var data = Console.ReadLine();

                //    if (data.ToUpper() == "E")
                //    {
                //        MinaControl.StopServer();

                //        Console.WriteLine("DISCONNECT");

                //        Environment.Exit(1);
                //    }
                //    else
                //    {
                //        MinaControl.UnLock(data, true);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Start", ex.ToString());
            }
        }
        public void Stop()
        {
            try
            {
                dockModel.UpdateStatus();

                if (AppSettings.TurnOnSocket)
                {
                    MinaControl.StopServer();
                }

                if (AppSettings.TurnOnSignalR)
                {
                    SignHub.DisconnectHub();
                }

                if (AppSettings.TurnOnQueryDb)
                {
                    JobScheduler.Stop();
                }

                JobCheckConnection.Stop();

                Utilities.WriteOperationLog("ProgramInit_Stop", "Try to stop service...");
                Utilities.CloseLog();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("ProgramInit_Stop", ex.ToString());
            }
        }
    }
}
