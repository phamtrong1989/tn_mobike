﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using TN.Mobike.LookOmniServices.Model;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Job
{
    class JobCheckConnection
    {
        private static Task<IScheduler> _scheduler;
        public static Dictionary<string, DateTime> DataConnection = new Dictionary<string, DateTime>();
        public static void Start()
        {
            try
            {
                StdSchedulerFactory factory = new StdSchedulerFactory();
                _scheduler = factory.GetScheduler();
                 _scheduler.Result.Start();

                IJobDetail job = JobBuilder.Create<CheckConnection>()
                    //.WithIdentity("Job1", "Group1")
                    .Build();

                ITrigger trigger = TriggerBuilder.Create()
                    //.WithIdentity("Trigger1", "Group1")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(60)
                        .RepeatForever())
                    .Build();
                _scheduler.Result.ScheduleJob(job, trigger);
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("JobSyncScheduler_Start", e.ToString());
            }
        }

        public static void Stop()
        {
            try
            {
                _scheduler.Result?.Shutdown();
            }
            catch (Exception ex)
            {
                Utilities.WriteErrorLog("JobSyncScheduler_Stop", ex.ToString());
            }
        }
    }

    class CheckConnection : IJob
    {
        public static bool IsRunning = false;
        private DockModel dockModel = new DockModel();
        private StationModel StationModel = new StationModel();
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                if (IsRunning)
                {
                    return;
                }

                IsRunning = true;
                var timeCheck = DateTime.Now;

                foreach (var data in JobCheckConnection.DataConnection)
                {
                    var totalSecond = (timeCheck - data.Value).TotalSeconds;
                    if (totalSecond > AppSettings.TimeCheckConnection)
                    {
                        dockModel.UpdateStatus(data.Key, 0);

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"LOCK {data.Key} DISCONNECT !");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        continue;
                    }
                }

                IsRunning = false;
            }
            catch (Exception e)
            {
                IsRunning = false;
                Utilities.WriteErrorLog("JobScanDB.Execute", $"Error: {e.Message}");
            }
            await Task.Delay(1);
        }
    }
}
