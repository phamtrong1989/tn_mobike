﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Core
{
    class ApiProcess
    {
        public static void PushStatusLock(string imei, string requestId)
        {
            try
            {
                var url = $"{AppSettings.ApiPushLock}/{imei}/1";
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);

                Utilities.WriteOperationLog("PushStatusLock",$"Imei: {imei} | Url: {url} | Response: {response.Content}");

                new MongoUtilities().AddLog(0, "PushStatusLock | Gửi trạng thái khóa API", $"{requestId}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Url: {url} | Response: {response.Content}");
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("PushStatusLock", $"Imei: {imei} |Error: {e.Message}");

                new MongoUtilities().AddLog(0, "PushStatusLock (Error) | Gửi trạng thái khóa API (Lỗi)", $"{requestId}", $"{imei}", MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} |Error: {e.Message}");

            }
        }
    }
}
