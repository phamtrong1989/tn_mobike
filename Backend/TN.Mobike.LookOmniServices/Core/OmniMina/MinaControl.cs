﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Features;
using Mina.Core.Service;
using Mina.Core.Session;
using Mina.Filter.Codec;
using Mina.Filter.Codec.TextLine;
using Mina.Filter.Executor;
using Mina.Filter.Logging;
using Mina.Transport.Socket;
using TN.Mobike.LookOmniServices.Model;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Core.OmniMina
{
    class MinaControl
    {
        private static readonly int PORT = AppSettings.PortService;
        public static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static IoAcceptor acceptor;
        private static IPAddress ipAddress = null;
        private static DockModel dockModel = new DockModel();

        private static string _requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";
        public static void StartServer()
        {
            try
            {
                acceptor = new AsyncSocketAcceptor();

                acceptor.FilterChain.AddLast("logger", new LoggingFilter());
                acceptor.FilterChain.AddLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Encoding.UTF8)));

                acceptor.ExceptionCaught +=
                    (o, e) =>
                    {
                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine(e.Exception);
                        }

                        Utilities.WriteErrorLog("MinaControl.StartServer", $"Error: {e.Exception}");
                    };

                acceptor.SessionIdle +=
                    (o, e) =>
                    {
                        if (AppSettings.ModeRun)
                        {
                            Console.WriteLine("IDLE " + e.Session.GetIdleCount(e.IdleStatus));
                        }
                        Utilities.WriteOperationLog("MinaControl.StartServer", $"IDLE: {e.Session.GetIdleCount(e.IdleStatus)}");
                    };

                acceptor.FilterChain.AddLast("exceutor", new ExecutorFilter());
                acceptor.Handler = new MinaSocket();
                acceptor.SessionConfig.ReadBufferSize = 2048;
                acceptor.SessionConfig.SetIdleTime(IdleStatus.ReaderIdle, 8 * 60);

                string hostName = Dns.GetHostName();
                
                ipAddress = !String.IsNullOrEmpty(AppSettings.HostService) ? IPAddress.Parse(AppSettings.HostService) : Dns.GetHostByName(hostName).AddressList[0];

                acceptor.Bind(new IPEndPoint(ipAddress, PORT));
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"START ON: {ipAddress} | LISTENING ON PORT : {PORT}");
                }
                
                Utilities.WriteOperationLog("MinaOmni.StartServer", $"Start on: {ipAddress} | Listening on port: {PORT}");

                new MongoUtilities().AddLog(0, "StartServer | Bật server", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Start on: {ipAddress} | Listening on port: {PORT}");

            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.StartServer", $"Listening on port: {PORT} | Error: {e.Message}");

                new MongoUtilities().AddLog(0, "StartServer (Error) | Bật server (Lỗi)", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Listening on port: {PORT} | Error: {e.Message}");

            }
        }

        public static void StopServer()
        {
            try
            {
                acceptor?.Unbind();
                Utilities.WriteOperationLog("MinaOmni.StopServer", $"Stop server Mina Omni successful !");

                new MongoUtilities().AddLog(0, "StopServer | Tắt server", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Stop server Mina Omni successful !");

            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.StopServer", $"Error: {e.Message}");

                new MongoUtilities().AddLog(0, "StopServer (Error) | Tắt server (Lỗi)", $"{_requestId}", $"{ipAddress}", MongoUtilities.EnumMongoLogType.Start, $"Error: {e.Message}");

            }
        }

        public static bool UnLock(string imei)
        {
            var key = "L0";
            int uid = 0;
            long timestamp = (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
            var message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},{key},0,{uid},{timestamp}#";

            var requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";

            var command = MinaSocket.AddBytes(new byte[] { (byte)0xFF, (byte)0xFF }, Encoding.ASCII.GetBytes(message));

            var result = SessionMap.NewInstance().SendMessage(Convert.ToInt64(imei), command, false, requestId);

            //var result = SessionMap.NewInstance().SendMessage(Convert.ToInt64(imei), message, false);

            //var status = result == 1 ? $"Send message {message} successful !" : $"Send message {message} fail !";

            var status = "";

            if(result == 1)
            {
                status = $"Send message {message} successful !";
            }
            else
            {
                dockModel.UpdateStatus(imei, 0);
                status = $"Send message {message} fail !";
            }

            if (AppSettings.ModeRun)
            {
                Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Message : {message}");
                Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : {status}");
                Console.WriteLine("====================================================================================================");
            }

            Utilities.WriteOperationLog("MinaOmni.Unlock", $"Imei: {imei} | Status: {status}");

            new MongoUtilities().AddLog(0, "Unlock | Mở khóa", $"{requestId}", imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | Status: {status}");

            return Convert.ToBoolean(result);
        }

        public static bool UnLock(string key, bool check = true)
        {
            var imei = "861123053783161";
            int uid = 0;
            long timestamp = (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;

            var message = "";

            switch (key.ToUpper())
            {
                case "L0": // MỞ KHÓAl0
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},L0,0,{uid},{timestamp}#\n";
                    break;
                case "D0": // KIỂM TRA VỊ TRÍ
                    //message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},D0#\n";
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},D0#\n";
                    break;
                case "D1": // CÀI ĐẶT KHÓA TỰ ĐỘNG TRACKING VỊ TRÍ
                    //message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},D1,0#\n";
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},D1,0#\n";
                    break;
                case "S5": // KIỂM TRA THÔNG TIN KHÓA
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},S5#\n";
                    break;
                case "S8": // TÌM KIẾM XE ĐẠP
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},S8,5,0#\n";
                    break;
                case "G0": // KIỂM TRA PHIÊN BẢN PHẦN MỀM
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},G0#\n";
                    break;
                case "I0": // NHẬN SỐ ICCID ĐÃ GẮN TRÊN SIM
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},I0#\n";
                    break;
                case "M0": // NHẬN ĐỊA CHỈ MAC BLUETOOTH
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},M0#\n";
                    break;
                case "S0": // TẮT THIẾT BỊ
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},S0#\n";
                    break;
                case "S1": // KHỞI ĐỘNG LẠI THIẾT BỊ
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},S1#\n";
                    break;
                case "C0": // MỞ KHÓA BẰNG MÃ RFID
                    message = $"*CMDR ,OM,{imei},{DateTime.Now:yyMMddHHmmss},C0,0,0,000000001A2B3C4D#\n";
                    break;
                case "C1": // CÀI ĐẶT MÃ SỐ CHO KHÓA
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},C1,0,000000001A2B3C4D#\n";
                    break;
                default:
                    goto case "L0";
            }

            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Message : {message}");

            var command = MinaSocket.AddBytes(new byte[] { (byte)0xFF, (byte)0xFF }, Encoding.ASCII.GetBytes(message));

            var result = SessionMap.NewInstance().SendMessage(Convert.ToInt64(imei), command, false);
            //var result = SessionMap.NewInstance().SendMessage(Convert.ToInt64(imei), message, false);

            Console.WriteLine(result == 1 ? $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Success" : $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Fail");
            Console.WriteLine("====================================================================================================");
            return Convert.ToBoolean(result);
        }
    }
}
