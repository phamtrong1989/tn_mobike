﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mina.Core.Service;
using Mina.Core.Session;
using TN.Mobike.LookOmniServices.Entity;
using TN.Mobike.LookOmniServices.Job;
using TN.Mobike.LookOmniServices.Model;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Core.OmniMina
{
    class MinaSocket : IoHandlerAdapter
    {
        private DockModel dockModel = new DockModel();
        private OpenLockHistoryModel openLockHistoryModel = new OpenLockHistoryModel();
        private OpenLockRequestModel openLockRequestModel = new OpenLockRequestModel();
        private MongoUtilities mongoUtilities = new MongoUtilities();
        private string message = "";
        private int status = 0;
        private string pin = "0 %";
        private string time = "";
        private string gms = "";
        private string timeActive = "";
        private double percent = 0;

        public override void ExceptionCaught(IoSession session, Exception cause)
        {
            if (AppSettings.ModeRun)
            {
                Console.WriteLine(cause.ToString());
            }
            Utilities.WriteErrorLog("MinaOmni.ExceptionCaught", $"Error : {cause.Message}");
        }

        public override void MessageReceived(IoSession session, object message)
        {
            try
            {
                string msg = this.message != null ? message.ToString() : "";

                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"Message : {msg}");
                }
                Utilities.WriteOperationLog("MinaOmni.MessageReceived", $"Message : {msg}");

                if (msg != null && msg.Length > 10)
                {
                    if (msg.Contains("*CMDR") && msg.Contains("#"))
                    {
                        int firstIndex = msg.IndexOf("#", StringComparison.Ordinal);
                        int lastIndex = msg.LastIndexOf("#", StringComparison.Ordinal);

                        if (firstIndex == lastIndex)
                        {
                            int startIndex = msg.IndexOf("*CMDR", StringComparison.Ordinal);
                            string order = msg.Substring(startIndex, lastIndex + 1);
                            HandCommand(session, order);
                        }
                        else
                        {
                            string[] strMsg = msg.Split('#');
                            foreach (var m in strMsg)
                            {
                                int startIndex = m.IndexOf("*CMDR", StringComparison.Ordinal);
                                string oneComm = m.Substring(startIndex, m.Length);
                                HandCommand(session, $"{oneComm}#");
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.MessageReceived", $"Error : {e.Message}");
            }
        }

        public override void SessionIdle(IoSession session, IdleStatus status)
        {
            try
            {
                if (AppSettings.ModeRun)
                {
                    Console.WriteLine($"IDLE : {session.GetIdleCount(status)}");

                    if (status == IdleStatus.ReaderIdle)
                    {
                        Console.WriteLine("READER IDLE");
                        SessionMap.NewInstance().RemoveSession(session);
                        session.CloseOnFlush();

                    }
                    else if (status == IdleStatus.WriterIdle)
                    {
                        Console.WriteLine("WRITER IDLE");
                    }
                    else
                    {
                        Console.WriteLine("BOTH IDLE");
                    }
                }

                Utilities.WriteOperationLog("MinaOmni.SessionIdle", $"Status : {status.ToString()}");

            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("MinaOmni.SessionIdle", $"Error : {e.Message}");
            }
        }

        public new void InputClosed(IoSession session)
        {
            SessionMap.NewInstance().RemoveSession(session);
            InputClosed(session);
        }

        public override void SessionClosed(IoSession session)
        {
            session.CloseOnFlush();
        }

        public void HandCommand(IoSession session, string command)
        {
            if (command.Length <= 1) return;

            var comm = command.Split(',').ToList();
            var comCode = comm[4];
            var imei = comm[2];
            var imeiL = Convert.ToInt64(imei);
            string requestId = $"{DateTime.Now:yyyyMMddHHmmssffffff}";

            if (JobCheckConnection.DataConnection.TryGetValue(imei, out var value))
            {
                JobCheckConnection.DataConnection[imei] = DateTime.Now;
            }
            else
            {
                JobCheckConnection.DataConnection.Add(imei, DateTime.Now);
            }

            if (AppSettings.ModeRun)
            {
                Console.WriteLine($"MESSAGE FROM IMEI: {imeiL} | {command}");
            }

            SessionMap.NewInstance().AddSession(imeiL, session, requestId);

            Dock dock = null;

            //Dock dock = dockModel.GetOneDock(imei);

            //Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | Dock: {dock.ToString()}");

            Utilities.WriteOperationLog("MinaOmni.ReceiverMessage", $"Imei: {imei} | {command}");

            switch (comCode.ToUpper())
            {
                //
                case "Q0":

                    // *CMDR ,OM,123456789123456,200318123020,Q0,412#

                    percent = Utilities.CheckPercent(comm[5].Replace("#", ""));

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                    pin = $"{percent} %";

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | Q0 - SIGN IN COMMAND | Thời gian : {time} | Pin : {pin}");
                    mongoUtilities.AddLog(0, "Q0 | Kết nối sv", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | Q0 - SIGN IN COMMAND | Thời gian : {time} | Pin : {pin}");

                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.Battery = percent;
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | Q0 - SIGN IN COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "Q0 (Update) | Kết nối sv (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | Q0 - SIGN IN COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");
                    }
                    else
                    {
                        dock = new Dock(imei, percent, 0, 0);
                        dockModel.InsertDock(dock);

                        message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},I0#\n";
                        status = SessionMap.NewInstance().SendMessage(imeiL, message, false, requestId);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | Q0 - SIGN IN COMMAND | Dock: {dock.ToString()}");
                        mongoUtilities.AddLog(0, "Q0 (Insert) | Kết nối sv (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | Q0 - SIGN IN COMMAND | Dock: {dock.ToString()}");
                    }

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Q0 - SIGN IN COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Q0 - IMEI = {imei}");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER Q0 : {command}");


                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine($"Pin       : {pin}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    break;
                case "H0":

                    // *CMDR ,OM,123456789123456,200318123020,H0,0,412,28#

                    percent = Utilities.CheckPercent(comm[6]);

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                    pin = $"{percent} %";
                    gms = Utilities.CheckGMS(comm[7].Replace("#", ""));

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : H0 - HEARTBEAT COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER H0 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine(comm[5] == "0" ? $"Trạng thái : Đã mở khóa" : $"Trạng thái : Đã khóa");
                        Console.WriteLine($"Pin : {pin}");
                        Console.WriteLine($"Tín hiệu : {gms}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | H0 - HEARTBEAT COMMAND | Thời gian : {time} | Pin : {pin} % | Lock (1: Lock \\ 0: Unlock) : {comm[5]} | GMS : {gms}");
                    mongoUtilities.AddLog(0, "H0 | Giữ kết nối", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | H0 - HEARTBEAT COMMAND | Thời gian : {time} | Pin : {pin} % | Lock (1: Lock \\ 0: Unlock) : {comm[5]} | GMS : {gms}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        if (comm[5] == "0")
                        {
                            dock.LockStatus = false;
                        }
                        else
                        {
                            dock.LockStatus = true;
                        }

                        dock.Battery = percent;
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | H0 - HEARTBEAT COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");
                        mongoUtilities.AddLog(0, "H0 (Update) | Giữ kết nối (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | H0 - HEARTBEAT COMMAND | Thời gian : {time} | Pin : {pin} % | Lock (1: Lock \\ 0: Unlock) : {comm[5]} | GMS : {gms}");

                    }
                    else
                    {
                        dock = new Dock(imei, percent, 0,0);
                        dockModel.InsertDock(dock);
                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | H0 - HEARTBEAT COMMAND | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "H0 (Insert) | Giữ kết nối (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | H0 - HEARTBEAT COMMAND | Dock: {dock.ToString()}");
                    }

                    break;
                case "L1":

                    // Receiver : *CMDR ,OM,123456789123456,200318123020,L1,1234,1497689816,3#

                    Task.Run(() => ApiProcess.PushStatusLock(imei, requestId));

                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},Re,L1#\n";
                    status = SessionMap.NewInstance().SendMessage(imeiL, message, false, requestId);

                    var messageD1Off = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},D1,0#\n";
                    var statusD1Off = SessionMap.NewInstance().SendMessage(imeiL, messageD1Off, false, requestId);

                    var messageS5 = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},S5#\n";
                    var statusS5 = SessionMap.NewInstance().SendMessage(imeiL, messageS5, false, requestId);

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}"; 
                    timeActive = Utilities.UnixTimeStampToDateTime(comm[6]);
                    var cycle = $"{comm[7].Replace("#", "")} phút";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : L1 -  LOCK COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER L1 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine($"USER ID   : {comm[5]}");
                        Console.WriteLine($"Thời gian hoạt động : {timeActive}");
                        Console.WriteLine($"Thời gian chu kỳ : {cycle}");
                        Console.ForegroundColor = ConsoleColor.White;

                        Console.WriteLine($"Send message turn off tracking location status: {statusD1Off}");
                        Console.WriteLine($"Send message check lock status: {statusS5}");

                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message ?? "No Message"}");
                        Console.WriteLine(status == 1 ? $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Success" : $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Fail");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | L1 - LOCK COMMAND | Thời gian : {time} | User : {comm[5]} | Thời gian hoạt động : {timeActive} | Thời gian đi (Phút) : {cycle} | Định vị tự động : Tắt - {statusD1Off} | Gửi kiểm tra trạng thái : {statusS5}");
                    mongoUtilities.AddLog(0, "L1 | Đóng khóa", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | L1 - LOCK COMMAND | Thời gian : {time} | User : {comm[5]} | Thời gian hoạt động : {timeActive} | Thời gian đi (Phút) : {cycle} | Định vị tự động : Tắt - {statusD1Off} | Gửi kiểm tra trạng thái : {statusS5}");

                    Utilities.WriteOperationLog("MinaOmni.Resend", $"Status: {status} | Message : {message}");
                    mongoUtilities.AddLog(0, "L1 (Resend) | Đóng khóa (Phản hồi L1)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Status: {status} | Message : {message}");

                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LockStatus = true;
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | L1 - LOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "L1 (Update) | Đóng khóa (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | L1 - LOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);
                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | L1 - LOCK COMMAND | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "L1 (Insert) | Đóng khóa (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | L1 - LOCK COMMAND | Dock: {dock.ToString()}");

                    }

                    break;
                case "L0":

                    // Receiver : *CMDR ,OM,123456789123456,200318123020,L0,0,1234,1497689816#

                    // Resend
                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},Re,L0#\n";
                    status = SessionMap.NewInstance().SendMessage(imeiL, message, false, requestId);

                    var messageD1On = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},D1,{AppSettings.TimeTrackingLocation}#\n";
                    var statusD1On = SessionMap.NewInstance().SendMessage(imeiL, messageD1On, false, requestId);

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                    timeActive = $"{Utilities.UnixTimeStampToDateTime(comm[7].Replace("#", ""))}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : L0 - UNLOCK COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER L0 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($" Thời gian : {time}");
                        Console.WriteLine(comm[5] == "0" ? $"Trạng thái : Mở thành công" : $"Trạng thái : Mở lỗi");
                        Console.WriteLine($" Customer ID : {comm[6]}");
                        Console.WriteLine($" Timestamp : {timeActive}");
                        Console.ForegroundColor = ConsoleColor.White;

                        Console.WriteLine($"Send message turn on tracking location status: {statusD1On}");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message ?? "No Message"}");
                        Console.WriteLine(status == 1 ? $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Success" : $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Fail");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | L0 - UNLOCK COMMAND | Thời gian : {time} | User : {comm[6]} | Thời gian hoạt động : {timeActive} | Lock (1: Lock \\ 0: Unlock) : {comm[5]} | Định vị tự động : Bật - {statusD1On}");
                    mongoUtilities.AddLog(0, "L0 | Mở khóa", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | L0 - UNLOCK COMMAND | Thời gian : {time} | User : {comm[6]} | Thời gian hoạt động : {timeActive} | Lock (1: Lock \\ 0: Unlock) : {comm[5]} | Định vị tự động : Bật - {statusD1On}");


                    Utilities.WriteOperationLog("MinaOmni.Resend", $"Status: {status} | Message : {message}");
                    mongoUtilities.AddLog(0, "L0 (Resend) | Mở khóa (Phản hồi L0)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Status: {status} | Message : {message}");

                    dock = dockModel.GetOneDock(imei);
                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LockStatus = comm[5] != "0";
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | L0 - UNLOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "L0 (Update) | Mở khóa (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | L0 - UNLOCK COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0, false);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | L0 - UNLOCK COMMAND | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "L0 (Insert) | Mở khóa (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | L0 - UNLOCK COMMAND | Dock: {dock.ToString()}");
                    }

                    break;
                case "D0":

                    // *CMDR, OM, 123456789123456, 200318123020, D0, 0,124458.00, A,2237.7514, N,11408.6214, E,6, 0.21, 151216,10, M,A#

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},Re,D0#\n";
                    status = SessionMap.NewInstance().SendMessage(imeiL, message, false, requestId);

                    var data12 = comm[17];
                    var data11 = comm[16];
                    var data10 = comm[15];
                    var data9 = comm[14];
                    var data8 = comm[13];
                    var data7 = comm[12];
                    var data6 = comm[11];
                    var data5 = Utilities.getLatitudeLongitude(comm[10], false);
                    var data4 = comm[9];
                    var data3 = Utilities.getLatitudeLongitude(comm[8]);
                    var data2 = comm[7];
                    var data1 = comm[6];
                    var data0 = comm[5];

                    var location = $@"https://maps.google.com?q={data3},{data5}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : D0 - POSITIONING COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER D0 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;

                        Console.WriteLine($"<1> : {data0}");
                        Console.WriteLine($"<2> : {data1} (hhmmss / UTC)");

                        if (data2 == "V" || data2 == "VA")
                        {
                            Console.WriteLine($"<3> : Location status : {data2} = Định vị không hoạt động");
                        }
                        else if (data2 == "A")
                        {
                            Console.WriteLine($"<3> : Location status : {data2} = Định vị hoạt động");
                        }

                        Console.WriteLine($"<4> : {data3} | Vĩ độ ddmm.mmmm");
                        Console.WriteLine($"<5> : {data4} | N (Bán cầu Bắc) | S (Bán cầu Nam)");
                        Console.WriteLine($"<6> : {data5} | Kinh độ dddmm.mmmm");
                        Console.WriteLine($"<7> : {data6} | E (Đông) | W (Tây)");
                        Console.WriteLine($"<8> : {data7} | Vận tốc");
                        Console.WriteLine($"<9> : {data8} | HDOP độ chính sác định vị");
                        Console.WriteLine($"<10> : {data9} | Ngày UTC | ddmmyy");
                        Console.WriteLine($"<11> : {data10} | Độ cao  so với mực nước biển");
                        Console.WriteLine($"<12> : {data11} | Đơn vị chiều cao mét");

                        if (data12 == "N#")
                        {
                            Console.WriteLine("<13> : N = Dữ liệu định vị không hợp lệ");
                        }
                        else if (data12 == "A#")
                        {
                            Console.WriteLine("<13> : A = Định vị tự động");
                        }
                        else if (data12 == "D#")
                        {
                            Console.WriteLine("<13> : D = Khác");
                        }
                        else
                        {
                            Console.WriteLine("<13> : E = Ước lượng");
                        }

                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message ?? "No Message"}");
                        Console.WriteLine(status == 1 ? $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Success" : $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Fail");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | D0 - POSITIONING COMMAND | Thời gian: {time} | Trạng thái định vị (A: Active \\ V|VA: Not Active) : {data2} | Lat (Vĩ Độ): {data3} | Long (Kinh độ): {data5} | {data4} - {data6} | Point HDOP: {data8} | Altitude: {data10} | Mode (A: Định vị tự động\\ D: Khác\\ E: Ước lượng\\ N: Dữ liệu lỗi): {data12} | Resend : {status} | {location}");
                    mongoUtilities.AddLog(0, "D0 | Vị trí", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | D0 - POSITIONING COMMAND | Thời gian: {time} | Trạng thái định vị (A: Active \\ V|VA: Not Active) : {data2} | Lat (Vĩ Độ): {data3} | Long (Kinh độ): {data5} | {data4} - {data6} | Point HDOP: {data8} | Altitude: {data10} | Mode (A: Định vị tự động\\ D: Khác\\ E: Ước lượng\\ N: Dữ liệu lỗi): {data12} | Resend : {status} | {location}");


                    Utilities.WriteOperationLog("MinaOmni.Resend", $"Status: {status} | Message : {message}");
                    mongoUtilities.AddLog(0, "D0 (Resend) | Vị trí (Phản hồi D0)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Status: {status} | Message : {message}");


                    dock = dockModel.GetOneDock(imei);
                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.Lat = data3;
                        dock.Long = data5;
                        dock.ConnectionStatus = true;
                        dock.UpdatedDate = DateTime.Now;

                        if (data3 != 0 || data5 != 0)
                        {
                            dockModel.UpdateDock(dock);
                            Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | D0 - POSITIONING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lat: {data3} - Long: {data5} | Lock Status: {dock.LockStatus}");

                            mongoUtilities.AddLog(0, "D0 (Update) | Vị trí (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | D0 - POSITIONING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lat: {data3} - Long: {data5} | Lock Status: {dock.LockStatus}");


                            foreach (var station in JobScheduler.ListStations)
                            {
                                var check = Utilities.DistanceInMeter(data3, data5, station.Lat, station.Lng);
                                if (check)
                                {
                                    dockModel.UpdateStation(imei, station.Id);
                                    dockModel.UpdateBike(dock.Id, station.Id, dock.Lat, dock.Long);
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                    }
                    else
                    {
                        dock = new Dock(imei, 0, data3, data5, true);
                        dockModel.InsertDock(dock);

                        
                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | D0 - POSITIONING COMMAND | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "D0 (Insert) | Vị trí (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Imei: {imei} | D0 - POSITIONING COMMAND | Dock: {dock.ToString()}");
                    }

                    SqlHelper.InsertGPS(imei, data3, data5, requestId);

                    break;

                case "D1":

                    // *CMDR,OM,123456789123456,200318123020,D1,60#
                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : D1 - TRACKING LOCATION COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER S5 : {command}");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : Location tracking each : {comm[5].Replace("#","")} giây");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Thời gian: {time} | Bật tự động gửi vị trí mỗi {comm[5].Replace("#", "")} giây");
                    mongoUtilities.AddLog(0, "D1 | Định vị tự động", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Thời gian: {time} | Bật tự động gửi vị trí mỗi {comm[5].Replace("#", "")} giây");

                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                        mongoUtilities.AddLog(0, "D1 (Update) | Định vị tự động (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Dock: {dock.ToString()}");
                        mongoUtilities.AddLog(0, "D1 (Insert) | Định vị tự động (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | D1 - TRACKING LOCATION COMMAND | Dock: {dock.ToString()}");

                    }

                    break;

                case "S5":

                    // *CMDR,OM,123456789123456,200318123020,S5,412,30,5,0,0#

                    percent = Utilities.CheckPercent(comm[5].Replace("#", ""));

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                    gms = Utilities.CheckGMS(comm[6]);
                    pin = $"{percent} %";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : S5 - LOCK STATUS COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER S5 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Pin : {pin}");
                        Console.WriteLine($"Tín hiệu : {gms}");
                        Console.WriteLine($"Chỉ số GPS : {comm[7]}");
                        Console.WriteLine(comm[8] == "0" ? $"Trạng thái : Đang mở khóa" : $"Trạng thái : Đã khóa");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand" , $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Thời gian: {time} | Pin: {pin} | GMS: {gms} | GPS: {comm[7]} | Lock (1: Lock \\ 0: Unlock): {comm[8]}");
                    mongoUtilities.AddLog(0, "S5 | Trạng thái", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Thời gian: {time} | Pin: {pin} | GMS: {gms} | GPS: {comm[7]} | Lock (1: Lock \\ 0: Unlock): {comm[8]}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LockStatus = comm[8] != "0";
                        dock.LastConnectionTime = DateTime.Now;
                        dock.Battery = percent;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");
                        mongoUtilities.AddLog(0, "S5 (Update) | Trạng thái (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Last Connection Time: {dock.LastConnectionTime} | Battery: {percent} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, percent, 0, 0, comm[8] != "0");
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Dock: {dock.ToString()}");
                        mongoUtilities.AddLog(0, "S5 (Insert) | Trạng thái (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | S5 - LOCK STATUS COMMAND | Dock: {dock.ToString()}");

                    }

                    break;
                case "S8":

                    // *CMDR,OM,123456789123456,200318123020,S8,8,0#

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : S8 - RINGING FOR FINDING A BIKE COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER S8 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine($"Đổ chuông : {comm[5]} lần");
                        Console.WriteLine($"Reservations : {comm[6]}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | S8 - RINGING FOR FINDING A BIKE COMMAND | Thời gian: {time} | Số lần đổ chuông: {comm[5]} | Reservations: {comm[6]}");

                    mongoUtilities.AddLog(0, "S8 | Đổ chuông", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | S8 - RINGING FOR FINDING A BIKE COMMAND | Thời gian: {time} | Số lần đổ chuông: {comm[5]} | Reservations: {comm[6]}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dockModel.UpdateDock(dock);
                        dock.ConnectionStatus = true;
                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | S8 - RINGING | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "S8 (Update) | Đổ chuông (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | S8 - RINGING | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | S8 - RINGING | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "S8 (Insert) | Đổ chuông (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | S8 - RINGING | Dock: {dock.ToString()}");

                    }

                    break;
                case "G0":

                    // *CMDR,OM,123456789123456,200318123020,G0,XX_110,Jul 4 2018#

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : G0 - QUERY FIRMWARE VERSION COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER G0 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine($"Phiên bản : {comm[5]}");
                        Console.WriteLine($"Thời gian cập nhật : {comm[6].Replace("#", "")}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | G0 - QUERY FIRMWARE VERSION COMMAND | Thời gian: {time} | Phiên bản: {comm[5]} | Thời gian cập nhật: {comm[6].Replace("#", "")}");
                    mongoUtilities.AddLog(0, "G0 | Phần mềm", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | G0 - QUERY FIRMWARE VERSION COMMAND | Thời gian: {time} | Phiên bản: {comm[5]} | Thời gian cập nhật: {comm[6].Replace("#", "")}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | G0 - QUERY FIRMWARE | Last Connection Time: {dock.LastConnectionTime} | Model: {comm[5]} | Update: {comm[6]} | Lock Status: {dock.LockStatus}");
                        mongoUtilities.AddLog(0, "G0 (Update) | Phần mềm (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | G0 - QUERY FIRMWARE | Last Connection Time: {dock.LastConnectionTime} | Model: {comm[5]} | Update: {comm[6]} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | G0 - QUERY FIRMWARE | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "G0 (Insert) | Phần mềm (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | G0 - QUERY FIRMWARE | Dock: {dock.ToString()}");

                    }

                    break;
                case "W0":

                    // *CMDR,OM,123456789123456,200318123020,W0,1#

                    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},Re,W0#\n";
                    status = SessionMap.NewInstance().SendMessage(imeiL, message, false, requestId);

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                    var alarm = comm[5].Replace("#", "");
                    var statusAlarm = "";
                    if (alarm == "1")
                    {
                        statusAlarm = "Báo động : Chuyển động bất hợp pháp";
                    }
                    else if (alarm == "2")
                    {
                        statusAlarm = "Báo động : đổ xe";
                    }
                    else if (alarm == "6")
                    {
                        statusAlarm = "Báo động : Xe được dựng lên";
                    }

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : W0 - ALARMING COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER W0 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine(statusAlarm);

                        Console.ForegroundColor = ConsoleColor.White;

                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RESEND : {message ?? "No Message"}");
                        Console.WriteLine(status == 1 ? $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Success" : $"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : STATUS : Fail");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | W0 - ALARMING COMMAND | Thời gian: {time} | Status: {statusAlarm}");
                    mongoUtilities.AddLog(0, "W0 | Cảnh báo", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | W0 - ALARMING COMMAND | Thời gian: {time} | Status: {statusAlarm}");


                    Utilities.WriteOperationLog("MinaOmni.Resend", $"Status: {status} | Message : {message}");
                    mongoUtilities.AddLog(0, "W0 (Resend) | Cảnh báo (Phản hồi W0)", requestId, imei, MongoUtilities.EnumMongoLogType.Center, $"Status: {status} | Message : {message}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | W0 - ALARMING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                        mongoUtilities.AddLog(0, "W0 (Update) | Cảnh báo (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | W0 - ALARMING COMMAND | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | W0 - ALARMING COMMAND | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "W0 (Insert) | Cảnh bảo (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | W0 - ALARMING COMMAND | Dock: {dock.ToString()}");

                    }


                    //status = SessionMap.NewInstance().SendMessage(imeiL, message, false);
                    //Console.WriteLine($"Resend status: {status}");

                    break;
                case "U0":

                    // *CMDR ,OM,123456789123456,200318123020,U0,A 1,110,1101#
                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : U0 - Detection upgrade/start upgrade");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER U0 : {command}");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | U0 - Detection upgrade/start upgrade | Thời gian: {time} | Code: {comm[5]} | Date: {comm[7].Replace("#", "")} | Ver: {comm[6]}");
                    mongoUtilities.AddLog(0, "U0 | Nâng cấp phần mềm", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | U0 - Detection upgrade/start upgrade | Thời gian: {time} | Code: {comm[5]} | Date: {comm[7].Replace("#", "")} | Ver: {comm[6]}");


                    dock = dockModel.GetOneDock(imei);
                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | U0 - Detection | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "U0 (Update) | Nâng cấp phần mềm (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | U0 - Detection | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | U0 - Detection | Dock: {dock.ToString()}");
                        mongoUtilities.AddLog(0, "U0 (Insert) | Nâng cấp phần mềm (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | U0 - Detection | Dock: {dock.ToString()}");

                    }

                    break;

                case "U1":

                    // *CMDR ,OM,123456789123456,200318123020,U1,100,A1#

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : U1 - Acquisition of upgrade data");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER U1 : {command}");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | U1 - Acquisition of upgrade data | Thời gian: {time} | Packages: {comm[5]} | Code: {comm[6].Replace("#","")}");
                    mongoUtilities.AddLog(0, "U1 | Kiểm tra nâng cấp", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | U1 - Acquisition of upgrade data | Thời gian: {time} | Packages: {comm[5]} | Code: {comm[6].Replace("#", "")}");


                    dock = dockModel.GetOneDock(imei);
                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | U1 - Acquisition of upgrade | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                        mongoUtilities.AddLog(0, "U1 (Update) | Kiểm tra nâng cấp (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | U1 - Acquisition of upgrade | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | U1 - Acquisition of upgrade | Dock: {dock.ToString()}");
                        mongoUtilities.AddLog(0, "U1 (Insert) | Kiểm tra nâng cấp (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | U1 - Acquisition of upgrade | Dock: {dock.ToString()}");

                    }

                    break;

                case "U2":

                    // *CMDR ,OM,123456789123456,200318123020,U2,A1,0#
                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : U2 - Notification of upgrade results");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER U2 : {command}");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | U2 - Notification of upgrade results | Thời gian: {time} | Code: {comm[5]} | Status: {comm[6].Replace("#","")}");

                    mongoUtilities.AddLog(0, "U2 | Kết quả nâng cấp", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | U2 - Notification of upgrade results | Thời gian: {time} | Code: {comm[5]} | Status: {comm[6].Replace("#", "")}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | U2 - Notification of upgrade | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "U2 (Update) | Kết quả nâng cấp (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | U2 - Notification of upgrade | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | U2 - Notification of upgrade | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "U2 (Insert) | Kết quả nâng cấp (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | U2 - Notification of upgrade | Dock: {dock.ToString()}");

                    }

                    break;
                case "K0":

                    // *CMDR ,OM,123456789123456,200318123020,K0,yOTmK50z #
                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : K0 - set/get BLE 8 byte communication KEY");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER U2 : {command}");
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | K0 - set/get BLE 8 byte communication KEY | Thời gian: {time} | Key: {comm[5].Replace("#","")}");

                    mongoUtilities.AddLog(0, "K0 | Thiết lập khóa", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | K0 - set/get BLE 8 byte communication KEY | Thời gian: {time} | Key: {comm[5].Replace("#", "")}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | K0 | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                        mongoUtilities.AddLog(0, "K0 (Update) | Thiết lập khóa (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | K0 | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | K0 | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "K0 (Insert) | Thiết lập khóa (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | K0 | Dock: {dock.ToString()}");
                    }

                    break;

                case "I0":

                    // *CMDR,OM,123456789123456,200318123020,I0,123456789AB123456789#

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                    var ICCID = comm[5].Replace("#", "");

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : I0 - OBTAIN SIM CARD ICCID CODE COMMAND");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER I0 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine($"Mã ICCID  : {ICCID}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | I0 - OBTAIN SIM CARD ICCID CODE COMMAND | Thời gian: {time} | ICCID: {ICCID}");
                    mongoUtilities.AddLog(0, "I0 | Lấy mã ICCID", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | I0 - OBTAIN SIM CARD ICCID CODE COMMAND | Thời gian: {time} | ICCID: {ICCID}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.SIM = ICCID;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | I0 - ICCID | Last Connection Time: {dock.LastConnectionTime} | ICCID: {ICCID} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "I0 (Update) | Lấy mã ICCID (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | I0 - ICCID | Last Connection Time: {dock.LastConnectionTime} | ICCID: {ICCID} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0, true, ICCID);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | I0 - ICCID | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "I0 (Insert) | Lấy mã ICCID (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | I0 - ICCID | Dock: {dock.ToString()}");

                    }

                    break;
                case "M0":

                    // *CMDR,OM,123456789123456,200318123020,M0,12：34：56：78：90：AB#

                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                    var macAdd = comm[5].Replace("#", "");

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : M0 - GET LOCK BLUETOOTH MAC ADDRESS");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER M0 : {command}");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine($"MAC address  : {macAdd}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | M0 - GET LOCK BLUETOOTH MAC ADDRESS | Thời gian: {time} | Mac: {macAdd}");
                    mongoUtilities.AddLog(0, "M0 | Bluetooth", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | M0 - GET LOCK BLUETOOTH MAC ADDRESS | Thời gian: {time} | Mac: {macAdd}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | M0 - MAC | Last Connection Time: {dock.LastConnectionTime} | Mac: {macAdd} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "M0 (Update) | Bluetooth (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | M0 - MAC | Last Connection Time: {dock.LastConnectionTime} | Mac: {macAdd} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | M0 - MAC | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "M0 (Insert) | Bluetooth (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | M0 - MAC | Dock: {dock.ToString()}");

                    }
                    break;
                case "C1":

                    // *CMDR ,OM,863725031194523,000000000000,C1,000000001A2B3C4D#
                    time = $"{Utilities.ConvertDateTime(comm[3]):yyyy-MM-dd HH:mm:ss}";
                    var key = comm[5].Replace("#", "");

                    if (AppSettings.ModeRun)
                    {
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : C1 - Management biked number setting");
                        Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss.fff} : RECEIVER C1 : {command}");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Thời gian : {time}");
                        Console.WriteLine($"Key  : {key}");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | C1 - Management biked number setting | Thời gian: {time} | Key: {key}");
                    mongoUtilities.AddLog(0, "C1 | Cài đặt mã RFID", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | C1 - Management biked number setting | Thời gian: {time} | Key: {key}");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | C1 | Last Connection Time: {dock.LastConnectionTime} | Key: {key} | Lock Status: {dock.LockStatus}");
                        mongoUtilities.AddLog(0, "C1 (Update) | Cài đặt mã RFID (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | C1 | Last Connection Time: {dock.LastConnectionTime} | Key: {key} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | C1 | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "C1 (Insert) | Cài đặt mã RFID (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.End, $"Imei: {imei} | C1 | Dock: {dock.ToString()}");
                    }
                    break;
                case "C0":
                    var rfid = comm[7].Replace("#", "");
                    //if (MinaControl.listRFID.Contains(rfid))
                    //{
                    //    int uid = 0;
                    //    long timestamp = (long)(DateTime.UtcNow - MinaControl.Jan1st1970).TotalMilliseconds;
                    //    message = $"*CMDS,OM,{imei},{DateTime.Now:yyMMddHHmmss},L0,0,{uid},{timestamp}#\n";

                    //    var result = SessionMap.NewInstance().SendMessage(imeiL, message, false);
                    //}
                    Utilities.WriteOperationLog("MinaOmni.HandCommand", $"Imei: {imei} | C0 - RFID Unlock | Thời gian: {time} | Key: C0");

                    mongoUtilities.AddLog(0, "C0 | Mở khóa với RFID", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | C0 - RFID Unlock | Thời gian: {time} | Key: C0");


                    dock = dockModel.GetOneDock(imei);

                    if (dock != null && dock.Id >= 0)
                    {
                        dock.LastConnectionTime = DateTime.Now;
                        dock.ConnectionStatus = true;
                        dockModel.UpdateDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Update", $"Imei: {imei} | C0 - RFID Unlock | Last Connection Time: {dock.LastConnectionTime} | RFID: {rfid} | Lock Status: {dock.LockStatus}");

                        mongoUtilities.AddLog(0, "C0 (Update) | Mở khóa với RFID (Cập nhật khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | C0 - RFID Unlock | Last Connection Time: {dock.LastConnectionTime} | RFID: {rfid} | Lock Status: {dock.LockStatus}");

                    }
                    else
                    {
                        dock = new Dock(imei, 0, 0, 0);
                        dockModel.InsertDock(dock);

                        Utilities.WriteOperationLog("HandCommand.Insert", $"Imei: {imei} | C0 - RFID Unlock | Dock: {dock.ToString()}");

                        mongoUtilities.AddLog(0, "C0 (Insert) | Mở khóa với RFID (Thêm mới khóa)", requestId, imei, MongoUtilities.EnumMongoLogType.Start, $"Imei: {imei} | C0 - RFID Unlock | Dock: {dock.ToString()}");

                    }

                    break;
                default:
                    break;
            }
            
            Console.WriteLine("====================================================================================================");
            dock = null;
        }

        public static byte[] AddBytes(byte[] b1, byte[] b2)
        {
            byte[] b = new byte[b1.Length + b2.Length];
            Array.Copy(b1, 0, b, 0, b1.Length);
            Array.Copy(b2, 0, b, b1.Length, b2.Length);

            return b;
        }
    }
}
