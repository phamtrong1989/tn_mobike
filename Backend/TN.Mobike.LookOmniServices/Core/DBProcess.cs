﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TN.Mobike.LookOmniServices.Core.OmniMina;
using TN.Mobike.LookOmniServices.Entity;
using TN.Mobike.LookOmniServices.Model;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Core
{
    class DBProcess
    {
        private OpenLockRequestModel openLockRequestModel = new OpenLockRequestModel();
        private OpenLockHistoryModel openLockHistoryModel = new OpenLockHistoryModel();
        private DockModel dockModel = new DockModel();
        public void QueryDbToUnlock()
        {

            try
            {
                var listOpenRequest = openLockRequestModel.GetAllOpenLockRequests(DateTime.Now);

                if (listOpenRequest == null || listOpenRequest.Count <= 0)
                {
                    //Console.WriteLine($"NO DATA IN {DateTime.Now:yyyy/MM/dd HH:mm:ss}");
                    return;
                }

                foreach (var item in listOpenRequest)
                {
                    if (AppSettings.ModeRun)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("===============================================================");
                        Console.WriteLine(JsonConvert.SerializeObject(item));
                        Console.WriteLine("===============================================================");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    var check = MinaControl.UnLock(item.IMEI);

                    if (!check) continue;

                    var openLock = new OpenLockHistory(item);
                    openLock.Status = 0;

                    openLockHistoryModel.InsertOpenLockHistory(openLock);

                    openLockRequestModel.DeleteOpenLockRequest(item.Id);

                    //var dock = dockModel.GetOneDock(item.IMEI);
                    //if (dock != null)
                    //{
                    //    dock.LockStatus = false;
                    //    dock.LastConnectionTime = DateTime.Now;
                    //    dockModel.UpdateDock(dock);

                    //    Utilities.WriteOperationLog("QueryDbToUnlock.Update", $"Imei: {item.IMEI} | Query DB to Unlock | Last Connection Time: {dock.LastConnectionTime} | Lock Status: {dock.LockStatus}");
                    //}

                    Utilities.WriteOperationLog("[ControlServices.QueryDbToUnlock]", $"OPEN SUCCESSFUL IMEI = {item.IMEI} : {JsonConvert.SerializeObject(item)}");
                }
            }
            catch (Exception e)
            {
                Utilities.WriteErrorLog("[ControlServices.QueryDbToUnlock]", $"[ERROR: {e}]");
            }

        }
    }
}
