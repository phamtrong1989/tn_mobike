﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using TN.Mobike.LookOmniServices.Core.OmniMina;
using TN.Mobike.LookOmniServices.Entity;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Core
{
    class SignHub
    {
        public static HubConnection _connection;
        public static string _hubStringConnection = AppSettings.SignalRHub;

        public static void InitHub()
        {
            _connection = new HubConnectionBuilder()
                .WithUrl(_hubStringConnection, HttpTransportType.WebSockets | HttpTransportType.LongPolling)
                .WithAutomaticReconnect()
                //.ConfigureLogging(logging =>
                //{
                //    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                //    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                //})
                //.ConfigureLogging(logging =>
                //{
                //    // Log to the Console
                //    logging.AddConsole();

                //    // This will set ALL logging to Debug level
                //    logging.SetMinimumLevel(LogLevel.Information);
                //    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                //    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                //})
                .Build();
            _connection.HandshakeTimeout = new TimeSpan(0, 1, 0);
            _connection.ServerTimeout = new TimeSpan(0, 1, 0);

            //if (AppSettings.ModeRun)
            //{
            //    Console.WriteLine($"INITIAL SUCCESSFUL SIGNAL HUB : {_hubStringConnection}");
            //}
        }

        public static void DisconnectHub()
        {
            _connection?.StopAsync();
        }

        public static async void ReceiverToUnlock()
        {
            InitHub();

            await _connection.StartAsync();

            if (AppSettings.ModeRun)
            {
                Console.WriteLine("CONNECT SUCCESSFUL TO SIGNAL HUB !");
            }

            _connection.On<string>("DeviceReceiveMessage", (data) =>
            {
                if (AppSettings.ModeRun)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("===============================================================");
                    Console.WriteLine(data);
                    Console.WriteLine("===============================================================");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                UnlockFromHub(data);
            });

        }

        private static void UnlockFromHub(string data)
        {
            var device = JsonConvert.DeserializeObject<Device>(data);
            if (device != null)
            {
                MinaControl.UnLock(device.IMEI);
            }
            else
            {
                return;
            }
        }
    }
}
