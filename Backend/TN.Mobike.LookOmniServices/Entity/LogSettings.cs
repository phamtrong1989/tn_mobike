﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TN.Mobike.LookOmniServices.Settings;

namespace TN.Mobike.LookOmniServices.Entity
{
    public class LogSettings
    {

        public bool Is { get; set; }
        public string MongoClient { get; set; }
        public string MongoDataBase { get; set; }
        public string MongoCollection { get; set; }
        public bool IsUseMongo { get; set; }
        public string MongoDataBaseLog { get; set; }
        public string MongoCollectionLog { get; set; }

        public LogSettings()
        {
            Is = true;
            MongoClient = AppSettings.ConnectionMongoDb;
            MongoDataBase = "TN-Mobike-{TimeDB}";
            MongoCollection = "GPS";
            IsUseMongo = false;
            MongoDataBaseLog = "LOG-{TimeDB}";
            MongoCollectionLog = "Log";
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
