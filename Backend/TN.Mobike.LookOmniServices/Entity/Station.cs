﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.Mobike.LookOmniServices.Entity
{
    class Station
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int TotalDock { get; set; }

        public Station()
        {
            
        }

        public Station(int id, int tenantId, string name, string address, double lat, double lng, string description, int status, DateTime createdDate, DateTime updatedDate, int totalDock)
        {
            Id = id;
            TenantId = tenantId;
            Name = name;
            Address = address;
            Lat = lat;
            Lng = lng;
            Description = description;
            Status = status;
            CreatedDate = createdDate;
            UpdatedDate = updatedDate;
            TotalDock = totalDock;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
