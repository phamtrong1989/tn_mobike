﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TN.Mobike.LookOmniServices.Entity
{
    class Bike
    {
        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ColorId { get; set; }
        public int CreatedUser { get; set; }
        public string Images { get; set; }
        public int InvestorId { get; set; }
        public int ModelId { get; set; }
        public string Plate { get; set; }
        public int ProducerId { get; set; }
        public int ProjectId { get; set; }
        public int UpdatedUser { get; set; }
        public int WarehouseId { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public int StationId { get; set; }
        public int DockId { get; set; }
        public string Note { get; set; }

        public Bike()
        {
            
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
