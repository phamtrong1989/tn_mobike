﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Mobike.LookOmniServices.Settings
{
    class AppSettings
    {
        // CONNECTION STRING
        public static string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];

        // SERVICES NAME
        public static string ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        public static string ServiceDisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"];
        public static string ServiceDescription = ConfigurationManager.AppSettings["ServiceDescription"];

        //<!--Thời gian giới hạn để mở khóa-->
        public static int LimitTimeOpen = Convert.ToInt32(ConfigurationManager.AppSettings["LimitTimeOpen"]);
        public static int TimeJobSchedulerSecond = Convert.ToInt32(ConfigurationManager.AppSettings["TimeJobSchedulerSecond"]);

        //<!--URL connect to Hub SignalR-->
        public static string SignalRHub = ConfigurationManager.AppSettings["SignalRHub"];

        //<!--PORT nhận dữ liệu từ khóa trả về-->
        public static int PortService = Convert.ToInt32(ConfigurationManager.AppSettings["PortService"]);
        public static string HostService = ConfigurationManager.AppSettings["HostService"];

        // <!--Tổng dung lượng pin-->
        public static int TotalPin = Convert.ToInt32(ConfigurationManager.AppSettings["TotalPin"]);

        // <!--Thời gian bật định vị vị trí khóa ( tính theo giây )-->
        public static int TimeTrackingLocation = Convert.ToInt32(ConfigurationManager.AppSettings["TimeTrackingLocation"]);

        public static int TimeCheckConnection = Convert.ToInt32(ConfigurationManager.AppSettings["TimeCheckConnection"]);

        public static int MeterCheck = Convert.ToInt32(ConfigurationManager.AppSettings["MeterCheck"]);

        // <!--Cài đặt xem chạy ở dạng services hay console-->
        public static bool ModeRun = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["ModeRun"]));

        //<!--CONNECTION STRING TO MONGODB-->
        public static string ConnectionMongoDb = ConfigurationManager.AppSettings["ConnectionMongoDb"];
        //!--Tên database MongoDB-->
        public static string DatabaseNameMongo = ConfigurationManager.AppSettings["DatabaseNameMongo"];
        //<!--Tên bảng lưu lịch sử GPS MongoDb-->
        public static string TableGPSNameMongo = ConfigurationManager.AppSettings["TableGPSNameMongo"];

        public static string ApiPushLock = ConfigurationManager.AppSettings["ApiPushLock"];

        public static bool TurnOnSignalR = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnSignalR"]));
        public static bool TurnOnQueryDb = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnQueryDb"]));
        public static bool TurnOnSocket = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["TurnOnSocket"]));
    }
}
