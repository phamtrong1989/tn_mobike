﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Model;
namespace TN.Infrastructure.Interfaces
{
    public interface IRoleRepository : IEntityBaseRepository<ApplicationRole>
    {
        Task<bool> IsUse(int roleId);
        Task<List<ApplicationRole>> RolesByUser(int userId);
        Task<RoleDetailObjModel> GetRoleDetail(int roleId);
        Task UpdateRoleDetail(int roleId, List<int> ids);
    }
}
