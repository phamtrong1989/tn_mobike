﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.Infrastructure.Interfaces
{
    public interface IUserRepository : IEntityBaseRepository<ApplicationUser>
    {
        Task<List<RoleActionModel>> RoleActionsByUser2Async(int userId);
        Task<BaseLogDataModel<string>> UpdateRolesAsync(int userId, List<int> roles);
        Task<List<int>> GetRolesAsync(int userId);
        Task<int> StatusUserAsync(int userId);
        void DeleteRoles(int userId);
        Task<List<DataRoleActionModel>> RoleActionsByUserAsync(int userId);
        Task<ApiResponseData<List<ApplicationUser>>> SearchPagedAsync(
            int pageIndex,
            int pageSize,
            int? roleId,
            Expression<Func<ApplicationUser, bool>> predicate = null,
            Func<IQueryable<ApplicationUser>, IOrderedQueryable<ApplicationUser>> orderBy = null,
            Expression<Func<ApplicationUser, ApplicationUser>> select = null,
            params Expression<Func<ApplicationUser, object>>[] includeProperties
        );
        Task<List<ApplicationUser>> SeachByIds(List<int> ids);
        Task<List<ApplicationUser>> SeachByIds(List<int> ids, List<int> ids2);
        Task<List<ApplicationUser>> SeachByIds(List<int> ids, List<int> ids2, List<int> ids3);
        Task<BaseSearchModel<List<ApplicationUser>>> SearchPaged2List(int page, int limit, int? roleId, Expression<Func<ApplicationUser, bool>> predicate = null, Func<IQueryable<ApplicationUser>, IOrderedQueryable<ApplicationUser>> orderBy = null, Expression<Func<ApplicationUser, ApplicationUser>> select = null, params Expression<Func<ApplicationUser, object>>[] includeProperties);
    }
}
