﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Model;
using static TN.Domain.Model.Parameter;

namespace TN.Infrastructure.Interfaces
{
    public interface IParameterRepository : IEntityBaseRepository<Parameter>
    {
        Task UpdateValue(ParameterType type, string value);
    }
}
