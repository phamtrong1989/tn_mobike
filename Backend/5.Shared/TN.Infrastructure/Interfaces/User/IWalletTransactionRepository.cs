using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IWalletTransactionRepository : IEntityBaseRepository<WalletTransaction>
    {
        Task<List<Tuple<EPaymentGroup, int, decimal>>> GroupByPayGate(int? projectId, DateTime begin, DateTime end);
        Task<List<Tuple<EWalletTransactionStatus, int, decimal>>> CountAndSumDirectPayment(int? projectId, DateTime begin, DateTime end);
        Task<List<Tuple<int, decimal>>> RevenueGroupByAccount(IEnumerable<int> accountIds, DateTime? begin, DateTime? end);
        Task<List<Tuple<EPaymentGroup, decimal>>> SumRevenueGroupByPayGate(int? projectId, Expression<Func<WalletTransaction, bool>> predicate);
        Task<int> CountAgeRecharge(int minAge, int maxAge, Expression<Func<WalletTransaction, bool>> predicate = null);
        Task<decimal> SumRechargeWithAge(int? projectId, int minAge, int maxAge, DateTime from, DateTime to);
        Task<List<Tuple<string, double>>> CountAndGroupByTime(string typeView, Expression<Func<WalletTransaction, bool>> predicate = null);
        Task<List<Tuple<string, double>>> SumAndGroupByTime(string typeView, Expression<Func<WalletTransaction, bool>> predicate = null);
        Task<List<Tuple<string, double>>> SumRechargeWithAgeAndGroupByTime(int? projectId, string typeView, int minAge, int maxAge, DateTime from, DateTime to);
        Task<List<Tuple<DateTime, double>>> CountAndGroupByDay(Expression<Func<WalletTransaction, bool>> predicate = null);
        Task<List<Tuple<DateTime, double>>> SumAndGroupByDay(Expression<Func<WalletTransaction, bool>> predicate = null);
    }
}