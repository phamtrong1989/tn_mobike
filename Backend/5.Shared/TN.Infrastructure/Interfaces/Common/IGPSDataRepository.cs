﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.Infrastructure.Interfaces
{
    public interface IGPSDataRepository
    {
        List<GPSData> GetByIMEI(LogSettings settings, string imei, DateTime startTime, DateTime endTime);
        Task<EventSystemMongo> EventSend(
            LogSettings settings,
            EEventSystemType type,
            EEventSystemWarningType warningType,
            int accountId,
            string transactionCode,
            int projectId,
            int stationId,
            int bikeId,
            int dockId,
            string name,
            string content,
            string datas = null
            );
        ApiResponseData<List<EventSystemMongo>> EventSystemSearchPageAsync(
            LogSettings settings,
            int pageIndex,
            int pageSize,
            string key,
            bool? isFlag,
            bool? isProcessed,
            EEventSystemType? type,
            EEventSystemGroup? group,
            EEventSystemWarningType? warningType,
            DateTime? start,
            DateTime? end,
            int? projectId,
            string transactionCode,
            string plate
        );

        TransactionGPS GetTransactionGPS(LogSettings settings, int transactionId, DateTime endTime);
    }
}
