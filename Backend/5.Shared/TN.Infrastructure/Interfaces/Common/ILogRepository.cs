﻿using TN.Domain.Model;
using System.Threading.Tasks;
using PT.Domain.Model;
using System.Collections.Generic;
using System;
using TN.Domain.Model.Common;

namespace TN.Infrastructure.Interfaces
{
    public interface ILogRepository : IEntityBaseRepository<Log>
    {
        Task AddLog(int userId, string objectName, int objectId, string action, LogType type, string objectType = null);
        Task MongoAdd(LogSettings settings, MongoLog data);
        Task AddLog(int userId, string controllerName, int objectId, string action, LogType type);
        ApiResponseData<List<MongoLog>> MongoSearch(LogSettings settings, int page, int limit, int? accountId, string logKey, string functionName, EnumMongoLogType? type, DateTime? date, EnumSystemType? systemType, string deviceKey, bool? isExeption, string sortby, string sorttype);
    }
}
