﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Drawing.Imaging;

namespace TN.Infrastructure.Interfaces
{
    public interface IFileRepository
    {
        void SettingsUpdate(string map, object a);
    }
}
