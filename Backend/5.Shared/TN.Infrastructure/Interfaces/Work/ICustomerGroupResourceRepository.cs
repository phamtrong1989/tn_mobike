﻿using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface ICustomerGroupResourceRepository : IEntityBaseRepository<CustomerGroupResource>
    {
    }
}
