﻿
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IProjectResourceRepository : IEntityBaseRepository<ProjectResource>
    {
    }
}
