﻿using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IBookingFailRepository : IEntityBaseRepository<BookingFail>
    {
    }
}