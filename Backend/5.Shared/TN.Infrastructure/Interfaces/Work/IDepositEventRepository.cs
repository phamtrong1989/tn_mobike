﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IDepositEventRepository : IEntityBaseRepository<DepositEvent>
    {
        Task<DepositEvent> EventByAccount(int accountId, decimal amount, int addProjectId, int addCustomerGroupId);
        Task UpdateUsed(int id);
    }
}
