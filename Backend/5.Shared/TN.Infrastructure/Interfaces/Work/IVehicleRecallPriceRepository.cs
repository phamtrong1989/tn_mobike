using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IVehicleRecallPriceRepository : IEntityBaseRepository<VehicleRecallPrice>
    {
    }
}

