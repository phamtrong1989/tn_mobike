﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces.Work
{
    public interface IWalletTransactionRepository : IEntityBaseRepository<WalletTransaction>
    {
    }

    //Task<WalletTransaction> GetDataReport(Expression<Func<WalletTransaction, bool>> predicate = null, Func<IQueryable<WalletTransaction>, IOrderedQueryable<WalletTransaction>> orderBy = null, Expression<Func<WalletTransaction, WalletTransaction>> select = null, params Expression<Func<WalletTransaction, object>>[] includeProperties);
}