﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.Infrastructure.Interfaces
{
    public interface ITransactionRepository : IEntityBaseRepository<Transaction>
    {
        Tuple<decimal, decimal> SplitTotalPrice(decimal totalPrice, bool allowChargeInSubAccount, Wallet wallet);
        string GetTicketPriceString(TicketPrice item);
        TotalPriceModel ToTotalPrice(ETicketType ticketType, DateTime startTime, DateTime endTime, decimal ticketPrice_TicketValue, int ticketPrice_LimitMinutes, int ticketPrice_BlockPerMinute, int ticketPrice_BlockPerViolation, decimal ticketPrice_BlockViolationValue, DateTime prepaid_EndTime, int prepaid_MinutesSpent, DiscountCode discountCode);

        Task<List<BikeTransaction>> CountTotalMinuteRentGroupByBikeAsync();
        Task<List<Tuple<EBookingStatus, int>>> CountTransacionGroupByStatus(int? projectId, DateTime begin, DateTime end);
        Task<ApiResponseData<List<Transaction>>> SearchPagedAsync(int pageIndex, int pageSize, string key, Expression<Func<Transaction, bool>> predicate = null, Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>> orderBy = null, Expression<Func<Transaction, Transaction>> select = null);
        Task<int> SumTotalMinutesRentByRangeAge(int? projectId, int minAge, int maxAge, DateTime from, DateTime to);
        Task<List<Tuple<string, double>>> CountAndGroupByTime(string typeView, Expression<Func<Transaction, bool>> predicate = null);
        Task<List<Tuple<string, double>>> CountByAgeAndGroupByTime(string typeView, int minAge, int maxAge, Expression<Func<Transaction, bool>> predicate = null);
        Task<List<Tuple<string, double>>> SumTotalMinutesAndGroupByTime(string typeView, Expression<Func<Transaction, bool>> predicate = null);
        Task<List<Tuple<string, double>>> SumTotalPriceAndGroupByTime(string typeView, Expression<Func<Transaction, bool>> predicate = null);
        Task<List<BikeTransaction>> Top20BikeMaxRentTime(int? projectId, DateTime from, DateTime to);
        Task<List<BikeTransaction>> Top20BikeMinRentTime(int? projectId, DateTime from, DateTime to);
        Task<List<Tuple<DateTime, double>>> CountAndGroupByDay(Expression<Func<Transaction, bool>> predicate = null);
        Task<List<Tuple<DateTime, double>>> SumTotalMinutesAndGroupByDay(Expression<Func<Transaction, bool>> predicate = null);
    }
}