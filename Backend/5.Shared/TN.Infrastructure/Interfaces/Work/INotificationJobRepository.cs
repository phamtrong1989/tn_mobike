using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Infrastructure;
using TN.Domain.Model;
using PT.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface INotificationJobRepository : IEntityBaseRepository<NotificationJob>
    {
        Task SendNow(int accountId, string title, string message, int createdUser, DockEventType dataType, string dataContent);
        Task SendNotificationByAccount(List<NotificationTemp> _notificationTempSettings, int accountId, ENotificationTempType type, params string[] prs);
    }
}

