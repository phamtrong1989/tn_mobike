﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using static TN.Domain.Model.Bike;

namespace TN.Infrastructure.Interfaces
{
    public interface IBikeRepository : IEntityBaseRepository<Bike>
    {
        Task<List<Station>> InitTotalBikeInStationAsync(List<Station> stations, int maxBattery);
        Task<List<Bike>> BikesInTransaction();
        Task<ApiResponseData<List<Bike>>> SearchPagedAsync(
            int pageIndex,
            int pageSize,
            string key,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            int? battery,
            bool? lockStatus,
            bool? charging,
            Expression<Func<Bike, bool>> predicate = null, Func<IQueryable<Bike>, IOrderedQueryable<Bike>> orderBy = null, Expression<Func<Bike, Bike>> select = null);
        Task<List<Tuple<EBikeStatus, int>>> GroupByType(int? projectId);
        Task<List<Bike>> SearchToReport(string key, EDockBookingStatus? bookingStatus, Expression<Func<Bike, bool>> predicate = null);
        Task<int> CountBikeFreeInStation(int? projectId);
        Task<List<Bike>> SearchNotBooking(int projectId);
    }
}