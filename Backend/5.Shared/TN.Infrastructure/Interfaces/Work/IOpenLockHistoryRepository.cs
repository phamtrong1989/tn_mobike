﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IOpenLockHistoryRepository : IEntityBaseRepository<OpenLockHistory>
    {
    }
}
