using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IExportBillRepository : IEntityBaseRepository<ExportBill>
    {
    }
}