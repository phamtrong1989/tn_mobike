using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface ILanguageRepository : IEntityBaseRepository<Language>
    {
    }
}

