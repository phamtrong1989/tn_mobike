﻿using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IDistrictRepository : IEntityBaseRepository<District>
    {
    }
}
