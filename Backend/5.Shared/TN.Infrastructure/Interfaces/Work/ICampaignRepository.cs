using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Infrastructure;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface ICampaignRepository : IEntityBaseRepository<Campaign>
    {
        Task<List<SumLimitModel>> GetSumLimit(List<int> campaignIds);
    }
}

