using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IWarehouseRepository : IEntityBaseRepository<Warehouse>
    {
    }
}