using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IContentPageRepository : IEntityBaseRepository<ContentPage>
    {
    }
}