using TN.Domain.Model;
using System;
using System.Threading.Tasks;

namespace TN.Infrastructure.Interfaces
{
    public interface ICyclingWeekRepository : IEntityBaseRepository<CyclingWeek>
    {
        Task<CyclingWeek> UpdateFlag(int accountId, DateTime checkOutTime);
    }
}

