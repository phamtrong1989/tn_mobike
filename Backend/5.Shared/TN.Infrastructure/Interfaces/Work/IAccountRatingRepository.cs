using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IAccountRatingRepository : IEntityBaseRepository<AccountRating>
    {
    }
}

