using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IImportBillRepository : IEntityBaseRepository<ImportBill>
    {
    }
}
