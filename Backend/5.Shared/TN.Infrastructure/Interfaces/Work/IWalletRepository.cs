﻿using System;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;

namespace TN.Infrastructure.Interfaces
{
    public interface IWalletRepository : IEntityBaseRepository<Wallet>
    {
        Task<Wallet> GetWalletAsync(int accountId);
        Task<bool> WalletChecked(bool allowChargeInSubAccount, decimal ticketValue, Wallet wInfo);
        Task<Wallet> UpdateAddWalletAsync(int accountId, decimal balance, decimal subBalance, decimal tripPoint, string hasKey, bool isPush, bool enableDayExpirys, int directDayExpirys);
        Tuple<decimal, decimal> SplitTotalPrice(decimal totalPrice, bool allowChargeInSubAccount, Wallet wallet);
        Task AddDayExpirys(int accountId, decimal balance, decimal subBalance, bool isEndTransaction);
        Task AddDayExpirysByDirect(int accountId, int days, string note);
        Task<ProjectAccountModel> LatLngToCustomerGroup(int accountId);
    }
}
