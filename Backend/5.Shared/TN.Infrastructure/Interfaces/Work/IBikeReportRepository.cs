using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IBikeReportRepository : IEntityBaseRepository<BikeReport>
    {
    }
}