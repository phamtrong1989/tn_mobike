using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface ISuppliesRepository : IEntityBaseRepository<Supplies>
    {
        Task SuppliesWarehouseExport(int exportBillId);
        Task<List<Supplies>> SuppliesSearchKey(string key, int warehouseId);
        Task<int> GetInventory(int suppliesId, int warehouseId);
        Task SuppliesWarehouseImport(int exportBillId);
        Task SuppliesWarehouseReImport(int exportBillId);
        Task<List<Supplies>> InitSuppliesWarehouses(List<Supplies> data, int warehouseId);
        Task SuppliesWarehouseExportMaintenanceRepair(int maintenanceRepairId, int warehouseId);
    }
}