using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface INewsRepository : IEntityBaseRepository<News>
    {
    }
}