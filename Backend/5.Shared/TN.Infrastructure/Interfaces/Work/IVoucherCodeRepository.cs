using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IVoucherCodeRepository : IEntityBaseRepository<VoucherCode>
    {
        Task<List<VoucherCode>> VoucherCodePublicByAccount(int accountId);
    }
}

