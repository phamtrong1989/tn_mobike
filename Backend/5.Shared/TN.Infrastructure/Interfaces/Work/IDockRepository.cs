﻿using System;
using System.Collections.Generic;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using System.Linq.Expressions;
using System.Linq;

namespace TN.Infrastructure.Interfaces
{
    public interface IDockRepository : IEntityBaseRepository<Dock>
    {
        Task<ApiResponseData<List<Dock>>> SearchPagedAsync(int pageIndex, int pageSize, string key, EDockBookingStatus? bookingStatus, Expression<Func<Dock, bool>> predicate = null, Func<IQueryable<Dock>, IOrderedQueryable<Dock>> orderBy = null, Expression<Func<Dock, Dock>> select = null);

        Task<List<Dock>> SearchToReport(EDockBookingStatus? bookingStatus, Expression<Func<Dock, bool>> predicate = null);
        Task<List<Dock>> SearchNotBooking(int projectId);
    }
}