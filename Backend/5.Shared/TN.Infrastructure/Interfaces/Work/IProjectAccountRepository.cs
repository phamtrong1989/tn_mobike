using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Infrastructure;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.Infrastructure.Interfaces
{
    public interface IProjectAccountRepository : IEntityBaseRepository<ProjectAccount>
    {
        Task<ApiResponseData<List<ProjectAccount>>> SearchPagedAsync(
            int pageIndex,
            int pageSize,
            int? projectId,
            int? customerGroupId,
            EAccountStatus? status,
            EAccountType? type,
            ESex? sex,
            string key,
            int? id,
            Func<IQueryable<ProjectAccount>, IOrderedQueryable<ProjectAccount>> orderBy = null,
            Expression<Func<ProjectAccount, ProjectAccount>> select = null);
    }
}

