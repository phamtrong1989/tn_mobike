﻿using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IStationResourceRepository : IEntityBaseRepository<StationResource>
    {
    }
}
