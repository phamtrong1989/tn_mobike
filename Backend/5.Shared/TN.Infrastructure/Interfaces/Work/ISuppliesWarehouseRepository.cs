using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.Infrastructure.Interfaces
{
    public interface ISuppliesWarehouseRepository : IEntityBaseRepository<SuppliesWarehouse>
    {
        Task<ApiResponseData<List<SuppliesWarehouse>>> SearchPagedInventoryAsync(
            int pageIndex,
            int pageSize,
            string key,
            int warehouseId,
            ESuppliesType? type);
    }
}