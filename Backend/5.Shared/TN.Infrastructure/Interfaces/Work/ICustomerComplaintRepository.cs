using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;

namespace TN.Infrastructure.Interfaces
{
    public interface ICustomerComplaintRepository : IEntityBaseRepository<CustomerComplaint>
    {
        Task<ApiResponseData<List<CustomerComplaint>>> SearchPagedAsync(
            int? projectId,
            int pageIndex,
            int pageSize,
            Expression<Func<CustomerComplaint, bool>> predicate = null,
            Func<IQueryable<CustomerComplaint>, IOrderedQueryable<CustomerComplaint>> orderBy = null,
            Expression<Func<CustomerComplaint, CustomerComplaint>> select = null
        );
    }
}