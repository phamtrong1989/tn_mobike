﻿using System;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using System.Linq.Expressions;
using System.Linq;

namespace TN.Infrastructure.Interfaces
{
    public interface IBookingRepository : IEntityBaseRepository<Booking>
    {
        Task<ApiResponseData<List<Booking>>> SearchPagedAsync(int pageIndex, int pageSize, string key, Expression<Func<Booking, bool>> predicate = null, Func<IQueryable<Booking>, IOrderedQueryable<Booking>> orderBy = null, Expression<Func<Booking, Booking>> select = null);
    }
}
