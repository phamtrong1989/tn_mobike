using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IRFIDBookingRepository : IEntityBaseRepository<RFIDBooking>
    {
        Task RFIDTransactionAdd(RFIDBooking cm);
    }
}

