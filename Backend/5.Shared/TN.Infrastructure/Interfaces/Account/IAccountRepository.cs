﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface IAccountRepository : IEntityBaseRepository<Account>
    {
        Task<List<AccountDevice>> FilterDevice(int accountId);
        Task<List<Tuple<EAccountVerifyStatus, int>>> GroupByVerifyStatus(int? projectId, DateTime fromDate, DateTime toDate);
        Task<int> CountWithProjectId(int? projectId = null, Expression<Func<Account, bool>> predicate = null);
        Task<List<Tuple<string, double>>> CountAndGroupByTime(int? projectId, string typeTimeView, Expression<Func<Account, bool>> predicate = null);
        Task<List<Tuple<DateTime, int>>> CountAndGroupByDay(int? projectId, Expression<Func<Account, bool>> predicate = null);
    }
}