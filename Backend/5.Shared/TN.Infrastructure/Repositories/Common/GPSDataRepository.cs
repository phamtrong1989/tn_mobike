﻿using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
namespace TN.Infrastructure.Repositories
{

    public class GPSDataRepository : IGPSDataRepository
    {
        private readonly IAccountRepository _iAccountRepository;
        private readonly IBikeRepository _bikeRepository;
        public GPSDataRepository(IAccountRepository iAccountRepository, IBikeRepository bikeRepository)
        {
            _iAccountRepository = iAccountRepository;
            _bikeRepository = bikeRepository;
        }

        public TransactionGPS GetTransactionGPS(LogSettings settings, int transactionId, DateTime endTime)
        {
            try
            {
                string dbName = "TNGO-Data";
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                string query = "{ Date : " + endTime.ToString("yyyyMMdd") + ", TransactionId: "+ transactionId + " }";
                var list =   db.GetCollection<TransactionGPS>("TransactionGPS").Find(query).Limit(10).ToList();
                return list.LastOrDefault();
            }
            catch
            {
                return null;
            }
        }

       public  List<GPSData> GetByIMEI(LogSettings settings, string imei, DateTime startTime, DateTime endTime)
       {
            try
            {
                string dbName = settings.MongoDataBase.Replace("{TimeDB}", $"{startTime:yyyyMM}");
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                string query = "{ IMEI : \"" + imei + "\", Date : { $gte: " + startTime.ToString("yyyyMMdd") + ", $lte: " + endTime.ToString("yyyyMMdd") + " }, CreateTimeTicks: { $gte: " + startTime.Ticks.ToString() + ", $lte: " + endTime.Ticks.ToString() + " } }";
                var _context = db.GetCollection<GPSData>("GPS").Find(query).ToList();
                return _context.OrderBy(x => x.CreateTimeTicks).ToList();
            }
            catch
            {
                return new List<GPSData>();
            }
        }

        private async Task EventSystemAddAsync(LogSettings settings, EventSystemMongo data)
        {
            try
            {
                string dbName = settings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(settings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                var collection = db.GetCollection<EventSystemMongo>("EventSystem");
                await collection.InsertOneAsync(data);
            }
            catch { }
        }

       public ApiResponseData<List<EventSystemMongo>> EventSystemSearchPageAsync(
            LogSettings settings,
            int pageIndex,
            int pageSize,
            string key,
            bool? isFlag,
            bool? isProcessed,
            EEventSystemType? type,
            EEventSystemGroup? group,
            EEventSystemWarningType? warningType,
            DateTime? start,
            DateTime? end,
            int? projectId,
            string transactionCode,
            string plate
        )
        {
            int? bikeId = null;
            start = start == null ? DateTime.Now.AddDays(-1).Date : start.Value;
            end = end == null ? DateTime.Now.AddDays(1).Date :end.Value;
            var ktBike =  _bikeRepository.SearchOne(x => x.Plate == plate || x.Plate.Replace(" ", "").Replace("-", "").Replace(".", "") == plate);
            if(ktBike!=null)
            {
                bikeId = ktBike.Id;
            }    
            string dbName = settings.MongoDataBaseLog.Replace("{TimeDB}", $"{end:yyyyMM}");
            var dbClient = new MongoClient(settings.MongoClient);
            var db = dbClient.GetDatabase(dbName);
            string isFlagQR = isFlag == null ? "" : " IsFlagQR: " + isFlag + ",";
            string isProcessedQR = isProcessed == null ? "" : " IsProcessed: " + isProcessed + ",";
            string typeQR = type == null ? "" : " Type: " + (int)type + ",";
            string groupQR = group == null ? "" : " Group: " + (int)group + ",";
            string warningTypeQR = warningType == null ? "" : " WarningType: " + (int)warningType + ",";
            string projectIdQR = projectId == null ? "" : " ProjectId: " + projectId + ",";
            string transactionCodeQR = transactionCode == null ? "" : " TransactionCode: " + transactionCode + ",";
            string keyQR = key == null ? "" : " Content: '" + key + "',";
            string bikeIdQR = bikeId == null ? "" : " BikeId: " + bikeId + ",";
            long startTicks = start?.Date.Ticks ?? 0;
            long endTicks = end?.AddDays(1).Date.Ticks ?? 0;

            string query = "{ Date : { $gte: " + start?.ToString("yyyyMMdd") + ", $lte: " + end?.ToString("yyyyMMdd") + " }, CreatedDateTicks: { $gte: " + startTicks + ", $lte: " + endTicks + " }, " + isFlagQR + isProcessedQR + typeQR + warningTypeQR + projectIdQR + transactionCodeQR + keyQR + bikeIdQR + " IsJob: false , UserId: 0 }";
            var _context = db.GetCollection<EventSystemMongo>("EventSystem").Find(query).ToList();
            var count = _context.Count();
            _context = _context.OrderByDescending(x => x.CreatedDateTicks).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
           
            return new ApiResponseData<List<EventSystemMongo>>
            {
                Data = _context,
                Count = count,
                PageIndex = pageIndex,
                PageSize = pageSize,
                Status = 1
            };
        }

        public async Task<EventSystemMongo> EventSend(
            LogSettings settings,
            EEventSystemType type,
            EEventSystemWarningType warningType,
            int accountId,
            string transactionCode,
            int projectId,
            int stationId,
            int bikeId,
            int dockId,
            string name,
            string content,
            string datas = null
            )
        {
            try
            {
                if (accountId > 0)
                {
                    var account = await _iAccountRepository.SearchOneAsync(x => x.Id == accountId);
                    if (account != null)
                    {
                        content += $" | khách hàng {account.Code} {account.Phone} {account.FullName}";
                    }
                }
                var data = new EventSystemMongo
                {
                    AccountId = accountId,
                    TransactionCode = transactionCode,
                    BikeId = bikeId,
                    Content = $"{content}",
                    CreatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                    DockId = dockId,
                    IsMultiple = false,
                    ProjectId = projectId,
                    StationId = stationId,
                    Title = type.ToName(),
                    Name = name,
                    UserId = 0,
                    Type = type,
                    WarningType = warningType,
                    RoleType = RoleManagerType.Default,
                    UpdatedDate = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                    IsJob = true,
                    Datas = datas,
                    Date = Convert.ToInt32($"{DateTime.Now:yyyyMMdd}"),
                    CreatedDateTicks = DateTime.Now.Ticks,
                    UpdatedDateTicks = DateTime.Now.Ticks,
                    IsFlag = false,
                    IsProcessed = false,
                    ParentId = 0,
                    Id = long.Parse($"{DateTime.Now:yyyyMMddHHmmssffff}")
                };
                await EventSystemAddAsync(settings, data);
                return data;
            }
            catch {
                return null;
            }
        }
    }
}