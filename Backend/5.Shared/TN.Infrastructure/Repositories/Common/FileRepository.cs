﻿using Microsoft.AspNetCore.Http;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
namespace TN.Infrastructure.Repositories
{

    public class FileRepository : IFileRepository
    {
        public bool ThumbnailCallback()
        {
            return false;
        }
        public void SettingsUpdate(string map, object a)
        {
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(a, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(map, output);
        }
    }
}