﻿using System;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using PT.Domain.Model;
using MongoDB.Driver;
using System.Collections.Generic;
using TN.Domain.Model.Common;
using System.Linq;

namespace TN.Infrastructure.Repositories
{
    public class LogRepository : EntityBaseRepository<Log>, ILogRepository
    {
        private readonly ApplicationContext _db;
        public LogRepository(ApplicationContext context) : base(context)
        {
            _db = context;
        }

        public ApiResponseData<List<MongoLog>> MongoSearch(
            LogSettings settings, 
            int page, 
            int limit, 
            int? accountId, 
            string logKey, 
            string functionName, 
            EnumMongoLogType? type, 
            DateTime? date, 
            EnumSystemType? systemType, 
            string deviceKey,
            bool? isExeption,
            string sortby, 
            string sorttype
            )
        {
            date ??= DateTime.Now;
            string dbName = date == null ? settings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}") : settings.MongoDataBaseLog.Replace("{TimeDB}", $"{date.Value:yyyyMM}");
            
            var _client = new MongoClient(settings.MongoClient);

            var _database = _client.GetDatabase(dbName);

            systemType ??= EnumSystemType.None;
            string accountIdQR = accountId == null ?  "" : ", AccountId: " + accountId.Value.ToString();
            string logKeyQR = logKey == null ? "" : ", LogKey: \"" + logKey +"\"";
            string functionNameQR = functionName == null ? "" : ", FunctionName:  \"" + functionName + "\"";
            string typeQR = type == null ? "" : ", Type: " + ((int)type).ToString();
            string deviceKeyQR = deviceKey == null ? "" : ", DeviceKey:  \"" + deviceKey + "\"";
            string isExeptionQR = isExeption == null ? "" : ", IsExeption: " + isExeption.ToString().ToLower();

            //var data = @"{ SystemType: "+ ((int)systemType).ToString() + ", CreateTimeTicks: { $gte: "+ startTime.ToString() + ", $lte: " + endTime.ToString() + " }"+ accountIdQR + logKeyQR + functionNameQR+ typeQR+ deviceKeyQR + isExeptionQR + "}";

            var data = @"{ SystemType: " + ((int)systemType).ToString() + $", Date: {Convert.ToInt64($"{date.Value:yyyyMMdd}")}" + accountIdQR + logKeyQR + functionNameQR + typeQR + deviceKeyQR + isExeptionQR + "}";
            var _context = _database.GetCollection<MongoLog>("Log").Find(data).Sort(@"{ CreateTimeTicks: -1 }").Skip((page - 1) * limit).Limit(limit).ToList();
            
            return new ApiResponseData<List<MongoLog>>
            {
                Data = _context,
                PageSize = limit,
                Count = 99999999,
                PageIndex = page,
                Status = 1
            };
        }

        public async Task MongoAdd(LogSettings settings, MongoLog data)
        {
            string dbName = settings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
            var dbClient = new MongoClient(settings.MongoClient);
            var db = dbClient.GetDatabase(dbName);
            var collection = db.GetCollection<MongoLog>(settings.MongoCollectionLog);
            await collection.InsertOneAsync(data);
        }

        public async Task AddLog(int userId,  string objectName, int objectId, string action, LogType type, string objectType = null)
        {
            await _db.Logs.AddAsync( new Log {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = objectName,
                    ObjectId = objectId,
                    ObjectType = objectType ?? objectName,
                    SystemUserId = userId,
                    Type = type
            });
            await _db.SaveChangesAsync();
        }

        public async Task AddLog(int userId, string controllerName, int objectId, string action, LogType type)
        {
            await _db.Logs.AddAsync(
                new Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = null,
                    SystemUserId = userId,
                    Type = type
                });
            await _db.SaveChangesAsync();
        }
    }
}