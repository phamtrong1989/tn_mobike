﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class NotificationRepository : EntityBaseRepository<NotificationData>, INotificationRepository
    {
        public NotificationRepository(ApplicationContext context) : base(context)
        {

        }
    }
}
