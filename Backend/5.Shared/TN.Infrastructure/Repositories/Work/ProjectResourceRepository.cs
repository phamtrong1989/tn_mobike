using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class ProjectResourceRepository : EntityBaseRepository<ProjectResource>, IProjectResourceRepository
    {
        public ProjectResourceRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

