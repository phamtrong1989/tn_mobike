﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace TN.Infrastructure.Repositories
{
    public class StationRepository : EntityBaseRepository<Station>, IStationRepository
    {
        public StationRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<Station>> FindByLatLng(double min_lat, double max_lat, double min_lng, double max_lng)
        {
            return await _context.Stations.Where(x => x.Lat >= min_lat && x.Lat <= max_lat && x.Lng >= min_lng && x.Lng <= max_lng).ToListAsync();
        }

        public async Task<Station> FindByImei(string imei)
        {
            return await SearchOneAsync(x => x.Address == imei);
        }

        public async Task<bool> CheckAvaiable(int stationId)
        {
            return await _context.Docks.AnyAsync(x => x.StationId == stationId);
        }

        public virtual async Task<List<Station>> FindByMap(Expression<Func<Station, bool>> predicate = null, Func<IQueryable<Station>, IOrderedQueryable<Station>> orderBy = null, Expression<Func<Station, Station>> select = null)
        {
            IQueryable<Station> query = _context.Set<Station>();
            if (predicate != null)
            {
                query = _context.Set<Station>().Where(predicate).AsQueryable();
            }
            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }
            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }
            return await query.ToListAsync();
        }

        public async Task<AccountLocation> AccountLocationGetByAccountId(int accountId)
        {
            return await _context.AccountLocations.OrderByDescending(x=>x.UpdatedTime).FirstOrDefaultAsync(x => x.AcountId == accountId);
        }

        public async Task<List<StationReport>> CountAvailableBikeInStation()
        {
            return await _context.Bikes.Where(x => x.Status == Bike.EBikeStatus.Active && !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start))
                .GroupBy(item => new { item.StationId, item.Type })
                .Select(g => new StationReport
                {
                    StationId = g.Key.StationId,
                    Type = g.Key.Type,
                    Count = g.Count()
                }).ToListAsync();
        }
    }
}