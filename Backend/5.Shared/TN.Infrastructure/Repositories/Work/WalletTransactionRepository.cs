﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories.Work
{
    public class WalletTransactionRepository : EntityBaseRepository<WalletTransaction>, IWalletTransactionRepository
    {
        public WalletTransactionRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<WalletTransaction>> GetDataReport(Expression<Func<WalletTransaction, bool>> predicate = null, Func<IQueryable<WalletTransaction>, IOrderedQueryable<WalletTransaction>> orderBy = null, Expression<Func<WalletTransaction, WalletTransaction>> select = null, params Expression<Func<WalletTransaction, object>>[] includeProperties)
        {
            IQueryable<WalletTransaction> query = _context.Set<WalletTransaction>().AsNoTracking();

            if (predicate != null)
            {
                query = _context.Set<WalletTransaction>().Where(predicate).AsQueryable();
            }
            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }
            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }
            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }
            }
            //
            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<List<Tuple<EPaymentGroup, int, decimal>>> GroupByPayGate(int? projectId, DateTime begin, DateTime end)
        {
            return await _context.WalletTransactions
                .Where(x => x.Type == EWalletTransactionType.Deposit
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= begin
                            && x.CreatedDate < end.AddDays(1)
                            && (projectId == null || x.LocationProjectId == projectId)
                )
                .GroupBy(x => x.PaymentGroup)
                .Select(x => new Tuple<EPaymentGroup, int, decimal>(x.Key, x.Count(), x.Sum(y => y.Price)))
                .ToListAsync();
        }

        public async Task<List<Tuple<EWalletTransactionStatus, int, decimal>>> CountAndSumDirectPayment(int? projectId, DateTime begin, DateTime end)
        {
            return await _context.WalletTransactions
                .Where(x => (x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= begin
                            && x.CreatedDate < end.AddDays(1)
                            && (projectId == null || x.LocationProjectId == projectId)
                )
                .GroupBy(x => x.Status)
                .Select(x => new Tuple<EWalletTransactionStatus, int, decimal>(x.Key, x.Count(), x.Sum(y => y.Price)))
                .ToListAsync();
        }

        public async Task<List<Tuple<int, decimal>>> RevenueGroupByAccount(IEnumerable<int> accountIds, DateTime? begin, DateTime? end)
        {
            return await _context.WalletTransactions
                .Where(x => (!accountIds.Any() || accountIds.Contains(x.AccountId))
                            && (x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money || x.Type == EWalletTransactionType.Deposit)
                            && x.Status == EWalletTransactionStatus.Done
                            && (begin == null || x.CreatedDate >= begin)
                            && (end == null || x.CreatedDate < end.Value.AddDays(1))
                )
                .GroupBy(x => x.AccountId)
                .Select(x => new Tuple<int, decimal>(x.Key, x.Sum(y => y.Price)))
                .ToListAsync();
        }

        public async Task<List<Tuple<EPaymentGroup, decimal>>> SumRevenueGroupByPayGate(int? projectId, Expression<Func<WalletTransaction, bool>> predicate)
        {
            IQueryable<WalletTransaction> query = _context.Set<WalletTransaction>().AsNoTracking();

            // Lọc theo dự án
            //if (projectId != null && projectId > 0)
            //{
            //    query = query.Where(x => _context.ProjectAccounts.Any(m => m.ProjectId == projectId && m.AccountId == x.AccountId)).AsQueryable();
            //}

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            return await query.GroupBy(x => x.PaymentGroup)
                .Select(x => new Tuple<EPaymentGroup, decimal>(x.Key, x.Sum(y => y.Price)))
                .ToListAsync();
        }

        public async Task<int> CountAgeRecharge(int minAge, int maxAge, Expression<Func<WalletTransaction, bool>> predicate = null)
        {
            IQueryable<WalletTransaction> query = _context.Set<WalletTransaction>().AsNoTracking();

            query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId && m.Birthday != null && m.Birthday.Value.AddYears(minAge) <= DateTime.Now.Date && m.Birthday.Value.AddYears(maxAge) > DateTime.Now.Date)).AsQueryable();

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            return await query.CountAsync();
        }

        public async Task<decimal> SumRechargeWithAge(int? projectId, int minAge, int maxAge, DateTime from, DateTime to)
        {
            IQueryable<WalletTransaction> query = _context.Set<WalletTransaction>().AsNoTracking();

            query = query.Where(x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                                        && x.Status == EWalletTransactionStatus.Done
                                        && x.CreatedDate >= from
                                        && x.CreatedDate < to.AddDays(1)
                                        && (projectId == null || x.LocationProjectId == projectId)
                    ).AsQueryable();

            if (minAge == 0 && maxAge == 0)
            {
                query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId && !m.Birthday.HasValue)).AsQueryable();
            }
            else
            {
                query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId
                                                                    && m.Birthday.HasValue
                                                                    && m.Birthday.Value.AddYears(minAge) <= DateTime.Now.Date
                                                                    && m.Birthday.Value.AddYears(maxAge) > DateTime.Now.Date
                                                                )
                        ).AsQueryable();
            }

            return await query.SumAsync(x => x.Price);
        }

        public async Task<List<Tuple<string, double>>> CountAndGroupByTime(string typeView, Expression<Func<WalletTransaction, bool>> predicate = null)
        {
            IQueryable<WalletTransaction> query = _context.WalletTransactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.WalletTransactions.Where(predicate).AsQueryable();
            }

            if (typeView == "day")
            {
                return await query.GroupBy(x => x.CreatedDate.Date)
                                    .Select(x => new Tuple<string, double>(x.Key.ToString("yyyy/MM/dd"), x.Count()))
                                    .ToListAsync();
            }
            else if (typeView == "month")
            {
                return await query.GroupBy(x => new { x.CreatedDate.Year, x.CreatedDate.Month })
                                    .Select(x => new Tuple<string, double>($"{x.Key.Year}/{x.Key.Month.ToString().PadLeft(2, '0')}", x.Count()))
                                    .ToListAsync();
            }
            //
            return null;
        }

        public async Task<List<Tuple<string, double>>> SumAndGroupByTime(string typeView, Expression<Func<WalletTransaction, bool>> predicate = null)
        {
            IQueryable<WalletTransaction> query = _context.WalletTransactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.WalletTransactions.Where(predicate).AsQueryable();
            }

            if (typeView == "day")
            {
                return await query.GroupBy(x => x.CreatedDate.Date)
                                    .Select(x => new Tuple<string, double>(x.Key.ToString("yyyy/MM/dd"), (double)x.Sum(y => y.Price)))
                                    .ToListAsync();
            }
            else if (typeView == "month")
            {
                return await query.GroupBy(x => new { x.CreatedDate.Year, x.CreatedDate.Month })
                                    .Select(x => new Tuple<string, double>($"{x.Key.Year}/{x.Key.Month.ToString().PadLeft(2, '0')}", (double)x.Sum(y => y.Price)))
                                    .ToListAsync();
            }
            //
            return null;
        }

        public async Task<List<Tuple<string, double>>> SumRechargeWithAgeAndGroupByTime(int? projectId, string typeView, int minAge, int maxAge, DateTime from, DateTime to)
        {
            IQueryable<WalletTransaction> query = _context.Set<WalletTransaction>().AsNoTracking();

            query = query.Where(x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                                        && x.Status == EWalletTransactionStatus.Done
                                        && x.CreatedDate >= from
                                        && x.CreatedDate < to.AddDays(1)
                                        && (projectId == null || x.LocationProjectId == projectId)
                    ).AsQueryable();

            if (minAge == 0 && maxAge == 0)
            {
                query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId && !m.Birthday.HasValue)).AsQueryable();
            }
            else
            {
                query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId
                                                                    && m.Birthday.HasValue
                                                                    && m.Birthday.Value.AddYears(minAge) <= DateTime.Now.Date
                                                                    && m.Birthday.Value.AddYears(maxAge) > DateTime.Now.Date
                                                                )
                        ).AsQueryable();
            }

            if (typeView == "day")
            {
                return await query.GroupBy(x => x.CreatedDate.Date)
                                    .Select(x => new Tuple<string, double>(x.Key.ToString("yyyy/MM/dd"), (double)x.Sum(y => y.Price)))
                                    .ToListAsync();
            }
            else if (typeView == "month")
            {
                return await query.GroupBy(x => new { x.CreatedDate.Year, x.CreatedDate.Month })
                                    .Select(x => new Tuple<string, double>($"{x.Key.Year}/{x.Key.Month.ToString().PadLeft(2, '0')}", (double)x.Sum(y => y.Price)))
                                    .ToListAsync();
            }
            //
            return null;
        }

        public async Task<List<Tuple<DateTime, double>>> CountAndGroupByDay(Expression<Func<WalletTransaction, bool>> predicate = null)
        {
            IQueryable<WalletTransaction> query = _context.WalletTransactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.WalletTransactions.Where(predicate).AsQueryable();
            }

            return await query.GroupBy(x => x.CreatedDate.Date)
                                .Select(x => new Tuple<DateTime, double>(x.Key, x.Count()))
                                .ToListAsync();
        }

        public async Task<List<Tuple<DateTime, double>>> SumAndGroupByDay(Expression<Func<WalletTransaction, bool>> predicate = null)
        {
            IQueryable<WalletTransaction> query = _context.WalletTransactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.WalletTransactions.Where(predicate).AsQueryable();
            }

            return await query.GroupBy(x => x.CreatedDate.Date)
                                .Select(x => new Tuple<DateTime, double>(x.Key, (double)x.Sum(y => y.Price)))
                                .ToListAsync();
        }
    }
}