﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class DepositEventRepository : EntityBaseRepository<DepositEvent>, IDepositEventRepository
    {
        public DepositEventRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<DepositEvent> EventByAccount(int accountId, decimal amount, int addProjectId, int addCustomerGroupId)
        {
            var datas = new List<DepositEvent>();
            // Lấy nhóm khách hàng của tài khoản
            var groupsMyAccount = await _context.ProjectAccounts.Where(x => x.AccountId == accountId).ToListAsync();
            var projectIds = groupsMyAccount.Select(x => x.ProjectId).Distinct().ToList();
            if (addProjectId > 0)
            {
                projectIds.Add(addProjectId);
            }
            var customerGroupIds = groupsMyAccount.Select(x => x.CustomerGroupId).Distinct().ToList();
            if (addCustomerGroupId > 0)
            {
                customerGroupIds.Add(addCustomerGroupId);
            }
            // Check xem có những chiến dịch nạp điểm nào trong thời gian này
            var campaigns = await _context.Campaigns.Where(m => m.Status == ECampaignStatus.Active && m.StartDate.Date <= DateTime.Now && m.EndDate.Date > DateTime.Now.AddDays(-1)
                && (m.ProjectId == 0 || projectIds.Contains(m.ProjectId))
                && !_context.WalletTransactions.Any(w => w.AccountId == accountId && w.CampaignId == m.Id && (w.Status == EWalletTransactionStatus.Done || w.Status == EWalletTransactionStatus.Pending) && w.CreatedDate >= m.StartDate)
                ).ToListAsync();
            var campaignIds = campaigns.Select(x => x.Id).ToList();
            // Kiểm tra có lằm trong sự kiện ko
            datas = await _context.DepositEvents.Where(x => x.Used < x.Limit && x.StartAmount <= amount && campaignIds.Contains(x.CampaignId) && (x.CustomerGroupId == 0 || customerGroupIds.Contains(x.CustomerGroupId))).ToListAsync();

            if (!datas.Any())
            {
                return null;
            }    
            // Lấy ra chiến dịch có lợi nhất cho khách hàng nếu có nhiều sự lựa chọn

            foreach (var data in datas)
            {
                if (data.Type == EDepositEventType.Point)
                {
                    data.ToSubPrice = data.SubPoint;
                    data.ToPrice = data.Point;
                }
                else if (data.Type == EDepositEventType.Percent)
                {
                    if (data.Percent > 0)
                    {
                        data.ToPrice = (amount / 100) * data.Percent;
                    }

                    if (data.SubPercent > 0)
                    {
                        data.ToSubPrice = (amount / 100) * data.SubPercent;
                    }

                    if (data.ToPrice + data.ToSubPrice > data.MaxAmount)
                    {
                        // số % point + % sub point chia ra tỉ lệ, từ đó => tỉ lệ được chia trong MaxAmount, ví dụ 50% point và 100% subpoint thì tỉ lệ chia Tổng 150%, 50% = 1/3, 100% là 2/3 => tkc = 500k * 1/3 và tkkm = 500k * 2/3
                        var totalPercent = data.Percent + data.SubPercent;
                        double pricePe = Math.Round(Convert.ToDouble(data.Percent) / Convert.ToDouble(totalPercent), 2);
                        double subPricePe = Math.Round(Convert.ToDouble(data.SubPercent) / Convert.ToDouble(totalPercent), 2);
                        data.ToPrice = Convert.ToDecimal(pricePe) * data.MaxAmount;
                        data.ToSubPrice = Convert.ToDecimal(subPricePe) * data.MaxAmount;
                    }
                }
            }

            var dataMax = datas.Where(x => (x.ToPrice + x.ToSubPrice) == datas.Max(m => m.ToPrice + m.ToSubPrice)).FirstOrDefault();
            if(dataMax == null)
            {
                dataMax = datas.FirstOrDefault();
            }

            if (dataMax.ProvisoType == ECampaignProvisoType.VerifyOke)
            {
                var account = await _context.Accounts.FirstOrDefaultAsync(x => x.Id == accountId);
                if (account == null || account.VerifyStatus != EAccountVerifyStatus.Ok)
                {
                    return null;
                }
            }

            dataMax.Campaign = await _context.Campaigns.FirstOrDefaultAsync(x => x.Id == dataMax.CampaignId);
            return dataMax;
        }

        public async Task UpdateUsed(int id)
        {
            var dl = await _context.DepositEvents.FirstOrDefaultAsync(x => x.Id == id);
            if(dl!=null)
            {
                dl.Used += 1;
                _context.DepositEvents.Update(dl);
                await _context.SaveChangesAsync();
            }    
        }
    }
}
