using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class LanguageRepository : EntityBaseRepository<Language>, ILanguageRepository
    {
        public LanguageRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

