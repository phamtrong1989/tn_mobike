using TN.Domain.Model;
using TN.Infrastructure;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace TN.Infrastructure.Repositories
{
    public class TicketPriceRepository : EntityBaseRepository<TicketPrice>, ITicketPriceRepository
    {
        public TicketPriceRepository(ApplicationContext context) : base(context)
        {
        }
        public async Task<TicketPrepaid> PrepaidUpdatePriceTypeDay(int accountId, int ticketPrepaidId, int addM)
        {
            var kt = await _context.TicketPrepaids.SingleOrDefaultAsync(x => x.AccountId == accountId && x.Id == ticketPrepaidId && (x.TicketPrice_TicketType == ETicketType.Day || x.TicketPrice_TicketType == ETicketType.Month));
            if (kt != null)
            {
                if ((kt.MinutesSpent + addM) > kt.TicketPrice_LimitMinutes)
                {
                    kt.MinutesSpent = kt.TicketPrice_LimitMinutes;
                }
                else
                {
                    kt.MinutesSpent += addM;
                }
                _context.TicketPrepaids.Update(kt);
                await _context.SaveChangesAsync();

                var bookings = await _context.Bookings.Where(x => x.AccountId == accountId && x.Status == EBookingStatus.Start && x.TicketPrepaidId == kt.Id).ToListAsync();
                if (bookings.Any())
                {
                    foreach (var item in bookings)
                    {
                        var ktBooking = await _context.Bookings.FirstOrDefaultAsync(x => x.Id == item.Id && x.Status == EBookingStatus.Start);
                        if (ktBooking != null)
                        {
                            ktBooking.Prepaid_MinutesSpent = kt.MinutesSpent;
                            _context.Bookings.Update(ktBooking);
                        }
                    }
                    await _context.SaveChangesAsync();
                }
                return kt;
            }
            return null;
        }
    }
}

