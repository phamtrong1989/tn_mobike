﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class AccountRepository : EntityBaseRepository<Account>, IAccountRepository
    {
        public AccountRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<AccountDevice>> FilterDevice(int accountId)
        {
            return await _context.AccountDevices.Where(x => x.AcountId == accountId).ToListAsync();
        }

        public async Task<List<Tuple<EAccountVerifyStatus, int>>> GroupByVerifyStatus(int? projectId, DateTime fromDate, DateTime toDate)
        {
            return await _context.Accounts
                .Where(x => (x.VerifyStatus == EAccountVerifyStatus.Not || x.VerifyStatus == EAccountVerifyStatus.Waiting || x.VerifyStatus == EAccountVerifyStatus.Ok)
                            && x.CreatedDate >= fromDate
                            && x.CreatedDate < toDate.AddDays(1)
                //&& (projectId == null || _context.ProjectAccounts.Any(m => m.ProjectId == projectId && m.AccountId == x.Id)) // Chọn theo dự án
                )
                .GroupBy(x => x.VerifyStatus)
                .Select(x => new Tuple<EAccountVerifyStatus, int>(x.Key, x.Count()))
                .ToListAsync();
        }

        public async Task<int> CountWithProjectId(int? projectId = null, Expression<Func<Account, bool>> predicate = null)
        {
            IQueryable<Account> query = _context.Accounts.AsNoTracking();

            //if (projectId != null && projectId > 0)
            //{
            //    query = _context.Accounts.Where(x => _context.ProjectAccounts.Any(m => m.ProjectId == projectId && m.AccountId == x.Id)).AsQueryable();
            //}

            if (predicate != null)
            {
                query = _context.Accounts.Where(predicate).AsQueryable();
            }

            return await query.CountAsync();
        }

        public async Task<List<Tuple<string, double>>> CountAndGroupByTime(int? projectId, string typeTimeView, Expression<Func<Account, bool>> predicate = null)
        {
            IQueryable<Account> query = _context.Accounts.AsNoTracking();

            //if (projectId != null && projectId > 0)
            //{
            //    query = _context.Accounts.Where(x => _context.ProjectAccounts.Any(m => m.ProjectId == projectId && m.AccountId == x.Id)).AsQueryable();
            //}

            if (predicate != null)
            {
                query = _context.Accounts.Where(predicate).AsQueryable();
            }

            if (typeTimeView == "day")
            {
                return await query.GroupBy(x => x.CreatedDate.Value.Date)
                                    .Select(x => new Tuple<string, double>(x.Key.ToString("yyyy/MM/dd"), x.Count()))
                                    .ToListAsync();
            }
            else if (typeTimeView == "month")
            {
                return await query.GroupBy(x => new { x.CreatedDate.Value.Year, x.CreatedDate.Value.Month })
                                    .Select(x => new Tuple<string, double>($"{x.Key.Year}/{x.Key.Month.ToString().PadLeft(2, '0')}", x.Count()))
                                    .ToListAsync();
            }
            //
            return null;
        }

        public async Task<List<Tuple<DateTime, int>>> CountAndGroupByDay(int? projectId, Expression<Func<Account, bool>> predicate = null)
        {
            IQueryable<Account> query = _context.Accounts.AsNoTracking();

            //if (projectId != null && projectId > 0)
            //{
            //    query = _context.Accounts.Where(x => _context.ProjectAccounts.Any(m => m.ProjectId == projectId && m.AccountId == x.Id)).AsQueryable();
            //}

            if (predicate != null)
            {
                query = _context.Accounts.Where(predicate).AsQueryable();
            }

            return await query.GroupBy(x => x.CreatedDate.Value.Date)
                                .Select(x => new Tuple<DateTime, int>(x.Key, x.Count()))
                                .ToListAsync();
        }
    }
}