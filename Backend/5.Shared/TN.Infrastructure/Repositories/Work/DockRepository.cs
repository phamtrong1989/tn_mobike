﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class DockRepository : EntityBaseRepository<Dock>, IDockRepository
    {
        public DockRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<ApiResponseData<List<Dock>>> SearchPagedAsync(int pageIndex, int pageSize, string key, EDockBookingStatus? bookingStatus, Expression<Func<Dock, bool>> predicate = null, Func<IQueryable<Dock>, IOrderedQueryable<Dock>> orderBy = null, Expression<Func<Dock, Dock>> select = null)
        {
            var query = _context.Docks.AsNoTracking();

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            if(key != null)
            {
                query = query.Where(x => key == null || x.IMEI.Contains(key) || x.SerialNumber.Contains(key) || x.SIM.Contains(key) || _context.Bikes.Any(m => m.DockId == x.Id && m.Plate.Replace(".", "").Replace("-", "").Replace(" ", "").ToLower().Contains(key.Replace(".", "").Replace("-", "").Replace(" ", "").ToLower()))).AsQueryable();
            }  
            
            if (bookingStatus != null)
            {
                if (bookingStatus == EDockBookingStatus.Moving)
                {
                    query = query.Where(x => _context.Bookings.Any(m => m.DockId == x.Id && m.Status == EBookingStatus.Start)).AsQueryable();
                }
                else
                {
                    query = query.Where(x => !_context.Bookings.Any(m => m.DockId == x.Id && m.Status == EBookingStatus.Start)).AsQueryable();
                }
            }

            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }

            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }

            return new ApiResponseData<List<Dock>>
            {
                Data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync(),
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = await query.CountAsync()
            };
        }

        public async Task<List<Dock>> SearchNotBooking(int projectId)
        {
            return await _context.Docks.AsNoTracking().Where(x => x.ProjectId == projectId && !_context.Bookings.Any(m => m.DockId == x.Id && m.Status == EBookingStatus.Start)).ToListAsync();
        }

        public async Task<List<Dock>> SearchToReport(EDockBookingStatus? bookingStatus, Expression<Func<Dock, bool>> predicate = null)
        {
            var query = _context.Docks.AsNoTracking();

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            if (bookingStatus != null)
            {
                if (bookingStatus == EDockBookingStatus.Moving)
                {
                    query = query.Where(x => _context.Bookings.Any(m => m.DockId == x.Id && m.Status == EBookingStatus.Start)).AsQueryable();
                }
                else
                {
                    query = query.Where(x => !_context.Bookings.Any(m => m.DockId == x.Id && m.Status == EBookingStatus.Start)).AsQueryable();
                }
            }

            return await query.AsNoTracking().ToListAsync();
        }
    }
}
