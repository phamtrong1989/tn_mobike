﻿using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using Microsoft.EntityFrameworkCore;
using TN.Domain.Model.Common;
using System.Linq.Expressions;
using System;
using static TN.Domain.Model.Bike;

namespace TN.Infrastructure.Repositories
{
    public class BikeRepository : EntityBaseRepository<Bike>, IBikeRepository
    {
        public BikeRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<Bike>> SearchNotBooking(int projectId)
        {
            return await _context.Bikes.AsNoTracking().Where(x => x.ProjectId == projectId && x.DockId > 0 && !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)).ToListAsync();
        }

        public async Task<ApiResponseData<List<Bike>>> SearchPagedAsync(
            int pageIndex,
            int pageSize,
            string key,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            int? battery,
            bool? lockStatus,
            bool? charging,
            Expression<Func<Bike, bool>> predicate = null, Func<IQueryable<Bike>, IOrderedQueryable<Bike>> orderBy = null, Expression<Func<Bike, Bike>> select = null)
        {
            var query = _context.Bikes.AsNoTracking();

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            if (bookingStatus != null)
            {
                if (bookingStatus == EDockBookingStatus.Moving)
                {
                    query = query.Where(x => _context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)).AsQueryable();
                }
                else
                {
                    query = query.Where(x => !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)).AsQueryable();
                }
            }

            if (connectionStatus != null || battery != null || lockStatus != null || charging != null)
            {
                query = query.Where(x => _context.Docks.Any(m => m.Id == x.DockId
                    && (connectionStatus == null || m.ConnectionStatus == connectionStatus)
                    && (lockStatus == null || m.LockStatus == lockStatus)
                    && (lockStatus == null || m.LockStatus == lockStatus)
                    && (battery == null || m.Battery <= battery)
                    && (charging == null || m.Charging == charging)
                )).AsQueryable();
            }

            if (!string.IsNullOrEmpty(key))
            {
                query = query.Where(x => x.SerialNumber.Contains(key) || x.Plate.Replace(".","").Replace("-", "").Replace(" ", "").ToLower().Contains(key.Replace(".", "").Replace("-", "").Replace(" ", "").ToLower()) || (x.DockId > 0 && _context.Docks.Any(m => m.Id == x.DockId && (m.SerialNumber.Contains(key) || m.IMEI.Contains(key))))).AsQueryable();
            }

            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }

            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }

            return new ApiResponseData<List<Bike>>
            {
                Data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync(),
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = await query.CountAsync()
            };
        }

        public async Task<List<Station>> InitTotalBikeInStationAsync(List<Station> stations, int maxBattery)
        {
            var bikeGroupTotal = await _context.Bikes.Where(x => x.Status == Bike.EBikeStatus.Active && _context.Docks.Any(m => m.Id == x.DockId) && !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start) && stations.Select(x => x.Id).Contains(x.StationId)).GroupBy(x => new { x.StationId }).Select(x => new { x.Key.StationId, Total = x.Count() }).ToListAsync();
            var bikeGroupGood = await _context.Bikes.Where(x => x.DockId > 0 && _context.Docks.Any(m => m.Id == x.DockId && m.Battery >= maxBattery && m.ConnectionStatus) && !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start && x.StationId > 0) && x.Status == Bike.EBikeStatus.Active).GroupBy(x => new { x.StationId }).Select(x => new { x.Key.StationId, Total = x.Count() }).ToListAsync();

            var bikes = await _context.Bikes.Where(x => x.Status == Bike.EBikeStatus.Active && _context.Docks.Any(m => m.Id == x.DockId) && !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)).ToListAsync();
            var docks = await _context.Docks.Where(x => bikes.Select(x => x.DockId).Contains(x.Id)).ToListAsync();

            foreach (var item in bikes)
            {
                item.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
            }

            foreach (var item in stations)
            {
                item.Name = string.IsNullOrEmpty(item.DisplayName) ? item.Name : item.DisplayName;
                item.TotalBike = bikeGroupTotal.Where(x => x.StationId == item.Id).Sum(x => x.Total);
                item.TotalBikeGood = bikeGroupGood.Where(x => x.StationId == item.Id).Sum(x => x.Total);
                item.TotalBikeNotGood = item.TotalBike - item.TotalBikeGood;
                item.Bikes = bikes.Where(x => x.StationId == item.Id).ToList();
            }
            return stations;
        }

        public async Task<List<Bike>> BikesInTransaction()
        {
            return await _context.Bikes.Where(x => x.Status == Bike.EBikeStatus.Active && _context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)).Select(x => new Bike
            {
                Id = x.Id,
                Plate = x.Plate,
                Lat = x.Lat,
                Long = x.Long,
                Status = x.Status
            }).ToListAsync();
        }

        public async Task<List<Tuple<EBikeStatus, int>>> GroupByType(int? projectId)
        {
            return await _context.Bikes
                .Where(
                x => (projectId == null || x.ProjectId == projectId)
                    && ((x.Status == Bike.EBikeStatus.Active && x.DockId > 0)
                        || x.Status == Bike.EBikeStatus.Warehouse
                        || x.Status == Bike.EBikeStatus.ErrorWarehouse
                    )
                )
                .GroupBy(x => x.Status)
                .Select(x => new Tuple<EBikeStatus, int>(x.Key, x.Count())
            ).ToListAsync();
        }

        public async Task<List<Bike>> SearchToReport(string key, EDockBookingStatus? bookingStatus, Expression<Func<Bike, bool>> predicate = null)
        {
            var query = _context.Bikes.AsNoTracking();

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            if (bookingStatus != null)
            {
                if (bookingStatus == EDockBookingStatus.Moving)
                {
                    query = query.Where(x => _context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)).AsQueryable();
                }
                else
                {
                    query = query.Where(x => !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)).AsQueryable();
                }
            }

            if (!string.IsNullOrEmpty(key))
            {
                query = query.Where(x => x.SerialNumber.Contains(key) || x.Plate.Contains(key) || (x.DockId > 0 && _context.Docks.Any(m => m.Id == x.DockId && (m.SerialNumber.Contains(key) || m.IMEI.Contains(key))))).AsQueryable();
            }

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<int> CountBikeFreeInStation(int? projectId)
        {
            return await _context.Bikes.Where(
                x => x.StationId > 0
                    && x.DockId > 0
                    && !_context.Bookings.Any(m => m.BikeId == x.Id && m.Status == EBookingStatus.Start)
                    && (projectId == null || x.ProjectId == projectId)
            ).CountAsync();
        }
    }
}