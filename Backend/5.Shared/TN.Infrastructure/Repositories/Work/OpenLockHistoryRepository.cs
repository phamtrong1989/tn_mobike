﻿using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class OpenLockHistoryRepository : EntityBaseRepository<OpenLockHistory>, IOpenLockHistoryRepository
    {
        public OpenLockHistoryRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
