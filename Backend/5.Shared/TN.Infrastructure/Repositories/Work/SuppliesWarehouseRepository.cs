﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class SuppliesWarehouseRepository : EntityBaseRepository<SuppliesWarehouse>, ISuppliesWarehouseRepository
    {
        public SuppliesWarehouseRepository(ApplicationContext context) : base(context)
        {
        }

        public virtual async Task<ApiResponseData<List<SuppliesWarehouse>>> SearchPagedInventoryAsync(
            int pageIndex,
            int pageSize,
            string key,
            int warehouseId,
            ESuppliesType? type
            )
        {
            pageIndex = pageIndex <= 0 ? 1 : pageIndex;
            pageSize = (pageSize > 100 || pageSize < 0) ? 10 : pageSize;

            IQueryable<SuppliesWarehouse> query = _context.Set<SuppliesWarehouse>()
                                                    .Where(x => x.WarehouseId == warehouseId
                                                                && (key == null || _context.Suppliess.Any(s => x.SuppliesId == s.Id && (s.Code.Contains(key) || s.Name.Contains(key))))
                                                                && (type == null || _context.Suppliess.Any(s => x.SuppliesId == s.Id && s.Type == type))
                                                        )
                                                    .AsQueryable();

            return new ApiResponseData<List<SuppliesWarehouse>>
            {
                Data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync(),
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = await query.CountAsync()
            };
        }
    }
}