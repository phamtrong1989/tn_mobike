﻿using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class BookingFailRepository : EntityBaseRepository<BookingFail>, IBookingFailRepository
    {
        public BookingFailRepository(ApplicationContext context) : base(context)
        {
        }
    }
}