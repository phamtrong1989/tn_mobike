using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class AccountRatingItemRepository : EntityBaseRepository<AccountRatingItem>, IAccountRatingItemRepository
    {
        public AccountRatingItemRepository(ApplicationContext context) : base(context)
        {
        }
    }
}