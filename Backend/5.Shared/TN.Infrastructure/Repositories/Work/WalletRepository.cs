﻿using System;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using TN.Utility;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;

namespace TN.Infrastructure.Repositories
{
    public class WalletRepository : EntityBaseRepository<Wallet>, IWalletRepository
    {
        private readonly INotificationJobRepository _iNotificationJobRepository;
        public WalletRepository(ApplicationContext context, INotificationJobRepository iNotificationJobRepository) : base(context)
        {
            _iNotificationJobRepository = iNotificationJobRepository;
        }

        public Tuple<decimal, decimal> SplitTotalPrice(decimal totalPrice, bool allowChargeInSubAccount, Wallet wallet)
        {
            // Trừ tiền trong ví
            if (allowChargeInSubAccount && wallet.SubBalance > 0)
            {
                // Nếu tài khoản phụ đủ tiền trả
                if (totalPrice <= wallet.SubBalance)
                {
                    return new Tuple<decimal, decimal>(0, totalPrice);
                }
                else
                {
                    return new Tuple<decimal, decimal>(totalPrice - wallet.SubBalance, wallet.SubBalance);
                }
            }
            else
            {
                return new Tuple<decimal, decimal>(totalPrice, 0);
            }
        }

        public async Task<Wallet> GetWalletAsync(int accountId)
        {
            var kt = await _context.Wallets.AsNoTracking().FirstOrDefaultAsync(x => x.AccountId == accountId);
            if(kt == null)
            {
                kt = new Wallet()
                {
                    AccountId = accountId,
                    Balance = 0,
                    Status = EWalletStatus.Active,
                    CreateDate = DateTime.Now,
                    SubBalance = 0
                };
                await _context.Wallets.AddAsync(kt);
                await _context.SaveChangesAsync();
            }


            var subEx = await _context.SubPointExpirys.FirstOrDefaultAsync(x => x.AccountId == accountId);
            if (subEx != null)
            {
                kt.SubBalanceExpiryDate = subEx.ExpiryDate;
            }
            var dataDeb = await _context.Transactions.Where(x => x.Status == EBookingStatus.Debt && x.AccountId == accountId).SumAsync(x => x.ChargeDebt);
            kt.Debt = dataDeb ?? 0;
            return kt;
        }

        public async Task<Wallet> UpdateAddWalletAsync(int accountId, decimal balance, decimal subBalance, decimal tripPoint, string hasKey, bool isPush, bool enableDayExpirys, int directDayExpirys)
        {

            var kt = await _context.Wallets.FirstOrDefaultAsync(x => x.AccountId == accountId);
            if (kt == null)
            {
                kt = new Wallet()
                {
                    AccountId = accountId,
                    Balance = 0,
                    Status = EWalletStatus.Active,
                    CreateDate = DateTime.Now,
                    SubBalance = 0
                };
                await _context.Wallets.AddAsync(kt);
                await _context.SaveChangesAsync();
            }
            kt.Balance += balance;
            kt.SubBalance += subBalance;
            kt.TripPoint += tripPoint;

            if (kt.Balance < 0)
            {
                kt.Balance = 0;
            }

            if (kt.SubBalance < 0)
            {
                kt.SubBalance = 0;
            }

            kt.HashCode = Function.SignSHA256((kt.Balance + kt.SubBalance).ToString(), hasKey);

            _context.Wallets.Update(kt);
            await _context.SaveChangesAsync();

            if (balance > 0 && isPush)
            {
                await ExpirysFirstDeposit(accountId);
            }
            // Add hạn cho các giao dịch phát sinh
            if ((balance > 0 || subBalance > 0) && isPush && enableDayExpirys)
            {
                await AddDayExpirys(accountId, balance, subBalance, false);
            }

            // Add trực tiếp số ngày do phát sinh khi có sự kiện nạp
            if (directDayExpirys > 0 && balance > 0 && isPush && enableDayExpirys)
            {
                await AddDayExpirysByDirect(accountId, directDayExpirys, "Phát sinh trong sự kiện nạp điểm trong TG khuyến mãi");
            }

            if (isPush)
            {
                return await HandlingDebtCharges(accountId, kt, hasKey);
            }

            return kt;
        }

        private async Task<Wallet> HandlingDebtCharges(int accountId, Wallet wallet, string hasKey)
        {
            // Bất kỳ có phát sinh giao dịch + tk chính hay tk phụ đều phải xử lý cắt nợ
            // Trừ tài khoản phụ hết sau đó trừ tài khoản chính
            // Lấy ra giao dịch xe qua nợ cước
            var transactions = await _context.Transactions.Where(x => x.AccountId == accountId && x.Status == EBookingStatus.Debt && x.ChargeDebt <= (wallet.Balance + wallet.SubBalance)).ToListAsync();
            if(!transactions.Any())
            {
                return wallet;
            }    

            foreach(var item in transactions)
            {
                if ((wallet.Balance + wallet.SubBalance) < item.ChargeDebt)
                {
                    continue;
                }
                // Cắt xem có thể trừ tk nào
                var splitprice = SplitTotalPrice(item.ChargeDebt ?? 0, item.TicketPrice_AllowChargeInSubAccount, wallet);
                string textNote = $"Hệ thống tự động thanh toán {Function.FormatMoney(item.ChargeDebt)} điểm nợ cước mã chuyến đi {item.TransactionCode}";
                // Tạo lịch sử giao dịch
                await _context.WalletTransactions.AddAsync(new WalletTransaction
                {
                    AccountId = accountId,
                    WalletId = wallet.Id,
                    Type = EWalletTransactionType.TruTienNoCuoc,
                    TransactionCode = item.TransactionCode,
                    OrderIdRef = $"{accountId:D6}-{DateTime.Now:yyyyMMddHHmmssfff}",
                    Amount = splitprice.Item1,
                    SubAmount = splitprice.Item2,
                    Status = EWalletTransactionStatus.Done,
                    TotalAmount = splitprice.Item1 + splitprice.Item2,
                    CreatedDate = DateTime.Now,
                    Wallet_Balance = wallet.Balance,
                    Wallet_SubBalance = wallet.SubBalance,
                    Wallet_HashCode = wallet.HashCode,
                    PaymentGroup = EPaymentGroup.Default,
                    PadTime = DateTime.Now,
                    Note = textNote,
                    HashCode = Function.SignSHA256((splitprice.Item1 + splitprice.Item2).ToString(), hasKey),
                    TransactionId = item.Id
                });
                await _context.SaveChangesAsync();
                //Cập nhật ví 
                await UpdateAddWalletAsync(accountId, -splitprice.Item1, -splitprice.Item2, 0, hasKey, false, false, 0);
                // Cập nhật giao dịch
                item.Status = EBookingStatus.End;
                item.UpdatedDate = DateTime.Now;
                item.ChargeInAccount += splitprice.Item1;
                item.ChargeInSubAccount += splitprice.Item2;
                _context.Transactions.Update(item);
                await _context.SaveChangesAsync();
                // Gửi thông báo cho khác về thanh toàn
                await _iNotificationJobRepository.AddAsync(new NotificationJob { 
                    AccountId = accountId,
                    CreatedDate = DateTime.Now,
                    DeviceId = "",
                    IsSend = false,
                    IsSystem = true,
                    LanguageId = 0,
                    PublicDate = DateTime.Now,
                    Tile = "TNGO thông báo",
                    Message = textNote
                });
                await _context.SaveChangesAsync();
            }
            return wallet;
        }

        public async Task<bool> WalletChecked(bool allowChargeInSubAccount, decimal ticketValue, Wallet wInfo)
        {
            // Tài khoản chính < 0 sẽ không được nhập
            if (wInfo.Balance < 0)
            {
                return false;
            }

            if (wInfo.Balance == 0)
            {
                var dlAccount = await _context.Accounts.AsNoTracking().SingleOrDefaultAsync(x => x.Id == wInfo.AccountId);
                if (dlAccount == null || dlAccount.VerifyStatus != EAccountVerifyStatus.Ok)
                {
                    return false;
                }
            }
            // Tổng số tiền == 0
            if (wInfo.Balance + wInfo.SubBalance == 0)
            {
                return false;
            }
            else if (allowChargeInSubAccount)
            {
                // Giá vé cho phép trừ vào tài khoản phụ
                if (ticketValue > (wInfo.Balance + wInfo.SubBalance))
                {
                    return false;
                }
            }
            else if (!allowChargeInSubAccount && (ticketValue > wInfo.Balance))
            {
                return false;
            }
            else if (!allowChargeInSubAccount && (ticketValue <= wInfo.Balance))
            {
                return true;
            }
            return true;
        }

        public async Task AddDayExpirysByDirect(int accountId, int days, string note)
        {
            var ktA = await _context.SubPointExpirys.FirstOrDefaultAsync(x => x.AccountId == accountId);
            if (ktA == null)
            {
                ktA = new SubPointExpiry
                {
                    AccountId = accountId,
                    IsFirst = false
                };
                await _context.SubPointExpirys.AddAsync(ktA);
                await _context.SaveChangesAsync();
            }
            // Add Log
            var start = (ktA.ExpiryDate == null || ktA.ExpiryDate.Value.Date < DateTime.Now.Date) ? DateTime.Now.Date : ktA.ExpiryDate.Value.Date;
            await _context.SubPointExpiryLogs.AddAsync(new SubPointExpiryLog
            {
                CreatedDate = DateTime.Now,
                AccountId = accountId,
                Days = days,
                ExpiryDate = start,
                Note = note
            });
            await _context.SaveChangesAsync();

            ktA.PrvExpiryDate = start;
            ktA.ExpiryDate = start.AddDays(days);

            if (ktA.ExpiryDate.Value > DateTime.Now && (ktA.ExpiryDate.Value - DateTime.Now).TotalDays > 365)
            {
                ktA.ExpiryDate = DateTime.Now.AddDays(365);
            }

            _context.SubPointExpirys.Update(ktA);
            await _context.SaveChangesAsync();
        }

        public async Task AddDayExpirys(int accountId, decimal balance, decimal subBalance, bool isEndTransaction)
        {
            try
            {
                int tile = isEndTransaction ? 5000 : 2000;
                // 2k 1 ngày
                int totalData = 0;
                totalData += Convert.ToInt32(balance / tile);
                if (subBalance > 0)
                {
                    totalData += Convert.ToInt32(subBalance / (tile * 2));
                }

                if (totalData <= 0)
                {
                    return;
                }

                var ktA = await _context.SubPointExpirys.FirstOrDefaultAsync(x => x.AccountId == accountId);
                if (ktA == null)
                {
                    ktA = new SubPointExpiry
                    {
                        AccountId = accountId,
                        IsFirst = false
                    };
                    await _context.SubPointExpirys.AddAsync(ktA);
                    await _context.SaveChangesAsync();
                }
                // Add Log
                var start = (ktA.ExpiryDate == null || ktA.ExpiryDate.Value.Date < DateTime.Now.Date) ? DateTime.Now.Date : ktA.ExpiryDate.Value.Date;
                await _context.SubPointExpiryLogs.AddAsync(new SubPointExpiryLog
                {
                    CreatedDate = DateTime.Now,
                    AccountId = accountId,
                    Days = totalData,
                    ExpiryDate = start,
                    Note = $"{(isEndTransaction ? "Phát sinh khi kết thúc chuyến đi" : "Phát sinh điểm ")} {balance} được thêm {totalData} ngày"
                });
                await _context.SaveChangesAsync();
                ktA.PrvExpiryDate = start;
                ktA.ExpiryDate = start.AddDays(totalData);

                if (ktA.ExpiryDate.Value > DateTime.Now && (ktA.ExpiryDate.Value - DateTime.Now).TotalDays > 365)
                {
                    ktA.ExpiryDate = DateTime.Now.AddDays(365);
                }

                _context.SubPointExpirys.Update(ktA);
                await _context.SaveChangesAsync();
            }
            catch
            {
            }
        }

        private async Task<bool> ExpirysFirstDeposit(int accountId)
        {
            try
            {
                var ktA = await _context.SubPointExpirys.FirstOrDefaultAsync(x => x.AccountId == accountId);
                if (ktA == null)
                {
                    ktA = new SubPointExpiry
                    {
                        AccountId = accountId,
                        IsFirst = false
                    };
                    await _context.SubPointExpirys.AddAsync(ktA);
                    await _context.SaveChangesAsync();
                }

                if (ktA.IsFirst)
                {
                    return false;
                }

                var ktFirst = await _context.WalletTransactions.AnyAsync(x =>
                    x.AccountId == accountId
                    && (x.Status == EWalletTransactionStatus.Done || x.Status == EWalletTransactionStatus.Pending)
                    && (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                );

                if (ktFirst)
                {
                    // Add Log
                    var start = ktA.ExpiryDate == null ? DateTime.Now : ktA.ExpiryDate.Value;

                    await _context.SubPointExpiryLogs.AddAsync(new SubPointExpiryLog
                    {
                        CreatedDate = DateTime.Now,
                        AccountId = accountId,
                        Days = 60,
                        ExpiryDate = start,
                        Note = "Nạp lần đầu +60 ngày hạn(tương đương 2 tháng)"
                    });
                    await _context.SaveChangesAsync();

                    ktA.PrvExpiryDate = start;
                    ktA.ExpiryDate = start.AddDays(60);
                    ktA.IsFirst = true;
                    _context.SubPointExpirys.Update(ktA);
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
               
            }
            catch
            {
                return false;
            }
        }

        public virtual async Task<ProjectAccountModel> LatLngToCustomerGroup(int accountId)
        {
            var newData = new ProjectAccountModel();
            try
            {
                var accountLocation = await _context.AccountLocations.Where(x => x.AcountId == accountId).FirstOrDefaultAsync();
                if (accountLocation != null)
                {
                    newData = new ProjectAccountModel()
                    {
                        Lat = accountLocation.Lat,
                        Lng = accountLocation.Lng
                    };
                }
                else
                {
                    return new ProjectAccountModel();
                }
                var listProject = await _context.Projects.Where(x => x.Status == EProjectStatus.Active).ToListAsync();
                foreach (var item in listProject)
                {
                    item.ListCityAreaData = new List<LatLngCityAreaDataModel>();
                    var arrayData = item.CityAreaData.Replace("\n", "").Replace("\r", "").Trim('[', ']').Split("],").Select(x => x.Replace("[", "").Replace("]", "").Trim()).ToArray();
                    foreach (var dataItem in arrayData)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(dataItem))
                            {
                                continue;
                            }
                            var itemSpl = dataItem.Split(",").ToArray();
                            item.ListCityAreaData.Add(new LatLngCityAreaDataModel { Latitude = double.Parse(itemSpl[1].Trim(), CultureInfo.GetCultureInfo("en-US")), Longitude = double.Parse(itemSpl[0].Trim(), CultureInfo.GetCultureInfo("en-US")) });
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                var customerGroup = await _context.CustomerGroups.Where(x => x.Status == ECustomerGroupStatus.Active).ToListAsync();

                foreach (var item in listProject)
                {
                    item.DistanceToProject = Function.DistanceInMeter(newData.Lat, newData.Lng, item.Lat ?? 0, item.Lng ?? 0);
                }
                listProject = listProject.OrderBy(x => x.DistanceToProject).ToList();
                foreach (var item in listProject)
                {
                    if (IsPointInPolygon(new LatLngCityAreaDataModel { Latitude = newData.Lat, Longitude = newData.Lng }, item.ListCityAreaData))
                    {
                        newData.ProjectId = item.Id;
                        newData.CustomerGroupId = customerGroup.FirstOrDefault(x => x.ProjectId == item.Id && x.IsDefault)?.Id ?? 0;
                        newData.LocationProjectOverwrite = false;
                        return newData;
                    }
                }
                newData.ProjectId = listProject.FirstOrDefault()?.Id ?? 0;
                newData.CustomerGroupId = customerGroup.FirstOrDefault(x => x.ProjectId == newData.ProjectId && x.IsDefault)?.Id ?? 0;
                newData.LocationProjectOverwrite = true;
                return newData;
            }
            catch
            {
                return newData;
            }
        }

        private bool IsPointInPolygon(LatLngCityAreaDataModel p, List<LatLngCityAreaDataModel> polygon)
        {
            if (polygon == null || polygon.Count <= 0)
            {
                return false;
            }
            double minX = polygon[0].Latitude;
            double maxX = polygon[0].Latitude;
            double minY = polygon[0].Longitude;
            double maxY = polygon[0].Longitude;
            for (int i = 1; i < polygon.Count; i++)
            {
                LatLngCityAreaDataModel q = polygon[i];
                minX = Math.Min(q.Latitude, minX);
                maxX = Math.Max(q.Latitude, maxX);
                minY = Math.Min(q.Longitude, minY);
                maxY = Math.Max(q.Longitude, maxY);
            }
            if (p.Latitude < minX || p.Latitude > maxX || p.Longitude < minY || p.Longitude > maxY)
            {
                return false;
            }
            // https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html
            bool inside = false;
            for (int i = 0, j = polygon.Count - 1; i < polygon.Count; j = i++)
            {
                if ((polygon[i].Longitude > p.Longitude) != (polygon[j].Longitude > p.Longitude) &&
                     p.Latitude < (polygon[j].Latitude - polygon[i].Latitude) * (p.Longitude - polygon[i].Longitude) / (polygon[j].Longitude - polygon[i].Longitude) + polygon[i].Latitude)
                {
                    inside = !inside;
                }
            }
            return inside;
        }
    }
}
