using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class CustomerGroupResourceRepository : EntityBaseRepository<CustomerGroupResource>, ICustomerGroupResourceRepository
    {
        public CustomerGroupResourceRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

