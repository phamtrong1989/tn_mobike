using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class StationResourceRepository : EntityBaseRepository<StationResource>, IStationResourceRepository
    {
        public StationResourceRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

