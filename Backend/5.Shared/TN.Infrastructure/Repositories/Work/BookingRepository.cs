﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using TN.Domain.Model.Common;
using System.Linq.Expressions;

namespace TN.Infrastructure.Repositories
{
    public class BookingRepository : EntityBaseRepository<Booking>, IBookingRepository
    {
        public BookingRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<ApiResponseData<List<Booking>>> SearchPagedAsync(int pageIndex, int pageSize, string key, Expression<Func<Booking, bool>> predicate = null, Func<IQueryable<Booking>, IOrderedQueryable<Booking>> orderBy = null, Expression<Func<Booking, Booking>> select = null)
        {
            pageIndex = pageIndex <= 0 ? 1 : pageIndex;
            pageSize = (pageSize > 100 || pageSize < 0) ? 10 : pageSize;

            var query = _context.Bookings.AsNoTracking();

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            if (key != null)
            {
                query = query.Where(x => key == null || x.TransactionCode == key || _context.Bikes.Any(m => m.Plate.Replace(".", "").Replace("-", "").Replace(" ", "").ToLower() == key.Replace(".", "").Replace("-", "").Replace(" ", "") && x.BikeId == m.Id) || _context.Docks.Any(m => m.SerialNumber == key && x.DockId == m.Id)).AsQueryable();
            }

            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }
            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }
            return new ApiResponseData<List<Booking>>
            {
                Data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync(),
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = await query.CountAsync()
            };
        }
    }
}
