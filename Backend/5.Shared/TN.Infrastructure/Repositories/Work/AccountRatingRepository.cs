﻿using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class AccountRatingRepository : EntityBaseRepository<AccountRating>, IAccountRatingRepository
    {
        public AccountRatingRepository(ApplicationContext context) : base(context)
        {
        }
    }
}