using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class ExportBillRepository : EntityBaseRepository<ExportBill>, IExportBillRepository
    {
        public ExportBillRepository(ApplicationContext context) : base(context)
        {
        }
    }
}