using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class ImportBillRepository : EntityBaseRepository<ImportBill>, IImportBillRepository
    {
        public ImportBillRepository(ApplicationContext context) : base(context)
        {
        }
    }
}