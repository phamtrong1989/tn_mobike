﻿using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class DistrictRepository : EntityBaseRepository<District>, IDistrictRepository
    {
        public DistrictRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
