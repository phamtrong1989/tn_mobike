using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class NewsRepository : EntityBaseRepository<News>, INewsRepository
    {
        public NewsRepository(ApplicationContext context) : base(context)
        {
        }
    }
}