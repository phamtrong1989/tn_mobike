using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class InvestorRepository : EntityBaseRepository<Investor>, IInvestorRepository
    {
        public InvestorRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

