using TN.Domain.Model;
using TN.Infrastructure;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace TN.Infrastructure.Repositories
{
    public class CampaignRepository : EntityBaseRepository<Campaign>, ICampaignRepository
    {
        public CampaignRepository(ApplicationContext context) : base(context)
        {
        }
        public async Task<List<SumLimitModel>> GetSumLimit(List<int> campaignIds)
        {
            if(!campaignIds.Any())
            {
                return new List<SumLimitModel>();
            }

            var data = _context.VoucherCodes.Where(x => campaignIds.Contains(x.CampaignId))
                .GroupBy(x => x.CampaignId)
                .Select(x => new SumLimitModel{
                    CampaignId = x.Key,
                    CurentLimit = x.Sum(m=>m.CurentLimit),
                    Limit = x.Sum(m => m.Limit)
                });

            return await data.ToListAsync();
        }
    }
}

