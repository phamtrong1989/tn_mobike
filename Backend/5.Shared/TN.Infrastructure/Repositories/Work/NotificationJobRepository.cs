﻿using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using System;
using PT.Domain.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace TN.Infrastructure.Repositories
{
    public class NotificationJobRepository : EntityBaseRepository<NotificationJob>, INotificationJobRepository
    {
        public NotificationJobRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task SendNotificationByAccount(List<NotificationTemp> _notificationTempSettings, int accountId, ENotificationTempType type, params string[] prs)
        {
            try
            {
                var lang = (await _context.Accounts.FirstOrDefaultAsync(x => x.Id == accountId))?.Language ?? "vi";
                var dataByLan = _notificationTempSettings.FirstOrDefault(x => x.Language == lang);

                var getNotifyByType = dataByLan?.Data?.FirstOrDefault(x => x.Type == type);
                if (getNotifyByType == null)
                {
                    getNotifyByType = new NotificationTempData()
                    {
                        Body = type.GetDisplayName(),
                        Title = "TNGO",
                        Type = type,
                        Name = type.GetDisplayName()
                    };
                }

                string newBody = getNotifyByType.Body;
                try
                {
                    if (prs != null && prs.Length > 0)
                    {
                        newBody = string.Format(newBody, prs);
                    }
                }
                catch { }

                await _context.NotificationJobs.AddAsync(new NotificationJob
                {
                    CreatedDate = DateTime.Now,
                    AccountId = accountId,
                    DeviceId = "",
                    CreatedUser = 0,
                    IsSend = false,
                    LanguageId = 1,
                    PublicDate = DateTime.Now,
                    Message = newBody,
                    Tile = getNotifyByType.Title,
                    IsSystem = true,
                    DataDockEventType = DockEventType.Not,
                    DataContent = ""
                });
                await _context.SaveChangesAsync();
            }
            catch
            {
            }
        }

        public async Task SendNow(int accountId, string title, string message, int createdUser, DockEventType dataType, string dataContent)
        {
            try
            {
                await _context.NotificationJobs.AddAsync(new NotificationJob
                {
                    CreatedDate = DateTime.Now,
                    AccountId = accountId,
                    DeviceId = "",
                    CreatedUser = createdUser,
                    IsSend = false,
                    LanguageId = 1,
                    PublicDate = DateTime.Now,
                    Message = message,
                    Tile = title,
                    IsSystem = true,
                    DataDockEventType = dataType,
                    DataContent = dataContent
                });
                await _context.SaveChangesAsync();
            }
            catch {}
        }
    }
}

