using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class ContentPageRepository : EntityBaseRepository<ContentPage>, IContentPageRepository
    {
        public ContentPageRepository(ApplicationContext context) : base(context)
        {
        }
    }
}