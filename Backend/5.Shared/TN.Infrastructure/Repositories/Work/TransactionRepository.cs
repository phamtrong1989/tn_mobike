﻿using Microsoft.EntityFrameworkCore;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Infrastructure.Repositories
{
    public class TransactionRepository : EntityBaseRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<ApiResponseData<List<Transaction>>> SearchPagedAsync(int pageIndex, int pageSize, string key, Expression<Func<Transaction, bool>> predicate = null, Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>> orderBy = null, Expression<Func<Transaction, Transaction>> select = null)
        {
            pageIndex = pageIndex <= 0 ? 1 : pageIndex;
            pageSize = (pageSize > 100 || pageSize < 0) ? 10 : pageSize;

            var query = _context.Transactions.AsNoTracking();

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            if (key != null)
            {
                query = query.Where(x => key == null || x.TransactionCode == key || _context.Bikes.Any(m => m.Plate.Replace(".", "").Replace("-", "").Replace(" ", "").ToLower() == key.Replace(".", "").Replace("-", "").Replace(" ", "").ToLower() && x.BikeId == m.Id) || _context.Docks.Any(m => m.SerialNumber == key && x.DockId == m.Id)).AsQueryable();
            }

            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }
            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }
            return new ApiResponseData<List<Transaction>>
            {
                Data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync(),
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = await query.CountAsync()
            };
        }

        public string GetTicketPriceString(TicketPrice item)
        {
            if (item.TicketType == ETicketType.Block)
            {
                return $"{item.TicketType.GetDisplayName()}, { FormatMoney(item.TicketValue)} điểm cho { FormatMoney(item.BlockPerMinute)} phút đầu tiên, nếu vượt quá quy định mỗi { FormatMoney(item.BlockPerViolation)} phút vượt qua tiếp theo cộng thêm { FormatMoney(item.BlockViolationValue) } điểm";
            }
            else if (item.TicketType == ETicketType.Day)
            {
                return $"{item.TicketType.GetDisplayName()}, { FormatMoney(item.TicketValue)} điểm cho { FormatMoney(item.LimitMinutes)} phút sử dụng trong ngày, nếu vượt quá quy định mỗi {FormatMoney(item.BlockPerViolation)} phút vượt qua tiếp theo cộng thêm { FormatMoney(item.BlockViolationValue)} điểm";
            }
            else if (item.TicketType == ETicketType.Month)
            {
                return $"{item.TicketType.GetDisplayName()}, { FormatMoney(item.TicketValue)} điểm cho { FormatMoney(item.LimitMinutes)} phút sử dụng trong tháng, nếu vượt quá quy định mỗi {FormatMoney(item.BlockPerViolation)} phút vượt qua tiếp theo cộng thêm { FormatMoney(item.BlockViolationValue)} điểm";
            }
            return "";
        }

        public Tuple<decimal, decimal> SplitTotalPrice(decimal totalPrice, bool allowChargeInSubAccount, Wallet wallet)
        {
            if (totalPrice <= (wallet.SubBalance + wallet.Balance))
            {
                if (totalPrice <= wallet.SubBalance)
                {
                    return new Tuple<decimal, decimal>(0, totalPrice);
                }
                else
                {
                    return new Tuple<decimal, decimal>(totalPrice - wallet.SubBalance, wallet.SubBalance);
                }
            }
            else
            {
                // Trường hợp ko đủ trừ tiền thì sẽ phải ưu tiền tài khoản phụ âm trước
                return new Tuple<decimal, decimal>(wallet.Balance, totalPrice - wallet.Balance);
            }
        }

        public TotalPriceModel ToTotalPrice(
            ETicketType ticketType,
            DateTime startTime,
            DateTime endTime,
            decimal ticketPrice_TicketValue,
            int ticketPrice_LimitMinutes,
            int ticketPrice_BlockPerMinute,
            int ticketPrice_BlockPerViolation,
            decimal ticketPrice_BlockViolationValue,
            DateTime prepaid_EndTime,
            int prepaid_MinutesSpent,
            DiscountCode discountCode
            )
        {
            var totalMinutes = Function.TotalMilutes(startTime, endTime);
            int soPhutPhat;
            if (ticketType == ETicketType.Block)
            {

                soPhutPhat = totalMinutes - ticketPrice_BlockPerMinute;
                if (soPhutPhat <= 0)
                {
                    return ToEndPrice(discountCode, ticketPrice_TicketValue);
                }
                var totalPrice = ticketPrice_TicketValue + ((soPhutPhat % ticketPrice_BlockPerViolation > 0) ? ((soPhutPhat / ticketPrice_BlockPerViolation) + 1) : (soPhutPhat / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;
                return ToEndPrice(discountCode, totalPrice);
            }
            else if (ticketType == ETicketType.Day || ticketType == ETicketType.Month)
            {
                var soPhutConLai = ticketPrice_LimitMinutes - prepaid_MinutesSpent;

                if (soPhutConLai < 0)
                {
                    soPhutConLai = 0;
                }

                // Vé trả trước trong ngày
                if (prepaid_EndTime > endTime)
                {
                    soPhutPhat = (totalMinutes < soPhutConLai) ? 0 : (totalMinutes - soPhutConLai);
                }
                // trường hợp đi hết hạn vé trả trước
                else
                {
                    // Tính theo end time là mốc hết hạn vé trả trước
                    // thời hạn vé trả trước - thời gian bắt đầu = số phút đi được theo hết mốc
                    // Tổng số phút còn lại > số thời gian theo hết mốc
                    double tongSoPhutHetMoc = (prepaid_EndTime - startTime).TotalMinutes;
                    if (soPhutConLai > tongSoPhutHetMoc)
                    {
                        soPhutPhat = Convert.ToInt32(Math.Ceiling(totalMinutes - tongSoPhutHetMoc));
                    }
                    else
                    {
                        soPhutPhat = totalMinutes - soPhutConLai;
                    }
                }

                if (soPhutPhat <= 0)
                {
                    return new TotalPriceModel();
                }
                var totalPrice = ((soPhutPhat % ticketPrice_BlockPerViolation > 0) ? ((soPhutPhat / ticketPrice_BlockPerViolation) + 1) : (soPhutPhat / ticketPrice_BlockPerViolation)) * ticketPrice_BlockViolationValue;
                return new TotalPriceModel()
                {
                    DiscountPrice = 0,
                    TotalPrice = totalPrice,
                    Price = totalPrice
                };
            }
            return new TotalPriceModel();
        }

        private TotalPriceModel ToEndPrice(DiscountCode discountCode, decimal totalPrice)
        {
            decimal priceForDiscount = 0;
            if (discountCode != null)
            {
                if (discountCode.Type == EDiscountCodeType.Point)
                {
                    priceForDiscount = discountCode.Point;
                }
                else
                {
                    priceForDiscount = (totalPrice / 100) * discountCode.Percent;
                }

                if (priceForDiscount >= totalPrice)
                {
                    return new TotalPriceModel()
                    {
                        DiscountPrice = priceForDiscount,
                        TotalPrice = totalPrice,
                        Price = 0
                    };
                }
                else
                {
                    return new TotalPriceModel()
                    {
                        DiscountPrice = priceForDiscount,
                        TotalPrice = totalPrice,
                        Price = totalPrice - priceForDiscount
                    };
                }
            }
            return new TotalPriceModel()
            {
                TotalPrice = totalPrice,
                DiscountPrice = priceForDiscount,
                Price = totalPrice
            };
        }

        public async Task<List<BikeTransaction>> CountTotalMinuteRentGroupByBikeAsync()
        {
            return await _context.Transactions
                .GroupBy(x => x.BikeId)
                .Select(x => new BikeTransaction
                {
                    BikeId = x.Key,
                    TotalMinutesRent = x.Sum(y => y.TotalMinutes)
                }).ToListAsync();
        }

        public async Task<List<BikeTransaction>> Top20BikeMaxRentTime(int? projectId, DateTime from, DateTime to)
        {
            return await _context.Transactions
                .Where(x => (projectId == null || x.ProjectId == projectId)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1))
                .GroupBy(x => x.BikeId)
                .Select(x => new BikeTransaction
                {
                    BikeId = x.Key,
                    TotalMinutesRent = x.Sum(y => y.TotalMinutes)
                })
                .OrderByDescending(x => x.TotalMinutesRent)
                .Take(20)
                .ToListAsync();
        }

        public async Task<List<BikeTransaction>> Top20BikeMinRentTime(int? projectId, DateTime from, DateTime to)
        {
            return await _context.Transactions
                .Where(x => (projectId == null || x.ProjectId == projectId)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1))
                .GroupBy(x => x.BikeId)
                .Select(x => new BikeTransaction
                {
                    BikeId = x.Key,
                    TotalMinutesRent = x.Sum(y => y.TotalMinutes)
                })
                .OrderBy(x => x.TotalMinutesRent)
                .Take(20)
                .ToListAsync();
        }

        public async Task<List<Tuple<EBookingStatus, int>>> CountTransacionGroupByStatus(int? projectId, DateTime begin, DateTime end)
        {
            return await _context.Transactions
                .Where(x => (projectId == null || x.ProjectId == projectId)
                            && x.CreatedDate >= begin
                            && x.CreatedDate < end.AddDays(1)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.Debt))
                .GroupBy(x => x.Status)
                .Select(x => new Tuple<EBookingStatus, int>(x.Key, x.Count()))
                .ToListAsync();
        }

        public async Task<int> SumTotalMinutesRentByRangeAge(int? projectId, int minAge, int maxAge, DateTime from, DateTime to)
        {
            IQueryable<Transaction> query = _context.Set<Transaction>().AsNoTracking();

            query = query.Where(x => (projectId == null || x.ProjectId == projectId)
                                        && x.CreatedDate >= from
                                        && x.CreatedDate < to.AddDays(1)
                    ).AsQueryable();

            if (minAge == 0 && maxAge == 0)
            {
                query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId && !m.Birthday.HasValue)).AsQueryable();
            }
            else
            {
                query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId
                                                                    && m.Birthday.HasValue
                                                                    && m.Birthday.Value.AddYears(minAge) <= DateTime.Now.Date
                                                                    && m.Birthday.Value.AddYears(maxAge) > DateTime.Now.Date
                                                                )
                        ).AsQueryable();
            }

            return await query.SumAsync(x => x.TotalMinutes);
        }

        public async Task<List<Tuple<string, double>>> CountAndGroupByTime(string typeView, Expression<Func<Transaction, bool>> predicate = null)
        {
            IQueryable<Transaction> query = _context.Transactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.Transactions.Where(predicate).AsQueryable();
            }

            if (typeView == "day")
            {
                return await query.GroupBy(x => x.CreatedDate.Date)
                                    .Select(x => new Tuple<string, double>(x.Key.ToString("yyyy/MM/dd"), x.Count()))
                                    .ToListAsync();
            }
            else if (typeView == "month")
            {
                return await query.GroupBy(x => new { x.CreatedDate.Year, x.CreatedDate.Month })
                                    .Select(x => new Tuple<string, double>($"{x.Key.Year}/{x.Key.Month.ToString().PadLeft(2, '0')}", x.Count()))
                                    .ToListAsync();
            }
            //
            return null;
        }

        public async Task<List<Tuple<string, double>>> CountByAgeAndGroupByTime(string typeView, int minAge, int maxAge, Expression<Func<Transaction, bool>> predicate = null)
        {
            IQueryable<Transaction> query = _context.Transactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.Transactions.Where(predicate).AsQueryable();
            }

            if (minAge == 0 && maxAge == 0)
            {
                query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId && !m.Birthday.HasValue)).AsQueryable();
            }
            else
            {
                query = query.Where(x => _context.Accounts.Any(m => m.Id == x.AccountId
                                                                    && m.Birthday.HasValue
                                                                    && m.Birthday.Value.AddYears(minAge) <= DateTime.Now.Date
                                                                    && m.Birthday.Value.AddYears(maxAge) > DateTime.Now.Date
                                                                )
                        ).AsQueryable();
            }

            if (typeView == "day")
            {
                return await query.GroupBy(x => x.CreatedDate.Date)
                                    .Select(x => new Tuple<string, double>(x.Key.ToString("yyyy/MM/dd"), x.Count()))
                                    .ToListAsync();
            }
            else if (typeView == "month")
            {
                return await query.GroupBy(x => new { x.CreatedDate.Year, x.CreatedDate.Month })
                                    .Select(x => new Tuple<string, double>($"{x.Key.Year}/{x.Key.Month.ToString().PadLeft(2, '0')}", x.Count()))
                                    .ToListAsync();
            }
            //
            return null;
        }

        public async Task<List<Tuple<string, double>>> SumTotalMinutesAndGroupByTime(string typeView, Expression<Func<Transaction, bool>> predicate = null)
        {
            IQueryable<Transaction> query = _context.Transactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.Transactions.Where(predicate).AsQueryable();
            }

            if (typeView == "day")
            {
                return await query.GroupBy(x => x.CreatedDate.Date)
                                    .Select(x => new Tuple<string, double>(x.Key.ToString("yyyy/MM/dd"), x.Sum(y => y.TotalMinutes) / 60))
                                    .ToListAsync();
            }
            else if (typeView == "month")
            {
                return await query.GroupBy(x => new { x.CreatedDate.Year, x.CreatedDate.Month })
                                    .Select(x => new Tuple<string, double>($"{x.Key.Year}/{x.Key.Month.ToString().PadLeft(2, '0')}", x.Sum(y => y.TotalMinutes) / 60))
                                    .ToListAsync();
            }
            //
            return null;
        }

        public async Task<List<Tuple<string, double>>> SumTotalPriceAndGroupByTime(string typeView, Expression<Func<Transaction, bool>> predicate = null)
        {
            IQueryable<Transaction> query = _context.Transactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.Transactions.Where(predicate).AsQueryable();
            }

            if (typeView == "day")
            {
                return await query.GroupBy(x => x.CreatedDate.Date)
                                    .Select(x => new Tuple<string, double>(x.Key.ToString("yyyy/MM/dd"), (double)x.Sum(y => y.TotalPrice)))
                                    .ToListAsync();
            }
            else if (typeView == "month")
            {
                return await query.GroupBy(x => new { x.CreatedDate.Year, x.CreatedDate.Month })
                                    .Select(x => new Tuple<string, double>($"{x.Key.Year}/{x.Key.Month.ToString().PadLeft(2, '0')}", (double)x.Sum(y => y.TotalPrice)))
                                    .ToListAsync();
            }
            //
            return null;
        }

        public async Task<List<Tuple<DateTime, double>>> CountAndGroupByDay(Expression<Func<Transaction, bool>> predicate = null)
        {
            IQueryable<Transaction> query = _context.Transactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.Transactions.Where(predicate).AsQueryable();
            }

            return await query.GroupBy(x => x.CreatedDate.Date)
                                .Select(x => new Tuple<DateTime, double>(x.Key, x.Count()))
                                .ToListAsync();
        }

        public async Task<List<Tuple<DateTime, double>>> SumTotalMinutesAndGroupByDay(Expression<Func<Transaction, bool>> predicate = null)
        {
            IQueryable<Transaction> query = _context.Transactions.AsNoTracking();

            if (predicate != null)
            {
                query = _context.Transactions.Where(predicate).AsQueryable();
            }
            //
            return await query.GroupBy(x => x.CreatedDate.Date)
                                .Select(x => new Tuple<DateTime, double>(x.Key, x.Sum(y => y.TotalMinutes)))
                                .ToListAsync();
        }

        private string FormatMoney(decimal? number)
        {
            if (number == null)
            {
                return "";
            }
            if (number < 1000)
            {
                return number.ToString();
            }
            var culture = CultureInfo.GetCultureInfo("vi-VN");
            return string.Format(culture, "{0:00,0}", number);
        }
    }
}