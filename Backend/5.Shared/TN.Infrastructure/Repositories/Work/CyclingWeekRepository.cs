﻿using TN.Domain.Model;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class CyclingWeekRepository : EntityBaseRepository<CyclingWeek>, ICyclingWeekRepository
    {
        public CyclingWeekRepository(ApplicationContext context) : base(context)
        {
        }

        private async Task<CyclingWeek> GetOrCreate(int accountId, DateTime checkOutTime)
        {
            var checkTT = await _context.CyclingWeeks.FirstOrDefaultAsync(x => x.AccountId == accountId && x.StartDate <= checkOutTime.Date && x.EndDate >= checkOutTime.Date);
            if (checkTT == null)
            {
                int weekCurrent = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(checkOutTime, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                var startTime = checkOutTime.AddDays(-(checkOutTime.DayOfWeek - DayOfWeek.Monday)).Date;
                checkTT = new CyclingWeek
                {
                    AccountId = accountId,
                    WeekNo = weekCurrent,
                    StartDate = startTime,
                    EndDate = startTime.AddDays(6)
                };
                await _context.CyclingWeeks.AddAsync(checkTT);
                await _context.SaveChangesAsync();
            }
            return checkTT;
        }

        private async Task<CyclingWeek> PrevGet(CyclingWeek currentData)
        {
            var prevDate = currentData.StartDate.AddDays(-1);
            return await _context.CyclingWeeks.FirstOrDefaultAsync(x => x.AccountId == currentData.AccountId && x.EndDate == prevDate);
        }

        public async Task<CyclingWeek> UpdateFlag(int accountId, DateTime checkOutTime)
        {
            var checkTT = await GetOrCreate(accountId, checkOutTime);
            var isUpdate = false;
            if(checkTT == null)
            {
                return null;
            }    
            // Nếu là thứ 2 xem CN, TN tuần trước có đi chuyến nào không nếu không thì đính T2 là bắt đầu
            if(checkOutTime.DayOfWeek == DayOfWeek.Monday && checkTT.MondayCycling == null)
            {
                var prvData = await PrevGet(checkTT);
                if(prvData != null && (prvData.SundayCycling != null || prvData.SaturdayCycling != null))
                {
                    checkTT.MondayCycling = 50;
                }   
                else
                {
                    checkTT.MondayCycling = 0;
                }    
                isUpdate = true;
            }
            // Nếu là T3 xem T2 đã bắt đầu chuyến nào chưa, nếu rồi thì + 10%
            else if (checkOutTime.DayOfWeek == DayOfWeek.Tuesday && checkTT.TuesdayCycling == null)
            {
                checkTT.TuesdayCycling = checkTT.MondayCycling != null ? 20 : 0;
                isUpdate = true;
            }
            // Nếu là T4 xem T3 đã bắt đầu chuyến nào chưa, nếu rồi thì + 10%
            else if (checkOutTime.DayOfWeek == DayOfWeek.Wednesday && checkTT.WednesdayCycling == null)
            {
                checkTT.WednesdayCycling = checkTT.TuesdayCycling != null ? (checkTT.TuesdayCycling + 20) : 0;
                isUpdate = true;
            }
            // Nếu là T5 xem T4 đã bắt đầu chuyến nào chưa, nếu rồi thì + 10%
            else if (checkOutTime.DayOfWeek == DayOfWeek.Thursday && checkTT.ThursdayCycling == null)
            {
                checkTT.ThursdayCycling = checkTT.WednesdayCycling != null ? (checkTT.WednesdayCycling + 20) : 0;
                isUpdate = true;
            }
            // Nếu là T6 xem T5 đã bắt đầu chuyến nào chưa, nếu rồi thì + 10%
            else if (checkOutTime.DayOfWeek == DayOfWeek.Friday && checkTT.FridayCycling == null)
            {
                checkTT.FridayCycling = checkTT.ThursdayCycling != null ? (checkTT.ThursdayCycling + 20) : 0;
                isUpdate = true;
            }
            // Nếu là T7 xem T6 đã bắt đầu chuyến nào chưa, nếu rồi thì + 10%
            else if (checkOutTime.DayOfWeek == DayOfWeek.Saturday && checkTT.SaturdayCycling == null)
            {
                checkTT.SaturdayCycling = 0;
                isUpdate = true;
            }
            // Nếu là chủ nhật không được +
            else if (checkOutTime.DayOfWeek == DayOfWeek.Sunday && checkTT.SundayCycling == null)
            {
                checkTT.SundayCycling = 0;
                isUpdate = true;
            }

            if (isUpdate)
            {
                _context.CyclingWeeks.Update(checkTT);
                await _context.SaveChangesAsync();
            }    
            return checkTT;
        }
    }
}

