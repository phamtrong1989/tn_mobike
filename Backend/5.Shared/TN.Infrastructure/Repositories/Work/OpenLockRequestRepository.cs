﻿using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class OpenLockRequestRepository : EntityBaseRepository<OpenLockRequest>, IOpenLockRequestRepository
    {
        public OpenLockRequestRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
