using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class CustomerComplaintRepository : EntityBaseRepository<CustomerComplaint>, ICustomerComplaintRepository
    {
        public CustomerComplaintRepository(ApplicationContext context) : base(context)
        {
        }

        public virtual async Task<ApiResponseData<List<CustomerComplaint>>> SearchPagedAsync(
            int? projectId,
            int pageIndex,
            int pageSize,
            Expression<Func<CustomerComplaint, bool>> predicate = null,
            Func<IQueryable<CustomerComplaint>, IOrderedQueryable<CustomerComplaint>> orderBy = null,
            Expression<Func<CustomerComplaint, CustomerComplaint>> select = null
            )
        {
            pageIndex = pageIndex <= 0 ? 1 : pageIndex;
            pageSize = (pageSize > 100 || pageSize < 0) ? 10 : pageSize;

            IQueryable<CustomerComplaint> query = _context.Set<CustomerComplaint>().AsNoTracking();

            if (projectId != null)
            {
                query = query.Where(x => _context.ProjectAccounts.Any(y => y.ProjectId == projectId && y.AccountId == x.AccountId)).AsQueryable();
            }

            if (predicate != null)
            {
                query = query.Where(predicate).AsQueryable();
            }

            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }

            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }

            return new ApiResponseData<List<CustomerComplaint>>
            {
                Data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync(),
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = await query.CountAsync()
            };
        }
    }
}