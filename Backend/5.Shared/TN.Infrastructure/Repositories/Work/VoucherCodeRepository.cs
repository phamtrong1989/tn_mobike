using TN.Domain.Model;
using TN.Infrastructure;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace TN.Infrastructure.Repositories
{
    public class VoucherCodeRepository : EntityBaseRepository<VoucherCode>, IVoucherCodeRepository
    {
        public VoucherCodeRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<VoucherCode>> VoucherCodePublicByAccount(int accountId)
        {
            var groupsMyAccount = await _context.ProjectAccounts.Where(x => x.AccountId == accountId).Select(x => x.CustomerGroupId).ToListAsync();

            var data = await _context.VoucherCodes
                .Where(x => 
                x.Public
                && (x.CustomerGroupId == 0 || groupsMyAccount.Contains(x.CustomerGroupId))
                && x.StartDate <= DateTime.Now 
                && x.EndDate > DateTime.Now.AddDays(-1)
                && !_context.WalletTransactions.Any(m=> m.AccountId == accountId && m.CampaignId == x.CampaignId)
                && _context.Campaigns.Any(m=> m.Id == x.CampaignId && m.Status == ECampaignStatus.Active && x.CurentLimit > 0)).OrderByDescending(x=>x.Point).Take(200).ToListAsync();
            return data;
        }
    }
}

