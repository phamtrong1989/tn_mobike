﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.Infrastructure.Repositories
{
    public class RFIDBookingRepository : EntityBaseRepository<RFIDBooking>, IRFIDBookingRepository
    {
        public RFIDBookingRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task RFIDTransactionAdd(RFIDBooking cm)
        {
            await _context.RFIDTransactions.AddAsync(new RFIDTransaction
            {
                AccountId = cm.AccountId,
                BikeId = cm.BikeId,
                DockId = cm.DockId,
                EndLat = cm.EndLat,
                InvestorId = cm.InvestorId,
                EndLng = cm.EndLng,
                Note = cm.Note,
                EndTime = cm.EndTime,
                ProjectId = cm.ProjectId,
                RFID = cm.RFID,
                StartLat = cm.StartLat,
                StartLng = cm.StartLng,
                StartTime = cm.StartTime,
                StationIn = cm.StationIn,
                StationOut = cm.StationOut,
                Status = cm.Status,
                Type = cm.Type,
                TransactionCode = cm.TransactionCode,
                UpatedUser = cm.UpatedUser,
                UpdatedDate = cm.UpdatedDate,
                UserId = cm.UserId
            });
            await _context.SaveChangesAsync();

            _context.RFIDBookings.Remove(_context.RFIDBookings.FirstOrDefault(x => x.Id == cm.Id));
            await _context.SaveChangesAsync();
        }
    }
}
