﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class SuppliesRepository : EntityBaseRepository<Supplies>, ISuppliesRepository
    {
        public SuppliesRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<int> GetInventory(int suppliesId, int warehouseId)
        {
            var kt = await _context.SuppliesWarehouses.FirstOrDefaultAsync(x => x.SuppliesId == suppliesId && x.WarehouseId == warehouseId);
            if(kt == null)
            {
                return 0;
            }    
            else
            {
                return kt.Quantity;
            }    
        }

        public async Task<List<Supplies>> SuppliesSearchKey(string key, int warehouseId)
        {
            var data = await SearchTop(20, x => key == null || key == " " || x.Name.Contains(key) || x.Code.Contains(key));
            return await InitSuppliesWarehouses(data, warehouseId);
        }

        public async Task<List<Supplies>> InitSuppliesWarehouses(List<Supplies> data, int warehouseId)
        {
            if (data.Any())
            {
                var w = await _context.SuppliesWarehouses.Where(x => x.WarehouseId == warehouseId && data.Select(m => m.Id).Contains(x.SuppliesId)).ToListAsync();
                foreach (var item in data)
                {
                    item.Quantity = w.FirstOrDefault(x => x.SuppliesId == item.Id)?.Quantity ?? 0;
                }
            }
            return data;
        }

        public async Task SuppliesWarehouseExport(int exportBillId)
        {
            var dl = await _context.ExportBills.FirstOrDefaultAsync(x => x.Id == exportBillId);
            if (dl != null && dl.Status == EExportBillStatus.TK_XacNhanXuatKho)
            {
                var list = await _context.ExportBillItems.Where(x => x.ExportBillId == exportBillId).ToListAsync();
                foreach (var item in list)
                {
                    var subbW = await _context.SuppliesWarehouses.FirstOrDefaultAsync(x => x.SuppliesId == item.SuppliesId && x.WarehouseId == dl.WarehouseId);
                    if(subbW == null)
                    {
                        subbW = new SuppliesWarehouse
                        {
                            SuppliesId = item.SuppliesId,
                            Quantity = 0,
                            WarehouseId = dl.WarehouseId
                        };
                        await _context.SuppliesWarehouses.AddAsync(subbW);
                        await _context.SaveChangesAsync();
                    }
                    subbW.Quantity -= item.Count;
                    _context.SuppliesWarehouses.Update(subbW);
                }
                await _context.SaveChangesAsync();
            }
        }

        public async Task SuppliesWarehouseExportMaintenanceRepair(int maintenanceRepairId, int warehouseId)
        {
            var maintenanceRepair = await _context.MaintenanceRepairs.FirstOrDefaultAsync(x => x.Id == maintenanceRepairId);
            var listOld = await _context.MaintenanceRepairItems.Where(x => x.MaintenanceRepairId == maintenanceRepairId).ToListAsync();
            var listVT = listOld.GroupBy(x => x.SuppliesId).Select(x => x.FirstOrDefault()).ToList();
            var listBike = listOld.GroupBy(x => x.BikeId).Select(x => x.FirstOrDefault()).ToList();
            foreach (var item in listVT)
            {
                var ton = await GetInventory(item.SuppliesId, warehouseId);
                var totalBySupp = listOld.Where(x => x.SuppliesId == item.SuppliesId).Sum(x => x.SuppliesQuantity);

                var subbW = await _context.SuppliesWarehouses.FirstOrDefaultAsync(x => x.SuppliesId == item.SuppliesId && x.WarehouseId == warehouseId);
                if (subbW == null)
                {
                    subbW = new SuppliesWarehouse
                    {
                        SuppliesId = item.SuppliesId,
                        Quantity = 0,
                        WarehouseId = warehouseId
                    };
                    await _context.SuppliesWarehouses.AddAsync(subbW);
                    await _context.SaveChangesAsync();
                }
                subbW.Quantity -= totalBySupp;
                _context.SuppliesWarehouses.Update(subbW);
            }
            await _context.SaveChangesAsync();

            foreach(var item in listBike)
            {
                var bikeInfo = await _context.BikeInfos.FirstOrDefaultAsync(x => x.BikeId == item.BikeId);
                var repairData = listOld.Where(x => x.BikeId == item.BikeId && x.Type == EMaintenanceRepairItemType.Repair);
                var maintenanceData = listOld.Where(x => x.BikeId == item.BikeId && x.Type == EMaintenanceRepairItemType.Maintenane);
                if (bikeInfo == null)
                {
                    bikeInfo = new BikeInfo
                    {
                        BikeId = item.BikeId
                    };
                    await _context.BikeInfos.AddAsync(bikeInfo);
                    await _context.SaveChangesAsync();
                }

                if (repairData.Any())
                {
                    bikeInfo.RepairId = maintenanceRepairId;
                    bikeInfo.RepairDataItem = Newtonsoft.Json.JsonConvert.SerializeObject(repairData);
                    bikeInfo.RepairTime = repairData.LastOrDefault().CompleteDate;
                    bikeInfo.RepairData = Newtonsoft.Json.JsonConvert.SerializeObject(maintenanceRepair);
                }

                if (maintenanceData.Any())
                {
                    bikeInfo.MaintenanceId = maintenanceRepairId;
                    bikeInfo.MaintenanceData = Newtonsoft.Json.JsonConvert.SerializeObject(maintenanceRepair);
                    bikeInfo.MaintenanceDataItem = Newtonsoft.Json.JsonConvert.SerializeObject(maintenanceData);
                    bikeInfo.LastMaintenanceTime = maintenanceData.LastOrDefault().CompleteDate;
                    bikeInfo.TotalMinutes = 0;
                }

                _context.BikeInfos.Update(bikeInfo);
                await _context.SaveChangesAsync();
            }
            var bikes = _context.Bikes.Where(x => x.Status != Bike.EBikeStatus.Inactive);
            var suppliess = _context.Suppliess.Where(x => x.Status != ESuppliesStatus.Enabled);
            // Gửi email cho sếp VTS
            int stt = 1;
            var str = new StringBuilder();
            string styletd = "border: 1px solid silver; padding: 1px 10px;";
            str.Append($"<table style=\"border-collapse: collapse;\">");
            str.Append($"<tr><td style=\"{styletd}\">Stt</td><td style=\"{styletd}\">Biển số</td><td style=\"{styletd}\">Loại lịch</td><td style=\"{styletd}\">Vật tư sử dụng</td><td style=\"{styletd}\">Số lượng vật tư</td><td style=\"{styletd}\">Hoàn thành ngày</td></tr>");
            foreach (var item in listOld)
            {
                var bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                var supplies = suppliess.FirstOrDefault(x => x.Id == item.SuppliesId);
                str.Append($"<tr><td style=\"{styletd}\">{stt}</td><td style=\"{styletd}\">{bike?.Plate}</td><td style=\"{styletd}\">{(item.Type == EMaintenanceRepairItemType.Repair ? "Sửa chữa": "Bảo dưỡng")}</td><td style=\"{styletd}\">{supplies?.Code} - {supplies?.Name}</td><td style=\"{styletd}\">{item.SuppliesQuantity}</td><td style=\"{styletd}\">{item.CompleteDate:dd/MM/yyyy}</td></tr>");
                stt++;
            }
            str.Append("</table>");

            var userDPBD = await _context.Users.FirstOrDefaultAsync(x => x.Id == maintenanceRepair.CreatedUser);
            // User boss 
            var userBoss = await _context.Users.Where(x => x.IsRetired == false && (x.RoleType == RoleManagerType.BOD_VTS || x.RoleType == RoleManagerType.ThuKho_VTS || x.RoleType == RoleManagerType.KTT_VTS)).ToListAsync();

            await _context.EmailBoxs.AddAsync(new EmailBox
            {
                From ="BE TNGO",
                NotSend = false,
                IsSend = false,
                Subject = $"[TNGO-thông báo hoàn thành sửa chữa báo dưỡng {maintenanceRepair?.EndDate:dd-MM-yyyy}] nhân viên hoàn thành \"{ userDPBD?.FullName }\"",
                Body = str.ToString(),
                CreatedDate = DateTime.Now,
                Type = 0,
                To = userDPBD.Email,
                Cc = string.Join(",", userBoss.Select(x => x.Email).ToList())
            });
        }

        public async Task SuppliesWarehouseImport(int exportBillId)
        {
            var dl = await _context.ExportBills.FirstOrDefaultAsync(x => x.Id == exportBillId);
            if (dl != null && dl.Status == EExportBillStatus.TK_XacNhanXuatKho && dl.CompleteStatus == EExportBillCompleteStatus.MatchingFigures)
            {
                var list = await _context.ExportBillItems.Where(x => x.ExportBillId == exportBillId).ToListAsync();
                foreach (var item in list)
                {
                    var subbW = await _context.SuppliesWarehouses.FirstOrDefaultAsync(x => x.SuppliesId == item.SuppliesId && x.WarehouseId == dl.ToWarehouseId);
                    if (subbW == null)
                    {
                        subbW = new SuppliesWarehouse
                        {
                            SuppliesId = item.SuppliesId,
                            Quantity = 0,
                            WarehouseId = dl.ToWarehouseId
                        };
                        await _context.SuppliesWarehouses.AddAsync(subbW);
                        await _context.SaveChangesAsync();
                    }
                    subbW.Quantity += item.Count;
                    _context.SuppliesWarehouses.Update(subbW);
                }
                await _context.SaveChangesAsync();
            }
        }

        public async Task SuppliesWarehouseReImport(int exportBillId)
        {
            var dl = await _context.ExportBills.FirstOrDefaultAsync(x => x.Id == exportBillId);
            if (dl != null && dl.Status == EExportBillStatus.TK_XacNhanXuatKho && dl.CompleteStatus == EExportBillCompleteStatus.ReImport)
            {
                var list = await _context.ExportBillItems.Where(x => x.ExportBillId == exportBillId).ToListAsync();
                foreach (var item in list)
                {
                    var subbW = await _context.SuppliesWarehouses.FirstOrDefaultAsync(x => x.SuppliesId == item.SuppliesId && x.WarehouseId == dl.WarehouseId);
                    if (subbW == null)
                    {
                        subbW = new SuppliesWarehouse
                        {
                            SuppliesId = item.SuppliesId,
                            Quantity = 0,
                            WarehouseId = dl.WarehouseId
                        };
                        await _context.SuppliesWarehouses.AddAsync(subbW);
                        await _context.SaveChangesAsync();
                    }
                    subbW.Quantity += item.Count;
                    _context.SuppliesWarehouses.Update(subbW);
                }
                await _context.SaveChangesAsync();
            }
        }
    }
}