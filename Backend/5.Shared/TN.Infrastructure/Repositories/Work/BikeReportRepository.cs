using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class BikeReportRepository : EntityBaseRepository<BikeReport>, IBikeReportRepository
    {
        public BikeReportRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
