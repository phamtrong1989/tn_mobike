﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class RoleRepository : EntityBaseRepository<ApplicationRole>, IRoleRepository
    {
        private readonly ApplicationContext db;
        public RoleRepository(ApplicationContext context) : base(context)
        {
            db = context;
        }

        public virtual async Task<bool> IsUse(int roleId)
        {
            return await db.UserRoles.AnyAsync(m => m.RoleId == roleId);
        }

        public virtual async Task<List<ApplicationRole>> RolesByUser(int userId)
        {
            return await db.Roles.Where(x => db.UserRoles.Any(m => m.UserId == userId && x.Id == m.RoleId)).AsNoTracking().ToListAsync();
        }

        public virtual async Task UpdateRoleDetail(int roleId, List<int> ids)
        {
            db.RoleDetails.RemoveRange(db.RoleDetails.Where(x => x.RoleId == roleId));
            await db.SaveChangesAsync();

            foreach (var item in ids)
            {
                await db.RoleDetails.AddAsync(new RoleDetail
                {
                    ActionId = item,
                    RoleId = roleId
                });
            }
            await db.SaveChangesAsync();
        }

        public async Task<RoleDetailObjModel> GetRoleDetail(int roleId)
        {
            var roleControlls = await db.RoleControllers.Where(x => x.IsShow).OrderBy(x => x.Order).ToListAsync();
            var roleActions = await db.RoleActions.Where(x => x.IsShow).OrderBy(x => x.Order).ToListAsync();
            var roleDetails = await db.RoleDetails.Where(x => x.RoleId == roleId).ToListAsync();
            var roleGroups = await db.RoleGroups.Where(x => x.IsShow).OrderBy(x=>x.Order).ToListAsync();

            return new RoleDetailObjModel
            {
                RoleControllers = roleControlls,
                RoleActions = roleActions,
                RoleDetails = roleDetails,
                RoleGroups = roleGroups
            };
        }
    }
}