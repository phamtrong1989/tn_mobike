﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Linq.Expressions;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Infrastructure.Repositories
{
    public class UserRepository : EntityBaseRepository<ApplicationUser>, IUserRepository
    {
        public UserRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<DataRoleActionModel>> RoleActionsByUserAsync(int userId)
        {
            var newList = new List<DataRoleActionModel>();
            var controllers = await _context.RoleControllers.Where(x => x.IsShow).ToListAsync();
            var listAction = await _context.RoleActions.Where(m => _context.UserRoles.Any(x => x.UserId == userId && _context.RoleDetails.Any(r => r.RoleId == x.RoleId && r.ActionId == m.Id))).ToListAsync();
            foreach(var item in listAction)
            {
                newList.Add(new DataRoleActionModel
                {
                    ActionName = item.ActionName,
                    AreaName = controllers.FirstOrDefault(m => m.Id == item.ControllerId)?.AreaId ?? "Null",
                    ControllerName = item.ControllerId,
                    Id = item.Id
                });
            }    
            return newList.ToList();
        }

        public async Task<List<ApplicationUser>> SeachByIds(List<int> ids)
        {
            return await _context.Users.Where(x => ids.Contains(x.Id)).Select(x=> new ApplicationUser { Id = x.Id, DisplayName = x.DisplayName, UserName = x.UserName, Sex = x.Sex, PhoneNumber = x.PhoneNumber, FullName = x.FullName , Code = x.Code, RoleType = x.RoleType, RoleName = x.RoleType.ToDisplayName()}).AsNoTracking().ToListAsync();
        }

        public async Task<List<ApplicationUser>> SeachByIds(List<int> ids, List<int> ids2)
        {
            return await _context.Users.Where(x => ids.Contains(x.Id) || ids2.Contains(x.Id)).Select(x => new ApplicationUser { Id = x.Id, DisplayName = x.DisplayName, UserName = x.UserName, Sex = x.Sex, PhoneNumber = x.PhoneNumber, FullName = x.FullName, Code = x.Code, RoleType = x.RoleType, RoleName = x.RoleType.ToDisplayName() }).AsNoTracking().ToListAsync();
        }

        public async Task<List<ApplicationUser>> SeachByIds(List<int> ids, List<int> ids2, List<int> ids3)
        {
            return await _context.Users.Where(x => ids.Contains(x.Id) || ids2.Contains(x.Id) || ids3.Contains(x.Id)).Select(x => new ApplicationUser { Id = x.Id, DisplayName = x.DisplayName, UserName = x.UserName, Sex = x.Sex, PhoneNumber = x.PhoneNumber, FullName = x.FullName, Code = x.Code, RoleType = x.RoleType, RoleName = x.RoleType.ToDisplayName() }).AsNoTracking().ToListAsync();
        }

        public async Task<List<RoleActionModel>> RoleActionsByUser2Async(int userId)
        {
            //var listAllRole = await _context.RoleActions.Where(m => m.IsShow == true).Select(x => new RoleActionModel
            //{
            //    ControllerName = x.ControllerId,
            //    AreaName = x.RoleController.AreaId,
            //    ActionName = x.Name,
            //    Id = x.Id,
            //    Is=false
            //}).ToListAsync();

              var listCa = await _context.RoleActions.Where(m =>
                 _context.UserRoles.Any(x => x.UserId == userId && _context.RoleDetails.Any(r => r.RoleId == x.RoleId && r.ActionId == m.Id))
                ).GroupBy(m=>m.Id).Select(m=>m.FirstOrDefault()).Select(x => new RoleActionModel
                {
                    ControllerName = x.ControllerId,
                    AreaName = x.RoleController.AreaId,
                    ActionName = x.Name,
                    Id = x.Id
                }).ToListAsync();

           // listAllRole.ForEach(x => x.Is = listCa.Any(m=>m.Id==x.Id));
            return listCa;
        }

        public async Task<int> StatusUserAsync(int userId)
        {
            var dl= await _context.Users
                .Where(m=>m.Id==userId)
                .Select(
                    x=>new ApplicationUser {
                        Id = x.Id,
                        IsLock =x.IsLock,
                        IsReLogin =x.IsReLogin
                    })
                .FirstOrDefaultAsync();
            if (dl == null) return 0;
            else if (dl.IsReLogin) return 3;
            else if (dl.IsLock) return 2;
            return 1;
        }
        public async Task<BaseLogDataModel<string>> UpdateRolesAsync(int userId,List<int> roles)
        {
            var listFirst =await _context.UserRoles.Where(m => m.UserId == userId).ToListAsync();
            var strFirst = Newtonsoft.Json.JsonConvert.SerializeObject(listFirst);
            _context.UserRoles.RemoveRange(listFirst);
            await _context.SaveChangesAsync();
            await _context.UserRoles.AddRangeAsync(roles.Select(m => new IdentityUserRole<int> { RoleId = m, UserId = userId }));
            await _context.SaveChangesAsync();
            var listLast =await _context.UserRoles.Where(m => m.UserId == userId).ToListAsync();
            return new BaseLogDataModel<string>() { DataBefore = strFirst,DataAfter= Newtonsoft.Json.JsonConvert.SerializeObject(listLast) };
        }
        public async Task<List<int>> GetRolesAsync(int userId)
        {
            return await _context.UserRoles.Where(m => m.UserId == userId).Select(x => x.RoleId).ToListAsync();
        }

        public  async Task<BaseSearchModel<List<ApplicationUser>>> SearchPaged2List(int page, int limit, int? roleId, Expression<Func<ApplicationUser, bool>> predicate = null, Func<IQueryable<ApplicationUser>, IOrderedQueryable<ApplicationUser>> orderBy = null, Expression<Func<ApplicationUser, ApplicationUser>> select = null, params Expression<Func<ApplicationUser, object>>[] includeProperties)
        {
            IQueryable<ApplicationUser> query = _context.Set<ApplicationUser>();
            if (predicate != null)
            {
                query = _context.Set<ApplicationUser>().Where(predicate).AsQueryable();
            }
            if(roleId != null)
            {
                query= query.Where(m => _context.UserRoles.Any(x => x.UserId == m.Id && x.RoleId == roleId)).AsQueryable();
            }
            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }
            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }
            
            var currentData = await query.Skip((page - 1) * limit).Take(limit).ToListAsync();
            var rolesUser = await _context.UserRoles.Where(m => currentData.Any(x => x.Id == m.UserId))
                .Join(_context.Roles,
                entryPoint => entryPoint.RoleId,
                entry => entry.Id,
                (entryPoint, entry) => new { entryPoint, entry }).ToListAsync();
            foreach (var item in currentData)
            {
                //item.Roles = rolesUser.Where(m => m.entryPoint.UserId == item.Id)
                //                        .Select(x => new IdentityRole<int>
                //                        {
                //                            Id = x.entry.Id,
                //                            Name = x.entry.Name
                //                        }).ToList();
            }
            return new BaseSearchModel<List<ApplicationUser>>
            {
                Data = currentData,
                Limit = limit,
                Page = page,
                ReturnUrl = "",
                TotalRows = await query.Select(m => 1).CountAsync()
            };
        }

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchPagedAsync(
            int pageIndex,
            int pageSize,
            int? roleId,
            Expression<Func<ApplicationUser, bool>> predicate = null,
            Func<IQueryable<ApplicationUser>, IOrderedQueryable<ApplicationUser>> orderBy = null,
            Expression<Func<ApplicationUser, ApplicationUser>> select = null,
            params Expression<Func<ApplicationUser, object>>[] includeProperties
            )
        {
            IQueryable<ApplicationUser> query = _context.Set<ApplicationUser>();
            if (predicate != null)
            {
                query = _context.Set<ApplicationUser>().Where(predicate).AsQueryable();
            }

            if (roleId != null)
            {
                query = query.Where(m => _context.UserRoles.Any(x => x.UserId == m.Id && x.RoleId == roleId)).AsQueryable();
            }

            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }

            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }

            var currentData = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            var ids = currentData.Select(x => x.Id);

            var rolesUser = await _context.UserRoles.Where(m => ids.Contains(m.UserId))
                .Join(_context.Roles,
                entryPoint => entryPoint.RoleId,
                entry => entry.Id,
                (entryPoint, entry) => new { entryPoint, entry }).ToListAsync();

            foreach (var item in currentData)
            {
                item.Roles = rolesUser.Where(m => m.entryPoint.UserId == item.Id)
                                        .Select(x => new ApplicationRole
                                        {
                                            Id = x.entry.Id,
                                            Name = x.entry.Name
                                        }).ToList();
            }
            return new ApiResponseData<List<ApplicationUser>>
            {
                Data = currentData,
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = await query.Select(m => 1).CountAsync()
            };
        }

        public  void DeleteRoles(int userId)
        {
              _context.UserRoles.RemoveRange(_context.UserRoles.Where(m => m.UserId == userId));
        }
    }
}
