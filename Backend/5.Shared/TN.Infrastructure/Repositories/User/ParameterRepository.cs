﻿using TN.Infrastructure.Interfaces;
using TN.Domain.Model;
using System.Threading.Tasks;
using static TN.Domain.Model.Parameter;
using System.Linq;

namespace TN.Infrastructure.Repositories
{
    public class ParameterRepository : EntityBaseRepository<Parameter>, IParameterRepository
    {
        private readonly ApplicationContext db;
        public ParameterRepository(ApplicationContext context) : base(context)
        {
            db = context;
        }
        public async Task UpdateValue(ParameterType type, string value)
        {
            var kt = db.Parameters.FirstOrDefault(x => x.Type == type);
            if (kt == null)
            {
                kt = new Parameter
                {
                    Type = type,
                    Value = value
                };
                await db.Parameters.AddAsync(kt);
                await db.SaveChangesAsync();
                return;
            }
            kt.Value = value;
            db.Parameters.Update(kt);
            await db.SaveChangesAsync();
            return;
        }

    }
}
