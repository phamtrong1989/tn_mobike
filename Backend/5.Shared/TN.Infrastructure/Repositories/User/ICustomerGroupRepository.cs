﻿using TN.Infrastructure.Interfaces;
using TN.Domain.Model;

namespace TN.Infrastructure.Repositories
{
    public class CustomerGroupRepository : EntityBaseRepository<CustomerGroup>, ICustomerGroupRepository
    {
        private readonly ApplicationContext db;
        public CustomerGroupRepository(ApplicationContext context) : base(context)
        {
            db = context;
        }
    }
}
