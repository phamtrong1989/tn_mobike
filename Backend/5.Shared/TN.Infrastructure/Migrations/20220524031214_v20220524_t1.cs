﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TN.Infrastructure.Migrations
{
    public partial class v20220524_t1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DiscountCodeId",
                table: "Transaction",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountCode_Point",
                table: "Transaction",
                type: "decimal (18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountCode_Price",
                table: "Transaction",
                type: "decimal (18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DiscountCode_ProjectId",
                table: "Transaction",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "DiscountCode_Type",
                table: "Transaction",
                type: "tinyint",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DiscountCodeId",
                table: "Booking",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountCode_Point",
                table: "Booking",
                type: "decimal (18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountCode_Price",
                table: "Booking",
                type: "decimal (18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DiscountCode_ProjectId",
                table: "Booking",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "DiscountCode_Type",
                table: "Booking",
                type: "tinyint",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DiscountCode",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    InvestorId = table.Column<int>(type: "int", nullable: false),
                    CustomerGroupId = table.Column<int>(type: "int", nullable: false),
                    CampaignId = table.Column<int>(type: "int", nullable: false),
                    Code = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Type = table.Column<byte>(type: "tinyint", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Limit = table.Column<int>(type: "int", nullable: false),
                    CurentLimit = table.Column<int>(type: "int", nullable: false),
                    Point = table.Column<decimal>(type: "decimal (18,2)", nullable: false),
                    Public = table.Column<bool>(type: "bit", nullable: false),
                    MaximumUses = table.Column<int>(type: "int", nullable: false),
                    ProvisoType = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedUser = table.Column<int>(type: "int", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedUser = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountCode", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DiscountCode");

            migrationBuilder.DropColumn(
                name: "DiscountCodeId",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "DiscountCode_Point",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "DiscountCode_Price",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "DiscountCode_ProjectId",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "DiscountCode_Type",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "DiscountCodeId",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "DiscountCode_Point",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "DiscountCode_Price",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "DiscountCode_ProjectId",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "DiscountCode_Type",
                table: "Booking");
        }
    }
}
