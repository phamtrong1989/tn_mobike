﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TN.Infrastructure.Migrations
{
    public partial class v20220919_t1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GroupAppointment",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StationId = table.Column<int>(type: "int", nullable: false),
                    LeaderId = table.Column<int>(type: "int", nullable: false),
                    LeaderFullName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    LeaderSex = table.Column<byte>(type: "tinyint", nullable: false),
                    Descripts = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    Status = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupAppointment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GroupAppointmentItem",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GroupAppointmentId = table.Column<int>(type: "int", nullable: false),
                    MemberId = table.Column<int>(type: "int", nullable: false),
                    MemberFullName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    MemberSex = table.Column<byte>(type: "tinyint", nullable: false),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsConfirm = table.Column<bool>(type: "bit", nullable: false),
                    TransactionCode = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupAppointmentItem", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupAppointment");

            migrationBuilder.DropTable(
                name: "GroupAppointmentItem");
        }
    }
}
