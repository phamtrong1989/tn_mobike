﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TN.Infrastructure.Migrations
{
    public partial class v20220919_t2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Amount",
                table: "GroupAppointment",
                newName: "MaxAmount");

            migrationBuilder.AddColumn<int>(
                name: "CurrentAmount",
                table: "GroupAppointment",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentAmount",
                table: "GroupAppointment");

            migrationBuilder.RenameColumn(
                name: "MaxAmount",
                table: "GroupAppointment",
                newName: "Amount");
        }
    }
}
