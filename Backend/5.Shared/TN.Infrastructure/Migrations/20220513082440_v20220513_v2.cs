﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TN.Infrastructure.Migrations
{
    public partial class v20220513_v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LocationCustomerGroupId",
                table: "Account",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LocationProjectId",
                table: "Account",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "LocationProjectOverwrite",
                table: "Account",
                type: "bit",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationCustomerGroupId",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "LocationProjectId",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "LocationProjectOverwrite",
                table: "Account");
        }
    }
}
