﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TN.Infrastructure.Migrations
{
    public partial class v20220527_t1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvestorId",
                table: "DiscountCode");

            migrationBuilder.DropColumn(
                name: "MaximumUses",
                table: "DiscountCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InvestorId",
                table: "DiscountCode",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MaximumUses",
                table: "DiscountCode",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
