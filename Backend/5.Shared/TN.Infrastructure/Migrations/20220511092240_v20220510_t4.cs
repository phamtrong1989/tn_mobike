﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TN.Infrastructure.Migrations
{
    public partial class v20220510_t4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LocationProjectId",
                table: "WalletTransaction",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "LocationProjectOverwrite",
                table: "WalletTransaction",
                type: "bit",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationProjectId",
                table: "WalletTransaction");

            migrationBuilder.DropColumn(
                name: "LocationProjectOverwrite",
                table: "WalletTransaction");
        }
    }
}
