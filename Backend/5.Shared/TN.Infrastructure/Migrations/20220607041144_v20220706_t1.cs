﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TN.Infrastructure.Migrations
{
    public partial class v20220706_t1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GroupTransactionCode",
                table: "Transaction",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GroupTransactionOrder",
                table: "Transaction",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupTransactionCode",
                table: "Booking",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GroupTransactionOrder",
                table: "Booking",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GroupTransactionCode",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "GroupTransactionOrder",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "GroupTransactionCode",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "GroupTransactionOrder",
                table: "Booking");
        }
    }
}
