﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TN.Infrastructure.Migrations
{
    public partial class v20220919_t4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "GroupAppointmentItem");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "GroupAppointmentItem");

            migrationBuilder.RenameColumn(
                name: "Descripts",
                table: "GroupAppointment",
                newName: "Descriptions");

            migrationBuilder.AlterColumn<string>(
                name: "TransactionCode",
                table: "GroupAppointmentItem",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "MemberPhone",
                table: "GroupAppointmentItem",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "GroupAppointment",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "LeaderPhone",
                table: "GroupAppointment",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "GroupAppointment",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ReputationLevel",
                table: "GroupAppointment",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "StationLat",
                table: "GroupAppointment",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "StationLng",
                table: "GroupAppointment",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "TransactionCode",
                table: "GroupAppointment",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GroupAppointmentAccount",
                columns: table => new
                {
                    AccountId = table.Column<int>(type: "int", nullable: false),
                    ReputationLevel = table.Column<int>(type: "int", nullable: true),
                    Lock = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupAppointmentAccount", x => x.AccountId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupAppointmentAccount");

            migrationBuilder.DropColumn(
                name: "MemberPhone",
                table: "GroupAppointmentItem");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "GroupAppointment");

            migrationBuilder.DropColumn(
                name: "LeaderPhone",
                table: "GroupAppointment");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "GroupAppointment");

            migrationBuilder.DropColumn(
                name: "ReputationLevel",
                table: "GroupAppointment");

            migrationBuilder.DropColumn(
                name: "StationLat",
                table: "GroupAppointment");

            migrationBuilder.DropColumn(
                name: "StationLng",
                table: "GroupAppointment");

            migrationBuilder.DropColumn(
                name: "TransactionCode",
                table: "GroupAppointment");

            migrationBuilder.RenameColumn(
                name: "Descriptions",
                table: "GroupAppointment",
                newName: "Descripts");

            migrationBuilder.AlterColumn<int>(
                name: "TransactionCode",
                table: "GroupAppointmentItem",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Amount",
                table: "GroupAppointmentItem",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartTime",
                table: "GroupAppointmentItem",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
