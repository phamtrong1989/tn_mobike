﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Base.Models.NgxChart;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Utility;
using static TN.Domain.Model.Bike;

namespace TN.Base.Services
{
    public interface ITroubleshootingCenterService : IService<Station>
    {
        Task<List<Tuple<string, int>>> GetAnalyticTransaction(string type, int? projectId, DateTime statisticDate);
        Task<List<Tuple<string, double>>> GetAnalyticRevenue(string type, int? projectId, DateTime statisticDate);
        Task<List<Tuple<string, int>>> GetAnalyticCustomer(string type, int? projectId, DateTime statisticDate);
        Task<List<Tuple<string, int, decimal>>> GetAnalyticCashFlow(string type, int? projectId, DateTime statisticDate);
        Task<List<Tuple<string, int>>> GetAnalyticBike(int? projectId);
        Task<List<Tuple<string, int, string>>> GetAnalyticDock(int? projectId);
    }

    public class TroubleshootingCenterService : ITroubleshootingCenterService
    {
        private readonly ILogger _logger;
        private readonly LogSettings _logSettings;

        private readonly IBikeRepository _iBikeRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly ITicketPrepaidRepository _iTicketPrepaidRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;

        public TroubleshootingCenterService
        (
            ILogger<StationService> logger,
            IOptions<LogSettings> logSettings,
            IBikeRepository iBikeRepository,
            IDockRepository iDockRepository,
            IBookingRepository iBookingRepository,
            IAccountRepository iAccountRepository,
            ITransactionRepository iTransactionRepository,
            ITicketPrepaidRepository iTicketPrepaidRepository,
            IWalletTransactionRepository iWalletTransactionRepository
        )
        {
            _logger = logger;
            _logSettings = logSettings.Value;
            _iBikeRepository = iBikeRepository;
            _iDockRepository = iDockRepository;
            _iBookingRepository = iBookingRepository;
            _iAccountRepository = iAccountRepository;
            _iTransactionRepository = iTransactionRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iTicketPrepaidRepository = iTicketPrepaidRepository;
        }

        public async Task<List<Tuple<string, double>>> GetAnalyticRevenue(string type, int? projectId, DateTime statisticDate)
        {
            try
            {
                var rangeDay = _GetRangeDay(type);

                if (type == "day")
                {
                    rangeDay = new Tuple<DateTime, DateTime>(statisticDate, statisticDate);
                }

                // Nạp tiền
                var rechargeAll = await _iWalletTransactionRepository
                    .Sum(
                        x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                                && x.CreatedDate >= rangeDay.Item1
                                && x.CreatedDate < rangeDay.Item2.AddDays(1)
                                && x.Status == EWalletTransactionStatus.Done
                        ,
                        x => (double)x.Price
                    );

                var recharge = await _iWalletTransactionRepository
                    .Sum(
                        x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                                && x.CreatedDate >= rangeDay.Item1
                                && x.CreatedDate < rangeDay.Item2.AddDays(1)
                                && x.Status == EWalletTransactionStatus.Done
                                && (projectId == null || x.LocationProjectId == projectId)
                        ,
                        x => (double)x.Price
                    );

                // Tiền thuê xe
                var postpaid = await _iTransactionRepository
                    .Sum(
                        x => (projectId == null || x.ProjectId == projectId)
                                && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall)
                                && x.CreatedDate >= rangeDay.Item1
                                && x.CreatedDate < rangeDay.Item2.AddDays(1)
                        ,
                        x => (double)x.TotalPrice
                    );

                var prepaid = await _iTicketPrepaidRepository
                    .Sum(
                        x => (projectId == null || x.ProjectId == projectId)
                                && x.CreatedDate >= rangeDay.Item1
                                && x.CreatedDate < rangeDay.Item2.AddDays(1)
                        ,
                        x => (double)x.TicketValue
                    );

                var rent = postpaid + prepaid;

                // Tiền nợ cước
                var debt = await _iTransactionRepository
                    .Sum(x => ((projectId == null || x.ProjectId == projectId)
                                && x.Status == EBookingStatus.Debt)
                                && x.CreatedDate >= rangeDay.Item1
                                && x.CreatedDate < rangeDay.Item2.AddDays(1)
                        ,
                        x => (double)x.ChargeDebt
                    );

                return new List<Tuple<string, double>>
                {
                    new Tuple<string, double>("Tiền nạp tổng", rechargeAll),
                    new Tuple<string, double>("Tiền nạp dự án", recharge),
                    new Tuple<string, double>("Đã thuê xe", rent),
                    new Tuple<string, double>("Nợ cước", debt),
                };
            }
            catch (Exception ex)
            {
                _logger.LogError("TroubleshootingCenterService.Revenue", ex.ToString());
                return new List<Tuple<string, double>>();
            }
        }

        public async Task<List<Tuple<string, int, decimal>>> GetAnalyticCashFlow(string type, int? projectId, DateTime statisticDate)
        {
            try
            {
                var rangeDay = _GetRangeDay(type);

                if (type == "day")
                {
                    rangeDay = new Tuple<DateTime, DateTime>(statisticDate, statisticDate);
                }

                var groupPayGates = await _iWalletTransactionRepository.GroupByPayGate(projectId, rangeDay.Item1, rangeDay.Item2);
                var directPayment = await _iWalletTransactionRepository.CountAndSumDirectPayment(projectId, rangeDay.Item1, rangeDay.Item2);

                var result = new List<Tuple<string, int, decimal>>
                {
                    new Tuple<string, int, decimal>("MOMO", 0, 0),
                    new Tuple<string, int, decimal>("ZALOPay", 0, 0),
                    new Tuple<string, int, decimal>("VTCPay", 0, 0),
                    new Tuple<string, int, decimal>("Payoo", 0, 0),
                    new Tuple<string, int, decimal>("Trực tiếp", 0, 0)
                };

                foreach (var item in groupPayGates)
                {
                    if (item.Item1 == EPaymentGroup.MomoPay)
                    {
                        result[0] = new Tuple<string, int, decimal>("MOMO", item.Item2, item.Item3);
                    }
                    //
                    if (item.Item1 == EPaymentGroup.ZaloPay)
                    {
                        result[1] = new Tuple<string, int, decimal>("ZALOPay", item.Item2, item.Item3);
                    }
                    //
                    if (item.Item1 == EPaymentGroup.VTCPay)
                    {
                        result[2] = new Tuple<string, int, decimal>("VTCPay", item.Item2, item.Item3);
                    }
                    //
                    if (item.Item1 == EPaymentGroup.PayooPay)
                    {
                        result[3] = new Tuple<string, int, decimal>("Payoo", item.Item2, item.Item3);
                    }
                }

                foreach (var item in directPayment)
                {
                    if (item.Item1 == EWalletTransactionStatus.Done)
                    {
                        result[3] = new Tuple<string, int, decimal>("Trực tiếp", item.Item2, item.Item3);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("TroubleshootingCenterService.CashFlow", ex.ToString());
                return new List<Tuple<string, int, decimal>>();
            }
        }

        public async Task<List<Tuple<string, int>>> GetAnalyticTransaction(string type, int? projectId, DateTime statisticDate)
        {
            try
            {
                var rangeDay = _GetRangeDay(type);

                if (type == "day")
                {
                    rangeDay = new Tuple<DateTime, DateTime>(statisticDate, statisticDate);
                }

                var result = new List<Tuple<string, int>>();
                var booking = await _iBookingRepository.Count(x => (projectId == null || x.ProjectId == projectId) && x.Status == EBookingStatus.Start);
                result.Add(new Tuple<string, int>("Đang thuê", booking));

                var transactions = await _iTransactionRepository.CountTransacionGroupByStatus(projectId, rangeDay.Item1, rangeDay.Item2);
                result.Add(new Tuple<string, int>("Hoàn thành", 0));
                result.Add(new Tuple<string, int>("Nợ cước", 0));

                foreach (var item in transactions)
                {
                    if (item.Item1 == EBookingStatus.End)
                    {
                        result[1] = new Tuple<string, int>("Hoàn thành", item.Item2);
                    }
                    //
                    if (item.Item1 == EBookingStatus.Debt)
                    {
                        result[2] = new Tuple<string, int>("Nợ cước", item.Item2);
                    }
                }
                //
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("TroubleshootingCenterService.Transaction", ex.ToString());
                return new List<Tuple<string, int>>();
            }
        }

        public async Task<List<Tuple<string, int>>> GetAnalyticCustomer(string type, int? projectId, DateTime statisticDate)
        {
            try
            {
                var rangeDay = _GetRangeDay(type);

                if (type == "day")
                {
                    rangeDay = new Tuple<DateTime, DateTime>(statisticDate, statisticDate);
                }

                var group = await _iAccountRepository.GroupByVerifyStatus(projectId, rangeDay.Item1, rangeDay.Item2);

                var result = new List<Tuple<string, int>>
                {
                    new Tuple<string, int>("Đăng ký mới", 0),
                    new Tuple<string, int>("Chưa xác thực", 0),
                    new Tuple<string, int>("Chờ duyệt", 0),
                    new Tuple<string, int>("Đã xác thực", 0)
                };

                var total = 0;

                foreach (var item in group)
                {
                    total += item.Item2;

                    if (item.Item1 == EAccountVerifyStatus.Not)
                    {
                        result[1] = new Tuple<string, int>("Chưa xác thực", item.Item2);
                    }
                    //
                    if (item.Item1 == EAccountVerifyStatus.Waiting)
                    {
                        result[2] = new Tuple<string, int>("Chờ duyệt", item.Item2);
                    }
                    //
                    if (item.Item1 == EAccountVerifyStatus.Ok)
                    {
                        result[3] = new Tuple<string, int>("Đã xác thực", item.Item2);
                    }
                }
                result[0] = new Tuple<string, int>("Đăng ký mới", total);

                //
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("TroubleshootingCenterService.Customer", ex.ToString());
                return new List<Tuple<string, int>>();
            }
        }

        public async Task<List<Tuple<string, int>>> GetAnalyticBike(int? projectId)
        {
            try
            {
                var result = new List<Tuple<string, int>>
                {
                    new Tuple<string, int>("Sử dụng", 0),
                    new Tuple<string, int>("Sẵn sàng tại trạm", 0),
                    new Tuple<string, int>("Lưu kho", 0),
                    new Tuple<string, int>("Lỗi | Bảo dưỡng", 0)
                };

                var bikeFreeInStations = await _iBikeRepository.CountBikeFreeInStation(projectId);
                result[1] = new Tuple<string, int>("Sẵn sàng tại trạm", bikeFreeInStations);

                var bikes = await _iBikeRepository.GroupByType(projectId);

                foreach (var item in bikes)
                {
                    if (item.Item1 == EBikeStatus.Active)
                    {
                        result[0] = new Tuple<string, int>("Sử dụng", item.Item2);
                    }
                    //
                    if (item.Item1 == EBikeStatus.Warehouse)
                    {
                        result[2] = new Tuple<string, int>("Lưu kho", item.Item2);
                    }
                    //
                    if (item.Item1 == EBikeStatus.ErrorWarehouse)
                    {
                        result[3] = new Tuple<string, int>("Lỗi | Bảo dưỡng", item.Item2);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("TroubleshootingCenterService.Bike", ex.ToString());
                return new List<Tuple<string, int>>();
            }
        }

        public async Task<List<Tuple<string, int, string>>> GetAnalyticDock(int? projectId)
        {
            try
            {
                var connect = await _iDockRepository.Count(x => x.ConnectionStatus && (projectId == null || x.ProjectId == projectId));
                var disconnect = await _iDockRepository.Count(x => !x.ConnectionStatus && (projectId == null || x.ProjectId == projectId));
                var lowBattery = await _iDockRepository.Count(x => x.Battery < 10 && (projectId == null || x.ProjectId == projectId));

                return new List<Tuple<string, int, string>>
                {
                    new Tuple<string, int, string>("Đang kết nối", connect, "success"),
                    new Tuple<string, int, string>("Mất kết nối", disconnect, "danger"),
                    new Tuple<string, int, string>("Sắp hết pin", lowBattery, "warning")
                };
            }
            catch (Exception ex)
            {
                _logger.LogError("TroubleshootingCenterService.Dock", ex.ToString());
                return new List<Tuple<string, int, string>>();
            }
        }

        private Tuple<DateTime, DateTime> _GetRangeDay(string type)
        {
            var today = DateTime.Now.Date;

            switch (type)
            {
                case "day":
                    {
                        return new Tuple<DateTime, DateTime>(today, today);
                    }
                case "week":
                    {
                        int delta = DayOfWeek.Monday - today.DayOfWeek;
                        var monday = today.AddDays(delta);

                        return new Tuple<DateTime, DateTime>(monday, today);
                    }
                case "month":
                    {
                        var firstDayOfMonth = new DateTime(today.Year, today.Month, 1);

                        return new Tuple<DateTime, DateTime>(firstDayOfMonth, today);
                    }
                case "quarter":
                    {
                        int quarterNumber = (today.Month - 1) / 3 + 1;
                        var firstDayOfQuarter = new DateTime(today.Year, (quarterNumber - 1) * 3 + 1, 1);

                        return new Tuple<DateTime, DateTime>(firstDayOfQuarter, today);
                    }
                case "year":
                    {
                        var firstDayOfYear = new DateTime(today.Year, 1, 1);

                        return new Tuple<DateTime, DateTime>(firstDayOfYear, today);
                    }
                default:
                    {
                        return new Tuple<DateTime, DateTime>(today, today);
                    }
            }
        }
    }
}