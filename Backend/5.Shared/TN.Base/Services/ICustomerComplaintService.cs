﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Base;
using PT.Domain.Model;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace TN.Base.Services
{
    public interface ICustomerComplaintService : IService<CustomerComplaint>
    {
        Task<ApiResponseData<List<CustomerComplaint>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            ECustomerComplaintType? bikeReportType,
            ECustomerComplaintStatus? status,
            ECustomerComplaintPattern? pattern,
            int? accountId,
            DateTime? start,
            DateTime? end,
            string sortby,
            string sorttype
            );
        Task<ApiResponseData<CustomerComplaint>> GetById(int id);
        Task<ApiResponseData<CustomerComplaint>> Create(CustomerComplaintCommand model, ECustomerComplaintStatus? status);
        Task<ApiResponseData<CustomerComplaint>> Edit(CustomerComplaintCommand model, ECustomerComplaintStatus? status);
        Task<ApiResponseData<CustomerComplaint>> Delete(int id);
        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);

        Task<string> ExportReport(
            string key,
            ECustomerComplaintType? bikeReportType,
            ECustomerComplaintStatus? status,
            ECustomerComplaintPattern? pattern,
            int? accountId,
            DateTime? start,
            DateTime? end
        );
    }
    public class CustomerComplaintService : ICustomerComplaintService
    {
        private readonly ILogger _logger;
        private readonly BaseSettings _baseSettings;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IUserRepository _iUserRepository;
        private readonly IBikeRepository _iBikeRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly ICustomerComplaintRepository _iCustomerComplaintRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly string controllerName = "";

        public CustomerComplaintService
        (
            IOptions<BaseSettings> baseSettings,
            ILogger<CustomerComplaintService> logger,
            IWebHostEnvironment iWebHostEnvironment,

            IUserRepository iUserRepository,
            IBikeRepository iBikeRepository,
            IDockRepository iDockRepository,
            IStationRepository iStationRepository,
            IAccountRepository iAccountRepository,
            ITransactionRepository iTransactionRepository,
            ILogRepository iLogRepository,
            ICustomerComplaintRepository iCustomerComplaintRepository
        )
        {
            _logger = logger;
            _baseSettings = baseSettings.Value;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iUserRepository = iUserRepository;
            _iBikeRepository = iBikeRepository;
            _iDockRepository = iDockRepository;
            _iAccountRepository = iAccountRepository;
            _iStationRepository = iStationRepository;
            _iTransactionRepository = iTransactionRepository;
            _iCustomerComplaintRepository = iCustomerComplaintRepository;
            _iLogRepository = iLogRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<CustomerComplaint>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            ECustomerComplaintType? bikeReportType,
            ECustomerComplaintStatus? status,
            ECustomerComplaintPattern? pattern,
            int? accountId,
            DateTime? start,
            DateTime? end,
            string sortby,
            string sorttype
            )
        {
            try
            {
                var data = await _iCustomerComplaintRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (key == null || x.TransactionCode.Contains(key) || x.Description.Contains(key) || x.Note.Contains(key))
                        && (start == null || x.ComplaintDate >= start.Value.Date)
                        && (end == null || x.ComplaintDate <= end.Value.Date)
                        && (accountId == null || x.AccountId == accountId)
                        && (status == null || x.Status == status)
                        && (bikeReportType == null || x.BikeReportType == bikeReportType)
                        && (pattern == null || x.Pattern == pattern)
                    ,
                    OrderByExtention(sortby, sorttype));

                var accounts = await _iAccountRepository.Search(
                    x => data.Data.Select(x => x.AccountId).Distinct().Contains(x.Id),
                    null,
                    x => new Account
                    {
                        Id = x.Id,
                        Code = x.Code,
                        FullName = x.FullName,
                        Phone = x.Phone,
                        Status = x.Status,
                        Type = x.Type
                    });

                var transactions = await _iTransactionRepository.Search(
                    x => data.Data.Select(x => x.TransactionCode).Contains(x.TransactionCode),
                    null,
                    x => new Transaction
                    {
                        Id = x.Id,
                        TransactionCode = x.TransactionCode,
                        StationIn = x.StationIn,
                        StationOut = x.StationOut,
                        StartTime = x.StartTime,
                        EndTime = x.EndTime,
                        TotalMinutes = x.TotalMinutes,
                        TicketPrice_TicketType = x.TicketPrice_TicketType,
                        ChargeInAccount = x.ChargeInAccount,
                        ChargeInSubAccount = x.ChargeInSubAccount,
                        TripPoint = x.TripPoint
                    });

                var bikes = await _iBikeRepository.Search(
                    x => transactions.Select(x => x.BikeId).Distinct().Contains(x.Id),
                    null,
                    x => new Bike
                    {
                        Id = x.Id,
                        Plate = x.Plate
                    });

                var docks = await _iDockRepository.Search(
                    x => transactions.Select(x => x.DockId).Distinct().Contains(x.Id),
                    null,
                    x => new Dock
                    {
                        Id = x.Id,
                        IMEI = x.IMEI
                    });

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.CreatedUser).ToList(), data.Data.Select(x => x.UpdatedUser ?? 0).ToList());

                if (accountId != null && accountId > 0)
                {
                    data.OutData = await _iAccountRepository.SearchOneAsync(x => x.Id == accountId);
                }

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                    item.Transaction = transactions.FirstOrDefault(x => x.TransactionCode == item.TransactionCode);
                    item.BikeReportTypeText = item.BikeReportType.ToEnumGetDisplayName();
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerComplaintSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<CustomerComplaint>>();
            }
        }

        public async Task<ApiResponseData<CustomerComplaint>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<CustomerComplaint>() { Data = await _iCustomerComplaintRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerComplaintSevice.Get", ex.ToString());
                return new ApiResponseData<CustomerComplaint>();
            }
        }

        public async Task<ApiResponseData<CustomerComplaint>> Create(CustomerComplaintCommand model, ECustomerComplaintStatus? status)
        {
            try
            {
                var dlAdd = new CustomerComplaint
                {
                    AccountId = model.AccountId,
                    TransactionCode = model.TransactionCode,
                    BikeId = model.BikeId,
                    BikeReportType = model.BikeReportType,
                    Pattern = model.Pattern,
                    Description = model.Description,
                    Note = model.Note,
                    ComplaintDate = model.ComplaintDate,
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    Files = model.Files,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };

                if (status != null)
                {
                    dlAdd.Status = status ?? ECustomerComplaintStatus.NoProcess;
                }

                await _iCustomerComplaintRepository.AddAsync(dlAdd);
                await _iCustomerComplaintRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới khiếu nại khách hàng '{dlAdd.Description}' trang thái {dlAdd.Status}", LogType.Insert);

                return new ApiResponseData<CustomerComplaint>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerComplaintSevice.Create", ex.ToString());
                return new ApiResponseData<CustomerComplaint>();
            }
        }

        public async Task<ApiResponseData<CustomerComplaint>> Edit(CustomerComplaintCommand model, ECustomerComplaintStatus? status)
        {
            try
            {
                var dl = await _iCustomerComplaintRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<CustomerComplaint>() { Data = null, Status = 0 };
                }

                dl.AccountId = model.AccountId;
                dl.TransactionCode = model.TransactionCode;
                dl.BikeId = model.BikeId;
                dl.BikeReportType = model.BikeReportType;
                dl.Description = model.Description;
                dl.Note = model.Note;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.ComplaintDate = model.ComplaintDate;
                dl.Files = model.Files;
                dl.StartDate = model.StartDate;
                dl.EndDate = model.EndDate;
                dl.Pattern = model.Pattern;

                if (status != null)
                {
                    dl.Status = status ?? ECustomerComplaintStatus.NoProcess;
                }

                _iCustomerComplaintRepository.Update(dl);
                await _iCustomerComplaintRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật khiếu nại khách hàng '{dl.Description}' trạng thái {dl.Status}", LogType.Update);

                return new ApiResponseData<CustomerComplaint>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerComplaintSevice.Edit", ex.ToString());
                return new ApiResponseData<CustomerComplaint>();
            }
        }

        public async Task<ApiResponseData<CustomerComplaint>> Delete(int id)
        {
            try
            {
                var dl = await _iCustomerComplaintRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<CustomerComplaint>() { Data = null, Status = 0 };
                }

                if (dl.Status == ECustomerComplaintStatus.Processed)
                {
                    return new ApiResponseData<CustomerComplaint>() { Data = dl, Status = 2 };
                }

                _iCustomerComplaintRepository.Delete(dl);
                await _iCustomerComplaintRepository.Commit();

                await AddLog(dl.Id, $"Xóa khiếu nại khách hàng '{dl.Description}' trang thái {dl.Status}", LogType.Update);

                return new ApiResponseData<CustomerComplaint>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerComplaintSevice.Delete", ex.ToString());
                return new ApiResponseData<CustomerComplaint>();
            }
        }

        private Func<IQueryable<CustomerComplaint>, IOrderedQueryable<CustomerComplaint>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "accountId" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.AccountId)),
                "transactionCode" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.TransactionCode)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.TransactionCode)),
                "bikeId" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.BikeId)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.BikeId)),
                "bikeReportType" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.BikeReportType)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.BikeReportType)),
                "description" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.Description)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.Description)),
                "note" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.Note)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.Note)),
                "status" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.Status)),
                "createdDate" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                "updatedUser" => sorttype == "asc" ? EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
                _ => EntityExtention<CustomerComplaint>.OrderBy(m => m.OrderByDescending(x => x.ComplaintDate)),
            };
        }

        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Code.Contains(key) || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerComplaintSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }

        #region [Upload file]
        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/CustomerComplaint/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfffffff}_{Path.GetFileName(file.FileName)}";
                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);
                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                    System.Threading.Thread.Sleep(100);
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }
        #endregion

        #region [ExportReport]
        public async Task<string> ExportReport(
            string key,
            ECustomerComplaintType? bikeReportType,
            ECustomerComplaintStatus? status,
            ECustomerComplaintPattern? pattern,
            int? accountId,
            DateTime? start,
            DateTime? end
        )
        {
            var customerComplaints = await _iCustomerComplaintRepository.Search(
                x => (key == null || x.TransactionCode.Contains(key) || x.Description.Contains(key) || x.Note.Contains(key))
                        && (start == null || x.ComplaintDate >= start.Value.Date)
                        && (end == null || x.ComplaintDate <= end.Value.Date)
                        && (accountId == null || x.AccountId == accountId)
                        && (status == null || x.Status == status)
                        && (bikeReportType == null || x.BikeReportType == bikeReportType)
                        && (pattern == null || x.Pattern == pattern)
                );

            var accounts = await _iAccountRepository.Search(
                x => customerComplaints.Select(x => x.AccountId).Distinct().Contains(x.Id),
                null,
                x => new Account
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Code = x.Code
                });

            var transactions = await _iTransactionRepository.Search(
                    x => customerComplaints.Select(x => x.TransactionCode).Contains(x.TransactionCode),
                    null,
                    x => new Transaction
                    {
                        Id = x.Id,
                        TransactionCode = x.TransactionCode,
                        StationIn = x.StationIn,
                        StationOut = x.StationOut,
                        StartTime = x.StartTime,
                        EndTime = x.EndTime,
                        TotalMinutes = x.TotalMinutes,
                        TicketPrice_TicketType = x.TicketPrice_TicketType,
                        ChargeInAccount = x.ChargeInAccount,
                        ChargeInSubAccount = x.ChargeInSubAccount,
                        TripPoint = x.TripPoint
                    });

            var stations = await _iStationRepository.Search(
                x => transactions.Select(x => x.StationIn).Distinct().Contains(x.Id)
                    || transactions.Select(x => x.StationOut).Distinct().Contains(x.Id),
                null,
                x => new Station
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var bikes = await _iBikeRepository.Search(
                x => transactions.Select(x => x.BikeId).Distinct().Contains(x.Id),
                null,
                x => new Bike
                {
                    Id = x.Id,
                    Plate = x.Plate
                });

            var docks = await _iDockRepository.Search(
                x => transactions.Select(x => x.DockId).Distinct().Contains(x.Id),
                null,
                x => new Dock
                {
                    Id = x.Id,
                    IMEI = x.IMEI
                });

            foreach (var item in customerComplaints)
            {
                item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                item.Transaction = transactions.FirstOrDefault(x => x.TransactionCode == item.TransactionCode);

                if (item.Transaction != null)
                {
                    item.Transaction.ObjStationIN = stations.FirstOrDefault(x => x.Id == item.Transaction.StationIn);
                    item.Transaction.ObjStationOut = stations.FirstOrDefault(x => x.Id == item.Transaction.StationOut);
                    item.Transaction.Bike = bikes.FirstOrDefault(x => x.Id == item.Transaction.BikeId);
                    item.Transaction.Dock = docks.FirstOrDefault(x => x.Id == item.Transaction.DockId);
                }
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC GQKN");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelCustomerComplaint(workSheet, customerComplaints, start, end);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelCustomerComplaint(ExcelWorksheet worksheet, List<CustomerComplaint> items, DateTime? start, DateTime? end)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 25}, {3 , 15}, {4 , 15}, {5 , 30},
                {6 , 15}, {7 , 20}, {8 , 20}, {9 , 20}, {10, 15},
                {11, 15}, {12, 10}, {13, 15}, {14, 15}, {15, 15},
                {16, 15}, {17, 15}, {18, 15}, {19, 40}, {20, 15},
                {21, 40}, {22, 15}, {23, 25}, {24, 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 3, 4, 5, 6, 7, 10, 11, 17, 20, 22 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 12, 14, 15, 16 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:F2"].Merge = true;
            worksheet.Cells["B2:F2"].Style.Font.Bold = true;
            worksheet.Cells["B2:F2"].Value = $"BÁO CÁO KHIẾU NẠI VÀ GIẢI QUYẾT KHIẾU NẠI";

            var fromText = start != null ? start?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = end != null ? end?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:F3"].Merge = true;
            worksheet.Cells["B3:F3"].Style.Font.Italic = true;
            worksheet.Cells["B3:F3"].Value = $"Mở bán từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:X5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:X5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:X5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:X5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên KH";
            worksheet.Cells["C5"].Value = "SĐT";
            worksheet.Cells["D5"].Value = "Mã KH";
            worksheet.Cells["E5"].Value = "Mã chuyến đi";
            worksheet.Cells["F5"].Value = "Biển số xe";
            worksheet.Cells["G5"].Value = "IMEI khóa";
            worksheet.Cells["H5"].Value = "Từ trạm";
            worksheet.Cells["I5"].Value = "Đến trạm";
            worksheet.Cells["J5"].Value = "Thời gian bắt đầu";
            worksheet.Cells["K5"].Value = "Thời gian kết thúc";
            worksheet.Cells["L5"].Value = "Tổng số phút đi";
            worksheet.Cells["M5"].Value = "Loại vé";
            worksheet.Cells["N5"].Value = "Phí trừ TK gốc";
            worksheet.Cells["O5"].Value = "Phí trừ TK KM";
            worksheet.Cells["P5"].Value = "Tích điểm";
            worksheet.Cells["Q5"].Value = "Thời gian khiếu nại";
            worksheet.Cells["R5"].Value = "Hình thức khiếu nại";
            worksheet.Cells["S5"].Value = "Loại lỗi";
            worksheet.Cells["T5"].Value = "Thời gian tiếp nhận KN";
            worksheet.Cells["U5"].Value = "Mô tả lỗi";
            worksheet.Cells["V5"].Value = "Thời gian hoàn thành";
            worksheet.Cells["W5"].Value = "Mô tả xử lý KN";
            worksheet.Cells["X5"].Value = "KPI GQ KN";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Account?.FullName;
                worksheet.Cells[rs, 3].Value = item.Account?.Phone;
                worksheet.Cells[rs, 4].Value = item.Account?.Code;
                worksheet.Cells[rs, 5].Value = item.TransactionCode;
                worksheet.Cells[rs, 6].Value = item.Transaction?.Bike?.Plate;
                worksheet.Cells[rs, 7].Value = item.Transaction?.Dock?.IMEI;
                worksheet.Cells[rs, 8].Value = item.Transaction?.ObjStationIN?.Name;
                worksheet.Cells[rs, 9].Value = item.Transaction?.ObjStationOut?.Name;
                worksheet.Cells[rs, 10].Value = item.Transaction?.StartTime;
                worksheet.Cells[rs, 11].Value = item.Transaction?.EndTime;
                worksheet.Cells[rs, 12].Value = item.Transaction?.TotalMinutes;
                worksheet.Cells[rs, 13].Value = item.Transaction?.TicketPrice_TicketType.ToEnumGetDisplayName();
                worksheet.Cells[rs, 14].Value = item.Transaction?.ChargeInAccount;
                worksheet.Cells[rs, 15].Value = item.Transaction?.ChargeInSubAccount;
                worksheet.Cells[rs, 16].Value = item.Transaction?.TripPoint;
                worksheet.Cells[rs, 17].Value = item.ComplaintDate.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 18].Value = item.Pattern.ToEnumGetDisplayName();
                worksheet.Cells[rs, 19].Value = item.BikeReportType.ToEnumGetDisplayName();
                worksheet.Cells[rs, 20].Value = item.StartDate != DateTime.MinValue ? item.StartDate.ToString("dd/MM/yyyy") : "";
                worksheet.Cells[rs, 21].Value = item.Description;
                worksheet.Cells[rs, 22].Value = item.EndDate != DateTime.MinValue ? item.EndDate.ToString("dd/MM/yyyy") : "";
                worksheet.Cells[rs, 23].Value = item.Note;

                worksheet.Cells[rs, 24].Value = item.BikeReportType switch
                {
                    ECustomerComplaintType.KNHT01 => "24h",
                    ECustomerComplaintType.KN01 => "2h",
                    ECustomerComplaintType.KN02 => "1h",
                    ECustomerComplaintType.KN03 => "2h",
                    ECustomerComplaintType.KN04 => "2h",
                    _ => "Chưa xác định",
                };
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:X{rs - 1}");
            worksheet.Cells[$"A6:X{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string, string> InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}