﻿using Microsoft.Extensions.Logging;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using PT.Domain.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IDashboardService : IService<Station>
    {
        Task<ApiResponseData<List<Station>>> Stations(int projectId);
        Task<ApiResponseData<List<Booking>>> Bookings(int projectId);
        ApiResponseData<List<GPSData>> GPSByIMEI(string imei, DateTime start, DateTime end);
        Task<ApiResponseData<List<Bike>>> Bikes();
        ApiResponseData<List<EventSystemMongo>> EventSystemSearchPageAsync(
                int pageIndex,
                int pageSize,
                string key,
                bool? isFlag,
                bool? isProcessed,
                EEventSystemType? type,
                EEventSystemGroup? group,
                EEventSystemWarningType? warningType,
                DateTime? start,
                DateTime? end,
                int? projectId,
                string transactionCode, string plate);

        Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            string @object,
            int? systemUserId,
            DateTime? start,
            DateTime? end
        );

        ApiResponseData<EventSystemMongo> EventSystemChangeIsFlag(long id, bool status);
       
        Task<ApiResponseData<DataInfo>> DataInfoAccountOnline();
        Task<ApiResponseData<List<Bike>>> BikeNotBookings(int projectId);
        Task<ApiResponseData<List<RFIDBooking>>> RFIDBookings(int projectId);
    }

    public class DashboardService : IDashboardService
    {
        private readonly ILogger _logger;
        private readonly IStationRepository _iStationRepository;
        private readonly IBikeRepository _iBikeRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly IGPSDataRepository _iGPSDataRepository;
        private readonly LogSettings _logSettings;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IRoleControllerRepository _iRoleControllerRepository;
        private readonly IBookingFailRepository _iBookingFailRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IDataInfoRepository _iDataInfoRepository;
        private readonly IRFIDBookingRepository _iRFIDBookingRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        public DashboardService
        (
            ILogger<StationService> logger,
            IStationRepository iStationRepository,
            IBikeRepository iBikeRepository,
            IDockRepository iDockRepository,
            IBookingRepository iBookingRepository,
            ITransactionRepository iTransactionRepository,
            IGPSDataRepository iGPSDataRepository,
            IOptions<LogSettings> logSettings,
            IAccountRepository iAccountRepository,
            ILogRepository iLogRepository,
            IUserRepository iUserRepository,
            IRoleControllerRepository iRoleControllerRepository,
            IBookingFailRepository iBookingFailRepository,
            IWalletRepository iWalletRepository,
            IDataInfoRepository iDataInfoRepository,
            IRFIDBookingRepository iRFIDBookingRepository,
            IProjectAccountRepository iProjectAccountRepository,
            ICustomerGroupRepository iCustomerGroupRepository
        )
        {
            _logger = logger;
            _iStationRepository = iStationRepository;
            _iBikeRepository = iBikeRepository;
            _iDockRepository = iDockRepository;
            _iBookingRepository = iBookingRepository;
            _iTransactionRepository = iTransactionRepository;
            _iGPSDataRepository = iGPSDataRepository;
            _logSettings = logSettings.Value;
            _iAccountRepository = iAccountRepository;
            _iLogRepository = iLogRepository;
            _iUserRepository = iUserRepository;
            _iRoleControllerRepository = iRoleControllerRepository;
            _iBookingFailRepository = iBookingFailRepository;
            _iWalletRepository = iWalletRepository;
            _iDataInfoRepository = iDataInfoRepository;
            _iRFIDBookingRepository =  iRFIDBookingRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
        }

        public async Task<ApiResponseData<List<Station>>> Stations(int projectId)
        {
            try
            {
                var listData = await _iStationRepository.Search(x => x.Status == ESationStatus.Active && (x.ProjectId == projectId || projectId == 0));
                listData = await _iBikeRepository.InitTotalBikeInStationAsync(listData, 10);
                return new ApiResponseData<List<Station>> { Data = listData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DashboardService.Stations", ex.ToString());
                return new ApiResponseData<List<Station>>();
            }
        }

        public  ApiResponseData<List<GPSData>> GPSByIMEI(string imei, DateTime start, DateTime end)
        {
            var data =  _iGPSDataRepository.GetByIMEI(_logSettings, imei, start, end).Where(x => x.Lat > 0 && x.Long > 0).ToList();
            return new ApiResponseData<List<GPSData>> { Data = data.OrderBy(x=>x.CreateTimeTicks).ToList(), Status = 1 };
        }

        public async Task<ApiResponseData<List<Bike>>> Bikes()
        {
            try
            {
                var listData = await _iBikeRepository.BikesInTransaction();
                return new ApiResponseData<List<Bike>> { Data = listData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DashboardService.Bikes", ex.ToString());
                return new ApiResponseData<List<Bike>>();
            }
        }


        public async Task<ApiResponseData<DataInfo>> DataInfoAccountOnline()
        {
            try
            {
                var data = await _iDataInfoRepository.SearchOneAsync(x=>x.Type == EDataInfoType.SumAccountLive);
                return new ApiResponseData<DataInfo> { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DashboardService.DataInfoAccountOnline", ex.ToString());
                return new ApiResponseData<DataInfo>();
            }
        }

        public async Task<ApiResponseData<List<Bike>>> BikeNotBookings(int projectId)
        {
            var docks = await _iDockRepository.SearchNotBooking(projectId);
            var bikes = await _iBikeRepository.SearchNotBooking(projectId);

            var bikeNewList = new List<Bike>();
            foreach(var bike in bikes)
            {
                bike.Dock = docks.FirstOrDefault(x => x.Id == bike.DockId);
                bikeNewList.Add(bike);
            }
            // Dock không gắn với xe nào
            var listDockOny = docks.Where(x => !bikes.Any(m => m.DockId == x.Id));
            foreach(var item in listDockOny)
            {
                bikeNewList.Add(new Bike
                {
                    Id = 0,
                    Plate = "Chỉ khóa",
                    Dock = item,
                    Lat = item.Lat,
                    Long = item.Long
                });
            }
            return new ApiResponseData<List<Bike>>() { Data = bikeNewList, Status = 1 };
        }

        public async Task<ApiResponseData<List<Booking>>> Bookings(int projectId)
        {
            try
            {
                var listData = await _iBookingRepository.Search(x => x.Status == EBookingStatus.Start && x.ProjectId == projectId, x=> x.OrderBy(m=>m.StartTime));
                var bikes = await _iBikeRepository.Search(x => listData.Select(m=>m.BikeId).Contains(x.Id) && x.Status == Bike.EBikeStatus.Active);
                var docks = await _iDockRepository.Search(x => bikes.Select(x=>x.DockId).Contains(x.Id));
                var stations = await _iStationRepository.Search(x=>  listData.Select(m => m.StationIn).Contains(x.Id));
                var accounts = await _iAccountRepository.Search(x => listData.Select(m => m.AccountId).Contains(x.Id));
                var wallets = await _iWalletRepository.Search(x => listData.Select(y => y.AccountId).Contains(x.AccountId));

                var customerGroups = await _iCustomerGroupRepository.Search(x => listData.Select(y => y.CustomerGroupId).Contains(x.Id)&& x.ProjectId == projectId
                        ,
                        null,
                        x => new CustomerGroup
                        {
                            Id = x.Id,
                            Name = x.Name
                        }
                    );

                foreach (var item in bikes)
                {
                    item.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                }

                var accountIds = accounts.Select(x => x.Id).Distinct().ToList();
                var vis = await _iWalletRepository.Search(x => accountIds.Contains(x.AccountId)); 

                foreach (var item in listData)
                {
                    item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                    item.StationInObject = stations.FirstOrDefault(x => x.Id == item.StationIn);
                    item.Wallet = wallets.FirstOrDefault(x => x.AccountId == item.AccountId);

                    var dlAccount = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                    if(dlAccount != null)
                    {
                        item.Account = new Account(dlAccount);
                        item.Account.CustomerGroups = customerGroups.Where(x => x.Id == item.CustomerGroupId);
                    }    

                    var ticketPrice = new TicketPrice
                    {
                        AllowChargeInSubAccount = item.TicketPrice_AllowChargeInSubAccount,
                        BlockPerMinute = item.TicketPrice_BlockPerMinute,
                        BlockPerViolation = item.TicketPrice_BlockPerViolation, 
                        BlockViolationValue = item.TicketPrice_BlockViolationValue,
                        CustomerGroupId = item.CustomerGroupId,
                        LimitMinutes = item.TicketPrice_LimitMinutes,
                        ProjectId = item.ProjectId,
                        TicketValue = item.TicketPrice_TicketValue,
                        TicketType = item.TicketPrice_TicketType,
                        IsDefault = item.TicketPrice_IsDefault
                    };

                    item.TicketPrice_Note = _iTransactionRepository.GetTicketPriceString(ticketPrice);
                    item.TotalMinutes = Function.TotalMilutes(item.StartTime, DateTime.Now);
                }

                // Lấy ra tài khoản là vé nhóm
                var timeNow = DateTime.Now;
                int totalMinutes = 0;
                decimal totalPrice = 0;

                foreach (var account in accounts)
                {
                     totalMinutes = 0;
                     totalPrice = 0;

                    var wallet = wallets.FirstOrDefault(x => x.AccountId == account.Id);
                    if (wallet == null)
                    {
                        continue;
                    }

                    var bookingByAccount = listData.Where(x => x.AccountId == account.Id).OrderByDescending(x=>x.StartTime).ToList();

                    int prepaid_MinutesSpent = bookingByAccount.FirstOrDefault()?.Prepaid_MinutesSpent ?? 0;

                    foreach (var booking in bookingByAccount)
                    {
                        var ticketPrice = new TicketPrice
                        {
                            AllowChargeInSubAccount = booking.TicketPrice_AllowChargeInSubAccount,
                            BlockPerMinute = booking.TicketPrice_BlockPerMinute,
                            BlockPerViolation = booking.TicketPrice_BlockPerViolation,
                            BlockViolationValue = booking.TicketPrice_BlockViolationValue,
                            CustomerGroupId = booking.CustomerGroupId,
                            LimitMinutes = booking.TicketPrice_LimitMinutes,
                            ProjectId = booking.ProjectId,
                            TicketValue = booking.TicketPrice_TicketValue,
                            TicketType = booking.TicketPrice_TicketType,
                            IsDefault = booking.TicketPrice_IsDefault
                        };

                        booking.TotalMinutes = Function.TotalMilutes(booking.StartTime, timeNow);

                        booking.TotalPrice = _iTransactionRepository.ToTotalPrice(
                            booking.TicketPrice_TicketType,
                            booking.StartTime,
                            timeNow,
                            booking.TicketPrice_TicketValue,
                            booking.TicketPrice_LimitMinutes,
                            booking.TicketPrice_BlockPerMinute,
                            booking.TicketPrice_BlockPerViolation,
                            booking.TicketPrice_BlockViolationValue,
                            booking.Prepaid_EndTime ?? DateTime.Now,
                            prepaid_MinutesSpent,
                            new DiscountCode { Percent = booking.DiscountCode_Percent ?? 0, Point = booking.DiscountCode_Point ?? 0, Type = booking.DiscountCode_Type ?? EDiscountCodeType.Point }
                            )?.Price ?? 0;

                        totalMinutes += booking.TotalMinutes;
                        totalPrice += booking.TotalPrice;
                        prepaid_MinutesSpent += totalMinutes;
                    }

                    bookingByAccount.ForEach(x =>
                    {
                        if (totalPrice > (wallet.Balance + wallet.SubBalance))
                        {
                            x.DebtTotal = totalPrice - (wallet.Balance + wallet.SubBalance);
                        }
                        x.TotalPriceUT = totalPrice;
                        x.BikeCount = bookingByAccount.Count();
                        if(x.TicketPrice_TicketType != ETicketType.Block)
                        {
                            x.TotalPrice = 0;
                        }    
                    });
                }

                return new ApiResponseData<List<Booking>> { Data = listData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DashboardService.Bikes", ex.ToString());
                return new ApiResponseData<List<Booking>>();
            }
        }

        public async Task<ApiResponseData<List<RFIDBooking>>> RFIDBookings(int projectId)
        {
            try
            {
                var listData = await _iRFIDBookingRepository.Search(x => x.Status == ERFIDCoordinatorStatus.Start && x.ProjectId == projectId, x=>x.OrderBy(m=>m.StartTime));
                var bikes = await _iBikeRepository.Search(x => listData.Select(m => m.BikeId).Contains(x.Id) && x.Status == Bike.EBikeStatus.Active);
                var docks = await _iDockRepository.Search(x => bikes.Select(x => x.DockId).Contains(x.Id));
                var stations = await _iStationRepository.Search(x => listData.Select(m => m.StationIn).Distinct().Contains(x.Id));
                var accounts = await _iAccountRepository.Search(x => listData.Select(m => m.AccountId).Distinct().Contains(x.Id));
                var users = await _iUserRepository.Search(x => listData.Select(m => m.UserId).Distinct().Contains(x.Id));

                foreach (var item in bikes)
                {
                    item.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                }

                var accountIds = accounts.Select(x => x.Id).Distinct().ToList();
                var vis = await _iWalletRepository.Search(x => accountIds.Contains(x.AccountId));

                foreach (var item in listData)
                {
                    item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                    item.StationInObject = stations.FirstOrDefault(x => x.Id == item.StationIn);
                    item.TotalMinutes = Utility.Function.TotalMilutes(item.StartTime, DateTime.Now);
                    var dlAccount = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                    if (dlAccount != null)
                    {
                        item.Account = new Account(dlAccount);
                    }

                    var dlUser = users.FirstOrDefault(x => x.Id == item.UserId);
                    if (dlUser != null)
                    {
                        item.User = new ApplicationUser { Id = dlUser.Id, UserName = dlUser.UserName, FullName = dlUser.FullName, PhoneNumber = dlUser.PhoneNumber };
                    }
                }
                return new ApiResponseData<List<RFIDBooking>> { Data = listData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DashboardService.RFIDBookings", ex.ToString());
                return new ApiResponseData<List<RFIDBooking>>();
            }
        }
        public ApiResponseData<List<EventSystemMongo>> EventSystemSearchPageAsync(
            int pageIndex, 
            int pageSize, 
            string key, 
            bool? isFlag,
            bool? isProcessed,
            EEventSystemType? type,
            EEventSystemGroup? group,
            EEventSystemWarningType? warningType,
            DateTime? start,
            DateTime? end,
            int? projectId,
            string transactionCode, string plate)
        {
            try
            {
                var data =  _iGPSDataRepository.EventSystemSearchPageAsync(
                    _logSettings, 
                    pageIndex, 
                    pageSize, 
                    key,
                    isFlag, isProcessed, type, group, warningType, start, end, projectId, transactionCode, plate);
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("DashboardService.EventSystemSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<EventSystemMongo>>();
            }
        }

        public  ApiResponseData<EventSystemMongo> EventSystemChangeIsFlag(long id, bool status)
        {
            try
            {
                string dbName = _logSettings.MongoDataBaseLog.Replace("{TimeDB}", $"{DateTime.Now:yyyyMM}");
                var dbClient = new MongoClient(_logSettings.MongoClient);
                var db = dbClient.GetDatabase(dbName);
                db.GetCollection<EventSystemMongo>("EventSystem").UpdateOne(Builders<EventSystemMongo>.Filter.Eq("Id", id), Builders<EventSystemMongo>.Update.Set("IsFlag", status));
                return new ApiResponseData<EventSystemMongo>() { Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("EventSystemSevice.EventSystemChangeIsFlag", ex.ToString());
                return new ApiResponseData<EventSystemMongo>();
            }
        }


        public async Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            string @object,
            int? systemUserId,
            DateTime? start,
            DateTime? end
        )
        {
            try
            {
                var data = await _iLogRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (key == null || x.Action.Contains(key))
                    && (@object == null || x.Object == @object)
                    && (systemUserId == null || x.SystemUserId == systemUserId)
                    && (start == null || x.CreatedDate.Date >= start)
                    && (end == null || x.CreatedDate.Date <= end),
                    x => x.OrderByDescending(m => m.CreatedDate));

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.SystemUserId).Distinct().ToList());
                var controllerIds = data.Data.Select(x => x.Object).Distinct().ToList();
                var controllers = await _iRoleControllerRepository.SearchTop(99999, x => controllerIds.Contains(x.Id));

                foreach (var item in data.Data)
                {
                    item.SystemUserObject = users.FirstOrDefault(x => x.Id == item.SystemUserId);
                    item.RoleController = controllers.FirstOrDefault(x => x.Id == item.Object);
                }    

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("EventSystemSevice.LogSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Domain.Model.Log>>();
            }
        }
    }
}
