﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IContentPageService : IService<ContentPage>
    {
        Task<ApiResponseData<List<ContentPage>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<ContentPage>> GetById(int id);
        Task<ApiResponseData<ContentPage>> Create(ContentPageCommand model);
        Task<ApiResponseData<ContentPage>> Edit(ContentPageCommand model);
        Task<ApiResponseData<ContentPage>> Delete(int id);
        Task<object> UploadImage(HttpRequest request);
    }
    public class ContentPageService : IContentPageService
    {
        private readonly ILogger _logger;
        private readonly BaseSettings _baseSettings;
        private readonly IUserRepository _iUserRepository;
        private readonly IContentPageRepository _iContentPageRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly string controllerName = "";
        public ContentPageService
        (
            ILogger<ContentPageService> logger,
            IOptions<BaseSettings> baseSettings,
            IContentPageRepository iContentPageRepository,
            IUserRepository iUserRepository,
            ILogRepository iLogRepository
        )
        {
            _logger = logger;
            _baseSettings = baseSettings.Value;
            _iUserRepository = iUserRepository;
            _iContentPageRepository = iContentPageRepository;
            _iLogRepository = iLogRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<ContentPage>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                var data = await _iContentPageRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => (key == null || x.Name.Contains(key)),
                    OrderByExtention(sortby, sorttype),
                    x => new ContentPage
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Content = x.Content,
                        Type = x.Type,
                        LanguageId = x.LanguageId,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser
                    });

                // Binding User info
                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ContentPageSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ContentPage>>();
            }
        }

        public async Task<ApiResponseData<ContentPage>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<ContentPage>() { Data = await _iContentPageRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ContentPageSevice.Get", ex.ToString());
                return new ApiResponseData<ContentPage>();
            }
        }

        public async Task<ApiResponseData<ContentPage>> Create(ContentPageCommand model)
        {
            try
            {
                var dl = await _iContentPageRepository.SearchOneAsync(x => x.Type == (EContentPageType)model.Type && x.LanguageId == model.LanguageId);
                if (dl != null)
                {
                    return new ApiResponseData<ContentPage>() { Data = null, Status = 2 };
                }

                var dlAdd = new ContentPage
                {
                    Id = model.Id,
                    Name = model.Name,
                    Content = model.Content,
                    Type = (EContentPageType)model.Type,
                    LanguageId = model.LanguageId,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };
                await _iContentPageRepository.AddAsync(dlAdd);
                await _iContentPageRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới trang nội dung '{dlAdd.Name}'", LogType.Insert);

                return new ApiResponseData<ContentPage>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ContentPageSevice.Create", ex.ToString());
                return new ApiResponseData<ContentPage>();
            }
        }

        public async Task<ApiResponseData<ContentPage>> Edit(ContentPageCommand model)
        {
            try
            {
                var dl = await _iContentPageRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ContentPage>() { Data = null, Status = 0 };
                }

                var dl2 = await _iContentPageRepository.SearchOneAsync(x => x.Type == (EContentPageType)model.Type && x.LanguageId == model.LanguageId && x.Id != model.Id);
                if (dl2 != null)
                {
                    return new ApiResponseData<ContentPage>() { Data = null, Status = 2 };
                }

                dl.Id = model.Id;
                dl.Name = model.Name;
                dl.Content = model.Content;
                dl.Type = (EContentPageType)model.Type;
                dl.LanguageId = model.LanguageId;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iContentPageRepository.Update(dl);
                await _iContentPageRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật trang nội dung '{dl.Name}'", LogType.Update);

                return new ApiResponseData<ContentPage>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ContentPageSevice.Edit", ex.ToString());
                return new ApiResponseData<ContentPage>();
            }
        }
        public async Task<ApiResponseData<ContentPage>> Delete(int id)
        {
            try
            {
                var dl = await _iContentPageRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ContentPage>() { Data = null, Status = 0 };
                }

                _iContentPageRepository.Delete(dl);
                await _iContentPageRepository.Commit();

                await AddLog(dl.Id, $"Xóa trang nội dung '{dl.Name}'", LogType.Delete);

                return new ApiResponseData<ContentPage>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ContentPageSevice.Delete", ex.ToString());
                return new ApiResponseData<ContentPage>();
            }
        }

        private static Func<IQueryable<ContentPage>, IOrderedQueryable<ContentPage>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<ContentPage>, IOrderedQueryable<ContentPage>> functionOrder = null;
            functionOrder = sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "name" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "content" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.Content)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.Content)),
                "type" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.Type)),
                "languageId" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.LanguageId)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.LanguageId)),
                "createdDate" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                "updatedUser" => sorttype == "asc" ? EntityExtention<ContentPage>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
                _ => EntityExtention<ContentPage>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
            return functionOrder;
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        #region [Upload file]
        public async Task<object> UploadImage(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');

                string pathServer = $"/Data/WalletTransaction/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";
                var host = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var file = request.Form.Files[0];

                if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                {
                    return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                }
                else if (_baseSettings.ImagesMaxSize < file.Length)
                {
                    return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                }

                var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfff}_{Path.GetFileName(file.FileName)}";

                var pathFile = Path.Combine(path, newFilename);

                using (var stream = new FileStream(pathFile, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                //
                return new { url = $"{host}{pathServer}/{newFilename}" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ContentPageSevice.UploadImage", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }
        #endregion
    }
}