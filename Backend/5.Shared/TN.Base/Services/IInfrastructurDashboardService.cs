//using Microsoft.AspNetCore.Hosting;
//using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Options;
//using TN.Base.Command;
//using TN.Domain.Model;
//using TN.Infrastructure.Interfaces;
//using TN.Shared;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using PT.Base;
//using TN.Domain.Model.Common;
//using TN.Utility;
//using PT.Domain.Model;

//namespace TN.Base.Services
//{
//    public interface IInfrastructurDashboardService : IService<Station>
//    {
//        Task<ApiResponseData<List<StationDTO>>> StationSearch(string key, int? tenantId);
//        Task<ApiResponseData<List<DockDTO>>> DockByStation(int stationId);
//    }

//    public class InfrastructurDashboardService : IInfrastructurDashboardService
//    {
//        private readonly ILogger _logger;
//        private readonly IStationRepository _iStationRepository;
//        private readonly IProjectRepository _iTenantRepository;
//        private readonly IDockRepository _iDockRepository;
//        private readonly IBikeRepository _iBikeRepository;

//        public InfrastructurDashboardService
//        (
//            ILogger<StationService> logger,
//            IStationRepository iStationRepository,
//            IProjectRepository iTenantRepository,
//            IDockRepository iDockRepository,
//            IBikeRepository iBikeRepository
//        )
//        {
//            _logger = logger;
//            _iStationRepository = iStationRepository;
//            _iTenantRepository = iTenantRepository;
//            _iDockRepository = iDockRepository;
//            _iBikeRepository = iBikeRepository;
//        }

//        public async Task<ApiResponseData<List<StationDTO>>> StationSearch(string key, int? tenantId)
//        {
//            var data = await _iStationRepository.Search(x => x.Status == ESationStatus.Active && (x.InvestorId == tenantId || tenantId == null) && (x.Name.Contains(key) || key == null));
//            var dtoData = data.Select(x => new StationDTO(x)).ToList();
//            var tenaIds = data.Select(x => x.InvestorId).ToList();
//            var stationIds = data.Select(x => x.Id).ToList();
//            var tenants = await _iTenantRepository.Search();
//            var dockInfo = await _iDockRepository.DockInfoByStation(data);
//            var docks = await _iDockRepository.Search(x => x.Status != EDockStatus.Inactive && stationIds.Contains(x.StationId));
//            var bikes = await _iBikeRepository.Search(x => x.Status != Bike.EBikeStatus.Inactive  && tenaIds.Contains(x.ProjectId));

//            foreach (var item in dtoData)
//            {
//                var totaDoc = dockInfo.FirstOrDefault(x => x.StationId == item.Id);
//                //item.Tenant = tenants.FirstOrDefault(x => x.Id == item.TenantId);
//                item.TotalDock = totaDoc?.TotalDock ?? 0;
//                item.TotalDockFree = totaDoc?.TotalDockFree ?? 0;
//                var stationDocs = docks.Where(x => x.StationId == item.Id).ToList();
//                item.Docks = new List<DockDTO>();
//                foreach (var stationDoc in stationDocs)
//                {
//                    //item.Docks.Add(new DockDTO(stationDoc, tenants.FirstOrDefault(x => x.Id == item.TenantId), new BikeDTO(bikes.FirstOrDefault(x => x.Id == stationDoc.BikeId))));
//                }    
//            }
//            return new ApiResponseData<List<StationDTO>> { Data = dtoData, Count = dtoData.Count };
//        }

//        //public async Task<ApiResponseData<List<DockDTO>>> DockByStation(int stationId)
//        //{
//        //    var tenants = await _iTenantRepository.Search();
//        //    return new ApiResponseData<List<DockDTO>> { Data = await DockByStation(stationId, new List<Investor>()) };
//        //}

//        //private async Task<List<DockDTO>> DockByStation(int stationId, List<Investor> tenants)
//        //{
//        //    var newList = new List<DockDTO>();
//        //    var listDoc = await _iDockRepository.Search(x => x.StationId == stationId);
           
//        //    var idbikeIds = listDoc.Select(x => x.BikeId).ToList();
//        //    var bikes = await _iBikeRepository.Search(x => idbikeIds.Contains(x.Id));

//        //    foreach (var item in listDoc)
//        //    {
//        //        var tenant = tenants.FirstOrDefault(x => x.Id == item.InvestorId);
//        //        var bike = new BikeDTO(bikes.FirstOrDefault(x => x.Id == item.BikeId));
//        //        newList.Add(new DockDTO(item, tenant, bike));
//        //    }
//        //    return newList;
//        //}
//    }
//}
