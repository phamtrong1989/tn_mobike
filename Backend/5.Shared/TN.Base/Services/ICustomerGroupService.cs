using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PT.Base.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace PT.Base.Services
{
    public interface ICustomerGroupService : IService<CustomerGroup>
    {
        Task<ApiResponseData<List<CustomerGroup>>> SearchPageAsync(int pageIndex, int pageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<CustomerGroup>> GetById(int id);
        Task<ApiResponseData<CustomerGroup>> Create(CustomerGroupCommand model);
        Task<ApiResponseData<CustomerGroup>> Edit(CustomerGroupCommand model);
        Task<ApiResponseData<CustomerGroup>> Delete(int id);

    }
    public class CustomerGroupService : ICustomerGroupService
    {
        private readonly ILogger _logger;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly IHubContext<CategoryHub> _hubContext;

        public CustomerGroupService
        (
            ILogger<CustomerGroupService> logger,
            ICustomerGroupRepository iCustomerGroupRepository,
            IHubContext<CategoryHub> hubContext,
            IParameterRepository iParameterRepository
        )
        {
            _logger = logger;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _hubContext = hubContext;
            _iParameterRepository = iParameterRepository;
        }

        public async Task<ApiResponseData<List<CustomerGroup>>> SearchPageAsync(int pageIndex, int pageSize, string key, string sortby, string sorttype)
        {
            try
            {
                return await _iCustomerGroupRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => 
                     (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype));
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerGroupSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<CustomerGroup>>();
            }
        }

        public async Task<ApiResponseData<CustomerGroup>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<CustomerGroup>() { Data = await _iCustomerGroupRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerGroupSevice.Get", ex.ToString());
                return new ApiResponseData<CustomerGroup>();
            }
        }

        public async Task<ApiResponseData<CustomerGroup>> Create(CustomerGroupCommand model)
        {
            try
            {
                var dlAdd = new CustomerGroup
                {
                    Name = model.Name,
                    Order = model.Order,
                    CreatedDate = DateTime.Now,
                    Status = model.Status,
                    Note = model.Note,
                };

                await _iCustomerGroupRepository.AddAsync(dlAdd);
                await _iCustomerGroupRepository.Commit();
                await InitCategoryAsync();
                return new ApiResponseData<CustomerGroup>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerGroupSevice.Create", ex.ToString());
                return new ApiResponseData<CustomerGroup>();
            }
        }

        public async Task<ApiResponseData<CustomerGroup>> Edit(CustomerGroupCommand model)
        {
            try
            {
                var dl = await _iCustomerGroupRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<CustomerGroup>() { Data = null, Status = 0 };
                }

                dl.Name = model.Name;
                dl.Note = model.Note;
                dl.Order = model.Order;
                dl.Status = model.Status;
                _iCustomerGroupRepository.Update(dl);
                await _iCustomerGroupRepository.Commit();
                await InitCategoryAsync();
                return new ApiResponseData<CustomerGroup>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerGroupSevice.Edit", ex.ToString());
                return new ApiResponseData<CustomerGroup>();
            }
        }

        public async Task<ApiResponseData<CustomerGroup>> Delete(int id)
        {
            try
            {
                var dl = await _iCustomerGroupRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<CustomerGroup>() { Data = null, Status = 0 };
                }
                _iCustomerGroupRepository.Delete(dl);
                await _iCustomerGroupRepository.Commit();
                await InitCategoryAsync();
                return new ApiResponseData<CustomerGroup>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CustomerGroupSevice.Delete", ex.ToString());
                return new ApiResponseData<CustomerGroup>();
            }
        }

        private Func<IQueryable<CustomerGroup>, IOrderedQueryable<CustomerGroup>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<CustomerGroup>, IOrderedQueryable<CustomerGroup>> functionOrder = null;
            switch (sortby)
            {
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<CustomerGroup>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<CustomerGroup>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                default:
                    functionOrder = EntityExtention<CustomerGroup>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }
        private async Task InitCategoryAsync()
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", "CustomerGroup");
            }
            catch{}
        }
    }
}
