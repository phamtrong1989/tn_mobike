﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.Domain.Model.Common;
using PT.Base.Command;
using PT.Base;
using Microsoft.AspNetCore.SignalR;

namespace TN.Base.Services
{
    public interface IProjectService : IService<Project>
    {
        Task<ApiResponseData<List<Project>>> SearchPageAsync(int pageIndex, int PageSize, string key, EProjectStatus? status, string orderby, string ordertype);
        Task<ApiResponseData<Project>> GetById(int id);
        Task<ApiResponseData<Project>> Create(ProjectCommand model);
        Task<ApiResponseData<Project>> Edit(ProjectCommand model);
        Task<ApiResponseData<Project>> Delete(int id);

        #region [CustomerGroup]
        Task<ApiResponseData<List<CustomerGroup>>> CustomerGroupSearchPageAsync(int pageIndex, int pageSize, int ProjectId, string orderby, string ordertype);
        Task<ApiResponseData<CustomerGroup>> CustomerGroupGetById(int id);
        Task<ApiResponseData<CustomerGroup>> CustomerGroupCreate(CustomerGroupCommand model);
        Task<ApiResponseData<CustomerGroup>> CustomerGroupEdit(CustomerGroupCommand model);
        Task<ApiResponseData<CustomerGroup>> CustomerGroupDelete(int id);
        #endregion

        #region [TicketPrice]
        Task<ApiResponseData<List<TicketPrice>>> TicketPriceSearchPageAsync(int pageIndex, int pageSize, int? ProjectId, int? customerGroupId, string sortby, string sorttype);
        Task<ApiResponseData<TicketPrice>> TicketPriceGetById(int id);
        Task<ApiResponseData<TicketPrice>> TicketPriceCreate(TicketPriceCommand model);
        Task<ApiResponseData<TicketPrice>> TicketPriceEdit(TicketPriceCommand model);
        Task<ApiResponseData<TicketPrice>> TicketPriceDelete(int id);
        #endregion

        #region [ProjectAccount]
        Task<ApiResponseData<List<ProjectAccount>>> ProjectAccountSearchPageAsync(int pageIndex, int pageSize,int? ProjectId, int? customerGroupId, EAccountStatus? status, EAccountType? type, ESex? sex, string key, int? id, string sortby, string sorttype);
        Task<ApiResponseData<ProjectAccount>> ProjectAccountGetById(int id);
        Task<ApiResponseData<ProjectAccount>> ProjectAccountCreate(ProjectAccountCommand model);
        Task<ApiResponseData<ProjectAccount>> ProjectAccountEdit(ProjectAccountCommand model);
        Task<ApiResponseData<ProjectAccount>> ProjectAccountDelete(int id);
        #endregion

        #region [Account]
        Task<ApiResponseData<Account>> AccountGetByPhone(string phone);
        #endregion

        #region [Log]
        Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? projectId, string sortby, string sorttype);
        #endregion
        Task<ApiResponseData<Project>> HeadquartersEdit(HeadquartersEditCommand model);
        Task<ApiResponseData<ProjectResource>> ProjectResourceGetById(int projectId, string language);
        Task<ApiResponseData<ProjectResource>> ProjectResourceEdit(ProjectResourceCommand model);
        Task<ApiResponseData<CustomerGroupResource>> CustomerGroupResourceGetById(int customerGroupId, string language);
        Task<ApiResponseData<CustomerGroupResource>> CustomerGroupResourceEdit(CustomerGroupResourceCommand model);
    }
    public class ProjectService : IProjectService
    {
        private readonly ILogger _logger;
        private readonly IProjectRepository _iProjectRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IProjectResourceRepository _iProjectResourceRepository;
        private readonly ILanguageRepository _iLanguageRepository;
        private readonly ICustomerGroupResourceRepository _iCustomerGroupResourceRepository;
        public ProjectService
        (
            ILogger<ProjectService> logger,
            IProjectRepository iProjectRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IHubContext<CategoryHub> hubContext,
            IParameterRepository iParameterRepository,
            ITicketPriceRepository iTicketPriceRepository,
            IProjectAccountRepository iProjectAccountRepository,
            IAccountRepository iAccountRepository,
            ILogRepository iLogRepository,
            IUserRepository iUserRepository,
            IProjectResourceRepository iProjectResourceRepository,
            ILanguageRepository iLanguageRepository,
            ICustomerGroupResourceRepository iCustomerGroupResourceRepository
        )
        {
            _logger = logger;
            _iProjectRepository = iProjectRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iParameterRepository =  iParameterRepository;
            _hubContext = hubContext;
            _iTicketPriceRepository = iTicketPriceRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iAccountRepository = iAccountRepository;
            _iLogRepository = iLogRepository;
            _iUserRepository = iUserRepository;
            _iProjectResourceRepository = iProjectResourceRepository;
            _iLanguageRepository = iLanguageRepository;
            _iCustomerGroupResourceRepository = iCustomerGroupResourceRepository;
        }

        #region [X]

        public async Task<ApiResponseData<List<Project>>> SearchPageAsync(int pageIndex, int pageSize, string key, EProjectStatus? status, string sortby, string sorttype)
        {
            try
            {
                var data = await _iProjectRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => 
                     (key == null || x.Name.Contains(key) || x.Description.Contains(key) || x.Code.Contains(key))
                     && (x.Status == status || status == null)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Project
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        Code = x.Code,
                        IsPrivate = x.IsPrivate,
                        Status = x.Status,
                        CreatedDate = x.CreatedDate,
                        UpdatedDate = x.UpdatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedUser = x.UpdatedUser,
                        DefaultZoom = x.DefaultZoom,
                        Lat = x.Lat,
                        Lng = x.Lng,
                        HeadquartersLng = x.HeadquartersLng,
                        HeadquartersLat = x.HeadquartersLat
                    });

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Project>>();
            }
        }

        public async Task<ApiResponseData<Project>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Project>() { Data = await _iProjectRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.Get", ex.ToString());
                return new ApiResponseData<Project>();
            }
        }

        public async Task<ApiResponseData<Project>> Create(ProjectCommand model)
        {
            try
            {
                var dlAdd = new Project
                {
                    Name = model.Name,
                    Description = model.Description,
                    Code = model.Code,
                    IsPrivate = model.IsPrivate,
                    Status = model.Status,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    Lat = model.Lat,
                    Lng = model.Lng,
                    DefaultZoom = model.DefaultZoom
                };
                await _iProjectRepository.AddAsync(dlAdd);
                await _iProjectRepository.Commit();
                await InitCategoryAsync("Project");
                await AddLog(dlAdd.Id, $"Thêm mới dự án '{dlAdd.Name}'", LogType.Insert);
                return new ApiResponseData<Project>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.Create", ex.ToString());
                return new ApiResponseData<Project>();
            }
        }

        public async Task<ApiResponseData<Project>> Edit(ProjectCommand model)
        {
            try
            {
                var dl = await _iProjectRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Project>() { Data = null, Status = 0 };
                }

                dl.Name = model.Name;
                dl.Description = model.Description;
                dl.Code = model.Code;
                dl.IsPrivate = model.IsPrivate;
                dl.Status = model.Status;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                dl.Lat = model.Lat;
                dl.Lng = model.Lng;
                dl.DefaultZoom = model.DefaultZoom;
                _iProjectRepository.Update(dl);
                await _iProjectRepository.Commit();
                await AddLog(dl.Id, $"Cập nhật dự án '{dl.Name}'", LogType.Update);
                await InitCategoryAsync("Project");

                return new ApiResponseData<Project>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.Edit", ex.ToString());
                return new ApiResponseData<Project>();
            }
        }

        public async Task<ApiResponseData<ProjectResource>> ProjectResourceGetById(int projectId, string language)
        {
            try
            {
                return new ApiResponseData<ProjectResource>() { Data = await _iProjectResourceRepository.SearchOneAsync(x => x.ProjectId == projectId && x.Language == language), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.ProjectResourceGetById", ex.ToString());
                return new ApiResponseData<ProjectResource>();
            }
        }

        public async Task<ApiResponseData<ProjectResource>> ProjectResourceEdit(ProjectResourceCommand model)
        {
            try
            {
                var dl = await _iProjectResourceRepository.SearchOneAsync(x => x.ProjectId == model.ProjectId && x.Language == model.Language);
                if (dl == null)
                {
                    var lan = await _iLanguageRepository.SearchOneAsync(x => x.Code1 == model.Language);
                    await _iProjectResourceRepository.AddAsync(new ProjectResource
                    {
                        Language = model.Language,
                        Name = model.Name,
                        ProjectId = model.ProjectId,
                        LanguageId = lan?.Id ?? 0
                    });
                    await _iProjectResourceRepository.Commit();
                    await AddLog(dl.Id, $"Thêm mới ngôn ngữ '{dl.Language}' dự án '{dl.Name}'", LogType.Update);

                }
                else
                {
                    dl.Name = model.Name;
                    _iProjectResourceRepository.Update(dl);
                    await _iProjectResourceRepository.Commit();
                    await AddLog(dl.Id, $"Cập nhật ngôn ngữ '{dl.Language}' dự án '{dl.Name}'", LogType.Update);
                }

                return new ApiResponseData<ProjectResource>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.ProjectResourceEdit", ex.ToString());
                return new ApiResponseData<ProjectResource>();
            }
        }

        public async Task<ApiResponseData<CustomerGroupResource>> CustomerGroupResourceGetById(int customerGroupId, string language)
        {
            try
            {
                return new ApiResponseData<CustomerGroupResource>() { Data = await _iCustomerGroupResourceRepository.SearchOneAsync(x => x.CustomerGroupId == customerGroupId && x.Language == language), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.CustomerGroupResourceGetById", ex.ToString());
                return new ApiResponseData<CustomerGroupResource>();
            }
        }

        public async Task<ApiResponseData<CustomerGroupResource>> CustomerGroupResourceEdit(CustomerGroupResourceCommand model)
        {
            try
            {
                var dl = await _iCustomerGroupResourceRepository.SearchOneAsync(x => x.CustomerGroupId == model.CustomerGroupId && x.Language == model.Language);
                if (dl == null)
                {
                    var lan = await _iLanguageRepository.SearchOneAsync(x => x.Code1 == model.Language);
                    dl = new CustomerGroupResource
                    {
                        Language = model.Language,
                        Name = model.Name,
                        CustomerGroupId = model.CustomerGroupId,
                        LanguageId = lan?.Id ?? 0
                    };
                    await _iCustomerGroupResourceRepository.AddAsync(dl);
                    await _iCustomerGroupResourceRepository.Commit();
                    await AddLog(dl.Id, $"Thêm mới ngôn ngữ '{dl.Language}' nhóm khách hàng '{dl.Name}'", LogType.Update);
                }
                else
                {
                    dl.Name = model.Name;
                    _iCustomerGroupResourceRepository.Update(dl);
                    await _iCustomerGroupResourceRepository.Commit();
                    await AddLog(dl.Id, $"Cập nhật ngôn ngữ '{dl.Language}' nhóm khách hàng '{dl.Name}'", LogType.Update);
                }

                return new ApiResponseData<CustomerGroupResource>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.Edit", ex.ToString());
                return new ApiResponseData<CustomerGroupResource>();
            }
        }

        public async Task<ApiResponseData<Project>> HeadquartersEdit(HeadquartersEditCommand model)
        {
            try
            {
                var dl = await _iProjectRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Project>() { Data = null, Status = 0 };
                }
                dl.HeadquartersLat = model.Lat;
                dl.HeadquartersLng = model.Lng;
                _iProjectRepository.Update(dl);
                await _iProjectRepository.Commit();
                await AddLog(dl.Id, $"Cập nhật vị trí trung tâm dự án '{dl.Name}'", LogType.Update);
                await InitCategoryAsync("Project");

                return new ApiResponseData<Project>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.Edit", ex.ToString());
                return new ApiResponseData<Project>();
            }
        }

        public async Task<ApiResponseData<Project>> Delete(int id)
        {
            try
            {
                var dl = await _iProjectRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Project>() { Data = null, Status = 0 };
                }
                await AddLog(dl.Id, $"Xóa dự án '{dl.Name}'", LogType.Delete);
                _iProjectRepository.Delete(dl);
                await _iProjectRepository.Commit();
                await InitCategoryAsync("Project");
                return new ApiResponseData<Project>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.Delete", ex.ToString());
                return new ApiResponseData<Project>();
            }
        }

        private Func<IQueryable<Project>, IOrderedQueryable<Project>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Project>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Project>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "name" => sorttype == "asc" ? EntityExtention<Project>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<Project>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "createDate" => sorttype == "asc" ? EntityExtention<Project>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Project>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "updateDate" => sorttype == "asc" ? EntityExtention<Project>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Project>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                _ => EntityExtention<Project>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        #region [CustomerGroup]
        public async Task<ApiResponseData<List<CustomerGroup>>> CustomerGroupSearchPageAsync(int pageIndex, int pageSize, int projectId, string sortby, string sorttype)
        {
            try
            {
                return await _iCustomerGroupRepository.SearchPagedAsync( pageIndex, pageSize, x => x.ProjectId == projectId, CustomerGroupOrderByExtention(sortby, sorttype));
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<CustomerGroup>>();
            }
        }

        public async Task<ApiResponseData<CustomerGroup>> CustomerGroupGetById(int id)
        {
            try
            {
                return new ApiResponseData<CustomerGroup>() { Data = await _iCustomerGroupRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.Get", ex.ToString());
                return new ApiResponseData<CustomerGroup>();
            }
        }

        public async Task<ApiResponseData<CustomerGroup>> CustomerGroupCreate(CustomerGroupCommand model)
        {
            try
            {
                if(model.IsDefault)
                {
                    // Kiểm tra đã có biểu mẫu mặc định chưa
                    var anyMain = await _iCustomerGroupRepository.AnyAsync(x => x.IsDefault && x.ProjectId == model.ProjectId);
                    if (anyMain)
                    {
                        return new ApiResponseData<CustomerGroup>() { Data = null, Status = 2 };
                    }
                }    

                var dlAdd = new CustomerGroup
                {
                    Name = model.Name,
                    Order = model.Order,
                    CreatedDate = DateTime.Now,
                    Status = model.Status,
                    Note = model.Note,
                    ProjectId = model.ProjectId,
                    IsDefault = model.IsDefault,
                    CreatedUser = UserInfo.UserId,
                };

                await _iCustomerGroupRepository.AddAsync(dlAdd);
                await _iCustomerGroupRepository.Commit();
                await InitCategoryAsync("CustomerGroup");
                await AddLog(dlAdd.ProjectId, $"Thêm mới nhóm khách hàng '#{dlAdd.Id},{dlAdd.Name}'", LogType.Insert);

                return new ApiResponseData<CustomerGroup>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.CustomerGroupCreate", ex.ToString());
                return new ApiResponseData<CustomerGroup>();
            }
        }

        public async Task<ApiResponseData<CustomerGroup>> CustomerGroupEdit(CustomerGroupCommand model)
        {
            try
            {
                var dl = await _iCustomerGroupRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<CustomerGroup>() { Data = null, Status = 0 };
                }

                if(model.IsDefault)
                {
                    var anyMain = await _iCustomerGroupRepository.AnyAsync(x => x.IsDefault && x.ProjectId == model.ProjectId && x.Id != dl.Id);
                    if (anyMain)
                    {
                        return new ApiResponseData<CustomerGroup>() { Data = null, Status = 2 };
                    }
                }

                bool isChangeDefault = dl.IsDefault != model.IsDefault;

                dl.Name = model.Name;
                dl.Note = model.Note;
                dl.Order = model.Order;
                dl.Status = model.Status;
                dl.IsDefault = model.IsDefault;
                dl.UpdatedDate = DateTime.Now;

                // Cập nhật lại bảng vé mặc định
                if(isChangeDefault)
                {
                    var listTicketPrice = await _iTicketPriceRepository.Search(x => x.CustomerGroupId == dl.Id);
                    foreach (var item in listTicketPrice)
                    {
                        item.IsDefault = model.IsDefault;
                        _iTicketPriceRepository.Update(item);
                    }
                    await _iTicketPriceRepository.Commit();
                }    

                _iCustomerGroupRepository.Update(dl);
                await _iCustomerGroupRepository.Commit();
                await InitCategoryAsync("CustomerGroup");
                await AddLog(dl.ProjectId, $"Cập nhật nhóm khách hàng '#{dl.Id},{dl.Name}'", LogType.Update);

                return new ApiResponseData<CustomerGroup>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.CustomerGroupEdit", ex.ToString());
                return new ApiResponseData<CustomerGroup>();
            }
        }
        public async Task<ApiResponseData<CustomerGroup>> CustomerGroupDelete(int id)
        {
            try
            {
                var dl = await _iCustomerGroupRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<CustomerGroup>() { Data = null, Status = 0 };
                }

                // Kiểm tra xem có bảng giá vé đính theo nhóm không
                var ktTicket = await _iTicketPriceRepository.AnyAsync(x => x.CustomerGroupId == id);
                if(ktTicket)
                {
                    return new ApiResponseData<CustomerGroup>() { Data = null, Status = 2 };
                }

                await AddLog(dl.ProjectId, $"Xóa nhóm khách hàng '#{dl.Id},{dl.Name}'", LogType.Delete);

                _iCustomerGroupRepository.Delete(dl);
                await _iCustomerGroupRepository.Commit();
                await InitCategoryAsync("CustomerGroup");
                return new ApiResponseData<CustomerGroup>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.CustomerGroupDelete", ex.ToString());
                return new ApiResponseData<CustomerGroup>();
            }
        }

        private Func<IQueryable<CustomerGroup>, IOrderedQueryable<CustomerGroup>> CustomerGroupOrderByExtention(string sortby, string sorttype)
        {
           
            return sortby switch
            {
                "name" => sorttype == "asc" ? EntityExtention<CustomerGroup>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<CustomerGroup>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                _ => EntityExtention<CustomerGroup>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        private async Task InitCategoryAsync(string tableName)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName);
            }
            catch { }
        }
        #endregion

        #region [TicketPrice]

        public async Task<ApiResponseData<List<TicketPrice>>> TicketPriceSearchPageAsync(int pageIndex, int pageSize, int? projectId, int? customerGroupId, string sortby, string sorttype)
        {
            try
            {
                return await _iTicketPriceRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.ProjectId == projectId || projectId == null) &&
                         (x.CustomerGroupId == customerGroupId || customerGroupId == null)
                    ,
                    TicketPriceOrderByExtention(sortby, sorttype),
                    x => new TicketPrice
                    {
                        Id = x.Id,
                        ProjectId = x.ProjectId,
                        CustomerGroupId = x.CustomerGroupId,
                        TicketType = x.TicketType,
                        AllowChargeInSubAccount = x.AllowChargeInSubAccount,
                        TicketValue = x.TicketValue,
                        BlockPerMinute = x.BlockPerMinute,
                        BlockPerViolation = x.BlockPerViolation,
                        BlockViolationValue = x.BlockViolationValue,
                        LimitMinutes = x.LimitMinutes,
                        IsDefault = x.IsDefault,
                        IsActive = x.IsActive,
                        CreateDate = x.CreateDate,
                        UpdateDate = x.UpdateDate,
                        Name = x.Name,
                        Note = x.Note
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.TicketPriceSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<TicketPrice>>();
            }
        }

        public async Task<ApiResponseData<TicketPrice>> TicketPriceGetById(int id)
        {
            try
            {
                return new ApiResponseData<TicketPrice>() { Data = await _iTicketPriceRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.TicketPriceGetById", ex.ToString());
                return new ApiResponseData<TicketPrice>();
            }
        }

        public async Task<ApiResponseData<TicketPrice>> TicketPriceCreate(TicketPriceCommand model)
        {
            try
            {
                var data = await CustomerGroupGetById(model.CustomerGroupId);
                if(data.Data ==null)
                {
                    return new ApiResponseData<TicketPrice>() { Status = 2 };
                }    

                var dlAdd = new TicketPrice
                {
                    ProjectId = model.ProjectId,
                    CustomerGroupId = model.CustomerGroupId,
                    TicketType = model.TicketType,
                    AllowChargeInSubAccount = true,
                    TicketValue = model.TicketValue,
                    BlockPerMinute = model.BlockPerMinute,
                    BlockPerViolation = model.BlockPerViolation,
                    BlockViolationValue = model.BlockViolationValue,
                    LimitMinutes = model.LimitMinutes,
                    IsDefault = data.Data.IsDefault,
                    IsActive = model.IsActive,
                    CreateDate = DateTime.Now,
                    UpdateDate = null,
                    Name = model.Name
                };

                await _iTicketPriceRepository.AddAsync(dlAdd);
                await _iTicketPriceRepository.Commit();

                await AddLog(dlAdd.ProjectId, $"Thêm giá vé '#{dlAdd.Id},{dlAdd.Name}'", LogType.Insert);


                return new ApiResponseData<TicketPrice>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.TicketPriceCreate", ex.ToString());
                return new ApiResponseData<TicketPrice>();
            }
        }

        public async Task<ApiResponseData<TicketPrice>> TicketPriceEdit(TicketPriceCommand model)
        {
            try
            {
                var dl = await _iTicketPriceRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<TicketPrice>() { Data = null, Status = 0 };
                }

                var data = await CustomerGroupGetById(model.CustomerGroupId);
                if (data.Data == null)
                {
                    return new ApiResponseData<TicketPrice>() { Status = 0 };
                }

                dl.Name = model.Name;
                dl.ProjectId = model.ProjectId;
                dl.CustomerGroupId = model.CustomerGroupId;
                dl.TicketType = model.TicketType;
                dl.AllowChargeInSubAccount = true;
                dl.TicketValue = model.TicketValue;
                dl.BlockPerMinute = model.BlockPerMinute;
                dl.BlockPerViolation = model.BlockPerViolation;
                dl.BlockViolationValue = model.BlockViolationValue;
                dl.LimitMinutes = model.LimitMinutes;
                dl.IsDefault = data.Data.IsDefault;
                dl.IsActive = model.IsActive;
                dl.UpdateDate = DateTime.Now;

                _iTicketPriceRepository.Update(dl);
                await _iTicketPriceRepository.Commit();

                await AddLog(dl.ProjectId, $"Cập nhật giá vé '#{dl.Id},{dl.Name}'", LogType.Update);

                return new ApiResponseData<TicketPrice>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.TicketPriceEdit", ex.ToString());
                return new ApiResponseData<TicketPrice>();
            }
        }

        public async Task<ApiResponseData<TicketPrice>> TicketPriceDelete(int id)
        {
            try
            {
                var dl = await _iTicketPriceRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<TicketPrice>() { Data = null, Status = 0 };
                }

                await AddLog(dl.ProjectId, $"Xóa giá vé '#{dl.Id},{dl.Name}'", LogType.Delete);

                _iTicketPriceRepository.Delete(dl);
                await _iTicketPriceRepository.Commit();
                return new ApiResponseData<TicketPrice>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.Delete", ex.ToString());
                return new ApiResponseData<TicketPrice>();
            }
        }

        private Func<IQueryable<TicketPrice>, IOrderedQueryable<TicketPrice>> TicketPriceOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "createDate" => sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.CreateDate)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.CreateDate)),
                "updateDate" => sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.UpdateDate)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.UpdateDate)),
                _ => EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        #region [ProjectAccount]
        public async Task<ApiResponseData<List<ProjectAccount>>> ProjectAccountSearchPageAsync(
            int pageIndex, 
            int pageSize,
            int? ProjectId,
            int? customerGroupId,
            EAccountStatus? status,
            EAccountType? type,
            ESex? sex,
            string key,
            int? id,
            string sortby, 
            string sorttype)
        {
            try
            {
                return await _iProjectAccountRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    ProjectId,
                    customerGroupId,
                    status,
                    type,
                    sex,
                    key,
                    id
                    ,
                    ProjectAccountOrderByExtention(sortby, sorttype));
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.ProjectAccountSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ProjectAccount>>();
            }
        }

        public async Task<ApiResponseData<ProjectAccount>> ProjectAccountGetById(int id)
        {
            try
            {
                var data = await _iProjectAccountRepository.SearchOneAsync(x => x.Id == id);
                if(data!=null)
                {
                    data.Account = await _iAccountRepository.SearchOneAsync(x => x.Id == data.AccountId);
                    if (data.Account!=null)
                    {
                        data.Account.Password = "";
                        data.Account.PasswordSalt = "";
                    }
                }    
                return new ApiResponseData<ProjectAccount>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.ProjectAccountGetById", ex.ToString());
                return new ApiResponseData<ProjectAccount>();
            }
        }

        public async Task<ApiResponseData<Account>> AccountGetByPhone(string phone)
        {
            try
            {
                var data = await _iAccountRepository.SearchOneAsync(x => x.Phone == phone);
                if (data != null)
                {
                    data.Password = "";
                    data.PasswordSalt = "";
                }
                return new ApiResponseData<Account>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.AccountGetByPhone", ex.ToString());
                return new ApiResponseData<Account>();
            }
        }

        public async Task<ApiResponseData<ProjectAccount>> ProjectAccountCreate(ProjectAccountCommand model)
        {
            try
            {
                var data = await _iProjectAccountRepository.SearchOneAsync(x => x.AccountId == model.AccountId && x.ProjectId == model.ProjectId);
                if(data!=null)
                {
                    return new ApiResponseData<ProjectAccount>() { Status = 2 };
                }    

                var dl = new ProjectAccount
                {
                    ProjectId = model.ProjectId,
                    AccountId = model.AccountId,
                    CustomerGroupId = model.CustomerGroupId,
                    SourceType = EProjectAccountSourceType.Edit,
                    DateImport = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    InvestorId = 0
                };

                await _iProjectAccountRepository.AddAsync(dl);
                await _iProjectAccountRepository.Commit();

                await AddLog(dl.ProjectId, $"Thêm phân nhóm khách hàng '#{dl.Id},{dl.AccountId}'", LogType.Insert);

                return new ApiResponseData<ProjectAccount>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.ProjectAccountCreate", ex.ToString());
                return new ApiResponseData<ProjectAccount>();
            }
        }

        public async Task<ApiResponseData<ProjectAccount>> ProjectAccountEdit(ProjectAccountCommand model)
        {
            try
            {
                var dl = await _iProjectAccountRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ProjectAccount>() { Data = null, Status = 0 };
                }

                dl.CustomerGroupId = model.CustomerGroupId;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iProjectAccountRepository.Update(dl);
                await _iProjectAccountRepository.Commit();

                await AddLog(dl.ProjectId, $"Cập nhật phân nhóm khách hàng '#{dl.Id},{dl.AccountId}'", LogType.Update);

                return new ApiResponseData<ProjectAccount>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.ProjectAccountEdit", ex.ToString());
                return new ApiResponseData<ProjectAccount>();
            }
        }
        public async Task<ApiResponseData<ProjectAccount>> ProjectAccountDelete(int id)
        {
            try
            {
                var dl = await _iProjectAccountRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ProjectAccount>() { Data = null, Status = 0 };
                }

                await AddLog(dl.ProjectId, $"Xóa phân nhóm khách hàng '#{dl.Id},{dl.AccountId}'", LogType.Delete);

                _iProjectAccountRepository.Delete(dl);
                await _iProjectAccountRepository.Commit();

                return new ApiResponseData<ProjectAccount>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.ProjectAccountDelete", ex.ToString());
                return new ApiResponseData<ProjectAccount>();
            }
        }

        private Func<IQueryable<ProjectAccount>, IOrderedQueryable<ProjectAccount>> ProjectAccountOrderByExtention(string sortby, string sorttype) => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "sourceType" => sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.SourceType)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.SourceType)),
            "dateImport" => sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.DateImport)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.DateImport)),
            "createDate" => sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "updateDate" => sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            _ => EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.Id)),
        };
        #endregion

        #region [Log]
        public async Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? projectId, string sortby, string sorttype)
        {
            try
            {
                var data =  await _iLogRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.ObjectId == projectId || projectId == null) && x.Object == "Project", LogOrderByExtention(sortby, sorttype));

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.SystemUserId).ToList());
                foreach(var item in data.Data)
                {
                    item.SystemUserObject = users.FirstOrDefault(x => x.Id == item.SystemUserId);
                }   
                
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.LogSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Domain.Model.Log>>();
            }
        }

        private Func<IQueryable<Domain.Model.Log>, IOrderedQueryable<Domain.Model.Log>> LogOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "CreatedDate" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                _ => EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion
    }
}

