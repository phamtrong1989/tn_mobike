﻿using Microsoft.Extensions.Logging;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Base;
using PT.Domain.Model;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Drawing;
using Newtonsoft.Json;
using System.Diagnostics;
using TN.Shared;

namespace TN.Base.Services
{
    public interface IBikeService : IService<Bike>
    {
        Task<ApiResponseData<List<Bike>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            EDockBookingStatus? bookingStatus,
            string key,
            int? warehouseId,
            int? modelId,
            int? projectId,
            Bike.EBikeType? type,
            Bike.EBikeStatus? status,
            bool? connectionStatus,
            int? battery,
            bool? lockStatus,
            bool? charging,
            int? stationId,
            string sortby,
            string sorttype
            );
        Task<ApiResponseData<Bike>> GetById(int id);
        Task<ApiResponseData<Bike>> Create(BikeCommand model);
        Task<ApiResponseData<Bike>> Edit(BikeCommand model);
        Task<ApiResponseData<Bike>> Delete(int id);
        Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? objectId, string sortby, string sorttype);
        Task<ApiResponseData<List<Dock>>> DockSearch(string key, int projectId, int? investorId);
        Task<ApiResponseData<Bike>> EditLocation(BikeEditLocationCommand model);
        Task<ApiResponseData<List<GPSData>>> GPSByDock(int id, DateTime date, string startTime, string endTime, bool incudeStation, bool disableZeroLocation);

        Task<FileContentResult> ExportReport(
            string key,
            int? warehouseId,
            int? modelId,
            int? projectId,
            Bike.EBikeType? type,
            Bike.EBikeStatus? status,
            EDockBookingStatus? bookingStatus
        );
    }

    public class BikeService : IBikeService
    {
        private readonly ILogger _logger;
        private readonly LogSettings _logSettings;

        private readonly IBikeRepository _iBikeRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IWarehouseRepository _iWarehouseRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly IGPSDataRepository _iGPSDataRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IProjectRepository _iProjectRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IEntityBaseRepository<Producer> _producerRepository;
        private readonly IEntityBaseRepository<Model> _modelRepository;
        private readonly IEntityBaseRepository<Supplies> _suppliesRepository;
        private readonly IEntityBaseRepository<BikeInfo> _bikeInfoRepository;
        private readonly IEntityBaseRepository<BikeReport> _bikeReportRepository;
        private readonly IEntityBaseRepository<Domain.Model.Color> _colorRepository;
        private readonly string controllerName = "";
        public BikeService
        (
            ILogger<BikeService> logger,
            IOptions<LogSettings> logSettings,
            IWebHostEnvironment iWebHostEnvironment,

            IBikeRepository iBikeRepository,
            ILogRepository iLogRepository,
            IDockRepository iDockRepository,
            IUserRepository iUserRepository,
            IWarehouseRepository iWarehouseRepository,
            IBookingRepository iBookingRepository,
            IStationRepository iStationRepository,
            IGPSDataRepository iGPSDataRepository,
            IProjectRepository iProjectRepository,
            IInvestorRepository iInvestorRepository,
            IEntityBaseRepository<Producer> producerRepository,
            IEntityBaseRepository<Model> modelRepository,
            IEntityBaseRepository<Supplies> suppliesRepository,
            IEntityBaseRepository<Domain.Model.Color> colorRepository,
            ITransactionRepository transactionRepository,
            IEntityBaseRepository<BikeInfo> bikeInfoRepository,
            IEntityBaseRepository<BikeReport> bikeReportRepository
        )
        {
            _logger = logger;
            _logSettings = logSettings.Value;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iBikeRepository = iBikeRepository;
            _iLogRepository = iLogRepository;
            _iDockRepository = iDockRepository;
            _iUserRepository = iUserRepository;
            _iWarehouseRepository = iWarehouseRepository;
            _iBookingRepository = iBookingRepository;
            _iStationRepository = iStationRepository;
            _iGPSDataRepository = iGPSDataRepository;
            _iBikeRepository = iBikeRepository;
            _iUserRepository = iUserRepository;
            _iProjectRepository = iProjectRepository;
            _iInvestorRepository = iInvestorRepository;
            _iDockRepository = iDockRepository;
            _iStationRepository = iStationRepository;
            _iBookingRepository = iBookingRepository;
            _producerRepository = producerRepository;
            _modelRepository = modelRepository;
            _suppliesRepository = suppliesRepository;
            _colorRepository = colorRepository;
            _transactionRepository = transactionRepository;
            _bikeInfoRepository = bikeInfoRepository;
            _bikeReportRepository = bikeReportRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<Bike>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            EDockBookingStatus? bookingStatus,
            string key,
            int? warehouseId,
            int? modelId,
            int? projectId,
            Bike.EBikeType? type,
            Bike.EBikeStatus? status,
            bool? connectionStatus,
            int? battery,
            bool? lockStatus,
            bool? charging,
            int? stationId,
            string sortby,
            string sorttype
            )
        {
            try
            {
                var data = await _iBikeRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    key,
                    bookingStatus,
                    connectionStatus,
                    battery,
                    lockStatus,
                    charging,
                    x =>
                     (projectId == null || x.ProjectId == projectId)
                    && (status == null || x.Status == status)
                    && (type == null || x.Type == type)
                    && (modelId == null || x.ModelId == modelId)
                    && (stationId == null || x.StationId == stationId)
                    && (warehouseId == null || x.WarehouseId == warehouseId),
                    OrderByExtention(sortby, sorttype));

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                var docks = await _iDockRepository.Search(x => data.Data.Select(m => m.DockId).Contains(x.Id));

                var ws = await _iWarehouseRepository.Search(x => data.Data.Select(m => m.WarehouseId).Contains(x.Id));

                var books = await _iBookingRepository.Search(x => data.Data.Select(m => m.Id).Contains(x.BikeId) && x.Status == EBookingStatus.Start);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                    item.Warehouse = ws.FirstOrDefault(x => x.Id == item.WarehouseId);
                    item.BookingStatus = books.Any(x => x.BikeId == item.Id) ? EDockBookingStatus.Moving : EDockBookingStatus.Free;
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Bike>>();
            }
        }

        public async Task<ApiResponseData<Bike>> GetById(int id)
        {
            try
            {
                var data = new ApiResponseData<Bike>() { Data = await _iBikeRepository.SearchOneAsync(x => x.Id == id) };
                if (data.Data != null)
                {
                    data.Data.Dock = await _iDockRepository.SearchOneAsync(x => x.Id == data.Data.DockId);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeSevice.Get", ex.ToString());
                return new ApiResponseData<Bike>();
            }
        }

        public async Task<ApiResponseData<Bike>> Create(BikeCommand model)
        {
            try
            {
                var isPlate = await _iBikeRepository.AnyAsync(x => x.Plate == model.Plate.Trim());
                if (isPlate)
                {
                    return new ApiResponseData<Bike>() { Status = 2 };
                }

                var isSerialNumber = await _iBikeRepository.AnyAsync(x => x.SerialNumber == model.SerialNumber.Trim());
                if (isSerialNumber)
                {
                    return new ApiResponseData<Bike>() { Status = 3 };
                }

                if (model.DockId > 0)
                {
                    var isUser = await _iBookingRepository.AnyAsync(x => x.DockId == model.DockId && x.Status == EBookingStatus.Start);
                    if (isUser)
                    {
                        return new ApiResponseData<Bike>() { Status = 4 };
                    }
                    // Kiểm tra dock có cùng dự án không
                    var dock = await _iDockRepository.SearchOneAsync(x => x.Id == model.DockId);
                    if (dock != null && dock.ProjectId != model.ProjectId)
                    {
                        return new ApiResponseData<Bike>() { Status = 5 };
                    }

                    if (dock != null && dock.InvestorId != model.InvestorId)
                    {
                        return new ApiResponseData<Bike>() { Status = 6 };
                    }
                    // check dock đã được gắn cho khóa khác
                    var isGanBike = await _iBikeRepository.AnyAsync(x => x.DockId == model.DockId);
                    if (isGanBike)
                    {
                        return new ApiResponseData<Bike>() { Status = 7 };
                    }
                    // Kiểm tra khóa muốn gán vào có đang trong chuyến đi không
                    var isDockUse = await _iBookingRepository.AnyAsync(x => x.DockId == model.DockId && x.Status == EBookingStatus.Start);
                    if (isDockUse)
                    {
                        return new ApiResponseData<Bike>() { Status = 8 };
                    }
                }

                Station dlStation = null;
                if (model.IsSetLocation && model.StationId > 0)
                {
                    dlStation = await _iStationRepository.SearchOneAsync(x => x.Id == model.StationId);
                }

                var dl = new Bike
                {
                    SerialNumber = model.SerialNumber,
                    Status = model.Status,
                    Type = model.Type,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    ColorId = model.ColorId,
                    Description = model.Description,
                    Images = model.Images,
                    InvestorId = model.InvestorId,
                    ModelId = model.InvestorId,
                    Plate = model.Plate,
                    ProducerId = model.ProducerId,
                    ProjectId = model.ProjectId,
                    WarehouseId = model.WarehouseId,
                    DockId = model.DockId,
                    StationId = model.StationId,
                    Lat = 0,
                    Long = 0,
                    Note = model.Note,
                    StartDate = model.StartDate,
                    ProductionDate = model.ProductionDate,
                    MinuteRequiredMaintenance = model.MinuteRequiredMaintenance
                };

                if (model.IsSetLocation && model.StationId > 0 && dlStation != null)
                {
                    dl.Lat = dlStation.Lat;
                    dl.Long = dlStation.Lng;
                }

                if (model.Status == Bike.EBikeStatus.ErrorWarehouse || model.Status == Bike.EBikeStatus.Warehouse)
                {
                    dl.StationId = 0;
                }

                await _iBikeRepository.AddAsync(dl);
                await _iBikeRepository.Commit();

                if (dl.DockId > 0)
                {
                    var dock = await _iDockRepository.SearchOneAsync(x => x.Id == dl.DockId);
                    if (dock != null)
                    {
                        dock.StationId = dl.StationId;
                        dock.Status = dl.Status;
                        dock.WarehouseId = dl.WarehouseId;

                        if (model.IsSetLocation && model.StationId > 0)
                        {
                            dock.Lat = dlStation.Lat;
                            dock.Long = dlStation.Lng;
                        }
                        _iDockRepository.Update(dock);
                        await _iDockRepository.Commit();
                    }
                }

                await _iLogRepository.AddLog(UserInfo.UserId, controllerName, dl.Id, $"Thêm mới xe '{dl.Plate}'", LogType.Update, $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}");

                return new ApiResponseData<Bike>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeSevice.Create", ex.ToString());
                return new ApiResponseData<Bike>();
            }
        }

        public async Task<ApiResponseData<Bike>> EditLocation(BikeEditLocationCommand model)
        {

            try
            {
                var dl = await _iBikeRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Bike>() { Data = null, Status = 0 };
                }

                var dock = await _iDockRepository.SearchOneAsync(x => x.Id == dl.DockId);

                if (dock == null)
                {
                    return new ApiResponseData<Bike>() { Data = null, Status = 2 };
                }

                dl.StationId = model.StationId;
                dl.Long = model.Lng;
                dl.Lat = model.Lat;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iBikeRepository.Update(dl);
                await _iBikeRepository.Commit();


                dock.StationId = model.StationId;
                dock.Long = model.Lng;
                dock.Lat = model.Lat;
                dock.UpdatedDate = DateTime.Now;
                dock.UpdatedUser = UserInfo.UserId;

                _iDockRepository.Update(dock);
                await _iDockRepository.Commit();

                await _iLogRepository.AddLog(UserInfo.UserId, controllerName, dl.Id, $"Cập nhật tọa độ, vị trí xe '{dl.SerialNumber}'", LogType.Update, $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}");

                return new ApiResponseData<Bike>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DockSevice.Edit", ex.ToString());
                return new ApiResponseData<Bike>();
            }
        }

        public async Task<ApiResponseData<Bike>> Edit(BikeCommand model)
        {
            try
            {
                var dl = await _iBikeRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Bike>() { Data = null, Status = 0 };
                }

                var isPlate = await _iBikeRepository.AnyAsync(x => x.Plate == model.Plate.Trim() && x.Id != dl.Id);
                if (isPlate)
                {
                    return new ApiResponseData<Bike>() { Status = 2 };
                }

                var isSerialNumber = await _iBikeRepository.AnyAsync(x => x.SerialNumber == model.SerialNumber.Trim() && x.Id != dl.Id);
                if (isSerialNumber)
                {
                    return new ApiResponseData<Bike>() { Status = 3 };
                }
                // Kiểm tra xe này đang được sử dụng của chuyến đi ko
                var isUse = await _iBookingRepository.AnyAsync(x => x.BikeId == dl.Id && x.Status == EBookingStatus.Start);
                if (isUse)
                {
                    return new ApiResponseData<Bike>() { Status = 4 };
                }

                dl.DockId = model.DockId;

                if (dl.DockId > 0)
                {
                    // Kiểm tra khóa muốn gán vào có đang trong chuyến đi không
                    var isDockUse = await _iBookingRepository.AnyAsync(x => x.DockId == dl.DockId && x.Status == EBookingStatus.Start);
                    if (isDockUse)
                    {
                        return new ApiResponseData<Bike>() { Status = 8 };
                    }
                    // Kiểm tra dock có cùng dự án không
                    var dock = await _iDockRepository.SearchOneAsync(x => x.Id == model.DockId);
                    if (dock != null && dock.ProjectId != model.ProjectId)
                    {
                        return new ApiResponseData<Bike>() { Status = 5 };
                    }
                    if (dock != null && dock.InvestorId != model.InvestorId)
                    {
                        return new ApiResponseData<Bike>() { Status = 6 };
                    }
                    // check dock đã được gắn cho khóa khác
                    var isGanBike = await _iBikeRepository.AnyAsync(x => x.DockId == model.DockId && x.Id != dl.Id);
                    if (isGanBike)
                    {
                        return new ApiResponseData<Bike>() { Status = 7 };
                    }
                }

                Station dlStation = null;

                dl.SerialNumber = model.SerialNumber;
                dl.Status = model.Status;
                dl.Type = model.Type;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.ColorId = model.ColorId;
                dl.Description = model.Description;
                dl.Images = model.Images;
                dl.InvestorId = model.InvestorId;
                dl.ModelId = model.InvestorId;
                dl.Plate = model.Plate;
                dl.ProducerId = model.ProducerId;
                dl.ProjectId = model.ProjectId;
                dl.WarehouseId = model.WarehouseId;
                dl.StationId = model.StationId;
                dl.ModelId = model.ModelId;
                dl.WarehouseId = model.WarehouseId;
                dl.Note = model.Note;
                dl.StartDate = model.StartDate;
                dl.ProductionDate = model.ProductionDate;
                dl.MinuteRequiredMaintenance = model.MinuteRequiredMaintenance;

                if (model.IsSetLocation && model.StationId > 0 && dlStation != null)
                {
                    dl.Lat = dlStation.Lat;
                    dl.Long = dlStation.Lng;
                }

                if (model.Status == Bike.EBikeStatus.ErrorWarehouse || model.Status == Bike.EBikeStatus.Warehouse)
                {
                    dl.StationId = 0;
                }

                _iBikeRepository.Update(dl);
                await _iBikeRepository.Commit();

                if (dl.DockId > 0)
                {
                    var dock = await _iDockRepository.SearchOneAsync(x => x.Id == dl.DockId);
                    if (dock != null)
                    {
                        dock.StationId = dl.StationId;
                        dock.Status = dl.Status;
                        dock.WarehouseId = dl.WarehouseId;

                        if (model.IsSetLocation && model.StationId > 0)
                        {
                            dock.Lat = dlStation.Lat;
                            dock.Long = dlStation.Lng;
                        }
                        _iDockRepository.Update(dock);
                        await _iDockRepository.Commit();
                    }
                }

                await _iLogRepository.AddLog(UserInfo.UserId, controllerName, dl.Id, $"Cập nhật xe '{dl.SerialNumber}'", LogType.Update, $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}");

                return new ApiResponseData<Bike>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeSevice.Edit", ex.ToString());
                return new ApiResponseData<Bike>();
            }
        }

        public async Task<ApiResponseData<Bike>> Delete(int id)
        {
            try
            {
                var dl = await _iBikeRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Bike>() { Data = null, Status = 0 };
                }

                if (dl.DockId > 0)
                {
                    var isUse = await _iBookingRepository.AnyAsync(x => x.BikeId == dl.Id && x.Status == EBookingStatus.Start);
                    if (isUse)
                    {
                        return new ApiResponseData<Bike>() { Status = 4 };
                    }
                }

                _iBikeRepository.Delete(dl);
                await _iLogRepository.AddLog(UserInfo.UserId, controllerName, dl.Id, $"Xóa xe '{dl.SerialNumber}'", LogType.Delete, $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}");
                await _iBikeRepository.Commit();

                return new ApiResponseData<Bike>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeSevice.Delete", ex.ToString());
                return new ApiResponseData<Bike>();
            }
        }

        #region [ExportReport]
        public async Task<FileContentResult> ExportReport(
            string key,
            int? warehouseId,
            int? modelId,
            int? projectId,
            Bike.EBikeType? type,
            Bike.EBikeStatus? status,
            EDockBookingStatus? bookingStatus
            )
        {
            var bikes = await _iBikeRepository.SearchToReport(
                    key,
                    bookingStatus,
                    x => (projectId == null || x.ProjectId == projectId)
                    && (status == null || x.Status == status)
                    && (type == null || x.Type == type)
                    && (modelId == null || x.ModelId == modelId)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                );

            var docks = await _iDockRepository.Search(
                x => bikes.Select(x => x.DockId).Distinct().Contains(x.Id),
                null,
                x => new Dock
                {
                    Id = x.Id,
                    IMEI = x.IMEI
                });

            var producers = await _producerRepository.Search(
                x => bikes.Select(x => x.ProducerId).Distinct().Contains(x.Id),
                null,
                x => new Producer
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var colors = await _colorRepository.Search(
                x => bikes.Select(x => x.ColorId).Distinct().Contains(x.Id),
                null,
                x => new Domain.Model.Color
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var models = await _modelRepository.Search(
                x => bikes.Select(x => x.ModelId).Distinct().Contains(x.Id),
                null,
                x => new Model
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var transactionGroupByBike = await _transactionRepository.CountTotalMinuteRentGroupByBikeAsync();

            var stations = await _iStationRepository.Search(
                x => bikes.Select(x => x.StationId).Distinct().Contains(x.Id),
                null,
                x => new Station
                {
                    Id = x.Id,
                    Name = x.Name,
                    ManagerUserId = x.ManagerUserId,
                    ProjectId = x.ProjectId,
                    InvestorId = x.InvestorId
                });

            var users = await _iUserRepository.Search(
                x => stations.Select(y => y.ManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    ManagerUserId = x.ManagerUserId,
                    Code = x.Code,
                });

            var projects = await _iProjectRepository.Search(
                x => stations.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => stations.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var bikeInfos = await _bikeInfoRepository.Search(
                x => bikes.Select(y => y.Id).Distinct().Contains(x.BikeId));

            foreach (var bike in bikes)
            {
                var station = stations.FirstOrDefault(x => x.Id == bike.StationId);
                if (station != null)
                {
                    station.ManagerUserObject = users.FirstOrDefault(x => x.Id == station.ManagerUserId);
                }
                bike.Station = station;

                bike.Dock = docks.FirstOrDefault(x => x.Id == bike.DockId);
                bike.Project = projects.FirstOrDefault(x => x.Id == bike.ProjectId);
                bike.Investor = investors.FirstOrDefault(x => x.Id == bike.InvestorId);
                bike.Producer = producers.FirstOrDefault(x => x.Id == bike.ProducerId);
                bike.Color = colors.FirstOrDefault(x => x.Id == bike.ColorId);
                bike.Model = models.FirstOrDefault(x => x.Id == bike.ModelId);
                bike.BikeInfo = bikeInfos.FirstOrDefault(x => x.BikeId == bike.Id);
                bike.NumMinuteRent = transactionGroupByBike.FirstOrDefault(x => x.BikeId == bike.Id)?.TotalMinutesRent;
                bike.BikeReport = await _bikeReportRepository.SearchOneAsync(x => x.BikeId == bike.Id && x.Status == false);
            }

            var employees = await _iUserRepository.Search(
                x => x.RoleType == RoleManagerType.Coordinator_VTS,
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            var suppliess = await _suppliesRepository.GetAll();

            var filePath = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(filePath)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Khóa/Sim");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelBike(workSheet, bikes, suppliess, employees);
                MyExcel.Save();
            }
            //
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = Path.GetFileName(filePath)
            };
        }

        private void FormatExcelBike(ExcelWorksheet worksheet, List<Bike> items, IEnumerable<Supplies> suppliess, IEnumerable<ApplicationUser> employees)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 15}, {4 , 20}, {5 , 15},
                {6 , 25}, {7 , 10}, {8 , 15}, {9 , 15}, {10, 15},
                {11, 16}, {12, 13}, {13, 15}, {14, 15}, {15, 20},
                {16, 20}, {17, 25}, {18, 25}, {19, 50}, {20, 35},
                {21, 15}, {22, 20}, {23, 20}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO QUẢN LÝ XE";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:W5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:W5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:W5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:W5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Biển số";
            worksheet.Cells["C5"].Value = "Serial";
            worksheet.Cells["D5"].Value = "IMEI khóa";
            worksheet.Cells["E5"].Value = "Loại xe";
            worksheet.Cells["F5"].Value = "Nhà sản xuất";
            worksheet.Cells["G5"].Value = "Màu sắc";
            worksheet.Cells["H5"].Value = "Kiểu mẫu";
            worksheet.Cells["I5"].Value = "Ngày sản xuất";
            worksheet.Cells["J5"].Value = "Ngày đưa vào khai thác";
            worksheet.Cells["K5"].Value = "Trạng thái";
            worksheet.Cells["L5"].Value = "Lũy kế số phút đã đi";
            worksheet.Cells["M5"].Value = "Thời gian bảo dưỡng gần nhất";
            worksheet.Cells["N5"].Value = "Tổng số giờ đã sử dụng cần bảo dưỡng (trên tổng 4800 giờ)";
            worksheet.Cells["O5"].Value = "Thời gian cần bảo dưỡng (định kỳ 4 tháng)";
            worksheet.Cells["P5"].Value = "Xe bị sự cố";
            worksheet.Cells["Q5"].Value = "Mô tả trước khi bảo dưỡng sửa chữa";
            worksheet.Cells["R5"].Value = "Mô tả sau khi bảo dưỡng sửa chữa";
            worksheet.Cells["S5"].Value = "Thông tin vật tư thay thế";
            worksheet.Cells["T5"].Value = "Trạm xe đang online";
            worksheet.Cells["U5"].Value = "Nhân viên quản lý trạm";
            worksheet.Cells["V5"].Value = "Đơn vị đầu tư";
            worksheet.Cells["W5"].Value = "Dự án";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Plate;
                worksheet.Cells[rs, 3].Value = item.SerialNumber;
                worksheet.Cells[rs, 4].Value = item.Dock?.IMEI;
                worksheet.Cells[rs, 5].Value = item.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 6].Value = item.Producer?.Name;
                worksheet.Cells[rs, 7].Value = item.Color?.Name;
                worksheet.Cells[rs, 8].Value = item.Model?.Name;
                worksheet.Cells[rs, 9].Value = item.ProductionDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 10].Value = item.StartDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 11].Value = item.Status.ToEnumGetDisplayName();
                worksheet.Cells[rs, 12].Value = item.NumMinuteRent;
                worksheet.Cells[rs, 13].Value = item.BikeInfo?.LastMaintenanceTime?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 14].Value = item.BikeInfo?.TotalMinutes / 60;

                //
                if (item.BikeInfo?.LastMaintenanceTime != null)
                {
                    worksheet.Cells[rs, 15].Value = item.BikeInfo?.LastMaintenanceTime?.AddMonths(4).ToString("dd/MM/yyyy");
                }
                else if (item.StartDate != null)
                {
                    worksheet.Cells[rs, 15].Value = item.StartDate?.AddMonths(4).ToString("dd/MM/yyyy");
                }
                else
                {
                    worksheet.Cells[rs, 15].Value = "Xe chưa hoạt động";
                }

                worksheet.Cells[rs, 16].Value = item.BikeReport?.Type.ToEnumGetDisplayName();

                try
                {
                    if (item.BikeInfo?.MaintenanceData != null)
                    {
                        var maintainRepair = JsonConvert.DeserializeObject<MaintenanceRepair>(item.BikeInfo?.MaintenanceData);
                    }
                    else
                    {
                        worksheet.Cells[rs, 17].Value = "";
                        worksheet.Cells[rs, 18].Value = "";
                    }

                    if (item.BikeInfo?.MaintenanceDataItem != null)
                    {
                        var maintainRepairItem = JsonConvert.DeserializeObject<MaintenanceRepairItem>(item.BikeInfo?.MaintenanceDataItem);
                        var supplies = suppliess.FirstOrDefault(x => x.Id == maintainRepairItem.SuppliesId);
                        var employee = employees.FirstOrDefault(x => x.Id == maintainRepairItem.EmployeeId);
                        worksheet.Cells[rs, 19].Value = $"Mã: {supplies?.Code} | Tên: {supplies?.Name} | Slg: {maintainRepairItem?.SuppliesQuantity} | Ngày: {maintainRepairItem.CompleteDate?.ToString("dd/MM/yyyy")} | NV: {employee.Code}";
                    }
                    else
                    {
                        worksheet.Cells[rs, 19].Value = "";
                    }

                }
                catch (Exception)
                {
                    worksheet.Cells[rs, 17].Value = "ERROR";
                    worksheet.Cells[rs, 18].Value = "ERROR";
                    worksheet.Cells[rs, 19].Value = "ERROR";
                }

                worksheet.Cells[rs, 20].Value = item.Station?.Name;
                worksheet.Cells[rs, 21].Value = item.Station?.ManagerUserObject?.Code;
                worksheet.Cells[rs, 22].Value = item.Investor?.Name;
                worksheet.Cells[rs, 23].Value = item.Project?.Name;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:W{rs - 1}");
            worksheet.Cells[$"A6:W{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private Func<IQueryable<Bike>, IOrderedQueryable<Bike>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Bike>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Bike>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "createdDate" => sorttype == "asc" ? EntityExtention<Bike>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Bike>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<Bike>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Bike>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                _ => EntityExtention<Bike>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        #region [Log]
        public async Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? objectId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iLogRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.ObjectId == objectId || objectId == null) && x.Object == controllerName, LogOrderByExtention(sortby, sorttype));

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.SystemUserId).ToList());
                foreach (var item in data.Data)
                {
                    item.SystemUserObject = users.FirstOrDefault(x => x.Id == item.SystemUserId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeSevice.LogSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Domain.Model.Log>>();
            }
        }

        private Func<IQueryable<Domain.Model.Log>, IOrderedQueryable<Domain.Model.Log>> LogOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "CreatedDate" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                _ => EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        public async Task<ApiResponseData<List<Dock>>> DockSearch(string key, int projectId, int? investorId)
        {
            try
            {
                var docks = await _iDockRepository.SearchTop(20, x => x.ProjectId == projectId && (x.InvestorId == investorId || investorId == null || investorId == 0) && (key == null || key == " " || x.SerialNumber.Contains(key) || x.SIM.Contains(key) || x.IMEI.Contains(key)), x => x.OrderBy(m => m.SerialNumber));
                var bikes = await _iBikeRepository.Search(x => docks.Select(x => x.Id).Contains(x.DockId));
                foreach (var item in docks)
                {
                    item.Bike = bikes.FirstOrDefault(x => x.DockId == item.Id);
                }
                return new ApiResponseData<List<Dock>>() { Data = docks, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeSevice.BikeSearch", ex.ToString());
                return new ApiResponseData<List<Dock>>();
            }
        }

        public async Task<ApiResponseData<List<GPSData>>> GPSByDock(int id, DateTime date, string startTime, string endTime, bool incudeStation, bool disableZeroLocation)
        {
            try
            {
                var getBike = await _iBikeRepository.SearchOneAsync(x => x.Id == id);
                if (getBike == null)
                {
                    return new ApiResponseData<List<GPSData>>() { Data = new List<GPSData>(), Status = 0 };
                }

                var getDock = await _iDockRepository.SearchOneAsync(x => x.Id == getBike.DockId);
                if (getDock == null)
                {
                    return new ApiResponseData<List<GPSData>>() { Data = new List<GPSData>(), Status = 0 };
                }

                var data = _iGPSDataRepository.GetByIMEI(_logSettings, getDock?.IMEI, Convert.ToDateTime($"{date:yyyy-MM-dd} {startTime}"), Convert.ToDateTime($"{date:yyyy-MM-dd} {endTime}"));
                if (disableZeroLocation)
                {
                    data = data.Where(x => x.Lat > 0 && x.Long > 0).OrderByDescending(x => x.CreateTimeTicks).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.CreateTimeTicks).ToList();
                }

                if (incudeStation)
                {
                    var stations = await _iStationRepository.Search(x => x.Status == ESationStatus.Active);
                    //1km
                    double delta = 10;
                    foreach (var item in data)
                    {
                        if (item.Lat > 0 && item.Long > 0)
                        {
                            var stionInRadius = FindRadius(stations, item.Lat - delta, item.Lat + delta, item.Long - delta, item.Long + delta);
                            foreach (var r in stionInRadius)
                            {
                                r.Distance = Function.DistanceInMeter(r.Lat, r.Lng, item.Lat, item.Long);
                            }

                            var stationNn = stionInRadius.OrderBy(x => x.Distance).FirstOrDefault();

                            item.Station = new Station
                            {
                                Id = stationNn.Id,
                                Name = stationNn.Name,
                                Address = stationNn.Address,
                                Lat = stationNn.Lat,
                                Lng = stationNn.Lng,
                                DisplayName = stationNn.DisplayName,
                                Distance = stationNn.Distance
                            };
                        }
                    }
                }

                return new ApiResponseData<List<GPSData>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError($"BikeSevice.GPSByDock {ex}");
                return new ApiResponseData<List<GPSData>>();
            }
        }

        public List<Station> FindRadius(List<Station> list, double min_lat, double max_lat, double min_lng, double max_lng)
        {
            return list.Where(x => x.Lat >= min_lat && x.Lat <= max_lat && x.Lng >= min_lng && x.Lng <= max_lng && x.Status == ESationStatus.Active).ToList();
        }

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private string InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return filePath;

        }
    }


}
