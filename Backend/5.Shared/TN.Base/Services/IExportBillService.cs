﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IExportBillService : IService<ExportBill>
    {
        Task<ApiResponseData<List<ExportBill>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            EExportBillStatus? status,
            int? warehouseId,
            int? toWarehouseId,
            DateTime? start,
            DateTime? end,
            EExportBillCompleteStatus? completeStatus,
            string sortby,
            string sorttype
            );
        Task<ApiResponseData<ExportBill>> GetById(int id);
        Task<ApiResponseData<ExportBill>> Create(ExportBillCommand model);
        Task<ApiResponseData<ExportBill>> Edit(ExportBillCommand model);
        Task<ApiResponseData<ExportBill>> Approve(ExportBillApproveCommand model);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);
        FileStream View(string pathData, bool isPrivate);
        Task<ApiResponseData<ExportBill>> Cancel(int id);
        Task<ApiResponseData<ExportBill>> CompleteApprove(int id, EExportBillCompleteStatus cs);
        Task<ApiResponseData<ExportBill>> Refuse(int id);

        #region [ExportBillItem]
        Task<ApiResponseData<List<ExportBillItem>>> ExportBillItemSearchPageAsync(int pageIndex, int pageSize, int exportBillId);
        Task<ApiResponseData<ExportBillItem>> ExportBillItemGetById(int id);
        Task<ApiResponseData<ExportBillItem>> ExportBillItemEdit(ExportBillItemCommand model);
        Task<ApiResponseData<ExportBillItem>> ExportBillItemCreate(ExportBillItemCommand model);
        Task<ApiResponseData<ExportBillItem>> ExportBillItemDelete(int id);
        #endregion

        Task<ApiResponseData<List<Supplies>>> SuppliesSearchKey(string key, int warehouseId);
    }

    public class ExportBillService : IExportBillService
    {
        private readonly ILogger _logger;
        private readonly BaseSettings _baseSettings;

        private readonly IExportBillRepository _iExportBillRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IExportBillItemRepository _iExportBillItemRepository;
        private readonly ISuppliesRepository _iSuppliesRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly string controllerName = "";

        public ExportBillService
        (
            ILogger<ExportBillService> logger,
            IOptions<BaseSettings> baseSettings,
            IExportBillRepository iExportBillRepository,
            ILogRepository iLogRepository,
            IExportBillItemRepository iExportBillItemRepository,
            ISuppliesRepository iSuppliesRepository,
            IUserRepository iUserRepository
        )
        {
            _logger = logger;
            _baseSettings = baseSettings.Value;
            _iExportBillRepository = iExportBillRepository;
            _iLogRepository = iLogRepository;
            _iExportBillItemRepository =  iExportBillItemRepository;
            _iSuppliesRepository = iSuppliesRepository;
            _iUserRepository = iUserRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        #region [ExportBill]
        public async Task<ApiResponseData<List<ExportBill>>> SearchPageAsync(
            int pageIndex, 
            int pageSize, 
            string key, 
            EExportBillStatus? status, 
            int? warehouseId,
            int? toWarehouseId,
            DateTime? start,
            DateTime? end,
            EExportBillCompleteStatus? completeStatus,
            string sortby, 
            string sorttype
            )
        {
            try
            {
                var data = await _iExportBillRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (key == null || x.Code.Contains(key) || x.Note.Contains(key))
                    && (status == null || x.Status == status)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                    && (toWarehouseId == null || x.ToWarehouseId == toWarehouseId)
                    && (start == null || x.DateBill.Date >= start.Value.Date)
                    && (end == null || x.DateBill.Date <= start.Value.Date)
                    && (completeStatus == null || x.CompleteStatus == completeStatus)
                    ,
                    OrderByExtention(sortby, sorttype));

                var idUsers = data.Data.Select(x => x.CreatedUser).ToList();
                var users = await _iUserRepository.SeachByIds(idUsers);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ExportBill>>();
            }
        }

        public async Task<ApiResponseData<ExportBill>> GetById(int id)
        {
            try
            {
                var data = await _iExportBillRepository.SearchOneAsync(x => x.Id == id);
                return new ApiResponseData<ExportBill>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.Get", ex.ToString());
                return new ApiResponseData<ExportBill>();
            }
        }

        public async Task<ApiResponseData<ExportBill>> Create(ExportBillCommand model)
        {
            try
            {
                var dlAdd = new ExportBill
                {
                    Code = model.Code,
                    DateBill = model.DateBill,
                    WarehouseId = model.WarehouseId,
                    Note = model.Note,
                    Files = model.Files,
                    Status = EExportBillStatus.Draft,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    ToWarehouseId = model.ToWarehouseId
                };
                await _iExportBillRepository.AddAsync(dlAdd);
                await _iExportBillRepository.Commit();

                await AddLog(dlAdd.Id, $"Lập nháp phiếu yêu cầu vật tư #{dlAdd.Id}", LogType.Insert);

                return new ApiResponseData<ExportBill>() { Data = dlAdd, Status = 1, Message = "Nhân viên điều phối lập yêu cầu vật tư thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.Create", ex.ToString());
                return new ApiResponseData<ExportBill>();
            }
        }

        public async Task<ApiResponseData<ExportBill>> Edit(ExportBillCommand model)
        {
            try
            {
                var dl = await _iExportBillRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ExportBill>() { Data = null, Status = 0 };
                }

                if(dl.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<ExportBill>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                dl.Code = model.Code;
                dl.DateBill = model.DateBill;
                dl.WarehouseId = model.WarehouseId;
                dl.Note = model.Note;
                dl.Files = model.Files;
                _iExportBillRepository.Update(dl);
                await _iExportBillRepository.Commit();
                await AddLog(dl.Id, $"Cập nhật thông tin #'{dl.Code }'", LogType.Update);
                return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = "Cập nhật thông tin thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.EditContent", ex.ToString());
                return new ApiResponseData<ExportBill>();
            }
        }

        public async Task<ApiResponseData<ExportBill>> Refuse(int id)
        {
            var dl = await _iExportBillRepository.SearchOneAsync(x => x.Id == id);
            if (dl == null)
            {
                return new ApiResponseData<ExportBill>() { Data = null, Status = 0 };
            }

            if (dl.Status == EExportBillStatus.NVDP_LapPhieu && UserInfo.RoleType == RoleManagerType.KTT_VTS)
            {
                dl.Status = EExportBillStatus.Draft;
                _iExportBillRepository.Update(dl);
                await _iExportBillRepository.Commit();
                await AddLog(dl.Id, $"Nhân viên kế toán VDS cừ chối yêu cầu lập lệnh xuất kho", LogType.Update);
                return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = $"Nhân viên kế toán VDS cừ chối yêu cầu lập lệnh xuất kho thành công" };
            }
            else if (dl.Status == EExportBillStatus.KT_LapLenhXuatKho && UserInfo.RoleType == RoleManagerType.ThuKho_VTS)
            {
                dl.Status = EExportBillStatus.NVDP_LapPhieu;
                _iExportBillRepository.Update(dl);
                await _iExportBillRepository.Commit();
                await AddLog( dl.Id, $"Thủ kho từ chối xuất phiếu trên hệ thống", LogType.Update);
                return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = $"Thủ kho từ chối xuất phiếu trên hệ thống thành công" };
            }
            else if (dl.Status == EExportBillStatus.TK_LapPhieuXuat && UserInfo.RoleType == RoleManagerType.BOD_VTS)
            {
                dl.Status = EExportBillStatus.KT_LapLenhXuatKho;
                _iExportBillRepository.Update(dl);
                await _iExportBillRepository.Commit();
                await AddLog( dl.Id, $"GĐ VTS từ chối xác nhận của thủ kho VTS", LogType.Update);
                return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = $"GĐ VTS từ chối xác nhận của thủ kho VTS thành công" };
            }
            else if (dl.Status == EExportBillStatus.BODVTS_XacNhanPhieuXuatKho && UserInfo.RoleType == RoleManagerType.ThuKho_VTS)
            {
                dl.Status = EExportBillStatus.TK_LapPhieuXuat;
                _iExportBillRepository.Update(dl);
                await _iExportBillRepository.Commit();
                await AddLog( dl.Id, $"Thủ kho từ chối yêu cầu xuất kho của GĐ VTS", LogType.Update);
                return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = $"Thủ kho từ chối yêu cầu xuất kho của GĐ VTS thành công" };
            }
            else
            {
                return new ApiResponseData<ExportBill>() { Data = dl, Status = 0, Message = $"Ở trạng thái này không thể từ chối xác nhận" };
            }
        }

        public async Task<ApiResponseData<ExportBill>> Approve(ExportBillApproveCommand model)
        {
            try
            {
                var dl = await _iExportBillRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ExportBill>() { Data = null, Status = 0 };
                }
                // KT ko có item
                var anyItem = await _iExportBillItemRepository.AnyAsync(x => x.ExportBillId == dl.Id);
                if(!anyItem)
                {
                    return new ApiResponseData<ExportBill>() { Status = 0, Message = "Phiếu không tồn tại vật tư nào không thể thay đổi trạng thái" };
                }
                // Kiểm tra vật tư có còn đủ không
                var listItems = await _iExportBillItemRepository.Search(x => x.ExportBillId == dl.Id);
                foreach(var item in listItems)
                {
                    var ton = await _iSuppliesRepository.GetInventory(item.SuppliesId, dl.WarehouseId);
                    if(ton < item.Count)
                    {
                        return new ApiResponseData<ExportBill>() { Status = 0, Message = "Kiểm tra lại vật tư, có vật tư số lượng không còn trong kho" };
                    }    
                }    

                if (dl.Status == EExportBillStatus.Draft && model.Status == EExportBillStatus.NVDP_LapPhieu)
                {
                    // Nhân viên điều phối lập phiếu trình kế toán
                    if (UserInfo.RoleType == RoleManagerType.Coordinator_VTS)
                    {
                        if (dl.CreatedUser != UserInfo.UserId)
                        {
                            return new ApiResponseData<ExportBill>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                        }

                        dl.Status = model.Status;
                        dl.NVDPConfirm = DateTime.Now;
                        dl.NVDPUser = UserInfo.UserId;
                        _iExportBillRepository.Update(dl);
                        await _iExportBillRepository.Commit();
                        await AddLog( dl.Id, $"Gửi phiếu yêu cầu vật tư cho kế toán VDS", LogType.Update);
                        return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = "Gửi phiếu yêu cầu vật tư cho kế toán VDS thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<ExportBill>() { Status = 0, Message = "Chỉ nhân viên điều phối mới được lập phiếu trình kế toán VDS" };
                    }
                }
                else if (dl.Status == EExportBillStatus.NVDP_LapPhieu && model.Status == EExportBillStatus.KT_LapLenhXuatKho)
                {
                    // Nhân viên kế toán kiểm tra đơn rồi lập lệnh xuất kho cho thủ kho
                    if (UserInfo.RoleType == RoleManagerType.KTT_VTS)
                    {
                        dl.Status = model.Status;
                        dl.KTConfirm = DateTime.Now;
                        dl.KTUser = UserInfo.UserId;
                        _iExportBillRepository.Update(dl);
                        await _iExportBillRepository.Commit();
                        await AddLog( dl.Id, $"Nhân viên kế toán VDS lập lệnh xuất kho cho thủ kho", LogType.Update);
                        return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = $"Nhân viên kế toán VDS lập lệnh xuất kho cho thủ kho thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<ExportBill>() { Status = 0, Message = "Chỉ kế toán trưởng VDS mới được xác nhận lập lệnh xuất kho" };
                    }
                }
                else if (dl.Status == EExportBillStatus.KT_LapLenhXuatKho && model.Status == EExportBillStatus.TK_LapPhieuXuat)
                {
                    // Thủ kho lập phiếu xuất đánh mã trình GĐ VTS
                    if (UserInfo.RoleType == RoleManagerType.ThuKho_VTS)
                    {
                        dl.Status = model.Status;
                        dl.TKConfirm = DateTime.Now;
                        dl.TKUser = UserInfo.UserId;
                        _iExportBillRepository.Update(dl);
                        await _iExportBillRepository.Commit();
                        await AddLog( dl.Id, $"Nhân viên thủ kho lập phiếu xuất đánh mã trình GĐ VTS", LogType.Update);
                        return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = $"Nhân viên thủ kho lập phiếu xuất trình GĐ VTS thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<ExportBill>() { Status = 0, Message = "Thủ kho VDS mới có thể lập phiếu xuất đánh mã trình GĐ VTS" };
                    }
                }
                else if (dl.Status == EExportBillStatus.TK_LapPhieuXuat && model.Status == EExportBillStatus.BODVTS_XacNhanPhieuXuatKho)
                {
                    // BOD xác nhận trên hệ thống để chuyển cho thủ kho xử lý
                    if (UserInfo.RoleType == RoleManagerType.BOD_VTS)
                    {
                        dl.Status = model.Status;
                        dl.BODVTSConfirm = DateTime.Now;
                        dl.BODVTSUser = UserInfo.UserId;
                        _iExportBillRepository.Update(dl);
                        await _iExportBillRepository.Commit();
                        await AddLog( dl.Id, $"BOD xác nhận trên hệ thống để chuyển cho thủ kho xử lý", LogType.Update);
                        return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = $"BOD xác nhận trên hệ thống để chuyển cho thủ kho xử lý thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<ExportBill>() { Status = 0, Message = "Chỉ GĐ VTS mới được thực hiện thao tác này" };
                    }
                }
                else if (dl.Status == EExportBillStatus.BODVTS_XacNhanPhieuXuatKho && model.Status == EExportBillStatus.TK_XacNhanXuatKho)
                {
                    // Thủ kho xác nhận xuất kho và trừ vật tư trên hệ thống
                    if (UserInfo.RoleType == RoleManagerType.ThuKho_VTS)
                    {
                        dl.Status = model.Status;
                        dl.TK2Confirm = DateTime.Now;
                        dl.TK2User = UserInfo.UserId;
                        _iExportBillRepository.Update(dl);
                        await _iExportBillRepository.Commit();
                        // Trừ số lượng trong kho
                        await _iSuppliesRepository.SuppliesWarehouseExport(dl.Id);
                        await AddLog( dl.Id, $"Thủ kho xác nhận xuất kho", LogType.Update);
                        return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = $"Thủ kho xác nhận xuất kho thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<ExportBill>() { Status = 0, Message = "Thủ kho mới có thể xác nhận xuất kho trên hệ thống" };
                    }
                }  
                else
                {
                    return new ApiResponseData<ExportBill>() { Status = 0, Message = "Bạn không có quyền thực hiện chức năng này" };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.Edit", ex.ToString());
                return new ApiResponseData<ExportBill>();
            }
        }

        public async Task<ApiResponseData<ExportBill>> Cancel(int id)
        {
            try
            {
                var dl = await _iExportBillRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ExportBill>() { Status = 0 , Message = "Dữ liệu không tồn tại vui lòng kiểm tra lại"};
                }

                if(dl.Status != EExportBillStatus.Draft)
                {
                    return new ApiResponseData<ExportBill>() { Status = 0, Message = "Trạng thái lập yêu cầu nháp mới có thể hủy được yêu cầu" };
                }

                if(UserInfo.RoleType != RoleManagerType.Coordinator_VTS)
                {
                    return new ApiResponseData<ExportBill>() { Status = 0, Message = "Chỉ có nhân viên điều phối mới có thể hủy được yêu cầu" };
                }

                if (dl.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<ExportBill>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                dl.Status = EExportBillStatus.Cancel;
                _iExportBillRepository.Update(dl);
                await _iExportBillRepository.Commit();

                await AddLog( id, $"Hủy phiếu yêu cầu vật tư #{id}", LogType.Update);

                return new ApiResponseData<ExportBill>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.Delete", ex.ToString());
                return new ApiResponseData<ExportBill>();
            }
        }

        private Func<IQueryable<ExportBill>, IOrderedQueryable<ExportBill>> OrderByExtention(string sortby, string sorttype) =>  sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "code" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.Code)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.Code)),
                "dateBill" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.DateBill)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.DateBill)),
                "total" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.Total)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.Total)),
                "warehouseId" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.WarehouseId)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.WarehouseId)),
                "note" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.Note)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.Note)),
                "files" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.Files)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.Files)),
                "status" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.Status)),
                "tK2Confirm" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.TK2Confirm)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.TK2Confirm)),
                "tK2User" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.TK2User)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.TK2User)),
                "tKConfirm" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.TKConfirm)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.TKConfirm)),
                "tKUser" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.TKUser)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.TKUser)),
                "bODVTSConfirm" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.BODVTSConfirm)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.BODVTSConfirm)),
                "bODVTSUser" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.BODVTSUser)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.BODVTSUser)),
                "completeConfirm" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.CompleteConfirm)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.CompleteConfirm)),
                "completeUser" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.CompleteUser)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.CompleteUser)),
                "createdDate" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "kT2Confirm" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.KT2Confirm)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.KT2Confirm)),
                "kT2User" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.KT2User)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.KT2User)),
                "kTConfirm" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.KTConfirm)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.KTConfirm)),
                "kTUser" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.KTUser)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.KTUser)),
                "nVDPConfirm" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.NVDPConfirm)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.NVDPConfirm)),
                "nVDPUser" => sorttype == "asc" ? EntityExtention<ExportBill>.OrderBy(m => m.OrderBy(x => x.NVDPUser)) : EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.NVDPUser)),
                _ => EntityExtention<ExportBill>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            };
        #endregion

        #region [ExportBillItem]
        public async Task<ApiResponseData<List<ExportBillItem>>> ExportBillItemSearchPageAsync(int pageIndex, int pageSize, int exportBillId)
        {
            try
            {
                var data  = await _iExportBillItemRepository.SearchPagedAsync(pageIndex, pageSize, x => x.ExportBillId == exportBillId, x=>x.OrderByDescending(m=>m.Id));
                var subb = await _iSuppliesRepository.Search(x => data.Data.Select(m => m.SuppliesId).Contains(x.Id));
                foreach(var item in data.Data)
                {
                    item.Supplies = subb.FirstOrDefault(x => x.Id == item.SuppliesId);
                }
                // Tính sum
                var count = await _iExportBillItemRepository.Sum(x => x.ExportBillId == exportBillId, x=>x.Count);
                var sum = await _iExportBillItemRepository.Sum(x => x.ExportBillId == exportBillId, x => x.Total);
                data.OutData = new { count, sum };
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.ExportBillItemSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ExportBillItem>>();
            }
        }

        public async Task<ApiResponseData<ExportBillItem>> ExportBillItemGetById(int id)
        {
            try
            {
                var data = await _iExportBillItemRepository.SearchOneAsync(x => x.Id == id);
                if(data != null)
                {
                    data.Supplies = await _iSuppliesRepository.SearchOneAsync(x => x.Id == data.SuppliesId);
                }    
                return new ApiResponseData<ExportBillItem>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.ExportBillItemGetById", ex.ToString());
                return new ApiResponseData<ExportBillItem>();
            }
        }

        public async Task<ApiResponseData<ExportBillItem>> ExportBillItemCreate(ExportBillItemCommand model)
        {
            try
            {
                if(model.ExportBillId <= 0)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Không tìm thấy đơn yêu cầu vật tư" };
                }

                var exportBill = await _iExportBillRepository.SearchOneAsync(x => x.Id == model.ExportBillId);
                if(exportBill == null)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Phiếu xuất không tồn tại" };
                }

                if(exportBill.Status == EExportBillStatus.TK_XacNhanXuatKho)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Phiếu đã xuất kho không thể sử dụng chức năng này" };
                }

                var ktTon = await _iExportBillItemRepository.AnyAsync(x => x.SuppliesId == model.SuppliesId && x.ExportBillId == model.ExportBillId);
                if(ktTon)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Không thể thêm vật tư, đã có vật tư tồn tại trong phiếu rồi" };
                }

                var ton = await _iSuppliesRepository.GetInventory(model.SuppliesId, exportBill.WarehouseId);
                if (ton < model.Count)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Số lượng trong kho không đủ để thực hiện" };
                }

                if (exportBill.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                var dl = new ExportBillItem
                {
                    ExportBillId = model.ExportBillId,
                    SuppliesId = model.SuppliesId,
                    Count = model.Count,
                    Price = model.Price,
                    Total = model.Count * model.Price,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };

                await _iExportBillItemRepository.AddAsync(dl);
                await _iExportBillItemRepository.Commit();
                await ExportBillUpdateTotal(dl.ExportBillId);
                await AddLog( dl.ExportBillId, $"Thêm vật tư #{model.SuppliesId} số lượng {model.Count}", LogType.Insert);

                return new ApiResponseData<ExportBillItem>() { Data = dl, Status = 1, Message = "Thêm vật tư vào phiếu thành công"};
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.ExportBillItemCreate", ex.ToString());
                return new ApiResponseData<ExportBillItem>();
            }
        }

        public async Task<ApiResponseData<ExportBillItem>> ExportBillItemEdit(ExportBillItemCommand model)
        {
            try
            {
                var dl = await _iExportBillItemRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0 };
                }

                var exportBill = await _iExportBillRepository.SearchOneAsync(x => x.Id == dl.ExportBillId);
                if (exportBill == null)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Phiếu xuất không tồn tại" };
                }

                if (exportBill.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                if (exportBill.Status == EExportBillStatus.TK_XacNhanXuatKho)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Phiếu đã xuất kho không thể sử dụng chức năng này" };
                }

                var ton = await _iSuppliesRepository.GetInventory(dl.SuppliesId, exportBill.WarehouseId);
                if (ton < model.Count)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Số lượng trong kho không đủ để thực hiện" };
                }

                dl.Count = model.Count;
                dl.Price = model.Price;
                dl.Total = model.Count * model.Price;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iExportBillItemRepository.Update(dl);
                await _iExportBillItemRepository.Commit();
                await ExportBillUpdateTotal(dl.ExportBillId);

                await AddLog( dl.ExportBillId, $"Cập nhật vật tư #{model.SuppliesId} số lượng {model.Count}", LogType.Update);

                return new ApiResponseData<ExportBillItem>() { Data = dl, Status = 1, Message = "Cập nhật vật tư thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.ExportBillItemEdit", ex.ToString());
                return new ApiResponseData<ExportBillItem>();
            }
        }
        public async Task<ApiResponseData<ExportBillItem>> ExportBillItemDelete(int id)
        {
            try
            {
                var dl = await _iExportBillItemRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0 };
                }

                var exportBill = await _iExportBillRepository.SearchOneAsync(x => x.Id == dl.ExportBillId);
                if (exportBill == null)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Phiếu xuất không tồn tại" };
                }

                if (exportBill.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                if (exportBill.Status == EExportBillStatus.TK_XacNhanXuatKho)
                {
                    return new ApiResponseData<ExportBillItem>() { Data = null, Status = 0, Message = "Phiếu đã xuất kho không thể sử dụng chức năng này" };
                }

                int billId = dl.ExportBillId;
                _iExportBillItemRepository.Delete(dl);
                await _iExportBillItemRepository.Commit();
                await ExportBillUpdateTotal(billId);
                await AddLog( dl.ExportBillId, $"Xóa vật tư #{dl.SuppliesId}", LogType.Update);

                return new ApiResponseData<ExportBillItem>() { Data = null, Status = 1, Message = "Xóa sản phẩm khỏi phiếu thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.ExportBillItemDelete", ex.ToString());
                return new ApiResponseData<ExportBillItem>();
            }
        }

        private async Task ExportBillUpdateTotal(int id)
        {
            var dl = await _iExportBillRepository.SearchOneAsync(x => x.Id == id);
            if(dl == null)
            {
                return;
            }
            var list = await _iExportBillItemRepository.Search(x => x.ExportBillId == id);
            if(list.Any())
            {
                dl.Count = list.Sum(x => x.Count);
                dl.Total = list.Sum(x => x.Total);
                _iExportBillRepository.Update(dl);
                _iExportBillRepository.Count();
            }    
        }
        #endregion

        #region [Báo cáo số lượng nhận không đủ]
        public async Task<ApiResponseData<ExportBill>> CompleteApprove(int id, EExportBillCompleteStatus cs)
        {
            try
            {
                var dl = await _iExportBillRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ExportBill>() { Data = null, Status = 0 };
                }

                if(dl.Status != EExportBillStatus.TK_XacNhanXuatKho)
                {
                    return new ApiResponseData<ExportBill>() { Status = 0, Message = "Phiếu này chưa được xuất kho thì không thể thực hiện thao tác này" };
                }
                // Điều phối mới đc xử lý
                if (UserInfo.RoleType == RoleManagerType.Coordinator_VTS && dl.CompleteStatus == EExportBillCompleteStatus.Default && cs != EExportBillCompleteStatus.ReImport)
                {
                    if (dl.CreatedUser != UserInfo.UserId)
                    {
                        return new ApiResponseData<ExportBill>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                    }

                    dl.CompleteStatus = cs;
                    _iExportBillRepository.Update(dl);
                    await _iExportBillRepository.Commit();
                    await AddLog( dl.Id, cs == EExportBillCompleteStatus.MatchingFigures ? $"Xác nhận nhận được đủ vật tư" : "Không xác nhận nhận đủ vật tư và đề nghị tái nhập", LogType.Update);
                    // nhập vào ảo cho điều phối viên nếu thông tin hợp lệ
                    if(cs == EExportBillCompleteStatus.MatchingFigures)
                    {
                        await _iSuppliesRepository.SuppliesWarehouseImport(dl.Id);
                    }
                    return new ApiResponseData<ExportBill>() { Data = dl, Status = 1, Message = "Cập nhật thông tin thành công" };
                }
                else  if (UserInfo.RoleType == RoleManagerType.ThuKho_VTS && dl.CompleteStatus == EExportBillCompleteStatus.WrongData && cs == EExportBillCompleteStatus.ReImport)
                {
                    dl.CompleteStatus = cs;
                    _iExportBillRepository.Update(dl);
                    await _iExportBillRepository.Commit();
                    await AddLog( dl.Id, $"Thủ kho tái nhập vật tư lại hệ thống", LogType.Update);
                    // Nhập lại data vào kho như cũ
                    await _iSuppliesRepository.SuppliesWarehouseReImport(dl.Id);

                    return new ApiResponseData<ExportBill>() { Status = 1, Message = "Xác nhận hoàn kho thành công" };
                }
                return new ApiResponseData<ExportBill>() { Status = 0, Message = "Bạn không được sử dụng chức năng này hoặc trạng thái đã thay đổi, vui lòng thử lại" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.EditContent", ex.ToString());
                return new ApiResponseData<ExportBill>();
            }
        }
        #endregion

        public async Task<ApiResponseData<List<Supplies>>> SuppliesSearchKey(string key, int warehouseId)
        {
            try
            {
                var data = await _iSuppliesRepository.SuppliesSearchKey(key, warehouseId);
                return new ApiResponseData<List<Supplies>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SuppliesSearchKey", ex.ToString());
                return new ApiResponseData<List<Supplies>>();
            }
        }

        #region [Upload file]
        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/ExportBill/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfff}_{Path.GetFileName(file.FileName)}";

                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);

                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ExportBillSevice.Upload", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }

        public FileStream View(string pathData, bool isPrivate)
        {
            try
            {
                if (isPrivate)
                {
                    string file = $"{_baseSettings.PrivateDataPath}{pathData}";
                    return new FileStream(file, FileMode.Open, FileAccess.Read);
                }
                else
                {
                    return new FileStream(pathData, FileMode.Open, FileAccess.Read);
                }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}
