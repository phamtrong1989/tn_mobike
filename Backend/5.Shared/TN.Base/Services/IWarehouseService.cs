﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IWarehouseService : IService<Warehouse>
    {
        Task<ApiResponseData<List<Warehouse>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype, int? employeeId, EWarehouseType? type);
        Task<ApiResponseData<Warehouse>> GetById(int id);
        Task<ApiResponseData<Warehouse>> Create(WarehouseCommand model);
        Task<ApiResponseData<Warehouse>> Edit(WarehouseCommand model);
        Task<ApiResponseData<Warehouse>> Delete(int id);
        Task<ApiResponseData<List<ApplicationUser>>> SearchByEmployee(string key);
        Task<ApiResponseData<List<ApplicationUser>>> SearchByEmployeeFree(string key);
        Task<ApiResponseData<List<SuppliesWarehouse>>> SuppliesSearchPageAsync(int pageIndex, int pageSize, string key, int warehouseId, ESuppliesType? type);
        Task<FileContentResult> ExportReport(
            string key,
            int? employeeId,
            EWarehouseType? type
        );

    }
    public class WarehouseService : IWarehouseService
    {
        private readonly ILogger _logger;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        private readonly IUserRepository _iUserRepository;
        private readonly ISuppliesRepository _suppliesRepository;
        private readonly IWarehouseRepository _iWarehouseRepository;
        private readonly ISuppliesWarehouseRepository _suppliesWarehouseRepository;
        private readonly IEntityBaseRepository<Warehouse> _warehouseRepository;
        private readonly ILogRepository _iLogRepository;

        public WarehouseService
        (
            ILogger<WarehouseService> logger,
            IWebHostEnvironment iWebHostEnvironment,

            IWarehouseRepository iWarehouseRepository,
            IUserRepository iUserRepository,
            ISuppliesWarehouseRepository suppliesWarehouseRepository,
            ISuppliesRepository suppliesRepository,
            IEntityBaseRepository<Warehouse> warehouseRepository,
            ILogRepository iLogRepository
        )
        {
            _logger = logger;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iUserRepository = iUserRepository;
            _iWarehouseRepository = iWarehouseRepository;
            _suppliesWarehouseRepository = suppliesWarehouseRepository;
            _suppliesRepository = suppliesRepository;
            _iUserRepository = iUserRepository;
            _suppliesRepository = suppliesRepository;
            _warehouseRepository = warehouseRepository;
            _suppliesWarehouseRepository = suppliesWarehouseRepository;
            _iLogRepository = iLogRepository;
        }

        public async Task<ApiResponseData<List<Warehouse>>> SearchPageAsync(int pageIndex, int pageSize, string key, string sortby, string sorttype, int? employeeId, EWarehouseType? type)
        {
            try
            {
                var data = await _iWarehouseRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (key == null || x.Name.Contains(key))
                        && (employeeId == null || x.EmployeeId == employeeId)
                        && (type == null || x.Type == type)
                    ,
                    OrderByExtention(sortby, sorttype));

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                ids.AddRange(data.Data.Select(x => x.EmployeeId).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.Employee = users.FirstOrDefault(x => x.Id == item.EmployeeId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("WarehouseSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Warehouse>>();
            }
        }

        public async Task<ApiResponseData<Warehouse>> GetById(int id)
        {
            try
            {
                var warehouse = await _iWarehouseRepository.SearchOneAsync(x => x.Id == id);
                var employee = await _iUserRepository.SearchOneAsync(x => x.Id == warehouse.EmployeeId);
                warehouse.Employee = employee;

                return new ApiResponseData<Warehouse>() { Data = warehouse, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WarehouseSevice.Get", ex.ToString());
                return new ApiResponseData<Warehouse>();
            }
        }

        public async Task<ApiResponseData<Warehouse>> Create(WarehouseCommand model)
        {
            try
            {
                var existEmployee = await _iWarehouseRepository.SearchOneAsync(x => x.EmployeeId == model.EmployeeId);
                if (existEmployee != null)
                {
                    return new ApiResponseData<Warehouse>() { Data = null, Status = 2, Message = "Nhân viên này đã quản lý 1 kho khác. Mỗi nhân viên chỉ được phép quản lý 1 kho!" };
                }

                var dlAdd = new Warehouse
                {
                    Name = model.Name,
                    Address = model.Address,
                    Type = model.Type,
                    EmployeeId = model.EmployeeId,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };
                await _iWarehouseRepository.AddAsync(dlAdd);
                await _iWarehouseRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới {dlAdd.Name}", LogType.Insert);

                return new ApiResponseData<Warehouse>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WarehouseSevice.Create", ex.ToString());
                return new ApiResponseData<Warehouse>();
            }
        }

        public async Task<ApiResponseData<Warehouse>> Edit(WarehouseCommand model)
        {
            try
            {
                var dl = await _iWarehouseRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Warehouse>() { Data = null, Status = 0 };
                }

                var existEmployee = await _iWarehouseRepository.SearchOneAsync(x => x.EmployeeId == model.EmployeeId && x.Id != model.Id && x.EmployeeId > 0);
                if (existEmployee != null)
                {
                    return new ApiResponseData<Warehouse>() { Data = null, Status = 2, Message = "Nhân viên này đã quản lý 1 kho khác. Mỗi nhân viên chỉ được phép quản lý 1 kho!" };
                }

                dl.Name = model.Name;
                dl.Address = model.Address;
                dl.Type = model.Type;
                dl.EmployeeId = model.EmployeeId;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iWarehouseRepository.Update(dl);
                await _iWarehouseRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật {dl.Name}", LogType.Update);

                return new ApiResponseData<Warehouse>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WarehouseSevice.Edit", ex.ToString());
                return new ApiResponseData<Warehouse>();
            }
        }

        public async Task<ApiResponseData<Warehouse>> Delete(int id)
        {
            try
            {
                var dl = await _iWarehouseRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Warehouse>() { Data = null, Status = 0 };
                }
                if (dl.Type == EWarehouseType.Virtual)
                {
                    return new ApiResponseData<Warehouse>() { Data = null, Status = 2 };
                }
                _iWarehouseRepository.Delete(dl);
                await _iWarehouseRepository.Commit();

                await AddLog(dl.Id, $"Xóa {dl.Name}", LogType.Delete);

                return new ApiResponseData<Warehouse>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WarehouseSevice.Delete", ex.ToString());
                return new ApiResponseData<Warehouse>();
            }
        }

        #region [ExportReport]
        public async Task<FileContentResult> ExportReport(string key, int? employeeId, EWarehouseType? type)
        {
            var warehouses = await _warehouseRepository.Search(
                x => (key == null || x.Name.Contains(key))
                    && (employeeId == null || x.EmployeeId == employeeId)
                    && (type == null || x.Type == type)
                ,
                null,
                x => new Warehouse
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var suppliesWarehouses = await _suppliesWarehouseRepository.Search(
                x => warehouses.Select(y => y.Id).Contains(x.WarehouseId)
                );

            var suppliess = await _suppliesRepository.Search(x => suppliesWarehouses.Select(y => y.SuppliesId).Distinct().Contains(x.Id)
                ,
                null,
                x => new Supplies
                {
                    Id = x.Id,
                    Code = x.Code,
                    Name = x.Name
                });

            foreach (var item in suppliesWarehouses)
            {
                item.Warehouse = warehouses.FirstOrDefault(x => x.Id == item.WarehouseId);
                item.Supplies = suppliess.FirstOrDefault(x => x.Id == item.SuppliesId);
            }

            var filePath = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(filePath)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Vật tư tồn kho");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelSuppliesInWarehouse(workSheet, suppliesWarehouses);
                MyExcel.Save();
            }
            //
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = Path.GetFileName(filePath)
            };
        }

        private void FormatExcelSuppliesInWarehouse(ExcelWorksheet worksheet, List<SuppliesWarehouse> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1, 6}, {2, 15}, {3, 40}, {4, 15}, {5, 15}, {6, 30}
            };

            SetWidthColumns(worksheet, configWidth);
            //SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO TỒN KHO VẬT TƯ";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:F5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã vật tư";
            worksheet.Cells["C5"].Value = "Tên vật tư";
            worksheet.Cells["D5"].Value = "Loại vật tư";
            worksheet.Cells["E5"].Value = "Số lượng tồn kho";
            worksheet.Cells["F5"].Value = "Tên kho";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Supplies?.Code;
                worksheet.Cells[rs, 3].Value = item.Supplies?.Name;
                worksheet.Cells[rs, 4].Value = item.Supplies?.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.Quantity;
                worksheet.Cells[rs, 6].Value = item.Warehouse?.Name;

                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:F{rs - 1}");
            worksheet.Cells[$"A6:F{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        } 
        #endregion

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchByEmployee(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _iUserRepository.SearchTop(10, x => (key == null || key == " " || x.Id == idSearch || x.DisplayName.Contains(key) || x.Code.Contains(key))  && x.RoleType == RoleManagerType.Coordinator_VTS);
                return new ApiResponseData<List<ApplicationUser>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WareHouseSevice.SearchByEmployee", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchByEmployeeFree(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var employeeHasWarehouseIds = (await _iWarehouseRepository.Search(x => x.EmployeeId > 0)).Select(x => x.EmployeeId);

                var data = await _iUserRepository.SearchTop(10, x => (key == null || key == " " || x.Id == idSearch || x.DisplayName.Contains(key) || x.Code.Contains(key))
                                                                        && x.RoleType == RoleManagerType.Coordinator_VTS
                                                                        && !employeeHasWarehouseIds.Contains(x.Id));
                return new ApiResponseData<List<ApplicationUser>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WareHouseSevice.SearchByEmployee", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }

        public async Task<ApiResponseData<List<SuppliesWarehouse>>> SuppliesSearchPageAsync(int pageIndex, int pageSize, string key, int warehouseId, ESuppliesType? type)
        {
            try
            {
                var data = await _suppliesWarehouseRepository.SearchPagedInventoryAsync(
                    pageIndex,
                    pageSize,
                    key,
                    warehouseId,
                    type);

                var suppliesIds = data.Data.Select(x => x.SuppliesId).ToList();

                var suppliess = await _suppliesRepository.Search(x => suppliesIds.Contains(x.Id));

                foreach (var item in data.Data)
                {
                    item.Supplies = suppliess.FirstOrDefault(x => x.Id == item.SuppliesId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("WarehouseSevice.SuppliesSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<SuppliesWarehouse>>();
            }
        }

        private Func<IQueryable<Warehouse>, IOrderedQueryable<Warehouse>> OrderByExtention(string sortby, string sorttype) => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<Warehouse>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Warehouse>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "name" => sorttype == "asc" ? EntityExtention<Warehouse>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<Warehouse>.OrderBy(m => m.OrderByDescending(x => x.Name)),
            "address" => sorttype == "asc" ? EntityExtention<Warehouse>.OrderBy(m => m.OrderBy(x => x.Address)) : EntityExtention<Warehouse>.OrderBy(m => m.OrderByDescending(x => x.Address)),
            "createdDate" => sorttype == "asc" ? EntityExtention<Warehouse>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Warehouse>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "createdUser" => sorttype == "asc" ? EntityExtention<Warehouse>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<Warehouse>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
            "updatedDate" => sorttype == "asc" ? EntityExtention<Warehouse>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Warehouse>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            "updatedUser" => sorttype == "asc" ? EntityExtention<Warehouse>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<Warehouse>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
            _ => EntityExtention<Warehouse>.OrderBy(m => m.OrderByDescending(x => x.Id)),
        };

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private string InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return filePath;
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}