﻿using Microsoft.Extensions.Logging;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IRedeemPointService : IService<RedeemPoint>
    {
        Task<ApiResponseData<List<RedeemPoint>>> SearchPageAsync(int pageIndex, int pageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<RedeemPoint>> GetById(int id);
        Task<ApiResponseData<RedeemPoint>> Create(RedeemPointCommand model);
        Task<ApiResponseData<RedeemPoint>> Edit(RedeemPointCommand model);
        Task<ApiResponseData<RedeemPoint>> Delete(int id);

    }
    public class RedeemPointService : IRedeemPointService
    {
        private readonly ILogger _logger;
        private readonly IUserRepository _iUserRepository;
        private readonly IRedeemPointRepository _iRedeemPointRepository;
        private readonly ILogRepository _iLogRepository;
        public RedeemPointService
        (
            IUserRepository iUserRepository,
            ILogger<RedeemPointService> logger,
            IRedeemPointRepository iRedeemPointRepository,
            ILogRepository iLogRepository
        )
        {
            _logger = logger;
            _iUserRepository = iUserRepository;
            _iRedeemPointRepository = iRedeemPointRepository;
            _iLogRepository = iLogRepository;
        }

        public async Task<ApiResponseData<List<RedeemPoint>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                var data = await _iRedeemPointRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new RedeemPoint
                    {
                        Id = x.Id,
                        Name = x.Name,
                        TripPoint = x.TripPoint,
                        ToPoint = x.ToPoint,
                        Note = x.Note,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        CreatedDate = x.CreatedDate,
                    });

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("RedeemPointSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<RedeemPoint>>();
            }
        }

        public async Task<ApiResponseData<RedeemPoint>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<RedeemPoint>() { Data = await _iRedeemPointRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RedeemPointSevice.Get", ex.ToString());
                return new ApiResponseData<RedeemPoint>();
            }
        }

        public async Task<ApiResponseData<RedeemPoint>> Create(RedeemPointCommand model)
        {
            try
            {
                var dlAdd = new RedeemPoint
                {
                    Name = model.Name,
                    TripPoint = model.TripPoint,
                    ToPoint = model.ToPoint,
                    Note = model.Note,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId

                };
                await _iRedeemPointRepository.AddAsync(dlAdd);
                await _iRedeemPointRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới '{dlAdd.Name}'", LogType.Insert);

                return new ApiResponseData<RedeemPoint>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RedeemPointSevice.Create", ex.ToString());
                return new ApiResponseData<RedeemPoint>();
            }
        }

        public async Task<ApiResponseData<RedeemPoint>> Edit(RedeemPointCommand model)
        {
            try
            {
                var dl = await _iRedeemPointRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<RedeemPoint>() { Data = null, Status = 0 };
                }

                dl.Name = model.Name;
                dl.TripPoint = model.TripPoint;
                dl.ToPoint = model.ToPoint;
                dl.Note = model.Note;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iRedeemPointRepository.Update(dl);
                await _iRedeemPointRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật '{dl.Name}'", LogType.Update);

                return new ApiResponseData<RedeemPoint>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RedeemPointSevice.Edit", ex.ToString());
                return new ApiResponseData<RedeemPoint>();
            }
        }
        public async Task<ApiResponseData<RedeemPoint>> Delete(int id)
        {
            try
            {
                var dl = await _iRedeemPointRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<RedeemPoint>() { Data = null, Status = 0 };
                }

                _iRedeemPointRepository.Delete(dl);
                await _iRedeemPointRepository.Commit();

                await AddLog(dl.Id, $"Xóa '{dl.Name}'", LogType.Delete);

                return new ApiResponseData<RedeemPoint>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RedeemPointSevice.Delete", ex.ToString());
                return new ApiResponseData<RedeemPoint>();
            }
        }

        private Func<IQueryable<RedeemPoint>, IOrderedQueryable<RedeemPoint>> OrderByExtention(string sortby, string sorttype) => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "name" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.Name)),
            "tripPoint" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.TripPoint)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.TripPoint)),
            "toPoint" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.ToPoint)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.ToPoint)),
            "note" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.Note)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.Note)),
            "createdUser" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
            "updatedDate" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            "updatedUser" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
            "createdDate" => sorttype == "asc" ? EntityExtention<RedeemPoint>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            _ => EntityExtention<RedeemPoint>.OrderBy(m => m.OrderByDescending(x => x.Id)),
        };

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}
