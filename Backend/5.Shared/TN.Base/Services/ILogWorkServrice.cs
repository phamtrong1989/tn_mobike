﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TN.Base.Models.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Domain.Model.Work;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Base.Services
{
    public interface ILogWorkService : IService<LogWork>
    {
        Task<ApiResponseData<List<LogWork>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? userId,
            EShift? shift,
            bool? isLate,
            string from,
            string to,
            string orderby,
            string ordertype);
        Task<ApiResponseData<LogWork>> GetById(int id);
        Task<ApiResponseData<LogWork>> Create(LogWorkCommand model);
        Task<ApiResponseData<LogWork>> Edit(LogWorkCommand model);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);
        Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key);
    }
    public class LogWorkService : ILogWorkService
    {
        private readonly ILogger _logger;
        private readonly BaseSettings _baseSettings;

        private readonly IUserRepository _iUserRepository;
        private readonly IEntityBaseRepository<LogWork> _iLogWorkRepository;

        public LogWorkService
        (
            ILogger<LogWorkService> logger,
            ILogRepository iLogRepository,
            IOptions<BaseSettings> baseSettings,

            IUserRepository iUserRepository,
            IEntityBaseRepository<LogWork> iLogWorkRepository
        )
        {
            _logger = logger;
            _baseSettings = baseSettings.Value;

            _iUserRepository = iUserRepository;
            _iLogWorkRepository = iLogWorkRepository;
        }

        public async Task<ApiResponseData<List<LogWork>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? userId,
            EShift? shift,
            bool? isLate,
            string from,
            string to,
            string sortby,
            string sorttype
        )
        {
            try
            {
                var data = await _iLogWorkRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (userId == null || x.CreatedUser == userId)
                            && (shift == null || x.Shift == shift)
                            && (isLate == null || x.IsLate == isLate)
                            && (from == null || x.CreatedDate >= Convert.ToDateTime(from))
                            && (to == null || x.CreatedDate < Convert.ToDateTime(to).AddDays(1))
                            && (key == null || x.Title.Contains(key) || x.Description.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype));


                var userIds = data.Data.Select(x => x.CreatedUser).Distinct().ToList();
                userIds.AddRange(data.Data.Select(x => x.UpdatedUser).Distinct());
                var users = await _iUserRepository.SeachByIds(userIds);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("LogWorkSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<LogWork>>();
            }
        }

        public async Task<ApiResponseData<LogWork>> GetById(int id)
        {
            try
            {
                var data = await _iLogWorkRepository.SearchOneAsync(x => x.Id == id);
                return new ApiResponseData<LogWork>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LogWorkSevice.Get", ex.ToString());
                return new ApiResponseData<LogWork>();
            }
        }

        public async Task<ApiResponseData<LogWork>> Create(LogWorkCommand model)
        {
            try
            {
                var dlAdd = new LogWork
                {
                    Shift = model.Shift,
                    Role = model.Role,
                    Title = model.Title,
                    Description = model.Description,
                    Files = model.Files,
                    LogDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    UpdatedDate = DateTime.Now,
                    UpdatedUser = UserInfo.UserId
                };

                var deadLine = DateTime.Today;
                switch (model.Shift)
                {
                    case EShift.S1:
                        {
                            deadLine.AddHours(13);
                        };
                        break;
                    case EShift.S2:
                        {
                            deadLine.AddHours(21);
                        };
                        break;
                    case EShift.S3:
                        {
                            deadLine.AddHours(5);
                            if (DateTime.Now.Hour < 5)
                            {
                                dlAdd.LogDate = DateTime.Now.AddDays(-1);
                            }
                        };
                        break;
                    default:
                        {
                            deadLine.AddHours(20);
                        };
                        break;
                }

                dlAdd.IsLate = DateTime.Compare(DateTime.Now, deadLine) > 0;

                //
                await _iLogWorkRepository.AddAsync(dlAdd);
                await _iLogWorkRepository.Commit();

                return new ApiResponseData<LogWork>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LogWorkSevice.Create", ex.ToString());
                return new ApiResponseData<LogWork>();
            }
        }

        public async Task<ApiResponseData<LogWork>> Edit(LogWorkCommand model)
        {
            try
            {
                var dl = await _iLogWorkRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<LogWork>() { Data = null, Status = 0 };
                }

                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.Title = model.Title;
                dl.Description = model.Description;
                dl.Files = model.Files;

                _iLogWorkRepository.Update(dl);
                await _iLogWorkRepository.Commit();

                return new ApiResponseData<LogWork>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LogWorkSevice.Edit", ex.ToString());
                return new ApiResponseData<LogWork>();
            }
        }

        private Func<IQueryable<LogWork>, IOrderedQueryable<LogWork>> OrderByExtention(string sortby, string sorttype)
        => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<LogWork>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<LogWork>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "description" => sorttype == "asc" ? EntityExtention<LogWork>.OrderBy(m => m.OrderBy(x => x.Description)) : EntityExtention<LogWork>.OrderBy(m => m.OrderByDescending(x => x.Description)),
            "createdDate" => sorttype == "asc" ? EntityExtention<LogWork>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<LogWork>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "updatedDate" => sorttype == "asc" ? EntityExtention<LogWork>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<LogWork>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            _ => EntityExtention<LogWork>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
        };

        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/LogWork/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfffffff}_{Path.GetFileName(file.FileName)}";
                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);
                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                    System.Threading.Thread.Sleep(100);
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LogWorkSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key)
        {
            try
            {
                var data = await _iUserRepository.SearchTop(10, x => key == null || key == " " || x.Code.Contains(key) || x.FullName.Contains(key) || x.PhoneNumber.Contains(key) || x.Email.Contains(key));
                return new ApiResponseData<List<ApplicationUser>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LogWorkSevice.SearchByEmployee", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }
    }
}