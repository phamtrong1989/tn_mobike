﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface ISuppliesService : IService<Supplies>
    {
        Task<ApiResponseData<List<Supplies>>> SearchPageAsync(int pageIndex, int PageSize, string key, int? type, string orderby, string ordertype);
        Task<ApiResponseData<Supplies>> GetById(int id);
        Task<ApiResponseData<Supplies>> Create(SuppliesCommand model);
        Task<ApiResponseData<Supplies>> Edit(SuppliesCommand model);
        Task<ApiResponseData<Supplies>> Delete(int id);
        Task<ApiResponseData<List<Supplies>>> SearchBySupplies(string key);
        Task<FileContentResult> ExportReport(string key, int? type);

    }
    public class SuppliesService : ISuppliesService
    {
        private readonly ILogger _logger;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IUserRepository _iUserRepository;
        private readonly ISuppliesRepository _suppliesRepository;
        private readonly IEntityBaseRepository<Warehouse> _warehouseRepository;
        private readonly IEntityBaseRepository<SuppliesWarehouse> _suppliesWarehouseRepository;
        private readonly ILogRepository _iLogRepository;
        public SuppliesService
        (
            ILogger<SuppliesService> logger,
            IWebHostEnvironment iWebHostEnvironment,

            IUserRepository iUserRepository,
            ISuppliesRepository iSuppliesRepository,
            IEntityBaseRepository<SuppliesWarehouse> suppliesWarehouseRepository,
            IEntityBaseRepository<Warehouse> warehouseRepository,
            ILogRepository iLogRepository
        )
        {
            _logger = logger;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iUserRepository = iUserRepository;
            _suppliesRepository = iSuppliesRepository;
            _suppliesWarehouseRepository = suppliesWarehouseRepository;
            _warehouseRepository = warehouseRepository;
            _iLogRepository = iLogRepository;
        }

        public async Task<ApiResponseData<List<Supplies>>> SearchPageAsync(int pageIndex, int PageSize, string key, int? type, string sortby, string sorttype)
        {
            try
            {
                var data = await _suppliesRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => (key == null || x.Name.Contains(key) || x.Code.Contains(key) || x.Descriptions.Contains(key))
                        && (type == null || x.Type == (ESuppliesType)type)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Supplies
                    {
                        Id = x.Id,
                        Code = x.Code,
                        Name = x.Name,
                        Descriptions = x.Descriptions,
                        Quantity = x.Quantity,
                        Unit = x.Unit,
                        Type = x.Type,
                        Price = x.Price,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                    });

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                var suppliesWarehouses = await _suppliesWarehouseRepository.Search(x => data.Data.Select(y => y.Id).Contains(x.SuppliesId));
                var warehouses = await _warehouseRepository.Search(x => suppliesWarehouses.Select(y => y.WarehouseId).Contains(x.Id));

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.SuppliesWarehouses = suppliesWarehouses.Where(x => x.SuppliesId == item.Id)
                                                                .Select(x => new SuppliesWarehouse
                                                                {
                                                                    Id = x.Id,
                                                                    WarehouseId = x.WarehouseId,
                                                                    SuppliesId = x.SuppliesId,
                                                                    Quantity = x.Quantity,
                                                                    Warehouse = warehouses.FirstOrDefault(y => y.Id == x.WarehouseId)
                                                                });
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("SuppliesSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Supplies>>();
            }
        }

        public async Task<ApiResponseData<Supplies>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Supplies>() { Data = await _suppliesRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("SuppliesSevice.Get", ex.ToString());
                return new ApiResponseData<Supplies>();
            }
        }

        public async Task<ApiResponseData<Supplies>> Create(SuppliesCommand model)
        {
            try
            {
                var dlAdd = new Supplies
                {
                    Code = model.Code,
                    Name = model.Name,
                    Descriptions = model.Descriptions,
                    Unit = model.Unit,
                    Type = (ESuppliesType)model.Type,
                    Price = model.Price,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };
                await _suppliesRepository.AddAsync(dlAdd);
                await _suppliesRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới '{dlAdd.Name}'", LogType.Insert);

                return new ApiResponseData<Supplies>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("SuppliesSevice.Create", ex.ToString());
                return new ApiResponseData<Supplies>();
            }
        }

        public async Task<ApiResponseData<Supplies>> Edit(SuppliesCommand model)
        {
            try
            {
                var dl = await _suppliesRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Supplies>() { Data = null, Status = 0 };
                }

                dl.Code = model.Code;
                dl.Name = model.Name;
                dl.Descriptions = model.Descriptions;
                dl.Unit = model.Unit;
                dl.Type = (ESuppliesType)model.Type;
                dl.Price = model.Price;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _suppliesRepository.Update(dl);
                await _suppliesRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật '{dl.Name}'", LogType.Update);

                return new ApiResponseData<Supplies>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("SuppliesSevice.Edit", ex.ToString());
                return new ApiResponseData<Supplies>();
            }
        }

        public async Task<ApiResponseData<Supplies>> Delete(int id)
        {
            try
            {
                var dl = await _suppliesRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Supplies>() { Data = null, Status = 0 };
                }

                _suppliesRepository.Delete(dl);
                await _suppliesRepository.Commit();

                await AddLog(dl.Id, $"Xóa '{dl.Name}'", LogType.Delete);

                return new ApiResponseData<Supplies>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("SuppliesSevice.Delete", ex.ToString());
                return new ApiResponseData<Supplies>();
            }
        }

        #region [ExportReport]
        public async Task<FileContentResult> ExportReport(string key, int? type)
        {
            var suppliess = await _suppliesRepository.Search(
                x => (key == null || x.Name.Contains(key) || x.Code.Contains(key) || x.Descriptions.Contains(key))
                    && (type == null || x.Type == (ESuppliesType)type)
                );

            var filePath = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(filePath)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Vật tư");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelSupplies(workSheet, suppliess);
                MyExcel.Save();
            }
            //
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = Path.GetFileName(filePath)
            };
        }

        private void FormatExcelSupplies(ExcelWorksheet worksheet, List<Supplies> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1, 6}, {2, 12}, {3, 40}, {4, 13}, {5, 50}
            };

            SetWidthColumns(worksheet, configWidth);
            //SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 9, 10, 11, 12 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO THÔNG TIN CHI TIẾT VẬT TƯ";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:E5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã vật tư";
            worksheet.Cells["C5"].Value = "Tên vật tư";
            worksheet.Cells["D5"].Value = "Loại vật tư";
            worksheet.Cells["E5"].Value = "Mô tả";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Code;
                worksheet.Cells[rs, 3].Value = item.Name;
                worksheet.Cells[rs, 4].Value = item.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.Descriptions;

                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:E{rs - 1}");
            worksheet.Cells[$"A6:E{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private Func<IQueryable<Supplies>, IOrderedQueryable<Supplies>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<Supplies>, IOrderedQueryable<Supplies>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "code":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.Code)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Code));
                    break;
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                case "descriptions":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.Descriptions)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Descriptions));
                    break;
                case "quantity":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.Quantity)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Quantity));
                    break;
                case "unit":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.Unit)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Unit));
                    break;
                case "type":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Type));
                    break;
                case "price":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.Price)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Price));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "createdUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser));
                    break;
                case "updatedDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate));
                    break;
                case "updatedUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<Supplies>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser));
                    break;

                default:
                    functionOrder = EntityExtention<Supplies>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }

        public async Task<ApiResponseData<List<Supplies>>> SearchBySupplies(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _suppliesRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.Code.Contains(key) || x.Name.Contains(key) || x.Descriptions.Contains(key));

                return new ApiResponseData<List<Supplies>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("SuppliesSevice.SearchBySupplies", ex.ToString());
                return new ApiResponseData<List<Supplies>>();
            }
        }

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private string InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return filePath;
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}