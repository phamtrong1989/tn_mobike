﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Domain.Model;
using PT.Base;

namespace TN.Base.Services
{
    public interface ITransactionService : IService<Transaction>
    {
        Task<ApiResponseData<List<Transaction>>> SearchPageAsync(int pageIndex, int pageSize, string key, int? investorId, int? projectId, int? customerGroupId, EBookingStatus? status, int? stationIn, int? stationOut, string from, string to, int? accountId, ETicketType? ticketPrice_TicketType, bool? isDebt, string orderby, string ordertype);
        Task<ApiResponseData<Transaction>> GetById(int id);
        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);
        Task<ApiResponseData<Transaction>> Refund(int id, string note);
    }
    public class TransactionService : ITransactionService
    {
        private readonly ILogger _logger;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IBikeRepository _iBikeRepository;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly ICustomerGroupRepository _customerGroupRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IOptions<LogSettings> _logSettings;
        private readonly IGPSDataRepository _iGPSDataRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly IWalletTransactionService _iWalletTransactionService;

        public TransactionService
        (
            ILogger<TransactionService> logger,
            ITransactionRepository iTransactionRepository,
            IAccountRepository iAccountRepository,
            IBikeRepository iBikeRepository,
            ITicketPriceRepository iTicketPriceRepository,
            ICustomerGroupRepository customerGroupRepository,
            IDockRepository iDockRepository,
            IUserRepository iUserRepository,
            IOptions<LogSettings> logSettings,
            IGPSDataRepository iGPSDataRepository,
            IWalletTransactionRepository iWalletTransactionRepository,
            IWalletTransactionService iWalletTransactionService
        )
        {
            _logger = logger;
            _iTransactionRepository = iTransactionRepository;
            _iAccountRepository = iAccountRepository;
            _iBikeRepository = iBikeRepository;
            _iTicketPriceRepository = iTicketPriceRepository;
            _customerGroupRepository = customerGroupRepository;
            _iDockRepository = iDockRepository;
            _iUserRepository = iUserRepository;
            _logSettings = logSettings;
            _iGPSDataRepository = iGPSDataRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iWalletTransactionService = iWalletTransactionService;
        }

        public async Task<ApiResponseData<List<Transaction>>> SearchPageAsync(
            int pageIndex, 
            int pageSize, 
            string key, 
            int? investorId, 
            int? projectId,
            int? customerGroupId, 
            EBookingStatus? status,
            int? stationIn,
            int? stationOut, 
            string from, 
            string to, 
            int? accountId,
            ETicketType? ticketPrice_TicketType,
            bool? isDebt,
            string sortby, 
            string sorttype
            )
        {
            try
            {
                var data = await _iTransactionRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    key,
                    x =>
                         (investorId == null || x.InvestorId == investorId)
                         && (accountId == null || x.AccountId == accountId)
                         && (ticketPrice_TicketType == null || x.TicketPrice_TicketType == ticketPrice_TicketType)
                         && (projectId == null || x.ProjectId == projectId)
                         && (customerGroupId == null || x.CustomerGroupId == customerGroupId)
                         && (status == null || x.Status == status)
                         && (isDebt == null || (isDebt == true && x.Status == EBookingStatus.Debt) || (isDebt == false && x.Status != EBookingStatus.Debt))
                         && (stationIn == null || x.StationIn == stationIn)
                         && (stationOut == null || x.StationOut == stationOut)
                         && (from == null || x.EndTime >= Convert.ToDateTime(from))
                         && (to == null || x.EndTime < Convert.ToDateTime(to).AddDays(1))
                    ,
                    OrderByExtention(sortby, sorttype));

                var bikes = await _iBikeRepository.Search(x => data.Data.Select(x => x.BikeId).Contains(x.Id));
                var accounts = await _iAccountRepository.Search(x => data.Data.Select(x => x.AccountId).Contains(x.Id));
                var customerGroups = await _customerGroupRepository.Search(x => data.Data.Select(x => x.CustomerGroupId).Contains(x.Id));
                var docks = await _iDockRepository.Search(x => data.Data.Select(x => x.DockId).Contains(x.Id));
                var userIds = data.Data.Where(x => x.UpdatedUser != null && x.UpdatedUser > 0).Select(x => x.UpdatedUser ?? 0).ToList();
                var users = await _iUserRepository.Search(x => userIds.Contains(x.Id), null, x => new ApplicationUser { Id = x.Id, DisplayName = x.DisplayName, UserName = x.UserName });
                foreach (var item in data.Data)
                {
                    item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                    item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                    item.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);
                    if(item.UpdatedUser != null && item.UpdatedUser > 0)
                    {
                        item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    }    
                    if (item.Bike != null)
                    {
                        item.Bike.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                    }    
                    
                     var  ticketPrice = new TicketPrice()
                        {
                            Id = item.TicketPriceId,
                            CustomerGroupId = item.CustomerGroupId,
                            BlockPerMinute = item.TicketPrice_BlockPerMinute,
                            AllowChargeInSubAccount = item.TicketPrice_AllowChargeInSubAccount,
                            BlockPerViolation = item.TicketPrice_BlockPerViolation,
                            BlockViolationValue = item.TicketPrice_BlockViolationValue,
                            IsDefault = item.TicketPrice_IsDefault,
                            TicketType = item.TicketPrice_TicketType,
                            TicketValue = item.TicketPrice_TicketValue,
                            LimitMinutes = item.TicketPrice_LimitMinutes,
                            Name = GetDisplayName(item.TicketPrice_TicketType)
                     };

                    item.TicketPrice = ticketPrice;
                    item.TicketPrice_Note = _iTransactionRepository.GetTicketPriceString(ticketPrice);

                }

                if (accountId != null && accountId > 0)
                {
                    data.OutData = await _iAccountRepository.SearchOneAsync(x => x.Id == accountId);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("TransactionSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Transaction>>();
            }
        }

        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TransactionSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }

        public async Task<ApiResponseData<Transaction>> GetById(int id)
        {
            try
            {
                var data = await _iTransactionRepository.SearchOneAsync(x => x.Id == id);
                data.Account = await _iAccountRepository.SearchOneAsync(x => x.Id.Equals(data.AccountId));
                data.Bike = await _iBikeRepository.SearchOneAsync(x => x.Id.Equals(data.BikeId));
                data.CustomerGroup = await _customerGroupRepository.SearchOneAsync(x => x.Id.Equals(data.CustomerGroupId));
                if(data.UpdatedUser!=null && data.UpdatedUser > 0)
                {
                    data.UpdatedUserObject = (await _iUserRepository.Search(x => x.Id == data.UpdatedUser, null, x => new ApplicationUser { Id = x.Id, DisplayName = x.DisplayName, UserName = x.UserName })).FirstOrDefault();
                }
                var ticketInfo = new TicketPrice()
                {
                    CustomerGroupId = data.CustomerGroupId,
                    BlockPerMinute = data.TicketPrice_BlockPerMinute,
                    AllowChargeInSubAccount = data.TicketPrice_AllowChargeInSubAccount,
                    BlockPerViolation = data.TicketPrice_BlockPerViolation,
                    BlockViolationValue = data.TicketPrice_BlockViolationValue,
                    IsDefault = data.TicketPrice_IsDefault,
                    TicketType = data.TicketPrice_TicketType,
                    TicketValue = data.TicketPrice_TicketValue,
                    LimitMinutes = data.TicketPrice_LimitMinutes,
                    Name = GetDisplayName(data.TicketPrice_TicketType)
                };

                data.TicketPrice = ticketInfo;
                data.TicketPrice_Note = _iTransactionRepository.GetTicketPriceString(ticketInfo);
                try
                {
                    var gpsData = _iGPSDataRepository.GetTransactionGPS(_logSettings.Value, id, data.EndTime ?? data.StartTime);
                    if (gpsData != null)
                    {
                        data.GPS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GPSData>>(gpsData.Data);
                    }
                }
                catch { }
                return new ApiResponseData<Transaction>() { Data = data, Status = 1 };

            }
            catch (Exception ex)
            {
                _logger.LogError("TransactionSevice.Get", ex.ToString());
                return new ApiResponseData<Transaction>();
            }
        }

        public async Task<ApiResponseData<Transaction>> Refund(int id, string note)
        {
            try
            {
                var data = await _iTransactionRepository.SearchOneAsync(x => x.Id == id);
                if(data == null)
                {
                    return new ApiResponseData<Transaction>() { Status = 0 };
                }

                if(data.TotalPrice <= 0)
                {
                    return new ApiResponseData<Transaction>() { Status = 4, Message = "Hoàn điểm thất bại, chỉ hoàn điểm được chuyến có chi phí lớn hơn 0" };
                }

                if((DateTime.Now - data.EndTime.Value).TotalMinutes > 2880)
                {
                    return new ApiResponseData<Transaction>() { Status = 4, Message = "Hoàn điểm thất bại, chỉ hoàn điểm được những chuyến đi kết thúc không quá 48 giờ" };
                }

                data.Note = note;
                data.UpdatedDate = DateTime.Now;
                data.UpdatedUser = UserInfo.UserId;

                // Trường hợp ko nợ cước
                if (data.Status == EBookingStatus.End || data.Status == EBookingStatus.EndByAdmin)
                {
                    var kt = await _iWalletTransactionService.Create(new WalletTransactionCommand
                    {
                        AccountId = data.AccountId,
                        Note = note,
                        Type = EWalletTransactionType.PointRefund,
                        Amount = data.ChargeInAccount,
                        SubAmount = data.ChargeInSubAccount,
                        PaymentGroup = EPaymentGroup.Default,
                        TransactionCode = data.TransactionCode
                    });

                    if(kt.Status == 1)
                    {
                        data.Status = EBookingStatus.Refund;
                        _iTransactionRepository.Update(data);
                        await _iTransactionRepository.Commit();
                        return new ApiResponseData<Transaction>() { Data = data, Status = 1, Message = "Hoàn điểm cho khách thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<Transaction>() { Data = data, Status = 0, Message = "Hoàn điểm thất bại vui lòng thử lại" };
                    }
                } 
                else if(data.Status == EBookingStatus.Debt)
                {
                    var kt = await _iWalletTransactionService.Create(new WalletTransactionCommand
                    {
                        AccountId = data.AccountId,
                        Note = note,
                        Type = EWalletTransactionType.PointRefund,
                        Amount = data.EstimateChargeInAccount,
                        SubAmount = data.EstimateChargeInSubAccount,
                        PaymentGroup = EPaymentGroup.Default,
                        TransactionCode = data.TransactionCode
                    });

                    if (kt.Status == 1)
                    {
                        data.Status = EBookingStatus.Refund;
                        _iTransactionRepository.Update(data);
                        await _iTransactionRepository.Commit();
                        return new ApiResponseData<Transaction>() { Data = data, Status = 1, Message = "Hoàn điểm cho khách thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<Transaction>() { Data = data, Status = 0, Message = "Hoàn điểm thất bại vui lòng thử lại" };
                    }
                }   
                else
                {
                    return new ApiResponseData<Transaction>() { Status = 3, Message = "Hoàn điểm thất bại, chỉ giao dịch đã hoàn thành, nợ cước và kết thúc bởi quản trị mới có thể hoàn" };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("TransactionSevice.Refund " + ex.ToString());
                return new ApiResponseData<Transaction>();
            }
        }

        private Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "stationOut":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.StationOut)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.StationOut));
                    break;
                case "bikeId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.BikeId)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.BikeId));
                    break;
                case "dockId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.DockId)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.DockId));
                    break;
                case "accountId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.AccountId));
                    break;
                case "customerGroupId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.CustomerGroupId)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.CustomerGroupId));
                    break;
                case "transactionCode":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.TransactionCode)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.TransactionCode));
                    break;
                case "vote":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.Vote)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.Vote));
                    break;
                case "status":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.Status));
                    break;
                case "chargeInAccount":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.ChargeInAccount)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.ChargeInAccount));
                    break;
                case "chargeInSubAccount":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.ChargeInSubAccount)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.ChargeInSubAccount));
                    break;
                case "ticketPriceId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.TicketPriceId)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.TicketPriceId));
                    break;
                case "dateOfPayment":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.DateOfPayment)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.DateOfPayment));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                default:
                    functionOrder = EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }

        private static string GetDisplayName(Enum enumValue)
        {
            return enumValue.GetType().GetMember(enumValue.ToString())
                .First()
                .GetCustomAttribute<DisplayAttribute>()
                .Name;
        }
    }
}
