using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IAccountRatingItemService : IService<AccountRatingItem>
    {
        Task<ApiResponseData<List<AccountRatingItem>>> SearchPageAsync(
            int pageIndex,
            int PageSize,
            string key,
            string orderby,
            string ordertype,
            string code
        );
        Task<ApiResponseData<AccountRatingItem>> GetById(int id);
        Task<ApiResponseData<AccountRatingItem>> Create(AccountRatingItemCommand model);
        Task<ApiResponseData<AccountRatingItem>> Edit(AccountRatingItemCommand model);
        Task<ApiResponseData<AccountRatingItem>> Delete(int id);
        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);

    }
    public class AccountRatingItemService : IAccountRatingItemService
    {
        private readonly ILogger _logger;
        private readonly IAccountRatingItemRepository _iAccountRatingItemRepository;
        private readonly IAccountRepository _iAccountRepository;
        public AccountRatingItemService
        (
            ILogger<AccountRatingItemService> logger,
            IAccountRatingItemRepository iAccountRatingItemRepository,
            IAccountRepository iAccountRepository
        )
        {
            _logger = logger;
            _iAccountRatingItemRepository = iAccountRatingItemRepository;
            _iAccountRepository = iAccountRepository;
        }

        public async Task<ApiResponseData<List<AccountRatingItem>>> SearchPageAsync(
            int pageIndex,
            int PageSize,
            string key,
            string sortby,
            string sorttype,
            string code
        )
        {
            try
            {
                var data = await _iAccountRatingItemRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => (x.Code == code)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new AccountRatingItem
                    {
                        Id = x.Id,
                        Code = x.Code,
                        ProjectId = x.ProjectId,
                        Order = x.Order,
                        AccountId = x.AccountId,
                        Value = x.Value,
                        UpdatedDate = x.UpdatedDate,
                        Status = x.Status,
                    });
                //
                var accounts = await _iAccountRepository.Search(
                    x => data.Data.Select(x => x.AccountId).Distinct().Contains(x.Id),
                    null,
                    x => new Account
                    {
                        Id = x.Id,
                        Code = x.Code,
                        FullName = x.FullName,
                        Phone = x.Phone,
                        Status = x.Status,
                        Type = x.Type
                    });
                //
                foreach (var item in data.Data)
                {
                    item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingItemSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<AccountRatingItem>>();
            }
        }

        public async Task<ApiResponseData<AccountRatingItem>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<AccountRatingItem>() { Data = await _iAccountRatingItemRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingItemSevice.Get", ex.ToString());
                return new ApiResponseData<AccountRatingItem>();
            }
        }
        public async Task<ApiResponseData<AccountRatingItem>> Create(AccountRatingItemCommand model)
        {
            try
            {
                var dlAdd = new AccountRatingItem
                {
                    Id = model.Id,
                    Code = model.Code,
                    ProjectId = 0,
                    Order = model.Order,
                    AccountId = model.AccountId,
                    Value = model.Value,
                    UpdatedDate = DateTime.Now,
                    Status = false
                };
                await _iAccountRatingItemRepository.AddAsync(dlAdd);
                await _iAccountRatingItemRepository.Commit();

                return new ApiResponseData<AccountRatingItem>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingItemSevice.Create", ex.ToString());
                return new ApiResponseData<AccountRatingItem>();
            }
        }
        public async Task<ApiResponseData<AccountRatingItem>> Edit(AccountRatingItemCommand model)
        {
            try
            {
                var dl = await _iAccountRatingItemRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<AccountRatingItem>() { Data = null, Status = 0 };
                }

                dl.Order = model.Order;
                dl.UpdatedDate = DateTime.Now;

                _iAccountRatingItemRepository.Update(dl);
                await _iAccountRatingItemRepository.Commit();
                return new ApiResponseData<AccountRatingItem>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingItemSevice.Edit", ex.ToString());
                return new ApiResponseData<AccountRatingItem>();
            }
        }
        public async Task<ApiResponseData<AccountRatingItem>> Delete(int id)
        {
            try
            {
                var dl = await _iAccountRatingItemRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<AccountRatingItem>() { Data = null, Status = 0 };
                }

                _iAccountRatingItemRepository.Delete(dl);
                await _iAccountRatingItemRepository.Commit();
                return new ApiResponseData<AccountRatingItem>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingItemSevice.Delete", ex.ToString());
                return new ApiResponseData<AccountRatingItem>();
            }
        }
        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                _ = int.TryParse(key, out int idSearch);
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }

        private Func<IQueryable<AccountRatingItem>, IOrderedQueryable<AccountRatingItem>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<AccountRatingItem>, IOrderedQueryable<AccountRatingItem>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "code":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.Code)) : EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderByDescending(x => x.Code));
                    break;
                case "projectId":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderByDescending(x => x.ProjectId));
                    break;
                case "order":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.Order)) : EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderByDescending(x => x.Order));
                    break;
                case "accountId":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderByDescending(x => x.AccountId));
                    break;
                case "value":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.Value)) : EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderByDescending(x => x.Value));
                    break;
                case "updatedDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate));
                    break;
                case "status":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderByDescending(x => x.Status));
                    break;
                default:
                    functionOrder = EntityExtention<AccountRatingItem>.OrderBy(m => m.OrderBy(x => x.Order).ThenByDescending(x => x.Value));
                    break;
            }
            return functionOrder;
        }
    }
}
