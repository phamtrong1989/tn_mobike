﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Base;
using PT.Domain.Model;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.Drawing;
using Microsoft.AspNetCore.Mvc;

namespace TN.Base.Services
{
    public interface IMaintenanceRepairService : IService<MaintenanceRepair>
    {
        Task<ApiResponseData<List<MaintenanceRepair>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            EMaintenanceRepairStatus? status,
            DateTime? start,
            DateTime? end,
            int? warehouseId,
            string sortby,
            string sorttype
        );
        Task<ApiResponseData<MaintenanceRepair>> GetById(int id);
        Task<ApiResponseData<MaintenanceRepair>> Create(MaintenanceRepairCommand model);
        Task<ApiResponseData<MaintenanceRepair>> Edit(MaintenanceRepairCommand model);
        Task<ApiResponseData<MaintenanceRepair>> Cancel(int id);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);
        Task<ApiResponseData<MaintenanceRepair>> Approve(int id, DateTime endDate);

        #region [MaintenanceRepairItem]
        Task<ApiResponseData<List<MaintenanceRepairItem>>> MaintenanceRepairItemSearchPageAsync(int pageIndex, int pageSize, int maintenanceRepairId);
        Task<ApiResponseData<MaintenanceRepairItem>> MaintenanceRepairItemGetById(int id);
        Task<ApiResponseData<MaintenanceRepairItem>> MaintenanceRepairItemCreate(MaintenanceRepairItemCommand model);
        Task<ApiResponseData<MaintenanceRepairItem>> MaintenanceRepairItemEdit(MaintenanceRepairItemCommand model);
        Task<ApiResponseData<MaintenanceRepairItem>> MaintenanceRepairItemDelete(int id);
        #endregion
        Task<ApiResponseData<List<Bike>>> BikesSearchKey(string key);
        Task<ApiResponseData<List<Supplies>>> SuppliesSearchKey(string key, int warehouseId);
        Task<FileContentResult> ExportReport(
            DateTime? from,
            DateTime? to,
            string key,
            EMaintenanceRepairStatus? status,
            int? warehouseId
        );

    }
    public class MaintenanceRepairService : IMaintenanceRepairService
    {
        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        private readonly BaseSettings _baseSettings;

        private readonly IUserRepository _iUserRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly ISuppliesRepository _iSuppliesRepository;
        private readonly IBikeRepository _iBikeRepository;
        private readonly IWarehouseRepository _iWarehouseRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly IMaintenanceRepairRepository _iMaintenanceRepairRepository;
        private readonly IMaintenanceRepairItemRepository _iMaintenanceRepairItemRepository;
        private readonly IEntityBaseRepository<Producer> _producerRepository;
        private readonly IEntityBaseRepository<Supplies> _suppliesRepository;
        private readonly IEntityBaseRepository<Domain.Model.Color> _colorRepository;
        private readonly IEntityBaseRepository<MaintenanceRepair> _maintainRepairRepository;
        private readonly IEntityBaseRepository<MaintenanceRepairItem> _maintainRepairItemRepository;
        private readonly string controllerName = "";

        public MaintenanceRepairService
        (
            IOptions<BaseSettings> baseSettings,
            ILogger<MaintenanceRepairService> logger,
            IWebHostEnvironment iWebHostEnvironment,

            IMaintenanceRepairRepository iMaintenanceRepairRepository,
            IUserRepository iUserRepository,
            IMaintenanceRepairItemRepository iMaintenanceRepairItemRepository,
            ISuppliesRepository iSuppliesRepository,
            IBikeRepository iBikeRepository,
            IWarehouseRepository iWarehouseRepository,
            ILogRepository iLogRepository,
            IProjectRepository iProjectRepository,
            IInvestorRepository iInvestorRepository,
            IDockRepository iDockRepository,
            IEntityBaseRepository<Producer> producerRepository,
            IEntityBaseRepository<Supplies> suppliesRepository,
            IEntityBaseRepository<Domain.Model.Color> colorRepository,
            IEntityBaseRepository<MaintenanceRepair> maintainRepairRepository,
            IEntityBaseRepository<MaintenanceRepairItem> maintainRepairItemRepository
        )
        {
            _logger = logger;
            _iWebHostEnvironment = iWebHostEnvironment;
            _iMaintenanceRepairRepository = iMaintenanceRepairRepository;
            _iUserRepository = iUserRepository;
            _baseSettings = baseSettings.Value;
            _iMaintenanceRepairItemRepository = iMaintenanceRepairItemRepository;
            _iSuppliesRepository = iSuppliesRepository;
            _iBikeRepository = iBikeRepository;
            _iWarehouseRepository = iWarehouseRepository;
            _iLogRepository = iLogRepository;

            _iBikeRepository = iBikeRepository;
            _iUserRepository = iUserRepository;
            _iProjectRepository = iProjectRepository;
            _iInvestorRepository = iInvestorRepository;
            _iDockRepository = iDockRepository;
            _producerRepository = producerRepository;
            _suppliesRepository = suppliesRepository;
            _colorRepository = colorRepository;
            _maintainRepairRepository = maintainRepairRepository;
            _maintainRepairItemRepository = maintainRepairItemRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<MaintenanceRepair>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            EMaintenanceRepairStatus? status,
            DateTime? start,
            DateTime? end,
            int? warehouseId,
            string sortby,
            string sorttype
        )
        {
            try
            {
                var data = await _iMaintenanceRepairRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (key == null || x.Name.Contains(key) || x.Code.Contains(key))
                    && (status == null || x.Status == status)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                    && (start == null || x.StartDate.Date >= start.Value.Date)
                    && (end == null || x.StartDate.Date <= start.Value.Date)
                    ,
                    OrderByExtention(sortby, sorttype));

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.CreatedUser).ToList(), data.Data.Select(x => x.UpdatedUser ?? 0).ToList());

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<MaintenanceRepair>>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepair>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<MaintenanceRepair>() { Data = await _iMaintenanceRepairRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.Get", ex.ToString());
                return new ApiResponseData<MaintenanceRepair>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepair>> Create(MaintenanceRepairCommand model)
        {
            try
            {
                if(model.WarehouseId == 0)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Status = 0, Message = "Bạn chưa có kho, vui lòng truy cập quản lý kho xem bạn đã được cấu hình kho chưa, nếu chưa vui lòng liên hệ quản trị để xử lý." };
                }

                var ktW = await _iWarehouseRepository.SearchOneAsync(x => x.EmployeeId == UserInfo.UserId && x.Id == model.WarehouseId);
                if (ktW == null)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Status = 0, Message = "Không phải kho của bạn, chỉ có nhân viên điều phối mới có kho riêng" };
                }

                var dlAdd = new MaintenanceRepair
                {
                    Name = model.Name,
                    Descriptions = model.Descriptions,
                    Files = model.Files,
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    Status = EMaintenanceRepairStatus.Add,
                    WarehouseId = model.WarehouseId ?? 0,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    Code = $"{DateTime.Now:yyyyMMddHHmmss}",
                    LastDescriptions = model.LastDescriptions,
                    LastFiles = model.LastFiles,
                };
                await _iMaintenanceRepairRepository.AddAsync(dlAdd);
                await _iMaintenanceRepairRepository.Commit();

                await AddLog( dlAdd.Id, $"Thêm mới lịch '{dlAdd.Name}'", LogType.Insert);

                return new ApiResponseData<MaintenanceRepair>() { Data = dlAdd, Status = 1, Message = "Lập sửa chữa bảo dưỡng thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.Create", ex.ToString());
                return new ApiResponseData<MaintenanceRepair>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepair>> Edit(MaintenanceRepairCommand model)
        {
            try
            {
                var dl = await _iMaintenanceRepairRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Data = null, Status = 0 };
                }

                if (dl.Status == EMaintenanceRepairStatus.End)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Status = 1, Message = "Sửa chữa bảo dưỡng đã hoàn thành không thể cập nhật" };
                }

                if (dl.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                dl.Name = model.Name;
                dl.Descriptions = model.Descriptions;
                dl.LastDescriptions = model.LastDescriptions;
                dl.Files = model.Files;
                dl.LastFiles = model.LastFiles;
                dl.StartDate = model.StartDate;
                dl.EndDate = model.EndDate;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iMaintenanceRepairRepository.Update(dl);
                await _iMaintenanceRepairRepository.Commit();

                await AddLog( dl.Id, $"Cập nhật lịch '{dl.Name}'", LogType.Update);

                return new ApiResponseData<MaintenanceRepair>() { Data = dl, Status = 1, Message = "Cập nhật sửa chữa bảo dưỡng thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.Edit", ex.ToString());
                return new ApiResponseData<MaintenanceRepair>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepair>> Cancel(int id)
        {
            try
            {
                var dl = await _iMaintenanceRepairRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Data = null, Status = 0 };
                }

                if (dl.Status == EMaintenanceRepairStatus.End)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Status = 1, Message = "Sửa chữa bảo dưỡng đã hoàn thành không thể hủy" };
                }

                if (dl.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                dl.Status = EMaintenanceRepairStatus.Cancel;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iMaintenanceRepairRepository.Update(dl);
                await _iMaintenanceRepairRepository.Commit();

                await AddLog( dl.Id, $"Hủy lịch '{dl.Name}'", LogType.Update);

                return new ApiResponseData<MaintenanceRepair>() { Data = dl, Status = 1, Message = "Hủy sửa chữa bảo dưỡng thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.Cancel", ex.ToString());
                return new ApiResponseData<MaintenanceRepair>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepair>> Approve(int id, DateTime endDate)
        {
            try
            {
                var dl = await _iMaintenanceRepairRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Data = null, Status = 0 };
                }

                if (dl.Status == EMaintenanceRepairStatus.End)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Status = 1, Message = "Lịch này không thể xác nhận hoàn thành nữa" };
                }

                var ktCount = await _iMaintenanceRepairItemRepository.Count(x => x.MaintenanceRepairId == id);
                if (ktCount == 0)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Data = dl, Status = 1, Message = "Không có trang thiết bị nào được sửa chữa hoặc bảo dưỡng, không thể xác nhận hoàn thành" };
                }

                var isInventory = await CheckInventoryApprove(dl.Id, dl.WarehouseId);
                if (!isInventory)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Status = 2, Message = "Số lượng trong kho không đủ" };
                }

                if (dl.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<MaintenanceRepair>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                dl.EndDate = endDate;
                dl.Status = EMaintenanceRepairStatus.End;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                _iMaintenanceRepairRepository.Update(dl);
                await _iMaintenanceRepairRepository.Commit();
                // Trừ trong kho
                await _iSuppliesRepository.SuppliesWarehouseExportMaintenanceRepair(dl.Id, dl.WarehouseId);
                // Xử lý lưu log lịch sử
                await AddLog( dl.Id, $"Xác nhận hoàn thành lịch '{dl.Name}'", LogType.Update);

                return new ApiResponseData<MaintenanceRepair>() { Data = dl, Status = 1, Message = "Xác nhận hoàn thành sửa chưa bảo dưỡng thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.Delete", ex.ToString());
                return new ApiResponseData<MaintenanceRepair>();
            }
        }

        #region [ExportReport]
        public async Task<FileContentResult> ExportReport(
            DateTime? from,
            DateTime? to,
            string key,
            EMaintenanceRepairStatus? status,
            int? warehouseId
        )
        {
            var maintainRepairs = await _maintainRepairRepository.Search(
                x => (key == null || x.Name.Contains(key) || x.Code.Contains(key))
                    && (status == null || x.Status == status)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                );

            var maintainRepairItems = await _maintainRepairItemRepository.Search(
                x => maintainRepairs.Select(y => y.Id).Contains(x.MaintenanceRepairId)
                    && (from == null || x.CompleteDate >= from)
                    && (to == null || x.CompleteDate < to.Value.AddDays(1))
                    ,
                    null
                    ,
                    x => new MaintenanceRepairItem
                    {
                        Id = x.Id,
                        MaintenanceRepairId = x.MaintenanceRepairId,
                        SuppliesId = x.SuppliesId,
                        SuppliesCode = x.SuppliesCode,
                        SuppliesQuantity = x.SuppliesQuantity,
                        CompleteDate = x.CompleteDate,
                        EmployeeId = x.EmployeeId,
                        BikeId = x.BikeId,
                        ProjectId = x.ProjectId,
                        InvestorId = x.InvestorId
                    }
                );

            var suppliess = await _suppliesRepository.Search(
                x => maintainRepairItems.Select(x => x.SuppliesId).Contains(x.Id),
                null,
                x => new Supplies
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var bikes = await _iBikeRepository.Search(
                x => maintainRepairItems.Select(x => x.BikeId).Contains(x.Id),
                null,
                x => new Bike
                {
                    Id = x.Id,
                    Plate = x.Plate,
                    Type = x.Type,
                    DockId = x.DockId,
                    ProducerId = x.ProducerId,
                    ColorId = x.ColorId
                });

            var producers = await _producerRepository.Search(
                x => bikes.Select(x => x.ProducerId).Distinct().Contains(x.Id),
                null,
                x => new Producer
                {
                    Id = x.Id,
                    Name = x.Name
                });

            var docks = await _iDockRepository.Search(
                x => bikes.Select(x => x.DockId).Contains(x.Id),
                null,
                x => new Dock
                {
                    Id = x.Id,
                    SerialNumber = x.SerialNumber,
                    IMEI = x.IMEI
                });

            var employees = await _iUserRepository.Search(
                x => maintainRepairItems.Select(y => y.EmployeeId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code,
                });

            var projects = await _iProjectRepository.Search(
                x => maintainRepairItems.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var investors = await _iInvestorRepository.Search(
                x => maintainRepairItems.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                null,
                x => new Investor
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var colors = await _colorRepository.Search(
                x => bikes.Select(y => y.ColorId).Distinct().Contains(x.Id),
                null,
                x => new Domain.Model.Color
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            foreach (var item in maintainRepairItems)
            {
                item.Bike = new Bike { };
                item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                item.Bike.Color = colors.FirstOrDefault(x => x.Id == item.Bike?.ColorId);
                item.Bike.Dock = docks.FirstOrDefault(x => x.Id == item.Bike?.DockId);
                item.Bike.Producer = producers.FirstOrDefault(x => x.Id == item.Bike?.ProducerId);
                item.Supplies = suppliess.FirstOrDefault(x => x.Id == item.SuppliesId);
                item.Project = projects.FirstOrDefault(x => x.Id == item.ProjectId);
                item.Investor = investors.FirstOrDefault(x => x.Id == item.InvestorId);
                item.Employee = employees.FirstOrDefault(x => x.Id == item.EmployeeId);
            }

            var filePath = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(filePath)))
            {
                MyExcel.Workbook.Worksheets.Add("BC Bảo dưỡng - Sửa chữa");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelMaintainRepair(workSheet, maintainRepairItems, from, to);
                MyExcel.Save();
            }
            //
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return new FileContentResult(memory.GetBuffer(), "application/x-msdownload")
            {
                FileDownloadName = Path.GetFileName(filePath)
            };
        }

        private void FormatExcelMaintainRepair(ExcelWorksheet worksheet, List<MaintenanceRepairItem> items, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 20}, {4 , 15}, {5, 25},
                {6 , 12}, {7 , 15}, {8 , 20}, {9 , 10}, {10, 15},
                {11, 15}, {12, 20}, {13, 20}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2, 3, 4, 5, 6, 7, 10, 11, 12 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO BẢO DƯỠNG - SỬA CHỮA";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["B3:D3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:M5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Serial khóa";
            worksheet.Cells["C5"].Value = "IMEI";
            worksheet.Cells["D5"].Value = "Loại xe";
            worksheet.Cells["E5"].Value = "Nhà sản xuất";
            worksheet.Cells["F5"].Value = "Màu sắc";
            worksheet.Cells["G5"].Value = "Mã vật tư";
            worksheet.Cells["H5"].Value = "Tên vật tư";
            worksheet.Cells["I5"].Value = "Số lượng";
            worksheet.Cells["J5"].Value = "Thời gian sửa chữa/bảo dưỡng";
            worksheet.Cells["K5"].Value = "Nhân viên thực hiện";
            worksheet.Cells["L5"].Value = "Đơn vị đầu tư";
            worksheet.Cells["M5"].Value = "Dự án";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Bike?.Dock?.SerialNumber;
                worksheet.Cells[rs, 3].Value = item.Bike?.Dock?.IMEI;
                worksheet.Cells[rs, 4].Value = item.Bike?.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 5].Value = item.Bike?.Producer?.Name;
                worksheet.Cells[rs, 6].Value = item.Bike?.Color?.Name;
                worksheet.Cells[rs, 7].Value = item.Supplies?.Code;
                worksheet.Cells[rs, 8].Value = item.Supplies?.Name;
                worksheet.Cells[rs, 9].Value = item.SuppliesQuantity;
                worksheet.Cells[rs, 10].Value = item.CompleteDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 11].Value = item.Employee?.Code;
                worksheet.Cells[rs, 12].Value = item.Investor?.Name;
                worksheet.Cells[rs, 13].Value = item.Project?.Name;

                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:M{rs - 1}");
            worksheet.Cells[$"A6:M{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        #endregion

        private Func<IQueryable<MaintenanceRepair>, IOrderedQueryable<MaintenanceRepair>> OrderByExtention(string sortby, string sorttype) => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "name" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.Name)),
            "descriptions" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.Descriptions)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.Descriptions)),
            "files" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.Files)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.Files)),
            "startDate" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.StartDate)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.StartDate)),
            "endDate" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.EndDate)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.EndDate)),
            "status" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.Status)),
            "warehouseId" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.WarehouseId)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.WarehouseId)),
            "createdDate" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "createdUser" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
            "updatedDate" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            "updatedUser" => sorttype == "asc" ? EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
            _ => EntityExtention<MaintenanceRepair>.OrderBy(m => m.OrderByDescending(x => x.Id)),
        };

        #region [Upload file]
        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/MaintenanceRepair/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfff}_{Path.GetFileName(file.FileName)}";

                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);

                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.Upload", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }

        public FileStream View(string pathData, bool isPrivate)
        {
            try
            {
                if (isPrivate)
                {
                    string file = $"{_baseSettings.PrivateDataPath}{pathData}";
                    return new FileStream(file, FileMode.Open, FileAccess.Read);
                }
                else
                {
                    return new FileStream(pathData, FileMode.Open, FileAccess.Read);
                }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region [MaintenanceRepairItem]
        public async Task<ApiResponseData<List<MaintenanceRepairItem>>> MaintenanceRepairItemSearchPageAsync(int pageIndex, int pageSize, int maintenanceRepairId)
        {
            try
            {
                var data = await _iMaintenanceRepairItemRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => x.MaintenanceRepairId == maintenanceRepairId
                    ,
                    x => x.OrderByDescending(m => m.Id));

                var bikes = await _iBikeRepository.Search(m => data.Data.Select(x => x.BikeId).Contains(m.Id));
                var suppliess = await _iSuppliesRepository.Search(m => data.Data.Select(x => x.SuppliesId).Contains(m.Id));

                foreach (var item in data.Data)
                {
                    item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                    item.Supplies = suppliess.FirstOrDefault(x => x.Id == item.SuppliesId);
                }
                data.OutData = await _iMaintenanceRepairItemRepository.Sum(x => x.MaintenanceRepairId == maintenanceRepairId, x => x.SuppliesQuantity);
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.MaintenanceRepairSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<MaintenanceRepairItem>>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepairItem>> MaintenanceRepairItemGetById(int id)
        {
            try
            {
                var data = await _iMaintenanceRepairItemRepository.SearchOneAsync(x => x.Id == id);
                if (data != null)
                {
                    var dl = await _iMaintenanceRepairRepository.SearchOneAsync(x => x.Id == data.MaintenanceRepairId);
                    if (dl == null)
                    {
                        return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0 };
                    }

                    data.Bike = await _iBikeRepository.SearchOneAsync(x => x.Id == data.BikeId);
                    data.Supplies = await _iSuppliesRepository.SearchOneAsync(x => x.Id == data.SuppliesId);
                    if (data.Supplies != null)
                    {
                        var dataS = await _iSuppliesRepository.InitSuppliesWarehouses(new List<Supplies> { data.Supplies }, dl.WarehouseId);
                        data.Supplies = dataS.FirstOrDefault();
                    }
                }
                return new ApiResponseData<MaintenanceRepairItem>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.MaintenanceRepairItemGetById", ex.ToString());
                return new ApiResponseData<MaintenanceRepairItem>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepairItem>> MaintenanceRepairItemCreate(MaintenanceRepairItemCommand model)
        {
            try
            {
                if(model.SuppliesId > 0 && model.SuppliesQuantity <= 0)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0, Message = "Bạn chưa nhập số lượng vật tư sử dụng" };
                }

                if (model.SuppliesId == 0 && model.SuppliesQuantity != 0)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0, Message = "Nếu không sử dụng vật tư nào thì số lượng vật tư phải = 0" };
                }

                var dl = await _iMaintenanceRepairRepository.SearchOneAsync(x => x.Id == model.MaintenanceRepairId);
                if (dl == null)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0 };
                }

                if (dl.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                if (dl.Status == EMaintenanceRepairStatus.End)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Status = 1, Message = "Lịch này đã hoàn thành rồi" };
                }

                var isInventory = await CheckInventory(dl.Id, model.SuppliesId, dl.WarehouseId, model.SuppliesQuantity, 0);
                if (!isInventory)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Status = 2, Message = "Số lượng trong kho không đủ" };
                }


                var dlAdd = new MaintenanceRepairItem
                {
                    MaintenanceRepairId = model.MaintenanceRepairId,
                    BikeId = model.BikeId,
                    ProjectId = model.ProjectId,
                    InvestorId = model.InvestorId,
                    Type = model.Type,
                    Status = model.Status,
                    Descriptions = model.Descriptions,
                    Files = model.Files,
                    CompleteDate = model.CompleteDate,
                    SuppliesId = model.SuppliesId,
                    SuppliesCode = model.SuppliesCode,
                    SuppliesQuantity = model.SuppliesQuantity,
                    EmployeeId = model.EmployeeId,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };
                await _iMaintenanceRepairItemRepository.AddAsync(dlAdd);
                await _iMaintenanceRepairItemRepository.Commit();

                await AddLog( dl.Id, $"Thêm mới nội dung #{dlAdd.SuppliesId}", LogType.Insert);

                return new ApiResponseData<MaintenanceRepairItem>() { Data = dlAdd, Status = 1 , Message = "Thêm mới thành công"};
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.MaintenanceRepairItemCreate", ex.ToString());
                return new ApiResponseData<MaintenanceRepairItem>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepairItem>> MaintenanceRepairItemEdit(MaintenanceRepairItemCommand model)
        {
            try
            {
                var dl = await _iMaintenanceRepairItemRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0, Message = "Dữ liệu không tồn tại, vui lòng thử lại" };
                }

                var dlM = await _iMaintenanceRepairRepository.SearchOneAsync(x => x.Id == dl.MaintenanceRepairId);
                if (dlM == null)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0, Message = "Dữ liệu không tồn tại, vui lòng thử lại" };
                }

                if (dl.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                if (dlM.Status == EMaintenanceRepairStatus.End)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Status = 1, Message = "Lịch này đã hoàn thành rồi" };
                }

                var isInventory = await CheckInventory(dlM.Id, model.SuppliesId, dlM.WarehouseId, model.SuppliesQuantity, dl.Id);
                if (!isInventory)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Status = 2, Message = "Số lượng trong kho không đủ" };
                }

                dl.Type = model.Type;
                dl.Status = model.Status;
                dl.Descriptions = model.Descriptions;
                dl.Files = model.Files;
                dl.CompleteDate = model.CompleteDate;
                dl.SuppliesId = model.SuppliesId;
                dl.SuppliesCode = model.SuppliesCode;
                dl.SuppliesQuantity = model.SuppliesQuantity;
                dl.EmployeeId = model.EmployeeId;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iMaintenanceRepairItemRepository.Update(dl);
                await _iMaintenanceRepairItemRepository.Commit();
                return new ApiResponseData<MaintenanceRepairItem>() { Data = dl, Status = 1, Message = "Thêm nội dung thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.MaintenanceRepairItemEdit", ex.ToString());
                return new ApiResponseData<MaintenanceRepairItem>();
            }
        }

        public async Task<ApiResponseData<MaintenanceRepairItem>> MaintenanceRepairItemDelete(int id)
        {
            try
            {
                var dl = await _iMaintenanceRepairItemRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0 };
                }

                var dlM = await _iMaintenanceRepairRepository.SearchOneAsync(x => x.Id == dl.MaintenanceRepairId);
                if (dlM == null)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 0 };
                }

                if (dlM.CreatedUser != UserInfo.UserId)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Data = dl, Status = 0, Message = "Chỉ có người lập ra phiếu này mới có thể sử dụng thao tác này" };
                }

                if (dlM.Status == EMaintenanceRepairStatus.End)
                {
                    return new ApiResponseData<MaintenanceRepairItem>() { Status = 1, Message = "Lịch này đã hoàn thành rồi" };
                }

                _iMaintenanceRepairItemRepository.Delete(dl);
                await _iMaintenanceRepairItemRepository.Commit();
                return new ApiResponseData<MaintenanceRepairItem>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.MaintenanceRepairItemDelete", ex.ToString());
                return new ApiResponseData<MaintenanceRepairItem>();
            }
        }
        #endregion

        public async Task<ApiResponseData<List<Supplies>>> SuppliesSearchKey(string key, int warehouseId)
        {
            try
            {
                var data = await _iSuppliesRepository.SuppliesSearchKey(key, warehouseId);
                return new ApiResponseData<List<Supplies>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("MaintenanceRepairSevice.SuppliesSearchKey", ex.ToString());
                return new ApiResponseData<List<Supplies>>();
            }
        }

        public async Task<ApiResponseData<List<Bike>>> BikesSearchKey(string key)
        {
            try
            {
                var data = await _iBikeRepository.SearchTop(50, x => key == null || x.Plate.Contains(key) || x.SerialNumber.Contains(key));
                return new ApiResponseData<List<Bike>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SuppliesSearchKey", ex.ToString());
                return new ApiResponseData<List<Bike>>();
            }
        }

        public async Task<bool> CheckInventoryApprove(int maintenanceRepairId, int warehouseId)
        {
            var listOld = (await _iMaintenanceRepairItemRepository.Search(x => x.MaintenanceRepairId == maintenanceRepairId));
            var listVT = listOld.GroupBy(x => x.SuppliesId).Select(x => x.FirstOrDefault()).ToList();
            foreach (var item in listVT)
            {
                var ton = await _iSuppliesRepository.GetInventory(item.SuppliesId, warehouseId);
                var totalBySupp = listOld.Where(x => x.SuppliesId == item.SuppliesId).Sum(x => x.SuppliesQuantity);
                if (ton < totalBySupp)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }
    
        private async Task<bool> CheckInventory(int maintenanceRepairId, int suppliesId, int warehouseId, int count, int id)
        {
            if (suppliesId > 0)
            {
                var ton = await _iSuppliesRepository.GetInventory(suppliesId, warehouseId);
                var listOld = (await _iMaintenanceRepairItemRepository.Search(x => x.MaintenanceRepairId == maintenanceRepairId && x.SuppliesId == suppliesId));
                if (id > 0)
                {
                    foreach (var item in listOld)
                    {
                        if (item.Id == id)
                        {
                            item.SuppliesQuantity = count;
                        }
                    }
                    var totalSupp = listOld.Sum(x => x.SuppliesQuantity);
                    if (ton < totalSupp)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    var totalSupp = listOld.Sum(x => x.SuppliesQuantity);
                    if (ton < (totalSupp + count))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return true;
        }

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private string InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return filePath;
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}
