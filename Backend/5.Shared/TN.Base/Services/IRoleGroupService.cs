using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using Microsoft.AspNetCore.SignalR;
using PT.Base;

namespace TN.Base.Services
{
    public interface IRoleGroupService : IService<RoleGroup>
    {
        Task<ApiResponseData<List<RoleGroup>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<RoleGroup>> GetById(int id);
        Task<ApiResponseData<RoleGroup>> Create(RoleGroupCommand model);
        Task<ApiResponseData<RoleGroup>> Edit(RoleGroupCommand model);
        Task<ApiResponseData<RoleGroup>> Delete(int id);

    }
    public class RoleGroupService : IRoleGroupService
    {
        private readonly ILogger _logger;
        private readonly IRoleGroupRepository _iRoleGroupRepository;
        private readonly IRoleControllerRepository _iRoleControllerRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        public RoleGroupService
        (
            ILogger<RoleGroupService> logger,
            IRoleGroupRepository iRoleGroupRepository,
            IRoleControllerRepository iRoleControllerRepository,
            IParameterRepository iParameterRepository,
            IHubContext<CategoryHub> hubContext
        )
        {
            _logger = logger;
            _iRoleGroupRepository = iRoleGroupRepository;
            _iRoleControllerRepository = iRoleControllerRepository;
            _iParameterRepository = iParameterRepository;
            _hubContext = hubContext;
        }

        public async Task<ApiResponseData<List<RoleGroup>>> SearchPageAsync(int pageIndex, int pageSize, string key, string sortby, string sorttype)
        {
            try
            {
                return await _iRoleGroupRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => 
                        (key == null || x.Name.ToLower().Contains(key)),
                    OrderByExtention(sortby, sorttype),
                    x => new RoleGroup
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        Order = x.Order,
                        IsShow = x.IsShow,
                        Icon = x.Icon
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleGroupSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<RoleGroup>>();
            }
        }

        public async Task<ApiResponseData<RoleGroup>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<RoleGroup>() { Data = await _iRoleGroupRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleGroupSevice.Get", ex.ToString());
                return new ApiResponseData<RoleGroup>();
            }
        }

        public async Task<ApiResponseData<RoleGroup>> Create(RoleGroupCommand model)
        {
            try
            {
                var dlAdd = new RoleGroup
                {
                    Name = model.Name,
                    Description = model.Description,
                    Order = model.Order,
                    IsShow = model.IsShow,
                    Icon = model.Icon
                };
                await _iRoleGroupRepository.AddAsync(dlAdd);
                await _iRoleGroupRepository.Commit();
                await InitCategoryAsync();

                return new ApiResponseData<RoleGroup>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleGroupSevice.Create", ex.ToString());
                return new ApiResponseData<RoleGroup>();
            }
        }

        public async Task<ApiResponseData<RoleGroup>> Edit(RoleGroupCommand model)
        {
            try
            {
                var dl = await _iRoleGroupRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<RoleGroup>() { Data = null, Status = 0 };
                }

                dl.Name = model.Name;
                dl.Description = model.Description;
                dl.Order = model.Order;
                dl.IsShow = model.IsShow;
                dl.Icon = model.Icon;

                _iRoleGroupRepository.Update(dl);
                await _iRoleGroupRepository.Commit();
                await InitCategoryAsync();

                return new ApiResponseData<RoleGroup>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleGroupSevice.Edit", ex.ToString());
                return new ApiResponseData<RoleGroup>();
            }
        }
        public async Task<ApiResponseData<RoleGroup>> Delete(int id)
        {
            try
            {
                var dl = await _iRoleGroupRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<RoleGroup>() { Data = null, Status = 0 };
                }
                else
                {
                    var check = await _iRoleControllerRepository.Search(x => x.GroupId == dl.Id);
                    if (check != null && check.Count > 0)
                    {
                        return new ApiResponseData<RoleGroup>() { Data = null, Status = 2 };
                    }
                    else
                    {
                        _iRoleGroupRepository.Delete(dl);
                        await _iRoleGroupRepository.Commit();
                        await InitCategoryAsync();
                        return new ApiResponseData<RoleGroup>() { Data = null, Status = 1 };
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleGroupSevice.Delete", ex.ToString());
                return new ApiResponseData<RoleGroup>();
            }
        }

        private Func<IQueryable<RoleGroup>, IOrderedQueryable<RoleGroup>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<RoleGroup>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<RoleGroup>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "name" => sorttype == "asc" ? EntityExtention<RoleGroup>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<RoleGroup>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "description" => sorttype == "asc" ? EntityExtention<RoleGroup>.OrderBy(m => m.OrderBy(x => x.Description)) : EntityExtention<RoleGroup>.OrderBy(m => m.OrderByDescending(x => x.Description)),
                "order" => sorttype == "asc" ? EntityExtention<RoleGroup>.OrderBy(m => m.OrderBy(x => x.Order)) : EntityExtention<RoleGroup>.OrderBy(m => m.OrderByDescending(x => x.Order)),
                "isShow" => sorttype == "asc" ? EntityExtention<RoleGroup>.OrderBy(m => m.OrderBy(x => x.IsShow)) : EntityExtention<RoleGroup>.OrderBy(m => m.OrderByDescending(x => x.IsShow)),
                _ => EntityExtention<RoleGroup>.OrderBy(m => m.OrderByDescending(x => x.Order)),
            };
        }

        #region [Hub]
        private async Task InitCategoryAsync(string tableName = null)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName ?? "RoleGroup");
            }
            catch { }
        }
        #endregion
    }
}
