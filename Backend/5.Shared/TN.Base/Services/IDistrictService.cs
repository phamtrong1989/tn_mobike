﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PT.Base;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IDistrictService : IService<District>
    {
        Task<ApiResponseData<List<District>>> SearchPageAsync(int pageIndex, int PageSize, string key, int? cityId, string orderby, string ordertype);
        Task<ApiResponseData<District>> GetById(int id);
        Task<ApiResponseData<District>> Create(DistrictCommand model);
        Task<ApiResponseData<District>> Edit(DistrictCommand model);
        Task<ApiResponseData<District>> Delete(int id);

    }
    public class DistrictService : IDistrictService
    {
        private readonly ILogger _logger;
        private readonly IDistrictRepository _iDistrictRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly string controllerName = "";

        public DistrictService
        (
            ILogger<DistrictService> logger,
            IDistrictRepository iDistrictRepository,
            IUserRepository iUserRepository,
            ILogRepository iLogRepository,
            IParameterRepository iParameterRepository,
            IHubContext<CategoryHub> hubContext

        )
        {
            _logger = logger;
            _iDistrictRepository = iDistrictRepository;
            _iUserRepository = iUserRepository;
            _iParameterRepository = iParameterRepository;
            _iLogRepository = iLogRepository;
            _hubContext = hubContext;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<District>>> SearchPageAsync(int pageIndex, int PageSize, string key, int? cityId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iDistrictRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    && (key == null || x.Name.Contains(key))
                    && (cityId == null || x.CityId == cityId)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new District
                    {
                        Id = x.Id,
                        PrivateId = x.PrivateId,
                        CityId = x.CityId,
                        Name = x.Name,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,

                    });
                var idUsers = data.Data.Select(x => x.CreatedUser).ToList();
                idUsers.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(idUsers);
                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }
                
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("DistrictSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<District>>();
            }
        }

        public async Task<ApiResponseData<District>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<District>() { Data = await _iDistrictRepository.SearchOneAsync( x => x.Id == id)};
            }
            catch (Exception ex)
            {
                _logger.LogError("DistrictSevice.Get", ex.ToString());
                return new ApiResponseData<District>();
            }
        }

        public async Task<ApiResponseData<District>> Create(DistrictCommand model)
        {
            try
            {
                var dlAdd = new District
                {
                    Id = model.Id,
                    PrivateId = model.PrivateId,
                    CityId = model.CityId,
                    Name = model.Name,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    UpdatedDate = DateTime.Now,
                    UpdatedUser = UserInfo.UserId

                };
                await _iDistrictRepository.AddAsync(dlAdd);
                await _iDistrictRepository.Commit();
                await InitCategoryAsync("Districts");
                await AddLog(dlAdd.Id, $"Thêm mới quận/huyện '{dlAdd.Name}'", LogType.Insert);
                return new ApiResponseData<District>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DistrictSevice.Create", ex.ToString());
                return new ApiResponseData<District>();
            }
        }

        public async Task<ApiResponseData<District>> Edit(DistrictCommand model)
        {
            try
            {
                var dl = await _iDistrictRepository.SearchOneAsync( x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<District>() { Data = null, Status = 0 };
                }

                dl.Id = model.Id;
                dl.PrivateId = model.PrivateId;
                dl.CityId = model.CityId;
                dl.Name = model.Name;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;


                _iDistrictRepository.Update(dl);
                await _iDistrictRepository.Commit();

                await InitCategoryAsync("Districts");
                await AddLog(dl.Id, $"Thêm mới quận/huyện '{dl.Name}'", LogType.Insert);

                return new ApiResponseData<District>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DistrictSevice.Edit", ex.ToString());
                return new ApiResponseData<District>();
            }
        }
        public async Task<ApiResponseData<District>> Delete(int id)
        {
            try
            {
                var dl = await _iDistrictRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<District>() { Data = null, Status = 0 };
                }

                _iDistrictRepository.Delete(dl);
                await _iDistrictRepository.Commit();
                await InitCategoryAsync("Districts");
                await AddLog(dl.Id, $"Thêm mới quận/huyện '{dl.Name}'", LogType.Insert);
               
                return new ApiResponseData<District>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DistrictSevice.Delete", ex.ToString());
                return new ApiResponseData<District>();
            }
        }

        private Func<IQueryable<District>, IOrderedQueryable<District>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<District>, IOrderedQueryable<District>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<District>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "privateId":
                    functionOrder = sorttype == "asc" ? EntityExtention<District>.OrderBy(m => m.OrderBy(x => x.PrivateId)) : EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.PrivateId));
                    break;
                case "cityId":
                    functionOrder = sorttype == "asc" ? EntityExtention<District>.OrderBy(m => m.OrderBy(x => x.CityId)) : EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.CityId));
                    break;
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<District>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<District>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "createdUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<District>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser));
                    break;
                case "updatedDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<District>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate));
                    break;
                case "updatedUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<District>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser));
                    break;

                default:
                    functionOrder = EntityExtention<District>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }
        private async Task AddLog(int objectId, string action, LogType type, string objectType = null)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        private async Task InitCategoryAsync(string tableName)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName);
            }
            catch { }
        }
    }
}
