﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PT.Base;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Domain.Model;

namespace TN.Base.Services
{
    public interface ILogService : IService<Domain.Model.Log>
    {
        ApiResponseData<List<MongoLog>> MongoSearchPageAsync(int page, int pageSize, int? accountId, string logKey, string functionName, EnumMongoLogType? type, DateTime? date, EnumSystemType? systemType,
            string deviceKey, bool? isExeption, string sortby, string sorttype);

        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);
    }
    public class LogService : ILogService
    {
        private readonly ILogger _logger;
        private readonly ILogRepository _iLogRepository;
        private readonly IOptions<LogSettings> _logSettings;
        private readonly IAccountRepository _iAccountRepository;

        public LogService
        (
            ILogger<ColorService> logger,
            ILogRepository iLogRepository,
            IOptions<LogSettings> logSettings,
            IAccountRepository iAccountRepository
        )
        {
            _logger = logger;
            _iLogRepository = iLogRepository;
            _logSettings = logSettings;
            _iAccountRepository = iAccountRepository;
        }

        public ApiResponseData<List<MongoLog>> MongoSearchPageAsync(
            int page, 
            int limit, 
            int? accountId, 
            string logKey, 
            string functionName, 
            EnumMongoLogType? type, 
            DateTime? date, 
            EnumSystemType? systemType, 
            string deviceKey,
            bool? isExeption,
            string sortby, 
            string sorttype
            )
        {
            try
            {
                return  _iLogRepository.MongoSearch(_logSettings.Value, page, limit, accountId, logKey, functionName, type, date, systemType, deviceKey, isExeption, sortby, sorttype);
            }
            catch (Exception ex)
            {
                _logger.LogError("LogService.SearchPageAsync: " + ex.ToString());
                return new ApiResponseData<List<MongoLog>>();
            }
        }

        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LogService.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }
    }
}
