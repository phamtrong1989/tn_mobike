﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Base;
using System.Threading;
using PT.Domain.Model;
using Firebase.Database;
using Firebase.Auth;
using Firebase.Database.Query;
using Microsoft.AspNetCore.SignalR;

namespace TN.Base.Services
{
    public interface IBookingService : IService<Booking>
    {
        Task<ApiResponseData<List<Booking>>> SearchPageAsync(int? pageIndex, int? pageSize, string key, int? investorId, int? projectId, int? customerGroupId, int? stationIn, int? accountId, string from, string to, ETicketType? ticketPrice_TicketType, string sortby, string sorttype);
        Task<ApiResponseData<Booking>> GetById(int id);
        Task<ApiResponseData<Transaction>> BookingCheckOut(BookingEndCommand cm, decimal vehicleRecallPrice, double reCallM);
        Task<ApiResponseData<Transaction>> BookingCancel(BookingCancelCommand cm);
        Task<ApiResponseData<OpenLockHistory>> OpenLock(BookingOpenLockCommand cm);
        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);
        Task<ApiResponseData<Booking>> BookingCalculate(int id);
        Task<ApiResponseData<Transaction>> ReCall(ReCallCommand cm);
        Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key);
        Task<ApiResponseData<Booking>> BookingAdd(BookingAddCommand cm, EBookingFailStatus status);
        Task<ApiResponseData<RFIDBooking>> RFIDBookingCheckOut(RFIDBookingEndCommand cm);
    }
    public class BookingService : IBookingService
    {
        private readonly ILogger _logger;
        private readonly IBookingRepository _iBookingRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IBikeRepository _iBikeRepository;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly ICustomerGroupRepository _customerGroupRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IOptions<BaseSettings> _baseSettings;
        private readonly IWalletTransactionRepository _iIWalletTransactionRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IOpenLockRequestRepository _iOpenLockRequestRepository;
        private readonly IOpenLockHistoryRepository _iOpenLockHistoryRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IGPSDataRepository _iGPSDataRepository;
        private readonly IOptions<LogSettings> _logSettings;
        private readonly INotificationJobRepository _iNotificationJobRepository;
        private readonly List<NotificationTemp> _notificationTempSettings;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IVehicleRecallPriceRepository _iVehicleRecallPriceRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly IGPSDataRepository _iEventSystemRepository;
        private readonly IHubContext<EventSystemHub> _hubContext;
        private readonly IUserRepository _iUserRepository;
        private readonly IBookingFailRepository _iBookingFailRepository;
        private readonly IRFIDBookingRepository _iRFIDBookingRepository;
        private readonly ICyclingWeekRepository _iCyclingWeekRepository;
        public BookingService
        (
            ILogger<BookingService> logger,
            IBookingRepository iBookingRepository, 
            IAccountRepository iAccountRepository, 
            IBikeRepository iBikeRepository, 
            ITicketPriceRepository iTicketPriceRepository, 
            ITransactionRepository iTransactionRepository,
            ICustomerGroupRepository customerGroupRepository,
            IWalletRepository iWalletRepository,
            IOptions<BaseSettings> baseSettings,
            IWalletTransactionRepository iIWalletTransactionRepository,
            IDockRepository iDockRepository,
            IOpenLockRequestRepository iOpenLockRequestRepository,
            IOpenLockHistoryRepository iOpenLockHistoryRepository,
            ILogRepository iLogRepository,
            IGPSDataRepository iGPSDataRepository,
            IOptions<LogSettings> logSettings,
            INotificationJobRepository iNotificationJobRepository,
            IOptions<List<NotificationTemp>> notificationTempSettings,
            IProjectRepository iProjectRepository,
            IVehicleRecallPriceRepository iVehicleRecallPriceRepository,
            IStationRepository iStationRepository,
            IGPSDataRepository iEventSystemRepository,
            IHubContext<EventSystemHub> hubContext,
            IUserRepository iUserRepository,
            IBookingFailRepository iBookingFailRepository,
            IRFIDBookingRepository iRFIDBookingRepository,
            ICyclingWeekRepository iCyclingWeekRepository
            )
        {
            _logger = logger;
            _iBookingRepository = iBookingRepository;
            _iAccountRepository = iAccountRepository;
            _iBikeRepository = iBikeRepository;
            _iTicketPriceRepository = iTicketPriceRepository;
            _iTransactionRepository = iTransactionRepository;
            _customerGroupRepository = customerGroupRepository;
            _iWalletRepository = iWalletRepository;
            _baseSettings = baseSettings;
            _iIWalletTransactionRepository = iIWalletTransactionRepository;
            _iDockRepository = iDockRepository;
            _iOpenLockRequestRepository = iOpenLockRequestRepository;
            _iOpenLockHistoryRepository = iOpenLockHistoryRepository;
            _iLogRepository = iLogRepository;
            _iGPSDataRepository = iGPSDataRepository;
            _logSettings = logSettings;
            _iNotificationJobRepository = iNotificationJobRepository;
            _notificationTempSettings = notificationTempSettings.Value;
            _iProjectRepository = iProjectRepository;
            _iVehicleRecallPriceRepository = iVehicleRecallPriceRepository;
            _iStationRepository = iStationRepository;
            _iEventSystemRepository = iEventSystemRepository;
            _hubContext = hubContext;
            _iUserRepository = iUserRepository;
            _iBookingFailRepository = iBookingFailRepository;
            _iRFIDBookingRepository =  iRFIDBookingRepository;
            _iCyclingWeekRepository = iCyclingWeekRepository;
        }

        public async Task<ApiResponseData<OpenLockHistory>> OpenLock(BookingOpenLockCommand cm)
        {
            var ktDock = await _iDockRepository.SearchOneAsync(x => x.Id == cm.DockId);
            if(ktDock == null)
            {
                return new ApiResponseData<OpenLockHistory>() { Data = null, Status = 0 };
            }

            var kt = new OpenLockRequest
            {
                CreatedDate = DateTime.Now,
                IMEI = ktDock.IMEI,
                OpenTime = null,
                Retry = 0,
                StationId = ktDock.StationId,
                Status = LockRequestStatus.Waiting,
                DockId = ktDock.Id,
                AccountId = 0,
                TransactionCode = null,
                SerialNumber = ktDock.SerialNumber,
                InvestorId = ktDock.InvestorId,
                ProjectId = ktDock.ProjectId
            };
            await _iOpenLockRequestRepository.AddAsync(kt);
            await _iOpenLockRequestRepository.Commit();
         
            for(int i = 0; i < 30; i++)
            {
                var ktOpen = await _iDockRepository.AnyAsync(x => x.Id == cm.DockId && x.LockStatus == false);
                if(ktOpen == true)
                {
                    await AddLog(0, $"Mở khóa ({ktDock.Id},{ktDock.SerialNumber})  => mở thành công, lý do => {cm.Feedback}", LogType.Other);
                    return new ApiResponseData<OpenLockHistory>() { Status = 1 };
                }
                await Task.Delay(500);
            }
            await AddLog(0, $"Mở khóa ({ktDock.Id},{ktDock.SerialNumber}) => không mở được, lý do => {cm.Feedback}", LogType.Other);
            return new ApiResponseData<OpenLockHistory>() { Status = 0 };
        }

        public async Task<ApiResponseData<Transaction>> BookingCancel(BookingCancelCommand cm)
        {
            try
            {
                // Chỉ được hủy khi ở trạng thái đang đi
                var booking = await _iBookingRepository.SearchOneAsync(x => x.Id == cm.BookingId && x.Status == EBookingStatus.Start);
                if (booking == null)
                {
                    return new ApiResponseData<Transaction>() { Data = null, Status = 0 };
                }
                // Kiểm tra khóa đóng chưa
                var dock = await _iDockRepository.SearchOneAsync(x => x.Id == booking.DockId);
                if (dock == null || dock.LockStatus == false && cm.CheckCloseDock)
                {
                    return new ApiResponseData<Transaction>() { Data = null, Status = 0, Message = "Khóa xe chưa đóng, nhân viên yêu cầu khách hàng đóng khóa rồi thực hiện lại" };
                }
                int stationId = 0;
                // Kiểm tra khóa có trong trạm không
                if(cm.CheckInStation)
                {
                    var kt = await InStationVerified(dock.Lat ?? 0, dock.Long ?? 0);
                    if (kt.Item1 == false)
                    {
                        return new ApiResponseData<Transaction>() { Data = null, Status = 0, Message = $"Xe không ở trong trạm ({kt.Item3}/{kt.Item2}), khách truy cập app để hệ thống kiểm tra vị trí" };
                    }
                    else
                    {
                        stationId = kt.Item4;
                    }
                }    

                booking.Status = EBookingStatus.CancelByAdmin;
                booking.Feedback = cm.Feedback;
                booking.EndTime = DateTime.Now;
                booking.TotalMinutes = Function.TotalMilutes(booking.StartTime, booking.EndTime.Value);
                booking.DateOfPayment = DateTime.Now;
                booking.StationOut = stationId;
                _iBookingRepository.Update(booking);
                await _iBookingRepository.Commit();
                var dataAdd = await MoveBookingToTransaction(booking, 0);
               // await SendDevice(booking.AccountId, DockEventType.EndBooking);
                await AddLog(dataAdd.Id, $"Quản trị hủy chuyến đi thủ công ({dataAdd.TransactionCode})", LogType.Other);

                await _iNotificationJobRepository.SendNow(booking.AccountId, "TNGO thông báo", $"{string.Format(GetNotificationText(ENotificationTempType.HuyChuyenDi)) }", UserInfo.UserId, DockEventType.CancelBooking, null);
                var data = await _iEventSystemRepository.EventSend(_logSettings.Value,EEventSystemType.M_3004_HuyChuyen, EEventSystemWarningType.Info, booking.AccountId, booking.TransactionCode, booking.ProjectId, booking.StationIn, booking.BikeId, booking.DockId, EEventSystemType.M_3004_HuyChuyen.ToEnumGetDisplayName(), $"Nhân viên #{UserInfo.UserId} hủy chuyến đi '{booking.TransactionCode}'");
                if (data != null)
                {
                    await _hubContext.Clients.All.SendAsync("ReceiveMessageEventSystemSendMessage", 0, data);
                }
                return new ApiResponseData<Transaction>() { Data = dataAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingService.BookingCancel", ex.ToString());
                return new ApiResponseData<Transaction>();
            }
        }

        private LatLngModel StringToLatLng(string lat_lng)
        {
            try
            {
                if (string.IsNullOrEmpty(lat_lng))
                {
                    return null;
                }
                string[] comps = lat_lng.Split(';');
                return new LatLngModel
                {
                    Lat = Convert.ToDouble(comps[0]),
                    Lng = Convert.ToDouble(comps[1])
                };
            }
            catch
            {
                return null;
            }
        }

        private async Task<Tuple<bool, double, double, int >> InStationVerified(double lat, double lng)
        {

            var stations = (await StationDistance(lat, lng)).OrderBy(x => x.Distance);
            var station = stations.FirstOrDefault();
            if (station == null)
            {
                return new Tuple<bool, double, double, int>(false, 0, 0, 0);
            }
            else if (station.Distance < 0)
            {
                return new Tuple<bool, double, double, int>(false, station.ReturnDistance, 0, station.Id);
            }
            else if (station.Distance > station.ReturnDistance)
            {
                return new Tuple<bool, double, double, int>(false, station.ReturnDistance, Math.Round(station.Distance,2), station.Id);
            }
            else
            {
                return new Tuple<bool, double, double, int>(true, station.ReturnDistance, Math.Round(station.Distance, 2), station.Id);
            }
        }

        private async Task<List<Station>> StationDistance(double lat, double lng)
        {
            double delta = 0.01;
            var stations = await _iStationRepository.FindByLatLng(lat - delta, lat + delta, lng - delta, lng + delta);
            foreach (var station in stations)
            {
                if (station.Lat != 0 && station.Lng != 0)
                {
                    var distance = Function.DistanceInMeter(lat, lng, station.Lat, station.Lng);
                    station.Distance = distance;
                }
            }
            return stations;
        }

        private async Task<Transaction> MoveBookingToTransaction(Booking booking, decimal chargeDebt)
        {
            var dlAdd = new Transaction
            {
                AccountId = booking.AccountId,
                BikeId = booking.BikeId,
                ChargeInAccount = booking.ChargeInAccount,
                ChargeInSubAccount = booking.ChargeInSubAccount,
                CreatedDate = booking.CreatedDate,
                CustomerGroupId = booking.CustomerGroupId,
                DateOfPayment = booking.DateOfPayment,
                DockId = booking.DockId,
                StationOut = booking.StationOut,
                Status = booking.Status,
                TransactionCode = booking.TransactionCode,
                Vote = booking.Vote,
                Feedback = booking.Feedback,
                TotalMinutes = booking.TotalMinutes,
                TotalPrice = booking.TotalPrice,
                EndTime = booking.EndTime,
                InvestorId = booking.InvestorId,
                KM = booking.KM,
                KCal = booking.KCal,
                Prepaid_EndDate = booking.Prepaid_EndDate,
                Prepaid_EndTime = booking.Prepaid_EndTime,
                Note = booking.Note,
                KG = booking.KG,
                Prepaid_MinutesSpent = booking.Prepaid_MinutesSpent,
                Prepaid_StartDate = booking.Prepaid_StartDate,
                Prepaid_StartTime = booking.Prepaid_StartTime,
                ProjectId = booking.ProjectId,
                StartTime = booking.StartTime,
                StationIn = booking.StationIn,
                TicketPrepaidCode = booking.TicketPrepaidCode,
                TicketPriceId = booking.TicketPriceId,
                TicketPrice_AllowChargeInSubAccount = booking.TicketPrice_AllowChargeInSubAccount,
                TicketPrice_BlockPerMinute = booking.TicketPrice_BlockPerMinute,
                TicketPrice_BlockPerViolation = booking.TicketPrice_BlockPerViolation,
                TicketPrice_BlockViolationValue = booking.TicketPrice_BlockViolationValue,
                TicketPrice_LimitMinutes = booking.TicketPrice_LimitMinutes,
                TicketPrepaidId = booking.TicketPrepaidId,
                TicketPrice_IsDefault = booking.TicketPrice_IsDefault,
                TicketPrice_TicketType = booking.TicketPrice_TicketType,
                TicketPrice_TicketValue = booking.TicketPrice_TicketValue,
                TicketPrice_Note = booking.TicketPrice_Note,
                TripPoint = booking.TripPoint,
                UpdatedDate = DateTime.Now,
                UpdatedUser = UserInfo.UserId,
                ChargeDebt = chargeDebt,
                EstimateChargeInAccount = booking.EstimateChargeInAccount,
                EstimateChargeInSubAccount = booking.EstimateChargeInSubAccount,
                VehicleRecallPrice = booking.VehicleRecallPrice,
                VehicleRecallStatus = booking.VehicleRecallStatus,
                ReCallM = booking.ReCallM,
                RFID = booking.RFID,
                IsOpenRFID = booking.IsOpenRFID,
                IsEndRFID = booking.IsEndRFID,
                DiscountCodeId = booking.DiscountCodeId,
                DiscountCode_Type = booking.DiscountCode_Type,
                DiscountCode_CampaignId = booking.DiscountCode_CampaignId,
                DiscountCode_Percent = booking.DiscountCode_Percent,
                DiscountCode_Point = booking.DiscountCode_Point,
                DiscountCode_Price = booking.DiscountCode_Price,
                DiscountCode_ProjectId = booking.DiscountCode_ProjectId,
                IsDockNotOpen = booking.IsDockNotOpen,
                IsForgotrReturnTheBike = booking.IsForgotrReturnTheBike,
                OpenLockTransaction = booking.OpenLockTransaction,
                GroupTransactionCode = booking.GroupTransactionCode,
                GroupTransactionOrder = booking.GroupTransactionOrder
            };

            // Thêm một bản ghi vào transaction, sau đó xóa booking đi
            await _iTransactionRepository.AddAsync(dlAdd);
            await _iTransactionRepository.Commit();

            _iBookingRepository.DeleteWhere(x => x.TransactionCode == dlAdd.TransactionCode && x.InvestorId == dlAdd.InvestorId && x.AccountId == dlAdd.AccountId);
            await _iBookingRepository.Commit();

            try
            {
                // Chuyên đi kết thúc thống thường, chuyến đi kết thúc bởi admin, chuyến đi thu hồi không nợ cước mới có thể gim
                if ((dlAdd.Status == EBookingStatus.End || dlAdd.Status == EBookingStatus.EndByAdmin || dlAdd.Status == EBookingStatus.ReCall) && _baseSettings.Value.EnableDiscountTransaction)
                {
                    await _iCyclingWeekRepository.UpdateFlag(dlAdd.AccountId, dlAdd.EndTime ?? DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingService.MoveBookingToTransaction.UpdateFlag", ex.ToString());
            }
            return dlAdd;
        }

        public async Task<ApiResponseData<List<Booking>>> SearchPageAsync(int? pageIndex, int? pageSize, string key, int? investorId, int? projectId, int? customerGroupId, int? stationIn, int? accountId, string from, string to, ETicketType? ticketPrice_TicketType, string sortby, string sorttype)
        {
            try
            {
                var data = await _iBookingRepository.SearchPagedAsync(
                   pageIndex ?? 1,
                   pageSize ?? 10,
                   key,
                   x =>
                        (investorId == null || x.InvestorId == investorId)
                        && (ticketPrice_TicketType == null || x.TicketPrice_TicketType == ticketPrice_TicketType)
                        && (projectId == null || x.ProjectId == projectId)
                        && (customerGroupId == null || x.CustomerGroupId == customerGroupId)
                        && (stationIn == null || x.StationIn == stationIn)
                        && (accountId == null || x.AccountId == accountId)
                        && (from == null || x.EndTime >= Convert.ToDateTime(from))
                        && (to == null || x.EndTime < Convert.ToDateTime(to).AddDays(1))
                   ,
                   OrderByExtention(sortby, sorttype));

                var bikes = await _iBikeRepository.Search(x => data.Data.Select(x => x.BikeId).Contains(x.Id));
                var accounts = await _iAccountRepository.Search(x => data.Data.Select(x => x.AccountId).Contains(x.Id));
                var customerGroups = await _customerGroupRepository.Search(x => data.Data.Select(x => x.CustomerGroupId).Contains(x.Id));
                var docks = await _iDockRepository.Search(x => data.Data.Select(x => x.DockId).Contains(x.Id));
                foreach (var item in data.Data)
                {
                    item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                    item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                    item.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);
                    if (item.Bike != null)
                    {
                        item.Bike.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                    }

                    var ticketPrice = new TicketPrice()
                    {
                        Id = item.TicketPriceId,
                        CustomerGroupId = item.CustomerGroupId,
                        BlockPerMinute = item.TicketPrice_BlockPerMinute,
                        AllowChargeInSubAccount = item.TicketPrice_AllowChargeInSubAccount,
                        BlockPerViolation = item.TicketPrice_BlockPerViolation,
                        BlockViolationValue = item.TicketPrice_BlockViolationValue,
                        IsDefault = item.TicketPrice_IsDefault,
                        TicketType = item.TicketPrice_TicketType,
                        TicketValue = item.TicketPrice_TicketValue,
                        LimitMinutes = item.TicketPrice_LimitMinutes,
                        Name = GetDisplayName(item.TicketPrice_TicketType)
                    };

                    item.TicketPrice = ticketPrice;
                    item.TicketPrice_Note = _iTransactionRepository.GetTicketPriceString(ticketPrice);

                    item.TotalMinutes = Convert.ToInt32((DateTime.Now - item.StartTime).TotalMinutes);
                    item.TotalPrice = _iTransactionRepository.ToTotalPrice(
                        item.TicketPrice_TicketType,
                        item.StartTime,
                        DateTime.Now,
                        item.TicketPrice_TicketValue,
                        item.TicketPrice_LimitMinutes,
                        item.TicketPrice_BlockPerMinute,
                        item.TicketPrice_BlockPerViolation,
                        item.TicketPrice_BlockViolationValue,
                        item.Prepaid_EndTime ?? DateTime.Now,
                        item.Prepaid_MinutesSpent, new DiscountCode { Percent = item.DiscountCode_Percent ?? 0, Point = item.DiscountCode_Point ?? 0, Type = item.DiscountCode_Type ?? EDiscountCodeType.Point })?.Price ?? 0;

                }

                if (accountId != null && accountId > 0)
                {
                    data.OutData = await _iAccountRepository.SearchOneAsync(x => x.Id == accountId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Booking>>();
            }
        }

        public async Task<ApiResponseData<Booking>> BookingCalculate(int id)
        {
            try
            {
                var data = await _iBookingRepository.SearchOneAsync(x => x.Id == id);
                if(data != null)
                {
                    data.Account = await _iAccountRepository.SearchOneAsync(x => x.Id.Equals(data.AccountId));
                    data.Bike = await _iBikeRepository.SearchOneAsync(x => x.Id.Equals(data.BikeId));
                    data.CustomerGroup = await _customerGroupRepository.SearchOneAsync(x => x.Id.Equals(data.CustomerGroupId));
                    var ticketInfo = new TicketPrice()
                    {
                        Id = data.TicketPriceId,
                        CustomerGroupId = data.CustomerGroupId,
                        BlockPerMinute = data.TicketPrice_BlockPerMinute,
                        AllowChargeInSubAccount = data.TicketPrice_AllowChargeInSubAccount,
                        BlockPerViolation = data.TicketPrice_BlockPerViolation,
                        BlockViolationValue = data.TicketPrice_BlockViolationValue,
                        IsDefault = data.TicketPrice_IsDefault,
                        TicketType = data.TicketPrice_TicketType,
                        TicketValue = data.TicketPrice_TicketValue,
                        LimitMinutes = data.TicketPrice_LimitMinutes,
                        Name = GetDisplayName(data.TicketPrice_TicketType)
                    };

                    data.TicketPrice = ticketInfo;

                    data.TicketPrice_Note = _iTransactionRepository.GetTicketPriceString(ticketInfo);

                    data.TotalMinutes = Function.TotalMilutes(data.StartTime, DateTime.Now);
                    data.Wallet = await _iWalletRepository.GetWalletAsync(data.AccountId);
                    data.TotalPrice = _iTransactionRepository.ToTotalPrice(
                        data.TicketPrice_TicketType,
                        data.StartTime,
                        DateTime.Now,
                        data.TicketPrice_TicketValue,
                        data.TicketPrice_LimitMinutes,
                        data.TicketPrice_BlockPerMinute,
                        data.TicketPrice_BlockPerViolation,
                        data.TicketPrice_BlockViolationValue,
                        data.Prepaid_EndTime ?? DateTime.Now,
                        data.Prepaid_MinutesSpent, 
                        new DiscountCode { Percent = data.DiscountCode_Percent ?? 0, Point = data.DiscountCode_Point ?? 0, Type = data.DiscountCode_Type ?? EDiscountCodeType.Point }
                        )?.Price ?? 0;

                    var dock = await _iDockRepository.SearchOneAsync(x => x.Id == data.DockId);
                    data.Dock = dock;
                    data.Bike.Dock = data.Dock;
                    var project = await _iProjectRepository.SearchOneAsync(x => x.Id == data.ProjectId);
                    var totalMet = Function.DistanceInMeter(dock?.Lat ?? 0, dock?.Long ?? 0, project.HeadquartersLat ?? 0, project.HeadquartersLng ?? 0);
                    var kmPrice = await _iVehicleRecallPriceRepository.SearchOneAsync(x=>x.To >= totalMet && x.From <= totalMet);
                    data.KM = totalMet;
                    if (kmPrice != null)
                    {
                        data.VehicleRecallPrice = kmPrice.Price;
                    }   
                    else
                    {
                        data.VehicleRecallPrice = 20000;
                    }
                }    

                return new ApiResponseData<Booking>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingSevice.Get", ex.ToString());
                return new ApiResponseData<Booking>();
            }
        }

        public async Task<ApiResponseData<Booking>> BookingAdd(BookingAddCommand cm, EBookingFailStatus status)
        {
            try
            {
                var bookingF = await _iBookingFailRepository.SearchOneAsync(x => x.Id == cm.Id);
                if (bookingF == null)
                {
                    return new ApiResponseData<Booking>() { Data = null, Status = 0, Message = "Dữ liệu không tồn tại" };
                }

                if (bookingF.Status != EBookingFailStatus.ViewAdmin)
                {
                    return new ApiResponseData<Booking>() { Data = null, Status = 0, Message = "Chi có trạng thái chờ xử lý mới có thể thực hiện thao tác này" };
                }
                // Kiểm tra khách hàng phải không trong chuyến đi nào mới được

                var kt = await _iBookingRepository.SearchOneAsync(x => x.AccountId == bookingF.AccountId && x.Status == EBookingStatus.Start);
                if (kt != null)
                {
                    return new ApiResponseData<Booking>() { Data = null, Status = 0, Message = "Khách hàng này đang trong một chuyến đi khác, có thể hủy nghi vấn này" };
                }

                if(status == EBookingFailStatus.AddBooking)
                {
                    var dlAdd = new Booking
                    {
                        AccountId = bookingF.AccountId,
                        StartTime = bookingF.CreatedDate,
                        BikeId = bookingF.BikeId,
                        CreatedDate = DateTime.Now,
                        ChargeInAccount = bookingF.ChargeInAccount,
                        TicketPrice_BlockPerMinute = bookingF.TicketPrice_BlockPerMinute,
                        ChargeInSubAccount = bookingF.ChargeInSubAccount,
                        CustomerGroupId = bookingF.CustomerGroupId,
                        DateOfPayment = null,
                        DockId = bookingF.DockId,
                        EndTime = null,
                        EstimateChargeInAccount = 0,
                        EstimateChargeInSubAccount = 0,
                        InvestorId = bookingF.InvestorId,
                        Feedback = null,
                        KCal = 0,
                        KM = 0,
                        KG = 0,
                        Note = cm.Note,
                        Prepaid_EndDate = bookingF.Prepaid_EndDate,
                        Prepaid_MinutesSpent = bookingF.Prepaid_MinutesSpent,
                        Prepaid_EndTime = bookingF.Prepaid_EndTime,
                        TicketPrepaidCode = bookingF.TicketPrepaidCode,
                        Vote = 0,
                        VehicleRecallStatus = EVehicleRecallStatus.Default,
                        TicketPrepaidId = bookingF.TicketPrepaidId,
                        Prepaid_StartDate = bookingF.Prepaid_StartDate,
                        Prepaid_StartTime = bookingF.Prepaid_StartTime,
                        ProjectId = bookingF.ProjectId,
                        StationOut = null,
                        StationIn = bookingF.StationIn,
                        TicketPriceId = bookingF.TicketPriceId,
                        Status = EBookingStatus.Start,
                        TicketPrice_IsDefault = bookingF.TicketPrice_IsDefault,
                        TicketPrice_LimitMinutes = bookingF.TicketPrice_LimitMinutes,
                        TicketPrice_BlockPerViolation = bookingF.TicketPrice_BlockPerViolation,
                        TicketPrice_AllowChargeInSubAccount = bookingF.TicketPrice_AllowChargeInSubAccount,
                        TicketPrice_Note = bookingF.TicketPrice_Note,
                        VehicleRecallPrice = 0,
                        TicketPrice_BlockViolationValue = bookingF.TicketPrice_BlockViolationValue,
                        TicketPrice_TicketValue = bookingF.TicketPrice_TicketValue,
                        TicketPrice_TicketType = bookingF.TicketPrice_TicketType,
                        TransactionCode = bookingF.TransactionCode,
                        TotalMinutes = 0,
                        TotalPrice = 0,
                        TripPoint = 0
                    };
                    await _iBookingRepository.AddAsync(dlAdd);
                    await _iBookingRepository.Commit();

                    bookingF.Status = status;
                    bookingF.UpdatedDate = DateTime.Now;
                    bookingF.Note = cm.Note;
                    bookingF.UpdatedUser = UserInfo.UserId;
                    _iBookingFailRepository.Update(bookingF);
                    await _iBookingFailRepository.Commit();
                    await AddLog(bookingF.Id, $"Thêm chuyến đi từ giao dịch nghi vấn '{dlAdd.TransactionCode}'", LogType.Other);

                    return new ApiResponseData<Booking>() { Data = dlAdd, Status = 1, Message = "Tạo chuyến đi thành công" };
                }
                else if(status == EBookingFailStatus.CancelAdmin)
                {
                    bookingF.Status = status;
                    bookingF.UpdatedDate = DateTime.Now;
                    bookingF.Note = cm.Note;
                    bookingF.UpdatedUser = UserInfo.UserId;
                    _iBookingFailRepository.Update(bookingF);
                    await _iBookingFailRepository.Commit();

                    await AddLog(bookingF.Id, $"Hủy giao dịch nghi vấn '{bookingF.TransactionCode}'", LogType.Other);
                    return new ApiResponseData<Booking>() { Status = 1, Message = "Hủy nghi vấn thành công" };
                }
                return new ApiResponseData<Booking>() {Status = 0, Message = "Thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingSevice.BookingAdd", ex.ToString());
                return new ApiResponseData<Booking>();
            }
        }

        public async Task<ApiResponseData<Transaction>> BookingCheckOut(BookingEndCommand cm, decimal vehicleRecallPrice, double reCallM)
        {
            try
            {
                decimal debit = 0;

                var booking = await _iBookingRepository.SearchOneAsync(x => x.Id == cm.BookingId && x.Status == EBookingStatus.Start);
                if (booking == null)
                {
                    return new ApiResponseData<Transaction>() { Data = null, Status = 0 };
                }

                var dock = await _iDockRepository.SearchOneAsync(x => x.Id == booking.DockId);
                if (dock == null || dock.LockStatus == false && cm.CheckCloseDock)
                {
                    return new ApiResponseData<Transaction>() { Data = null, Status = 0, Message = "Khóa xe chưa đóng, nhân viên yêu cầu khách hàng đóng khóa rồi thực hiện lại" };
                }

                int stationId = 0;
                // Kiểm tra khóa có trong trạm không
                if (cm.CheckInStation)
                {
                    var kt = await InStationVerified(dock.Lat ?? 0, dock.Long ?? 0);
                    if (kt.Item1 == false)
                    {
                        return new ApiResponseData<Transaction>() { Data = null, Status = 0, Message = $"Xe không ở trong trạm ({kt.Item3}/{kt.Item2}), khách truy cập app để hệ thống kiểm tra vị trí" };
                    }
                    else
                    {
                        stationId = kt.Item4;
                    }
                }
                booking.EndTime = DateTime.Now;
                // Lấy ví của người dùng
                var wInfo = await _iWalletRepository.GetWalletAsync(booking.AccountId);
                var totalPrice = _iTransactionRepository.ToTotalPrice(booking.TicketPrice_TicketType, booking.StartTime, booking.EndTime.Value, booking.TicketPrice_TicketValue, booking.TicketPrice_LimitMinutes, booking.TicketPrice_BlockPerMinute, booking.TicketPrice_BlockPerViolation, booking.TicketPrice_BlockViolationValue, booking.Prepaid_EndTime ?? DateTime.Now, booking.Prepaid_MinutesSpent, new DiscountCode { Percent = booking.DiscountCode_Percent ?? 0, Point = booking.DiscountCode_Point ?? 0, Type = booking.DiscountCode_Type ?? EDiscountCodeType.Point })?.Price ?? 0;
                totalPrice += vehicleRecallPrice;
                // Chia tiền 2 loại tài khoản
                // Có 2 trường hợp chia 1 là ko đủ thành toán, 2 là đủ thành toán, với trường hợp không đủ thanh toán thì ta phải ưu tiên nợ tài khoản km
                var splitTotalPrice = _iTransactionRepository.SplitTotalPrice(totalPrice, booking.TicketPrice_AllowChargeInSubAccount, wInfo);
                if ((wInfo.Balance + wInfo.SubBalance) < totalPrice)
                {
                    debit = totalPrice - (wInfo.Balance + wInfo.SubBalance);
                }

                // Chi phí thực tế
                booking.ChargeInAccount = splitTotalPrice.Item1;
                booking.ChargeInSubAccount = splitTotalPrice.Item2 - debit;
                // Chi phí ước tính đủ
                booking.EstimateChargeInAccount = splitTotalPrice.Item1;
                booking.EstimateChargeInSubAccount = splitTotalPrice.Item2;
                booking.TotalPrice = totalPrice;
                booking.StationOut = 0;
                booking.DateOfPayment = DateTime.Now;
                booking.KG = 0;
                booking.KCal = 0;
                booking.KM = 0;
            
                // Nợ cước sẽ ko cho + chuyến đi
                if(debit <= 0)
                {
                    booking.TripPoint = booking.TicketPrice_TicketType != ETicketType.Block ? 0 : ((totalPrice / 1000) * _baseSettings.Value.AddTripPoint);
                }

                booking.TotalMinutes = Function.TotalMilutes(booking.StartTime, booking.EndTime.Value);
                booking.VehicleRecallStatus = EVehicleRecallStatus.Processed;
                booking.VehicleRecallPrice = vehicleRecallPrice;
                booking.ReCallM = reCallM;
                booking.Note = cm.Feedback;
                booking.StationOut = stationId;
                booking.IsEndRFID = false;

                if(debit > 0)
                {
                    booking.Status = EBookingStatus.Debt;
                }    
                else if(vehicleRecallPrice > 0)
                {
                    booking.Status = EBookingStatus.ReCall;
                }
                else
                {
                    booking.Status = EBookingStatus.EndByAdmin;
                }    

                _iBookingRepository.Update(booking);
                await _iBookingRepository.Commit();

                if (booking.TicketPrice_TicketType != ETicketType.Block)
                {
                    // Trừ số lượng phút của ngày
                    await _iTicketPriceRepository.PrepaidUpdatePriceTypeDay(booking.AccountId, booking.TicketPrepaidId, booking.TotalMinutes);
                }

                var outTransaction = await MoveBookingToTransaction(booking, debit);
                if (outTransaction != null)
                {
                    var wt = new WalletTransaction
                    {
                        AccountId = booking.AccountId,
                        CampaignId = 0,
                        CreatedDate = DateTime.Now,
                        Status = EWalletTransactionStatus.Done,
                        TransactionCode = booking.TransactionCode,
                        Note = $"Trừ điểm chuyến đi, loại vé {outTransaction.TicketPrice_TicketType.ToEnumGetDisplayName()} { (debit > 0 ? $", khách hàng nợ {Function.FormatMoney(debit)} điểm cho chuyến đi này" : "")} { (vehicleRecallPrice <= 0 ? "": $", phát sinh thêm chi phí thu hồi xe {Function.FormatMoney(vehicleRecallPrice)}")}",
                        TransactionId = outTransaction.Id,
                        Type = EWalletTransactionType.Paid,
                        WalletId = wInfo.Id,
                        OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.Paid),
                        Amount = splitTotalPrice.Item1,
                        IsAsync = false,
                        SubAmount = splitTotalPrice.Item2 - debit,
                        TotalAmount = totalPrice,
                        TripPoint = outTransaction.TripPoint,
                        DepositEvent_SubAmount = 0,
                        ReqestId = $"{DateTime.Now:yyyyMMddHHMMssfff}",
                        PadTime = DateTime.Now,
                        DebtAmount = debit,
                        Wallet_Balance = wInfo.Balance,
                        Wallet_SubBalance = wInfo.SubBalance,
                        Wallet_HashCode = wInfo.HashCode,
                        Wallet_TripPoint = wInfo.TripPoint
                    };
                    wt.HashCode = Function.TransactionHash(wt.OrderIdRef, wt.Amount, wt.SubAmount, wt.TripPoint, _baseSettings.Value.TransactionSecretKey);
                    await _iIWalletTransactionRepository.AddAsync(wt);
                    await _iIWalletTransactionRepository.Commit();
                }
                wInfo = await _iWalletRepository.UpdateAddWalletAsync(booking.AccountId, -splitTotalPrice.Item1, -splitTotalPrice.Item2, _baseSettings.Value.AddTripPoint, _baseSettings.Value.TransactionSecretKey, false, false, 0);
                await _iWalletRepository.AddDayExpirys(outTransaction.AccountId, outTransaction.TotalPrice, 0, true);
                //var gps = await FilterGPS(outTransaction.Id, outTransaction.TransactionCode, outTransaction.DockId, outTransaction.StartTime, outTransaction.EndTime ?? DateTime.Now, outTransaction.AccountId);
               //  await _iNotificationJobRepository.SendNow(outTransaction.AccountId, "TNGO thông báo", $"{string.Format(GetNotificationText(ENotificationTempType.KetThucChuyenDi), $"{ Function.FormatMoney(outTransaction.TotalPrice) }") }", UserInfo.UserId, DockEventType.EndBooking, null);
                await _iNotificationJobRepository.SendNotificationByAccount(_notificationTempSettings, outTransaction.AccountId, ENotificationTempType.KetThucChuyenDi, $"{ Function.FormatMoney(outTransaction.TotalPrice) }");
                if (vehicleRecallPrice <= 0)
                {
                    var data = await _iEventSystemRepository.EventSend(_logSettings.Value, EEventSystemType.M_3005_KetThucGiaoDichCuongBuc, EEventSystemWarningType.Info, booking.AccountId, booking.TransactionCode, booking.ProjectId, booking.StationIn, booking.BikeId, booking.DockId, EEventSystemType.M_3004_HuyChuyen.ToEnumGetDisplayName(), $"Nhân viên #{UserInfo.UserId} kết thúc chuyến đi '{booking.TransactionCode}' tổng phí {Function.FormatMoney(booking.TotalPrice)}");
                    if (data != null)
                    {
                        await _hubContext.Clients.All.SendAsync("ReceiveMessageEventSystemSendMessage", 0, data);
                    }
                }
                else
                {
                   var data = await _iEventSystemRepository.EventSend(_logSettings.Value, EEventSystemType.M_3006_YeuCauThuHoiXe, EEventSystemWarningType.Info, booking.AccountId, booking.TransactionCode, booking.ProjectId, booking.StationIn, booking.BikeId, booking.DockId, EEventSystemType.M_3004_HuyChuyen.ToEnumGetDisplayName(), $"Nhân viên #{UserInfo.UserId} thu hồi xe thành công '{booking.TransactionCode}' tổng phí {Function.FormatMoney(booking.TotalPrice)}");
                    if (data != null)
                    {
                        await _hubContext.Clients.All.SendAsync("ReceiveMessageEventSystemSendMessage", 0, data);
                    }
                }

                await AddLog(outTransaction.Id, vehicleRecallPrice <= 0 ? $"{outTransaction.Status.ToEnumGetDisplayName()} - " + $"Quản trị kết thúc chuyến đi thủ công ({outTransaction.TransactionCode})" : $"Quản trị thu hồi xe chuyến đi ({outTransaction.TransactionCode})", LogType.Other);

                return new ApiResponseData<Transaction> { Status = 1, Data = outTransaction, Message = "Thực hiện thao tác thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError($"BookingSevice.BookingCheckOut {ex}");
                return new ApiResponseData<Transaction>();
            }
        }

        public async Task<ApiResponseData<RFIDBooking>> RFIDBookingCheckOut(RFIDBookingEndCommand cm)
        {
            try
            {

                var rfidBooking = await _iRFIDBookingRepository.SearchOneAsync(x => x.Id == cm.RFIDBookingId && x.Status == ERFIDCoordinatorStatus.Start);
                if (rfidBooking == null)
                {
                    return new ApiResponseData<RFIDBooking>() { Data = null, Status = 0 };
                }

                var dock = await _iDockRepository.SearchOneAsync(x => x.Id == rfidBooking.DockId);
                if (dock == null || dock.LockStatus == false && cm.CheckCloseDock)
                {
                    return new ApiResponseData<RFIDBooking>() { Data = null, Status = 0, Message = "Khóa xe chưa đóng, nhân viên yêu cầu khách hàng đóng khóa rồi thực hiện lại" };
                }

                int stationId = 0;
                // Kiểm tra khóa có trong trạm không
                if (cm.CheckInStation)
                {
                    var kt = await InStationVerified(dock.Lat ?? 0, dock.Long ?? 0);
                    if (kt.Item1 == false)
                    {
                        return new ApiResponseData<RFIDBooking>() { Data = null, Status = 0, Message = $"Xe không ở trong trạm ({kt.Item3}/{kt.Item2}), khách truy cập app để hệ thống kiểm tra vị trí" };
                    }
                    else
                    {
                        stationId = kt.Item4;
                    }
                }

                rfidBooking.Status = ERFIDCoordinatorStatus.End;
                rfidBooking.EndTime = DateTime.Now;
                rfidBooking.EndLat = dock.Lat;
                rfidBooking.EndLng = dock.Long;
                rfidBooking.UpatedUser = UserInfo.UserId;
                rfidBooking.UpdatedDate = DateTime.Now;

                _iRFIDBookingRepository.Update(rfidBooking);
                await _iRFIDBookingRepository.Commit();
                // A
                await _iRFIDBookingRepository.RFIDTransactionAdd(rfidBooking);

                await AddLog(rfidBooking.Id,  $"Quản trị kết thúc mở xe bằng RFID  '{rfidBooking.RFID}', '{rfidBooking.TransactionCode}'", LogType.Other);

                return new ApiResponseData<RFIDBooking> { Status = 1, Data = rfidBooking, Message = "Thực hiện thao tác thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingSevice.BookingCheckOut", ex.ToString());
                return new ApiResponseData<RFIDBooking>();
            }
        }

        public async Task<ApiResponseData<Transaction>> ReCall(ReCallCommand cm)
        {
            try
            {
                var booking = await _iBookingRepository.SearchOneAsync(x => x.Id == cm.BookingId && x.Status == EBookingStatus.Start);
                if (booking == null)
                {
                    return new ApiResponseData<Transaction>() { Data = null, Status = 0 };
                }

                // Lấy ví của người dùng
                var wInfo = await _iWalletRepository.GetWalletAsync(booking.AccountId);
                var totalPrice = _iTransactionRepository.ToTotalPrice(booking.TicketPrice_TicketType, booking.StartTime, DateTime.Now, booking.TicketPrice_TicketValue, booking.TicketPrice_LimitMinutes, booking.TicketPrice_BlockPerMinute, booking.TicketPrice_BlockPerViolation, booking.TicketPrice_BlockViolationValue, booking.Prepaid_EndTime ?? DateTime.Now, booking.Prepaid_MinutesSpent, new DiscountCode { Percent = booking.DiscountCode_Percent ?? 0, Point = booking.DiscountCode_Point ?? 0, Type = booking.DiscountCode_Type ?? EDiscountCodeType.Point })?.Price ?? 0;
                var dock = await _iDockRepository.SearchOneAsync(x => x.Id == booking.DockId);
                booking.Dock = dock;
                var project = await _iProjectRepository.SearchOneAsync(x => x.Id == booking.ProjectId);
                var totalMet = Function.DistanceInMeter(dock?.Lat ?? 0, dock?.Long ?? 0, project.HeadquartersLat ?? 0, project.HeadquartersLng ?? 0);
                var kmPrice = await _iVehicleRecallPriceRepository.SearchOneAsync(x => x.To >= totalMet && x.From <= totalMet);
                if (kmPrice != null)
                {
                    booking.VehicleRecallPrice = kmPrice.Price;
                }
                else
                {
                    booking.VehicleRecallPrice = 20000;
                }

                if(booking.Dock?.LockStatus == false && cm.CheckCloseDock)
                {
                    return new ApiResponseData<Transaction>() { Data = null, Status = 0, Message = "Khóa xe chưa đóng, nhân viên yêu cầu khách hàng đóng khóa rồi thực hiện lại" };
                }

                return await BookingCheckOut(new BookingEndCommand { BookingId = cm.BookingId, EndDate = DateTime.Now, Feedback = cm.Feedback }, booking.VehicleRecallPrice, totalMet);
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingSevice.ReCall", ex.ToString());
                return new ApiResponseData<Transaction>();
            }
        }

        private string GetNotificationText(ENotificationTempType type)
        {
            try
            {
                return _notificationTempSettings.FirstOrDefault(x=>x.Language == "vi")?.Data?.FirstOrDefault(x => x.Type == type)?.Body;
            }
            catch
            {
                return type.ToString();
            }
        }

        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key) || x.Code.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key)
        {
            try
            {
                var data = await _iUserRepository.SearchTop(30, x => (key == null || key == " " || x.FullName.Contains(key) || x.PhoneNumber.Contains(key) || x.Email.Contains(key) || x.UserName.Contains(key) || x.DisplayName.Contains(key) || x.Code.Contains(key)) && !x.IsSuperAdmin, null, x=> new ApplicationUser { Id = x.Id, FullName = x.FullName, Code = x.Code, Email = x.Email, PhoneNumber = x.PhoneNumber, DisplayName = x.DisplayName  });
                return new ApiResponseData<List<ApplicationUser>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingSevice.SearchByUser", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }

        public async Task<ApiResponseData<Booking>> GetById(int id)
        {
            try
            {
                var data = await _iBookingRepository.SearchOneAsync(x => x.Id == id);
                data.Account = await _iAccountRepository.SearchOneAsync(x => x.Id.Equals(data.AccountId));
                data.Bike = await _iBikeRepository.SearchOneAsync(x => x.Id.Equals(data.BikeId));
                data.CustomerGroup = await _customerGroupRepository.SearchOneAsync(x => x.Id.Equals(data.CustomerGroupId));
                var ticketInfo = new TicketPrice()
                {
                    Id = data.TicketPriceId,
                    CustomerGroupId = data.CustomerGroupId,
                    BlockPerMinute = data.TicketPrice_BlockPerMinute,
                    AllowChargeInSubAccount = data.TicketPrice_AllowChargeInSubAccount,
                    BlockPerViolation = data.TicketPrice_BlockPerViolation,
                    BlockViolationValue = data.TicketPrice_BlockViolationValue,
                    IsDefault = data.TicketPrice_IsDefault,
                    TicketType = data.TicketPrice_TicketType,
                    TicketValue = data.TicketPrice_TicketValue,
                    LimitMinutes = data.TicketPrice_LimitMinutes,
                    Name = GetDisplayName(data.TicketPrice_TicketType)
                };
                data.TicketPrice = ticketInfo;
                data.TicketPrice_Note = _iTransactionRepository.GetTicketPriceString(ticketInfo);
                var getDock = await _iDockRepository.SearchOneAsync(x => x.Id == data.DockId);
                if(getDock!= null)
                {
                    //data.GPS =  _iGPSDataRepository.GetByIMEI(_logSettings.Value, getDock?.IMEI, data.StartTime, DateTime.Now.AddMinutes(1)).Where(x => x.Lat > 0 && x.Long > 0).ToList();
                }
                return new ApiResponseData<Booking>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BookingSevice.Get", ex.ToString());
                return new ApiResponseData<Booking>();
            }
        }

        private Func<IQueryable<Booking>, IOrderedQueryable<Booking>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "stationIN" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.StationIn)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.StationIn)),
                "stationOut" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.StationOut)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.StationOut)),
                "bikeId" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.BikeId)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.BikeId)),
                "dockId" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.DockId)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.DockId)),
                "accountId" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.AccountId)),
                "customerGroupId" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.CustomerGroupId)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.CustomerGroupId)),
                "transactionCode" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TransactionCode)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TransactionCode)),
                "endDate" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.StartTime)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.StartTime)),
                "startDate" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.EndTime)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.EndTime)),
                "vote" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.Vote)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.Vote)),
                "feedback" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.Feedback)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.Feedback)),
                "ticketType" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TicketPrice_TicketType)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_TicketType)),
                "ticketValue" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TicketPrice_TicketValue)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_TicketValue)),
                "status" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.Status)),
                "chargeInAccount" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.ChargeInAccount)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.ChargeInAccount)),
                "allowChargeInSubAccount" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TicketPrice_AllowChargeInSubAccount)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_AllowChargeInSubAccount)),
                "chargeInSubAccount" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.ChargeInSubAccount)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.ChargeInSubAccount)),
                "ticketPriceId" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TicketPriceId)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TicketPriceId)),
                "blockPerMinute" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TicketPrice_BlockPerMinute)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_BlockPerMinute)),
                "blockPerViolation" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TicketPrice_BlockPerViolation)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_BlockPerViolation)),
                "blockViolationValue" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TicketPrice_BlockViolationValue)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_BlockViolationValue)),
                "limitMinutes" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TicketPrice_LimitMinutes)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_LimitMinutes)),
                "dateOfPayment" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.DateOfPayment)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.DateOfPayment)),
                "createdDate" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "totalPrice" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TotalPrice)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TotalPrice)),
                "totalMinutes" => sorttype == "asc" ? EntityExtention<Booking>.OrderBy(m => m.OrderBy(x => x.TotalMinutes)) : EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.TotalMinutes)),
                _ => EntityExtention<Booking>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        public static string GetDisplayName(Enum enumValue)
        {
            return enumValue.GetType().GetMember(enumValue.ToString())
                .First()
                .GetCustomAttribute<DisplayAttribute>()
                .Name;
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}
