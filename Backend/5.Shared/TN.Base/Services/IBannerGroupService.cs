using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IBannerGroupService : IService<BannerGroup>
    {
        Task<ApiResponseData<List<BannerGroup>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<BannerGroup>> GetById(int id);
        Task<List<BannerGroup>> GetBannerGroups();

        Task<ApiResponseData<List<Banner>>> BannerSearchPageAsync(int pageIndex, int pageSize, int bannerGroupId);
        Task<ApiResponseData<Banner>> BannerGetById(int id);
        Task<ApiResponseData<Banner>> BannerCreate(BannerCommand model);
        Task<ApiResponseData<Banner>> BannerEdit(BannerCommand model);
        Task<ApiResponseData<Banner>> BannerDelete(int id);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);
    }
    public class BannerGroupService : IBannerGroupService
    {
        private readonly ILogger _logger;
        private readonly BaseSettings _baseSettings;
        private readonly IUserRepository _iUserRepository;
        private readonly IBannerRepository _iBannerRepository;
        private readonly ILanguageRepository _iLanguageRepository;
        private readonly IBannerGroupRepository _iBannerGroupRepository;

        public BannerGroupService
        (
            ILogger<BannerGroupService> logger,
            IOptions<BaseSettings> baseSettings,
            IUserRepository iUserRepository,
            IBannerRepository iBannerRepository,
            ILanguageRepository iLanguageRepository,
            IBannerGroupRepository iBannerGroupRepository
        )
        {
            _logger = logger;
            _baseSettings = baseSettings.Value;
            _iUserRepository = iUserRepository;
            _iBannerRepository = iBannerRepository;
            _iLanguageRepository = iLanguageRepository;
            _iBannerGroupRepository = iBannerGroupRepository;
        }

        public async Task<ApiResponseData<List<BannerGroup>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                return await _iBannerGroupRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x =>
                    (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new BannerGroup
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Type = x.Type,
                        Status = x.Status,
                        Description = x.Description,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        LanguageId = x.LanguageId,
                        Language = x.Language,
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerGroupSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<BannerGroup>>();
            }
        }

        public async Task<ApiResponseData<BannerGroup>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<BannerGroup>() { Data = await _iBannerGroupRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerGroupSevice.Get", ex.ToString());
                return new ApiResponseData<BannerGroup>();
            }
        }

        public async Task<List<BannerGroup>> GetBannerGroups()
        {
            try
            {
                return await _iBannerGroupRepository.Search(
                    null,
                    null,
                    x => new BannerGroup
                    {
                        Id = x.Id,
                        Name = x.Name
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerGroupSevice.SearchPageAsync", ex.ToString());
                return new List<BannerGroup>();
            }
        }

        #region [Banner]
        public async Task<ApiResponseData<List<Banner>>> BannerSearchPageAsync(int pageIndex, int pageSize, int bannerGroupId)
        {
            try
            {
                var data = await _iBannerRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => x.BannerGroupId == bannerGroupId
                    ,
                    null,
                    x => new Banner
                    {
                        Id = x.Id,
                        BannerGroupId = x.BannerGroupId,
                        Name = x.Name,
                        Image = x.Image,
                        Data = x.Data,
                        Type = x.Type,
                        Status = x.Status,
                        StartTime = x.StartTime,
                        EndTime = x.EndTime,
                        Description = x.Description,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        LanguageId = x.LanguageId,
                        Language = x.Language
                    });
                //
                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                var langs = await _iLanguageRepository.Search(
                    x => data.Data.Select(y => y.LanguageId).Contains(x.Id),
                    null,
                    x => new Language
                    {
                        Id = x.Id,
                        Name = x.Name
                    }
                );

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.Language = langs.FirstOrDefault(x => x.Id == item.LanguageId)?.Name;
                }
                //
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerSevice.BannerSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Banner>>();
            }
        }

        public async Task<ApiResponseData<Banner>> BannerGetById(int id)
        {
            try
            {
                return new ApiResponseData<Banner>() { Data = await _iBannerRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerSevice.BannerGetById", ex.ToString());
                return new ApiResponseData<Banner>();
            }
        }

        public async Task<ApiResponseData<Banner>> BannerCreate(BannerCommand model)
        {
            try
            {
                var lan = await _iLanguageRepository.SearchOneAsync(x => x.Id == model.LanguageId);
                var dlAdd = new Banner
                {
                    BannerGroupId = model.BannerGroupId,
                    Name = model.Name,
                    Image = model.Image,
                    Data = model.Data,
                    Type = model.Type,
                    Status = model.Status,
                    StartTime = model.StartTime,
                    EndTime = model.EndTime,
                    Description = model.Description,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    LanguageId = model.LanguageId,
                    Language = lan?.Code1 ?? "vi"
                };
                await _iBannerRepository.AddAsync(dlAdd);
                await _iBannerRepository.Commit();

                return new ApiResponseData<Banner>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerSevice.BannerCreate", ex.ToString());
                return new ApiResponseData<Banner>();
            }
        }

        public async Task<ApiResponseData<Banner>> BannerEdit(BannerCommand model)
        {
            try
            {
                var dl = await _iBannerRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Banner>() { Data = null, Status = 0 };
                }

                var lan = await _iLanguageRepository.SearchOneAsync(x => x.Id == model.LanguageId);

                dl.BannerGroupId = model.BannerGroupId;
                dl.Name = model.Name;
                dl.Image = model.Image;
                dl.Data = model.Data;
                dl.Type = model.Type;
                dl.Status = model.Status;
                dl.StartTime = model.StartTime;
                dl.EndTime = model.EndTime;
                dl.Description = model.Description;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.LanguageId = model.LanguageId;
                dl.Language = lan?.Code1 ?? "vi";

                _iBannerRepository.Update(dl);
                await _iBannerRepository.Commit();
                return new ApiResponseData<Banner>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerSevice.BannerEdit", ex.ToString());
                return new ApiResponseData<Banner>();
            }
        }

        public async Task<ApiResponseData<Banner>> BannerDelete(int id)
        {
            try
            {
                var dl = await _iBannerRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Banner>() { Data = null, Status = 0 };
                }
                _iBannerRepository.Delete(dl);
                await _iBannerRepository.Commit();
                return new ApiResponseData<Banner>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerSevice.Delete", ex.ToString());
                return new ApiResponseData<Banner>();
            }
        }

        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/Banner/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfffffff}_{Path.GetFileName(file.FileName)}";
                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);
                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                    System.Threading.Thread.Sleep(100);
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BannerGroupsSevice.UploadFile", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }
        #endregion

        private static Func<IQueryable<BannerGroup>, IOrderedQueryable<BannerGroup>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "name" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "type" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.Type)),
                "status" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.Status)),
                "description" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.Description)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.Description)),
                "createdDate" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                "updatedUser" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
                "languageId" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.LanguageId)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.LanguageId)),
                "language" => sorttype == "asc" ? EntityExtention<BannerGroup>.OrderBy(m => m.OrderBy(x => x.Language)) : EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.Language)),
                _ => EntityExtention<BannerGroup>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
    }
}
