﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Base;

namespace TN.Base.Services
{
    public interface INotificationJobService : IService<NotificationJob>
    {
        Task<ApiResponseData<List<NotificationJob>>> SearchPageAsync(int pageIndex, int pageSize, string key, int? accountId, bool? isSend, DateTime? start, DateTime? end, string sortby, string sorttype);
        Task<ApiResponseData<NotificationJob>> GetById(int id);
        Task<ApiResponseData<NotificationJob>> Create(NotificationJobCommand model);
        Task<ApiResponseData<NotificationJob>> Edit(NotificationJobCommand model);
        Task<ApiResponseData<NotificationJob>> Delete(int id);
        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);
    }
    public class NotificationJobService : INotificationJobService
    {
        private readonly ILogger _logger;
        private readonly INotificationJobRepository _iNotificationJobRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly ILogRepository _iLogRepository;
        public NotificationJobService
        (
            ILogger<NotificationJobService> logger,
            INotificationJobRepository iNotificationJobRepository,
            IAccountRepository iAccountRepository,
            IUserRepository iUserRepository,
            ILogRepository iLogRepository
        )
        {
            _logger = logger;
            _iNotificationJobRepository = iNotificationJobRepository;
            _iAccountRepository = iAccountRepository;
            _iUserRepository = iUserRepository;
            _iLogRepository = iLogRepository;
        }

        public async Task<ApiResponseData<List<NotificationJob>>> SearchPageAsync(int pageIndex, int pageSize, string key, int? accountId, bool? isSend, DateTime? start, DateTime? end, string sortby, string sorttype)
        {
            try
            {
                if(end!= null)
                {
                    end = end.Value.AddDays(1);
                }    

                var data = await _iNotificationJobRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x =>
                     (key == null || x.Tile.Contains(key))
                     && (accountId == null || x.AccountId == accountId)
                     && (isSend == null || x.IsSend == isSend)
                     && (start == null || x.PublicDate >= start.Value.Date)
                     && (end == null || x.PublicDate <= end.Value.Date)
                     && x.IsSystem != true
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new NotificationJob
                    {
                        Id = x.Id,
                        AccountId = x.AccountId,
                        DeviceId = x.DeviceId,
                        Icon = x.Icon,
                        Tile = x.Tile,
                        Message = x.Message,
                        PublicDate = x.PublicDate,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        IsSend = x.IsSend,
                        SendDate = x.SendDate,
                        LanguageId = x.LanguageId,
                        IsSystem = x.IsSystem
                    });

                var idUsers = data.Data.Select(x => x.CreatedUser).ToList();
                idUsers.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(idUsers);
                var accountIds = data.Data.Where(x => x.AccountId > 0).Select(x => x.AccountId).ToList();
                var accounts = await _iAccountRepository.Search(x => accountIds.Contains(x.Id));

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.Account = new Account(accounts.FirstOrDefault(x => x.Id == item.AccountId));
                    if (item.Account.Id == 0) item.Account = null;
                }

                if (accountId > 0)
                {
                    data.OutData = new Account(await _iAccountRepository.SearchOneAsync(x => x.Id == accountId));
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("NotificationJobSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<NotificationJob>>();
            }
        }

        public async Task<ApiResponseData<NotificationJob>> GetById(int id)
        {
            try
            {
                var data =  await _iNotificationJobRepository.SearchOneAsync(x => x.Id == id);
                if(data == null)
                {
                    return new ApiResponseData<NotificationJob>() { Status = 0 };
                }
                data.Account = new Account(await _iAccountRepository.SearchOneAsync(x => x.Id == data.AccountId));
                if (data.Account.Id == 0) data.Account = null;
                return new ApiResponseData<NotificationJob>() { Status = 1, Data = data};
            }
            catch (Exception ex)
            {
                _logger.LogError("NotificationJobSevice.Get", ex.ToString());
                return new ApiResponseData<NotificationJob>();
            }
        }

        public async Task<ApiResponseData<NotificationJob>> Create(NotificationJobCommand model)
        {
            try
            {
                var pubTime = Convert.ToDateTime($"{model.PublicDate:yyyy-MM-dd} {model.PublicTime}");
                if (pubTime < DateTime.Now)
                {
                    return new ApiResponseData<NotificationJob>() { Data = null, Status = 3 };
                }

                var dlAdd = new NotificationJob
                {
                    AccountId = model.AccountId,
                    DeviceId = model.DeviceId,
                    Icon = model.Icon,
                    Tile = model.Tile,
                    Message = model.Message,
                    PublicDate =pubTime,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    IsSend = false,
                    LanguageId = model.LanguageId
                };

                await _iNotificationJobRepository.AddAsync(dlAdd);
                await _iNotificationJobRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới $'{dlAdd.Tile}'", LogType.Insert);

                return new ApiResponseData<NotificationJob>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("NotificationJobSevice.Create", ex.ToString());
                return new ApiResponseData<NotificationJob>();
            }
        }

        public async Task<ApiResponseData<NotificationJob>> Edit(NotificationJobCommand model)
        {
            try
            {
                var pubTime = Convert.ToDateTime($"{model.PublicDate:yyyy-MM-dd} {model.PublicTime}");

                var dl = await _iNotificationJobRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<NotificationJob>() { Data = null, Status = 0 };
                }

                if(dl.IsSend)
                {
                    return new ApiResponseData<NotificationJob>() { Data = null, Status = 2 };
                }

                if (pubTime < DateTime.Now)
                {
                    return new ApiResponseData<NotificationJob>() { Data = null, Status = 3 };
                }

                dl.AccountId = model.AccountId;
                dl.DeviceId = model.DeviceId;
                dl.Icon = model.Icon;
                dl.Tile = model.Tile;
                dl.Message = model.Message;
                dl.PublicDate = pubTime;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.LanguageId = model.LanguageId;

                _iNotificationJobRepository.Update(dl);
                await _iNotificationJobRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật $'{dl.Tile}'", LogType.Update);

                return new ApiResponseData<NotificationJob>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("NotificationJobSevice.Edit", ex.ToString());
                return new ApiResponseData<NotificationJob>();
            }
        }
        public async Task<ApiResponseData<NotificationJob>> Delete(int id)
        {
            try
            {
                var dl = await _iNotificationJobRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<NotificationJob>() { Data = null, Status = 0 };
                }

                _iNotificationJobRepository.Delete(dl);
                await _iNotificationJobRepository.Commit();

                await AddLog(dl.Id, $"Xóa $'{dl.Tile}'", LogType.Delete);

                return new ApiResponseData<NotificationJob>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("NotificationJobSevice.Delete", ex.ToString());
                return new ApiResponseData<NotificationJob>();
            }
        }

        private Func<IQueryable<NotificationJob>, IOrderedQueryable<NotificationJob>> OrderByExtention(string sortby, string sorttype) => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "accountId" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.AccountId)),
            "deviceId" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.DeviceId)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.DeviceId)),
            "icon" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.Icon)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.Icon)),
            "tile" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.Tile)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.Tile)),
            "message" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.Message)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.Message)),
            "publicDate" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.PublicDate)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.PublicDate)),
            "createdDate" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "createdUser" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
            "updatedDate" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            "updatedUser" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
            "isSend" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.IsSend)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.IsSend)),
            "sendDate" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.SendDate)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.SendDate)),
            "languageId" => sorttype == "asc" ? EntityExtention<NotificationJob>.OrderBy(m => m.OrderBy(x => x.LanguageId)) : EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.LanguageId)),
            _ => EntityExtention<NotificationJob>.OrderBy(m => m.OrderByDescending(x => x.Id)),
        };

        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}
