﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Models.NgxChart;
using TN.Base.Models.Response;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IChartReportService : IService<Station>
    {
        Task<string> GetInfoBoxData();
        Task<string> CommonInfo(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> GrowthChartData(int? projectId, string typeTimeView, DateTime from, DateTime to);

        #region [Customer Chart]
        Task<string> CustomerChartQuantity(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> CustomerChartQuantityAverage(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> CustomerChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> CustomerChartGenderTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> CustomerChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        #endregion

        #region [Finance Chart]
        Task<string> FinanceChartRechargeSum(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> FinanceChartRechargeSumAverage(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> FinanceChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> FinanceChartRangeHourTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> FinanceChartRechargeChanelTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> FinanceChartRechargeLevelTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> FinanceChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        #endregion

        #region [Transaction Chart]
        Task<string> TransactionChartQuantity(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> TransactionChartQuantityAverage(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> TransactionChartTypeTicketTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> TransactionChartRangeHourTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> TransactionChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> TransactionChartTripTimeTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> TransactionChartTripDistanceTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        Task<string> TransactionChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime from, DateTime to);
        #endregion

        #region [Station Chart]
        Task<string> GetStations(int? projectId);
        Task<string> StationChartData(int? projectId, string typeTimeView, string typeView, int stationId, DateTime from, DateTime to);
        #endregion

        #region [Bike Chart]
        Task<string> BikeChartData(int? projectId, string typeTimeView, DateTime from, DateTime to);
        #endregion
    }

    public class ChartReportService : IChartReportService
    {
        private readonly ILogger _logger;
        private readonly LogSettings _logSettings;

        private readonly IBikeRepository _iBikeRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly ITicketPrepaidRepository _iTicketPrepaidRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly IEntityBaseRepository<Domain.Model.Log> _logAdminRepository;

        private readonly DateTime _startSystemDate = new DateTime(2021, 11, 1);

        public ChartReportService
        (
            ILogger<StationService> logger,
            IOptions<LogSettings> logSettings,
            IBikeRepository iBikeRepository,
            IWalletRepository iWalletRepository,
            IStationRepository iStationRepository,
            IAccountRepository iAccountRepository,
            ITransactionRepository iTransactionRepository,
            ITicketPrepaidRepository iTicketPrepaidRepository,
            IWalletTransactionRepository iWalletTransactionRepository,
            IEntityBaseRepository<Domain.Model.Log> logAdminRepository
        )
        {
            _logger = logger;
            _logSettings = logSettings.Value;
            _iBikeRepository = iBikeRepository;
            _iWalletRepository = iWalletRepository;
            _iAccountRepository = iAccountRepository;
            _iStationRepository = iStationRepository;
            _logAdminRepository = logAdminRepository;
            _iTransactionRepository = iTransactionRepository;
            _iTicketPrepaidRepository = iTicketPrepaidRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
        }

        #region [Info Box]
        public async Task<string> GetInfoBoxData()
        {
            try
            {
                // Tổng số khách hàng
                var total_customer = _iAccountRepository.Count();

                // Tổng số giờ đã đi
                var total_hour = (await _iTransactionRepository
                    .Sum(
                        x => (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                        ,
                        x => x.TotalMinutes
                    )
                ) / 60;

                // Tổng số Km đã đi
                var total_km = (await _iTransactionRepository
                    .Sum(
                        x => (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                        ,
                        x => (double)x.KM
                    )
                ) / 1000;

                // Quãng đường đi xa nhất trong 1 chuyến đi
                var max_km = await GetMaximumKilometerTransaction();

                // Thời gian đi lâu nhất trong 1 chuyến đi
                var max_minute = await GetMaximumMinuteTransaction();

                // Tổng tài khoản chính
                var wallet_balance_sum = await _iWalletRepository.Sum(x => x.Status == EWalletStatus.Active, x => (double)x.Balance);

                // Tổng tài khoản khuyến mại
                var wallet_subbalance_sum = await _iWalletRepository.Sum(x => x.Status == EWalletStatus.Active, x => (double)x.SubBalance);

                // Tổng tiền nạp
                var recharge_sum = await _iWalletTransactionRepository
                    .Sum(
                        x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                                && x.Status == EWalletTransactionStatus.Done
                        ,
                        x => (double)x.Price
                    );

                // Tổng tiền đã sử dụng
                var postpaid = await _iTransactionRepository
                    .Sum(
                        x => (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall),
                        x => (double)x.TotalPrice
                    );

                var prepaid = await _iTicketPrepaidRepository
                    .Sum(
                        x => x.Id > 0,
                        x => (double)x.TicketValue
                    );

                var rent_sum = postpaid + prepaid;

                // Tổng tiền nợ cước
                var debt_sum = await _iTransactionRepository
                    .Sum(
                        x => x.Status == EBookingStatus.Debt,
                        x => (double)x.ChargeDebt
                    );

                // Tổng tiền khuyến mại vào tài khoản chính
                var balance_gift_sum = await _iWalletTransactionRepository
                    .Sum(
                        x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && x.Status == EWalletTransactionStatus.Done,
                        x => (double)(x.Amount - x.Price)
                    );
                //
                return JsonConvert.SerializeObject(
                    new
                    {
                        total_customer,
                        total_hour,
                        total_km,
                        max_km,
                        max_minute,

                        wallet_balance_sum,
                        wallet_subbalance_sum,
                        recharge_sum,
                        rent_sum,
                        debt_sum,
                        balance_gift_sum
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("ChartService.GetInfoBoxData", ex.ToString());
                return null;
            }
        }

        private async Task<double> GetMaximumKilometerTransaction()
        {
            var result = (await _iTransactionRepository.SearchTop(1, null, EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.KM)))).FirstOrDefault();
            var max = result?.KM ?? 0;
            return max / 1000;
        }

        private async Task<int> GetMaximumMinuteTransaction()
        {
            var result = (await _iTransactionRepository.SearchTop(1, null, EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.TotalMinutes)))).FirstOrDefault();
            var max = result?.TotalMinutes ?? 0;
            return max;
        }
        #endregion

        #region [Common Info]
        public async Task<string> CommonInfo(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            try
            {
                if (from < _startSystemDate)
                {
                    from = _startSystemDate;
                }

                var total_day = (to - from).TotalDays < 1 ? 1 : (to - from).TotalDays + 1;

                //---------------------------------------------------------------------------
                // NHÓM THÔNG TIN KHÁCH HÀNG
                //---------------------------------------------------------------------------

                var new_customer_quantity = await _iAccountRepository
                    .CountWithProjectId(
                        projectId,
                        x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                    );

                var new_customer_quantity_avg = new_customer_quantity / total_day;

                //---------------------------------------------------------------------------
                // NHÓM THÔNG TIN TÀI CHÍNH
                //---------------------------------------------------------------------------

                // Tổng lượng tiền nạp
                var recharge_sum = await _iWalletTransactionRepository
                    .Sum(
                        x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Status == EWalletTransactionStatus.Done
                            && (projectId == null || x.LocationProjectId == projectId)
                        ,
                        x => (double)x.Price
                    );

                var recharge_avg = recharge_sum / total_day;

                // Số lượng giao dịch nạp tiền
                var recharge_transaction_quantity = await _iWalletTransactionRepository
                    .Count(
                        x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Status == EWalletTransactionStatus.Done
                            && (projectId == null || x.LocationProjectId == projectId)
                    );

                var recharge_transaction_quantity_avg = recharge_transaction_quantity / total_day;

                // Tiền thuê xe
                var postpaid = await _iTransactionRepository
                    .Sum(
                        x => (projectId == null || x.ProjectId == projectId)
                                && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall)
                                && x.CreatedDate >= from
                                && x.CreatedDate < to.AddDays(1)
                        ,
                        x => (double)x.TotalPrice
                    );

                var prepaid = await _iTicketPrepaidRepository
                    .Sum(
                        x => (projectId == null || x.ProjectId == projectId)
                                && x.CreatedDate >= from
                                && x.CreatedDate < to.AddDays(1)
                        ,
                        x => (double)x.TicketValue
                    );

                var rent_sum = postpaid + prepaid;

                var rent_sum_avg = rent_sum / total_day;

                // Tiền nợ cước
                var debt_sum = await _iTransactionRepository
                    .Sum(
                        x => (projectId == null || x.ProjectId == projectId)
                            && x.Status == EBookingStatus.Debt
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                        ,
                        x => (double)x.ChargeDebt
                    );

                //---------------------------------------------------------------------------
                // NHÓM THÔNG TIN CHUYẾN ĐI
                //---------------------------------------------------------------------------

                // Tổng số chuyến đi
                var transaction_quantity = await _iTransactionRepository
                    .Count(
                        x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                    );

                var transaction_quantity_avg = transaction_quantity / total_day;

                // Tổng thời gian đi
                var transaction_time_sum = (await _iTransactionRepository
                    .Sum(
                        x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                        ,
                        x => (double)x.TotalMinutes
                    )) / 60;    // minutes to hour

                var transaction_time_sum_avg = transaction_time_sum / total_day;

                // Tổng quãng đường đi (Km)
                var transaction_km_sum = (await _iTransactionRepository
                    .Sum(
                        x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                        ,
                        x => (double)x.KM
                    )) / 1000;

                var transaction_km_sum_avg = transaction_km_sum / total_day;

                // Số chuyến đi nợ cước
                var transaction_debt_quantity = await _iTransactionRepository
                    .Count(
                        x => (projectId == null || x.ProjectId == projectId)
                            && x.Status == EBookingStatus.Debt
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                    );

                // Số chuyến đi thu hồi xe
                var transaction_recall_quantity = await _iTransactionRepository
                    .Count(
                        x => (projectId == null || x.ProjectId == projectId)
                            && x.Status == EBookingStatus.ReCall
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                    );
                //
                return JsonConvert.SerializeObject(
                    new
                    {
                        new_customer_quantity,
                        new_customer_quantity_avg,
                        recharge_sum,
                        recharge_avg,
                        recharge_transaction_quantity,
                        recharge_transaction_quantity_avg,
                        rent_sum,
                        rent_sum_avg,
                        debt_sum,
                        transaction_quantity,
                        transaction_quantity_avg,
                        transaction_time_sum,
                        transaction_time_sum_avg,
                        transaction_km_sum,
                        transaction_km_sum_avg,
                        transaction_debt_quantity,
                        transaction_recall_quantity
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("ChartService.CommonInfo", ex.ToString());
                return null;
            }
        }
        #endregion

        #region [Growth Chart]
        public async Task<string> GrowthChartData(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            try
            {
                var customer = await _iAccountRepository
                    .CountAndGroupByTime(
                        projectId,
                        typeTimeView,
                        x => x.CreatedDate >= from
                                && x.CreatedDate < to.AddDays(1)
                    );

                var revenue = await _iWalletTransactionRepository
                    .CountAndGroupByTime(
                        typeTimeView,
                        x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                                && x.Status == EWalletTransactionStatus.Done
                                && x.CreatedDate >= from
                                && x.CreatedDate < to.AddDays(1)
                                && (projectId == null || x.LocationProjectId == projectId)
                    );

                var transaction = await _iTransactionRepository
                    .CountAndGroupByTime(
                        typeTimeView,
                        x => (projectId == null || x.ProjectId == projectId)
                                && x.CreatedDate >= from
                                && x.CreatedDate < to.AddDays(1)
                                && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.Debt || x.Status == EBookingStatus.ReCall)
                    );
                //
                var listLabel = _GetListLabel(typeTimeView, from, to);
                //
                return JsonConvert.SerializeObject(
                    new List<ChartMultiData>
                    {
                        new ChartMultiData{name = "Khách hàng", series = _CastToChartSingleData(listLabel,customer)},
                        new ChartMultiData{name = "Lượt nạp", series = _CastToChartSingleData(listLabel,revenue)},
                        new ChartMultiData{name = "Chuyến đi", series = _CastToChartSingleData(listLabel,transaction)}
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("ChartService.GrowthChartData", ex.ToString());
                return null;
            }
        }
        #endregion

        #region [Customer Chart]
        public async Task<string> CustomerChartQuantity(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var total_customer = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(new { total_customer = _CastToChartSingleData(listLabel, total_customer) });
        }

        public async Task<string> CustomerChartQuantityAverage(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var customer = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    "month",
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );
            customer = customer.OrderBy(x => x.Item1).ToList();
            var customer_quantity_avg = new List<ChartSingleData>();

            foreach (var item in customer)
            {
                var time = item.Item1.Split("/");

                int days = DateTime.DaysInMonth(int.Parse(time[0]), int.Parse(time[1]));
                customer_quantity_avg.Add(new ChartSingleData { name = item.Item1, value = item.Item2 / days });
            }
            //
            return JsonConvert.SerializeObject(new { customer_quantity_avg });
        }

        public async Task<string> CustomerChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var block_14_18 = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Birthday.HasValue
                            && x.Birthday.Value.AddYears(14) <= DateTime.Now.Date
                            && x.Birthday.Value.AddYears(18) > DateTime.Now.Date
                );

            var block_18_22 = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Birthday.HasValue
                            && x.Birthday.Value.AddYears(18) <= DateTime.Now.Date
                            && x.Birthday.Value.AddYears(22) > DateTime.Now.Date
                );

            var block_22_40 = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Birthday.HasValue
                            && x.Birthday.Value.AddYears(22) <= DateTime.Now.Date
                            && x.Birthday.Value.AddYears(40) > DateTime.Now.Date
                );

            var block_40_60 = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Birthday.HasValue
                            && x.Birthday.Value.AddYears(40) <= DateTime.Now.Date
                            && x.Birthday.Value.AddYears(60) > DateTime.Now.Date
                );

            var block_60_inf = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Birthday.HasValue
                            && x.Birthday.Value.AddYears(60) <= DateTime.Now.Date
                            && x.Birthday.Value.AddYears(200) > DateTime.Now.Date
                );

            var block_other = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && !x.Birthday.HasValue
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "14 - 18", series = _CastToChartSingleData(listLabel,block_14_18)},
                    new ChartMultiData{name = "18 - 22", series = _CastToChartSingleData(listLabel,block_18_22)},
                    new ChartMultiData{name = "22 - 40", series = _CastToChartSingleData(listLabel,block_22_40)},
                    new ChartMultiData{name = "40 - 60", series = _CastToChartSingleData(listLabel,block_40_60)},
                    new ChartMultiData{name = "Trên 60", series = _CastToChartSingleData(listLabel,block_60_inf)},
                    new ChartMultiData{name = "Chưa rõ", series = _CastToChartSingleData(listLabel,block_other)}
                });
        }

        public async Task<string> CustomerChartGenderTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var male = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Sex == ESex.Nam
                );

            var female = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Sex == ESex.Nu
                );

            var other = await _iAccountRepository
                .CountAndGroupByTime(
                    projectId,
                    typeTimeView,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Sex == ESex.Khac
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "Nam", series = _CastToChartSingleData(listLabel,male)},
                    new ChartMultiData{name = "Nữ", series = _CastToChartSingleData(listLabel,female)},
                    new ChartMultiData{name = "Khác", series = _CastToChartSingleData(listLabel,other)}
                });
        }

        public async Task<string> CustomerChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var data = await _iAccountRepository
                .CountAndGroupByDay(
                    projectId,
                    x => x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );

            var mon = data.Where(x => x.Item1.DayOfWeek == DayOfWeek.Monday).Sum(x => x.Item2);
            var tue = data.Where(x => x.Item1.DayOfWeek == DayOfWeek.Tuesday).Sum(x => x.Item2);
            var wed = data.Where(x => x.Item1.DayOfWeek == DayOfWeek.Wednesday).Sum(x => x.Item2);
            var thu = data.Where(x => x.Item1.DayOfWeek == DayOfWeek.Thursday).Sum(x => x.Item2);
            var fri = data.Where(x => x.Item1.DayOfWeek == DayOfWeek.Friday).Sum(x => x.Item2);
            var sat = data.Where(x => x.Item1.DayOfWeek == DayOfWeek.Saturday).Sum(x => x.Item2);
            var sun = data.Where(x => x.Item1.DayOfWeek == DayOfWeek.Sunday).Sum(x => x.Item2);

            //
            return JsonConvert.SerializeObject(
                new List<ChartSingleData>{
                    new ChartSingleData { name="Thứ 2", value=mon},
                    new ChartSingleData { name="Thứ 3", value=tue},
                    new ChartSingleData { name="Thứ 4", value=wed},
                    new ChartSingleData { name="Thứ 5", value=thu},
                    new ChartSingleData { name="Thứ 6", value=fri},
                    new ChartSingleData { name="Thứ 7", value=sat},
                    new ChartSingleData { name="Chủ nhật", value=sun},
                }
            );
        }
        #endregion

        #region [Finance Chart]
        public async Task<string> FinanceChartRechargeSum(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var recharge_sum = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (projectId == null || x.LocationProjectId == projectId)
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(new { recharge_sum = _CastToChartSingleData(listLabel, recharge_sum) });
        }

        public async Task<string> FinanceChartRechargeSumAverage(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var recharge_sum = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    "month",
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (projectId == null || x.LocationProjectId == projectId)
                );
            recharge_sum = recharge_sum.OrderBy(x => x.Item1).ToList();
            var recharge_sum_avg = new List<ChartSingleData>();

            foreach (var item in recharge_sum)
            {
                var time = item.Item1.Split("/");
                var days = DateTime.DaysInMonth(int.Parse(time[0]), int.Parse(time[1]));
                recharge_sum_avg.Add(new ChartSingleData { name = item.Item1, value = item.Item2 / days });
            }
            //
            return JsonConvert.SerializeObject(new { recharge_sum_avg });
        }

        public async Task<string> FinanceChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var block_14_18 = await _iWalletTransactionRepository.SumRechargeWithAgeAndGroupByTime(projectId, typeTimeView, 14, 18, from, to);
            var block_18_22 = await _iWalletTransactionRepository.SumRechargeWithAgeAndGroupByTime(projectId, typeTimeView, 18, 22, from, to);
            var block_22_40 = await _iWalletTransactionRepository.SumRechargeWithAgeAndGroupByTime(projectId, typeTimeView, 22, 40, from, to);
            var block_40_60 = await _iWalletTransactionRepository.SumRechargeWithAgeAndGroupByTime(projectId, typeTimeView, 40, 60, from, to);
            var block_60_inf = await _iWalletTransactionRepository.SumRechargeWithAgeAndGroupByTime(projectId, typeTimeView, 60, 200, from, to);
            var block_other = await _iWalletTransactionRepository.SumRechargeWithAgeAndGroupByTime(projectId, typeTimeView, 0, 0, from, to);
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "14 - 18", series = _CastToChartSingleData(listLabel,block_14_18)},
                    new ChartMultiData{name = "18 - 22", series = _CastToChartSingleData(listLabel,block_18_22)},
                    new ChartMultiData{name = "22 - 40", series = _CastToChartSingleData(listLabel,block_22_40)},
                    new ChartMultiData{name = "40 - 60", series = _CastToChartSingleData(listLabel,block_40_60)},
                    new ChartMultiData{name = "Trên 60", series = _CastToChartSingleData(listLabel,block_60_inf)},
                    new ChartMultiData{name = "Chưa rõ", series = _CastToChartSingleData(listLabel,block_other)}
                });
        }

        public async Task<string> FinanceChartRangeHourTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var block_0_3 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > -1 && x.CreatedDate.Hour < 3
                );

            var block_3_6 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 2 && x.CreatedDate.Hour < 6

                );

            var block_6_9 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 5 && x.CreatedDate.Hour < 9
                );

            var block_9_12 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 8 && x.CreatedDate.Hour < 12
                );

            var block_12_15 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 11 && x.CreatedDate.Hour < 15
                );

            var block_15_18 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 14 && x.CreatedDate.Hour < 18
                );

            var block_18_21 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 17 && x.CreatedDate.Hour < 21
                );

            var block_21_24 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 20 && x.CreatedDate.Hour < 24
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "0h - 3h", series = _CastToChartSingleData(listLabel,block_0_3)},
                    new ChartMultiData{name = "3h - 6h", series = _CastToChartSingleData(listLabel,block_3_6)},
                    new ChartMultiData{name = "6h - 9h", series = _CastToChartSingleData(listLabel,block_6_9)},
                    new ChartMultiData{name = "9h - 12h", series = _CastToChartSingleData(listLabel,block_9_12)},
                    new ChartMultiData{name = "12h - 15h", series = _CastToChartSingleData(listLabel,block_12_15)},
                    new ChartMultiData{name = "15h - 18h", series = _CastToChartSingleData(listLabel,block_15_18)},
                    new ChartMultiData{name = "18h - 21h", series = _CastToChartSingleData(listLabel,block_18_21)},
                    new ChartMultiData{name = "21h - 24h", series = _CastToChartSingleData(listLabel,block_21_24)}
                });
        }

        public async Task<string> FinanceChartRechargeChanelTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var momo = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => x.Type == EWalletTransactionType.Deposit
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.PaymentGroup == EPaymentGroup.MomoPay
                );

            var zalopay = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => x.Type == EWalletTransactionType.Deposit
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.PaymentGroup == EPaymentGroup.ZaloPay
                );

            var vtcpay = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => x.Type == EWalletTransactionType.Deposit
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.PaymentGroup == EPaymentGroup.VTCPay
                );

            var direct = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "MoMo", series = _CastToChartSingleData(listLabel,momo)},
                    new ChartMultiData{name = "ZaloPay", series = _CastToChartSingleData(listLabel,zalopay)},
                    new ChartMultiData{name = "VTCPay", series = _CastToChartSingleData(listLabel,vtcpay)},
                    new ChartMultiData{name = "Trực tiếp", series = _CastToChartSingleData(listLabel,direct)}
                });
        }

        public async Task<string> FinanceChartRechargeLevelTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var block_0_10 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Price < 10000
                );

            var block_10_20 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Price >= 10000 && x.Price < 20000
                );

            var block_20_50 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Price >= 20000 && x.Price < 50000
                );

            var block_50_100 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Price >= 50000 && x.Price < 100000
                );

            var block_100_200 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Price >= 100000 && x.Price < 200000
                );

            var block_200_500 = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Price >= 200000 && x.Price < 500000
                );

            var block_500_inf = await _iWalletTransactionRepository
                .SumAndGroupByTime(
                    typeTimeView,
                    x => (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && (projectId == null || x.LocationProjectId == projectId)
                            && x.Status == EWalletTransactionStatus.Done
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.Price >= 500000
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "< 10k", series = _CastToChartSingleData(listLabel,block_0_10)},
                    new ChartMultiData{name = "10k - 20k", series = _CastToChartSingleData(listLabel,block_10_20)},
                    new ChartMultiData{name = "20k - 50k", series = _CastToChartSingleData(listLabel,block_20_50)},
                    new ChartMultiData{name = "50k - 100k", series = _CastToChartSingleData(listLabel,block_50_100)},
                    new ChartMultiData{name = "100k - 200k", series = _CastToChartSingleData(listLabel,block_100_200)},
                    new ChartMultiData{name = "200k - 500k", series = _CastToChartSingleData(listLabel,block_200_500)},
                    new ChartMultiData{name = ">= 500k", series = _CastToChartSingleData(listLabel,block_500_inf)}
                });
        }

        public async Task<string> FinanceChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var countData = await _iWalletTransactionRepository
                .CountAndGroupByDay(
                    x => (projectId == null || x.LocationProjectId == projectId)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && x.Status == EWalletTransactionStatus.Done
                );

            var mon = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Monday).Sum(x => x.Item2);
            var tue = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Tuesday).Sum(x => x.Item2);
            var wed = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Wednesday).Sum(x => x.Item2);
            var thu = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Thursday).Sum(x => x.Item2);
            var fri = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Friday).Sum(x => x.Item2);
            var sat = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Saturday).Sum(x => x.Item2);
            var sun = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Sunday).Sum(x => x.Item2);
            //
            var sumData = await _iWalletTransactionRepository
                .SumAndGroupByDay(
                    x => (projectId == null || x.LocationProjectId == projectId)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                            && x.Status == EWalletTransactionStatus.Done
                );

            var monSum = (sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Monday).Sum(x => x.Item2) / 1000) / CountDayOfWeekInRangeDay(DayOfWeek.Monday, from, to);
            var tueSum = (sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Tuesday).Sum(x => x.Item2) / 1000) / CountDayOfWeekInRangeDay(DayOfWeek.Tuesday, from, to);
            var wedSum = (sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Wednesday).Sum(x => x.Item2) / 1000) / CountDayOfWeekInRangeDay(DayOfWeek.Wednesday, from, to);
            var thuSum = (sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Thursday).Sum(x => x.Item2) / 1000) / CountDayOfWeekInRangeDay(DayOfWeek.Thursday, from, to);
            var friSum = (sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Friday).Sum(x => x.Item2) / 1000) / CountDayOfWeekInRangeDay(DayOfWeek.Friday, from, to);
            var satSum = (sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Saturday).Sum(x => x.Item2) / 1000) / CountDayOfWeekInRangeDay(DayOfWeek.Saturday, from, to);
            var sunSum = (sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Sunday).Sum(x => x.Item2) / 1000) / CountDayOfWeekInRangeDay(DayOfWeek.Sunday, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{
                        name = "Thứ 2",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name = "Số lượng nạp", value = mon},
                            new ChartSingleData{ name = "Trung bình nạp (nghìn đồng)", value = monSum}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 3",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số lượng nạp", value = tue},
                            new ChartSingleData{ name="Trung bình nạp (nghìn đồng)", value = tueSum}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 4",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số lượng nạp", value = wed},
                            new ChartSingleData{ name="Trung bình nạp (nghìn đồng)", value = wedSum}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 5",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số lượng nạp", value = thu},
                            new ChartSingleData{ name="Trung bình nạp (nghìn đồng)", value = thuSum}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 6",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số lượng nạp", value = fri},
                            new ChartSingleData{ name="Trung bình nạp (nghìn đồng)", value = friSum}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 7",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số lượng nạp", value = sat},
                            new ChartSingleData{ name="Trung bình nạp (nghìn đồng)", value = satSum}
                        }
                    },
                    new ChartMultiData{
                        name = "Chủ nhật",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số lượng nạp", value = sun},
                            new ChartSingleData{ name="Trung bình nạp (nghìn đồng)", value = sunSum}
                        }
                    }
                });
        }
        #endregion

        #region [Transaction Chart]
        public async Task<string> TransactionChartQuantity(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var transaction_quantity = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(new { transaction_quantity = _CastToChartSingleData(listLabel, transaction_quantity) });
        }

        public async Task<string> TransactionChartQuantityAverage(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var transaction_quantity = await _iTransactionRepository
                .CountAndGroupByTime(
                    "month",
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );
            transaction_quantity = transaction_quantity.OrderBy(x => x.Item1).ToList();
            var transaction_quantity_avg = new List<ChartSingleData>();

            foreach (var item in transaction_quantity)
            {
                var time = item.Item1.Split("/");

                int days = DateTime.DaysInMonth(int.Parse(time[0]), int.Parse(time[1]));
                transaction_quantity_avg.Add(new ChartSingleData { name = item.Item1, value = item.Item2 / days });
            }
            //
            return JsonConvert.SerializeObject(new { transaction_quantity_avg });
        }

        public async Task<string> TransactionChartTypeTicketTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var ticket_block = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.TicketPrice_TicketType == ETicketType.Block
                );

            var ticket_day = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.TicketPrice_TicketType == ETicketType.Day
                );

            var ticket_month = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.TicketPrice_TicketType == ETicketType.Month
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "Vé lượt", series = _CastToChartSingleData(listLabel,ticket_block)},
                    new ChartMultiData{name = "Vé ngày", series = _CastToChartSingleData(listLabel,ticket_day)},
                    new ChartMultiData{name = "Vé tháng", series = _CastToChartSingleData(listLabel,ticket_month)},
                });
        }

        public async Task<string> TransactionChartRangeHourTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var block_0_3 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > -1 && x.CreatedDate.Hour < 3
                );

            var block_3_6 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 2 && x.CreatedDate.Hour < 6
                );

            var block_6_9 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 5 && x.CreatedDate.Hour < 9
                );

            var block_9_12 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 8 && x.CreatedDate.Hour < 12
                );

            var block_12_15 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 11 && x.CreatedDate.Hour < 15
                );

            var block_15_18 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 14 && x.CreatedDate.Hour < 18
                );

            var block_18_21 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 17 && x.CreatedDate.Hour < 21
                );

            var block_21_24 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 20 && x.CreatedDate.Hour < 24
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "0h - 3h", series = _CastToChartSingleData(listLabel,block_0_3)},
                    new ChartMultiData{name = "3h - 6h", series = _CastToChartSingleData(listLabel,block_3_6)},
                    new ChartMultiData{name = "6h - 9h", series = _CastToChartSingleData(listLabel,block_6_9)},
                    new ChartMultiData{name = "9h - 12h", series = _CastToChartSingleData(listLabel,block_9_12)},
                    new ChartMultiData{name = "12h - 15h", series = _CastToChartSingleData(listLabel,block_12_15)},
                    new ChartMultiData{name = "15h - 18h", series = _CastToChartSingleData(listLabel,block_15_18)},
                    new ChartMultiData{name = "18h - 21h", series = _CastToChartSingleData(listLabel,block_18_21)},
                    new ChartMultiData{name = "21h - 24h", series = _CastToChartSingleData(listLabel,block_21_24)}
                });
        }

        public async Task<string> TransactionChartRangeAgeTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var block_14_18 = await _iTransactionRepository
                .CountByAgeAndGroupByTime(
                    typeTimeView,
                    14,
                    18,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );

            var block_18_22 = await _iTransactionRepository
                .CountByAgeAndGroupByTime(
                    typeTimeView,
                    18,
                    22,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );

            var block_22_40 = await _iTransactionRepository
                .CountByAgeAndGroupByTime(
                    typeTimeView,
                    22,
                    40,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );

            var block_40_60 = await _iTransactionRepository
                .CountByAgeAndGroupByTime(
                    typeTimeView,
                    40,
                    60,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );

            var block_60_inf = await _iTransactionRepository
                .CountByAgeAndGroupByTime(
                    typeTimeView,
                    60,
                    200,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );

            var block_other = await _iTransactionRepository
                .CountByAgeAndGroupByTime(
                    typeTimeView,
                    0,
                    0,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "14 - 18", series = _CastToChartSingleData(listLabel,block_14_18)},
                    new ChartMultiData{name = "18 - 22", series = _CastToChartSingleData(listLabel,block_18_22)},
                    new ChartMultiData{name = "22 - 40", series = _CastToChartSingleData(listLabel,block_22_40)},
                    new ChartMultiData{name = "40 - 60", series = _CastToChartSingleData(listLabel,block_40_60)},
                    new ChartMultiData{name = "Trên 60", series = _CastToChartSingleData(listLabel,block_60_inf)},
                    new ChartMultiData{name = "Chưa rõ", series = _CastToChartSingleData(listLabel,block_other)}
                });
        }

        public async Task<string> TransactionChartTripTimeTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var block_0_30 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.TotalMinutes < 30
                );

            var block_30_60 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.TotalMinutes > 29 && x.TotalMinutes < 60
                );

            var block_60_120 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.TotalMinutes > 59 && x.TotalMinutes < 120
                );

            var block_120_inf = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && x.CreatedDate.Hour > 119
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "< 30 phút", series = _CastToChartSingleData(listLabel,block_0_30)},
                    new ChartMultiData{name = "30 - 60 phút", series = _CastToChartSingleData(listLabel,block_30_60)},
                    new ChartMultiData{name = "60 - 120 phút", series = _CastToChartSingleData(listLabel,block_60_120)},
                    new ChartMultiData{name = "> 120 phút", series = _CastToChartSingleData(listLabel,block_120_inf)}
                });
        }

        public async Task<string> TransactionChartTripDistanceTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var block_0_1 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (double)x.KM <= 1000
                );

            var block_1_3 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (double)x.KM > 1000 && (double)x.KM <= 3000
                );

            var block_3_5 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (double)x.KM > 3000 && (double)x.KM <= 5000
                );

            var block_5_10 = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (double)x.KM > 5000 && (double)x.KM <= 10000
                );

            var block_10_inf = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                            && (double)x.KM > 10000
                );
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{name = "< 1 Km", series = _CastToChartSingleData(listLabel,block_0_1)},
                    new ChartMultiData{name = "1 - 3 Km", series = _CastToChartSingleData(listLabel,block_1_3)},
                    new ChartMultiData{name = "3 - 5 Km", series = _CastToChartSingleData(listLabel,block_3_5)},
                    new ChartMultiData{name = "5 - 10 Km", series = _CastToChartSingleData(listLabel,block_5_10)},
                    new ChartMultiData{name = "> 10 Km", series = _CastToChartSingleData(listLabel,block_10_inf)}
                });
        }

        public async Task<string> TransactionChartDayOfWeekTrend(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            var countData = await _iTransactionRepository
                .CountAndGroupByDay(
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );

            var mon = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Monday).Sum(x => x.Item2);
            var tue = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Tuesday).Sum(x => x.Item2);
            var wed = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Wednesday).Sum(x => x.Item2);
            var thu = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Thursday).Sum(x => x.Item2);
            var fri = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Friday).Sum(x => x.Item2);
            var sat = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Saturday).Sum(x => x.Item2);
            var sun = countData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Sunday).Sum(x => x.Item2);
            //
            var sumData = await _iTransactionRepository
                .SumTotalMinutesAndGroupByDay(
                    x => (projectId == null || x.ProjectId == projectId)
                            && (x.Status == EBookingStatus.End || x.Status == EBookingStatus.EndByAdmin || x.Status == EBookingStatus.ReCall || x.Status == EBookingStatus.Debt)
                            && x.CreatedDate >= from
                            && x.CreatedDate < to.AddDays(1)
                );

            var mons = sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Monday).Sum(x => x.Item2) / 60;
            var tues = sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Tuesday).Sum(x => x.Item2) / 60;
            var weds = sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Wednesday).Sum(x => x.Item2) / 60;
            var thus = sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Thursday).Sum(x => x.Item2) / 60;
            var fris = sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Friday).Sum(x => x.Item2) / 60;
            var sats = sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Saturday).Sum(x => x.Item2) / 60;
            var suns = sumData.Where(x => x.Item1.DayOfWeek == DayOfWeek.Sunday).Sum(x => x.Item2) / 60;
            //
            return JsonConvert.SerializeObject(
                new List<ChartMultiData>
                {
                    new ChartMultiData{
                        name = "Thứ 2",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name = "Số chuyến đi", value = mon},
                            new ChartSingleData{ name = "Tổng giờ thuê", value = mons}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 3",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số chuyến đi", value = tue},
                            new ChartSingleData{ name="Tổng giờ thuê", value = tues}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 4",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số chuyến đi", value = wed},
                            new ChartSingleData{ name="Tổng giờ thuê", value = weds}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 5",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số chuyến đi", value = thu},
                            new ChartSingleData{ name="Tổng giờ thuê", value = thus}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 6",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số chuyến đi", value = fri},
                            new ChartSingleData{ name="Tổng giờ thuê", value = fris}
                        }
                    },
                    new ChartMultiData{
                        name = "Thứ 7",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số chuyến đi", value = sat},
                            new ChartSingleData{ name="Tổng giờ thuê", value = sats}
                        }
                    },
                    new ChartMultiData{
                        name = "Chủ nhật",
                        series = new List<ChartSingleData>{
                            new ChartSingleData{ name="Số chuyến đi", value = sun},
                            new ChartSingleData{ name="Tổng giờ thuê", value = suns}
                        }
                    }
                });
        }
        #endregion

        #region [Station Chart]
        public async Task<string> GetStations(int? projectId)
        {
            var stations = await _iStationRepository
                .Search(
                    x => (projectId == null || x.ProjectId == projectId),
                    null,
                    x => new Station { Id = x.Id, Name = x.Name, Description = x.Description }
                );

            List<NgSelect> list = new List<NgSelect> { };

            foreach (var st in stations)
            {
                list.Add(new NgSelect { id = st.Id.ToString(), text = st.Name });
            }
            //
            return JsonConvert.SerializeObject(list);
        }

        public async Task<string> StationChartData(int? projectId, string typeTimeView, string typeView, int stationId, DateTime from, DateTime to)
        {
            var station = await _iStationRepository.SearchOneAsync(x => x.Id == stationId);

            if (station == null)
            {
                return null;
            }

            List<Tuple<string, double>> data = new List<Tuple<string, double>> { };

            if (typeView == "bikeOut")
            {
                data = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                        && x.StationIn == stationId
                        && x.CreatedDate >= from
                        && x.CreatedDate < to.AddDays(1)
                );
            }
            else if (typeView == "bikeIn")
            {
                data = await _iTransactionRepository
                .CountAndGroupByTime(
                    typeTimeView,
                    x => (projectId == null || x.ProjectId == projectId)
                        && x.StationOut == stationId
                        && x.CreatedDate >= from
                        && x.CreatedDate < to.AddDays(1)
                );
            }
            //
            var listLabel = _GetListLabel(typeTimeView, from, to);
            //
            return JsonConvert.SerializeObject(
                new ChartMultiData
                {
                    name = station.Name,
                    series = _CastToChartSingleData(listLabel, data)
                });
        }
        #endregion

        #region [Bike Chart]
        public async Task<string> BikeChartData(int? projectId, string typeTimeView, DateTime from, DateTime to)
        {
            // Top 20 xe thuê nhiều nhất và ít nhất
            var top_20_best = await _iTransactionRepository.Top20BikeMaxRentTime(projectId, from, to);
            var top_20_worst = await _iTransactionRepository.Top20BikeMinRentTime(projectId, from, to);

            var bikes = await _iBikeRepository
                .Search(
                    x => top_20_best.Select(y => y.BikeId).Contains(x.Id)
                        || top_20_worst.Select(y => y.BikeId).Contains(x.Id)
                    ,
                    null,
                    x => new Bike
                    {
                        Id = x.Id,
                        Plate = x.Plate
                    }
                );

            // Tìm xe không thuê
            //var transactions = await _iTransactionRepository
            //    .Search(
            //        x => x.CreatedDate >= from && x.CreatedDate < to.AddDays(1)
            //        ,
            //        null
            //        ,
            //        x => new Transaction
            //        {
            //            BikeId = x.BikeId
            //        }
            //    );

            //var rentBikeIds = transactions.Select(x => x.BikeId);
            //var nonRentBikes = _iBikeRepository.Search(x => !rentBikeIds.Contains(x.Id));
            //
            var chart_data_best_20 = new List<ChartMultiData>();
            var chart_data_worst_20 = new List<ChartMultiData>();

            var listLabel = _GetListLabel(typeTimeView, from, to);

            foreach (var best in top_20_best)
            {
                var data = await _iTransactionRepository
                    .SumTotalMinutesAndGroupByTime(
                        typeTimeView,
                        x => x.BikeId == best.BikeId
                            && (projectId == null || x.ProjectId == projectId)
                    );
                //
                chart_data_best_20.Add(new ChartMultiData { name = bikes.FirstOrDefault(y => y.Id == best.BikeId)?.Plate ?? "Không rõ", series = _CastToChartSingleData(listLabel, data) });
            }
            //
            foreach (var worst in top_20_worst)
            {
                var data = await _iTransactionRepository
                    .SumTotalMinutesAndGroupByTime(
                        typeTimeView,
                        x => x.BikeId == worst.BikeId
                            && (projectId == null || x.ProjectId == projectId)
                    );
                //
                chart_data_worst_20.Add(new ChartMultiData { name = bikes.FirstOrDefault(y => y.Id == worst.BikeId)?.Plate ?? "Không rõ", series = _CastToChartSingleData(listLabel, data) });
            }
            //
            return JsonConvert.SerializeObject(
                new
                {
                    chart_data_best_20,
                    chart_data_worst_20
                });
        }
        #endregion

        private static Tuple<DateTime, DateTime> _GetRangeDay(string typeOfTime)
        {
            var today = DateTime.Now.Date;

            switch (typeOfTime)
            {
                case "day":
                    {
                        return new Tuple<DateTime, DateTime>(today, today);
                    }
                case "week":
                    {
                        int delta = DayOfWeek.Monday - today.DayOfWeek;
                        var monday = today.AddDays(delta);

                        return new Tuple<DateTime, DateTime>(monday, today);
                    }
                case "month":
                    {
                        var firstDayOfMonth = new DateTime(today.Year, today.Month, 1);

                        return new Tuple<DateTime, DateTime>(firstDayOfMonth, today);
                    }
                case "quarter":
                    {
                        int quarterNumber = (today.Month - 1) / 3 + 1;
                        var firstDayOfQuarter = new DateTime(today.Year, (quarterNumber - 1) * 3 + 1, 1);

                        return new Tuple<DateTime, DateTime>(firstDayOfQuarter, today);
                    }
                case "year":
                    {
                        var firstDayOfYear = new DateTime(today.Year, 1, 1);

                        return new Tuple<DateTime, DateTime>(firstDayOfYear, today);
                    }
                default:
                    {
                        return new Tuple<DateTime, DateTime>(today, today);
                    }
            }
        }

        private static List<ChartSingleData> _CastToChartSingleData(List<string> listLabel, List<Tuple<string, double>> data)
        {
            var transaction_quantity = new List<ChartSingleData>();

            if (data != null)
            {
                foreach (var label in listLabel)
                {
                    var match = data.FirstOrDefault(x => x.Item1 == label);
                    if (match != null)
                    {
                        transaction_quantity.Add(new ChartSingleData { name = match.Item1, value = match.Item2 });
                    }
                    else
                    {
                        transaction_quantity.Add(new ChartSingleData { name = label, value = 0 });
                    }
                }
            }
            else
            {
                foreach (var label in listLabel)
                {
                    transaction_quantity.Add(new ChartSingleData { name = label, value = 0 });
                }
            }
            //
            return transaction_quantity.OrderBy(x => x.name).ToList();
        }

        private static List<string> _GetListLabel(string typeTimeView, DateTime from, DateTime to)
        {
            var listlabel = new List<string>();
            if (typeTimeView == "day")
            {
                for (var day = from.Date; day.Date <= to.Date; day = day.AddDays(1))
                {
                    listlabel.Add(day.ToString("yyyy/MM/dd"));
                }
            }
            else if (typeTimeView == "month")
            {
                for (int year = from.Year; year <= to.Year; year++)
                {
                    if (year == from.Year)
                    {
                        for (int month = from.Month; month < 13; month++)
                        {
                            listlabel.Add($"{year}/{month.ToString().PadLeft(2, '0')}");
                        }
                    }
                    //
                    if (year != from.Year && year != to.Year)
                    {
                        for (int month = 1; month < 13; month++)
                        {
                            listlabel.Add($"{year}/{month.ToString().PadLeft(2, '0')}");
                        }
                    }
                    //
                    if (year == to.Year)
                    {
                        for (int month = 1; month <= to.Month; month++)
                        {
                            listlabel.Add($"{year}/{month.ToString().PadLeft(2, '0')}");
                        }
                    }
                }
            }
            else if (typeTimeView == "dayOfWeek")
            {
                listlabel = new List<string> { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            }
            //
            return listlabel;
        }

        private static int _GetDayOfWeek(DateTime date)
        {
            var day = date.Day;
            var month = date.Month;
            var year = date.Year;

            var k = day;
            //
            int m = 1;
            switch (month)
            {
                case 1:
                    m = 11;
                    break;
                case 2:
                    m = 12;
                    break;
                case 3:
                    m = 1;
                    break;
                case 4:
                    m = 2;
                    break;
                case 5:
                    m = 3;
                    break;
                case 6:
                    m = 4;
                    break;
                case 7:
                    m = 5;
                    break;
                case 8:
                    m = 6;
                    break;
                case 9:
                    m = 7;
                    break;
                case 10:
                    m = 8;
                    break;
                case 11:
                    m = 9;
                    break;
                case 12:
                    m = 10;
                    break;
            }
            //
            var C = (int)(year / 100) + ((year % 100 == 0) ? 0 : 1);
            //
            var Y = year % 100;
            if (date.Month == 1 || date.Month == 2)
            {
                Y--;
            }
            //
            int W = (int)(k + Math.Floor(2.6 * m - 0.2) - 2 * C + Y + (int)Math.Floor((double)Y / 4) + (int)Math.Floor((double)C / 4)) % 7;
            //
            return W;
        }

        private static int CountDayOfWeekInRangeDay(DayOfWeek day, DateTime start, DateTime end)
        {
            TimeSpan ts = end - start;
            int count = (int)Math.Floor(ts.TotalDays / 7);
            int remainder = (int)(ts.TotalDays % 7);
            int sinceLastDay = (int)(end.DayOfWeek - day);
            if (sinceLastDay < 0) sinceLastDay += 7;

            if (remainder >= sinceLastDay) count++;

            return count;
        }
    }
}