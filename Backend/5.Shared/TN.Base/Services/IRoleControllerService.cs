﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Base;
using Microsoft.AspNetCore.SignalR;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace TN.Base.Services
{
    public interface IRoleControllerService : IService<RoleController>
    {
        Task<ApiResponseData<List<RoleController>>> SearchPageAsync(int pageIndex,
            int pageSize,
            string key,
            int? groupId,
            string areaId,
            string sortby,
            string sorttype);
        Task<ApiResponseData<RoleController>> GetById(string id);
        Task<ApiResponseData<RoleController>> Create(RoleControllerCommand model);
        Task<ApiResponseData<RoleController>> Edit(RoleControllerCommand model);
        Task<ApiResponseData<RoleController>> Delete(string id);

        #region [RoleAction]
        Task<ApiResponseData<List<RoleAction>>> RoleActionSearchPageAsync(int pageIndex, int pageSize, string key, string controllerId, string orderby, string ordertype);
        Task<ApiResponseData<RoleAction>> RoleActionGetById(int id);
        Task<ApiResponseData<RoleAction>> RoleActionCreate(RoleActionCommand model);
        Task<ApiResponseData<RoleAction>> RoleActionEdit(RoleActionCommand model);
        Task<ApiResponseData<RoleAction>> RoleActionDelete(int id);
        #endregion

    }
    public class RoleControllerService : IRoleControllerService
    {
        private readonly ILogger _logger;
        private readonly IRoleControllerRepository _iRoleControllerRepository;
        private readonly IRoleActionRepository _iRoleActionRepository;
        private readonly IRoleAreaRepository _iRoleAreaRepository;
        private readonly IRoleGroupRepository _iRoleGroupRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly IHubContext<CategoryHub> _hubContext;

        public RoleControllerService
        (
            ILogger<RoleControllerService> logger,
            IRoleControllerRepository iRoleControllerRepository,
            IRoleActionRepository iRoleActionRepository,
            IRoleAreaRepository iRoleAreaRepository,
            IRoleGroupRepository iRoleGroupRepository,
            IParameterRepository iParameterRepository,
            IHubContext<CategoryHub> hubContext
        )
        {
            _logger = logger;
            _iRoleControllerRepository = iRoleControllerRepository;
            _iRoleActionRepository = iRoleActionRepository;
            _iRoleAreaRepository = iRoleAreaRepository;
            _iRoleGroupRepository = iRoleGroupRepository;
            _iParameterRepository = iParameterRepository;
            _hubContext = hubContext;
        }

        public async Task<ApiResponseData<List<RoleController>>> SearchPageAsync(
            int pageIndex, 
            int pageSize, 
            string key, 
            int? groupId,
            string areaId,
            string sortby, 
            string sorttype
            )
        {
            try
            {
                var data = await _iRoleControllerRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x =>  
                        (key == null 
                        || x.Id.ToLower().Contains(key) 
                        || x.Name.ToLower().Contains(key)
                        || x.RouterLink.ToLower().Contains(key)
                        || x.Icon.ToLower().Contains(key)
                        ) 
                        && (groupId == null || x.GroupId == groupId) 
                        && (areaId == null || x.AreaId == areaId)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new RoleController
                    {
                        Id = x.Id,
                        AreaId = x.AreaId,
                        GroupId = x.GroupId,
                        Name = x.Name,
                        Description = x.Description,
                        Order = x.Order,
                        IsShow = x.IsShow,
                        Icon = x.Icon,
                        RouterLink = x.RouterLink,
                        Href = x.Href
                    });

                var areas = await _iRoleAreaRepository.Search(x => data.Data.Select(e => e.AreaId).Contains(x.Id));
                var groups = await _iRoleGroupRepository.Search(x => data.Data.Select(e => e.GroupId).Contains(x.Id));

                foreach (var item in data.Data)
                {
                    item.RoleArea = areas.FirstOrDefault(x => x.Id == item.AreaId);
                    item.RoleGroups = groups.FirstOrDefault(x => x.Id == item.GroupId);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<RoleController>>();
            }
        }


        public async Task<ApiResponseData<RoleController>> GetById(string id)
        {
            try
            {
                var data = await _iRoleControllerRepository.SearchOneAsync(x => x.Id == id);
                if(data == null)
                {
                    return new ApiResponseData<RoleController>() { Data = data, Status = 0 };
                }
                var listAcction = await _iRoleActionRepository.Search(x => x.ControllerId == id);
                if(!listAcction.Any())
                {
                    await _iRoleActionRepository.AddAsync(new RoleAction { Checked = false, ActionName = "Index", ControllerId = data.Id, IsShow = true, Name = "Danh sách", Order = 1  });
                    await _iRoleActionRepository.AddAsync(new RoleAction { Checked = false, ActionName = "Create", ControllerId = data.Id, IsShow = true, Name = "Thêm mới", Order = 2 });
                    await _iRoleActionRepository.AddAsync(new RoleAction { Checked = false, ActionName = "Edit", ControllerId = data.Id, IsShow = true, Name = "Cập nhật", Order = 3 });
                    await _iRoleActionRepository.AddAsync(new RoleAction { Checked = false, ActionName = "Delete", ControllerId = data.Id, IsShow = true, Name = "Xóa", Order = 4 });
                    await _iRoleActionRepository.Commit();
                    await InitCategoryAsync("RoleAction");
                }
                return new ApiResponseData<RoleController>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.Get", ex.ToString());
                return new ApiResponseData<RoleController>();
            }
        }

        public async Task<ApiResponseData<RoleController>> Create(RoleControllerCommand model)
        {
            try
            {
                var dl = await _iRoleControllerRepository.AnyAsync(x => x.Id == model.Id);
                if (dl)
                {
                    return new ApiResponseData<RoleController>() { Data = null, Status = 2 };
                }

                var dlAdd = new RoleController
                {
                    Id = model.Id,
                    AreaId = model.AreaId,
                    GroupId = model.GroupId,
                    Name = model.Name,
                    Description = model.Description,
                    Order = model.Order,
                    IsShow = model.IsShow,
                    Icon = model.Icon,
                    RouterLink = model.RouterLink,
                    Href = model.Href
                };
                await _iRoleControllerRepository.AddAsync(dlAdd);
                await _iRoleControllerRepository.Commit();

                await _iRoleActionRepository.AddAsync(new RoleAction { Checked = false, ActionName = "Index", ControllerId = dlAdd.Id, IsShow = true, Name = "Xem", Order = 1 });
                await _iRoleActionRepository.AddAsync(new RoleAction { Checked = false, ActionName = "Edit", ControllerId = dlAdd.Id, IsShow = true, Name = "Thêm mới, cập nhật", Order = 3 });
                await _iRoleActionRepository.AddAsync(new RoleAction { Checked = false, ActionName = "Delete", ControllerId = dlAdd.Id, IsShow = true, Name = "Xóa", Order = 4 });
                await _iRoleActionRepository.Commit();

                await InitCategoryAsync();

                return new ApiResponseData<RoleController>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.Create", ex.ToString());
                return new ApiResponseData<RoleController>();
            }
        }

        public async Task<ApiResponseData<RoleController>> Edit(RoleControllerCommand model)
        {
            try
            {
                var dl = await _iRoleControllerRepository.SearchOneAsync( x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<RoleController>() { Data = null, Status = 0 };
                }

                dl.AreaId = model.AreaId;
                dl.GroupId = model.GroupId;
                dl.Name = model.Name;
                dl.Description = model.Description;
                dl.Order = model.Order;
                dl.IsShow = model.IsShow;
                dl.Icon = model.Icon;
                dl.RouterLink = model.RouterLink;
                dl.Href = model.Href;
                _iRoleControllerRepository.Update(dl);
                await _iRoleControllerRepository.Commit();
                await InitCategoryAsync();

                return new ApiResponseData<RoleController>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.Edit", ex.ToString());
                return new ApiResponseData<RoleController>();
            }
        }
        public async Task<ApiResponseData<RoleController>> Delete(string id)
        {
            try
            {
                var dl = await _iRoleControllerRepository.SearchOneAsync( x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<RoleController>() { Data = null, Status = 0 };
                }
                else
                {
                    var checkList = await _iRoleActionRepository.AnyAsync(x => x.ControllerId == dl.Id);
                    if (checkList)
                    {
                        return new ApiResponseData<RoleController>() { Data = null, Status = 2 };
                    }
                    _iRoleControllerRepository.Delete(dl);
                    await _iRoleControllerRepository.Commit();
                    await InitCategoryAsync();
                    return new ApiResponseData<RoleController>() { Data = null, Status = 1 };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.Delete", ex.ToString());
                return new ApiResponseData<RoleController>();
            }
        }

        private Func<IQueryable<RoleController>, IOrderedQueryable<RoleController>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<RoleController>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<RoleController>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "areaId" => sorttype == "asc" ? EntityExtention<RoleController>.OrderBy(m => m.OrderBy(x => x.AreaId)) : EntityExtention<RoleController>.OrderBy(m => m.OrderByDescending(x => x.AreaId)),
                "groupId" => sorttype == "asc" ? EntityExtention<RoleController>.OrderBy(m => m.OrderBy(x => x.GroupId)) : EntityExtention<RoleController>.OrderBy(m => m.OrderByDescending(x => x.GroupId)),
                "name" => sorttype == "asc" ? EntityExtention<RoleController>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<RoleController>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "description" => sorttype == "asc" ? EntityExtention<RoleController>.OrderBy(m => m.OrderBy(x => x.Description)) : EntityExtention<RoleController>.OrderBy(m => m.OrderByDescending(x => x.Description)),
                "order" => sorttype == "asc" ? EntityExtention<RoleController>.OrderBy(m => m.OrderBy(x => x.Order)) : EntityExtention<RoleController>.OrderBy(m => m.OrderByDescending(x => x.Order)),
                "isShow" => sorttype == "asc" ? EntityExtention<RoleController>.OrderBy(m => m.OrderBy(x => x.IsShow)) : EntityExtention<RoleController>.OrderBy(m => m.OrderByDescending(x => x.IsShow)),
                _ => EntityExtention<RoleController>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        #region [RoleAction]
        public async Task<ApiResponseData<List<RoleAction>>> RoleActionSearchPageAsync(int pageIndex, int pageSize, string key, string controllerId, string sortby, string sorttype)
        {
            try
            {
                return await _iRoleActionRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => 
                    (key == null || x.Name.Contains(key))
                    && x.ControllerId == controllerId,
                    RoleActionOrderByExtention(sortby, sorttype),
                    x => new RoleAction
                    {
                        Id = x.Id,
                        ControllerId = x.ControllerId,
                        Name = x.Name,
                        ActionName = x.ActionName,
                        Description = x.Description,
                        Order = x.Order,
                        IsShow = x.IsShow
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.RoleActionSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<RoleAction>>();
            }
        }

        public async Task<ApiResponseData<RoleAction>> RoleActionGetById(int id)
        {
            try
            {
                return new ApiResponseData<RoleAction>() { Data = await _iRoleActionRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.RoleActionGetById", ex.ToString());
                return new ApiResponseData<RoleAction>();
            }
        }

        public async Task<ApiResponseData<RoleAction>> RoleActionCreate(RoleActionCommand model)
        {
            try
            {
                var dl = await _iRoleActionRepository.SearchOneAsync(x => x.ActionName == model.ActionName.Trim() && x.ControllerId == model.ControllerId);
                if (dl != null)
                {
                    return new ApiResponseData<RoleAction>() { Data = null, Status = 2 };
                }

                var dlAdd = new RoleAction
                {
                    ControllerId = model.ControllerId,
                    Name = model.Name,
                    ActionName = model.ActionName,
                    Description = model.Description,
                    Order = model.Order,
                    IsShow = model.IsShow,
                };
                await _iRoleActionRepository.AddAsync(dlAdd);
                await _iRoleActionRepository.Commit();

                return new ApiResponseData<RoleAction>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.RoleActionCreate", ex.ToString());
                return new ApiResponseData<RoleAction>();
            }
        }

        public async Task<ApiResponseData<RoleAction>> RoleActionEdit(RoleActionCommand model)
        {
            try
            {
                var dl = await _iRoleActionRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<RoleAction>() { Data = null, Status = 0 };
                }

                var kt = await _iRoleActionRepository.SearchOneAsync(x => x.ActionName == model.ActionName.Trim() && x.ControllerId == model.ControllerId && x.Id != model.Id);
                if (kt != null)
                {
                    return new ApiResponseData<RoleAction>() { Data = null, Status = 2 };
                }

                dl.ControllerId = model.ControllerId;
                dl.Name = model.Name;
                dl.ActionName = model.ActionName;
                dl.Description = model.Description;
                dl.Order = model.Order;
                dl.IsShow = model.IsShow;

                _iRoleActionRepository.Update(dl);
                await _iRoleActionRepository.Commit();
                return new ApiResponseData<RoleAction>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.RoleActionEdit", ex.ToString());
                return new ApiResponseData<RoleAction>();
            }
        }
        public async Task<ApiResponseData<RoleAction>> RoleActionDelete(int id)
        {
            try
            {
                var dl = await _iRoleActionRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<RoleAction>() { Data = null, Status = 0 };
                }

                _iRoleActionRepository.Delete(dl);
                await _iRoleActionRepository.Commit();
                return new ApiResponseData<RoleAction>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleControllerSevice.RoleActionDelete", ex.ToString());
                return new ApiResponseData<RoleAction>();
            }
        }

        private Func<IQueryable<RoleAction>, IOrderedQueryable<RoleAction>> RoleActionOrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<RoleAction>, IOrderedQueryable<RoleAction>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleAction>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<RoleAction>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "controllerId":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleAction>.OrderBy(m => m.OrderBy(x => x.ControllerId)) : EntityExtention<RoleAction>.OrderBy(m => m.OrderByDescending(x => x.ControllerId));
                    break;
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleAction>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<RoleAction>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                case "actionName":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleAction>.OrderBy(m => m.OrderBy(x => x.ActionName)) : EntityExtention<RoleAction>.OrderBy(m => m.OrderByDescending(x => x.ActionName));
                    break;
                case "description":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleAction>.OrderBy(m => m.OrderBy(x => x.Description)) : EntityExtention<RoleAction>.OrderBy(m => m.OrderByDescending(x => x.Description));
                    break;
                case "order":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleAction>.OrderBy(m => m.OrderBy(x => x.Order)) : EntityExtention<RoleAction>.OrderBy(m => m.OrderByDescending(x => x.Order));
                    break;
                case "isShow":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleAction>.OrderBy(m => m.OrderBy(x => x.IsShow)) : EntityExtention<RoleAction>.OrderBy(m => m.OrderByDescending(x => x.IsShow));
                    break;

                default:
                    functionOrder = EntityExtention<RoleAction>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }
        #endregion

        #region [Hub]
        private async Task InitCategoryAsync(string tableName = null)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName ??  "RoleController");
            }
            catch { }
        }
        #endregion
    }
}