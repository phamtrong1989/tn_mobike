﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.Domain.Model.Common;
using PT.Base;
using TN.Domain.ViewModels;

namespace TN.Base.Services
{
    public interface ICampaignService : IService<Campaign>
    {
        #region [Campaign]
        Task<ApiResponseData<List<Campaign>>> SearchPageAsync(int pageIndex, int pageSize, string key, int? projectId, ECampaignStatus? status, ECampaignType? type, string sortby, string sorttype);
        Task<ApiResponseData<Campaign>> GetById(int id);
        Task<ApiResponseData<Campaign>> Create(CampaignCommand model);
        Task<ApiResponseData<Campaign>> Edit(CampaignCommand model);
        Task<ApiResponseData<Campaign>> Delete(int id);
        #endregion

        #region [VoucherCode]
        Task<ApiResponseData<List<VoucherCode>>> VoucherCodeSearchPageAsync(int pageIndex, int pageSize, string key, int? campaignId, string sortby, string sorttype);
        Task<ApiResponseData<VoucherCode>> VoucherCodeGetById(int id);
        Task<ApiResponseData<VoucherCode>> VoucherCodeCreate(VoucherCodeCommand model);
        Task<ApiResponseData<VoucherCode>> VoucherCodeEdit(VoucherCodeEditCommand model);
        Task<ApiResponseData<VoucherCode>> VoucherCodeDelete(int id);
        #endregion

        Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? campaignId, string sortby, string sorttype);

        #region [DepositEvent]
        Task<ApiResponseData<List<DepositEvent>>> DepositEventSearchPageAsync(int pageIndex, int PageSize, int campaignId, string orderby, string ordertype);
        Task<ApiResponseData<DepositEvent>> DepositEventGetById(int id);
        Task<ApiResponseData<DepositEvent>> DepositEventCreate(DepositEventCommand model);
        Task<ApiResponseData<DepositEvent>> DepositEventEdit(DepositEventCommand model);
        Task<ApiResponseData<DepositEvent>> DepositEventDelete(int id);
        #endregion

        #region [CampaignResult]
        Task<ApiResponseData<List<CampaignResult>>> CampaignResultSearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            string sortby,
            string sorttype,

            DateTime? from,
            DateTime? to,
            int? projectId,
            int? campaignId,
            int? customerGroupId,
            ECampaignType? type);

        Task<ApiResponseData<List<Campaign>>> SearchByCampaign(string key);
        #endregion

        #region [DiscountCode]
        Task<ApiResponseData<List<DiscountCode>>> DiscountCodeSearchPageAsync(int pageIndex, int pageSize, int campaignId, string sortby, string sorttype);
        Task<ApiResponseData<DiscountCode>> DiscountCodeGetById(int id);
        Task<ApiResponseData<DiscountCode>> DiscountCodeCreate(DiscountCodeCommand model);
        Task<ApiResponseData<DiscountCode>> DiscountCodeEdit(DiscountCodeCommand model);
        Task<ApiResponseData<DiscountCode>> DiscountCodeDelete(int id);
        #endregion

        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);
    }

    public class CampaignService : ICampaignService
    {
        private readonly ILogger _logger;
        private readonly ICampaignRepository _iCampaignRepository;
        private readonly IVoucherCodeRepository _iVoucherCodeRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IDepositEventRepository _iDepositEventRepository;

        private readonly IProjectRepository _iProjectRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly string controllerName = "";
        private readonly IDiscountCodeRepository _iDiscountCodeRepository;

        public CampaignService
        (
            ILogger<CampaignService> logger,
            ICampaignRepository iCampaignRepository,
            IVoucherCodeRepository iVoucherCodeRepository,
            ILogRepository iLogRepository,
            IUserRepository iUserRepository,
            IDepositEventRepository iDepositEventRepository,
            IProjectRepository iProjectRepository,
            IAccountRepository iAccountRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IWalletTransactionRepository iWalletTransactionRepository,
            IDiscountCodeRepository iDiscountCodeRepository
        )
        {
            _logger = logger;
            _iCampaignRepository = iCampaignRepository;
            _iVoucherCodeRepository = iVoucherCodeRepository;
            _iLogRepository = iLogRepository;
            _iUserRepository = iUserRepository;
            _iDepositEventRepository = iDepositEventRepository;

            _iProjectRepository = iProjectRepository;
            _iAccountRepository = iAccountRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iDiscountCodeRepository = iDiscountCodeRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        #region [Campaign]
        public async Task<ApiResponseData<List<Campaign>>> SearchPageAsync(int pageIndex, int pageSize, string key, int? projectId, ECampaignStatus? status, ECampaignType? type, string sortby, string sorttype)
        {
            try
            {
                var data = await _iCampaignRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (status == null || x.Status == status)
                          && (projectId == null || x.ProjectId == projectId)
                          && (type == null || x.Type == type)
                          && (key == null || x.Name.Contains(key) || x.Description.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype));

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                var dataGroup = await _iCampaignRepository.GetSumLimit(data.Data.Select(x => x.Id).ToList());

                foreach (var item in data.Data)
                {
                    var getData = dataGroup.FirstOrDefault(x => x.CampaignId == item.Id);
                    if (getData != null)
                    {
                        item.TotalLimit = getData.Limit;
                        item.TotalCurentLimit = getData.CurentLimit;
                    }

                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("CampaignSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Campaign>>();
            }
        }

        public async Task<ApiResponseData<Campaign>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Campaign>() { Data = await _iCampaignRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CampaignSevice.Get", ex.ToString());
                return new ApiResponseData<Campaign>();
            }
        }

        public async Task<ApiResponseData<Campaign>> Create(CampaignCommand model)
        {
            try
            {
                if (model.StartDate.Date > model.EndDate.Date)
                {
                    return new ApiResponseData<Campaign>() { Status = 2 };
                }

                var dl = new Campaign
                {
                    ProjectId = model.ProjectId,
                    Name = model.Name,
                    Status = model.Status,
                    Description = model.Description,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    InvestorId = model.InvestorId,
                    TotalCurentLimit = 0,
                    TotalLimit = 0,
                    UpdatedDate = null,
                    UpdatedUser = null,
                    Type = model.Type ?? ECampaignType.GiftCode,
                    StartDate = model.StartDate,
                    EndDate = model.EndDate
                };

                await _iCampaignRepository.AddAsync(dl);
                await _iCampaignRepository.Commit();

                await AddLog(dl.Id, $"Thêm mới chương trình khuyến mãi '{dl.Name}'", LogType.Insert);

                return new ApiResponseData<Campaign>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CampaignSevice.Create", ex.ToString());
                return new ApiResponseData<Campaign>();
            }
        }

        public async Task<ApiResponseData<Campaign>> Edit(CampaignCommand model)
        {
            try
            {
                if (model.StartDate.Date > model.EndDate.Date)
                {
                    return new ApiResponseData<Campaign>() { Status = 2 };
                }

                var dl = await _iCampaignRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Campaign>() { Data = null, Status = 0 };
                }

                dl.Name = model.Name;
                dl.Status = model.Status;
                dl.Description = model.Description;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iCampaignRepository.Update(dl);
                await _iCampaignRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật chương trình khuyến mãi '{dl.Name}'", LogType.Update);

                return new ApiResponseData<Campaign>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CampaignSevice.Edit", ex.ToString());
                return new ApiResponseData<Campaign>();
            }
        }

        public async Task<ApiResponseData<Campaign>> Delete(int id)
        {
            try
            {
                var dl = await _iCampaignRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Campaign>() { Data = null, Status = 0 };
                }

                var ktUse = await _iWalletTransactionRepository.AnyAsync(x => x.CampaignId == id && x.Status == EWalletTransactionStatus.Done);
                if(ktUse)
                {
                    return new ApiResponseData<Campaign>() { Data = null, Status = 2 };
                }

                _iVoucherCodeRepository.DeleteWhere(x => x.CampaignId == dl.Id);
                await _iVoucherCodeRepository.Commit();

                _iDepositEventRepository.DeleteWhere(x => x.CampaignId == dl.Id);
                await _iDepositEventRepository.Commit();

                _iDiscountCodeRepository.DeleteWhere(x => x.CampaignId == dl.Id);
                await _iDiscountCodeRepository.Commit();

                _iCampaignRepository.Delete(dl);
                await _iCampaignRepository.Commit();

                await AddLog(dl.Id, $"Xóa chương trình khuyến mãi '{dl.Name}'", LogType.Delete);

                return new ApiResponseData<Campaign>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CampaignSevice.Delete", ex.ToString());
                return new ApiResponseData<Campaign>();
            }
        }

        private Func<IQueryable<Campaign>, IOrderedQueryable<Campaign>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Campaign>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Campaign>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "createdDate" => sorttype == "asc" ? EntityExtention<Campaign>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Campaign>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "updateDate" => sorttype == "asc" ? EntityExtention<Campaign>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Campaign>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                _ => EntityExtention<Campaign>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        #region [VoucherCode]
        public async Task<ApiResponseData<List<VoucherCode>>> VoucherCodeSearchPageAsync(int pageIndex, int pageSize, string key, int? campaignId, string sortby, string sorttype)
        {
            try
            {
                return await _iVoucherCodeRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x =>
                    x.CampaignId == campaignId
                    && (key == null || x.Code.Contains(key))
                    ,
                    x => x.OrderByDescending(x => x.Limit).ThenByDescending(x => x.CreatedDate),
                    x => new VoucherCode
                    {
                        Id = x.Id,
                        ProjectId = x.ProjectId,
                        CustomerGroupId = x.CustomerGroupId,
                        CampaignId = x.CampaignId,
                        Code = x.Code,
                        Type = x.Type,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        Limit = x.Limit,
                        CreatedDate = x.CreatedDate,
                        Point = x.Point,
                        Stacked = x.Stacked,
                        CurentLimit = x.CurentLimit,
                        CreatedUser = x.CreatedUser,
                        InvestorId = x.InvestorId,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        Public = x.Public,
                        ProvisoType = x.ProvisoType
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("VoucherCodeSevice.VoucherCodeSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<VoucherCode>>();
            }
        }

        public async Task<ApiResponseData<VoucherCode>> VoucherCodeGetById(int id)
        {
            try
            {
                return new ApiResponseData<VoucherCode>() { Data = await _iVoucherCodeRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("VoucherCodeSevice.VoucherCodeGetById", ex.ToString());
                return new ApiResponseData<VoucherCode>();
            }
        }

        public async Task<ApiResponseData<VoucherCode>> VoucherCodeCreate(VoucherCodeCommand model)
        {
            try
            {
                var camp = await _iCampaignRepository.SearchOneAsync(x => x.Id == model.CampaignId);
                if (camp == null)
                {
                    return new ApiResponseData<VoucherCode>() { Data = null, Status = 0 };
                }

                var dl = new VoucherCode
                {
                    ProjectId = camp.ProjectId,
                    CustomerGroupId = camp.ProjectId == 0 ? 0 : model.CustomerGroupId,
                    CampaignId = model.CampaignId,
                    Code = model.Code,
                    Type = model.Type,
                    Limit = model.Limit,
                    CreatedDate = DateTime.Now,
                    Point = model.Point,
                    Stacked = model.Stacked,
                    CurentLimit = model.Limit,
                    Public = model.Public,
                    StartDate = camp.StartDate,
                    EndDate = camp.EndDate,
                    ProvisoType = model.ProvisoType
                };

                if (model.Stacked)
                {
                    dl.Stacked = true;
                    var ktTT = await _iVoucherCodeRepository.AnyAsync(x => x.Code == model.Code);
                    if (ktTT)
                    {
                        return new ApiResponseData<VoucherCode>() { Data = dl, Status = 2 };
                    }
                    await _iVoucherCodeRepository.AddAsync(dl);
                    await AddLog(dl.CampaignId, $"Thêm mã khuyến mãi '{dl.Code}'", LogType.Insert);
                }
                await _iVoucherCodeRepository.Commit();
                return new ApiResponseData<VoucherCode>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("VoucherCodeSevice.VoucherCodeCreate", ex.ToString());
                return new ApiResponseData<VoucherCode>();
            }
        }

        public async Task<ApiResponseData<VoucherCode>> VoucherCodeEdit(VoucherCodeEditCommand model)
        {
            try
            {
                var dl = await _iVoucherCodeRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<VoucherCode>() { Data = null, Status = 0 };
                }

                var dlCam = await _iCampaignRepository.SearchOneAsync(x => x.Id == dl.CampaignId);
                if (dlCam == null)
                {
                    return new ApiResponseData<VoucherCode>() { Data = null, Status = 0 };
                }

                if (model.Limit > dl.Limit)
                {
                    dl.CurentLimit += model.Limit - dl.Limit;
                }
                else if (model.Limit < dl.Limit)
                {
                    dl.CurentLimit -= dl.Limit - model.Limit;
                }

                dl.ProjectId = dlCam.ProjectId;
                dl.Point = model.Point;
                dl.CustomerGroupId = dlCam.ProjectId == 0 ? 0 : model.CustomerGroupId;
                dl.Limit = model.Limit;
                dl.Public = model.Public;
                dl.StartDate = dlCam.StartDate;
                dl.EndDate = dlCam.EndDate;

                dl.UpdatedUser = UserInfo.UserId;
                dl.UpdatedDate = DateTime.Now;
                dl.ProvisoType = model.ProvisoType;

                _iVoucherCodeRepository.Update(dl);
                await _iVoucherCodeRepository.Commit();

                await AddLog(dl.CampaignId, $"Cập nhật mã khuyến mãi '{dl.Code}'", LogType.Update);

                return new ApiResponseData<VoucherCode>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("VoucherCodeSevice.VoucherCodeEdit", ex.ToString());
                return new ApiResponseData<VoucherCode>();
            }
        }

        public async Task<ApiResponseData<VoucherCode>> VoucherCodeDelete(int id)
        {
            try
            {
                var dl = await _iVoucherCodeRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<VoucherCode>() { Data = null, Status = 0 };
                }

                var ktUse = await _iWalletTransactionRepository.AnyAsync(x => x.CampaignId == dl.CampaignId && x.VoucherCodeId == id && x.Status == EWalletTransactionStatus.Done);
                if (ktUse)
                {
                    return new ApiResponseData<VoucherCode>() { Data = null, Status = 2 };
                }

                _iVoucherCodeRepository.Delete(dl);

                await AddLog(dl.CampaignId, $"Xóa mã khuyến mãi '{dl.Code}'", LogType.Delete);

                await _iVoucherCodeRepository.Commit();
                return new ApiResponseData<VoucherCode>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("VoucherCodeSevice.VoucherCodeDelete", ex.ToString());
                return new ApiResponseData<VoucherCode>();
            }
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        #region [Log]
        public async Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? campaignId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iLogRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.ObjectId == campaignId || campaignId == null) && x.Object == controllerName, LogOrderByExtention(sortby, sorttype));

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.SystemUserId).ToList());
                foreach (var item in data.Data)
                {
                    item.SystemUserObject = users.FirstOrDefault(x => x.Id == item.SystemUserId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.LogSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Domain.Model.Log>>();
            }
        }

        private Func<IQueryable<Domain.Model.Log>, IOrderedQueryable<Domain.Model.Log>> LogOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "CreatedDate" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                _ => EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        #region [DepositEvent]
        public async Task<ApiResponseData<List<DepositEvent>>> DepositEventSearchPageAsync(int pageIndex, int pageSize, int campaignId, string sortby, string sorttype)
        {
            try
            {
                return await _iDepositEventRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => x.CampaignId == campaignId
                    ,
                    DepositEventOrderByExtention(sortby, sorttype));
            }
            catch (Exception ex)
            {
                _logger.LogError("DepositEventSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<DepositEvent>>();
            }
        }

        public async Task<ApiResponseData<DepositEvent>> DepositEventGetById(int id)
        {
            try
            {
                return new ApiResponseData<DepositEvent>() { Data = await _iDepositEventRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DepositEventSevice.Get", ex.ToString());
                return new ApiResponseData<DepositEvent>();
            }
        }

        public async Task<ApiResponseData<DepositEvent>> DepositEventCreate(DepositEventCommand model)
        {
            try
            {
                var dlCam = await _iCampaignRepository.SearchOneAsync(x => x.Id == model.CampaignId);
                if (dlCam == null)
                {
                    return new ApiResponseData<DepositEvent>() { Data = null, Status = 0 };
                }

                var dlAdd = new DepositEvent
                {
                    ProjectId = dlCam.ProjectId,
                    InvestorId = dlCam.InvestorId,
                    CustomerGroupId = dlCam.ProjectId == 0 ? 0 : model.CustomerGroupId,
                    CampaignId = model.CampaignId,
                    StartAmount = model.StartAmount,
                    Point = model.Point,
                    Type = model.Type,
                    Limit = model.Limit,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    Percent = model.Percent,
                    StartDate = dlCam.StartDate,
                    EndDate = dlCam.EndDate,
                    ProvisoType = model.ProvisoType,
                    SubPercent = model.SubPercent,
                    SubPoint = model.SubPoint,
                    MaxAmount = model.MaxAmount,
                    SubPointExpiry = model.SubPointExpiry
                };
                await _iDepositEventRepository.AddAsync(dlAdd);
                await _iDepositEventRepository.Commit();

                dlAdd.TransactionCode = $"DEP-{model.CampaignId:D4}-{dlAdd.Id:D6}";
                _iDepositEventRepository.Update(dlAdd);
                await _iDepositEventRepository.Commit();

                await AddLog(model.CampaignId, $"Thêm sự kiện nạp tiền #{dlAdd.Id}", LogType.Insert);

                return new ApiResponseData<DepositEvent>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DepositEventSevice.Create", ex.ToString());
                return new ApiResponseData<DepositEvent>();
            }
        }

        public async Task<ApiResponseData<DepositEvent>> DepositEventEdit(DepositEventCommand model)
        {
            try
            {
                var dl = await _iDepositEventRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<DepositEvent>() { Data = null, Status = 0 };
                }

                var dlCam = await _iCampaignRepository.SearchOneAsync(x => x.Id == dl.CampaignId);
                if (dlCam == null)
                {
                    return new ApiResponseData<DepositEvent>() { Data = null, Status = 0 };
                }

                dl.InvestorId = dlCam.InvestorId;
                dl.ProjectId = dlCam.ProjectId;
                dl.CustomerGroupId = dl.ProjectId == 0 ? 0 : model.CustomerGroupId;
                dl.StartAmount = model.StartAmount;
                dl.StartDate = dlCam.StartDate;
                dl.EndDate = dlCam.EndDate;
                dl.Point = model.Point;
                dl.Limit = model.Limit;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.Percent = model.Percent;
                dl.Type = model.Type;
                dl.ProvisoType = model.ProvisoType;
                dl.SubPercent = model.SubPercent;
                dl.SubPoint = model.SubPoint;
                dl.MaxAmount = model.MaxAmount;
                dl.SubPointExpiry = model.SubPointExpiry;

                if (string.IsNullOrEmpty(dl.TransactionCode))
                {
                    dl.TransactionCode = $"DEP-{model.CampaignId:D4}-{dl.Id:D6}";
                }

                _iDepositEventRepository.Update(dl);
                await _iDepositEventRepository.Commit();

                await AddLog(model.CampaignId, $"Cập nhật sự kiện nạp tiền #{dl.Id}", LogType.Update);

                return new ApiResponseData<DepositEvent>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DepositEventSevice.Edit", ex.ToString());
                return new ApiResponseData<DepositEvent>();
            }
        }

        public async Task<ApiResponseData<DepositEvent>> DepositEventDelete(int id)
        {
            try
            {
                var dl = await _iDepositEventRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<DepositEvent>() { Data = null, Status = 0 };
                }

                var ktUse = await _iWalletTransactionRepository.AnyAsync(x => x.CampaignId == dl.CampaignId && x.DepositEventId == id && x.Status == EWalletTransactionStatus.Done);
                if (ktUse)
                {
                    return new ApiResponseData<DepositEvent>() { Data = null, Status = 2 };
                }

                _iDepositEventRepository.Delete(dl);
                await _iDepositEventRepository.Commit();

                await AddLog(dl.CampaignId, $"Cập nhật sự kiện nạp tiền #{id}", LogType.Update);

                return new ApiResponseData<DepositEvent>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DepositEventSevice.Delete", ex.ToString());
                return new ApiResponseData<DepositEvent>();
            }
        }

        private Func<IQueryable<DepositEvent>, IOrderedQueryable<DepositEvent>> DepositEventOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "projectId" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.ProjectId)),
                "investorId" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.InvestorId)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.InvestorId)),
                "customerGroupId" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.CustomerGroupId)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.CustomerGroupId)),
                "campaignId" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.CampaignId)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.CampaignId)),
                "transactionCode" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.TransactionCode)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.TransactionCode)),
                "startAmount" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.StartAmount)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.StartAmount)),
                "startDate" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.StartDate)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.StartDate)),
                "endDate" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.EndDate)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.EndDate)),
                "point" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.Point)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.Point)),
                "type" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.Type)),
                "limit" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.Limit)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.Limit)),
                "createdDate" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                "updatedUser" => sorttype == "asc" ? EntityExtention<DepositEvent>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
                _ => EntityExtention<DepositEvent>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        #region [CampaignResult]
        public async Task<ApiResponseData<List<CampaignResult>>> CampaignResultSearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            string sortby,
            string sorttype,
            DateTime? from,
            DateTime? to,
            int? projectId,
            int? campaignId,
            int? customerGroupId,
            ECampaignType? type
        )
        {
            if(from == null || to == null)
            {
                return new ApiResponseData<List<CampaignResult>>();
            } 
            
            var campaigns = await _iCampaignRepository.Search(
                x => (campaignId == null || x.Id == campaignId)
                    && (projectId == null || x.ProjectId == projectId || x.ProjectId == 0)
                    && !(x.EndDate < from || x.StartDate > to),
                null,
                x => new Campaign
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    ProjectId = x.ProjectId,
                    InvestorId = x.InvestorId,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Type = x.Type
                });

            var depositEvents = await _iDepositEventRepository.Search(
                x => campaigns.Select(y => y.Id).Contains(x.CampaignId)
                    && (customerGroupId == null || x.CustomerGroupId == customerGroupId || x.CustomerGroupId == 0),
                null,
                x => new DepositEvent
                {
                    Id = x.Id,
                    Type = x.Type,
                    Used = x.Used,
                    Point = x.Point,
                    Limit = x.Limit,
                    Percent = x.Percent,
                    CampaignId = x.CampaignId,
                    StartAmount = x.StartAmount,
                    CustomerGroupId = x.CustomerGroupId,
                    TransactionCode = x.TransactionCode,
                });

            var voucherCodes = await _iVoucherCodeRepository.Search(
                x => campaigns.Select(y => y.Id).Contains(x.CampaignId)
                    && (customerGroupId == null || x.CustomerGroupId == customerGroupId || x.CustomerGroupId == 0),
                null,
                x => new VoucherCode
                {
                    Id = x.Id,
                    Code = x.Code,
                    Point = x.Point,
                    Limit = x.Limit,
                    CampaignId = x.CampaignId,
                    CurentLimit = x.CurentLimit,
                    CustomerGroupId = x.CustomerGroupId
                });

            var discountCodes = await _iDiscountCodeRepository.Search(
                x => campaigns.Select(y => y.Id).Contains(x.CampaignId)
                    && (customerGroupId == null || x.CustomerGroupId == customerGroupId || x.CustomerGroupId == 0),
                null,
                x => new DiscountCode
                {
                    Id = x.Id,
                    Code = x.Code,
                    Point = x.Point,
                    Limit = x.Limit,
                    CampaignId = x.CampaignId,
                    CurentLimit = x.CurentLimit,
                    CustomerGroupId = x.CustomerGroupId
                });

            campaigns = campaigns.Where(
                x => depositEvents.Select(y => y.CampaignId).Contains(x.Id)
                    || voucherCodes.Select(y => y.CampaignId).Contains(x.Id)
                    || discountCodes.Select(y => y.CampaignId).Contains(x.Id)                    
            ).ToList();

            var projects = await _iProjectRepository.Search(
                x => campaigns.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name
                });

            //
            var data = new ApiResponseData<List<WalletTransaction>>();
            var campaignIds = campaigns.Select(y => y.Id);

            if (type != null)
            {
                if (type == ECampaignType.DepositEvent)
                {
                    data = await _iWalletTransactionRepository.SearchPagedAsync(
                            pageIndex,
                            pageSize,
                            x => x.CampaignId > 0
                                 && campaignIds.Contains(x.CampaignId)
                                 && (x.Type == EWalletTransactionType.Deposit || x.Type == EWalletTransactionType.Manually || x.Type == EWalletTransactionType.Money)
                                 && (from == null || x.CreatedDate >= from)
                                 && (to == null || x.CreatedDate < to.Value.AddDays(1)),
                            x => x.OrderByDescending(o => o.CreatedDate)
                    );
                }
                else if (type == ECampaignType.GiftCode)
                {
                    data = await _iWalletTransactionRepository.SearchPagedAsync(
                            pageIndex,
                            pageSize,
                            x => x.CampaignId > 0
                                 && campaignIds.Contains(x.CampaignId)
                                 && x.Type == EWalletTransactionType.Voucher
                                 && (from == null || x.CreatedDate >= from)
                                 && (to == null || x.CreatedDate < to.Value.AddDays(1)),
                            x => x.OrderByDescending(o => o.CreatedDate)
                    );
                }
                else if (type == ECampaignType.DiscountCode)
                {
                    data = await _iWalletTransactionRepository.SearchPagedAsync(
                            pageIndex,
                            pageSize,
                            x => x.CampaignId > 0
                                && campaignIds.Contains(x.CampaignId)
                                && x.Type == EWalletTransactionType.Paid
                                && (from == null || x.CreatedDate >= from)
                                && (to == null || x.CreatedDate < to.Value.AddDays(1)),
                            x => x.OrderByDescending(o => o.CreatedDate)
                    );
                }
            }
            else
            {
                data = await _iWalletTransactionRepository.SearchPagedAsync(
                        pageIndex,
                        pageSize,
                        x => x.CampaignId > 0
                            && campaignIds.Contains(x.CampaignId)
                            && (from == null || x.CreatedDate >= from)
                            && (to == null || x.CreatedDate < to.Value.AddDays(1)),
                        x => x.OrderByDescending(o => o.CreatedDate)
                );
            }

            var walletTransactions = data.Data;

            var customerGroups = await _iCustomerGroupRepository.Search(
                x => depositEvents.Select(y => y.CustomerGroupId).Distinct().Contains(x.Id)
                    || voucherCodes.Select(y => y.CustomerGroupId).Distinct().Contains(x.Id)
                    || discountCodes.Select(y => y.CustomerGroupId).Distinct().Contains(x.Id),
                null,
                x => new CustomerGroup
                {
                    Id = x.Id,
                    Name = x.Name,
                    ProjectId = x.ProjectId
                });

            var accounts = await _iAccountRepository.Search(
                x => walletTransactions.Select(y => y.AccountId).Distinct().Contains(x.Id),
                null,
                x => new Account
                {
                    Id = x.Id,
                    ManagerUserId = x.ManagerUserId,
                    SubManagerUserId = x.SubManagerUserId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    RFID = x.RFID,
                    CreatedDate = x.CreatedDate
                });

            var users = await _iUserRepository.Search(
                x => accounts.Select(y => y.ManagerUserId).Distinct().Contains(x.Id)
                    || accounts.Select(y => y.SubManagerUserId).Distinct().Contains(x.Id),
                null,
                x => new ApplicationUser
                {
                    Id = x.Id,
                    Code = x.Code
                });

            var result = new List<CampaignResult> { };

            var campaignResult = new CampaignResult { };

            foreach (var item in walletTransactions)
            {
                campaignResult.WalletTransaction = item;
                campaignResult.Project = projects.FirstOrDefault(x => x.Id == campaigns.FirstOrDefault(y => y.Id == item.CampaignId)?.ProjectId);
                campaignResult.Campaign = campaigns.FirstOrDefault(x => x.Id == item.CampaignId);
                campaignResult.Customer = accounts.FirstOrDefault(x => x.Id == item.AccountId);

                if (item.Type == EWalletTransactionType.Deposit || item.Type == EWalletTransactionType.Manually || item.Type == EWalletTransactionType.Money)
                {
                    var depositEvent = depositEvents.FirstOrDefault(x => x.Id == item.DepositEventId);
                    if (depositEvent != null)
                    {
                        campaignResult.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == depositEvent.CustomerGroupId);
                        campaignResult.Code = depositEvent.TransactionCode;
                        campaignResult.StartAmount = depositEvent.StartAmount;
                        campaignResult.Limit = depositEvent.Limit;
                        if (depositEvent.Type == EDepositEventType.Point)
                        {
                            campaignResult.Point = depositEvent.Point;
                        }
                        else
                        {
                            campaignResult.Percent = depositEvent.Percent;
                        }
                    }
                }
                else if (item.Type == EWalletTransactionType.Voucher)
                {
                    var voucherCode = voucherCodes.FirstOrDefault(x => x.Id == item.VoucherCodeId);
                    if (voucherCode != null)
                    {
                        campaignResult.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == voucherCode.CustomerGroupId);
                        campaignResult.Code = voucherCode.Code;
                        campaignResult.Point = voucherCode.Point;
                        campaignResult.Limit = voucherCode.Limit;
                    }
                }
                else if (item.Type == EWalletTransactionType.Paid)
                {
                    var discountCode = discountCodes.FirstOrDefault(x => x.Id == item.VoucherCodeId);
                    if (discountCode != null)
                    {
                        campaignResult.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == discountCode.CustomerGroupId);
                        campaignResult.Code = discountCode.Code;
                        campaignResult.Point = discountCode.Point;
                        campaignResult.Limit = discountCode.Limit;
                    }
                }
                //
                result.Add(campaignResult);
            }
            //
            result.OrderByDescending(x=>x.WalletTransaction.CreatedDate);
            //
            return new ApiResponseData<List<CampaignResult>>
            {
                Data = result,
                PageSize = pageSize,
                PageIndex = pageIndex,
                Count = data.Count
            };
        }

        public async Task<ApiResponseData<List<Campaign>>> SearchByCampaign(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _iCampaignRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.Name.Contains(key) || x.Description.Contains(key));

                var projects = await _iProjectRepository.Search(x => data.Select(y => y.ProjectId).Contains(x.Id));
                foreach (var item in data)
                {
                    item.Project = projects.FirstOrDefault(x => x.Id == item.ProjectId);
                }

                return new ApiResponseData<List<Campaign>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CampaignSevice.SearchByCampaign", ex.ToString());
                return new ApiResponseData<List<Campaign>>();
            }
        }
        #endregion

        #region [DiscountCode]
        public async Task<ApiResponseData<List<DiscountCode>>> DiscountCodeSearchPageAsync(int pageIndex, int pageSize, int campaignId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iDiscountCodeRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => x.CampaignId == campaignId
                    ,
                    DiscountCodeOrderByExtention(sortby, sorttype));
                var accounts = await _iAccountRepository.Search(x => data.Data.Select(x => x.BeneficiaryAccountId).Contains(x.Id), null, x => new Account() { Id = x.Id, FullName = x.FullName, Phone = x.Phone, Status = x.Status, Type = x.Type });
                foreach(var item in data.Data)
                {
                    item.BeneficiaryAccount = accounts.FirstOrDefault(x => x.Id == item.BeneficiaryAccountId);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("DiscountCodeSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<DiscountCode>>();
            }
        }

        public async Task<ApiResponseData<DiscountCode>> DiscountCodeGetById(int id)
        {
            try
            {
                return new ApiResponseData<DiscountCode>() { Data = await _iDiscountCodeRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DiscountCodeSevice.Get", ex.ToString());
                return new ApiResponseData<DiscountCode>();
            }
        }

        public async Task<ApiResponseData<DiscountCode>> DiscountCodeCreate(DiscountCodeCommand model)
        {
            try
            {
                var camp = await _iCampaignRepository.SearchOneAsync(x => x.Id == model.CampaignId);
                if (camp == null)
                {
                    return new ApiResponseData<DiscountCode>() { Data = null, Status = 0 };
                }

                var ktTT = await _iVoucherCodeRepository.AnyAsync(x => x.Code == model.Code);
                if (ktTT)
                {
                    return new ApiResponseData<DiscountCode>() { Data = null, Status = 2 };
                }

                var dlAdd = new DiscountCode
                {
                    ProjectId = camp.ProjectId,
                    CustomerGroupId = camp.ProjectId == 0 ? 0 : model.CustomerGroupId,
                    CampaignId = model.CampaignId,
                    Code = model.Code,
                    Type = model.Type,
                    StartDate = camp.StartDate,
                    EndDate = camp.EndDate,
                    Limit = model.Limit,
                    Point = model.Point,
                    Public = model.Public,
                    ProvisoType = model.ProvisoType,
                    CreatedDate = DateTime.Now,
                    BeneficiaryAccountId = model.BeneficiaryAccountId,
                    Percent = model.Percent,
                    CurentLimit = model.Limit
                };
                await _iDiscountCodeRepository.AddAsync(dlAdd);
                await _iDiscountCodeRepository.Commit();
                await AddLog(dlAdd.CampaignId, $"Thêm mã giảm giá '{dlAdd.Code}'", LogType.Insert);

                return new ApiResponseData<DiscountCode>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DiscountCodeSevice.Create", ex.ToString());
                return new ApiResponseData<DiscountCode>();
            }
        }

        public async Task<ApiResponseData<DiscountCode>> DiscountCodeEdit(DiscountCodeCommand model)
        {
            try
            {
                var dl = await _iDiscountCodeRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<DiscountCode>() { Data = null, Status = 0 };
                }

                var camp = await _iCampaignRepository.SearchOneAsync(x => x.Id == model.CampaignId);
                if (camp == null)
                {
                    return new ApiResponseData<DiscountCode>() { Data = null, Status = 0 };
                }

                if (model.Limit > dl.Limit)
                {
                    dl.CurentLimit += model.Limit - dl.Limit;
                }
                else if (model.Limit < dl.Limit)
                {
                    dl.CurentLimit -= dl.Limit - model.Limit;
                }

                dl.CustomerGroupId = camp.ProjectId == 0 ? 0 : model.CustomerGroupId;
                dl.Type = model.Type;
                dl.StartDate = camp.StartDate;
                dl.EndDate = camp.EndDate;
                dl.Limit = model.Limit;
                dl.Point = model.Point;
                dl.Public = model.Public;
                dl.ProvisoType = model.ProvisoType;
                dl.UpdatedUser = UserInfo.UserId;
                dl.UpdatedDate = DateTime.Now;
                dl.BeneficiaryAccountId = model.BeneficiaryAccountId;
                dl.Percent = model.Percent;

                _iDiscountCodeRepository.Update(dl);
                await _iDiscountCodeRepository.Commit();

                await AddLog(dl.CampaignId, $"Cập nhật mã giảm giá '{dl.Code}'", LogType.Update);

                return new ApiResponseData<DiscountCode>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DiscountCodeSevice.Edit", ex.ToString());
                return new ApiResponseData<DiscountCode>();
            }
        }
        public async Task<ApiResponseData<DiscountCode>> DiscountCodeDelete(int id)
        {
            try
            {
                var dl = await _iDiscountCodeRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<DiscountCode>() { Data = null, Status = 0 };
                }

                var ktUse = await _iWalletTransactionRepository.AnyAsync(x => x.CampaignId == dl.CampaignId && x.VoucherCodeId == id && x.Status == EWalletTransactionStatus.Done);
                if (ktUse)
                {
                    return new ApiResponseData<DiscountCode>() { Data = null, Status = 2 };
                }
                _iDiscountCodeRepository.Delete(dl);
                await _iDiscountCodeRepository.Commit();

                await AddLog(dl.CampaignId, $"Xóa mã giảm giá '{dl.Code}'", LogType.Delete);
                return new ApiResponseData<DiscountCode>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DiscountCodeSevice.Delete", ex.ToString());
                return new ApiResponseData<DiscountCode>();
            }
        }

        private Func<IQueryable<DiscountCode>, IOrderedQueryable<DiscountCode>> DiscountCodeOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "projectId" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.ProjectId)),
                "customerGroupId" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.CustomerGroupId)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.CustomerGroupId)),
                "campaignId" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.CampaignId)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.CampaignId)),
                "code" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.Code)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.Code)),
                "type" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.Type)),
                "startDate" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.StartDate)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.StartDate)),
                "endDate" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.EndDate)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.EndDate)),
                "limit" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.Limit)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.Limit)),
                "curentLimit" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.CurentLimit)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.CurentLimit)),
                "point" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.Point)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.Point)),
                "public" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.Public)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.Public)),
                "provisoType" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.ProvisoType)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.ProvisoType)),
                "createdDate" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                "updatedUser" => sorttype == "asc" ? EntityExtention<DiscountCode>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
                _ => EntityExtention<DiscountCode>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }
    }
}
