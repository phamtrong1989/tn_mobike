﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Base.Command;
using TN.Utility;
using PT.Base;
using PT.Base.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using PT.Domain.Model;
using System.Net.Http.Headers;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Microsoft.AspNetCore.SignalR;

namespace TN.Base.Services
{
    public interface ICustomersService : IService<Account>
    {
        Task<ApiResponseData<List<Account>>> SearchPageAsync(int pageIndex, int pageSize, string key, int? customerGroupId, EAccountVerifyStatus? verifyStatus, EAccountStatus? status, EAccountType? type, ESex? sex, int? id, int? userId, string orderby, string ordertype);
        Task<ApiResponseData<Account>> GetById(int id, HttpRequest request);
        Task<ApiResponseData<Account>> Create(AccountEditCommand model);
        Task<ApiResponseData<Account>> Edit(AccountEditCommand model);
        Task<ApiResponseData<object>> ChangePassword(AccountChangePasswordCommand model);
        Task<ApiResponseData<Account>> ConnectEmployee(AccountEditCommand2 model);
        Task<ApiResponseData<Account>> VerifyEdit(AccountVerifyCommand model);
        Task<ApiResponseData<List<WalletTransaction>>> WalletTransactionSearchPageAsync(int pageIndex, int pageSize, int accountId, string sortby, string sorttype);
        Task<ApiResponseData<List<Feedback>>> FeedbackSearchPage(int pageIndex, int pageSize, int accountId, string sortby, string sorttype);
        Task<ApiResponseData<List<Transaction>>> TransactionSearchPageAsync(int pageIndex, int pageSize, int accountId, string orderby, string ordertype);
        Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? accountId, string sortby, string sorttype);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);
        Task<ApiResponseData<Account>> EditCustomerGroup(int accountId, List<EditCustomerGroupCommand> model, HttpRequest request);
        FileStream View(string pathData, bool isPrivate);
        Task<string> ExportDataAsync();
        Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key);

        Task<ApiResponseData<List<AccountBlackList>>> AccountBlackListSearchPageAsync(int pageIndex, int pageSize, int accountId);
        Task<ApiResponseData<AccountBlackList>> AccountBlackListGetById(int id);
        Task<ApiResponseData<AccountBlackList>> AccountBlackListCreate(AccountBlackListCommand model);
        Task<ApiResponseData<AccountBlackList>> AccountBlackListEdit(AccountBlackListCommand model);
        Task<ApiResponseData<AccountBlackList>> AccountBlackListDelete(int id);
    }
    public class CustomersService : ICustomersService
    {
        private readonly ILogger _logger;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly ICampaignRepository _iCampaignRepository;
        private readonly IFeedbackRepository _iFeedbackRepository;
        private readonly IBikeRepository _iBikeRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly ITicketPrepaidRepository _iTicketPrepaidRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private IWebHostEnvironment _iWebHostEnvironment;
        private readonly BaseSettings _baseSettings;
        private readonly INotificationJobRepository _iNotificationJobRepository;
        private readonly List<NotificationTemp> _notificationTempSettings;
        private readonly IGPSDataRepository _iEventSystemRepository;
        private readonly IHubContext<EventSystemHub> _hubContext;
        private readonly string controllerName = "";
        private readonly IOptions<LogSettings> _logSettings;
        private readonly IAccountBlackListRepository _iAccountBlackListRepository;
        private readonly IProjectRepository _iProjectRepository;
        public CustomersService
        (
            ILogger<CustomersService> logger,
            IAccountRepository iAccountRepository,
            IWalletRepository iWalletRepository,
            ITransactionRepository iTransactionRepository,
            IWalletTransactionRepository iWalletTransactionRepository,
            ILogRepository iLogRepository,
            ICampaignRepository iCampaignRepository,
            IFeedbackRepository iFeedbackRepository,
            IBikeRepository iBikeRepository,
            ITicketPriceRepository iTicketPriceRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IUserRepository iUserRepository,
            ITicketPrepaidRepository iTicketPrepaidRepository,
            IProjectAccountRepository iProjectAccountRepository,
            IWebHostEnvironment iWebHostEnvironment,
            IOptions<BaseSettings> baseSettings,
            INotificationJobRepository iNotificationJobRepository,
            IOptions<List<NotificationTemp>> notificationTempSettings,
            IGPSDataRepository iEventSystemRepository,
            IHubContext<EventSystemHub> hubContext,
            IOptions<LogSettings> logSettings,
            IAccountBlackListRepository iAccountBlackListRepository,
            IProjectRepository iProjectRepository
        )
        {
            _logger = logger;
            _iAccountRepository = iAccountRepository;
            _iWalletRepository = iWalletRepository;
            _iTransactionRepository = iTransactionRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iLogRepository = iLogRepository;
            _iCampaignRepository = iCampaignRepository;
            _iFeedbackRepository = iFeedbackRepository;
            _iBikeRepository = iBikeRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iTicketPriceRepository = iTicketPriceRepository;
            _iUserRepository = iUserRepository;
            _iTicketPrepaidRepository = iTicketPrepaidRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iWebHostEnvironment = iWebHostEnvironment;
            _baseSettings = baseSettings.Value;
            _iNotificationJobRepository = iNotificationJobRepository;
            _notificationTempSettings = notificationTempSettings.Value;
            _iEventSystemRepository = iEventSystemRepository;
            _hubContext = hubContext;
            _logSettings = logSettings;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
            _iAccountBlackListRepository = iAccountBlackListRepository;
            _iProjectRepository = iProjectRepository;
        }

        public async Task<ApiResponseData<List<Account>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? customerGroupId,
            EAccountVerifyStatus? verifyStatus,
            EAccountStatus? status,
            EAccountType? type,
            ESex? sex,
            int? id,
            int? userId,
            string orderby,
            string ordertype)
        {
            try
            {

                var data = await _iAccountRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x =>
                    (id == null || x.Id == id) &&
                    (verifyStatus == null || x.VerifyStatus == verifyStatus) &&
                    (status == null || x.Status == status) &&
                    (type == null || x.Type == type) &&
                    (sex == null || x.Sex == sex) &&
                    (userId == null || x.ManagerUserId == userId || x.SubManagerUserId == userId) &&
                    (key == null || x.Address.Contains(key) || x.Phone == key || x.FirstName == key || x.LastName == key || x.Email == key || x.Code == key | x.ShareCode == key)
                    ,
                    OrderByExtention(orderby, ordertype),
                    x => new Account
                    {
                        Id = x.Id,
                        Address = x.Address,
                        Avatar = x.Avatar,
                        Birthday = x.Birthday,
                        CreatedDate = x.CreatedDate,
                        CreatedSystemUserId = x.CreatedSystemUserId,
                        Email = x.Email,
                        Etag = x.Etag,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Phone = x.Phone,
                        Sex = x.Sex,
                        Status = x.Status,
                        Type = x.Type,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedSystemUserId = x.UpdatedSystemUserId,
                        Username = x.Username,
                        Language = x.Language,
                        Code = x.Code,
                        ShareCode = x.ShareCode,
                        IdentificationID = x.IdentificationID,
                        IdentificationPhotoBackside = x.IdentificationPhotoBackside,
                        IdentificationPhotoFront = x.IdentificationPhotoFront,
                        IdentificationType = x.IdentificationType,
                        Note = x.Note,
                        NoteLock = x.NoteLock,
                        VerifyNote = x.VerifyNote,
                        VerifyStatus = x.VerifyStatus,
                        RFID = x.RFID,
                        FullName = x.FullName,
                        LanguageId = x.LanguageId,
                        CreateType = x.CreateType,
                        UpdatedSystemDate = x.UpdatedSystemDate,
                        UpdatedUserId = x.UpdatedUserId,
                        CreatedSystemDate = x.CreatedSystemDate,
                        CreatedUserId = x.CreatedUserId,
                        VerifyDate = x.VerifyDate,
                        VerifyUserId = x.VerifyUserId,
                        SubManagerUserId = x.SubManagerUserId,
                        ManagerUserId = x.ManagerUserId,
                        IsLock = x.IsLock,
                        IsReLogin = x.IsReLogin,
                        MaxBookingBike = x.MaxBookingBike
                    });
                var userIds = data.Data.Select(x => x.Id).Distinct().ToList();
                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.ManagerUserId).Distinct().ToList(), data.Data.Distinct().Select(x => x.SubManagerUserId).ToList(), data.Data.Distinct().Select(x => x.UpdatedSystemUserId).ToList());
                var wallets = await _iWalletRepository.Search(x => userIds.Contains(x.AccountId));

                var debts = await _iTransactionRepository.Search(x => x.Status == EBookingStatus.Debt && userIds.Contains(x.AccountId));
                var projectAccounts = await _iProjectAccountRepository.Search(
                        x => data.Data.Select(y => y.Id).Contains(x.AccountId),
                        null,
                        x => new ProjectAccount
                        {
                            AccountId = x.AccountId,
                            ProjectId = x.ProjectId,
                            CustomerGroupId = x.CustomerGroupId
                        }
                    );
                var projects = await _iProjectRepository.Search(
                     x => projectAccounts.Select(y => y.ProjectId).Contains(x.Id),
                        null,
                        x => new Project
                        {
                            Id = x.Id,
                            Name = x.Name
                        }
                    );
                var customerGroups = await _iCustomerGroupRepository.Search(
                        x => projectAccounts.Select(y => y.CustomerGroupId).Contains(x.Id),
                        null,
                        x => new CustomerGroup
                        {
                            Id = x.Id,
                            Name = x.Name,
                            ProjectId = x.ProjectId
                        }
                    );

                data.Data.ForEach(item =>
                {
                    item.ManagerUserObject = users.FirstOrDefault(x => x.Id == item.ManagerUserId);
                    item.SubManagerUserObject = users.FirstOrDefault(x => x.Id == item.SubManagerUserId);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedSystemUserId);
                    item.Wallet = wallets.FirstOrDefault(x => x.AccountId == item.Id);
                    if (item.Wallet != null)
                    {
                        item.Wallet.Debt = debts.FirstOrDefault(x => x.AccountId == item.Id)?.ChargeDebt ?? 0;
                    }

                    item.Projects = projects.Where(x => projectAccounts.Where(y => y.AccountId == item.Id).Select(y => y.ProjectId).Contains(x.Id));
                    item.CustomerGroups = customerGroups.Where(x => projectAccounts.Where(y => y.AccountId == item.Id).Select(y => y.CustomerGroupId).Contains(x.Id));
                    if (item.CustomerGroups.Any())
                    {
                        foreach (var cg in item.CustomerGroups)
                        {
                            cg.Project = projects.FirstOrDefault(y => y.Id == cg.ProjectId);
                        }
                    }
                });

                if (userId > 0)
                {
                    var x = await _iUserRepository.SearchOneAsync(x => x.Id == userId);
                    if (x != null)
                    {
                        data.OutData = new ApplicationUser { Id = x.Id, DisplayName = x.DisplayName, UserName = x.UserName, Sex = x.Sex, PhoneNumber = x.PhoneNumber, FullName = x.FullName, Code = x.Code, RoleType = x.RoleType, RoleName = x.RoleType.ToDisplayName() };
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }

        public async Task<ApiResponseData<Account>> GetById(int id, HttpRequest request)
        {

            try
            {
                var data = await _iAccountRepository.SearchOneAsync(x => x.Id == id);
                if (data == null)
                {
                    return new ApiResponseData<Account>() { Status = 0 };
                }

                data.Password = "";
                data.PasswordSalt = "";
                data.NoteLock = "";
                data.MobileToken = "";
                data.Wallet = await _iWalletRepository.GetWalletAsync(id);
                data.Transactions = await _iTransactionRepository.SearchTop(10, t => t.AccountId == id, x => x.OrderByDescending(m => m.Id));
                data.WalletTransactions = await _iWalletTransactionRepository.SearchTop(10, x => x.AccountId == id, o => o.OrderByDescending(m => m.Id));
                data.TicketPrepaids = await _iTicketPrepaidRepository.SearchTop(10, x => x.AccountId == id, o => o.OrderByDescending(m => m.Id));
                data.IdentificationPhotoBackside = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{data.IdentificationPhotoBackside}";
                data.IdentificationPhotoFront = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{data.IdentificationPhotoFront}";
                data.Avatar = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{data.Avatar}";
                data.ProjectAccounts = await _iProjectAccountRepository.Search(x => x.AccountId == id);

                var managerUser = await _iUserRepository.SearchOneAsync(x => x.Id == data.ManagerUserId);
                if (managerUser != null)
                {
                    data.ManagerUserObject = new ApplicationUser()
                    {
                        Id = managerUser.Id,
                        FullName = managerUser.FullName,
                        UserName = managerUser.UserName,
                        PhoneNumber = managerUser.PhoneNumber,
                        Email = managerUser.Email,
                        Code = managerUser.Code
                    };
                }
                foreach (var item in data.TicketPrepaids)
                {
                    var ticketPrice = new TicketPrice
                    {
                        AllowChargeInSubAccount = item.TicketPrice_AllowChargeInSubAccount,
                        BlockPerMinute = item.TicketPrice_BlockPerMinute,
                        BlockPerViolation = item.TicketPrice_BlockPerViolation,
                        BlockViolationValue = item.TicketPrice_BlockViolationValue,
                        CustomerGroupId = item.CustomerGroupId,
                        LimitMinutes = item.TicketPrice_LimitMinutes,
                        ProjectId = item.ProjectId,
                        TicketValue = item.TicketPrice_TicketValue,
                        TicketType = item.TicketPrice_TicketType,
                        IsDefault = item.TicketPrice_IsDefault
                    };
                    item.TicketTypeNote = _iTransactionRepository.GetTicketPriceString(ticketPrice);
                }

                return new ApiResponseData<Account>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.Get", ex.ToString());
                return new ApiResponseData<Account>();
            }
        }

        public async Task<ApiResponseData<Account>> Create(AccountEditCommand model)
        {
            try
            {
                if (model.Type != EAccountType.Normal && string.IsNullOrEmpty(model.RFID))
                {
                    return new ApiResponseData<Account>() { Status = 4 };
                }

                var isPhone = await _iAccountRepository.AnyAsync(m => m.Phone == model.Phone);
                if (isPhone)
                {
                    return new ApiResponseData<Account>() { Status = 2 };
                }

                if (model.Type != EAccountType.Normal)
                {
                    var isRfidUser = await _iUserRepository.AnyAsync(m => m.RFID == model.RFID);
                    if (isRfidUser)
                    {
                        return new ApiResponseData<Account>() { Status = 3 };
                    }

                    var isRfid = await _iAccountRepository.AnyAsync(m => m.RFID == model.RFID);
                    if (isRfid)
                    {
                        return new ApiResponseData<Account>() { Status = 3 };
                    }
                }

                if (model.Type == EAccountType.Normal)
                {
                    model.RFID = null;
                }

                var dlAdd = new Account
                {
                    RFID = model.RFID,
                    Password = Function.HMACMD5Password(model.Password ?? ""),
                    Email = model.Email,
                    Sex = model.Sex,
                    FullName = model.FullName,
                    Birthday = model.Birthday,
                    Phone = model.Phone,
                    Address = model.Address,
                    Status = model.Status,
                    Note = model.Note,
                    Type = model.Type,
                    LanguageId = model.LanguageId,
                    CreateType = EAccountCreateType.Admin,
                    CreatedSystemDate = DateTime.Now,
                    CreatedSystemUserId = UserInfo.UserId,
                    PasswordSalt = Guid.NewGuid().ToString(),
                    MaxBookingBike = model.MaxBookingBike
                };

                await _iAccountRepository.AddAsync(dlAdd);
                await _iAccountRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới tài khoản '{dlAdd.Phone}'", LogType.Insert);
                // Thông báo cho QT
                var data = await _iEventSystemRepository.EventSend(_logSettings.Value, EEventSystemType.M_4004_CoTaiKhoanMoiTao, EEventSystemWarningType.Info, dlAdd.Id, null, 0, 0, 0, 0, EEventSystemType.M_4004_CoTaiKhoanMoiTao.ToEnumGetDisplayName(), $"Nhân viên #{UserInfo.UserId} tạo mới Khách hàng '{dlAdd.FullName}' SĐT '{dlAdd.Phone}' đăng ký tài khoản thành công");
                if (data != null)
                {
                    await _hubContext.Clients.All.SendAsync("ReceiveMessageEventSystemSendMessage", 0, data);
                }
                return new ApiResponseData<Account>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.Create", ex.ToString());
                return new ApiResponseData<Account>();
            }
        }

        public async Task<ApiResponseData<Account>> EditCustomerGroup(int accountId, List<EditCustomerGroupCommand> model, HttpRequest request)
        {
            try
            {
                var dl = await _iAccountRepository.SearchOneAsync(x => x.Id == accountId);
                if (dl == null)
                {
                    return new ApiResponseData<Account>() { Data = null, Status = 0 };
                }

                if (model != null && model.Count > 0)
                {
                    var currents = (await _iProjectAccountRepository.Search(x => x.AccountId == accountId)).ToList();

                    var listIn = model.Where(x => currents.Any(m => m.ProjectId == x.ProjectId)).ToList();

                    var listNotIn = model.Where(x => !currents.Any(m => m.ProjectId == x.ProjectId)).ToList();

                    var listDelete = currents.Where(x => !model.Any(m => m.ProjectId == x.ProjectId)).ToList();
                    // Cập nhật các nhóm khách hàng đã tồn tại trong db
                    foreach (var item in listIn)
                    {
                        var get = currents.FirstOrDefault(x => x.ProjectId == item.ProjectId);
                        if (get != null)
                        {
                            get.CustomerGroupId = item.CustomerGroupId;
                            _iProjectAccountRepository.Update(get);
                        }
                    }
                    // Thêm các nhóm khách hàng đã tồn tại trong db
                    foreach (var item in listNotIn)
                    {
                        await _iProjectAccountRepository.AddAsync(new ProjectAccount { AccountId = accountId, CreatedDate = DateTime.Now, SourceType = EProjectAccountSourceType.Edit, CreatedUser = UserInfo.UserId, CustomerGroupId = item.CustomerGroupId, InvestorId = 0, ProjectId = item.ProjectId });
                    }
                    // Set nhưng nhóm khách hàng không còn tôn tại = 0
                    foreach (var item in listDelete)
                    {
                        var get = currents.FirstOrDefault(x => x.ProjectId == item.ProjectId);
                        if (get != null)
                        {
                            get.CustomerGroupId = 0;
                            _iProjectAccountRepository.Update(get);
                        }
                    }
                    await _iProjectAccountRepository.Commit();
                    _iProjectAccountRepository.DeleteWhere(x => x.AccountId == accountId && x.CustomerGroupId <= 0);
                    await _iProjectAccountRepository.Commit();
                }
                await AddLog(dl.Id, $"Cập nhật nhóm khách hàng cho tài khoản '{dl.Phone}'", LogType.Update);
                return await GetById(accountId, request);
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.Edit", ex.ToString());
                return new ApiResponseData<Account>();
            }
        }

        public async Task<ApiResponseData<Account>> Edit(AccountEditCommand model)
        {
            try
            {
                var dl = await _iAccountRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Account>() { Data = null, Status = 0 };
                }

                if (model.Type != EAccountType.Normal && string.IsNullOrEmpty(model.RFID))
                {
                    return new ApiResponseData<Account>() { Status = 4 };
                }

                var isPhone = await _iAccountRepository.AnyAsync(m => m.Phone == model.Phone && m.Id != dl.Id);
                if (isPhone)
                {
                    return new ApiResponseData<Account>() { Status = 2 };
                }

                if (model.Type != EAccountType.Normal)
                {
                    var isRfid = await _iAccountRepository.AnyAsync(m => m.RFID == model.RFID && m.Id != dl.Id);
                    if (isRfid)
                    {
                        return new ApiResponseData<Account>() { Status = 3 };
                    }

                    var isRfidUser = await _iUserRepository.AnyAsync(m => m.RFID == model.RFID);
                    if (isRfidUser)
                    {
                        return new ApiResponseData<Account>() { Status = 3 };
                    }
                }

                dl.RFID = model.RFID;
                dl.Email = model.Email;
                dl.Sex = model.Sex;
                dl.FullName = model.FullName;
                dl.Birthday = model.Birthday;
                dl.Phone = model.Phone;
                dl.Address = model.Address;
                dl.Status = model.Status;
                dl.Note = model.Note;
                dl.Type = model.Type;
                dl.LanguageId = model.LanguageId;
                dl.UpdatedSystemDate = DateTime.Now;
                dl.UpdatedSystemUserId = UserInfo.UserId;
                dl.MaxBookingBike = model.MaxBookingBike;

                _iAccountRepository.Update(dl);
                await _iAccountRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật tài khoản '{dl.Phone}'", LogType.Update);

                return new ApiResponseData<Account>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.Edit", ex.ToString());
                return new ApiResponseData<Account>();
            }
        }

        public async Task<ApiResponseData<Account>> VerifyEdit(AccountVerifyCommand model)
        {
            try
            {
                var dl = await _iAccountRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Account>() { Data = null, Status = 0 };
                }

                bool isDuocDuyet = model.VerifyStatus == EAccountVerifyStatus.Ok && dl.VerifyStatus != EAccountVerifyStatus.Ok;
                bool isKhongDuocDuyet = model.VerifyStatus == EAccountVerifyStatus.Refuse && dl.VerifyStatus != EAccountVerifyStatus.Refuse;

                dl.IdentificationID = model.IdentificationID;
                dl.IdentificationType = model.IdentificationType;
                dl.VerifyNote = model.VerifyNote;
                dl.VerifyStatus = model.VerifyStatus;
                dl.VerifyDate = DateTime.Now;
                dl.VerifyUserId = UserInfo.UserId;

                dl.UpdatedSystemDate = DateTime.Now;
                dl.UpdatedSystemUserId = UserInfo.UserId;

                _iAccountRepository.Update(dl);
                await _iAccountRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật thông tin xác thực tài khoản '{dl.Phone}'", LogType.Update);

                if (isKhongDuocDuyet)
                {
                    await _iNotificationJobRepository.SendNow(dl.Id, "TNGO thông báo", GetNotificationText(ENotificationTempType.TaiKhoanKhongDuocDuyet), UserInfo.UserId, DockEventType.Not, null);
                }

                if (isDuocDuyet)
                {
                    await _iNotificationJobRepository.SendNow(dl.Id, "TNGO thông báo", GetNotificationText(ENotificationTempType.TaiKhoanDuocDuyet), UserInfo.UserId, DockEventType.Not, null);
                }

                return new ApiResponseData<Account>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.Edit", ex.ToString());
                return new ApiResponseData<Account>();
            }
        }

        public async Task<ApiResponseData<object>> ChangePassword(AccountChangePasswordCommand model)
        {
            try
            {
                var dl = await _iAccountRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<object>() { Data = null, Status = 0 };
                }


                dl.Password = Function.HMACMD5Password(model.NewPassword ?? "");
                dl.PasswordSalt = Guid.NewGuid().ToString();
                dl.UpdatedSystemDate = DateTime.Now;
                dl.UpdatedSystemUserId = UserInfo.UserId;

                _iAccountRepository.Update(dl);
                await _iAccountRepository.Commit();
                await AddLog(dl.Id, $"Thay đổi mật khẩu '{dl.Phone}'", LogType.Update);
                return new ApiResponseData<object>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.ChangePassword", ex.ToString());
                return new ApiResponseData<object>();
            }
        }

        public async Task<ApiResponseData<Account>> ConnectEmployee(AccountEditCommand2 model)
        {
            try
            {
                var dl = await _iAccountRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Account>() { Data = null, Status = 0 };
                }

                if (model.UserId > 0)
                {
                    var manager = await _iUserRepository.SearchOneAsync(x => x.Id == model.UserId);
                    if (manager != null && manager.RoleType == RoleManagerType.CTV_VTS)
                    {
                        dl.SubManagerUserId = model.UserId;
                        dl.ManagerUserId = manager.ManagerUserId;
                    }
                    else
                    {
                        dl.ManagerUserId = model.UserId;
                    }
                }
                else
                {
                    dl.SubManagerUserId = 0;
                    dl.ManagerUserId = 0;
                }

                dl.UpdatedSystemDate = DateTime.Now;
                dl.UpdatedSystemUserId = UserInfo.UserId;

                _iAccountRepository.Update(dl);
                await _iAccountRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật nhân viên hỗ trợ tài khoản tài khoản '{dl.Phone}'", LogType.Update);

                return new ApiResponseData<Account>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.Edit", ex.ToString());
                return new ApiResponseData<Account>();
            }
        }

        public async Task<ApiResponseData<List<WalletTransaction>>> WalletTransactionSearchPageAsync(int pageIndex, int pageSize, int accountId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iWalletTransactionRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.AccountId == accountId) && x.Status != EWalletTransactionStatus.Draft
                    ,
                    WalletTransactionOrderByExtention(sortby, sorttype));

                var ids = data.Data.Select(x => x.CampaignId).ToList();
                var campaigns = await _iCampaignRepository.Search(x => ids.Contains(x.Id));

                foreach (var item in data.Data)
                {
                    item.Campaign = campaigns.FirstOrDefault(x => x.Id == item.CampaignId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletTransactionSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<WalletTransaction>>();
            }
        }

        public async Task<ApiResponseData<List<Feedback>>> FeedbackSearchPage(int pageIndex, int pageSize, int accountId, string sortby, string sorttype)
        {
            try
            {
                return await _iFeedbackRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.AccountId == accountId)
                    ,
                    FeedbackOrderByExtention(sortby, sorttype),
                    x => new Feedback
                    {
                        Id = x.Id,
                        AccountId = x.AccountId,
                        Message = x.Message,
                        Status = x.Status,
                        CreatedDate = x.CreatedDate,
                        Rating = x.Rating,
                        TransactionId = x.TransactionId,
                        Type = x.Type
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("FeedbackSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Feedback>>();
            }
        }

        public async Task<ApiResponseData<List<Transaction>>> TransactionSearchPageAsync(int pageIndex, int pageSize, int accountId, string orderby, string ordertype)
        {
            try
            {
                var data = await _iTransactionRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => x.AccountId == accountId,
                    TransactionOrderByExtention(orderby, ordertype));
                var bikeIds = data.Data.Select(x => x.BikeId).ToList();
                var customerGroupIds = data.Data.Select(x => x.CustomerGroupId).ToList();
                var bikes = await _iBikeRepository.Search(x => bikeIds.Contains(x.Id));
                var customerGroups = await _iCustomerGroupRepository.Search(x => customerGroupIds.Contains(x.Id));

                foreach (var item in data.Data)
                {
                    item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                    item.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);
                    var ticketInfo = await _iTicketPriceRepository.SearchOneAsync(x => x.Id.Equals(item.TicketPriceId));
                    if (ticketInfo != null)
                    {
                        item.TicketPrice_Note = _iTransactionRepository.GetTicketPriceString(new TicketPrice()
                        {
                            Id = ticketInfo.Id,
                            CustomerGroupId = ticketInfo.CustomerGroupId,
                            BlockPerMinute = ticketInfo.BlockPerMinute,
                            AllowChargeInSubAccount = ticketInfo.AllowChargeInSubAccount,
                            BlockPerViolation = ticketInfo.BlockPerViolation,
                            BlockViolationValue = ticketInfo.BlockViolationValue,
                            CreateDate = ticketInfo.CreateDate,
                            IsActive = ticketInfo.IsActive,
                            IsDefault = ticketInfo.IsDefault,
                            ProjectId = ticketInfo.ProjectId,
                            TicketType = ticketInfo.TicketType,
                            TicketValue = ticketInfo.TicketValue,
                            LimitMinutes = ticketInfo.LimitMinutes,
                            Name = ticketInfo.Name,
                            UpdateDate = ticketInfo.UpdateDate
                        });
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.TransactionSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Transaction>>();
            }
        }

        private Func<IQueryable<Account>, IOrderedQueryable<Account>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<Account>, IOrderedQueryable<Account>> functionOrder;
            functionOrder = sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Account>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Account>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "createdDate" => sorttype == "asc" ? EntityExtention<Account>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Account>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<Account>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Account>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                _ => EntityExtention<Account>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
            return functionOrder;
        }

        private Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>> FeedbackOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Feedback>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "createdDate" => sorttype == "asc" ? EntityExtention<Feedback>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                _ => EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        private Func<IQueryable<WalletTransaction>, IOrderedQueryable<WalletTransaction>> WalletTransactionOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "createdDate" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                _ => EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        private Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>> TransactionOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "startTime" => sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.StartTime)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.StartTime)),
                "endTime" => sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.EndTime)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.EndTime)),
                "dateOfPayment" => sorttype == "asc" ? EntityExtention<Transaction>.OrderBy(m => m.OrderBy(x => x.DateOfPayment)) : EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.DateOfPayment)),
                _ => EntityExtention<Transaction>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        #region [Log]
        public async Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? accountId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iLogRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.ObjectId == accountId || accountId == null) && x.Object == controllerName, LogOrderByExtention(sortby, sorttype));

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.SystemUserId).ToList());
                foreach (var item in data.Data)
                {
                    item.SystemUserObject = users.FirstOrDefault(x => x.Id == item.SystemUserId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.LogSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Domain.Model.Log>>();
            }
        }

        private Func<IQueryable<Domain.Model.Log>, IOrderedQueryable<Domain.Model.Log>> LogOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "CreatedDate" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                _ => EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type, string objectType = null)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        #region [Upload file]
        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/Account/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfff}_{Path.GetFileName(file.FileName)}";

                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);

                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }

        public FileStream View(string pathData, bool isPrivate)
        {
            try
            {
                if (isPrivate)
                {
                    string file = $"{_baseSettings.PrivateDataPath}{pathData}";
                    return new FileStream(file, FileMode.Open, FileAccess.Read);
                }
                else
                {
                    return new FileStream(pathData, FileMode.Open, FileAccess.Read);
                }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key)
        {
            try
            {
                var data = await _iUserRepository.SearchTop(10, x => (x.RoleType == RoleManagerType.CTV_VTS || x.RoleType == RoleManagerType.CSKH_VTS || x.RoleType == RoleManagerType.Coordinator_VTS) && (key == null || key == " " || x.Code.Contains(key) || x.FullName.Contains(key) || x.PhoneNumber.Contains(key) || x.Email.Contains(key)), x => x.OrderBy(m => m.Id),
                    x => new ApplicationUser
                    {
                        Id = x.Id,
                        PhoneNumber = x.PhoneNumber,
                        RoleType = x.RoleType,
                        FullName = x.FullName,
                        Email = x.Email,
                        Code = x.Code
                    });
                foreach (var item in data)
                {
                    item.RoleName = item.RoleType.ToEnumGetDisplayName();
                }
                return new ApiResponseData<List<ApplicationUser>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TransactionSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }

        #region [BlackList]
        public async Task<ApiResponseData<List<AccountBlackList>>> AccountBlackListSearchPageAsync(int pageIndex, int pageSize, int accountId)
        {
            try
            {
                return await _iAccountBlackListRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => x.AccountId == accountId,
                    x => x.OrderByDescending(m => m.Time),
                    x => new AccountBlackList
                    {
                        Id = x.Id,
                        AccountId = x.AccountId,
                        Type = x.Type,
                        Note = x.Note,
                        Time = x.Time,
                        CreatedUser = x.CreatedUser,
                        CreatedDate = x.CreatedDate,
                        UpdatedUser = x.UpdatedUser,
                        UpdatedDate = x.UpdatedDate
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountBlackListSevice.AccountBlackListSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<AccountBlackList>>();
            }
        }


        public async Task<ApiResponseData<AccountBlackList>> AccountBlackListGetById(int id)
        {
            try
            {
                return new ApiResponseData<AccountBlackList>() { Data = await _iAccountBlackListRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountBlackListSevice.AccountBlackListGetById", ex.ToString());
                return new ApiResponseData<AccountBlackList>();
            }
        }

        public async Task<ApiResponseData<AccountBlackList>> AccountBlackListCreate(AccountBlackListCommand model)
        {
            try
            {
                var dlAdd = new AccountBlackList
                {
                    AccountId = model.AccountId,
                    Type = model.Type,
                    Note = model.Note,
                    Time = model.Time,
                    CreatedUser = UserInfo.UserId,
                    CreatedDate = DateTime.Now
                };
                await _iAccountBlackListRepository.AddAsync(dlAdd);
                await _iAccountBlackListRepository.Commit();

                return new ApiResponseData<AccountBlackList>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountBlackListSevice.AccountBlackListCreate", ex.ToString());
                return new ApiResponseData<AccountBlackList>();
            }
        }

        public async Task<ApiResponseData<AccountBlackList>> AccountBlackListEdit(AccountBlackListCommand model)
        {
            try
            {
                var dl = await _iAccountBlackListRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<AccountBlackList>() { Data = null, Status = 0 };
                }

                dl.Type = model.Type;
                dl.Note = model.Note;
                dl.Time = model.Time;
                dl.UpdatedUser = UserInfo.UserId;
                dl.UpdatedDate = DateTime.Now;

                _iAccountBlackListRepository.Update(dl);
                await _iAccountBlackListRepository.Commit();
                return new ApiResponseData<AccountBlackList>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountBlackListSevice.AccountBlackListEdit", ex.ToString());
                return new ApiResponseData<AccountBlackList>();
            }
        }
        public async Task<ApiResponseData<AccountBlackList>> AccountBlackListDelete(int id)
        {
            try
            {
                var dl = await _iAccountBlackListRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<AccountBlackList>() { Data = null, Status = 0 };
                }

                _iAccountBlackListRepository.Delete(dl);
                await _iAccountBlackListRepository.Commit();
                return new ApiResponseData<AccountBlackList>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountBlackListSevice.AccountBlackListDelete", ex.ToString());
                return new ApiResponseData<AccountBlackList>();
            }
        }
        #endregion

        private string GetNotificationText(ENotificationTempType type)
        {
            try
            {
                return _notificationTempSettings.FirstOrDefault(x => x.Language == "vi")?.Data?.FirstOrDefault(x => x.Type == type)?.Body;
            }
            catch
            {
                return type.ToString();
            }
        }

        public async Task<string> ExportDataAsync()
        {
            string fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "ExportExcel", "Customer", DateTime.Now.ToString("ddMMyyyy"));

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            FileInfo file = new FileInfo(Path.Combine(folderPath, fileName));

            using (ExcelPackage MyExcel = new ExcelPackage(file))
            {
                MyExcel.Workbook.Worksheets.Add("BC TTKH");
                ExcelWorksheet myWorksheet = MyExcel.Workbook.Worksheets[0];

                var data = (await _iAccountRepository.GetAll()).ToList();

                await FormatExcel(myWorksheet, data);
                MyExcel.SaveAs(file);

                return $"{DateTime.Now.ToString("ddMMyyyy")}/{fileName}";
            }
        }

        #region [BlackList]

        #endregion

        private async Task FormatExcel(ExcelWorksheet worksheet, List<Account> items)
        {
            worksheet.Cells.Style.WrapText = true;

            // Style Columns
            worksheet.Column(1).Width = 5;
            worksheet.Column(2).Width = 20;
            worksheet.Column(3).Width = 25;
            worksheet.Column(4).Width = 10;
            worksheet.Column(5).Width = 15;
            worksheet.Column(6).Width = 15;
            worksheet.Column(7).Width = 15;
            worksheet.Column(8).Width = 20;
            worksheet.Column(9).Width = 20;
            worksheet.Column(10).Width = 15;
            worksheet.Column(11).Width = 15;
            worksheet.Column(12).Width = 10;
            worksheet.Column(13).Width = 15;
            worksheet.Column(14).Width = 15;
            worksheet.Column(15).Width = 15;
            worksheet.Column(16).Width = 15;
            worksheet.Column(17).Width = 15;
            worksheet.Column(18).Width = 15;
            worksheet.Column(19).Width = 15;

            worksheet.Column(14).Style.Numberformat.Format = "#,##0";
            worksheet.Column(15).Style.Numberformat.Format = "#,##0";

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Header
            worksheet.Cells["A2:S2"].Merge = true;
            worksheet.Cells["A2:S2"].Style.Font.Bold = true;
            worksheet.Cells["A2:S2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A2:S2"].Value = $"BÁO CÁO THÔNG TIN KHÁCH HÀNG NGÀY {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã KH";
            worksheet.Cells["C5"].Value = "Tên KH";
            worksheet.Cells["D5"].Value = "Giới tính";
            worksheet.Cells["E5"].Value = "Ngày sinh";
            worksheet.Cells["F5"].Value = "Số CCCD/HC";
            worksheet.Cells["G5"].Value = "SĐT";
            worksheet.Cells["H5"].Value = "Email";
            worksheet.Cells["I5"].Value = "Trạng thái xác thực";
            worksheet.Cells["J5"].Value = "Mã RFID";
            worksheet.Cells["K5"].Value = "Loại TK";
            worksheet.Cells["L5"].Value = "Ngày tạo TK";
            worksheet.Cells["M5"].Value = "Trạng thái TK";
            worksheet.Cells["N5"].Value = "Điểm gốc";
            worksheet.Cells["O5"].Value = "Điểm khuyến mại";
            worksheet.Cells["P5"].Value = "Mã CTV/NV phát triển KH";
            worksheet.Cells["Q5"].Value = "Mã NV quản lý";
            worksheet.Cells["R5"].Value = "Đơn vị đầu tư";
            worksheet.Cells["S5"].Value = "Dự án";

            worksheet.Row(5).Style.Font.Bold = true;

            // Assign borders
            var modelTable = worksheet.Cells[$"A5:S{items.Count + 5}"];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            var rs = 6; // row start loop data table
            // Binding data 
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                worksheet.Cells[rs + i, 1].Value = i + 1;
                worksheet.Cells[rs + i, 2].Value = item.Id.ToString().PadLeft(8, '0');
                worksheet.Cells[rs + i, 3].Value = item.FullName;
                worksheet.Cells[rs + i, 4].Value = item.Sex;
                worksheet.Cells[rs + i, 5].Value = item.Birthday?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs + i, 6].Value = item.IdentificationID;
                worksheet.Cells[rs + i, 7].Value = item.Phone;
                worksheet.Cells[rs + i, 8].Value = item.Email;
                worksheet.Cells[rs + i, 9].Value = item.VerifyStatus;
                worksheet.Cells[rs + i, 10].Value = item.RFID;
                worksheet.Cells[rs + i, 11].Value = item.Type;
                worksheet.Cells[rs + i, 12].Value = item.CreatedDate?.ToString("dd/MM/yyyy");
                worksheet.Cells[rs + i, 13].Value = item.Status;

                var wallet = await _iWalletRepository.SearchOneAsync(w => w.AccountId == item.Id);
                worksheet.Cells[rs + i, 14].Value = wallet?.Balance;
                worksheet.Cells[rs + i, 15].Value = wallet?.TripPoint;

                worksheet.Cells[rs + i, 16].Value = item.SubManagerUserId > 0 ? item.SubManagerUserId : "";
                worksheet.Cells[rs + i, 17].Value = item.ManagerUserId > 0 ? item.ManagerUserId : "";

                var project = await _iProjectAccountRepository.Search(x => x.AccountId == item.Id);
                worksheet.Cells[rs + i, 18].Value = "";
                worksheet.Cells[rs + i, 19].Value = "";
            }
        }
    }
}