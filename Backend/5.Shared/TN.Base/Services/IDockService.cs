﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;
using static TN.Domain.Model.Bike;

namespace TN.Base.Services
{
    public interface IDockService : IService<Dock>
    {
        Task<ApiResponseData<List<Dock>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? projectId,
            int? investorId,
            int? modelId,
            int? producerId,
            int? warehouseId,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            bool? charging,
            EBikeStatus? status,
            int? battery,
            bool? lockStatus,
            string sortby,
            string sorttype
        );
        Task<ApiResponseData<Dock>> GetById(int id);
        Task<ApiResponseData<Dock>> Create(DockCommand model);
        Task<ApiResponseData<Dock>> Edit(DockCommand model);
        Task<ApiResponseData<Dock>> Delete(int id);
        Task<ApiResponseData<List<Bike>>> BikeSearch(string key);
        Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? objectId, string sortby, string sorttype);
        Task<ApiResponseData<string>> OpenLock(DockOpenManualCommand cm);

        Task<FileContentResult> ExportReport(
            string key,
            int? projectId,
            int? investorId,
            int? modelId,
            int? producerId,
            int? warehouseId,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            bool? charging,
            EBikeStatus? status,
            int? battery,
            bool? lockStatus
        );
    }

    public class DockService : IDockService
    {
        private readonly ILogger _logger;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IDockRepository _iDockRepository;
        private readonly IBikeRepository _iBikeRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IWarehouseRepository _iWarehouseRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly IOpenLockRequestRepository _iOpenLockRequestRepository;
        private readonly IOpenLockHistoryRepository _iOpenLockHistoryRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly IEntityBaseRepository<Producer> _producerRepository;
        private readonly IEntityBaseRepository<Model> _modelRepository;
        private readonly string controllerName = "";

        public DockService
        (
            ILogger<DockService> logger,
            IWebHostEnvironment iWebHostEnvironment,

            IDockRepository iDockRepository,
            IBikeRepository iBikeRepository,
            ILogRepository iLogRepository,
            IUserRepository iUserRepository,
            IWarehouseRepository iWarehouseRepository,
            IBookingRepository iBookingRepository,
            IOpenLockRequestRepository iOpenLockRequestRepository,
            IOpenLockHistoryRepository iOpenLockHistoryRepository,
            IProjectRepository iProjectRepository,
            IInvestorRepository iInvestorRepository,
            IStationRepository iStationRepository,
            IEntityBaseRepository<Producer> producerRepository,
            IEntityBaseRepository<Model> modelRepository
        )
        {
            _logger = logger;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iDockRepository = iDockRepository;
            _iBikeRepository = iBikeRepository;
            _iLogRepository = iLogRepository;
            _iUserRepository = iUserRepository;
            _iWarehouseRepository = iWarehouseRepository;
            _iBookingRepository = iBookingRepository;
            _iOpenLockRequestRepository = iOpenLockRequestRepository;
            _iOpenLockHistoryRepository = iOpenLockHistoryRepository;
            _iBikeRepository = iBikeRepository;
            _iUserRepository = iUserRepository;
            _iProjectRepository = iProjectRepository;
            _iInvestorRepository = iInvestorRepository;
            _iDockRepository = iDockRepository;
            _iStationRepository = iStationRepository;
            _iBookingRepository = iBookingRepository;
            _producerRepository = producerRepository;
            _modelRepository = modelRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<Dock>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? projectId,
            int? investorId,
            int? modelId,
            int? producerId,
            int? warehouseId,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            bool? charging,
            EBikeStatus? status,
            int? battery,
            bool? lockStatus,
            string sortby,
            string sorttype)
        {
            try
            {
                var data = await _iDockRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize, key, bookingStatus, x =>
                    (projectId == null || x.ProjectId == projectId)
                    && (investorId == null || x.InvestorId == investorId)
                    && (modelId == null || x.ModelId == modelId)
                    && (producerId == null || x.ProducerId == producerId)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                    && (connectionStatus == null || x.ConnectionStatus == connectionStatus)
                    && (charging == null || x.Charging == charging)
                    && (lockStatus == null || x.LockStatus == lockStatus)
                    && (battery == null || x.Battery <= battery)
                    && (status == null || x.Status == status)
                    ,
                    OrderByExtention(sortby, sorttype));

                var bikes = await _iBikeRepository.Search(x => data.Data.Select(x => x.Id).Contains(x.DockId));
                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);
                var ws = await _iWarehouseRepository.Search(x => data.Data.Select(m => m.WarehouseId).Contains(x.Id));
                var books = await _iBookingRepository.Search(x => data.Data.Select(m => m.Id).Contains(x.DockId) && x.Status == EBookingStatus.Start);

                foreach (var item in data.Data)
                {
                    item.Bike = bikes.FirstOrDefault(x => x.DockId == item.Id);
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.Warehouse = ws.FirstOrDefault(x => x.Id == item.WarehouseId);
                    item.BookingStatus = books.Any(x => x.DockId == item.Id) ? EDockBookingStatus.Moving : EDockBookingStatus.Free;
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("DockSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Dock>>();
            }
        }

        public async Task<ApiResponseData<Dock>> GetById(int id)
        {
            try
            {
                var data = await _iDockRepository.SearchOneAsync(x => x.Id == id);
                if (data != null)
                {
                    data.Bike = await _iBikeRepository.SearchOneAsync(x => x.DockId == data.Id);
                }
                return new ApiResponseData<Dock>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DockSevice.Get", ex.ToString());
                return new ApiResponseData<Dock>();
            }
        }

        public async Task<ApiResponseData<Dock>> Create(DockCommand model)
        {
            try
            {
                var kt = await _iDockRepository.AnyAsync(x => x.IMEI == model.IMEI);
                if (kt)
                {
                    return new ApiResponseData<Dock>() { Status = 2 };
                }

                var kts = await _iDockRepository.AnyAsync(x => x.SerialNumber == model.SerialNumber);
                if (kts)
                {
                    return new ApiResponseData<Dock>() { Status = 3 };
                }

                var ktsim = await _iDockRepository.AnyAsync(x => x.SIM == model.SIM);
                if (ktsim)
                {
                    return new ApiResponseData<Dock>() { Status = 4 };
                }

                var dlAdd = new Dock
                {
                    InvestorId = model.InvestorId,
                    IMEI = model.IMEI,
                    SerialNumber = model.SerialNumber,
                    Status = model.Status,
                    CreatedDate = DateTime.Now,
                    SIM = model.SIM,
                    WarehouseId = model.WarehouseId,
                    ProducerId = model.ProducerId,
                    ModelId = model.ModelId,
                    LastConnectionTime = null,
                    CreatedUser = UserInfo.UserId,
                    Lat = 0,
                    Long = 0,
                    LockStatus = true,
                    ProjectId = model.ProjectId,
                    Battery = 0,
                    //BookingStatus = EDockBookingStatus.Free,
                    Charging = false,
                    ConnectionStatus = false,
                    StationId = model.StationId,
                    Note = model.Note
                };

                if (dlAdd.Status == EBikeStatus.ErrorWarehouse || dlAdd.Status == EBikeStatus.Warehouse)
                {
                    dlAdd.StationId = 0;
                }

                await _iDockRepository.AddAsync(dlAdd);
                await _iDockRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới khóa '{dlAdd.IMEI}'", LogType.Insert);

                return new ApiResponseData<Dock>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DockSevice.Create", ex.ToString());
                return new ApiResponseData<Dock>();
            }
        }

        public async Task<ApiResponseData<Dock>> Edit(DockCommand model)
        {
            try
            {
                var dl = await _iDockRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Dock>() { Data = null, Status = 0 };
                }

                var checkUse = await _iBookingRepository.AnyAsync(x => x.DockId == model.Id && x.Status == EBookingStatus.Start);
                if (checkUse)
                {
                    return new ApiResponseData<Dock>() { Status = 5 };
                }

                var kt = await _iDockRepository.AnyAsync(x => x.IMEI == model.IMEI && x.Id != model.Id);
                if (kt)
                {
                    return new ApiResponseData<Dock>() { Status = 2 };
                }

                var kts = await _iDockRepository.AnyAsync(x => x.SerialNumber == model.SerialNumber && x.Id != model.Id);
                if (kts)
                {
                    return new ApiResponseData<Dock>() { Status = 3 };
                }

                var ktsim = await _iDockRepository.AnyAsync(x => x.SIM == model.SIM && x.Id != model.Id);
                if (ktsim)
                {
                    return new ApiResponseData<Dock>() { Status = 4 };
                }

                // Khi nào không lắp khóa mới ăn theo trạng thái của chính nó,
                // Nếu lắp khóa thì khi xe thay đổi trạng thái thì khóa sẽ phải thay đổi trạng thái theo
                var bike = await _iBikeRepository.SearchOneAsync(x => x.DockId == dl.Id);
                if (bike == null)
                {
                    dl.StationId = model.StationId;
                    dl.Status = model.Status;
                    dl.WarehouseId = model.WarehouseId;
                }
                else if (bike.ProjectId != model.ProjectId)
                {
                    return new ApiResponseData<Dock>() { Status = 6 };
                }
                else if (bike.InvestorId != model.InvestorId)
                {
                    return new ApiResponseData<Dock>() { Status = 7 };
                }

                dl.InvestorId = model.InvestorId;
                dl.IMEI = model.IMEI;
                dl.SerialNumber = model.SerialNumber;
                dl.SIM = model.SIM;

                dl.ProducerId = model.ProducerId;
                dl.ModelId = model.ModelId;
                dl.ProjectId = model.ProjectId;


                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.Note = model.Note;

                if (model.Status == EBikeStatus.ErrorWarehouse || model.Status == EBikeStatus.Warehouse)
                {
                    dl.StationId = 0;
                }

                _iDockRepository.Update(dl);
                await _iDockRepository.Commit();
                await AddLog(dl.Id, $"Cập nhật khóa '{dl.IMEI}'", LogType.Update);
                return new ApiResponseData<Dock>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DockSevice.Edit", ex.ToString());
                return new ApiResponseData<Dock>();
            }
        }

        public async Task<ApiResponseData<Dock>> Delete(int id)
        {
            try
            {
                var dl = await _iDockRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Dock>() { Data = null, Status = 0 };
                }

                var checkUse = await _iBookingRepository.AnyAsync(x => x.DockId == dl.Id && x.Status == EBookingStatus.Start);
                if (checkUse)
                {
                    return new ApiResponseData<Dock>() { Status = 5 };
                }

                await AddLog(dl.Id, $"Xóa khóa '{dl.IMEI}'", LogType.Delete);

                _iDockRepository.Delete(dl);
                await _iDockRepository.Commit();
                return new ApiResponseData<Dock>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DockSevice.Delete", ex.ToString());
                return new ApiResponseData<Dock>();
            }
        }

        public async Task<ApiResponseData<List<Bike>>> BikeSearch(string key)
        {
            try
            {
                var data = await _iBikeRepository.SearchTop(20, x => (key == null || key == " " || x.SerialNumber.Contains(key) || x.Plate.Contains(key)), x => x.OrderBy(m => m.SerialNumber));
                return new ApiResponseData<List<Bike>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DockSevice.BikeSearch", ex.ToString());
                return new ApiResponseData<List<Bike>>();
            }
        }

        public async Task<ApiResponseData<Bike>> BikeGetById(int id)
        {
            try
            {
                var data = await _iBikeRepository.SearchOneAsync(x => x.Id == id);
                return new ApiResponseData<Bike>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("DockSevice.BikeGetById", ex.ToString());
                return new ApiResponseData<Bike>();
            }
        }

        #region [ExportReport]
        public async Task<FileContentResult> ExportReport(
            string key,
            int? projectId,
            int? investorId,
            int? modelId,
            int? producerId,
            int? warehouseId,
            EDockBookingStatus? bookingStatus,
            bool? connectionStatus,
            bool? charging,
            EBikeStatus? status,
            int? battery,
            bool? lockStatus
        )
                {
                    var docks = await _iDockRepository.SearchToReport(
                        bookingStatus,
                        x => (projectId == null || x.InvestorId == projectId)
                            && (investorId == null || x.InvestorId == investorId)
                            && (modelId == null || x.ModelId == modelId)
                            && (producerId == null || x.ProducerId == producerId)
                            && (warehouseId == null || x.WarehouseId == warehouseId)
                            && (connectionStatus == null || x.ConnectionStatus == connectionStatus)
                            && (charging == null || x.Charging == charging)
                            && (lockStatus == null || x.LockStatus == lockStatus)
                            && (battery == null || x.Battery <= battery)
                            && (status == null || x.Status == status)
                            && (key == null || x.IMEI.Contains(key) || x.SerialNumber.Contains(key) || x.SIM.Contains(key))
                        );

                    var producers = await _producerRepository.Search(
                        x => docks.Select(x => x.ProducerId).Distinct().Contains(x.Id),
                        null,
                        x => new Producer
                        {
                            Id = x.Id,
                            Name = x.Name
                        });

                    var models = await _modelRepository.Search(
                        x => docks.Select(x => x.ModelId).Distinct().Contains(x.Id),
                        null,
                        x => new Model
                        {
                            Id = x.Id,
                            Name = x.Name
                        });

                    var bikes = await _iBikeRepository.Search(
                        x => docks.Select(x => x.Id).Contains(x.DockId),
                        null,
                        x => new Bike
                        {
                            Id = x.Id,
                            Plate = x.Plate
                        });

                    var stations = await _iStationRepository.Search(
                        x => docks.Select(x => x.StationId).Distinct().Contains(x.Id),
                        null,
                        x => new Station
                        {
                            Id = x.Id,
                            Name = x.Name,
                            ManagerUserId = x.ManagerUserId,
                            ProjectId = x.ProjectId,
                            InvestorId = x.InvestorId
                        });

                    var users = await _iUserRepository.Search(
                        x => stations.Select(y => y.ManagerUserId).Distinct().Contains(x.Id),
                        null,
                        x => new ApplicationUser
                        {
                            Id = x.Id,
                            ManagerUserId = x.ManagerUserId,
                            Code = x.Code,
                        });

                    var projects = await _iProjectRepository.Search(
                        x => stations.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                        null,
                        x => new Project
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Code = x.Code
                        });

                    var investors = await _iInvestorRepository.Search(
                        x => stations.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                        null,
                        x => new Investor
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Code = x.Code
                        });

                    var books = await _iBookingRepository.Search(x => docks.Select(m => m.Id).Contains(x.DockId) && x.Status == EBookingStatus.Start);

                    foreach (var dock in docks)
                    {
                        var station = stations.FirstOrDefault(x => x.Id == dock.StationId);
                        if (station != null)
                        {
                            station.ManagerUserObject = users.FirstOrDefault(x => x.Id == station?.ManagerUserId);
                        }

                        dock.Project = projects.FirstOrDefault(x => x.Id == dock.ProjectId);
                        dock.Investor = investors.FirstOrDefault(x => x.Id == dock.InvestorId);
                        dock.Station = station;
                        dock.Producer = producers.FirstOrDefault(x => x.Id == dock.ProducerId);
                        dock.Model = models.FirstOrDefault(x => x.Id == dock.ModelId);
                        dock.Bike = bikes.FirstOrDefault(x => x.DockId == dock.Id);
                        dock.BookingStatus = books.Any(x => x.DockId == dock.Id) ? EDockBookingStatus.Moving : EDockBookingStatus.Free;
                    }

                    var filePath = InitPath();

                    using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(filePath)))
                    {
                        MyExcel.Workbook.Worksheets.Add("BC Khóa/Sim");
                        ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                        FormatExcelDock(workSheet, docks);
                        MyExcel.Save();
                    }
                    //
                    var memory = new MemoryStream();
                    using (var stream = new FileStream(filePath, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }
                    memory.Position = 0;
                    return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        FileDownloadName = Path.GetFileName(filePath)
                    };
                }

        private void FormatExcelDock(ExcelWorksheet worksheet, List<Dock> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 20}, {4 , 25}, {5 , 15},
                {6 , 22}, {7 , 15}, {8 , 25}, {9 , 10}, {10, 10},
                {11, 15}, {12, 20}, {13, 15}, {14, 15}, {15, 20},
                {16, 20}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2, 3, 5, 6, 7, 9, 10, 11, 12, 13, 14 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            //SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO QUẢN LÝ KHÓA - SIM";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:P5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:P5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:P5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Serial khóa";
            worksheet.Cells["C5"].Value = "IMEI";
            worksheet.Cells["D5"].Value = "Hãng sản xuất";
            worksheet.Cells["E5"].Value = "Mẫu mã";
            worksheet.Cells["F5"].Value = "Số SIM";
            worksheet.Cells["G5"].Value = "Biển số xe gắn khóa";
            worksheet.Cells["H5"].Value = "Đang tại trạm";
            worksheet.Cells["I5"].Value = "Trạng thái khóa/mở";
            worksheet.Cells["J5"].Value = "Pin (%)";
            worksheet.Cells["K5"].Value = "Trạng thái sạc pin";
            worksheet.Cells["L5"].Value = "Thời gian kết nối gần nhất";
            worksheet.Cells["M5"].Value = "Trạng thái giao dịch xe";
            worksheet.Cells["N5"].Value = "Nhân viên quản lý trạm";
            worksheet.Cells["O5"].Value = "Dự án";
            worksheet.Cells["P5"].Value = "Đơn vị đầu tư";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.SerialNumber;
                worksheet.Cells[rs, 3].Value = item.IMEI;
                worksheet.Cells[rs, 4].Value = item.Producer?.Name;
                worksheet.Cells[rs, 5].Value = item.Model?.Name;
                worksheet.Cells[rs, 6].Value = item.SIM;
                worksheet.Cells[rs, 7].Value = item.Bike?.Plate;
                worksheet.Cells[rs, 8].Value = item.Station?.Name;
                worksheet.Cells[rs, 9].Value = item.LockStatus ? "KHÓA" : "MỞ";
                worksheet.Cells[rs, 10].Value = item.Battery;
                worksheet.Cells[rs, 11].Value = item.Charging ? "ĐANG SẠC" : "KHÔNG SẠC";
                worksheet.Cells[rs, 12].Value = item.LastConnectionTime?.ToString("HH:mm:ss dd/MM/yyyy");

                if (item.BookingStatus == EDockBookingStatus.None || item.BookingStatus == EDockBookingStatus.Free)
                {
                    worksheet.Cells[rs, 13].Value = "CHƯA THUÊ";
                }
                else if (item.BookingStatus == EDockBookingStatus.Moving)
                {
                    worksheet.Cells[rs, 13].Value = "ĐANG THUÊ";
                }
                else
                {
                    worksheet.Cells[rs, 13].Value = "";
                }
                worksheet.Cells[rs, 14].Value = item.Station?.ManagerUserObject?.Code;
                worksheet.Cells[rs, 15].Value = item.Project?.Name;
                worksheet.Cells[rs, 16].Value = item.Investor?.Name;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:P{rs - 1}");
            worksheet.Cells[$"A6:P{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        } 
        #endregion

        private Func<IQueryable<Dock>, IOrderedQueryable<Dock>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<Dock>, IOrderedQueryable<Dock>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<Dock>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Dock>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "serialNumber":
                    functionOrder = sorttype == "asc" ? EntityExtention<Dock>.OrderBy(m => m.OrderBy(x => x.SerialNumber)) : EntityExtention<Dock>.OrderBy(m => m.OrderByDescending(x => x.SerialNumber));
                    break;
                case "lastConnectionTime":
                    functionOrder = sorttype == "asc" ? EntityExtention<Dock>.OrderBy(m => m.OrderBy(x => x.LastConnectionTime)) : EntityExtention<Dock>.OrderBy(m => m.OrderByDescending(x => x.LastConnectionTime));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Dock>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Dock>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "updatedDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Dock>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Dock>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate));
                    break;
                default:
                    functionOrder = EntityExtention<Dock>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }

        private async Task AddLog(int objectId, string action, LogType type, string objectType = null)
        {
            string objectName = controllerName;
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = objectName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        #region [Log]
        public async Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? objectId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iLogRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.ObjectId == objectId || objectId == null) && x.Object == controllerName, LogOrderByExtention(sortby, sorttype));

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.SystemUserId).ToList());
                foreach (var item in data.Data)
                {
                    item.SystemUserObject = users.FirstOrDefault(x => x.Id == item.SystemUserId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeSevice.LogSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Domain.Model.Log>>();
            }
        }

        private Func<IQueryable<Domain.Model.Log>, IOrderedQueryable<Domain.Model.Log>> LogOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "CreatedDate" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                _ => EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        public async Task<ApiResponseData<string>> OpenLock(DockOpenManualCommand cm)
        {
            var ktDock = await _iDockRepository.SearchOneAsync(x => x.Id == cm.DockId);
            if (ktDock == null)
            {
                return new ApiResponseData<string>() { Data = null, Status = 0 };
            }

            if(cm.CommandType == ECommandType.S0)
            {
                return new ApiResponseData<string>() { Data = null, Status = 0 };
            }

            if (!ktDock.LockStatus && cm.CommandType == ECommandType.L0)
            {
                return new ApiResponseData<string>() { Data = null, Status = 4 };
            }

            var transactionCode = $"{cm.CommandType}{DateTime.Now:yyyyMMddHHmmssfff}";
            var kt = new OpenLockRequest
            {
                CreatedDate = DateTime.Now,
                IMEI = ktDock.IMEI,
                OpenTime = null,
                Retry = 0,
                StationId = ktDock.StationId,
                Status = LockRequestStatus.Waiting,
                DockId = ktDock.Id,
                AccountId = 0,
                TransactionCode = transactionCode,
                SerialNumber = ktDock.SerialNumber,
                InvestorId = ktDock.InvestorId,
                ProjectId = ktDock.ProjectId,
                CommandType = cm.CommandType,
                CreatedUser = UserInfo.UserId
            };

            await _iOpenLockRequestRepository.AddAsync(kt);
            await _iOpenLockRequestRepository.Commit();

            string textOut = "";
            await AddLog(cm.DockId, $"Thao tác cm ({cm.CommandType}, {cm.CommandType.ToEnumGetDisplayName()}), ghi chú {cm.Note}", LogType.Other);

            if (cm.CommandType == ECommandType.L0)
            {
                for (int i = 0; i < 30; i++)
                {
                    var ktOpen = await _iDockRepository.AnyAsync(x => x.Id == cm.DockId && x.LockStatus == false);
                    if (ktOpen == true)
                    {
                        return new ApiResponseData<string>() { Data = $"{textOut}, mở khóa thành công {DateTime.Now:dd/MM/yyyy HH:mm:ss}", Status = 1, OutData = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") };
                    }
                    await Task.Delay(500);
                }
            }
            else
            {
                return new ApiResponseData<string>() { Data = $"Gửi lệnh thành công", Status = 1, OutData = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") };
            }
            return new ApiResponseData<string>() { Data = textOut, Status = 1 };
        }

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private string InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return filePath;
        }
    }
}
