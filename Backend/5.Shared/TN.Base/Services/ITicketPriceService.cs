using Microsoft.Extensions.Logging;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface ITicketPriceService : IService<TicketPrice>
    {
        Task<ApiResponseData<List<TicketPrice>>> SearchPageAsync(int pageIndex, int PageSize, int key, string orderby, string ordertype);
        Task<ApiResponseData<TicketPrice>> GetById(int id);
        Task<ApiResponseData<TicketPrice>> Create(TicketPriceCommand model);
        Task<ApiResponseData<TicketPrice>> Edit(TicketPriceCommand model);
        Task<ApiResponseData<TicketPrice>> Delete(int id);

    }
    public class TicketPriceService : ITicketPriceService
    {
        private readonly ILogger _logger;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly IProjectAccountRepository _iTenantAccountRepository;
        private readonly IProjectRepository _iTenantRepository;
        public TicketPriceService
        (
            ILogger<TicketPriceService> logger,
            ITicketPriceRepository iTicketPriceRepository,
            IProjectAccountRepository iTenantAccountRepository,
            IProjectRepository iTenantRepository
        )
        {
            _logger = logger;
            _iTicketPriceRepository = iTicketPriceRepository;
            _iTenantAccountRepository = iTenantAccountRepository;
            _iTenantRepository = iTenantRepository;
        }

        public async Task<ApiResponseData<List<TicketPrice>>> SearchPageAsync(int pageIndex, int pageSize, int key, string sortby, string sorttype)
        {
            try
            {
                var tenantAccounts = await _iTenantAccountRepository.Search(x => x.AccountId == key);
                var listData = new List<TicketPrice>();
                foreach (var ta in tenantAccounts)
                {
                    var tenant = await _iTenantRepository.SearchOneAsync(x => x.Id == ta.ProjectId && x.Status == EProjectStatus.Active);

                    var item = await _iTicketPriceRepository.SearchOneAsync(x => x.ProjectId == ta.ProjectId && x.CustomerGroupId == ta.CustomerGroupId);

                    if (item != null && tenant != null)
                    {
                        listData.Add(item);
                    }
                }
                
                ApiResponseData<List<TicketPrice>> dataRes = new ApiResponseData<List<TicketPrice>>();
                dataRes.PageIndex = pageIndex;
                dataRes.PageSize = pageSize;
                dataRes.Data = listData.OrderByDescending(x => x.Id).ToList();

                return dataRes;
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPriceSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<TicketPrice>>();
            }
        }

        public async Task<ApiResponseData<TicketPrice>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<TicketPrice>() { Data = await _iTicketPriceRepository.SearchOneAsync( x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPriceSevice.Get", ex.ToString());
                return new ApiResponseData<TicketPrice>();
            }
        }

        public async Task<ApiResponseData<TicketPrice>> Create(TicketPriceCommand model)
        {
            try
            {
                var dlAdd = new TicketPrice
                {
                    Id = model.Id,
                    ProjectId = model.ProjectId,
                    CustomerGroupId = model.CustomerGroupId,
                    TicketType = model.TicketType,
                    AllowChargeInSubAccount = model.AllowChargeInSubAccount,
                    TicketValue = model.TicketValue,
                    BlockPerMinute = model.BlockPerMinute,
                    BlockPerViolation = model.BlockPerViolation,
                    BlockViolationValue = model.BlockViolationValue,
                    LimitMinutes = model.LimitMinutes,
                    IsDefault = model.IsDefault,
                    IsActive = model.IsActive,
                    CreateDate = model.CreateDate,
                    UpdateDate = model.UpdateDate,
                    Name = model.Name,
                    Note = model.Note
                };
                await _iTicketPriceRepository.AddAsync(dlAdd);
                await _iTicketPriceRepository.Commit();

                return new ApiResponseData<TicketPrice>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPriceSevice.Create", ex.ToString());
                return new ApiResponseData<TicketPrice>();
            }
        }

        public async Task<ApiResponseData<TicketPrice>> Edit(TicketPriceCommand model)
        {
            try
            {
                var dl = await _iTicketPriceRepository.SearchOneAsync( x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<TicketPrice>() { Data = null, Status = 0 };
                }

                dl.ProjectId = model.ProjectId;
                dl.CustomerGroupId = model.CustomerGroupId;
                dl.TicketType = model.TicketType;
                dl.AllowChargeInSubAccount = model.AllowChargeInSubAccount;
                dl.TicketValue = model.TicketValue;
                dl.BlockPerMinute = model.BlockPerMinute;
                dl.BlockPerViolation = model.BlockPerViolation;
                dl.BlockViolationValue = model.BlockViolationValue;
                dl.LimitMinutes = model.LimitMinutes;
                dl.IsDefault = model.IsDefault;
                dl.IsActive = model.IsActive;
                dl.CreateDate = model.CreateDate;
                dl.UpdateDate = model.UpdateDate;
                dl.Name = model.Name;

                _iTicketPriceRepository.Update(dl);
                await _iTicketPriceRepository.Commit();
                return new ApiResponseData<TicketPrice>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPriceSevice.Edit", ex.ToString());
                return new ApiResponseData<TicketPrice>();
            }
        }
        public async Task<ApiResponseData<TicketPrice>> Delete(int id)
        {
            try
            {
                var dl = await _iTicketPriceRepository.SearchOneAsync( x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<TicketPrice>() { Data = null, Status = 0 };
                }

                _iTicketPriceRepository.Delete(dl);
                await _iTicketPriceRepository.Commit();
                return new ApiResponseData<TicketPrice>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPriceSevice.Delete", ex.ToString());
                return new ApiResponseData<TicketPrice>();
            }
        }

        private Func<IQueryable<TicketPrice>, IOrderedQueryable<TicketPrice>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<TicketPrice>, IOrderedQueryable<TicketPrice>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "tenantId":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.ProjectId));
                    break;
                case "customerGroupId":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.CustomerGroupId)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.CustomerGroupId));
                    break;
                case "ticketType":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.TicketType)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.TicketType));
                    break;
                case "allowChargeInSubAccount":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.AllowChargeInSubAccount)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.AllowChargeInSubAccount));
                    break;
                case "ticketValue":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.TicketValue)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.TicketValue));
                    break;
                case "blockPerMinute":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.BlockPerMinute)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.BlockPerMinute));
                    break;
                case "blockPerViolation":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.BlockPerViolation)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.BlockPerViolation));
                    break;
                case "blockViolationValue":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.BlockViolationValue)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.BlockViolationValue));
                    break;
                case "limitMinutes":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.LimitMinutes)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.LimitMinutes));
                    break;
                case "isDefault":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.IsDefault)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.IsDefault));
                    break;
                case "isActive":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.IsActive)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.IsActive));
                    break;
                case "createDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.CreateDate)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.CreateDate));
                    break;
                case "updateDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.UpdateDate)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.UpdateDate));
                    break;
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<TicketPrice>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;

                default:
                    functionOrder = EntityExtention<TicketPrice>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }
    }
}
