﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PT.Domain.Model;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;

namespace PT.Base.Services
{
    public interface IInitDataService : IService<object>
    {
        Task<ApiResponseData<object>> PullDataAsync(string v);
        Task<ApiResponseData<object>> CurrentVersion();
    }

    public class InitDataService : IInitDataService
    {
        private readonly ILogger _logger;
        private readonly IRoleRepository _iRoleRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly IStationRepository _iStationRepository;
        private readonly IRoleControllerRepository _iRoleControllerRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly ICityRepository _iCityRepository;
        private readonly IDistrictRepository _iDistrictRepository;
        private readonly IModelRepository _iModelRepository;
        private readonly IProducerRepository _iProducerRepository;
        private readonly IWarehouseRepository _iWarehouseRepository;
        private readonly IColorRepository _iColorRepository;
        private readonly ILanguageRepository _iLanguageRepository;
        private readonly IRoleGroupRepository _iRoleGroupRepository;
        private readonly IRoleAreaRepository _iRoleAreaRepository;
        private readonly BaseSettings _baseSettings;
        private readonly IRolesService _iRolesService;
        private readonly IAppVersionRepository _iAppVersionRepository;
        public InitDataService
        (
            ILogger<RolesService> logger,
            IRoleRepository iRoleRepository,
            IUserRepository iUserRepository,
            IParameterRepository iParameterRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IStationRepository iStationRepository,
            IRoleControllerRepository iRoleControllerRepository,
            IProjectRepository iProjectRepository,
            IInvestorRepository iInvestorRepository,
            ICityRepository iCityRepository,
            IDistrictRepository iDistrictRepository,
            IModelRepository iModelRepository,
            IProducerRepository iProducerRepository,
            IWarehouseRepository iWarehouseRepository,
            IColorRepository iColorRepository,
            ILanguageRepository iLanguageRepository,
            IRoleGroupRepository iRoleGroupRepository,
            IRoleAreaRepository iRoleAreaRepository,
            IOptions<BaseSettings> baseSettings,
            IRolesService iRolesService,
            IAppVersionRepository iAppVersionRepository
        )
        {
            _logger = logger;
            _iRoleRepository = iRoleRepository;
            _iUserRepository = iUserRepository;
            _iParameterRepository = iParameterRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iInvestorRepository = iInvestorRepository;
            _iStationRepository = iStationRepository;
            _iRoleControllerRepository = iRoleControllerRepository;
            _iProjectRepository = iProjectRepository;
            _iCityRepository = iCityRepository;
            _iDistrictRepository = iDistrictRepository;
            _iModelRepository = iModelRepository;
            _iProducerRepository = iProducerRepository;
            _iWarehouseRepository = iWarehouseRepository;
            _iColorRepository = iColorRepository;
            _iLanguageRepository = iLanguageRepository;
            _iRoleGroupRepository = iRoleGroupRepository;
            _iRoleAreaRepository = iRoleAreaRepository;
            _baseSettings = baseSettings.Value;
            _iRolesService = iRolesService;
            _iAppVersionRepository = iAppVersionRepository;
        }

        public async Task<ApiResponseData<object>> CurrentVersion()
        {
            var data = await _iAppVersionRepository.SearchOneAsync(x=> x.Id == 2);
            return new ApiResponseData<object>() { Data = data, Status = 1 };
        }

        public async Task<ApiResponseData<object>> PullDataAsync(string v)
        {
            
            try
            {
                var categoryVersion = await _iParameterRepository.SearchOneAsync(x => x.Type == Parameter.ParameterType.Category);
                
                if (categoryVersion != null && categoryVersion.Value == v)
                {
                    return new ApiResponseData<object> { Status = 2, Data = null };
                }

                var roles = await _iRoleRepository.Search();
                var customerGroups = await _iCustomerGroupRepository.Search();
                var projects = await _iProjectRepository.Search();
                var stations = await _iStationRepository.Search();
                var roleControllers = await _iRoleControllerRepository.Search();
                var investors = await _iInvestorRepository.Search();
                var cities = await _iCityRepository.Search();
                var districts = await _iDistrictRepository.Search();

                var models = await _iModelRepository.Search();
                var producers = await _iProducerRepository.Search();
                var warehouses = await _iWarehouseRepository.Search();
                var colors = await _iColorRepository.Search();
                var languages = await _iLanguageRepository.Search();
                var roleGroups = (await _iRoleGroupRepository.Search()).OrderBy(x=>x.Order);
                var roleAres = (await _iRoleAreaRepository.Search()).OrderBy(x => x.Order);
                var adminMenus = await _iRolesService.GetMenuAsync();

                var roleTypes = new List<object>
                {
                    new {Value = (int) RoleManagerType.Admin, Text = RoleManagerType.Admin.GetDisplayName()},
                    new {Value = (int) RoleManagerType.Accounting_TN, Text = RoleManagerType.Accounting_TN.GetDisplayName()},
                    new {Value = (int) RoleManagerType.BOD_VTS, Text = RoleManagerType.BOD_VTS.GetDisplayName()},
                    new {Value = (int) RoleManagerType.KTT_VTS, Text = RoleManagerType.KTT_VTS.GetDisplayName()},
                    new {Value = (int) RoleManagerType.CSKH_VTS, Text = RoleManagerType.CSKH_VTS.GetDisplayName()},
                    new {Value = (int) RoleManagerType.CTV_VTS, Text = RoleManagerType.CTV_VTS.GetDisplayName()},
                    new {Value = (int) RoleManagerType.Coordinator_VTS, Text = RoleManagerType.Coordinator_VTS.GetDisplayName()},
                    new {Value = (int) RoleManagerType.NVCU_TN, Text = RoleManagerType.NVCU_TN.GetDisplayName()},
                    new {Value = (int) RoleManagerType.ThuKho_VTS, Text = RoleManagerType.ThuKho_VTS.GetDisplayName()},
                    new {Value = (int) RoleManagerType.Investor, Text = RoleManagerType.Investor.GetDisplayName()},
                };

                return new ApiResponseData<object>
                {
                    Data = new
                    {
                        roles,
                        roleTypes,
                        categoryVersion = categoryVersion?.Value,
                        customerGroups,
                        projects,
                        stations,
                        roleControllers,
                        investors,
                        cities,
                        districts,
                        models,
                        producers,
                        warehouses,
                        colors,
                        languages,
                        roleGroups,
                        roleAres,
                        imagesMaxSize = _baseSettings.ImagesMaxSize,
                        imagesType = _baseSettings.ImagesType,
                        adminMenus
                    },
                    Status =  1
                };
            }
            catch (Exception ex)
            {
                _logger.LogError("RolesSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<object>();
            }
        }
    }
}