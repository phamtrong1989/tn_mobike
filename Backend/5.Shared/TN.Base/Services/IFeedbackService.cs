using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IFeedbackService : IService<Feedback>
    {
        Task<ApiResponseData<List<Feedback>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<Feedback>> GetById(int id);
        Task<ApiResponseData<Feedback>> Create(FeedbackCommand model);
        Task<ApiResponseData<Feedback>> Edit(FeedbackCommand model);
        Task<ApiResponseData<Feedback>> Delete(int id);

    }
    public class FeedbackService : IFeedbackService
    {
        private readonly ILogger _logger;
        private readonly IFeedbackRepository _iFeedbackRepository;
        public FeedbackService
        (
            ILogger<FeedbackService> logger,
            IFeedbackRepository iFeedbackRepository
        )
        {
            _logger = logger;
            _iFeedbackRepository = iFeedbackRepository;
        }

        public async Task<ApiResponseData<List<Feedback>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                return await _iFeedbackRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    && (x.AccountId == Convert.ToInt32(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Feedback
                    {
                        Id = x.Id,
                        AccountId = x.AccountId,
                        Message = x.Message,
                        Status = x.Status,
                        CreatedDate = x.CreatedDate,
                        Rating = x.Rating,

                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("FeedbackSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Feedback>>();
            }
        }

        public async Task<ApiResponseData<Feedback>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Feedback>() { Data = await _iFeedbackRepository.SearchOneAsync( x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("FeedbackSevice.Get", ex.ToString());
                return new ApiResponseData<Feedback>();
            }
        }

        public async Task<ApiResponseData<Feedback>> Create(FeedbackCommand model)
        {
            try
            {
                var dlAdd = new Feedback
                {
                    Id = model.Id,
                    AccountId = model.AccountId,
                    Message = model.Message,
                    Status = model.Status,
                    CreatedDate = model.CreatedDate,
                };
                await _iFeedbackRepository.AddAsync(dlAdd);
                await _iFeedbackRepository.Commit();

                return new ApiResponseData<Feedback>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("FeedbackSevice.Create", ex.ToString());
                return new ApiResponseData<Feedback>();
            }
        }

        public async Task<ApiResponseData<Feedback>> Edit(FeedbackCommand model)
        {
            try
            {
                var dl = await _iFeedbackRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Feedback>() { Data = null, Status = 0 };
                }

                dl.Id = model.Id;
                dl.AccountId = model.AccountId;
                dl.Message = model.Message;
                dl.Status = (Feedback.EFeedbackStatus) model.Status;
                dl.CreatedDate = model.CreatedDate;

                _iFeedbackRepository.Update(dl);
                await _iFeedbackRepository.Commit();
                return new ApiResponseData<Feedback>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("FeedbackSevice.Edit", ex.ToString());
                return new ApiResponseData<Feedback>();
            }
        }
        public async Task<ApiResponseData<Feedback>> Delete(int id)
        {
            try
            {
                var dl = await _iFeedbackRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Feedback>() { Data = null, Status = 0 };
                }

                _iFeedbackRepository.Delete(dl);
                await _iFeedbackRepository.Commit();
                return new ApiResponseData<Feedback>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("FeedbackSevice.Delete", ex.ToString());
                return new ApiResponseData<Feedback>();
            }
        }

        private Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<Feedback>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "accountId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Feedback>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.AccountId));
                    break;
                case "message":
                    functionOrder = sorttype == "asc" ? EntityExtention<Feedback>.OrderBy(m => m.OrderBy(x => x.Message)) : EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.Message));
                    break;
                case "statusId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Feedback>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.Status));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Feedback>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "rating":
                    functionOrder = sorttype == "asc" ? EntityExtention<Feedback>.OrderBy(m => m.OrderBy(x => x.Rating)) : EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.Rating));
                    break;

                default:
                    functionOrder = EntityExtention<Feedback>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }
    }
}
