﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PT.Base;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IColorService : IService<Color>
    {
        Task<ApiResponseData<List<Color>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<Color>> GetById(int id);
        Task<ApiResponseData<Color>> Create(ColorCommand model);
        Task<ApiResponseData<Color>> Edit(ColorCommand model);
        Task<ApiResponseData<Color>> Delete(int id);

    }
    public class ColorService : IColorService
    {
        private readonly ILogger _logger;
        private readonly IColorRepository _iColorRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly string controllerName = "";

        public ColorService
        (
            ILogger<ColorService> logger,
            IColorRepository iColorRepository,
            IUserRepository iUserRepository,
            IParameterRepository iParameterRepository,
            ILogRepository iLogRepository,
            IHubContext<CategoryHub> hubContext

        )
        {
            _logger = logger;
            _iColorRepository = iColorRepository;
            _iUserRepository = iUserRepository;
            _iParameterRepository = iParameterRepository;
            _iLogRepository = iLogRepository;
            _hubContext = hubContext;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<Color>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                var data = await _iColorRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    && (key == null || x.Name.Contains(key) || x.Code.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Color
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Code = x.Code,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,

                    });

                var idUsers = data.Data.Select(x => x.CreatedUser).ToList();
                idUsers.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(idUsers);
                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ColorSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Color>>();
            }
        }

        public async Task<ApiResponseData<Color>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Color>() { Data = await _iColorRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ColorSevice.Get", ex.ToString());
                return new ApiResponseData<Color>();
            }
        }

        public async Task<ApiResponseData<Color>> Create(ColorCommand model)
        {
            try
            {
                var dlAdd = new Color
                {
                    Id = model.Id,
                    Name = model.Name,
                    Code = model.Code,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    UpdatedDate = DateTime.Now,
                    UpdatedUser = UserInfo.UserId

                };
                await _iColorRepository.AddAsync(dlAdd);
                await _iColorRepository.Commit();

                await InitCategoryAsync("Colors");
                await AddLog(dlAdd.Id, $"Thêm mới mã mầu '{dlAdd.Name}'", LogType.Insert);
                return new ApiResponseData<Color>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ColorSevice.Create", ex.ToString());
                return new ApiResponseData<Color>();
            }
        }

        public async Task<ApiResponseData<Color>> Edit(ColorCommand model)
        {
            try
            {
                var dl = await _iColorRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Color>() { Data = null, Status = 0 };
                }

                dl.Name = model.Name;
                dl.Code = model.Code;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iColorRepository.Update(dl);
                await _iColorRepository.Commit();

                await InitCategoryAsync("Colors");
                await AddLog(dl.Id, $"Thêm mới mã mầu '{dl.Name}'", LogType.Insert);

                return new ApiResponseData<Color>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ColorSevice.Edit", ex.ToString());
                return new ApiResponseData<Color>();
            }
        }
        public async Task<ApiResponseData<Color>> Delete(int id)
        {
            try
            {
                var dl = await _iColorRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Color>() { Data = null, Status = 0 };
                }

                _iColorRepository.Delete(dl);
                await _iColorRepository.Commit();

                await InitCategoryAsync("Colors");
                await AddLog(dl.Id, $"Thêm mới mã mầu '{dl.Name}'", LogType.Insert);

                return new ApiResponseData<Color>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ColorSevice.Delete", ex.ToString());
                return new ApiResponseData<Color>();
            }
        }

        private Func<IQueryable<Color>, IOrderedQueryable<Color>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Color>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Color>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "name" => sorttype == "asc" ? EntityExtention<Color>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<Color>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "code" => sorttype == "asc" ? EntityExtention<Color>.OrderBy(m => m.OrderBy(x => x.Code)) : EntityExtention<Color>.OrderBy(m => m.OrderByDescending(x => x.Code)),
                "createdDate" => sorttype == "asc" ? EntityExtention<Color>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Color>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<Color>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<Color>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<Color>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Color>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                "updatedUser" => sorttype == "asc" ? EntityExtention<Color>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<Color>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
                _ => EntityExtention<Color>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        private async Task InitCategoryAsync(string tableName)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName);
            }
            catch { }
        }
    }
}
