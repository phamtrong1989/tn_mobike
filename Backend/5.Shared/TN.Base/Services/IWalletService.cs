using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IWalletService : IService<Wallet>
    {
        Task<ApiResponseData<List<Wallet>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<Wallet>> GetById(int id);
        Task<ApiResponseData<Wallet>> Create(WalletCommand model);
        Task<ApiResponseData<Wallet>> Edit(WalletCommand model);
        Task<ApiResponseData<Wallet>> Delete(int id);

    }
    public class WalletService : IWalletService
    {
        private readonly ILogger _logger;
        private readonly IWalletRepository _iWalletRepository;
        public WalletService
        (
            ILogger<WalletService> logger,
            IWalletRepository iWalletRepository
        )
        {
            _logger = logger;
            _iWalletRepository = iWalletRepository;
        }

        public async Task<ApiResponseData<List<Wallet>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                return await _iWalletRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    //&& (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Wallet
                    {
                        Id = x.Id,
                        AccountId = x.AccountId,
                        Balance = x.Balance,
                        SubBalance = x.SubBalance,
                        HashCode = x.HashCode,
                        Status = x.Status,
                        CreateDate = x.CreateDate,
                        UpdateDate = x.UpdateDate,

                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Wallet>>();
            }
        }

        public async Task<ApiResponseData<Wallet>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Wallet>() { Data = await _iWalletRepository.SearchOneAsync( x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletSevice.Get", ex.ToString());
                return new ApiResponseData<Wallet>();
            }
        }

        public async Task<ApiResponseData<Wallet>> Create(WalletCommand model)
        {
            try
            {
                var dlAdd = new Wallet
                {
                    Id = model.Id,
                    AccountId = model.AccountId,
                    Balance = model.Balance,
                    SubBalance = model.SubBalance,
                    HashCode = model.HashCode,
                    Status = model.Status,
                    CreateDate = model.CreateDate,
                    UpdateDate = model.UpdateDate,

                };
                await _iWalletRepository.AddAsync(dlAdd);
                await _iWalletRepository.Commit();

                return new ApiResponseData<Wallet>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletSevice.Create", ex.ToString());
                return new ApiResponseData<Wallet>();
            }
        }

        public async Task<ApiResponseData<Wallet>> Edit(WalletCommand model)
        {
            try
            {
                var dl = await _iWalletRepository.SearchOneAsync( x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Wallet>() { Data = null, Status = 0 };
                }

                dl.Id = model.Id;
                dl.AccountId = model.AccountId;
                dl.Balance = model.Balance;
                dl.SubBalance = model.SubBalance;
                dl.HashCode = model.HashCode;
                dl.Status = model.Status;
                dl.CreateDate = model.CreateDate;
                dl.UpdateDate = model.UpdateDate;


                _iWalletRepository.Update(dl);
                await _iWalletRepository.Commit();
                return new ApiResponseData<Wallet>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletSevice.Edit", ex.ToString());
                return new ApiResponseData<Wallet>();
            }
        }
        public async Task<ApiResponseData<Wallet>> Delete(int id)
        {
            try
            {
                var dl = await _iWalletRepository.SearchOneAsync( x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Wallet>() { Data = null, Status = 0 };
                }

                _iWalletRepository.Delete(dl);
                await _iWalletRepository.Commit();
                return new ApiResponseData<Wallet>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletSevice.Delete", ex.ToString());
                return new ApiResponseData<Wallet>();
            }
        }

        private Func<IQueryable<Wallet>, IOrderedQueryable<Wallet>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<Wallet>, IOrderedQueryable<Wallet>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<Wallet>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "accountId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Wallet>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.AccountId));
                    break;
                case "balance":
                    functionOrder = sorttype == "asc" ? EntityExtention<Wallet>.OrderBy(m => m.OrderBy(x => x.Balance)) : EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.Balance));
                    break;
                case "subBalance":
                    functionOrder = sorttype == "asc" ? EntityExtention<Wallet>.OrderBy(m => m.OrderBy(x => x.SubBalance)) : EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.SubBalance));
                    break;
                case "hashCode":
                    functionOrder = sorttype == "asc" ? EntityExtention<Wallet>.OrderBy(m => m.OrderBy(x => x.HashCode)) : EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.HashCode));
                    break;
                case "status":
                    functionOrder = sorttype == "asc" ? EntityExtention<Wallet>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.Status));
                    break;
                case "createDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Wallet>.OrderBy(m => m.OrderBy(x => x.CreateDate)) : EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.CreateDate));
                    break;
                case "updateDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Wallet>.OrderBy(m => m.OrderBy(x => x.UpdateDate)) : EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.UpdateDate));
                    break;

                default:
                    functionOrder = EntityExtention<Wallet>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }
    }
}
