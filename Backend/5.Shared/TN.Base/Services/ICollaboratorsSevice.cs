﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base.Command;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace PT.Base.Services
{
    public interface ICollaboratorsSevice : IService<ApplicationUser>
    {
        Task<ApiResponseData<ApplicationUser>> Create(ManagerRegisterCommand use);

        Task<ApiResponseData<ApplicationUser>> GetById(int id);

        Task<ApiResponseData<List<ApplicationUser>>> SearchPagedAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? roleId,
            bool? isLock,
            bool? isRetired,
            string code,
            string sortby,
            string sorttype
            );
        Task<ApiResponseData<ApplicationUser>> Edit(ManagerRegisterCommand use);
        Task<ApiResponseData<ApplicationUser>> Delete(int id);
        Task<ApiResponseData<FileDataDTO>> UploadImage(HttpRequest request);
        Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key);
        Task<FileContentResult> RevenueReport(DateTime? from, DateTime? to, string type);
    }
    public class CollaboratorsSevice : ICollaboratorsSevice
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        private readonly ILogger _logger;
        private readonly IUserRepository _aspNetUsers;
        private readonly IRoleRepository _iRoleRepository;
        private readonly BaseSettings _baseSettings;
        private readonly ILogRepository _iLogRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        public CollaboratorsSevice
        (
            ILogger<UsersService> logger,
            UserManager<ApplicationUser> userManager,
            IUserRepository aspNetUsers,
            IRoleRepository iRoleRepository,
            IOptions<BaseSettings> baseSettings,
            ILogRepository iLogRepository,
            IWebHostEnvironment iWebHostEnvironment,
            IAccountRepository iAccountRepository,
            IWalletTransactionRepository iWalletTransactionRepository
        )
        {
            _logger = logger;
            _userManager = userManager;
            _aspNetUsers = aspNetUsers;
            _iRoleRepository = iRoleRepository;
            this._baseSettings = baseSettings.Value;
            _iLogRepository = iLogRepository;
            _iWebHostEnvironment = iWebHostEnvironment;
            _iAccountRepository = iAccountRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
        }

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchPagedAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? roleId,
            bool? isLock,
            bool? isRetired,
            string code,
            string sortby,
            string sorttype
            )
        {
            try
            {
                var data = await _aspNetUsers.SearchPagedAsync(
                 pageIndex,
                 pageSize,
                 roleId,
                 m =>
                 (m.UserName.Contains(key) || key == null || m.DisplayName.Contains(key) || m.PhoneNumber.Contains(key) || m.FullName.Contains(key) || m.RFID.Contains(key) || m.IdentificationID.Contains(key))
                 && (m.IsLock == isLock || isLock == null)
                 && (m.IsRetired == isRetired || isRetired == null)
                 && (m.Code.Contains(code) || code == null)
                 && m.IsSuperAdmin == false
                 && m.RoleType == RoleManagerType.CTV_VTS
                 && m.ManagerUserId == UserInfo.UserId
                 ,
                 OrderByExtention(sortby, sorttype));

                var users = await _aspNetUsers.SeachByIds(data.Data.Select(x => x.CreatedUserId ?? 0).ToList(), data.Data.Select(x => x.UpdatedUserId ?? 0).ToList(), data.Data.Where(x => x.ManagerUserId > 0).Select(x => x.ManagerUserId).ToList());

                foreach (var item in data.Data)
                {
                    item.ManagerUser = users.FirstOrDefault(x => x.Id == item.ManagerUserId);
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUserId);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUserId);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }

        public async Task<ApiResponseData<ApplicationUser>> GetById(int id)
        {
            try
            {
                var data = await _aspNetUsers.SearchOneAsync(x => x.Id == id);
                if (data == null)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 0 };
                }

                if (data.RoleType != RoleManagerType.CTV_VTS && data.ManagerUserId == UserInfo.UserId)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 0 };
                }

                var roles = await _iRoleRepository.RolesByUser(id);
                data.IsSuperAdmin = false;
                data.IsReLogin = false;
                data.LockoutEnabled = false;
                data.EmailConfirmed = false;
                data.NormalizedEmail = null;
                data.PhoneNumberConfirmed = false;
                data.Roles = roles;
                var manager = await _aspNetUsers.SearchOneAsync(x => x.Id == data.ManagerUserId);
                if (manager != null)
                {
                    data.ManagerUser = new ApplicationUser
                    {
                        Id = manager.Id,
                        FullName = manager.FullName,
                        UserName = manager.UserName,
                        Sex = manager.Sex,
                        PhoneNumber = manager.PhoneNumber
                    };
                }
                return new ApiResponseData<ApplicationUser>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.Get", ex.ToString());
                return new ApiResponseData<ApplicationUser>();
            }
        }

        public async Task<ApiResponseData<ApplicationUser>> Create(ManagerRegisterCommand use)
        {
            try
            {
                string defaultPassword = "Matkhau@68";
                var ktUsername = await _userManager.FindByNameAsync(use.Username);
                if (ktUsername != null)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 2, Message = "Tài khoản này đã có người sử dụng, vui lòng thử lại" };
                }

                var ktEmail = await _userManager.FindByEmailAsync(use.Email);
                if (ktEmail != null)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 3, Message = "Email này đã có người sử dụng, vui lòng thử lại" };
                }

                var role = await _iRoleRepository.SearchOneAsync(x => x.Type == RoleManagerType.CTV_VTS);

                var user = new ApplicationUser
                {
                    UserName = use.Username,
                    Email = use.Email,
                    EmailConfirmed = true,
                    DisplayName = use.DisplayName,
                    PhoneNumber = use.PhoneNumber,
                    Note = use.Note,
                    IsLock = use.IsLock,
                    CreatedDate = DateTime.Now,
                    CreatedUserId = UserInfo.UserId,
                    Address = use.Address,
                    Birthday = use.Birthday,
                    FullName = use.FullName,
                    IdentificationAdress = use.IdentificationAdress,
                    IdentificationID = use.IdentificationID,
                    IdentificationPhotoBackside = use.IdentificationPhotoBackside,
                    IdentificationPhotoFront = use.IdentificationPhotoFront,
                    IdentificationSupplyAdress = use.IdentificationSupplyAdress,
                    IdentificationSupplyDate = use.IdentificationSupplyDate,
                    IsRetired = use.IsRetired,
                    IsReLogin = false,
                    IsSuperAdmin = false,
                    RoleId = role?.Id ?? 0,
                    RoleType = RoleManagerType.CTV_VTS,
                    Sex = use.Sex,
                    WorkingDateFrom = use.WorkingDateFrom,
                    WorkingDateTo = use.WorkingDateTo,
                    ManagerUserId = UserInfo.UserId,
                    Education = use.Education
                };

                if (use.IsChangePassword)
                {
                    defaultPassword = use.Password;
                }

                var result = await _userManager.CreateAsync(user, defaultPassword);
                if (result.Succeeded)
                {
                    user.Code = user.RoleType == RoleManagerType.CTV_VTS ? $"CTVTN{user.Id:D4}" : $"NVTN{user.Id:D4}";
                    _aspNetUsers.Update(user);
                    await _aspNetUsers.Commit();

                    var listRole = new List<int>();
                    // Cập nhật quyền tài khoản
                    if (user.RoleId > 0)
                    {
                        listRole = new List<int>() { user.RoleId };
                    }
                    await _aspNetUsers.UpdateRolesAsync(user.Id, listRole);
                    return new ApiResponseData<ApplicationUser>() { Status = 1, Message = "Tạo mới tài khoản thành công" };
                }

                await AddLog(ktEmail.Id, $"Thêm mới tài khoản $'{user.UserName}'", LogType.Insert);

                return new ApiResponseData<ApplicationUser>() { Status = 0 };
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.Create", ex.ToString());
                return new ApiResponseData<ApplicationUser>();
            }
        }

        public async Task<ApiResponseData<ApplicationUser>> Edit(ManagerRegisterCommand use)
        {
            try
            {
                var dl = await _aspNetUsers.SearchOneAsync(x => x.Id == use.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ApplicationUser>() { Data = null, Status = 0 };
                }

                if (dl.RoleType != RoleManagerType.CTV_VTS && dl.ManagerUserId == UserInfo.UserId)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 0 };
                }

                var ktEmail = await _aspNetUsers.AnyAsync(x => x.Id != x.Id && x.Email == use.Email);
                if (ktEmail)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 3, Message = "Email này đã có người sử dụng, vui lòng thử lại" };
                }


                var role = await _iRoleRepository.SearchOneAsync(x => x.Type == RoleManagerType.CTV_VTS);

                dl.Email = use.Email;
                dl.DisplayName = use.DisplayName;
                dl.PhoneNumber = use.PhoneNumber;
                dl.Note = use.Note;
                dl.IsLock = use.IsLock;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUserId = UserInfo.UserId;
                dl.Address = use.Address;
                dl.Birthday = use.Birthday;
                dl.FullName = use.FullName;
                dl.IdentificationAdress = use.IdentificationAdress;
                dl.IdentificationID = use.IdentificationID;
                dl.IdentificationPhotoBackside = use.IdentificationPhotoBackside;
                dl.IdentificationPhotoFront = use.IdentificationPhotoFront;
                dl.IdentificationSupplyAdress = use.IdentificationSupplyAdress;
                dl.IdentificationSupplyDate = use.IdentificationSupplyDate;
                dl.IsRetired = use.IsRetired;
                dl.RoleId = role?.Id ?? 0;
                dl.Sex = use.Sex;
                dl.WorkingDateFrom = use.WorkingDateFrom;
                dl.WorkingDateTo = use.WorkingDateTo;
                dl.Education = use.Education;

                dl.Code = dl.RoleType == RoleManagerType.CTV_VTS ? $"CTVTN{dl.Id:D4}" : $"NVTN{dl.Id:D4}";

                _aspNetUsers.Update(dl);
                await _aspNetUsers.Commit();

                if (use.IsChangePassword)
                {
                    await _userManager.RemovePasswordAsync(dl);
                    await _userManager.AddPasswordAsync(dl, use.Password);
                }

                var listRole = new List<int>();
                if (dl.RoleId > 0)
                {
                    listRole = new List<int>() { dl.RoleId };
                }
                await _aspNetUsers.UpdateRolesAsync(dl.Id, listRole);

                await AddLog(dl.Id, $"Cập nhật tài khoản $'{dl.UserName}'", LogType.Update);

                return new ApiResponseData<ApplicationUser>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.Edit", ex.ToString());
                return new ApiResponseData<ApplicationUser>();
            }
        }

        public async Task<ApiResponseData<ApplicationUser>> Delete(int id)
        {
            try
            {
                var dl = await _aspNetUsers.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ApplicationUser>() { Data = null, Status = 0 };
                }
                _aspNetUsers.DeleteRoles(id);
                _aspNetUsers.Delete(dl);
                await _aspNetUsers.Commit();

                await AddLog(dl.Id, $"Xóa tài khoản $'{dl.UserName}'", LogType.Delete);

                return new ApiResponseData<ApplicationUser>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.Delete", ex.ToString());
                return new ApiResponseData<ApplicationUser>();
            }
        }

        private Func<IQueryable<ApplicationUser>, IOrderedQueryable<ApplicationUser>> OrderByExtention(string orderby, string ordertype)
        {
            return orderby switch
            {
                "displayName" => ordertype == "asc" ? EntityExtention<ApplicationUser>.OrderBy(m => m.OrderBy(x => x.DisplayName)) : EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.DisplayName)),
                "userName" => ordertype == "asc" ? EntityExtention<ApplicationUser>.OrderBy(m => m.OrderBy(x => x.UserName)) : EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.UserName)),
                "email" => ordertype == "asc" ? EntityExtention<ApplicationUser>.OrderBy(m => m.OrderBy(x => x.Email)) : EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.Email)),
                "phoneNumber" => ordertype == "asc" ? EntityExtention<ApplicationUser>.OrderBy(m => m.OrderBy(x => x.PhoneNumber)) : EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.PhoneNumber)),
                _ => EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _aspNetUsers.SearchTop(10, x => (key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.PhoneNumber.Contains(key) || x.Email.Contains(key) || x.UserName.Contains(key)), null, x => new ApplicationUser
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    UserName = x.UserName,
                    Sex = x.Sex,
                    Birthday = x.Birthday
                });
                return new ApiResponseData<List<ApplicationUser>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }

        #region [Upload file]
        public async Task<ApiResponseData<FileDataDTO>> UploadImage(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/User/{DateTime.Now.Year}/{DateTime.Now.Month:D2}/{DateTime.Now.Day:D2}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<FileDataDTO> { Data = new FileDataDTO(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<FileDataDTO> { Data = new FileDataDTO(), Status = 3 };
                    }
                }

                var outData = new FileDataDTO();

                foreach (var file in files)
                {
                    var newFilename = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";

                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}".Replace("-", "");
                    pathServer = $"{pathServer}/{newFilename}".Replace("-", "");

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);

                    outData = new FileDataDTO
                    {
                        CreatedDate = DateTime.Now,
                        CreatedUser = UserInfo.UserId,
                        FileName = Path.GetFileName(file.FileName),
                        Path = pathServer,
                        FileSize = file.Length,
                        PathView = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{pathServer}"
                    };
                }
                return new ApiResponseData<FileDataDTO>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<FileDataDTO>();
            }
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new TN.Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        #region [RevenueReport]
        public async Task<FileContentResult> RevenueReport(DateTime? from, DateTime? to, string type)
        {
            var filePath = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(filePath)))
            {
                MyExcel.Workbook.Worksheets.Add("Doanh thu");
                ExcelWorksheet myWorksheet = MyExcel.Workbook.Worksheets[0];

                // Check role user call
                int? currentUserId = UserInfo.UserId; ;

                if (UserInfo.RoleType == RoleManagerType.Admin || UserInfo.RoleType == RoleManagerType.BOD_VTS)
                {
                    currentUserId = null;
                }

                // Query data
                List<ApplicationUser> nvs = new List<ApplicationUser>();
                List<ApplicationUser> ctvs = new List<ApplicationUser>();

                if (type == "all") // Show Tất cả nhân viên VTS và CTV trực thuộc nhân viên đó 
                {
                    nvs = await _aspNetUsers.Search(
                        x => (x.RoleType == RoleManagerType.CSKH_VTS || x.RoleType == RoleManagerType.Coordinator_VTS)
                            && (currentUserId == null || x.Id == currentUserId)
                            && !x.IsRetired
                            && !x.IsLock
                        ,
                        null,
                        x => new ApplicationUser
                        {
                            Id = x.Id,
                            Code = x.Code,
                            UserName = x.UserName,
                            FullName = x.FullName
                        });

                    ctvs = await _aspNetUsers.Search(
                        x => x.RoleType == RoleManagerType.CTV_VTS
                            && (currentUserId == null || x.ManagerUserId == currentUserId)
                            && !x.IsRetired
                            && !x.IsLock
                        ,
                        null,
                        x => new ApplicationUser
                        {
                            Id = x.Id,
                            Code = x.Code,
                            UserName = x.UserName,
                            FullName = x.FullName,
                            ManagerUserId = x.ManagerUserId
                        });
                }
                else if (type == "nv_cskh" || type == "nv_dpbd") // Chỉ nhân viên VTS - Không có CTV
                {
                    var roleType = type == "nv_cskh" ? RoleManagerType.CSKH_VTS : RoleManagerType.Coordinator_VTS;

                    nvs = await _aspNetUsers.Search(
                        x => (x.RoleType == roleType)
                            && (currentUserId == null || x.Id == currentUserId)
                            && !x.IsRetired
                            && !x.IsLock
                        ,
                        null,
                        x => new ApplicationUser
                        {
                            Id = x.Id,
                            Code = x.Code,
                            UserName = x.UserName,
                            FullName = x.FullName
                        });
                }
                else // Chỉ CTV
                {
                    ctvs = await _aspNetUsers.Search(
                        x => x.RoleType == RoleManagerType.CTV_VTS
                            && (currentUserId == null || x.ManagerUserId == currentUserId)
                            && !x.IsRetired
                            && !x.IsLock
                        ,
                        null,
                        x => new ApplicationUser
                        {
                            Id = x.Id,
                            Code = x.Code,
                            UserName = x.UserName,
                            FullName = x.FullName,
                            ManagerUserId = x.ManagerUserId
                        });
                }

                var accounts = await _iAccountRepository.Search(
                    x => ctvs.Select(y => y.Id).Contains(x.SubManagerUserId)
                        || nvs.Select(y => y.Id).Contains(x.ManagerUserId),
                    null,
                    x => new Account
                    {
                        Id = x.Id,
                        ManagerUserId = x.ManagerUserId,
                        SubManagerUserId = x.SubManagerUserId
                    });

                var revenueAccount = await _iWalletTransactionRepository.RevenueGroupByAccount(accounts.Select(x => x.Id), from, to);

                if (type == "ctv")
                {
                    foreach (var ctv in ctvs)
                    {
                        var ctvAccIds = accounts.Where(x => x.SubManagerUserId == ctv.Id).Select(x => x.Id).ToList();
                        ctv.RevenueSelf = revenueAccount.Where(x => ctvAccIds.Contains(x.Item1)).Sum(x => x.Item2);
                    }
                    //
                    FormatExcelRevenueCTV(myWorksheet, ctvs, from, to);
                }
                else
                {
                    foreach (var nv in nvs)
                    {
                        var nvAccIds = accounts.Where(x => x.ManagerUserId == nv.Id && x.SubManagerUserId == 0).Select(x => x.Id).ToList();
                        nv.RevenueSelf = revenueAccount.Where(x => nvAccIds.Contains(x.Item1)).Sum(x => x.Item2);
                        nv.Revenue = nv.RevenueSelf;

                        nv.CTVs = ctvs.Where(x => x.ManagerUserId == nv.Id);

                        foreach (var ctv in nv.CTVs)
                        {
                            var ctvAccIds = accounts.Where(x => x.SubManagerUserId == ctv.Id).Select(x => x.Id).ToList();
                            ctv.RevenueSelf = revenueAccount.Where(x => ctvAccIds.Contains(x.Item1)).Sum(x => x.Item2);
                            nv.Revenue += ctv.RevenueSelf;
                        }
                    }
                    //
                    FormatExcelRevenueNV(myWorksheet, nvs, from, to);
                }

                MyExcel.Save();
            }
            //
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = Path.GetFileName(filePath)
            };
        }

        private void FormatExcelRevenueNV(ExcelWorksheet worksheet, List<ApplicationUser> nvs, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 30}, {4 , 15}, {5 , 15},
                {6 , 15}, {7 , 15}, {8 , 15}, {9 , 15}, {10, 15},
                {11, 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 4, 5, 6, 7, 8, 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 3 });

            // Title
            worksheet.Cells["B2:E2"].Merge = true;
            worksheet.Cells["B2:E2"].Style.Font.Bold = true;
            worksheet.Cells["B2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B2:E2"].Value = $"BÁO CÁO DOANH THU NẠP TIỀN NHÂN VIÊN/CTV VTS";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:E3"].Merge = true;
            worksheet.Cells["B3:E3"].Style.Font.Italic = true;
            worksheet.Cells["B3:E3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:E3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:K5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã";
            worksheet.Cells["C5"].Value = "Họ tên";
            worksheet.Cells["D5"].Value = "Doanh thu";
            worksheet.Cells["E5"].Value = "Giờ làm";
            worksheet.Cells["F5"].Value = "Hiệu suất";
            worksheet.Cells["G5"].Value = "Đơn giá";
            worksheet.Cells["H5"].Value = "Tổng lương";
            worksheet.Cells["I5"].Value = "Tổng thưởng";
            worksheet.Cells["J5"].Value = "Tổng thu nhập";
            worksheet.Cells["K5"].Value = "Đánh giá";

            // Binding data 
            var rs = 6; // row start loop data table
            var sttNV = 1;
            var sttCTV = 1;
            foreach (var nv in nvs)
            {
                worksheet.Cells[rs, 1].Value = sttNV;
                worksheet.Cells[rs, 2].Value = nv.Code;
                worksheet.Cells[rs, 3].Value = nv.FullName;
                worksheet.Cells[rs, 4].Value = nv.Revenue;
                worksheet.Cells[rs, 5].Value = "";
                worksheet.Cells[rs, 6].Value = "";
                worksheet.Cells[rs, 7].Value = "";
                worksheet.Cells[rs, 8].Value = "";
                worksheet.Cells[rs, 9].Value = "";
                worksheet.Cells[rs, 10].Formula = $"=SUM(J{rs + 1}:J{rs + nv.CTVs.Count()})";
                worksheet.Cells[rs, 11].Value = "";
                worksheet.Cells[$"A{rs}:K{rs}"].Style.Font.Bold = true;
                rs++;
                //
                worksheet.Cells[rs, 1].Value = sttNV + "." + sttCTV;
                worksheet.Cells[rs, 2].Value = nv.Code;
                worksheet.Cells[rs, 3].Value = nv.FullName;
                worksheet.Cells[rs, 4].Value = nv.RevenueSelf;
                worksheet.Cells[rs, 5].Value = 1;
                worksheet.Cells[rs, 6].Formula = $"=D{rs}/E{rs}";
                worksheet.Cells[rs, 7].Value = 40000;
                worksheet.Cells[rs, 8].Formula = $"=E{rs}*G{rs}";
                worksheet.Cells[rs, 9].Formula = $"=D{rs}*5%";
                worksheet.Cells[rs, 10].Formula = $"=H{rs}+I{rs}";
                worksheet.Cells[rs, 11].Formula = $"=IF(F{rs}>=2*G{rs},\"Khá\",IF(F{rs}>=G{rs},\"Đạt\",\"Không Đạt\"))";
                rs++;
                sttCTV++;
                //
                foreach (var ctv in nv.CTVs)
                {
                    worksheet.Cells[rs, 1].Value = sttNV + "." + sttCTV;
                    worksheet.Cells[rs, 2].Value = ctv.Code;
                    worksheet.Cells[rs, 3].Value = ctv.FullName;
                    worksheet.Cells[rs, 4].Value = ctv.RevenueSelf;
                    worksheet.Cells[rs, 5].Value = 1;
                    worksheet.Cells[rs, 6].Formula = $"=D{rs}/E{rs}";
                    worksheet.Cells[rs, 7].Value = 40000;
                    worksheet.Cells[rs, 8].Formula = $"=E{rs}*G{rs}";
                    worksheet.Cells[rs, 9].Formula = $"=D{rs}*5%";
                    worksheet.Cells[rs, 10].Formula = $"=H{rs}+I{rs}";
                    worksheet.Cells[rs, 11].Formula = $"=IF(F{rs}>=2*G{rs},\"Khá\",IF(F{rs}>=G{rs},\"Đạt\",\"Không Đạt\"))";
                    rs++;
                    sttCTV++;
                }
                //
                sttNV++;
            }
            //
            SetBorderTable(worksheet, $"A5:K{rs - 1}");
            worksheet.Cells[$"A6:K{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        private void FormatExcelRevenueCTV(ExcelWorksheet worksheet, List<ApplicationUser> ctvs, DateTime? from, DateTime? to)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Rows
            worksheet.Row(5).Style.Font.Bold = true;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 15}, {3 , 30}, {4 , 15}, {5 , 15},
                {6 , 15}, {7 , 15}, {8 , 15}, {9 , 15}, {10, 15},
                {11, 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 2 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 4, 5, 6, 7, 8, 9, 10 });
            //SetNumberAsText(worksheet, new int[] { 3 });

            // Title
            worksheet.Cells["B2:E2"].Merge = true;
            worksheet.Cells["B2:E2"].Style.Font.Bold = true;
            worksheet.Cells["B2:E2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B2:E2"].Value = $"BÁO CÁO DOANH THU NẠP TIỀN NHÂN VIÊN/CTV VTS";

            var fromText = from != null ? from?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = to != null ? to?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:E3"].Merge = true;
            worksheet.Cells["B3:E3"].Style.Font.Italic = true;
            worksheet.Cells["B3:E3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:E3"].Value = $"Từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:K5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Mã";
            worksheet.Cells["C5"].Value = "Họ tên";
            worksheet.Cells["D5"].Value = "Doanh thu";
            worksheet.Cells["E5"].Value = "Giờ làm";
            worksheet.Cells["F5"].Value = "Hiệu suất";
            worksheet.Cells["G5"].Value = "Đơn giá";
            worksheet.Cells["H5"].Value = "Tổng lương";
            worksheet.Cells["I5"].Value = "Tổng thưởng";
            worksheet.Cells["J5"].Value = "Tổng thu nhập";
            worksheet.Cells["K5"].Value = "Đánh giá";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;

            foreach (var ctv in ctvs)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = ctv.Code;
                worksheet.Cells[rs, 3].Value = ctv.FullName;
                worksheet.Cells[rs, 4].Value = ctv.RevenueSelf;
                worksheet.Cells[rs, 5].Value = 1;
                worksheet.Cells[rs, 6].Formula = $"=D{rs}/E{rs}";
                worksheet.Cells[rs, 7].Value = 40000;
                worksheet.Cells[rs, 8].Formula = $"=E{rs}*G{rs}";
                worksheet.Cells[rs, 9].Formula = $"=D{rs}*5%";
                worksheet.Cells[rs, 10].Formula = $"=H{rs}+I{rs}";
                worksheet.Cells[rs, 11].Formula = $"=IF(F{rs}>=2*G{rs},\"Khá\",IF(F{rs}>=G{rs},\"Đạt\",\"Không Đạt\"))";
                rs++;
                stt++;
            }
            //
            SetBorderTable(worksheet, $"A5:K{rs - 1}");
            worksheet.Cells[$"A6:K{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private string InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return filePath;
        }
        #endregion
    }
}
