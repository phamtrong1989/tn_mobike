﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface INewsService : IService<News>
    {
        Task<ApiResponseData<List<News>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<News>> GetById(int id);
        Task<ApiResponseData<News>> Create(NewsCommand model);
        Task<ApiResponseData<News>> Edit(NewsCommand model);
        Task<ApiResponseData<News>> Delete(int id);
        Task<object> UploadImage(HttpRequest request);

    }
    public class NewsService : INewsService
    {
        private readonly ILogger _logger;
        private readonly BaseSettings _baseSettings;
        private IWebHostEnvironment _environment;
        private readonly IUserRepository _iUserRepository;
        private readonly INewsRepository _iNewsRepository;
        private readonly ILogRepository _iLogRepository;
        public NewsService
        (
            ILogger<NewsService> logger,
            IOptions<BaseSettings> baseSettings,
            IWebHostEnvironment environment,
            INewsRepository iNewsRepository,
            IUserRepository iUserRepository,
            ILogRepository iLogRepository
        )
        {
            _logger = logger;
            _baseSettings = baseSettings.Value;
            _environment = environment;
            _iNewsRepository = iNewsRepository;
            _iUserRepository = iUserRepository;
            _iLogRepository = iLogRepository;
        }

        public async Task<ApiResponseData<List<News>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                var data = await _iNewsRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new News
                    {
                        Id = x.Id,
                        CampaignId = x.CampaignId,
                        Author = x.Author,
                        Description = x.Description,
                        Content = x.Content,
                        PublicDate = x.PublicDate,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        Type = x.Type,
                        AccountId = x.AccountId,
                        Name = x.Name,
                        LanguageId = x.LanguageId,
                        ProjectId = x.ProjectId
                    });

                // Binding User info
                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("NewsSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<News>>();
            }
        }

        public async Task<ApiResponseData<News>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<News>() { Data = await _iNewsRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("NewsSevice.Get", ex.ToString());
                return new ApiResponseData<News>();
            }
        }

        public async Task<ApiResponseData<News>> Create(NewsCommand model)
        {
            try
            {
                var dlAdd = new News
                {
                    CampaignId = model.CampaignId,
                    Author = model.Author,
                    Description = model.Description,
                    Content = model.Content,
                    PublicDate = model.PublicDate,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    Type = (NewsType)model.Type,
                    AccountId = model.AccountId,
                    Name = model.Name,
                    LanguageId = model.LanguageId,
                    ProjectId = model.ProjectId,

                };
                await _iNewsRepository.AddAsync(dlAdd);
                await _iNewsRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới '{dlAdd.Name}'", LogType.Insert);

                return new ApiResponseData<News>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("NewsSevice.Create", ex.ToString());
                return new ApiResponseData<News>();
            }
        }

        public async Task<ApiResponseData<News>> Edit(NewsCommand model)
        {
            try
            {
                var dl = await _iNewsRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<News>() { Data = null, Status = 0 };
                }

                dl.CampaignId = model.CampaignId;
                dl.Author = model.Author;
                dl.Description = model.Description;
                dl.Content = model.Content;
                dl.PublicDate = model.PublicDate;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.Type = (NewsType)model.Type;
                dl.AccountId = model.AccountId;
                dl.Name = model.Name;
                dl.LanguageId = model.LanguageId;
                dl.ProjectId = model.ProjectId;

                _iNewsRepository.Update(dl);
                await _iNewsRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật '{dl.Name}'", LogType.Update);

                return new ApiResponseData<News>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("NewsSevice.Edit", ex.ToString());
                return new ApiResponseData<News>();
            }
        }
        public async Task<ApiResponseData<News>> Delete(int id)
        {
            try
            {
                var dl = await _iNewsRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<News>() { Data = null, Status = 0 };
                }

                _iNewsRepository.Delete(dl);
                await _iNewsRepository.Commit();

                await AddLog(dl.Id, $"Xóa '{dl.Name}'", LogType.Delete);

                return new ApiResponseData<News>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("NewsSevice.Delete", ex.ToString());
                return new ApiResponseData<News>();
            }
        }

        private static Func<IQueryable<News>, IOrderedQueryable<News>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<News>, IOrderedQueryable<News>> functionOrder = null;
            functionOrder = sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "campaignId" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.CampaignId)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.CampaignId)),
                "author" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.Author)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.Author)),
                "description" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.Description)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.Description)),
                "content" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.Content)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.Content)),
                "publicDate" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.PublicDate)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.PublicDate)),
                "createdDate" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                "updatedUser" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
                "type" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.Type)),
                "accountId" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.AccountId)),
                "name" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "language" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.LanguageId)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.LanguageId)),
                "projectId" => sorttype == "asc" ? EntityExtention<News>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.ProjectId)),
                _ => EntityExtention<News>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
            return functionOrder;
        }

        #region [Upload file]
        public async Task<object> UploadImage(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = Path.Combine(_environment.WebRootPath, "Data", "News", DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString());
                var host = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}";

                if (!Directory.Exists(pathServer))
                {
                    Directory.CreateDirectory(pathServer);
                }

                var file = request.Form.Files[0];

                if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                {
                    return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                }
                else if (_baseSettings.ImagesMaxSize < file.Length)
                {
                    return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                }

                var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfff}_{Path.GetFileName(file.FileName)}";

                var pathFile = Path.Combine(pathServer, newFilename);

                using (var stream = new FileStream(pathFile, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                //
                return new { url = $"{host}/Data/News/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/{newFilename}" };
            }
            catch (Exception ex)
            {
                _logger.LogError("NewsSevice.UploadImage", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}