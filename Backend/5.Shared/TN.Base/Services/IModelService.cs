﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PT.Base;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IModelService : IService<Model>
    {
        Task<ApiResponseData<List<Model>>> SearchPageAsync(int pageIndex, int PageSize, string key, int? producerId, string orderby, string ordertype);
        Task<ApiResponseData<Model>> GetById(int id);
        Task<ApiResponseData<Model>> Create(ModelCommand model);
        Task<ApiResponseData<Model>> Edit(ModelCommand model);
        Task<ApiResponseData<Model>> Delete(int id);

    }
    public class ModelService : IModelService
    {
        private readonly ILogger _logger;
        private readonly IModelRepository _iModelRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IHubContext<CategoryHub> _hubContext;

        public ModelService
        (
            ILogger<ModelService> logger,
            IModelRepository iModelRepository,
            IUserRepository iUserRepository,
            IParameterRepository iParameterRepository,
            ILogRepository iLogRepository,
            IHubContext<CategoryHub> hubContext

        )
        {
            _logger = logger;
            _iModelRepository = iModelRepository;
            _iUserRepository = iUserRepository;
            _iParameterRepository = iParameterRepository;
            _iLogRepository = iLogRepository;
            _hubContext = hubContext;
        }

        public async Task<ApiResponseData<List<Model>>> SearchPageAsync(int pageIndex, int PageSize, string key, int? producerId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iModelRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    && (key == null || x.Name.Contains(key))
                    && (producerId == null || x.ProducerId == producerId)

                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Model
                    {
                        Id = x.Id,
                        ProducerId = x.ProducerId,
                        Name = x.Name,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,

                    });

                var idUsers = data.Data.Select(x => x.CreatedUser).ToList();
                idUsers.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(idUsers);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ModelSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Model>>();
            }
        }

        public async Task<ApiResponseData<Model>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Model>() { Data = await _iModelRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ModelSevice.Get", ex.ToString());
                return new ApiResponseData<Model>();
            }
        }

        public async Task<ApiResponseData<Model>> Create(ModelCommand model)
        {
            try
            {
                var dlAdd = new Model
                {
                    ProducerId = model.ProducerId,
                    Name = model.Name,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    UpdatedDate = DateTime.Now,
                    UpdatedUser = UserInfo.UserId
                };
                await _iModelRepository.AddAsync(dlAdd);
                await _iModelRepository.Commit();

                await InitCategoryAsync("Models");
                await AddLog(dlAdd.Id, $"Thêm mới mã khóa '{dlAdd.Name}'", LogType.Insert);

                return new ApiResponseData<Model>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ModelSevice.Create", ex.ToString());
                return new ApiResponseData<Model>();
            }
        }

        public async Task<ApiResponseData<Model>> Edit(ModelCommand model)
        {
            try
            {
                var dl = await _iModelRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Model>() { Data = null, Status = 0 };
                }

                dl.Id = model.Id;
                dl.ProducerId = model.ProducerId;
                dl.Name = model.Name;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;


                _iModelRepository.Update(dl);
                await _iModelRepository.Commit();

                await InitCategoryAsync("Models");
                await AddLog(dl.Id, $"Thêm mới mã khóa '{dl.Name}'", LogType.Insert);

                return new ApiResponseData<Model>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ModelSevice.Edit", ex.ToString());
                return new ApiResponseData<Model>();
            }
        }
        public async Task<ApiResponseData<Model>> Delete(int id)
        {
            try
            {
                var dl = await _iModelRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Model>() { Data = null, Status = 0 };
                }

                _iModelRepository.Delete(dl);
                await _iModelRepository.Commit();

                await InitCategoryAsync("Models");
                await AddLog(dl.Id, $"Thêm mới mã khóa '{dl.Name}'", LogType.Insert);
                return new ApiResponseData<Model>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ModelSevice.Delete", ex.ToString());
                return new ApiResponseData<Model>();
            }
        }

        private Func<IQueryable<Model>, IOrderedQueryable<Model>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<Model>, IOrderedQueryable<Model>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<Model>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Model>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "producerId":
                    functionOrder = sorttype == "asc" ? EntityExtention<Model>.OrderBy(m => m.OrderBy(x => x.ProducerId)) : EntityExtention<Model>.OrderBy(m => m.OrderByDescending(x => x.ProducerId));
                    break;
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<Model>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<Model>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Model>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Model>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "createdUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<Model>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<Model>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser));
                    break;
                case "updatedDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Model>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Model>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate));
                    break;
                case "updatedUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<Model>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<Model>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser));
                    break;

                default:
                    functionOrder = EntityExtention<Model>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        private async Task InitCategoryAsync(string tableName)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName);
            }
            catch { }
        }
    }
}
