﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;
using static TN.Domain.Model.Bike;

namespace TN.Base.Services
{
    public interface IStationService : IService<Station>
    {
        Task<ApiResponseData<List<Station>>> SearchPageAsync(int pageIndex, int pageSize, string key, int? investorId, int? cityId, int? districtId, ESationStatus? status, int? projectId, string sortby, string sorttype);
        Task<ApiResponseData<Station>> GetById(int id);
        Task<ApiResponseData<Station>> Create(StationCommand model);
        Task<ApiResponseData<Station>> Edit(StationCommand model);
        Task<ApiResponseData<Station>> Delete(int id);
        Task<FileContentResult> ExportReport(
            string key,
            int? investorId,
            int? cityId,
            int? districtId,
            ESationStatus? status,
            int? projectId
        );

        Task<ApiResponseData<StationResource>> StationResourceEdit(StationResourceCommand model);
        Task<ApiResponseData<StationResource>> StationResourceGetById(int stationId, string language);

    }
    public class StationService : IStationService
    {
        private readonly ILogger _logger;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IStationRepository _iStationRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly ILogRepository _iLogRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly IEntityBaseRepository<City> _cityRepository;
        private readonly IEntityBaseRepository<District> _districtRepository;
        private readonly IStationResourceRepository _iStationResourceRepository;
        private readonly ILanguageRepository _iLanguageRepository;
        public StationService
        (
            ILogger<StationService> logger,
            IWebHostEnvironment iWebHostEnvironment,
            IStationRepository iStationRepository,
            IParameterRepository iParameterRepository,
            IHubContext<CategoryHub> hubContext,
            ILogRepository iLogRepository,
            IUserRepository iUserRepository,
            IProjectRepository iProjectRepository,
            IInvestorRepository iInvestorRepository,
            IEntityBaseRepository<City> cityRepository,
            IEntityBaseRepository<District> districtRepository,
            IStationResourceRepository iStationResourceRepository,
            ILanguageRepository iLanguageRepository
        )
        {
            _logger = logger;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iStationRepository = iStationRepository;
            _iParameterRepository = iParameterRepository;
            _hubContext = hubContext;
            _iLogRepository = iLogRepository;
            _iUserRepository = iUserRepository;
            _iProjectRepository = iProjectRepository;
            _iInvestorRepository = iInvestorRepository;
            _cityRepository = cityRepository;
            _districtRepository = districtRepository;
            _iStationResourceRepository = iStationResourceRepository;
            _iLanguageRepository = iLanguageRepository;
        }

        public async Task<ApiResponseData<List<Station>>> SearchPageAsync(int pageIndex, int pageSize, string key, int? investorId, int? cityId, int? districtId, ESationStatus? status, int? projectId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iStationRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x =>
                    (projectId == null || x.ProjectId == projectId)
                    && (investorId == null || x.InvestorId == investorId)
                    && (cityId == null || x.CityId == cityId)
                    && (districtId == null || x.DistrictId == districtId)
                    && (status == null || x.Status == status)
                    && (key == null || x.Name.Contains(key) || x.DisplayName.Contains(key) || x.Address.Contains(key))
                    , OrderByExtention(sortby, sorttype));

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                ids.AddRange(data.Data.Select(x => x.ManagerUserId).ToList());
                ids = ids.Where(x => x > 0).ToList();
                var users = await _iUserRepository.SeachByIds(ids);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.ManagerUserObject = users.FirstOrDefault(x => x.Id == item.ManagerUserId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("StationSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Station>>();
            }
        }

        public async Task<ApiResponseData<Station>> GetById(int id)
        {
            try
            {
                var data = await _iStationRepository.SearchOneAsync(x => x.Id == id);
                if (data != null)
                {
                    var dataUser = await _iUserRepository.SearchOneAsync(x => x.Id == data.ManagerUserId);
                    data.ManagerUserObject = dataUser == null ? null : new ApplicationUser
                    {
                        Id = dataUser.Id,
                        DisplayName = dataUser.DisplayName,
                        UserName = dataUser.UserName,
                        Sex = dataUser.Sex,
                        PhoneNumber = dataUser.PhoneNumber,
                        FullName = dataUser.FullName,
                        Code = dataUser.Code
                    };
                }
                return new ApiResponseData<Station>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("StationSevice.Get", ex.ToString());
                return new ApiResponseData<Station>();
            }
        }

        public async Task<ApiResponseData<Station>> Create(StationCommand model)
        {
            try
            {
                var dlAdd = new Station
                {
                    InvestorId = model.InvestorId,
                    Name = model.Name,
                    Address = model.Address,
                    Lat = model.Lat,
                    Lng = model.Lng,
                    Description = model.Description,
                    Status = model.Status,
                    CreatedDate = DateTime.Now,
                    CityId = model.CityId,
                    DistrictId = model.DistrictId,
                    CreatedUser = UserInfo.UserId,
                    Height = model.Height,
                    ProjectId = model.ProjectId,
                    UpdatedDate = null,
                    UpdatedUser = null,
                    Width = model.Width,
                    Spaces = model.Spaces,
                    DisplayName = model.DisplayName,
                    ManagerUserId = model.ManagerUserId,
                    ReturnDistance = model.ReturnDistance
                };
                await _iStationRepository.AddAsync(dlAdd);
                await _iStationRepository.Commit();
                await InitCategoryAsync();
                await AddLog(dlAdd.Id, $"Thêm mới trạm '{dlAdd.Name}'", LogType.Insert);
                return new ApiResponseData<Station>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("StationSevice.Create", ex.ToString());
                return new ApiResponseData<Station>();
            }
        }

        public async Task<ApiResponseData<Station>> Edit(StationCommand model)
        {
            try
            {
                var dl = await _iStationRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Station>() { Data = null, Status = 0 };
                }

                dl.InvestorId = model.InvestorId;
                dl.Name = model.Name;
                dl.Address = model.Address;
                dl.Lat = model.Lat;
                dl.Lng = model.Lng;
                dl.Description = model.Description;
                dl.Status = model.Status;
                dl.CityId = model.CityId;
                dl.DistrictId = model.DistrictId;
                dl.Height = model.Height;
                dl.ProjectId = model.ProjectId;
                dl.TotalDock = model.ProjectId;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.Width = model.Width;
                dl.Spaces = model.Spaces;
                dl.DisplayName = model.DisplayName;
                dl.ManagerUserId = model.ManagerUserId;
                dl.ReturnDistance = model.ReturnDistance;

                _iStationRepository.Update(dl);
                await _iStationRepository.Commit();
                await InitCategoryAsync();
                await AddLog(dl.Id, $"Cập nhật trạm '{dl.Name}'", LogType.Update);
                return new ApiResponseData<Station>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("StationSevice.Edit", ex.ToString());
                return new ApiResponseData<Station>();
            }
        }

        public async Task<ApiResponseData<Station>> Delete(int id)
        {
            try
            {
                var dl = await _iStationRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Station>() { Data = null, Status = 0 };
                }

                _iStationRepository.Delete(dl);
                await AddLog(dl.Id, $"Xóa trạm '{dl.Name}'", LogType.Delete);
                await _iStationRepository.Commit();
                await InitCategoryAsync();
                return new ApiResponseData<Station>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("StationSevice.Delete", ex.ToString());
                return new ApiResponseData<Station>();
            }
        }

        public async Task<ApiResponseData<StationResource>> StationResourceGetById(int stationId, string language)
        {
            try
            {
                return new ApiResponseData<StationResource>() { Data = await _iStationResourceRepository.SearchOneAsync(x => x.StationId == stationId && x.Language == language), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.StationResourceGetById", ex.ToString());
                return new ApiResponseData<StationResource>();
            }
        }

        public async Task<ApiResponseData<StationResource>> StationResourceEdit(StationResourceCommand model)
        {
            try
            {
                var dl = await _iStationResourceRepository.SearchOneAsync(x => x.StationId == model.StationId && x.Language == model.Language);
                if (dl == null)
                {
                    var lan = await _iLanguageRepository.SearchOneAsync(x => x.Code1 == model.Language);
                    dl = new StationResource
                    {
                        Language = model.Language,
                        Name = model.Name,
                        StationId = model.StationId,
                        LanguageId = lan?.Id ?? 0,
                        Address = model.Address,
                        DisplayName = model.DisplayName
                    };
                    await _iStationResourceRepository.AddAsync(dl);
                    await _iStationResourceRepository.Commit();
                    await AddLog(dl.Id, $"Thêm mới ngôn ngữ '{dl.Language}' trạm '{dl.Name}'", LogType.Update);
                }
                else
                {
                    dl.Name = model.Name;
                    dl.Address = model.Address;
                    dl.DisplayName = model.DisplayName;

                    _iStationResourceRepository.Update(dl);
                    await _iStationResourceRepository.Commit();
                    await AddLog(dl.Id, $"Cập nhật ngôn ngữ '{dl.Language}' trạm '{dl.Name}'", LogType.Update);
                }

                return new ApiResponseData<StationResource>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectService.CustomerGroupResourceEdit", ex.ToString());
                return new ApiResponseData<StationResource>();
            }
        }

        public async Task<FileContentResult> ExportReport(
            string key,
            int? investorId,
            int? cityId,
            int? districtId,
            ESationStatus? status,
            int? projectId
        )
        {
            {
                var stations = await _iStationRepository.Search(
                    x => (cityId == null || x.CityId == cityId)
                        && (status == null || x.Status == status)
                        && (projectId == null || x.ProjectId == projectId)
                        && (investorId == null || x.InvestorId == investorId)
                        && (districtId == null || x.DistrictId == districtId)
                        && (key == null || x.Name.Contains(key) || x.DisplayName.Contains(key) || x.Address.Contains(key))
                    );

                var numBikeInStation = await _iStationRepository.CountAvailableBikeInStation();

                var users = await _iUserRepository.Search(
                    x => stations.Select(y => y.ManagerUserId).Distinct().Contains(x.Id),
                    null,
                    x => new ApplicationUser
                    {
                        Id = x.Id,
                        ManagerUserId = x.ManagerUserId,
                        Code = x.Code,
                        PhoneNumber = x.PhoneNumber
                    });

                var projects = await _iProjectRepository.Search(
                    x => stations.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                    null,
                    x => new Project
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Code = x.Code
                    });

                var investors = await _iInvestorRepository.Search(
                    x => stations.Select(y => y.InvestorId).Distinct().Contains(x.Id),
                    null,
                    x => new Investor
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Code = x.Code
                    });

                var districts = await _districtRepository.Search(
                    x => stations.Select(y => y.DistrictId).Contains(x.Id),
                    null,
                    x => new District
                    {
                        Id = x.Id,
                        Name = x.Name,
                        CityId = x.CityId
                    });

                var cities = await _cityRepository.Search(
                    x => districts.Select(y => y.CityId).Contains(x.Id),
                    null,
                    x => new City
                    {
                        Id = x.Id,
                        Name = x.Name,
                    });

                foreach (var station in stations)
                {
                    station.Project = projects.FirstOrDefault(x => x.Id == station.ProjectId);
                    station.Investor = investors.FirstOrDefault(x => x.Id == station.InvestorId);
                    station.District = districts.FirstOrDefault(x => x.Id == station.DistrictId);
                    station.City = cities.FirstOrDefault(x => x.Id == station.CityId);
                    station.NumElectricBikeAvailable = numBikeInStation.FirstOrDefault(x => x.StationId == station.Id && x.Type == EBikeType.XeDapDien)?.Count;
                    station.NumMechanicBikeAvailable = numBikeInStation.FirstOrDefault(x => x.StationId == station.Id && x.Type == EBikeType.XeDap)?.Count;
                    station.ManagerUserObject = users.FirstOrDefault(x => x.Id == station.ManagerUserId);
                }

                var filePath = InitPath();

                using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(filePath)))
                {
                    MyExcel.Workbook.Worksheets.Add("BC Trạm xe");
                    ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                    FormatExcelStation(workSheet, stations);
                    MyExcel.Save();
                }
                //
                var memory = new MemoryStream();
                using (var stream = new FileStream(filePath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return new FileContentResult(memory.GetBuffer(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = Path.GetFileName(filePath)
                };
            }
        }

        private void FormatExcelStation(ExcelWorksheet worksheet, List<Station> items)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 25}, {3 , 25}, {4 , 20}, {5 , 40},
                {6 , 20}, {7 , 20}, {8 , 40}, {9 , 15}, {10, 15},
                {11, 14}, {12, 14}, {13, 13}, {14, 13}, {15, 13}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 9, 10, 11, 12, 13 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 9, 10 });
            SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;
            worksheet.Column(9).Style.Font.Bold = true;
            worksheet.Column(10).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:D2"].Merge = true;
            worksheet.Cells["B2:D2"].Style.Font.Bold = true;
            worksheet.Cells["B2:D2"].Value = $"BÁO CÁO THÔNG TIN TRẠM XE";

            worksheet.Cells["B3:D3"].Merge = true;
            worksheet.Cells["B3:D3"].Style.Font.Italic = true;
            worksheet.Cells["B3:D3"].Value = $"Thời điểm lập báo cáo {DateTime.Now.ToString("HH:mm")} ngày {DateTime.Now.ToString("dd/MM/yyyy")}";

            // Table head
            worksheet.Cells["A5:O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:O5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:O5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:O5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên trạm";
            worksheet.Cells["C5"].Value = "Thông tin hiển thị";
            worksheet.Cells["D5"].Value = "Tỉnh/TP";
            worksheet.Cells["E5"].Value = "Địa chỉ";
            worksheet.Cells["F5"].Value = "Dự án";
            worksheet.Cells["G5"].Value = "Đơn vị đầu tư";
            worksheet.Cells["H5"].Value = "Thông tin thiết kế";
            worksheet.Cells["I5"].Value = "Số lượng xe đạp điện online tại trạm";
            worksheet.Cells["J5"].Value = "Số lượng xe đạp cơ online tại trạm";
            worksheet.Cells["K5"].Value = "Quản lý trạm";
            worksheet.Cells["L5"].Value = "SĐT";
            worksheet.Cells["M5"].Value = "Kích thước (r x d) (m)";
            worksheet.Cells["N5"].Value = "Khoảng cách trả xe (m)";
            worksheet.Cells["O5"].Value = "Số chỗ đỗ thiết kế";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Name;
                worksheet.Cells[rs, 3].Value = item.DisplayName;
                worksheet.Cells[rs, 4].Value = $"{item.District?.Name} - {item.City?.Name}";
                worksheet.Cells[rs, 5].Value = item.Address;
                worksheet.Cells[rs, 6].Value = item.Project?.Name;
                worksheet.Cells[rs, 7].Value = item.Investor?.Name;
                worksheet.Cells[rs, 8].Value = item.Description;

                worksheet.Cells[rs, 9].Value = item.NumElectricBikeAvailable ?? 0;
                if (item.NumElectricBikeAvailable != null && item.NumElectricBikeAvailable > 0)
                {
                    worksheet.Cells[rs, 9].Style.Font.Color.SetColor(System.Drawing.Color.Green);
                }
                else
                {
                    worksheet.Cells[rs, 9].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                }

                worksheet.Cells[rs, 10].Value = item.NumMechanicBikeAvailable ?? 0;
                if (item.NumMechanicBikeAvailable != null && item.NumMechanicBikeAvailable > 0)
                {
                    worksheet.Cells[rs, 10].Style.Font.Color.SetColor(System.Drawing.Color.Green);
                }
                else
                {
                    worksheet.Cells[rs, 10].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                }

                worksheet.Cells[rs, 11].Value = item.ManagerUserObject?.Code;
                worksheet.Cells[rs, 12].Value = item.ManagerUserObject?.PhoneNumber;
                worksheet.Cells[rs, 13].Value = $"{item.Width} x {item.Height}";
                worksheet.Cells[rs, 14].Value = item.ReturnDistance;
                worksheet.Cells[rs, 15].Value = item.Spaces;
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:O{rs - 1}");
            worksheet.Cells[$"A6:O{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        private Func<IQueryable<Station>, IOrderedQueryable<Station>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Station>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Station>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                _ => EntityExtention<Station>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        private async Task InitCategoryAsync()
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", "Station");
            }
            catch { }
        }
        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private string InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return filePath;
        }
    }
}
