using Microsoft.Extensions.Logging;
using PT.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IAccountRatingService : IService<AccountRating>
    {
        Task<ApiResponseData<List<AccountRating>>> SearchPageAsync(
            int pageIndex,
            int PageSize,
            string key,
            string orderby,
            string ordertype,
            EAccountRatingType? type,
            EAccountRatingGroup? group
        );
        Task<ApiResponseData<AccountRating>> GetById(int id);
        Task<ApiResponseData<AccountRating>> Create(AccountRatingCommand model);
        Task<ApiResponseData<AccountRating>> Edit(AccountRatingCommand model);
        Task<ApiResponseData<AccountRating>> Delete(int id);
    }
    public class AccountRatingService : IAccountRatingService
    {
        private readonly ILogger _logger;
        private readonly IAccountRatingRepository _iAccountRatingRepository;
        public AccountRatingService
        (
            ILogger<AccountRatingService> logger,
            IAccountRatingRepository iAccountRatingRepository
        )
        {
            _logger = logger;
            _iAccountRatingRepository = iAccountRatingRepository;
        }

        public async Task<ApiResponseData<List<AccountRating>>> SearchPageAsync(
            int pageIndex,
            int PageSize,
            string key,
            string sortby,
            string sorttype,
            EAccountRatingType? type,
            EAccountRatingGroup? group
        )
        {
            try
            {
                return await _iAccountRatingRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    && (key == null || x.Name.Contains(key) || x.Reward.Contains(key))
                    && (type == null || x.Type == type)
                    && (group == null || x.Group == group)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new AccountRating
                    {
                        Id = x.Id,
                        Code = x.Code,
                        Name = x.Name,
                        Note = x.Note,
                        Reward = x.Reward,
                        Type = x.Type,
                        Group = x.Group,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        ProjectId = x.ProjectId,
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<AccountRating>>();
            }
        }

        public async Task<ApiResponseData<AccountRating>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<AccountRating>() { Data = await _iAccountRatingRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingSevice.Get", ex.ToString());
                return new ApiResponseData<AccountRating>();
            }
        }

        public async Task<ApiResponseData<AccountRating>> Create(AccountRatingCommand model)
        {
            try
            {
                var dlAdd = new AccountRating
                {
                    Id = model.Id,
                    Code = model.Code,
                    Name = model.Name,
                    Note = model.Note,
                    Reward = model.Reward,
                    Type = model.Type,
                    Group = model.Group,
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    CreatedDate = model.CreatedDate,
                    CreatedUser = model.CreatedUser,
                    UpdatedDate = model.UpdatedDate,
                    UpdatedUser = model.UpdatedUser,
                    ProjectId = model.ProjectId,

                };
                await _iAccountRatingRepository.AddAsync(dlAdd);
                await _iAccountRatingRepository.Commit();

                return new ApiResponseData<AccountRating>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingSevice.Create", ex.ToString());
                return new ApiResponseData<AccountRating>();
            }
        }

        public async Task<ApiResponseData<AccountRating>> Edit(AccountRatingCommand model)
        {
            try
            {
                var dl = await _iAccountRatingRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<AccountRating>() { Data = null, Status = 0 };
                }

                dl.Id = model.Id;
                dl.Code = model.Code;
                dl.Name = model.Name;
                dl.Note = model.Note;
                dl.Reward = model.Reward;
                dl.Type = model.Type;
                dl.Group = model.Group;
                dl.StartDate = model.StartDate;
                dl.EndDate = model.EndDate;

                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.ProjectId = model.ProjectId;

                _iAccountRatingRepository.Update(dl);
                await _iAccountRatingRepository.Commit();
                return new ApiResponseData<AccountRating>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingSevice.Edit", ex.ToString());
                return new ApiResponseData<AccountRating>();
            }
        }
        public async Task<ApiResponseData<AccountRating>> Delete(int id)
        {
            try
            {
                var dl = await _iAccountRatingRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<AccountRating>() { Data = null, Status = 0 };
                }

                _iAccountRatingRepository.Delete(dl);
                await _iAccountRatingRepository.Commit();
                return new ApiResponseData<AccountRating>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountRatingSevice.Delete", ex.ToString());
                return new ApiResponseData<AccountRating>();
            }
        }

        private Func<IQueryable<AccountRating>, IOrderedQueryable<AccountRating>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<AccountRating>, IOrderedQueryable<AccountRating>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "code":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.Code)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.Code));
                    break;
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                case "note":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.Note)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.Note));
                    break;
                case "reward":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.Reward)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.Reward));
                    break;
                case "type":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.Type));
                    break;
                case "group":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.Group)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.Group));
                    break;
                case "startDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.StartDate)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.StartDate));
                    break;
                case "endDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.EndDate)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.EndDate));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "createdUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser));
                    break;
                case "updatedDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate));
                    break;
                case "updatedUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser));
                    break;
                case "projectId":
                    functionOrder = sorttype == "asc" ? EntityExtention<AccountRating>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.ProjectId));
                    break;

                default:
                    functionOrder = EntityExtention<AccountRating>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }
    }
}
