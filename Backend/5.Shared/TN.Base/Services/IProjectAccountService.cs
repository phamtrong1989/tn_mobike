using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Base;

namespace TN.Base.Services
{
    public interface IProjectAccountService : IService<ProjectAccount>
    {
        Task<ApiResponseData<List<ProjectAccount>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<ProjectAccount>> GetById(int id);
        Task<ApiResponseData<ProjectAccount>> Create(ProjectAccountCommand model);
        Task<ApiResponseData<ProjectAccount>> Edit(ProjectAccountCommand model);
        Task<ApiResponseData<ProjectAccount>> Delete(int id);

    }
    public class ProjectAccountService : IProjectAccountService
    {
        private readonly ILogger _logger;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        public ProjectAccountService
        (
            ILogger<ProjectAccountService> logger,
            IProjectAccountRepository iProjectAccountRepository
        )
        {
            _logger = logger;
            _iProjectAccountRepository = iProjectAccountRepository;
        }

        public async Task<ApiResponseData<List<ProjectAccount>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                return await _iProjectAccountRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    //&& (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new ProjectAccount
                    {
                        Id = x.Id,
                        ProjectId = x.ProjectId,
                        AccountId = x.AccountId,
                        CustomerGroupId = x.CustomerGroupId,
                        SourceType = x.SourceType,
                        DateImport = x.DateImport,
                        CreatedDate = x.CreatedDate,
                        UpdatedDate = x.UpdatedDate,
                        InvestorId = x.InvestorId,
                        UpdatedUser = x.UpdatedUser,
                        CreatedUser = x.CreatedUser 
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ProjectAccount>>();
            }
        }

        public async Task<ApiResponseData<ProjectAccount>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<ProjectAccount>() { Data = await _iProjectAccountRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.Get", ex.ToString());
                return new ApiResponseData<ProjectAccount>();
            }
        }

        public async Task<ApiResponseData<ProjectAccount>> Create(ProjectAccountCommand model)
        {
            try
            {
                var dlAdd = new ProjectAccount
                {
                    Id = model.Id,
                    ProjectId = model.ProjectId,
                    AccountId = model.AccountId,
                    CustomerGroupId = model.CustomerGroupId,
                    SourceType = model.SourceType,
                    DateImport = model.DateImport,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };
                await _iProjectAccountRepository.AddAsync(dlAdd);
                await _iProjectAccountRepository.Commit();

                return new ApiResponseData<ProjectAccount>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.Create", ex.ToString());
                return new ApiResponseData<ProjectAccount>();
            }
        }

        public async Task<ApiResponseData<ProjectAccount>> Edit(ProjectAccountCommand model)
        {
            try
            {
                var dl = await _iProjectAccountRepository.SearchOneAsync( x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ProjectAccount>() { Data = null, Status = 0 };
                }

                dl.ProjectId = model.ProjectId;
                dl.AccountId = model.AccountId;
                dl.CustomerGroupId = model.CustomerGroupId;
                dl.SourceType = model.SourceType;
                dl.DateImport = model.DateImport;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;

                _iProjectAccountRepository.Update(dl);
                await _iProjectAccountRepository.Commit();
                return new ApiResponseData<ProjectAccount>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.Edit", ex.ToString());
                return new ApiResponseData<ProjectAccount>();
            }
        }
        public async Task<ApiResponseData<ProjectAccount>> Delete(int id)
        {
            try
            {
                var dl = await _iProjectAccountRepository.SearchOneAsync( x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ProjectAccount>() { Data = null, Status = 0 };
                }

                _iProjectAccountRepository.Delete(dl);
                await _iProjectAccountRepository.Commit();
                return new ApiResponseData<ProjectAccount>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProjectAccountSevice.Delete", ex.ToString());
                return new ApiResponseData<ProjectAccount>();
            }
        }

        private Func<IQueryable<ProjectAccount>, IOrderedQueryable<ProjectAccount>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<ProjectAccount>, IOrderedQueryable<ProjectAccount>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "tenantId":
                    functionOrder = sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.ProjectId));
                    break;
                case "accountId":
                    functionOrder = sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.AccountId));
                    break;
                case "customerGroupId":
                    functionOrder = sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.CustomerGroupId)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.CustomerGroupId));
                    break;
                case "sourceType":
                    functionOrder = sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.SourceType)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.SourceType));
                    break;
                case "dateImport":
                    functionOrder = sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.DateImport)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.DateImport));
                    break;
                case "createDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "updateDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<ProjectAccount>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate));
                    break;

                default:
                    functionOrder = EntityExtention<ProjectAccount>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }
    }
}
