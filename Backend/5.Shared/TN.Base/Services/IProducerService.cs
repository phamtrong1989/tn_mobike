﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PT.Base;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IProducerService : IService<Producer>
    {
        Task<ApiResponseData<List<Producer>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<Producer>> GetById(int id);
        Task<ApiResponseData<Producer>> Create(ProducerCommand model);
        Task<ApiResponseData<Producer>> Edit(ProducerCommand model);
        Task<ApiResponseData<Producer>> Delete(int id);

    }
    public class ProducerService : IProducerService
    {
        private readonly ILogger _logger;
        private readonly IProducerRepository _iProducerRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        public ProducerService
        (
            ILogger<ProducerService> logger,
            IProducerRepository iProducerRepository,
            IUserRepository iUserRepository,
            IParameterRepository iParameterRepository,
            ILogRepository iLogRepository,
            IHubContext<CategoryHub> hubContext


        )
        {
            _logger = logger;
            _iProducerRepository = iProducerRepository;
            _iUserRepository = iUserRepository;
            _iParameterRepository = iParameterRepository;
            _iLogRepository = iLogRepository;
            _hubContext = hubContext;
        }

        public async Task<ApiResponseData<List<Producer>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                var data = await _iProducerRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    && (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Producer
                    {
                        Id = x.Id,
                        Name = x.Name,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser
                    });

                var idUsers = data.Data.Select(x => x.CreatedUser).ToList();
                idUsers.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(idUsers);
                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ProducerSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Producer>>();
            }
        }

        public async Task<ApiResponseData<Producer>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Producer>() { Data = await _iProducerRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProducerSevice.Get", ex.ToString());
                return new ApiResponseData<Producer>();
            }
        }

        public async Task<ApiResponseData<Producer>> Create(ProducerCommand model)
        {
            try
            {
                var dlAdd = new Producer
                {
                    Name = model.Name,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    UpdatedDate = DateTime.Now,
                    UpdatedUser = UserInfo.UserId
                };
                await _iProducerRepository.AddAsync(dlAdd);
                await _iProducerRepository.Commit();
                await InitCategoryAsync("Producers");
                await AddLog(dlAdd.Id, $"Thêm mới nhà sản xuất '{dlAdd.Name}'", LogType.Insert);

                return new ApiResponseData<Producer>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProducerSevice.Create", ex.ToString());
                return new ApiResponseData<Producer>();
            }
        }

        public async Task<ApiResponseData<Producer>> Edit(ProducerCommand model)
        {
            try
            {
                var dl = await _iProducerRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Producer>() { Data = null, Status = 0 };
                }

                dl.Id = model.Id;
                dl.Name = model.Name;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;


                _iProducerRepository.Update(dl);
                await _iProducerRepository.Commit();
                await InitCategoryAsync("Producers");
                await AddLog(dl.Id, $"Thêm mới nhà sản xuất '{dl.Name}'", LogType.Insert);
                return new ApiResponseData<Producer>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProducerSevice.Edit", ex.ToString());
                return new ApiResponseData<Producer>();
            }
        }
        public async Task<ApiResponseData<Producer>> Delete(int id)
        {
            try
            {
                var dl = await _iProducerRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Producer>() { Data = null, Status = 0 };
                }

                _iProducerRepository.Delete(dl);
                await _iProducerRepository.Commit();
                await InitCategoryAsync("Producers");
                await AddLog(dl.Id, $"Thêm mới nhà sản xuất '{dl.Name}'", LogType.Insert);
                return new ApiResponseData<Producer>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ProducerSevice.Delete", ex.ToString());
                return new ApiResponseData<Producer>();
            }
        }

        private Func<IQueryable<Producer>, IOrderedQueryable<Producer>> OrderByExtention(string sortby, string sorttype) => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<Producer>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Producer>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "name" => sorttype == "asc" ? EntityExtention<Producer>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<Producer>.OrderBy(m => m.OrderByDescending(x => x.Name)),
            "createdDate" => sorttype == "asc" ? EntityExtention<Producer>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Producer>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "createdUser" => sorttype == "asc" ? EntityExtention<Producer>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<Producer>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
            "updatedDate" => sorttype == "asc" ? EntityExtention<Producer>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Producer>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            "updatedUser" => sorttype == "asc" ? EntityExtention<Producer>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<Producer>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
            _ => EntityExtention<Producer>.OrderBy(m => m.OrderByDescending(x => x.Id)),
        };

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        private async Task InitCategoryAsync(string tableName)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName);
            }
            catch { }
        }
    }
}
