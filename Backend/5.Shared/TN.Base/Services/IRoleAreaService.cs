using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IRoleAreaService : IService<RoleArea>
    {
        Task<ApiResponseData<List<RoleArea>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<RoleArea>> GetById(string id);
        Task<ApiResponseData<RoleArea>> Create(RoleAreaCommand model);
        Task<ApiResponseData<RoleArea>> Edit(RoleAreaCommand model);
        Task<ApiResponseData<RoleArea>> Delete(string id);

    }
    public class RoleAreaService : IRoleAreaService
    {
        private readonly ILogger _logger;
        private readonly IRoleAreaRepository _iRoleAreaRepository;
        private readonly IRoleControllerRepository _iRoleControllerRepository;

        public RoleAreaService
        (
            ILogger<RoleAreaService> logger,
            IRoleAreaRepository iRoleAreaRepository,
            IRoleControllerRepository iRoleControllerRepository
        )
        {
            _logger = logger;
            _iRoleAreaRepository = iRoleAreaRepository;
            _iRoleControllerRepository = iRoleControllerRepository;
        }

        public async Task<ApiResponseData<List<RoleArea>>> SearchPageAsync(int pageIndex, int pageSize, string key, string sortby, string sorttype)
        {
            try
            {
                return await _iRoleAreaRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (key == null || x.Id.Contains(key) || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new RoleArea
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Order = x.Order,
                        IsShow = x.IsShow
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleAreaSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<RoleArea>>();
            }
        }

        public async Task<ApiResponseData<RoleArea>> GetById(string id)
        {
            try
            {
                return new ApiResponseData<RoleArea>() { Data = await _iRoleAreaRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleAreaSevice.Get", ex.ToString());
                return new ApiResponseData<RoleArea>();
            }
        }

        public async Task<ApiResponseData<RoleArea>> Create(RoleAreaCommand model)
        {
            try
            {
                var dl = await _iRoleAreaRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl != null)
                {
                    return new ApiResponseData<RoleArea>() { Data = null, Status = 0 };
                }

                var kt = await _iRoleAreaRepository.AnyAsync(x => x.Id == model.Id.Trim());
                if(kt)
                {
                    return new ApiResponseData<RoleArea>() { Data = null, Status = 2 };
                }
                
                var dlAdd = new RoleArea
                {
                    Id = model.Id,
                    Name = model.Name,
                    Order = model.Order,
                    IsShow = model.IsShow,
                };
                await _iRoleAreaRepository.AddAsync(dlAdd);
                await _iRoleAreaRepository.Commit();

                return new ApiResponseData<RoleArea>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleAreaSevice.Create", ex.ToString());
                return new ApiResponseData<RoleArea>();
            }
        }

        public async Task<ApiResponseData<RoleArea>> Edit(RoleAreaCommand model)
        {
            try
            {
                var dl = await _iRoleAreaRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<RoleArea>() { Data = null, Status = 0 };
                }

                dl.Name = model.Name;
                dl.Order = model.Order;
                dl.IsShow = model.IsShow;

                _iRoleAreaRepository.Update(dl);
                await _iRoleAreaRepository.Commit();
                return new ApiResponseData<RoleArea>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleAreaSevice.Edit", ex.ToString());
                return new ApiResponseData<RoleArea>();
            }
        }

        public async Task<ApiResponseData<RoleArea>> Delete(string id)
        {
            try
            {
                var dl = await _iRoleAreaRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<RoleArea>() { Data = null, Status = 0 };
                }

                var check = await _iRoleControllerRepository.AnyAsync(x => x.AreaId == dl.Id);
                if (check)
                {
                    return new ApiResponseData<RoleArea>() { Data = null, Status = 2 };
                }

                _iRoleAreaRepository.Delete(dl);
                await _iRoleAreaRepository.Commit();
                return new ApiResponseData<RoleArea>() { Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RoleAreaSevice.Delete", ex.ToString());
                return new ApiResponseData<RoleArea>();
            }
        }

        private Func<IQueryable<RoleArea>, IOrderedQueryable<RoleArea>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<RoleArea>, IOrderedQueryable<RoleArea>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleArea>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<RoleArea>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleArea>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<RoleArea>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                case "order":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleArea>.OrderBy(m => m.OrderBy(x => x.Order)) : EntityExtention<RoleArea>.OrderBy(m => m.OrderByDescending(x => x.Order));
                    break;
                case "isShow":
                    functionOrder = sorttype == "asc" ? EntityExtention<RoleArea>.OrderBy(m => m.OrderBy(x => x.IsShow)) : EntityExtention<RoleArea>.OrderBy(m => m.OrderByDescending(x => x.IsShow));
                    break;

                default:
                    functionOrder = EntityExtention<RoleArea>.OrderBy(m => m.OrderByDescending(x => x.Order));
                    break;
            }
            return functionOrder;
        }
    }
}
