﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IImportBillService : IService<ImportBill>
    {
        Task<ApiResponseData<List<ImportBill>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype,

            EImportBillStatus? status,
            int? warehouseId,
            DateTime? from,
            DateTime? to);
        Task<ApiResponseData<ImportBill>> GetById(int id);
        Task<ApiResponseData<ImportBill>> Create(ImportBillCommand model);
        Task<ApiResponseData<ImportBill>> Approve(ImportBillCommand model);
        Task<ApiResponseData<ImportBill>> Cancel(ImportBillCommand model);
        Task<ApiResponseData<ImportBill>> Denied(ImportBillCommand model);
        Task<ApiResponseData<ImportBill>> Edit(ImportBillCommand model);
        Task<ApiResponseData<ImportBill>> Delete(int id);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);
        FileStream View(string pathData, bool isPrivate);

        #region [Import Bill Item]
        Task<ApiResponseData<List<ImportBillItem>>> ImportBillItemSearchPageAsync(int pageIndex, int PageSize, int importBillId, string orderby, string ordertype);
        Task<ApiResponseData<List<Supplies>>> SearchBySupplies(string key);
        Task<ApiResponseData<ImportBillItem>> ImportBillItemGetById(int id);
        Task<ApiResponseData<ImportBillItem>> ImportBillItemCreate(ImportBillItemCommand model);
        Task<ApiResponseData<ImportBillItem>> ImportBillItemEdit(ImportBillItemCommand model);
        Task<ApiResponseData<ImportBillItem>> ImportBillItemDelete(int id);
        #endregion

    }
    public class ImportBillService : IImportBillService
    {
        private readonly ILogger _logger;
        private readonly BaseSettings _baseSettings;
        private readonly ILogRepository _iLogRepository;

        private readonly IUserRepository _iUserRepository;
        private readonly IImportBillRepository _iImportBillRepository;
        private readonly IEntityBaseRepository<ImportBillItem> _importBillItemRepository;
        private readonly IEntityBaseRepository<Supplies> _suppliesRepository;
        private readonly IEntityBaseRepository<SuppliesWarehouse> _suppliesWarehouseRepository;
        private readonly string controllerName = "";
        public ImportBillService
        (
            ILogger<ImportBillService> logger,
            IOptions<BaseSettings> baseSettings,
            ILogRepository iLogRepository,

            IUserRepository iUserRepository,
            IImportBillRepository iImportBillRepository,
            IEntityBaseRepository<ImportBillItem> importBillItemRepository,
            IEntityBaseRepository<Supplies> suppliesRepository,
            IEntityBaseRepository<SuppliesWarehouse> suppliesWarehouseRepository
        )
        {
            _logger = logger;
            _baseSettings = baseSettings.Value;
            _iLogRepository = iLogRepository;

            _iUserRepository = iUserRepository;
            _iImportBillRepository = iImportBillRepository;
            _importBillItemRepository = importBillItemRepository;
            _suppliesRepository = suppliesRepository;
            _suppliesWarehouseRepository = suppliesWarehouseRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<ImportBill>>> SearchPageAsync(
            int pageIndex,
            int PageSize,
            string key,
            string sortby,
            string sorttype,

            EImportBillStatus? status,
            int? warehouseId,
            DateTime? from,
            DateTime? to)
        {
            try
            {
                var data = await _iImportBillRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => (key == null || x.Code.Contains(key))
                    && (status == null || x.Status == status)
                    && (warehouseId == null || x.WarehouseId == warehouseId)
                    && (from == null || x.DateBill.Date >= from.Value.Date)
                    && (to == null || x.DateBill.Date <= to.Value.Date)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new ImportBill
                    {
                        Id = x.Id,
                        Code = x.Code,
                        DateBill = x.DateBill,
                        Total = x.Total,
                        ReceiverUser = x.ReceiverUser,
                        WarehouseId = x.WarehouseId,
                        Note = x.Note,
                        Files = x.Files,
                        Type = x.Type,
                        Status = x.Status,
                        PCUConfirm = x.PCUConfirm,
                        PCUUser = x.PCUUser,
                        TKVTSConfirm = x.TKVTSConfirm,
                        TKVTSUser = x.TKVTSUser,
                        BODVTSConfirm = x.BODVTSConfirm,
                        BODVTSUser = x.BODVTSUser,
                        CompleteConfirm = x.CompleteConfirm,
                        CompleteUser = x.CompleteUser,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                    });

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                var users = await _iUserRepository.Search(
                    x => ids.Contains(x.Id),
                    null,
                    x => new ApplicationUser
                    {
                        Id = x.Id,
                        DisplayName = x.DisplayName,
                    });

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    var permission = CheckPermission(item);
                    item.ApproveBillPermission = permission.ApproveBillPermission;
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ImportBill>>();
            }
        }

        public async Task<ApiResponseData<ImportBill>> GetById(int id)
        {
            try
            {
                var data = await _iImportBillRepository.SearchOneAsync(x => x.Id == id);
                //
                return new ApiResponseData<ImportBill>() { Data = CheckPermission(data), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.Get", ex.ToString());
                return new ApiResponseData<ImportBill>();
            }
        }

        public async Task<ApiResponseData<ImportBill>> Create(ImportBillCommand model)
        {
            try
            {
                var existCode = await _iImportBillRepository.SearchOneAsync(x => x.Code == model.Code);
                if (existCode != null)
                {
                    return new ApiResponseData<ImportBill>() { Data = null, Status = 2, Message = "Mã phiếu nhập kho đã tồn tại!" };
                }

                var dlAdd = new ImportBill
                {
                    Code = model.Code,
                    DateBill = model.DateBill,
                    Total = 0,
                    ReceiverUser = model.ReceiverUser,
                    WarehouseId = model.WarehouseId,
                    Note = model.Note,
                    Files = model.Files,
                    Type = (EImportBillType)model.Type,
                    Status = EImportBillStatus.Draft,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };
                await _iImportBillRepository.AddAsync(dlAdd);
                await _iImportBillRepository.Commit();

                await AddLog(dlAdd.Id, $"Lập nháp phiếu nhập kho #{dlAdd.Id}", LogType.Insert);
                return new ApiResponseData<ImportBill>() { Data = CheckPermission(dlAdd), Status = 1, Message = "Thủ kho VTS lập phiếu nhập kho vật tư thành công!" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.Create", ex.ToString());
                return new ApiResponseData<ImportBill>();
            }
        }

        public async Task<ApiResponseData<ImportBill>> Edit(ImportBillCommand model)
        {
            try
            {
                var dlEdit = await _iImportBillRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dlEdit == null)
                {
                    return new ApiResponseData<ImportBill>() { Data = null, Status = 0, Message = "Không tìm thấy phiếu nhập kho tương ứng!" };
                }

                var existCode = await _iImportBillRepository.SearchOneAsync(x => x.Code == model.Code && x.Id != model.Id);
                if (existCode != null)
                {
                    return new ApiResponseData<ImportBill>() { Data = null, Status = 2, Message = "Mã phiếu đã tồn tại!" };
                }

                dlEdit.Code = model.Code;
                dlEdit.DateBill = model.DateBill;
                dlEdit.ReceiverUser = model.ReceiverUser;
                dlEdit.WarehouseId = model.WarehouseId;
                dlEdit.Note = model.Note;
                dlEdit.Files = model.Files;
                dlEdit.Type = (EImportBillType)model.Type;
                //
                _iImportBillRepository.Update(dlEdit);
                await _iImportBillRepository.Commit();
                await AddLog(dlEdit.Id, $"Cập nhật thông tin", LogType.Update);

                return new ApiResponseData<ImportBill>() { Data = CheckPermission(dlEdit), Status = 1, Message = "Cập nhật thông tin thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.Edit", ex.ToString());
                return new ApiResponseData<ImportBill>();
            }
        }

        public async Task<ApiResponseData<ImportBill>> Approve(ImportBillCommand model)
        {
            try
            {
                var dl = await _iImportBillRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ImportBill>() { Data = null, Status = 0, Message = "Không tìm thấy phiếu nhập kho tương ứng!" };
                }

                // KT ko có item
                var anyItem = await _importBillItemRepository.AnyAsync(x => x.ImportBillId == dl.Id);
                if (!anyItem)
                {
                    return new ApiResponseData<ImportBill>() { Status = 0, Message = "Phiếu không tồn tại vật tư nào không thể thay đổi trạng thái" };
                }
                //

                switch (model.Status)
                {
                    case EImportBillStatus.Draft:
                        {
                            if (UserInfo.RoleType == RoleManagerType.ThuKho_VTS)
                            {
                                dl.Status = EImportBillStatus.TK_XacNhanPCU;
                                dl.TKVTSConfirm = DateTime.Now;
                                dl.TKVTSUser = UserInfo.UserId;

                                _iImportBillRepository.Update(dl);
                                await _iImportBillRepository.Commit();
                                await AddLog(dl.Id, $"Thủ kho lập phiếu yêu cầu vật tư cho kế toán VTS", LogType.Update);
                                return new ApiResponseData<ImportBill>() { Data = dl, Status = 1, Message = "Gửi phiếu yêu cầu vật tư cho kế toán VTS thành công" };
                            }
                            else
                            {
                                return new ApiResponseData<ImportBill>() { Status = 0, Message = "Chỉ Thủ kho mới được lập phiếu trình kế toán VTS" };
                            }
                        }
                    case EImportBillStatus.TK_XacNhanPCU:
                        {
                            if (UserInfo.RoleType == RoleManagerType.KTT_VTS)
                            {
                                dl.Status = EImportBillStatus.KTTVTS_KiemTraXacNhan;
                                dl.KTTConfirm = DateTime.Now;
                                dl.KTTUser = UserInfo.UserId;

                                _iImportBillRepository.Update(dl);
                                await _iImportBillRepository.Commit();
                                await AddLog(dl.Id, $"Kế toán VTS xác nhận phiếu nhập kho, trình BOD VTS", LogType.Update);
                                return new ApiResponseData<ImportBill>() { Data = dl, Status = 1, Message = "Kế toán VTS xác nhận phiếu nhập kho thành công" };
                            }
                            else
                            {
                                return new ApiResponseData<ImportBill>() { Status = 0, Message = "Chỉ Kế toán VTS mới được xác nhận và gửi BOD" };
                            }
                        }
                    case EImportBillStatus.KTTVTS_KiemTraXacNhan:
                        {
                            if (UserInfo.RoleType == RoleManagerType.BOD_VTS)
                            {
                                dl.Status = EImportBillStatus.BODVTS_KiemTraXacNhan;
                                dl.BODVTSConfirm = DateTime.Now;
                                dl.BODVTSUser = UserInfo.UserId;

                                _iImportBillRepository.Update(dl);
                                await _iImportBillRepository.Commit();
                                await AddLog(dl.Id, $"BOD VTS xác nhận phiếu nhập kho, gửi Thủ kho tiến hành nhập kho", LogType.Update);
                                return new ApiResponseData<ImportBill>() { Data = dl, Status = 1, Message = "BOD VTS xác nhận phiếu nhập kho thành công" };
                            }
                            else
                            {
                                return new ApiResponseData<ImportBill>() { Status = 0, Message = "Chỉ BOD VTS mới được xác nhận và gửi Thủ kho" };
                            }
                        }
                    case EImportBillStatus.BODVTS_KiemTraXacNhan:
                        {
                            if (UserInfo.RoleType == RoleManagerType.ThuKho_VTS)
                            {
                                dl.Status = EImportBillStatus.TKVTS_NhapKho;
                                dl.CompleteConfirm = DateTime.Now;
                                dl.CompleteUser = UserInfo.UserId;

                                _iImportBillRepository.Update(dl);
                                await _iImportBillRepository.Commit();
                                await AddLog(dl.Id, $"Thủ kho xác nhận quá trình nhập kho thành công", LogType.Update);

                                // UPDATE WAREHOUSE
                                if (dl.Status == EImportBillStatus.TKVTS_NhapKho)
                                {
                                    var items = await _importBillItemRepository.Search(x => x.ImportBillId == dl.Id);
                                    foreach (var item in items)
                                    {
                                        var sw = await _suppliesWarehouseRepository.SearchOneAsync(x => x.SuppliesId == item.SuppliesId && x.WarehouseId == dl.WarehouseId);
                                        if (sw == null)
                                        {
                                            var newRecord = new SuppliesWarehouse
                                            {
                                                SuppliesId = item.SuppliesId,
                                                WarehouseId = dl.WarehouseId,
                                                Quantity = item.Count
                                            };
                                            //
                                            await _suppliesWarehouseRepository.AddAsync(newRecord);
                                        }
                                        else
                                        {
                                            sw.Quantity += item.Count;
                                            _suppliesWarehouseRepository.Update(sw);
                                        }
                                    }
                                    //
                                    await _suppliesWarehouseRepository.Commit();
                                }

                                _iImportBillRepository.Update(dl);
                                await _iImportBillRepository.Commit();
                                await AddLog(dl.Id, $"Cập nhật số lượng vật tư trong kho sau khi nhập thành công!", LogType.Update);

                                return new ApiResponseData<ImportBill>() { Data = dl, Status = 1, Message = "Thủ kho xác nhận quá trình nhập kho thành công" };
                            }
                            else
                            {
                                return new ApiResponseData<ImportBill>() { Status = 0, Message = "Chỉ Thủ kho mới được xác nhận quá trình nhập kho thánh công!" };
                            }
                        }
                    default:
                        {
                            dl.Status = EImportBillStatus.Draft;
                            dl.CreatedDate = DateTime.Now;
                            dl.CreatedUser = UserInfo.UserId;
                            //
                            await AddLog(dl.Id, $"Có lỗi xảy ra với hệ thống. Hoàn phiếu về bản nháp", LogType.Update);
                            return new ApiResponseData<ImportBill>() { Data = dl, Status = 0, Message = "Có lỗi xảy ra với hệ thống!" };
                        }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.Approve", ex.ToString());
                return new ApiResponseData<ImportBill>();
            }
        }

        public async Task<ApiResponseData<ImportBill>> Cancel(ImportBillCommand model)
        {
            try
            {
                var dl = await _iImportBillRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ImportBill>() { Data = null, Status = 0, Message = "Không tìm thấy phiếu nhập kho tương ứng!" };
                }

                if (
                    UserInfo.RoleType == RoleManagerType.ThuKho_VTS
                    && (model.Status == EImportBillStatus.Draft || model.Status == EImportBillStatus.TK_XacNhanPCU)
                )
                {
                    dl.Status = EImportBillStatus.Cancel;
                    dl.TKVTSConfirm = DateTime.Now;
                    dl.TKVTSUser = UserInfo.UserId;
                    dl.CompleteConfirm = DateTime.Now;
                    dl.CompleteUser = UserInfo.UserId;

                    _iImportBillRepository.Update(dl);
                    await _iImportBillRepository.Commit();
                    await AddLog(dl.Id, $"Thủ kho #{UserInfo.UserId} hủy phiếu nhập kho", LogType.Update);
                    return new ApiResponseData<ImportBill>() { Data = dl, Status = 1, Message = "Hủy phiếu nhập kho thành công!" };
                }
                else
                {
                    return new ApiResponseData<ImportBill>() { Status = 0, Message = "Chỉ Thủ kho VTS mới được hủy phiếu này" };
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.Cancel", ex.ToString());
                return new ApiResponseData<ImportBill>();
            }
        }

        public async Task<ApiResponseData<ImportBill>> Denied(ImportBillCommand model)
        {
            try
            {
                var dl = await _iImportBillRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ImportBill>() { Data = null, Status = 0, Message = "Không tìm thấy phiếu nhập kho tương ứng!" };
                }

                // Thủ kho từ chối xác nhận nhập kho thành công. Chuyển ngược lại BOD xem xét
                if (model.Status == EImportBillStatus.BODVTS_KiemTraXacNhan)
                {
                    if (UserInfo.RoleType == RoleManagerType.ThuKho_VTS)
                    {
                        dl.Status = EImportBillStatus.KTTVTS_KiemTraXacNhan;
                        dl.TKVTSConfirm = DateTime.Now;
                        dl.TKVTSUser = UserInfo.UserId;

                        _iImportBillRepository.Update(dl);
                        await _iImportBillRepository.Commit();
                        await AddLog(dl.Id, $"Thủ kho từ chối xác nhận nhập kho thành công. Chuyển ngược lại BOD xem xét.", LogType.Update);
                        return new ApiResponseData<ImportBill>() { Data = dl, Status = 1, Message = "Từ chối xác nhận phiếu nhập kho thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<ImportBill>() { Status = 0, Message = "Chỉ Thủ kho VTS mới được từ chối xác nhận kết quả quá trình nhập kho" };
                    }
                }

                // BOD từ chối xác nhận phiếu nhập kho. Chuyển ngược lại Kế toán VTS xem xét
                if (model.Status == EImportBillStatus.KTTVTS_KiemTraXacNhan)
                {
                    if (UserInfo.RoleType == RoleManagerType.BOD_VTS)
                    {
                        dl.Status = EImportBillStatus.TK_XacNhanPCU;
                        dl.BODVTSConfirm = DateTime.Now;
                        dl.BODVTSUser = UserInfo.UserId;

                        _iImportBillRepository.Update(dl);
                        await _iImportBillRepository.Commit();
                        await AddLog(dl.Id, $"BOD từ chối xác nhận phiếu nhập kho. Chuyển ngược lại Kế toán VTS xem xét.", LogType.Update);
                        return new ApiResponseData<ImportBill>() { Data = dl, Status = 1, Message = "Từ chối xác nhận phiếu nhập kho thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<ImportBill>() { Status = 0, Message = "Chỉ BOD mới được từ chối xác nhận phiếu nhập kho tại bước này" };
                    }
                }

                // Kế toán VTS từ chối xác nhận phiếu nhập kho. Chuyển ngược lại Thủ kho VTS xem xét
                if (model.Status == EImportBillStatus.TK_XacNhanPCU)
                {
                    if (UserInfo.RoleType == RoleManagerType.KTT_VTS)
                    {
                        dl.Status = EImportBillStatus.Draft;
                        dl.KTTConfirm = DateTime.Now;
                        dl.KTTUser = UserInfo.UserId;

                        _iImportBillRepository.Update(dl);
                        await _iImportBillRepository.Commit();
                        await AddLog(dl.Id, $"Kế toán VTS từ chối xác nhận phiếu nhập kho. Chuyển ngược lại Thủ kho VTS xem xét.", LogType.Update);
                        return new ApiResponseData<ImportBill>() { Data = dl, Status = 1, Message = "Từ chối xác nhận phiếu nhập kho thành công" };
                    }
                    else
                    {
                        return new ApiResponseData<ImportBill>() { Status = 0, Message = "Chỉ Kế toán VTS mới được từ chối xác nhận phiếu nhập kho tại bước này" };
                    }
                }
                //
                return new ApiResponseData<ImportBill>() { Status = 0, Message = "Bạn không có quyền sử dụng chức năng này!" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.Denied", ex.ToString());
                return new ApiResponseData<ImportBill>();
            }
        }

        public async Task<ApiResponseData<ImportBill>> Delete(int id)
        {
            try
            {
                var dl = await _iImportBillRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ImportBill>() { Data = null, Status = 0 };
                }

                _iImportBillRepository.Delete(dl);
                await _iImportBillRepository.Commit();
                return new ApiResponseData<ImportBill>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.Delete", ex.ToString());
                return new ApiResponseData<ImportBill>();
            }
        }

        private Func<IQueryable<ImportBill>, IOrderedQueryable<ImportBill>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<ImportBill>, IOrderedQueryable<ImportBill>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "code":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.Code)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.Code));
                    break;
                case "dateBill":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.DateBill)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.DateBill));
                    break;
                case "total":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.Total)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.Total));
                    break;
                case "receiverUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.ReceiverUser)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.ReceiverUser));
                    break;
                case "warehouseId":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.WarehouseId)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.WarehouseId));
                    break;
                case "note":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.Note)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.Note));
                    break;
                case "files":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.Files)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.Files));
                    break;
                case "type":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.Type));
                    break;
                case "status":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.Status));
                    break;
                case "pCUConfirm":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.PCUConfirm)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.PCUConfirm));
                    break;
                case "pCUUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.PCUUser)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.PCUUser));
                    break;
                case "tKVTSConfirm":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.TKVTSConfirm)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.TKVTSConfirm));
                    break;
                case "tKVTSUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.TKVTSUser)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.TKVTSUser));
                    break;
                case "bODVTSConfirm":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.BODVTSConfirm)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.BODVTSConfirm));
                    break;
                case "bODVTSUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.BODVTSUser)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.BODVTSUser));
                    break;
                case "completeConfirm":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.CompleteConfirm)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.CompleteConfirm));
                    break;
                case "completeUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.CompleteUser)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.CompleteUser));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "createdUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<ImportBill>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser));
                    break;

                default:
                    functionOrder = EntityExtention<ImportBill>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }

        private ImportBill CheckPermission(ImportBill importBill)
        {
            var userRole = UserInfo.RoleType;

            var cancelBillPermission = false;
            var approveBillPermission = false;
            var deniedBillPermission = false;
            var editItemPermission = false;

            // Check quyền phê duyệt
            if (
                // Thủ kho > Bản nháp
                (userRole == RoleManagerType.ThuKho_VTS && importBill.Status == EImportBillStatus.Draft)
                // Kế toán trưởng > Thủ kho xác nhận
                || (userRole == RoleManagerType.KTT_VTS && importBill.Status == EImportBillStatus.TK_XacNhanPCU)
                // BOD > Kế toán trưởng xác nhận
                || (userRole == RoleManagerType.BOD_VTS && importBill.Status == EImportBillStatus.KTTVTS_KiemTraXacNhan)
                // Thủ kho BOD xác nhận
                || (userRole == RoleManagerType.ThuKho_VTS && importBill.Status == EImportBillStatus.BODVTS_KiemTraXacNhan)
            )
            {
                approveBillPermission = true;
            }

            // Check quyền từ chối phê duyệt
            if (
                // Kế toán trưởng > Thủ kho xác nhận
                (userRole == RoleManagerType.KTT_VTS && importBill.Status == EImportBillStatus.TK_XacNhanPCU)
                // BOD > Kế toán trưởng xác nhận
                || (userRole == RoleManagerType.BOD_VTS && importBill.Status == EImportBillStatus.KTTVTS_KiemTraXacNhan)
                // Thủ kho BOD xác nhận
                || (userRole == RoleManagerType.ThuKho_VTS && importBill.Status == EImportBillStatus.BODVTS_KiemTraXacNhan)
            )
            {
                deniedBillPermission = true;
            }

            // Check quyền hủy phiếu
            if (userRole == RoleManagerType.ThuKho_VTS && importBill.Status == EImportBillStatus.Draft)
            {
                cancelBillPermission = true;
            }

            // Check quyền sửa danh sách,số lượng vật tư
            if ((userRole == RoleManagerType.ThuKho_VTS || userRole == RoleManagerType.KTT_VTS || userRole == RoleManagerType.BOD_VTS)
                && (importBill.Status != EImportBillStatus.Cancel && importBill.Status != EImportBillStatus.TKVTS_NhapKho)
            )
            {
                editItemPermission = true;
            }
            //
            importBill.ApproveBillPermission = approveBillPermission;
            importBill.CancelBillPermission = cancelBillPermission;
            importBill.DeniedBillPermission = deniedBillPermission;
            importBill.EditItemPermission = editItemPermission;
            //
            return importBill;
        }

        #region [Upload file]
        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/ImportBill/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfff}_{Path.GetFileName(file.FileName)}";

                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);

                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.Upload", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }

        public FileStream View(string pathData, bool isPrivate)
        {
            try
            {
                if (isPrivate)
                {
                    string file = $"{_baseSettings.PrivateDataPath}{pathData}";
                    return new FileStream(file, FileMode.Open, FileAccess.Read);
                }
                else
                {
                    return new FileStream(pathData, FileMode.Open, FileAccess.Read);
                }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region [Import Bill Item]
        public async Task<ApiResponseData<List<ImportBillItem>>> ImportBillItemSearchPageAsync(int pageIndex, int PageSize, int importBillId, string sortby, string sorttype)
        {
            try
            {
                var data = await _importBillItemRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.ImportBillId == importBillId,
                    ImportBillItemOrderByExtention(sortby, sorttype),
                    x => new ImportBillItem
                    {
                        Id = x.Id,
                        ImportBillId = x.ImportBillId,
                        SuppliesId = x.SuppliesId,
                        Count = x.Count,
                        Price = x.Price,
                        Total = x.Total,
                        Note = x.Note,
                        Files = x.Files
                    });

                var suppliesIds = data.Data.Select(x => x.SuppliesId).ToList();
                var supplies = await _suppliesRepository.Search(
                    x => suppliesIds.Contains(x.Id),
                    null,
                    x => new Supplies
                    {
                        Id = x.Id,
                        Name = x.Name,
                    });

                foreach (var item in data.Data)
                {
                    item.Supplies = supplies.FirstOrDefault(x => x.Id == item.SuppliesId);
                }
                //
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillItem.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ImportBillItem>>();
            }
        }

        public async Task<ApiResponseData<List<Supplies>>> SearchBySupplies(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _suppliesRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.Name.Contains(key) || x.Code.Contains(key));

                return new ApiResponseData<List<Supplies>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillSevice.SearchBySupplies", ex.ToString());
                return new ApiResponseData<List<Supplies>>();
            }
        }

        public async Task<ApiResponseData<ImportBillItem>> ImportBillItemGetById(int id)
        {
            try
            {
                var data = await _importBillItemRepository.SearchOneAsync(x => x.Id == id);
                data.Supplies = await _suppliesRepository.SearchOneAsync(x => x.Id == data.SuppliesId);
                return new ApiResponseData<ImportBillItem>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillItem.Get", ex.ToString());
                return new ApiResponseData<ImportBillItem>();
            }
        }

        public async Task<ApiResponseData<ImportBillItem>> ImportBillItemCreate(ImportBillItemCommand model)
        {
            try
            {
                if (model.ImportBillId <= 0)
                {
                    return new ApiResponseData<ImportBillItem>() { Data = null, Status = 0, Message = "Không tìm thấy phiếu nhập kho" };
                }

                var importBillId = await _iImportBillRepository.SearchOneAsync(x => x.Id == model.ImportBillId);
                if (importBillId == null)
                {
                    return new ApiResponseData<ImportBillItem>() { Data = null, Status = 0, Message = "Phiếu nhập không tồn tại" };
                }

                var ktTon = await _importBillItemRepository.AnyAsync(x => x.SuppliesId == model.SuppliesId && x.ImportBillId == model.ImportBillId);
                if (ktTon)
                {
                    return new ApiResponseData<ImportBillItem>() { Data = null, Status = 0, Message = "Đã có vật tư tồn tại trong phiếu rồi. Vui lòng chỉnh sửa số lượng của vật tư này." };
                }

                var dlAdd = new ImportBillItem
                {
                    ImportBillId = model.ImportBillId,
                    SuppliesId = model.SuppliesId,
                    Count = model.Count,
                    Price = model.Price,
                    Total = model.Count * model.Price,
                    Note = model.Note,
                    Files = model.Files,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };
                await _importBillItemRepository.AddAsync(dlAdd);
                await _importBillItemRepository.Commit();

                // Update Total price import bill
                await UpdateImportBillWhenUpdateItem(dlAdd.ImportBillId);

                await AddLog(dlAdd.ImportBillId, $"Thêm vật tư #{model.SuppliesId} số lượng {model.Count}", LogType.Update);
                return new ApiResponseData<ImportBillItem>() { Data = dlAdd, Status = 1, Message = "Thêm vật tư thành công!" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillItem.Create", ex.ToString());
                return new ApiResponseData<ImportBillItem>();
            }
        }

        public async Task<ApiResponseData<ImportBillItem>> ImportBillItemEdit(ImportBillItemCommand model)
        {
            try
            {
                var dl = await _importBillItemRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ImportBillItem>() { Data = null, Status = 0 };
                }

                var importBill = await _iImportBillRepository.SearchOneAsync(x => x.Id == dl.ImportBillId);
                if (importBill == null)
                {
                    return new ApiResponseData<ImportBillItem>() { Data = null, Status = 0, Message = "Phiếu nhập kho không tồn tại" };
                }

                // Update item
                var itemEdit = await _importBillItemRepository.SearchOneAsync(x => x.Id == model.Id);
                if (itemEdit == null)
                {
                    return new ApiResponseData<ImportBillItem>() { Data = null, Status = 3, Message = "Không tìm thấy vật tư!" };
                }

                itemEdit.SuppliesId = model.SuppliesId;
                itemEdit.Count = model.Count;
                itemEdit.Price = model.Price;
                itemEdit.Total = model.Count * model.Price;
                itemEdit.Note = model.Note;
                itemEdit.Files = model.Files;
                itemEdit.UpdatedDate = DateTime.Now;
                itemEdit.UpdatedUser = UserInfo.UserId;

                _importBillItemRepository.Update(itemEdit);
                await _importBillItemRepository.Commit();

                // Update Total price import bill
                await UpdateImportBillWhenUpdateItem(itemEdit.ImportBillId);
                await AddLog(itemEdit.ImportBillId, $"Cập nhật vật tư #{model.SuppliesId} số lượng {model.Count}", LogType.Update);

                return new ApiResponseData<ImportBillItem>() { Data = itemEdit, Status = 1, Message = "Cập nhật vật tư thành công!" };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillItem.Edit", ex.ToString());
                return new ApiResponseData<ImportBillItem>();
            }
        }

        public async Task<ApiResponseData<ImportBillItem>> ImportBillItemDelete(int id)
        {
            try
            {
                var dl = await _importBillItemRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ImportBillItem>() { Data = null, Status = 0 };
                }

                _importBillItemRepository.Delete(dl);
                await _importBillItemRepository.Commit();

                await UpdateImportBillWhenUpdateItem(dl.ImportBillId);

                return new ApiResponseData<ImportBillItem>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("ImportBillItem.Delete", ex.ToString());
                return new ApiResponseData<ImportBillItem>();
            }
        }

        private async Task UpdateImportBillWhenUpdateItem(int id)
        {
            var dl = await _iImportBillRepository.SearchOneAsync(x => x.Id == id);
            if (dl == null)
            {
                return;
            }

            var list = await _importBillItemRepository.Search(x => x.ImportBillId == id);
            if (list.Any())
            {
                dl.Total = list.Sum(x => x.Total);
                _iImportBillRepository.Update(dl);
                await _iImportBillRepository.Commit();
            }
        }

        private Func<IQueryable<ImportBillItem>, IOrderedQueryable<ImportBillItem>> ImportBillItemOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<ImportBillItem>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<ImportBillItem>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "suppliesId" => sorttype == "asc" ? EntityExtention<ImportBillItem>.OrderBy(m => m.OrderBy(x => x.SuppliesId)) : EntityExtention<ImportBillItem>.OrderBy(m => m.OrderByDescending(x => x.SuppliesId)),
                "investorId" => sorttype == "asc" ? EntityExtention<ImportBillItem>.OrderBy(m => m.OrderBy(x => x.Count)) : EntityExtention<ImportBillItem>.OrderBy(m => m.OrderByDescending(x => x.Count)),
                "customerGroupId" => sorttype == "asc" ? EntityExtention<ImportBillItem>.OrderBy(m => m.OrderBy(x => x.Price)) : EntityExtention<ImportBillItem>.OrderBy(m => m.OrderByDescending(x => x.Price)),
                "campaignId" => sorttype == "asc" ? EntityExtention<ImportBillItem>.OrderBy(m => m.OrderBy(x => x.Total)) : EntityExtention<ImportBillItem>.OrderBy(m => m.OrderByDescending(x => x.Total)),
                "transactionCode" => sorttype == "asc" ? EntityExtention<ImportBillItem>.OrderBy(m => m.OrderBy(x => x.Note)) : EntityExtention<ImportBillItem>.OrderBy(m => m.OrderByDescending(x => x.Note)),
                _ => EntityExtention<ImportBillItem>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}