﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface IBikeReportService : IService<BikeReport>
    {
        Task<ApiResponseData<List<BikeReport>>> SearchPageAsync(
            int pageIndex,
            int PageSize,
            string key,
            int? accountId,
            int? investorId,
            int? projectId,
            EBikeReportType? type,
            bool? status,
            string from,
            string to,
            string orderby,
            string ordertype);
        Task<ApiResponseData<BikeReport>> GetById(int id);
        Task<ApiResponseData<BikeReport>> Edit(BikeReportCommand model, bool? status);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);
    }
    public class BikeReportService : IBikeReportService
    {
        private readonly ILogger _logger;
        private readonly IBikeRepository _iBikeRepository;
        private readonly IDockRepository _iDockRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IBikeReportRepository _iBikeReportRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly BaseSettings _baseSettings;
        private readonly string controllerName = "";

        public BikeReportService
        (
            ILogger<BikeReportService> logger,
            IBikeRepository iBikeRepository,
            IDockRepository iDockRepository,
            IUserRepository iUserRepository,
            IAccountRepository iAccountRepository,
            IBikeReportRepository iBikeReportRepository,
            IOptions<BaseSettings> baseSettings,
            ILogRepository iLogRepository
        )
        {
            _logger = logger;
            _iBikeRepository = iBikeRepository;
            _iUserRepository = iUserRepository;
            _iDockRepository = iDockRepository;
            _iAccountRepository = iAccountRepository;
            _iBikeReportRepository = iBikeReportRepository;
            _baseSettings = baseSettings.Value;
            _iLogRepository = iLogRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<BikeReport>>> SearchPageAsync(
            int pageIndex,
            int PageSize,
            string key,
            int? accountId,
            int? investorId,
            int? projectId,
            EBikeReportType? type,
            bool? status,
            string from,
            string to,
            string sortby,
            string sorttype)
        {
            try
            {
                var data = await _iBikeReportRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => (accountId == null || x.AccountId == accountId)
                            && (investorId == null || x.InvestorId == investorId)
                            && (projectId == null || x.ProjectId == projectId)
                            && (type == null || x.Type == type)
                            && (status == null || x.Status == status)
                            && (from == null || x.CreatedDate >= Convert.ToDateTime(from))
                            && (to == null || x.CreatedDate < Convert.ToDateTime(to).AddDays(1))
                            && (key == null || x.DockId.ToString().Contains(key) || x.BikeId.ToString().Contains(key) || x.TransactionCode == key)
                    ,
                    OrderByExtention(sortby, sorttype));

                var accounts = await _iAccountRepository.Search(
                    x => data.Data.Select(x => x.AccountId).Contains(x.Id),
                    null,
                    x => new Account() { Id = x.Id, FullName = x.FullName, Phone = x.Phone, Status = x.Status, Type = x.Type });
                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var bikes = await _iBikeRepository.Search(x => data.Data.Select(y => y.BikeId).Contains(x.Id));
                var docks = await _iDockRepository.Search(x => data.Data.Select(y => y.DockId).Contains(x.Id));

                if (accountId != null && accountId > 0)
                {
                    data.OutData = await _iAccountRepository.SearchOneAsync(x => x.Id == accountId);
                }

                foreach (var item in data.Data)
                {
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                    item.Bike = bikes.FirstOrDefault(x => x.Id == item.BikeId);
                    item.Dock = docks.FirstOrDefault(x => x.Id == item.DockId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeReportSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<BikeReport>>();
            }
        }
        public async Task<ApiResponseData<BikeReport>> GetById(int id)
        {
            try
            {
                var data = await _iBikeReportRepository.SearchOneAsync(x => x.Id == id);
                if(data != null)
                {
                    data.Bike = await _iBikeRepository.SearchOneAsync(x => x.Id == data.BikeId);
                    if(data.Bike != null)
                    {
                        data.Bike.Dock = await _iDockRepository.SearchOneAsync(x => x.Id == data.DockId);
                    }    
                }    
                return new ApiResponseData<BikeReport>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeReportSevice.Get", ex.ToString());
                return new ApiResponseData<BikeReport>();
            }
        }
       
        public async Task<ApiResponseData<BikeReport>> Edit(BikeReportCommand model, bool? status)
        {
            try
            {
                var dl = await _iBikeReportRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<BikeReport>() { Data = null, Status = 0 };
                }

                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.Note = model.Note;
                dl.Files = model.Files;
                if(status !=null)
                {
                    dl.Status = status ?? false;
                }    

                _iBikeReportRepository.Update(dl);
                await _iBikeReportRepository.Commit();

                await _iLogRepository.AddLog(UserInfo.UserId, controllerName, dl.Id, $"Cập nhật '{model.Description}' trạng thái {dl.Status}", LogType.Update, $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}");

                return new ApiResponseData<BikeReport>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeReportSevice.Edit", ex.ToString());
                return new ApiResponseData<BikeReport>();
            }
        }
     
        private Func<IQueryable<BikeReport>, IOrderedQueryable<BikeReport>> OrderByExtention(string sortby, string sorttype)
        => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "dockId" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.DockId)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.DockId)),
            "bikeId" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.BikeId)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.BikeId)),
            "type" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.Type)),
            "description" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.Description)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.Description)),
            "createdDate" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "updatedDate" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            "accountId" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.AccountId)),
            "bookingId" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.BookingId)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.BookingId)),
            "transactionCode" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.TransactionCode)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.TransactionCode)),
            "note" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.Note)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.Note)),
            "status" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.Status)),
            "file" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.File)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.File)),
            "investorId" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.InvestorId)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.InvestorId)),
            "projectId" => sorttype == "asc" ? EntityExtention<BikeReport>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.ProjectId)),
            _ => EntityExtention<BikeReport>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
        };

        #region [Upload file]
        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/BikeReport/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3 };
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfffffff}_{Path.GetFileName(file.FileName)}";
                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);
                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                    System.Threading.Thread.Sleep(100);
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("BikeReportSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }
        #endregion
    }
}