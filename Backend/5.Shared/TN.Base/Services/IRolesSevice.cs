﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PT.Base.Command;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Domain.Seedwork;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;


namespace PT.Base.Services
{
    public interface IRolesService : IService<ApplicationRole>
    {
        Task<ApiResponseData<List<ApplicationRole>>> SearchPageAsync(int pageIndex, int pageSize, string key, RoleManagerType? type, string orderby, string ordertype);
        Task<ApiResponseData<ApplicationRole>> GetById(int id, bool importDetails = false);
        Task<ApiResponseData<ApplicationRole>> Create(RoleCommand model);
        Task<ApiResponseData<ApplicationRole>> Edit(RoleCommand model);
        Task<ApiResponseData<ApplicationRole>> Delete(int id);
        Task<List<AdminMenuDTO>> GetMenuAsync();
    }

    public class RolesService : IRolesService
    {
        private readonly ILogger _logger;
        private readonly IRoleRepository _iRoleRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly IParameterRepository _iParameterRepository;
        private readonly IRoleDetailRepository _iAspNetRoleDetailRepository;
        private readonly IRoleGroupRepository _iRoleGroupRepository;
        private readonly IRoleControllerRepository _iRoleControllerRepository;
        private readonly IRoleActionRepository _iRoleActionRepository;
        private readonly ILogRepository _iLogRepository;
        public RolesService
        (
            ILogger<RolesService> logger,
            IRoleRepository iRoleRepository,
            IHubContext<CategoryHub> hubContext,
            IParameterRepository iParameterRepository,
            IRoleDetailRepository iAspNetRoleDetailRepository,
            IRoleGroupRepository iRoleGroupRepository,
            IRoleControllerRepository iRoleControllerRepository,
            IRoleActionRepository iRoleActionRepository,
            ILogRepository iLogRepository
        )
        {
            _logger = logger;
            _iRoleRepository = iRoleRepository;
            _hubContext = hubContext;
            _iParameterRepository = iParameterRepository;
            _iAspNetRoleDetailRepository = iAspNetRoleDetailRepository;
            _iRoleGroupRepository = iRoleGroupRepository;
            _iRoleControllerRepository = iRoleControllerRepository;
            _iRoleActionRepository = iRoleActionRepository;
            _iLogRepository = iLogRepository;
        }

        public async Task<ApiResponseData<List<ApplicationRole>>> SearchPageAsync(int pageIndex, int pageSize, string key, RoleManagerType? type, string orderby, string ordertype)
        {
            try
            {
                return await _iRoleRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (key == null || x.Name.Contains(key)) && (type == null || x.Type == type),
                    string.IsNullOrEmpty(ordertype) ? null : OrderByExtention(orderby, ordertype),
                    x => new ApplicationRole
                    {
                        Id = x.Id,
                        Name = x.Name,
                        ConcurrencyStamp = x.ConcurrencyStamp,
                        Description = x.Description,
                        Type = x.Type,
                        TypeName = x.Type.GetDisplayName()
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError("RolesService.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ApplicationRole>>();
            }
        }

        public async Task<ApiResponseData<ApplicationRole>> GetById(int id, bool importDetails = false)
        {
            try
            {
                var data = await _iRoleRepository.SearchOneAsync(x => x.Id == id);
                if(importDetails)
                {
                    data.RoleDetailObj = await _iRoleRepository.GetRoleDetail(id);
                }    

                return new ApiResponseData<ApplicationRole>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RolesService.Get", ex.ToString());
                return new ApiResponseData<ApplicationRole>();
            }
        }

        public async Task<ApiResponseData<ApplicationRole>> Create(RoleCommand model)
        {
            try
            {
                var dlAdd = new ApplicationRole
                {
                    Name = model.Name,
                    Description = model.Description,
                    Type = model.Type ?? RoleManagerType.Admin
                };
                await _iRoleRepository.AddAsync(dlAdd);
                await _iRoleRepository.Commit();

                await AddLog(dlAdd.Id, $"Thêm mới '{dlAdd.Name}'", LogType.Insert);
                try
                {
                    await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                    await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", "Role");
                }
                catch
                {
                }

                return new ApiResponseData<ApplicationRole>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RolesService.Create", ex.ToString());
                return new ApiResponseData<ApplicationRole>();
            }
        }

        public async Task<ApiResponseData<ApplicationRole>> Edit(RoleCommand model)
        {
            try
            {
                var dl = await _iRoleRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ApplicationRole>() { Data = null, Status = 0 };
                }
                dl.Name = model.Name;
                dl.Description = model.Description;
                dl.Type = model.Type ?? RoleManagerType.Admin;

                _iRoleRepository.Update(dl);
                await _iRoleRepository.Commit();

                await AddLog(dl.Id, $"Cập nhật '{dl.Name}'", LogType.Update);

                //Xóa bỏ quyền cũ
                await _iRoleRepository.UpdateRoleDetail(dl.Id, model.RoleActions.Where(x => x.Checked).Select(x => x.Id).ToList());

                try
                {
                    await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                    await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", "Role");
                }
                catch
                {
                }

                return new ApiResponseData<ApplicationRole>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RolesService.Edit", ex.ToString());
                return new ApiResponseData<ApplicationRole>();
            }
        }
        public async Task<ApiResponseData<ApplicationRole>> Delete(int id)
        {
            try
            {
                var dl = await _iRoleRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ApplicationRole>() { Data = null, Status = 0 };
                }

                var isUser = await _iRoleRepository.IsUse(id);

                if(isUser)
                {
                    return new ApiResponseData<ApplicationRole>() { Data = dl, Status = 2 };
                }    

                _iRoleRepository.Delete(dl);
                _iAspNetRoleDetailRepository.DeleteWhere(x => x.RoleId == id);
                await _iAspNetRoleDetailRepository.Commit();
                await _iRoleRepository.Commit();

                await AddLog(dl.Id, $"Xóa '{dl.Name}'", LogType.Delete);

                try
                {
                    await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                    await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", "Role");
                }
                catch
                {
                }

                return new ApiResponseData<ApplicationRole>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("RolesService.Delete", ex.ToString());
                return new ApiResponseData<ApplicationRole>();
            }
        }

        public async Task<List<AdminMenuDTO>> GetMenuAsync()
        {
            var groups = await _iRoleGroupRepository.Search(x => x.IsShow);
            var controllers = await _iRoleControllerRepository.Search(x => x.IsShow);
            var acctions = await _iRoleActionRepository.Search(x => x.IsShow);
            var outData = new List<AdminMenuDTO>();
            int menuId = 0;
            foreach (var group in groups.OrderBy(x => x.Order))
            {
                var fControllers = controllers.Where(x => x.GroupId == group.Id).OrderBy(x => x.Order).ToList();
                if (!fControllers.Any())
                {
                    continue;
                }

                if (fControllers.Count() == 1)
                {
                    menuId++;
                    var controller = fControllers.FirstOrDefault();
                    var actions = acctions.Where(x => x.ControllerId == controller.Id);
                    outData.Add(new AdminMenuDTO
                    {
                        Id = menuId,
                        ActionId = 0,
                        Href = controller.Href,
                        HasSubMenu = false,
                        IsActive = true,
                        ParentId = 0,
                        Icon = controller.Icon,
                        RouterLink = controller.RouterLink,
                        Title = controller.Name,
                        RoleAcctionIds = string.Join(",", actions.Select(x => x.Id))
                    });
                }
                else
                {
                    menuId++;
                    var cActions = acctions.Where(x => fControllers.Select(m => m.Id).Contains(x.ControllerId)).OrderBy(x => x.Order).ToList();
                    outData.Add(new AdminMenuDTO
                    {
                        Id = menuId,
                        ActionId = 0,
                        HasSubMenu = true,
                        IsActive = true,
                        ParentId = 0,
                        Icon = group.Icon,
                        Title = group.Name,
                        RoleAcctionIds = string.Join(",", cActions.Select(x => x.Id))
                    });

                    int parentId = menuId;
                    foreach (var controller in fControllers)
                    {
                        cActions = acctions.Where(x => x.ControllerId == controller.Id).OrderBy(x=>x.Order).ToList();
                        menuId++;
                        outData.Add(new AdminMenuDTO
                        {
                            Id = menuId,
                            ActionId = 0,
                            Href = controller.Href,
                            Icon = controller.Icon,
                            RouterLink = controller.RouterLink,
                            HasSubMenu = false,
                            IsActive = true,
                            ParentId = parentId,
                            Title = controller.Name,
                            RoleAcctionIds = string.Join(",", cActions.Select(x => x.Id))
                        });
                    }
                }
            }
            //Add role mặc định
            menuId++;
            outData.Add(new AdminMenuDTO
            {
                Id = menuId,
                ActionId = 0,
                HasSubMenu = true,
                IsActive = true,
                ParentId = 0,
                Icon = "dns",
                Title = "Superadmin",
                RoleAcctionIds = "999999"
            });
            var parentId2 = menuId;
            menuId++;
            outData.Add(new AdminMenuDTO
            {
                Id = menuId,
                ActionId = 0,
                HasSubMenu = false,
                IsActive = true,
                ParentId = parentId2,
                Title = "Role Controllers",
                RoleAcctionIds = "999999",
                Href = null,
                RouterLink = "/admin/role-controllers"
            });

            menuId++;
            outData.Add(new AdminMenuDTO
            {
                Id = menuId,
                ActionId = 0,
                HasSubMenu = false,
                IsActive = true,
                ParentId = parentId2,
                Title = "Role Group",
                RoleAcctionIds = "999999",
                Href = null,
                RouterLink = "/admin/role-groups"
            });

            menuId++;
            outData.Add(new AdminMenuDTO
            {
                Id = menuId,
                ActionId = 0,
                HasSubMenu = false,
                IsActive = true,
                ParentId = parentId2,
                Title = "Role Areas",
                RoleAcctionIds = "999999",
                Href = null,
                RouterLink = "/admin/role-areas"
            });

            return outData;
        }

        private Func<IQueryable<ApplicationRole>, IOrderedQueryable<ApplicationRole>> OrderByExtention(string orderby, string ordertype)
        {
            Func<IQueryable<ApplicationRole>, IOrderedQueryable<ApplicationRole>> functionOrder = null;
            switch (orderby)
            {
                case "name":
                    functionOrder = ordertype == "asc" ? EntityExtention<ApplicationRole>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<ApplicationRole>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                case "id":
                    functionOrder = ordertype == "asc" ? EntityExtention<ApplicationRole>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<ApplicationRole>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new TN.Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}
