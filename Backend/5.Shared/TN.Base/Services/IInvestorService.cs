﻿using Microsoft.Extensions.Logging;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Utility;
using TN.Domain.Model.Common;
using PT.Base;
using Microsoft.AspNetCore.SignalR;
using TN.Shared;

namespace TN.Base.Services
{
    public interface IInvestorService : IService<Investor>
    {
        Task<ApiResponseData<List<Investor>>> SearchPageAsync(int pageIndex, int PageSize, string key, EInvestorStatus? status, string orderby, string ordertype);
        Task<ApiResponseData<Investor>> GetById(int id);
        Task<ApiResponseData<Investor>> Create(InvestorCommand model);
        Task<ApiResponseData<Investor>> Edit(InvestorCommand model);
        Task<ApiResponseData<Investor>> Delete(int id);

        #region [Log]
        Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? InvestorId, string sortby, string sorttype);
        #endregion
    }
    public class InvestorService : IInvestorService
    {
        private readonly ILogger _logger;
        private readonly IInvestorRepository _iInvestorRepository;
        private readonly string controllerName = "";

        public InvestorService(ICustomerGroupRepository iCustomerGroupRepository)
        {
        }

        private readonly IParameterRepository _iParameterRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly ILogRepository _iLogRepository;
        private readonly IUserRepository _iUserRepository;
        public InvestorService
        (
            ILogger<InvestorService> logger,
            IInvestorRepository iInvestorRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IHubContext<CategoryHub> hubContext,
            IParameterRepository iParameterRepository,
            ITicketPriceRepository iTicketPriceRepository,
            IAccountRepository iAccountRepository,
            ILogRepository iLogRepository,
            IUserRepository iUserRepository
        )
        {
            _logger = logger;
            _iInvestorRepository = iInvestorRepository;
            _iParameterRepository = iParameterRepository;
            _hubContext = hubContext;
            _iLogRepository = iLogRepository;
            _iUserRepository = iUserRepository;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        #region [Investor]

        public async Task<ApiResponseData<List<Investor>>> SearchPageAsync(int pageIndex, int pageSize, string key, EInvestorStatus? status, string sortby, string sorttype)
        {
            try
            {
                var data = await _iInvestorRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x =>
                     (key == null || x.Name.Contains(key) || x.Description.Contains(key) || x.Code.Contains(key))
                     && (x.Status == status || status == null)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Investor
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        Code = x.Code,
                        Status = x.Status,
                        CreatedDate = x.CreatedDate,
                        UpdatedDate = x.UpdatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedUser = x.UpdatedUser
                    });

                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("InvestorService.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Investor>>();
            }
        }

        public async Task<ApiResponseData<Investor>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Investor>() { Data = await _iInvestorRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("InvestorService.Get", ex.ToString());
                return new ApiResponseData<Investor>();
            }
        }


        public async Task<ApiResponseData<Investor>> Create(InvestorCommand model)
        {
            try
            {
                var dlAdd = new Investor
                {
                    Name = model.Name,
                    Description = model.Description,
                    Code = model.Code,
                    Status = model.Status,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId
                };
                await _iInvestorRepository.AddAsync(dlAdd);
                await _iInvestorRepository.Commit();
                await InitCategoryAsync("Investor");
                await AddLog(dlAdd.Id, $"Thêm mới dự án '{dlAdd.Name}'", LogType.Insert);


                return new ApiResponseData<Investor>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("InvestorService.Create", ex.ToString());
                return new ApiResponseData<Investor>();
            }
        }

        public async Task<ApiResponseData<Investor>> Edit(InvestorCommand model)
        {
            try
            {
                var dl = await _iInvestorRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Investor>() { Data = null, Status = 0 };
                }

                dl.Name = model.Name;
                dl.Description = model.Description;
                dl.Code = model.Code;
                dl.Status = model.Status;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                _iInvestorRepository.Update(dl);
                await _iInvestorRepository.Commit();
                await AddLog(dl.Id, $"Cập nhật dự án '{dl.Name}'", LogType.Update);
                await InitCategoryAsync("Investor");

                return new ApiResponseData<Investor>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("InvestorService.Edit", ex.ToString());
                return new ApiResponseData<Investor>();
            }
        }
        public async Task<ApiResponseData<Investor>> Delete(int id)
        {
            try
            {
                var dl = await _iInvestorRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Investor>() { Data = null, Status = 0 };
                }
                await AddLog(dl.Id, $"Xóa dự án '{dl.Name}'", LogType.Delete);
                _iInvestorRepository.Delete(dl);
                await _iInvestorRepository.Commit();
                await InitCategoryAsync("Investor");
                return new ApiResponseData<Investor>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("InvestorService.Delete", ex.ToString());
                return new ApiResponseData<Investor>();
            }
        }

        private Func<IQueryable<Investor>, IOrderedQueryable<Investor>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Investor>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Investor>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "name" => sorttype == "asc" ? EntityExtention<Investor>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<Investor>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "createDate" => sorttype == "asc" ? EntityExtention<Investor>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Investor>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "updateDate" => sorttype == "asc" ? EntityExtention<Investor>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Investor>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                _ => EntityExtention<Investor>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        #region [Log]
        public async Task<ApiResponseData<List<Domain.Model.Log>>> LogSearchPageAsync(int pageIndex, int pageSize, int? InvestorId, string sortby, string sorttype)
        {
            try
            {
                var data = await _iLogRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (x.ObjectId == InvestorId || InvestorId == null) && x.Object == controllerName, LogOrderByExtention(sortby, sorttype));

                var users = await _iUserRepository.SeachByIds(data.Data.Select(x => x.SystemUserId).ToList());
                foreach (var item in data.Data)
                {
                    item.SystemUserObject = users.FirstOrDefault(x => x.Id == item.SystemUserId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("InvestorService.LogSearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Domain.Model.Log>>();
            }
        }

        private Func<IQueryable<Domain.Model.Log>, IOrderedQueryable<Domain.Model.Log>> LogOrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "CreatedDate" => sorttype == "asc" ? EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                _ => EntityExtention<Domain.Model.Log>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        private async Task InitCategoryAsync(string tableName)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName);
            }
            catch { }
        }
    }
}

