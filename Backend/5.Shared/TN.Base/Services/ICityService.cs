﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PT.Base;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface ICityService : IService<City>
    {
        Task<ApiResponseData<List<City>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<City>> GetById(int id);
        Task<ApiResponseData<City>> Create(CityCommand model);
        Task<ApiResponseData<City>> Edit(CityCommand model);
        Task<ApiResponseData<City>> Delete(int id);

    }
    public class CityService : ICityService
    {
        private readonly ILogger _logger;
        private readonly ICityRepository _iCityRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly string controllerName = "";

        public CityService
        (
            ILogger<CityService> logger,
            ICityRepository iCityRepository,
            IUserRepository iUserRepository,
            ILogRepository iLogRepository,
            IParameterRepository iParameterRepository,
            IHubContext<CategoryHub> hubContext


        )
        {
            _logger = logger;
            _iCityRepository = iCityRepository;
            _iUserRepository = iUserRepository;
            _iParameterRepository = iParameterRepository;
            _iLogRepository = iLogRepository;
            _hubContext = hubContext;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<City>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                var data = await _iCityRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    && (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new City
                    {
                        Id = x.Id,
                        PrivateId = x.PrivateId,
                        Name = x.Name,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser

                    });
                var ids = data.Data.Select(x => x.CreatedUser).ToList();
                ids.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(ids);

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("CitySevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<City>>();
            }
        }

        public async Task<ApiResponseData<City>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<City>() { Data = await _iCityRepository.SearchOneAsync(x => x.Id == id) };
            }
            catch (Exception ex)
            {
                _logger.LogError("CitySevice.Get", ex.ToString());
                return new ApiResponseData<City>();
            }
        }

        public async Task<ApiResponseData<City>> Create(CityCommand model)
        {
            try
            {
                var dlAdd = new City
                {
                    Id = model.Id,
                    PrivateId = 0,
                    Name = model.Name,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    UpdatedDate = DateTime.Now,
                    UpdatedUser = UserInfo.UserId

                };
                await _iCityRepository.AddAsync(dlAdd);
                await _iCityRepository.Commit();
                await InitCategoryAsync("Cities");
                await AddLog(dlAdd.Id, $"Thêm mới tỉnh/thành phố '{dlAdd.Name}'", LogType.Insert);
                return new ApiResponseData<City>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CitySevice.Create", ex.ToString());
                return new ApiResponseData<City>();
            }
        }

        public async Task<ApiResponseData<City>> Edit(CityCommand model)
        {
            try
            {
                var dl = await _iCityRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<City>() { Data = null, Status = 0 };
                }

                dl.Id = model.Id;
                dl.PrivateId = model.PrivateId;
                dl.Name = model.Name;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;


                _iCityRepository.Update(dl);
                await _iCityRepository.Commit();
                await InitCategoryAsync("Cities");
                await AddLog(dl.Id, $"Thêm mới tỉnh/thành phố '{dl.Name}'", LogType.Insert);
                return new ApiResponseData<City>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CitySevice.Edit", ex.ToString());
                return new ApiResponseData<City>();
            }
        }
        public async Task<ApiResponseData<City>> Delete(int id)
        {
            try
            {
                var dl = await _iCityRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<City>() { Data = null, Status = 0 };
                }

                _iCityRepository.Delete(dl);
                await _iCityRepository.Commit();
                await InitCategoryAsync("Cities");
                await AddLog(dl.Id, $"Thêm mới tỉnh/thành phố '{dl.Name}'", LogType.Insert);

                return new ApiResponseData<City>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("CitySevice.Delete", ex.ToString());
                return new ApiResponseData<City>();
            }
        }

        private Func<IQueryable<City>, IOrderedQueryable<City>> OrderByExtention(string sortby, string sorttype)
        {
            return sortby switch
            {
                "id" => sorttype == "asc" ? EntityExtention<City>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<City>.OrderBy(m => m.OrderByDescending(x => x.Id)),
                "privateId" => sorttype == "asc" ? EntityExtention<City>.OrderBy(m => m.OrderBy(x => x.PrivateId)) : EntityExtention<City>.OrderBy(m => m.OrderByDescending(x => x.PrivateId)),
                "name" => sorttype == "asc" ? EntityExtention<City>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<City>.OrderBy(m => m.OrderByDescending(x => x.Name)),
                "createdDate" => sorttype == "asc" ? EntityExtention<City>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<City>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
                "createdUser" => sorttype == "asc" ? EntityExtention<City>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<City>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
                "updatedDate" => sorttype == "asc" ? EntityExtention<City>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<City>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
                "updatedUser" => sorttype == "asc" ? EntityExtention<City>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<City>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
                _ => EntityExtention<City>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        private async Task InitCategoryAsync(string tableName)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName);
            }
            catch { }
        }
    }
}
