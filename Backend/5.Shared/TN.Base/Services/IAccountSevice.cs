﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using PT.Base.Models;
using PT.Domain.Model;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;

namespace PT.Base.Services
{
    public interface IAccountService : IService<ApplicationUser>
    {
        Task<ApiResponseData<AccountData>> LoginAsync(LoginCommand model);
        Task<ApiResponseData<AccountProfilesDTO>> UpdateProfiles(AccountProfilesCommand use);
        Task<ApiResponseData<AccountProfilesDTO>> GetProfiles();
        Task<ApiResponseData<object>> ChangePassword(ChangePasswordCommand use);
        IActionResult ExternalLogin(string provider, string redirectUrl);
        Task<object> ForgotPassword(ForgotPasswordViewCommand model);
    }

    public class AccountService : IAccountService
    {
        private readonly ILogger _logger;
        private readonly IRoleRepository _iRoleRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IOptions<AuthorizeSettings> _authorizeSettings;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IOptions<ApiSettings> _apiSettings;
        private readonly IOptions<BaseSettings> _baseSettings;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountService
        (
            ILogger<RolesService> logger,
            IRoleRepository iRoleRepository,
            IUserRepository iUserRepository,
            IOptions<AuthorizeSettings> authorizeSettings,
            SignInManager<ApplicationUser> signInManager,
            IOptions<ApiSettings> apiSettings,
            UserManager<ApplicationUser> userManager,
            IOptions<BaseSettings> baseSettings
        )
        {
            _logger = logger;
            _iRoleRepository = iRoleRepository;
            _iUserRepository = iUserRepository;
            _authorizeSettings = authorizeSettings;
            _signInManager = signInManager;
            _apiSettings = apiSettings;
            _userManager = userManager;
            _baseSettings = baseSettings;
        }

        public async Task<ApiResponseData<AccountData>> LoginAsync(LoginCommand model)
        {
            try
            {
                // Kiểm tra xem tài khoản bị khóa hay không
                var kt = await _iUserRepository.SearchOneAsync(m => m.UserName == model.Username);
                if (kt == null)
                {
                    return new ApiResponseData<AccountData> { Status = 2 };
                }

                if (kt.IsLock && kt.IsSuperAdmin == false)
                {
                    return new ApiResponseData<AccountData> { Status = 3 };
                }

                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, true, false);
                if (result.Succeeded)
                {
                    if (kt.IsReLogin)
                    {
                        kt.IsReLogin = false;
                        _iUserRepository.Update(kt);
                        await _iUserRepository.Commit();
                    }

                    var tokenInfo = await GenToken(kt);
                    var tokenInfo2 = GenTokenMin(kt);
                    
                    return new ApiResponseData<AccountData>
                    {
                        Status = 1,
                        Data = new AccountData(kt, tokenInfo.Item3?.Type ?? RoleManagerType.Admin, tokenInfo.Item2, tokenInfo.Item1, tokenInfo2)
                    };
                }

                return new ApiResponseData<AccountData> { Status = 2 };
            }
            catch (Exception ex)
            {
                _logger.LogError("AccountSevice.LoginAsync", ex.ToString());
                return new ApiResponseData<AccountData> { Status = -1 };
            }
        }

        public async Task<Tuple<string, List<DataRoleActionModel>, ApplicationRole>> GenToken(ApplicationUser kt)
        {
            var dlRole = (await _iRoleRepository.RolesByUser(kt.Id)).FirstOrDefault();

            var roleAcctions = (await _iUserRepository.RoleActionsByUserAsync(kt.Id)).ToList();
            var strRole = JsonConvert.SerializeObject(roleAcctions);
            var claims = new[]
            {
                        new Claim(JwtRegisteredClaimNames.Sid, kt.Id.ToString()),
                        new Claim("RoleType", dlRole == null ? "-1" : $"{(int) dlRole.Type}"),
                        new Claim("IsSuperAdmin", kt.IsSuperAdmin.ToString()),
                        new Claim("RoleActions", strRole),
                        new Claim("ObjectIds", kt.ObjectIds ?? "0"),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_apiSettings.Value.TokenKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_apiSettings.Value.TokenIssuer, _apiSettings.Value.TokenAudience, claims, expires: DateTime.Now.AddHours(24), signingCredentials: creds);

            var tokenX = new JwtSecurityTokenHandler().WriteToken(token);
            return new Tuple<string, List<DataRoleActionModel>, ApplicationRole>(tokenX, roleAcctions, dlRole);
        }

        public string GenTokenMin(ApplicationUser kt)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sid, kt.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_apiSettings.Value.TokenKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_apiSettings.Value.TokenIssuer, _apiSettings.Value.TokenAudience, claims, expires: DateTime.Now.AddHours(24), signingCredentials: creds);
            return  new JwtSecurityTokenHandler().WriteToken(token);
        }


        public async Task<ApiResponseData<AccountProfilesDTO>> GetProfiles()
        {
            var dl = await _iUserRepository.SearchOneAsync(x => x.Id == UserInfo.UserId);
            if (dl == null)
            {
                return new ApiResponseData<AccountProfilesDTO> { Status = 0, Data = null };
            }
            return new ApiResponseData<AccountProfilesDTO> { Status = 1, Data = new AccountProfilesDTO(dl) };
        }

        public async Task<ApiResponseData<AccountProfilesDTO>> UpdateProfiles(AccountProfilesCommand use)
        {
            var dl = await _iUserRepository.SearchOneAsync(x => x.Id == UserInfo.UserId);
            if (dl == null) return new ApiResponseData<AccountProfilesDTO> { Status = 0, Data = null };

            dl.DisplayName = use.DisplayName;
            dl.Email = use.Email;
            dl.PhoneNumber = use.PhoneNumber;
            dl.Avatar = use.Avatar;

            _iUserRepository.Update(dl);
            await _iUserRepository.Commit();

            return new ApiResponseData<AccountProfilesDTO> { Status = 1, Data = new AccountProfilesDTO(dl) };
        }

        public async Task<ApiResponseData<object>> ChangePassword(ChangePasswordCommand use)
        {
            var dl = await _iUserRepository.SearchOneAsync(x => x.Id == UserInfo.UserId);
            if (dl == null)
            {
                return new ApiResponseData<object> { Status = 0, Data = null };
            }

            var changePasswordResult = await _userManager.ChangePasswordAsync(dl, use.OldPassword, use.NewPassword);

            if (changePasswordResult.Succeeded)
            {
                await _signInManager.RefreshSignInAsync(dl);
                return new ApiResponseData<object> { Status = 1, Message = "Thay đổi mật khẩu thành công" };
            }
            else
            {
                return new ApiResponseData<object> { Status = 3, Message = "Thay đổi mật khẩu thất bại do sai mật khẩu cũ, vui lòng nhập lại" };
            }
        }

        public IActionResult ExternalLogin(string provider, string redirectUrl)
        {
            return new ChallengeResult(provider, _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl));
        }

        private int DecodeTokenGetUserId(string stream)
        {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream);
            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            return Convert.ToInt32(tokenS.Claims.First(claim => claim.Type == "sid").Value);
        }

        public async Task<object> ForgotPassword(ForgotPasswordViewCommand model)
        {
            bool capchaOke = true;
            if (_baseSettings.Value.IsCapCha)
            {
                // var output = await _iUserRepository.VeryfyCapcha(_authorizeSettings.Value.CapchaVerifyUrl, _authorizeSettings.Value.CapChaSecret, model.Capcha);
                // capchaOke = output.Success;
            }

            if (capchaOke)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    return new ApiResponseData<AccountData> { Status = 2 };
                }

                // var code = Functions.RandomString(6);

                if (_authorizeSettings.Value.CheckForgotPassword)
                {
                }

                if (model.IsSendEmail)
                {

                }
                else
                {
                }

                return new ApiResponseData<AccountData> { Status = 1 };
            }
            else
            {
                return new ApiResponseData<AccountData> { Status = 0 };
            }
        }
    }
}