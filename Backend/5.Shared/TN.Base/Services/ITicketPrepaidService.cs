﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PT.Base;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace TN.Base.Services
{
    public interface ITicketPrepaidService : IService<TicketPrepaid>
    {
        Task<ApiResponseData<List<TicketPrepaid>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            ETicketType? ticketPrice_TicketType,
            int? accountId,
            int? projectId,
            int? customerGroupId,
            DateTime? sellStart,
            DateTime? sellEnd,
            string sortby,
            string sorttype
        );
        Task<ApiResponseData<TicketPrepaid>> GetById(int id);
        Task<ApiResponseData<TicketPrepaid>> Create(TicketPrepaidCommand model);
        Task<ApiResponseData<TicketPrepaid>> Edit(TicketPrepaidEditCommand model);
        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);
        Task<ApiResponseData<List<CustomerGroup>>> SearchTicketPriceByProject(int projectId, int accountId);
        Task<string> ExportReport(
            string key,
            ETicketType? ticketPrice_TicketType,
            int? accountId,
            int? projectId,
            int? customerGroupId,
            DateTime? sellStart,
            DateTime? sellEnd
        );

    }
    public class TicketPrepaidService : ITicketPrepaidService
    {
        private readonly ILogger _logger;
        private readonly BaseSettings _baseSettings;
        private readonly ILogRepository _iLogRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        private readonly IUserRepository _iUserRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly ITransactionRepository _iTransactionRepository;
        private readonly ITicketPriceRepository _iTicketPriceRepository;
        private readonly ITicketPrepaidRepository _iTicketPrepaidRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly INotificationJobRepository _iNotificationJobRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;

        public TicketPrepaidService
        (
            ILogRepository iLogRepository,
            ILogger<TicketPrepaidService> logger,
            IOptions<BaseSettings> baseSettings,
            IWebHostEnvironment iWebHostEnvironment,

            IUserRepository iUserRepository,
            IWalletRepository iWalletRepository,
            IAccountRepository iAccountRepository,
            IProjectRepository iProjectRepository,
            ITransactionRepository iTransactionRepository,
            ITicketPriceRepository iTicketPriceRepository,
            ITicketPrepaidRepository iTicketPrepaidRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IProjectAccountRepository iProjectAccountRepository,
            INotificationJobRepository iNotificationJobRepository,
            IWalletTransactionRepository iWalletTransactionRepository
        )
        {
            _logger = logger;
            _iLogRepository = iLogRepository;
            _baseSettings = baseSettings.Value;
            _iWebHostEnvironment = iWebHostEnvironment;

            _iUserRepository = iUserRepository;
            _iWalletRepository = iWalletRepository;
            _iAccountRepository = iAccountRepository;
            _iProjectRepository = iProjectRepository;
            _iTicketPriceRepository = iTicketPriceRepository;
            _iTransactionRepository = iTransactionRepository;
            _iTicketPrepaidRepository = iTicketPrepaidRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iNotificationJobRepository = iNotificationJobRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;

        }

        public async Task<ApiResponseData<List<TicketPrepaid>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            ETicketType? ticketPrice_TicketType,
            int? accountId,
            int? projectId,
            int? customerGroupId,
            DateTime? sellStart,
            DateTime? sellEnd,
            string sortby,
            string sorttype)
        {
            try
            {
                var data = await _iTicketPrepaidRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => x.Id > 0
                    && (key == null || x.Code.Contains(key))
                    && (accountId == null || x.AccountId == accountId)
                    && (customerGroupId == null || x.CustomerGroupId == customerGroupId)
                    && (projectId == null || x.ProjectId == projectId)
                    && (sellStart == null || x.DateOfPayment >= sellStart.Value.Date)
                    && (sellEnd == null || x.DateOfPayment <= sellEnd.Value.Date)
                    && (ticketPrice_TicketType == null || x.TicketPrice_TicketType == ticketPrice_TicketType)
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new TicketPrepaid
                    {
                        Id = x.Id,
                        AccountId = x.AccountId,
                        CustomerGroupId = x.CustomerGroupId,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        TicketValue = x.TicketValue,
                        TicketPriceId = x.TicketPriceId,
                        DateOfPayment = x.DateOfPayment,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        ProjectId = x.ProjectId,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        Code = x.Code,
                        TicketPrice_AllowChargeInSubAccount = x.TicketPrice_AllowChargeInSubAccount,
                        TicketPrice_BlockPerMinute = x.TicketPrice_BlockPerMinute,
                        TicketPrice_BlockPerViolation = x.TicketPrice_BlockPerViolation,
                        TicketPrice_BlockViolationValue = x.TicketPrice_BlockViolationValue,
                        TicketPrice_IsDefault = x.TicketPrice_IsDefault,
                        TicketPrice_LimitMinutes = x.TicketPrice_LimitMinutes,
                        TicketPrice_TicketType = x.TicketPrice_TicketType,
                        TicketPrice_TicketValue = x.TicketPrice_TicketValue,
                        ChargeInAccount = x.ChargeInAccount,
                        ChargeInSubAccount = x.ChargeInSubAccount,
                        MinutesSpent = x.MinutesSpent,
                        Note = x.Note,
                        TicketTypeNote = x.TicketTypeNote
                    });

                var customerGroups = await _iCustomerGroupRepository.Search(x => data.Data.Select(x => x.CustomerGroupId).Contains(x.Id));
                var accounts = await _iAccountRepository.Search(x => data.Data.Select(x => x.AccountId).Contains(x.Id), null, x => new Account() { Id = x.Id, FullName = x.FullName, Phone = x.Phone, Status = x.Status, Type = x.Type });
                var idUsers = data.Data.Select(x => x.CreatedUser).ToList();
                idUsers.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(idUsers);

                var ticketPrices = await _iTicketPriceRepository.Search(x => data.Data.Select(x => x.TicketPriceId).Contains(x.Id));

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);

                    item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                    var ticketPrice = new TicketPrice
                    {
                        AllowChargeInSubAccount = item.TicketPrice_AllowChargeInSubAccount,
                        BlockPerMinute = item.TicketPrice_BlockPerMinute,
                        BlockPerViolation = item.TicketPrice_BlockPerViolation,
                        BlockViolationValue = item.TicketPrice_BlockViolationValue,
                        CustomerGroupId = item.CustomerGroupId,
                        LimitMinutes = item.TicketPrice_LimitMinutes,
                        ProjectId = item.ProjectId,
                        TicketValue = item.TicketPrice_TicketValue,
                        TicketType = item.TicketPrice_TicketType,
                        IsDefault = item.TicketPrice_IsDefault,
                        Name = ticketPrices.FirstOrDefault(x => x.Id == item.TicketPriceId)?.Name
                    };
                    item.TicketPrice = ticketPrice;
                    item.TicketTypeNote = _iTransactionRepository.GetTicketPriceString(ticketPrice);
                }
                if (accountId > 0)
                {
                    data.OutData = new Account(await _iAccountRepository.SearchOneAsync(x => x.Id == accountId));
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<TicketPrepaid>>();
            }
        }

        public async Task<ApiResponseData<TicketPrepaid>> GetById(int id)
        {
            try
            {
                var data = await _iTicketPrepaidRepository.SearchOneAsync(x => x.Id == id);
                if (data == null)
                {
                    return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 0 };
                }
                data.Account = new Account(await _iAccountRepository.SearchOneAsync(x => x.Id == data.AccountId));
                var ticketPrice = await _iTicketPriceRepository.SearchOneAsync(x => x.Id == data.TicketPriceId);
                data.TicketPrice = new TicketPrice
                {
                    AllowChargeInSubAccount = data.TicketPrice_AllowChargeInSubAccount,
                    BlockPerMinute = data.TicketPrice_BlockPerMinute,
                    BlockPerViolation = data.TicketPrice_BlockPerViolation,
                    BlockViolationValue = data.TicketPrice_BlockViolationValue,
                    CustomerGroupId = data.CustomerGroupId,
                    LimitMinutes = data.TicketPrice_LimitMinutes,
                    ProjectId = data.ProjectId,
                    TicketValue = data.TicketPrice_TicketValue,
                    TicketType = data.TicketPrice_TicketType,
                    IsDefault = data.TicketPrice_IsDefault,
                    Name = ticketPrice?.Name
                };
                data.TicketTypeNote = _iTransactionRepository.GetTicketPriceString(data.TicketPrice);
                return new ApiResponseData<TicketPrepaid>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.Get", ex.ToString());
                return new ApiResponseData<TicketPrepaid>();
            }
        }

        public async Task<ApiResponseData<TicketPrepaid>> Create(TicketPrepaidCommand model)
        {
            try
            {
                return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 0, Message = "Thêm vé trả trước thất bại" };

                //var ticketType = await _iTicketPriceRepository.SearchOneAsync(x => x.Id == model.TicketPriceId);
                //if (ticketType == null)
                //{
                //    return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 2, Message = "Bảng giá vé không tồn tại" };
                //}
                //var startDate = model.StartDate;
                //var endDate = model.StartDate;
                //if(ticketType.TicketType == ETicketType.Month)
                //{
                //    startDate = new DateTime(model.Year, model.Month, 1);
                //    endDate = new DateTime(model.Year, model.Month, DateTime.DaysInMonth(model.Year, model.Month));
                //}    

                //if(startDate < DateTime.Now.Date && ticketType.TicketType == ETicketType.Day)
                //{
                //    return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 4, Message = "Không thể mua vé ngày của hôm trước" };
                //}
                //else if (endDate.Date < DateTime.Now.Date && ticketType.TicketType == ETicketType.Month)
                //{
                //    return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 4, Message = "Không thể mua vé tháng của tháng trước" };
                //}

                //var ktNoCuoc = await _iTransactionRepository.AnyAsync(x => x.Status == EBookingStatus.Debt && x.AccountId == model.AccountId);
                //if (ktNoCuoc)
                //{
                //    return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 10, Message = "Khách hàng đang nợ cước chuyến đi không thể mua vé trả trước" };
                //}

                //var getW = await _iWalletRepository.GetWalletAsync(model.AccountId);
                //var account = await _iAccountRepository.SearchOneAsync(x => x.Id == model.AccountId);
                //if (account == null || account.VerifyStatus != EAccountVerifyStatus.Ok)
                //{
                //    return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 4, Message = "Tài khoản khách hàng phải được xác nhận mới mua được vé trả trước" };
                //}
                //// Kiểm tra số tiền còn lại trong ví 
                //if (ticketType.TicketValue > (getW.Balance + getW.SubBalance))
                //{
                //    return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 4, Message = "Tài khoản của khách hàng không đủ điều kiện thực hiện thao tác này" };
                //}
                //// Nếu bảng giá vé là thuộc nhóm đặc biệt mà ko phải mặc dịnh thì phải check xem khách hàng này có thuộc nhóm kh không
                //if (ticketType.CustomerGroupId > 0 && !ticketType.IsDefault)
                //{
                //    var checkInCustomerGroup = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == model.AccountId && x.ProjectId == ticketType.ProjectId && x.CustomerGroupId == ticketType.CustomerGroupId);
                //    if (!checkInCustomerGroup)
                //    {
                //        return new ApiResponseData<TicketPrepaid>() { Status = 6, Message = "Khách hàng không thuộc nhóm đối tượng mua loại vé này" };
                //    }
                //}

                //// Chia tiền
                //var splitTotalPrice = _iWalletRepository.SplitTotalPrice(ticketType.TicketValue, ticketType.AllowChargeInSubAccount, getW);
                //// Kiểm tra để ra hạn
                //var exitsPrepad = await _iTicketPrepaidRepository.SearchOneAsync(x => x.ProjectId == ticketType.ProjectId && x.AccountId == model.AccountId && x.StartDate == startDate.Date && x.TicketPrice_TicketType == ticketType.TicketType);
                //if (exitsPrepad != null)
                //{
                //    // Ra hạn
                //    exitsPrepad.Note = $"{model.Note}";
                //    exitsPrepad.TicketPrice_LimitMinutes += ticketType.LimitMinutes;
                //    exitsPrepad.NumberExpirations += 1;
                //    exitsPrepad.UpdatedDate = DateTime.Now;
                //    exitsPrepad.UpdatedUser = UserInfo.UserId;

                //    _iTicketPrepaidRepository.Update(exitsPrepad);
                //    await _iTicketPrepaidRepository.Commit();

                //    await AddLog(exitsPrepad.AccountId, $"Ra hạn trả trước '{exitsPrepad.Code}', ngày bán {exitsPrepad.DateOfPayment:dd/MM/yyyy}, hạn từ/đến {exitsPrepad.StartDate:dd/MM/yyyy} - {exitsPrepad.EndDate:dd/MM/yyyy}", LogType.Update);

                //    // Tạo giao dịch log
                //    await _iWalletTransactionRepository.AddAsync(new WalletTransaction
                //    {
                //        AccountId = model.AccountId,
                //        CampaignId = 0,
                //        IsAsync = true,
                //        Note = $"Khách hàng ra hạn vé trả trước lần {exitsPrepad.NumberExpirations}",
                //        OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.WithdrawPrepaid),
                //        CreatedDate = DateTime.Now,
                //        CreatedUser = UserInfo.UserId,
                //        TransactionId = exitsPrepad.Id,
                //        TransactionCode = exitsPrepad.Code,
                //        WalletId = getW.Id,
                //        Status = EWalletTransactionStatus.Done,
                //        SubAmount = splitTotalPrice.Item2,
                //        Amount = splitTotalPrice.Item1,
                //        TotalAmount = splitTotalPrice.Item2 + splitTotalPrice.Item1,
                //        Type = EWalletTransactionType.WithdrawPrepaid,
                //        Wallet_Balance = getW.Balance,
                //        Wallet_SubBalance = getW.SubBalance,
                //        Wallet_HashCode = getW.HashCode,
                //        HashCode = Function.SignSHA256((splitTotalPrice.Item1 + splitTotalPrice.Item2).ToString(), _baseSettings.TransactionSecretKey)
                //    });
                //    await _iWalletTransactionRepository.Commit();
                //    // Trừ tiền trong ví
                //    await _iWalletRepository.UpdateAddWalletAsync(model.AccountId, -splitTotalPrice.Item1, -splitTotalPrice.Item2, 0, _baseSettings.TransactionSecretKey, false, false, 0);
                //    await _iNotificationJobRepository.SendNow(exitsPrepad.AccountId, "TNGO thông báo", $"Bạn ra hạn lần {exitsPrepad.NumberExpirations} vé {exitsPrepad.TicketPrice_TicketType.ToEnumGetDisplayName().ToLower()}, tài khoản của bạn bị trừ -{ Function.FormatMoney(exitsPrepad.TicketPrice_TicketValue)}  điểm, cám ơn sự ủng hộ của quý khách", UserInfo.UserId, DockEventType.Not, null);
                //}
                //else
                //{
                //    var dlAdd = new TicketPrepaid
                //    {
                //        AccountId = model.AccountId,
                //        CustomerGroupId = ticketType.CustomerGroupId,
                //        StartDate = startDate,
                //        EndDate = endDate,
                //        TicketPriceId = model.TicketPriceId,
                //        DateOfPayment = model.DateOfPayment,
                //        CreatedDate = DateTime.Now,
                //        CreatedUser = UserInfo.UserId,
                //        ProjectId = model.ProjectId,
                //        Code = $"{model.ProjectId:D4}{((byte)ticketType.TicketType):D2}{model.AccountId:D6}{DateTime.Now:yyyyMMddHHmmss}",
                //        TicketPrice_IsDefault = ticketType.IsDefault,
                //        TicketPrice_AllowChargeInSubAccount = ticketType.AllowChargeInSubAccount,
                //        TicketPrice_TicketType = ticketType.TicketType,
                //        TicketPrice_BlockPerMinute = ticketType.BlockPerMinute ?? 0,

                //        TicketPrice_BlockPerViolation = ticketType.BlockPerViolation ?? 0,
                //        TicketPrice_BlockViolationValue = ticketType.BlockViolationValue ?? 0,
                //        TicketPrice_LimitMinutes = ticketType.LimitMinutes,

                //        TicketPrice_TicketValue = ticketType.TicketValue,
                //        TicketValue = splitTotalPrice.Item1 + splitTotalPrice.Item2,
                //        Note = model.Note,
                //        ChargeInAccount = splitTotalPrice.Item1,
                //        ChargeInSubAccount = splitTotalPrice.Item2,
                //        MinutesSpent = 0
                //    };
                //    await _iTicketPrepaidRepository.AddAsync(dlAdd);
                //    await _iTicketPrepaidRepository.Commit();
                //    // Add log
                //    await AddLog(dlAdd.AccountId, $"Mua vé trả trước '{dlAdd.Code}', ngày bán {dlAdd.DateOfPayment:dd/MM/yyyy}, hạn từ/đến {dlAdd.StartDate:dd/MM/yyyy} - {dlAdd.EndDate:dd/MM/yyyy}", LogType.Insert);
                //    // Tạo giao dịch log
                //    await _iWalletTransactionRepository.AddAsync(new WalletTransaction
                //    {
                //        AccountId = model.AccountId,
                //        CampaignId = 0,
                //        IsAsync = true,
                //        Note = "Khách hàng mua vé trả trước",
                //        OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.WithdrawPrepaid),
                //        CreatedDate = DateTime.Now,
                //        CreatedUser = UserInfo.UserId,
                //        TransactionId = dlAdd.Id,
                //        TransactionCode = dlAdd.Code,
                //        WalletId = getW.Id,
                //        Status = EWalletTransactionStatus.Done,
                //        SubAmount = splitTotalPrice.Item2,
                //        Amount = splitTotalPrice.Item1,
                //        TotalAmount = splitTotalPrice.Item2 + splitTotalPrice.Item1,
                //        Type = EWalletTransactionType.WithdrawPrepaid,
                //        Wallet_Balance = getW.Balance,
                //        Wallet_SubBalance = getW.SubBalance,
                //        Wallet_HashCode = getW.HashCode,
                //        HashCode = Function.SignSHA256((splitTotalPrice.Item1 + splitTotalPrice.Item2).ToString(), _baseSettings.TransactionSecretKey)
                //    });
                //    await _iWalletTransactionRepository.Commit();
                //    // Trừ tiền trong ví
                //    await _iWalletRepository.UpdateAddWalletAsync(model.AccountId, -splitTotalPrice.Item1, -splitTotalPrice.Item2, 0, _baseSettings.TransactionSecretKey, false, false, 0);
                //    await _iNotificationJobRepository.SendNow(dlAdd.AccountId, "TNGO thông báo", $"Bạn mua thành công vé {dlAdd.TicketPrice_TicketType.ToEnumGetDisplayName().ToLower()}, tài khoản của bạn bị trừ -{ Function.FormatMoney(dlAdd.TicketPrice_TicketValue)}  điểm, cám ơn sự ủng hộ của quý khách", UserInfo.UserId, DockEventType.Not, null);
                //}
                //return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 1, Message = "Thêm vé trả trước thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.Create", ex.ToString());
                return new ApiResponseData<TicketPrepaid>();
            }
        }

        public async Task<ApiResponseData<TicketPrepaid>> Edit(TicketPrepaidEditCommand model)
        {
            try
            {
                var kt = await _iTicketPrepaidRepository.SearchOneAsync(x => x.Id == model.Id);
                if (kt == null)
                {
                    return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 0, Message = "Dữ liệu không tồn tại" };
                }
                kt.Note = model.Note;
                kt.UpdatedDate = DateTime.Now;
                kt.DateOfPayment = model.DateOfPayment;
                _iTicketPrepaidRepository.Update(kt);
                await _iTicketPrepaidRepository.Commit();
                // Add log
                await AddLog(kt.AccountId, $"Cập nhật vé trả trước '{kt.Code}', ngày bán {kt.DateOfPayment:dd/MM/yyyy}, hạn từ/đến {kt.StartDate:dd/MM/yyyy} - {kt.EndDate:dd/MM/yyyy}", LogType.Update);
                return new ApiResponseData<TicketPrepaid>() { Data = null, Status = 1, Message = "Cập nhật vé trả trước thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.Create", ex.ToString());
                return new ApiResponseData<TicketPrepaid>();
            }
        }

        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }

        public async Task<ApiResponseData<List<CustomerGroup>>> SearchTicketPriceByProject(int projectId, int accountId)
        {
            try
            {
                var listGroup = await _iCustomerGroupRepository.Search(x => x.ProjectId == projectId);
                var listTicketPrice = await _iTicketPriceRepository.Search(x => x.ProjectId == projectId && x.TicketType != ETicketType.Block);
                var projectAccount = await _iProjectAccountRepository.SearchOneAsync(x => x.ProjectId == projectId && x.AccountId == accountId);

                foreach (var item in listTicketPrice)
                {
                    item.Note = _iTransactionRepository.GetTicketPriceString(item);
                }

                foreach (var item in listGroup.Where(x => x.IsDefault).ToList())
                {
                    item.TicketPrices = listTicketPrice.Where(x => x.CustomerGroupId == item.Id).ToList();
                }

                foreach (var item in listGroup.Where(x => !x.IsDefault).ToList())
                {
                    item.TicketPrices = listTicketPrice.Where(x => x.CustomerGroupId == item.Id).ToList();
                    item.IsAccountActive = projectAccount != null && (projectAccount.CustomerGroupId == item.Id ? true : false);
                }
                return new ApiResponseData<List<CustomerGroup>>() { Data = listGroup, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchTicketPriceByProject", ex.ToString());
                return new ApiResponseData<List<CustomerGroup>>();
            }
        }

        private Func<IQueryable<TicketPrepaid>, IOrderedQueryable<TicketPrepaid>> OrderByExtention(string sortby, string sorttype) => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "accountId" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.AccountId)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.AccountId)),
            "customerGroupId" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.CustomerGroupId)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.CustomerGroupId)),
            "startDate" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.StartDate)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.StartDate)),
            "endDate" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.EndDate)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.EndDate)),
            "ticketValue" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketValue)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketValue)),
            "ticketPriceId" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPriceId)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPriceId)),
            "dateOfPayment" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.DateOfPayment)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.DateOfPayment)),
            "createdDate" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "createdUser" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser)),
            "projectId" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.ProjectId)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.ProjectId)),
            "updatedDate" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate)),
            "updatedUser" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser)),
            "code" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.Code)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.Code)),
            "ticketPrice_AllowChargeInSubAccount" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPrice_AllowChargeInSubAccount)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_AllowChargeInSubAccount)),
            "ticketPrice_BlockPerMinute" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPrice_BlockPerMinute)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_BlockPerMinute)),
            "ticketPrice_BlockPerViolation" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPrice_BlockPerViolation)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_BlockPerViolation)),
            "ticketPrice_BlockViolationValue" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPrice_BlockViolationValue)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_BlockViolationValue)),
            "ticketPrice_IsDefault" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPrice_IsDefault)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_IsDefault)),
            "ticketPrice_LimitMinutes" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPrice_LimitMinutes)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_LimitMinutes)),
            "ticketPrice_TicketType" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPrice_TicketType)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_TicketType)),
            "ticketPrice_TicketValue" => sorttype == "asc" ? EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderBy(x => x.TicketPrice_TicketValue)) : EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.TicketPrice_TicketValue)),
            _ => EntityExtention<TicketPrepaid>.OrderBy(m => m.OrderByDescending(x => x.Id)),
        };

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        #region [ExportReport]
        public async Task<string> ExportReport(
            string key,
            ETicketType? ticketPrice_TicketType,
            int? accountId,
            int? projectId,
            int? customerGroupId,
            DateTime? sellStart,
            DateTime? sellEnd
        )
        {
            var ticketPrepaids = await _iTicketPrepaidRepository.Search(
                x => (key == null || x.Code.Contains(key))
                    && (accountId == null || x.AccountId == accountId)
                    && (customerGroupId == null || x.CustomerGroupId == customerGroupId)
                    && (projectId == null || x.ProjectId == projectId)
                    && (sellStart == null || x.DateOfPayment >= sellStart.Value.Date)
                    && (sellEnd == null || x.DateOfPayment < sellEnd.Value.Date.AddDays(1))
                    && (ticketPrice_TicketType == null || x.TicketPrice_TicketType == ticketPrice_TicketType)
                );

            var accounts = await _iAccountRepository.Search(
                x => ticketPrepaids.Select(x => x.AccountId).Distinct().Contains(x.Id),
                null,
                x => new Account
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Code = x.Code,
                    RFID = x.RFID,
                    Type = x.Type,
                    Status = x.Status
                });

            var projectAccounts = await _iProjectAccountRepository.Search(
                x => accounts.Select(y => y.Id).Distinct().Contains(x.AccountId),
                null,
                x => new ProjectAccount
                {
                    Id = x.Id,
                    ProjectId = x.ProjectId,
                    AccountId = x.AccountId,
                    CustomerGroupId = x.CustomerGroupId
                });

            var projects = await _iProjectRepository.Search(
                x => projectAccounts.Select(y => y.ProjectId).Distinct().Contains(x.Id),
                null,
                x => new Project
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                });

            var customerGroups = await _iCustomerGroupRepository.Search(
                x => projectAccounts.Select(y => y.CustomerGroupId).Distinct().Contains(x.Id),
                null,
                x => new CustomerGroup
                {
                    Id = x.Id,
                    Name = x.Name
                });

            foreach (var item in ticketPrepaids)
            {
                item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                if (item.Account != null)
                {
                    item.Account.CustomerGroups = customerGroups.Where(x => projectAccounts.Where(y => y.AccountId == item.AccountId).Select(y => y.CustomerGroupId).Distinct().Contains(x.Id));

                    if (item.Account.CustomerGroups.Any())
                    {
                        foreach (var cg in item.Account.CustomerGroups)
                        {
                            cg.Project = projects.FirstOrDefault(x => x.Id == cg.ProjectId);
                        }
                    }
                }
            }

            var path = InitPath();

            using (ExcelPackage MyExcel = new ExcelPackage(new FileInfo(path.Item1)))
            {
                MyExcel.Workbook.Worksheets.Add("BC GQKN");
                ExcelWorksheet workSheet = MyExcel.Workbook.Worksheets[0];

                FormatExcelTicketPrepaid(workSheet, ticketPrepaids, sellStart, sellEnd);
                MyExcel.Save();
            }
            //
            //var memory = new MemoryStream();
            //using (var stream = new FileStream(filePath, FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //return new FileContentResult(memory.GetBuffer(), "application/x-msdownload")
            //{
            //    FileDownloadName = Path.GetFileName(filePath)
            //};

            var filePath = Path.Combine("Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), path.Item2);

            return filePath;
        }

        private void FormatExcelTicketPrepaid(ExcelWorksheet worksheet, List<TicketPrepaid> items, DateTime? sellStart, DateTime? sellEnd)
        {
            worksheet.Cells.Style.WrapText = true;
            worksheet.Cells.Style.Font.Name = "Times New Roman";
            worksheet.Cells.Style.Font.Size = 11;

            // Style Columns
            var configWidth = new Dictionary<int, int> {         // {numCol, width}
                {1 , 6 }, {2 , 25}, {3 , 15}, {4 , 15}, {5 , 15},
                {6 , 20}, {7 , 15}, {8 , 15}, {9 , 30}, {10, 15},
                {11, 15}, {12, 25}, {13, 15}, {14, 15}, {15, 15}
            };

            SetWidthColumns(worksheet, configWidth);
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Center, new int[] { 3, 4, 5, 7, 8, 9, 10, 13, 14, 15 });
            SetHorizontalAlignmentColumns(worksheet, ExcelHorizontalAlignment.Left, new int[] { 1 });
            SetNumberAsCurrency(worksheet, new int[] { 11 });
            //SetNumberAsText(worksheet, new int[] { 6, 7 });

            // Style Rows | Columns
            worksheet.Row(5).Style.Font.Bold = true;

            // Title
            worksheet.Cells["B2:E2"].Merge = true;
            worksheet.Cells["B2:E2"].Style.Font.Bold = true;
            worksheet.Cells["B2:E2"].Value = $"BÁO CÁO TRA CỨU CHI TIẾT VÉ THEO KHÁCH HÀNG";

            var fromText = sellStart != null ? sellStart?.ToString("dd/MM/yyyy") : "bắt đầu";
            var toText = sellEnd != null ? sellEnd?.ToString("dd/MM/yyyy") : "nay";

            worksheet.Cells["B3:E3"].Merge = true;
            worksheet.Cells["B3:E3"].Style.Font.Italic = true;
            worksheet.Cells["B3:E3"].Value = $"Mở bán từ ngày {fromText} đến {toText}";

            // Table head
            worksheet.Cells["A5:O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:O5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ddd"));
            worksheet.Cells["A5:O5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:O5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells["A5"].Value = "STT";
            worksheet.Cells["B5"].Value = "Tên KH";
            worksheet.Cells["C5"].Value = "SĐT";
            worksheet.Cells["D5"].Value = "Mã KH";
            worksheet.Cells["E5"].Value = "Mã RFID";
            worksheet.Cells["F5"].Value = "Nhóm KH";
            worksheet.Cells["G5"].Value = "Loại TK";
            worksheet.Cells["H5"].Value = "Trạng thái";
            worksheet.Cells["I5"].Value = "Mã vé";
            worksheet.Cells["J5"].Value = "Loại vé";
            worksheet.Cells["K5"].Value = "Giá vé";
            worksheet.Cells["L5"].Value = "Mô tả vé";
            worksheet.Cells["M5"].Value = "Ngày bán";
            worksheet.Cells["N5"].Value = "Ngày có hiệu lực";
            worksheet.Cells["O5"].Value = "Ngày hết hiệu lực";

            // Binding data 
            var rs = 6; // row start loop data table
            var stt = 1;
            foreach (var item in items)
            {
                worksheet.Cells[rs, 1].Value = stt;
                worksheet.Cells[rs, 2].Value = item.Account?.FullName;
                worksheet.Cells[rs, 3].Value = item.Account?.Phone;
                worksheet.Cells[rs, 4].Value = item.Account?.Code;
                worksheet.Cells[rs, 5].Value = item.Account?.RFID;
                worksheet.Cells[rs, 6].Value = string.Join("\r\n ", item.Account?.CustomerGroups.Select(x => $"{x.Name} ({x.Project?.Code})"));
                worksheet.Cells[rs, 7].Value = item.Account?.Type.ToEnumGetDisplayName();
                worksheet.Cells[rs, 8].Value = item.Account?.Status.ToEnumGetDisplayName();
                worksheet.Cells[rs, 9].Value = item.Code;
                worksheet.Cells[rs, 10].Value = item.TicketPrice_TicketType.ToEnumGetDisplayName();
                worksheet.Cells[rs, 11].Value = item.TicketValue;
                worksheet.Cells[rs, 12].Value = item.TicketTypeNote;
                worksheet.Cells[rs, 13].Value = item.DateOfPayment.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 14].Value = item.StartDate.ToString("dd/MM/yyyy");
                worksheet.Cells[rs, 15].Value = item.EndDate.ToString("dd/MM/yyyy");
                //
                stt++;
                rs++;
            }
            //
            SetBorderTable(worksheet, $"A5:O{rs - 1}");
            worksheet.Cells[$"A6:O{rs}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        private void SetBorderTable(ExcelWorksheet worksheet, string range)
        {
            var modelTable = worksheet.Cells[range];

            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void SetNumberAsText(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "@";
            }
        }

        private void SetNumberAsCurrency(ExcelWorksheet worksheet, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.Numberformat.Format = "#,##0";
            }
        }

        private void SetHorizontalAlignmentColumns(ExcelWorksheet worksheet, ExcelHorizontalAlignment type, int[] cols)
        {
            foreach (var col in cols)
            {
                worksheet.Column(col).Style.HorizontalAlignment = type;
            }
        }

        private void SetWidthColumns(ExcelWorksheet worksheet, Dictionary<int, int> dicts)
        {
            foreach (var item in dicts)
            {
                worksheet.Column(item.Key).Width = item.Value;
            }
        }

        private Tuple<string, string> InitPath()
        {
            var fileName = $"{Guid.NewGuid()}.xlsx";
            var folderPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "Report", DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            var filePath = Path.Combine(folderPath, fileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            //
            return new Tuple<string, string>(filePath, fileName);
        }
        #endregion
    }
}
