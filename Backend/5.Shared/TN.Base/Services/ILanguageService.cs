﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PT.Base;
using TN.Domain.Model.Common;
using TN.Utility;

namespace TN.Base.Services
{
    public interface ILanguageService : IService<Language>
    {
        Task<ApiResponseData<List<Language>>> SearchPageAsync(int pageIndex, int PageSize, string key, string orderby, string ordertype);
        Task<ApiResponseData<Language>> GetById(int id);
        Task<ApiResponseData<Language>> Create(LanguageCommand model);
        Task<ApiResponseData<Language>> Edit(LanguageCommand model);
        Task<ApiResponseData<Language>> Delete(int id);

    }
    public class LanguageService : ILanguageService
    {
        private readonly ILogger _logger;
        private readonly ILanguageRepository _iLanguageRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly ILogRepository _iLogRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly string controllerName = "";
        public LanguageService
        (
            ILogger<LanguageService> logger,
            ILanguageRepository iLanguageRepository,
            IUserRepository iUserRepository,
            IParameterRepository iParameterRepository,
            ILogRepository iLogRepository,
            IHubContext<CategoryHub> hubContext

        )
        {
            _logger = logger;
            _iLanguageRepository = iLanguageRepository;
            _iUserRepository = iUserRepository;
            _iParameterRepository = iParameterRepository;
            _iLogRepository = iLogRepository;
            _hubContext = hubContext;
            controllerName = AppHttpContext.Current.Request.RouteValues["controller"].ToString();
        }

        public async Task<ApiResponseData<List<Language>>> SearchPageAsync(int pageIndex, int PageSize, string key, string sortby, string sorttype)
        {
            try
            {
                var data = await _iLanguageRepository.SearchPagedAsync(
                    pageIndex,
                    PageSize,
                    x => x.Id > 0
                    && (key == null || x.Name.Contains(key))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new Language
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Code1 = x.Code1,
                        Code2 = x.Code2,
                        CreatedDate = x.CreatedDate,
                        CreatedUser = x.CreatedUser,
                        UpdatedDate = x.UpdatedDate,
                        UpdatedUser = x.UpdatedUser,
                        Flag1 = x.Flag1,
                        Flag2 = x.Flag2,

                    });

                var idUsers = data.Data.Select(x => x.CreatedUser).ToList();
                idUsers.AddRange(data.Data.Select(x => x.UpdatedUser ?? 0).ToList());
                var users = await _iUserRepository.SeachByIds(idUsers);
                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("LanguageSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<Language>>();
            }
        }

        public async Task<ApiResponseData<Language>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<Language>() { Data = await _iLanguageRepository.SearchOneAsync(x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LanguageSevice.Get", ex.ToString());
                return new ApiResponseData<Language>();
            }
        }

        public async Task<ApiResponseData<Language>> Create(LanguageCommand model)
        {
            try
            {
                var dlAdd = new Language
                {
                    Id = model.Id,
                    Name = model.Name,
                    Code1 = model.Code1,
                    Code2 = model.Code2,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    UpdatedDate = DateTime.Now,
                    UpdatedUser = UserInfo.UserId,
                    Flag1 = model.Flag1,
                    Flag2 = model.Flag2,

                };
                await _iLanguageRepository.AddAsync(dlAdd);
                await _iLanguageRepository.Commit();

                await InitCategoryAsync("Languages");
                await AddLog(dlAdd.Id, $"Thêm mới ngôn ngữ '{dlAdd.Name}'", LogType.Insert);
                return new ApiResponseData<Language>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LanguageSevice.Create", ex.ToString());
                return new ApiResponseData<Language>();
            }
        }

        public async Task<ApiResponseData<Language>> Edit(LanguageCommand model)
        {
            try
            {
                var dl = await _iLanguageRepository.SearchOneAsync(x => x.Id == model.Id);
                if (dl == null)
                {
                    return new ApiResponseData<Language>() { Data = null, Status = 0 };
                }

                dl.Id = model.Id;
                dl.Name = model.Name;
                dl.Code1 = model.Code1;
                dl.Code2 = model.Code2;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUser = UserInfo.UserId;
                dl.Flag1 = model.Flag1;
                dl.Flag2 = model.Flag2;


                _iLanguageRepository.Update(dl);
                await _iLanguageRepository.Commit();
                await InitCategoryAsync("Languages");
                await AddLog(dl.Id, $"Thêm mới ngôn ngữ '{dl.Name}'", LogType.Insert);
                return new ApiResponseData<Language>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LanguageSevice.Edit", ex.ToString());
                return new ApiResponseData<Language>();
            }
        }
        public async Task<ApiResponseData<Language>> Delete(int id)
        {
            try
            {
                var dl = await _iLanguageRepository.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<Language>() { Data = null, Status = 0 };
                }

                _iLanguageRepository.Delete(dl);
                await _iLanguageRepository.Commit();
                await InitCategoryAsync("Languages");
                await AddLog(dl.Id, $"Thêm mới ngôn ngữ '{dl.Name}'", LogType.Insert);
                return new ApiResponseData<Language>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("LanguageSevice.Delete", ex.ToString());
                return new ApiResponseData<Language>();
            }
        }

        private Func<IQueryable<Language>, IOrderedQueryable<Language>> OrderByExtention(string sortby, string sorttype)
        {
            Func<IQueryable<Language>, IOrderedQueryable<Language>> functionOrder = null;
            switch (sortby)
            {
                case "id":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
                case "name":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.Name)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.Name));
                    break;
                case "code1":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.Code1)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.Code1));
                    break;
                case "code2":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.Code2)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.Code2));
                    break;
                case "createdDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate));
                    break;
                case "createdUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.CreatedUser)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.CreatedUser));
                    break;
                case "updatedDate":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.UpdatedDate)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.UpdatedDate));
                    break;
                case "updatedUser":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.UpdatedUser)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.UpdatedUser));
                    break;
                case "flag1":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.Flag1)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.Flag1));
                    break;
                case "flag2":
                    functionOrder = sorttype == "asc" ? EntityExtention<Language>.OrderBy(m => m.OrderBy(x => x.Flag2)) : EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.Flag2));
                    break;

                default:
                    functionOrder = EntityExtention<Language>.OrderBy(m => m.OrderByDescending(x => x.Id));
                    break;
            }
            return functionOrder;
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = controllerName,
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
        private async Task InitCategoryAsync(string tableName)
        {
            try
            {
                await _iParameterRepository.UpdateValue(Parameter.ParameterType.Category, Guid.NewGuid().ToString());
                await _hubContext.Clients.All.SendAsync("ReceiveMessageUpdateCategory", tableName);
            }
            catch { }
        }
    }
}
