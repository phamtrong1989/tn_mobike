﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TN.Base.Command;
using TN.Domain.Model;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model.Common;
using TN.Utility;
using PT.Base;
using System.IO;
using Microsoft.AspNetCore.Http;
using PT.Domain.Model;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace TN.Base.Services
{
    public interface IWalletTransactionService : IService<WalletTransaction>
    {
        Task<ApiResponseData<List<WalletTransaction>>> SearchPageAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? projectId,
            int? accountId,
            EPaymentGroup? paymentGroup,
            EWalletTransactionType? type,
            EWalletTransactionStatus? status,
            DateTime? start,
            DateTime? end,
            string sortby,
            string sorttype);
        Task<ApiResponseData<WalletTransaction>> GetById(int id);
        Task<ApiResponseData<WalletTransaction>> Create(WalletTransactionCommand model);
        Task<ApiResponseData<List<Account>>> SearchByAccount(string key);
        Task<ApiResponseData<WalletTransaction>> Edit(WalletTransactionEditCommand model);
        Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request);
        FileStream View(string pathData, bool isPrivate);
        Task<ApiResponseData<WalletTransaction>> Gift(WalletTransactionGiftCommand model);
        Task<ApiResponseData<VoucherCode>> AddGiftCard(string code, int accountId);
        Task<ApiResponseData<RedeemPoint>> RedeemPointAdd(int accountId, int id);
        Task<ApiResponseData<object>> GiftCardGetData(int accountId);

        Task<ApiResponseData<WalletTransaction>> Arrears(WalletTransactionArrearsCommand model);
    }
    public class WalletTransactionService : IWalletTransactionService
    {
        private readonly ILogger _logger;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        private readonly IAccountRepository _iAccountRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly BaseSettings _baseSettings;
        private readonly ILogRepository _iLogRepository;
        private readonly IUserRepository _iUserRepository;
        private readonly IDepositEventRepository _iDepositEventRepository;
        private readonly INotificationJobRepository _iNotificationJobRepository;
        private readonly List<NotificationTemp> _notificationTempSettings;
        private readonly IBookingRepository _iBookingRepository;
        private readonly IVoucherCodeRepository _iVoucherCodeRepository;
        private readonly ICampaignRepository _iCampaignRepository;
        private readonly IProjectAccountRepository _iProjectAccountRepository;
        private readonly IRedeemPointRepository _iRedeemPointRepository;
        private readonly ICustomerGroupRepository _iCustomerGroupRepository;
        private readonly IProjectRepository _iProjectRepository;
        private readonly IGPSDataRepository _iEventSystemRepository;
        private readonly IHubContext<EventSystemHub> _hubContext;
        private readonly IEmailBoxRepository _iEmailBoxRepository;
        private readonly IEmailManageRepository _iEmailManageRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        private readonly IOptions<LogSettings> _logSettings;
        public WalletTransactionService
        (
            ILogger<WalletTransactionService> logger,
            IWalletTransactionRepository iWalletTransactionRepository,
            IAccountRepository iAccountRepository,
            IWalletRepository iWalletRepository,
            IOptions<BaseSettings> baseSettings,
            ILogRepository iLogRepository,
            IUserRepository iUserRepository,
            IDepositEventRepository iDepositEventRepository,
            INotificationJobRepository iNotificationJobRepository,
            IOptions<List<NotificationTemp>> notificationTempSettings,
            IBookingRepository iBookingRepository,
            IVoucherCodeRepository iVoucherCodeRepository,
            ICampaignRepository iCampaignRepository,
            IProjectAccountRepository iProjectAccountRepository,
            IRedeemPointRepository iRedeemPointRepository,
            ICustomerGroupRepository iCustomerGroupRepository,
            IProjectRepository iProjectRepository,
            IGPSDataRepository iEventSystemRepository,
            IHubContext<EventSystemHub> hubContext,
            IEmailBoxRepository iEmailBoxRepository,
            IEmailManageRepository iEmailManageRepository,
            IWebHostEnvironment iWebHostEnvironment,
            IOptions<LogSettings> logSettings
        )
        {
            _logger = logger;
            _iWalletTransactionRepository = iWalletTransactionRepository;
            _iAccountRepository = iAccountRepository;
            _iWalletRepository = iWalletRepository;
            _baseSettings = baseSettings.Value;
            _iLogRepository = iLogRepository;
            _iUserRepository = iUserRepository;
            _iDepositEventRepository = iDepositEventRepository;
            _iNotificationJobRepository = iNotificationJobRepository;
            _notificationTempSettings = notificationTempSettings.Value;
            _iBookingRepository = iBookingRepository;
            _iVoucherCodeRepository = iVoucherCodeRepository;
            _iCampaignRepository = iCampaignRepository;
            _iProjectAccountRepository = iProjectAccountRepository;
            _iRedeemPointRepository = iRedeemPointRepository;
            _iCustomerGroupRepository = iCustomerGroupRepository;
            _iProjectRepository = iProjectRepository;
            _iEventSystemRepository = iEventSystemRepository;
            _hubContext = hubContext;
            _iEmailBoxRepository = iEmailBoxRepository;
            _iEmailManageRepository = iEmailManageRepository;
            _iWebHostEnvironment = iWebHostEnvironment;
            _logSettings = logSettings;
        }

        public async Task<ApiResponseData<List<WalletTransaction>>> SearchPageAsync(
            int pageIndex, 
            int pageSize, 
            string key,
            int? projectId,
            int? accountId,
            EPaymentGroup? paymentGroup,
            EWalletTransactionType? type,
            EWalletTransactionStatus? status,
            DateTime? start,
            DateTime? end,
            string sortby, 
            string sorttype)
        {
            try
            {
                var data = await _iWalletTransactionRepository.SearchPagedAsync(
                    pageIndex,
                    pageSize,
                    x => (projectId == null || x.LocationProjectId == projectId)
                    && (accountId == null || x.AccountId == accountId)
                    && (paymentGroup == null || x.PaymentGroup == paymentGroup)
                    && (type == null || x.Type == type)
                    && (status == null || x.Status == status)
                    && (key == null || x.TransactionCode == key || x.OrderIdRef == key || x.ReqestId == key)
                    && (start == null || x.CreatedDate >= start)
                    && x.Status != EWalletTransactionStatus.Draft
                    && (end == null || x.CreatedDate < end.Value.AddDays(1))
                    ,
                    OrderByExtention(sortby, sorttype),
                    x => new WalletTransaction
                    {
                        Id = x.Id,
                        AccountId = x.AccountId,
                        WalletId = x.WalletId,
                        Type = x.Type,
                        TransactionId = x.TransactionId,
                        TransactionCode = x.TransactionCode,
                        HashCode = x.HashCode,
                        Amount = x.Amount,
                        Status = x.Status,
                        Note = x.Note,
                        CreatedDate = x.CreatedDate,
                        CampaignId = x.CampaignId,
                        IsAsync = x.IsAsync,
                        OrderIdRef = x.OrderIdRef,
                        SubAmount = x.SubAmount,
                        TotalAmount = x.TotalAmount,
                        AdminNote = x.AdminNote,
                        Campaign = x.Campaign,
                        CreatedUser = x.CreatedUser,
                        IsWarningHistory = x.IsWarningHistory,
                        PadTime = x.PadTime,
                        PaymentGroup = x.PaymentGroup,
                        ReqestId = x.ReqestId,
                        SetPenddingTime = x.SetPenddingTime,
                        TripPoint = x.TripPoint,
                        Wallet_Balance = x.Wallet_Balance,
                        Wallet_HashCode = null,
                        Wallet_SubBalance = x.Wallet_SubBalance,
                        WarningHistoryData = x.WarningHistoryData,
                        UpdatedUser = x.UpdatedUser,
                        UpdatedDate = x.UpdatedDate,
                        Files = x.Files,
                        Wallet_TripPoint = x.Wallet_TripPoint,
                        Price = x.Price,
                        DebtAmount = x.DebtAmount,
                        GiftToAccountId = x.GiftToAccountId,
                        DepositEventId = x.DepositEventId,
                        VoucherCodeId = x.VoucherCodeId,
                    });

                var accounts = await _iAccountRepository.Search(
                    x => data.Data.Select(x => x.AccountId).Contains(x.Id),
                    null,
                    x => new Account() { Id = x.Id, FullName = x.FullName, Phone = x.Phone, Status = x.Status, Type = x.Type });

                var users = await _iUserRepository.SeachByIds(
                    data.Data.Select(x => x.CreatedUser).ToList(),
                    data.Data.Select(x => x.UpdatedUser ?? 0).ToList());

                if(accountId!=null && accountId > 0)
                {
                    data.OutData = await _iAccountRepository.SearchOneAsync(x => x.Id == accountId);
                }    

                foreach (var item in data.Data)
                {
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUser);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUser);
                    item.Account = accounts.FirstOrDefault(x => x.Id == item.AccountId);
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletTransactionSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<WalletTransaction>>();
            }
        }

        public async Task<ApiResponseData<WalletTransaction>> GetById(int id)
        {
            try
            {
                return new ApiResponseData<WalletTransaction>() { Data = await _iWalletTransactionRepository.SearchOneAsync( x => x.Id == id), Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletTransactionSevice.Get", ex.ToString());
                return new ApiResponseData<WalletTransaction>();
            }
        }

        public async Task<ApiResponseData<WalletTransaction>> Arrears(WalletTransactionArrearsCommand model)
        {
            try
            {
                if(model.Amount <= 0 && model.SubAmount <=0)
                {
                    return new ApiResponseData<WalletTransaction>() { Data = null, Status = 0, Message = "Số điểm truy thu phải lớn hơn 0" };
                }

                var dl = await _iAccountRepository.SearchOneAsync(x => x.Id == model.AccountId);
                if (dl == null)
                {
                    return new ApiResponseData<WalletTransaction>() { Data = null, Status = 0 };
                }
                // Kiểm tra xem khách hàng có đủ điểm để truy thu không
                var getW = await _iWalletRepository.GetWalletAsync(dl.Id);

                if((getW.Balance < model.Amount) || (getW.SubBalance < model.SubAmount))
                {
                    return new ApiResponseData<WalletTransaction>() { Data = null, Status = 6, Message = "Số dư điểm của tài khoản không đủ để thực hiện truy thu" };
                }

                var dlAdd = new WalletTransaction
                {
                    AccountId = model.AccountId,
                    WalletId = getW.Id,
                    Type = EWalletTransactionType.Withdraw,
                    TransactionCode = model.TransactionCode,
                    Status = EWalletTransactionStatus.Done,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    IsAsync = false,
                    Wallet_Balance = getW.Balance,
                    Wallet_SubBalance = getW.SubBalance,
                    Wallet_HashCode = getW.HashCode,
                    Wallet_TripPoint = getW.TripPoint,
                    TripPoint = 0,
                    PaymentGroup = model.PaymentGroup,
                    PadTime = DateTime.Now,
                    OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.Withdraw),
                    Note = model.Note,
                    Files = model.Files,
                    Price = model.Amount,
                    Amount = model.Amount,
                    SubAmount = model.SubAmount
                };

                dlAdd.TotalAmount = dlAdd.Amount + dlAdd.SubAmount;
                dlAdd.HashCode = Function.TransactionHash(dlAdd.OrderIdRef, dlAdd.Amount, dlAdd.SubAmount, dlAdd.TripPoint, _baseSettings.TransactionSecretKey);

                await _iWalletTransactionRepository.AddAsync(dlAdd);
                await _iWalletTransactionRepository.Commit();

                await _iWalletRepository.UpdateAddWalletAsync(dlAdd.AccountId, -dlAdd.Amount, -dlAdd.SubAmount, 0, _baseSettings.TransactionSecretKey, false, false, 0);
                // Gửi cho admin phần quản lý
                var data = await _iEventSystemRepository.EventSend(_logSettings.Value, EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, dlAdd.AccountId, null, 0, 0, 0, 0, EEventSystemType.M_4005_CoTaiKhoanNapTien.ToEnumGetDisplayName(),
                   $"[{dlAdd.Type.ToEnumGetDisplayName()}] truy thu điểm khách hàng -{Function.FormatMoney(dlAdd.Amount)} điểm, -{Function.FormatMoney(dlAdd.SubAmount)} điểm KM");
                if (data != null)
                {
                    await _hubContext.Clients.All.SendAsync("ReceiveMessageEventSystemSendMessage", 0, data);
                }
                await AddLog(dlAdd.AccountId, $"{dlAdd.Type.ToEnumGetDisplayName()} cho tài khoản #'{dlAdd.AccountId}' ({dlAdd.TotalAmount} điểm,{dlAdd.SubAmount} điểm km)", LogType.Update);
                await _iNotificationJobRepository.SendNow(dlAdd.AccountId, "TNGO thông báo", $"Tài khoản của bạn bị trừ { (dlAdd.Amount <= 0 ? "" : $"{Function.FormatMoney(dlAdd.Amount)} điểm ")} { (dlAdd.SubAmount <= 0 ? "" : $"{ Function.FormatMoney(dlAdd.SubAmount)} điểm khuyến mãi") }, lý do {dlAdd.Type.ToEnumGetDisplayName()}", UserInfo.UserId, DockEventType.Not, null);
                return new ApiResponseData<WalletTransaction>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletTransactionSevice.Create", ex.ToString());
                return new ApiResponseData<WalletTransaction>();
            }
        }

        public async Task<ApiResponseData<WalletTransaction>> Create(WalletTransactionCommand model)
        { 
            try
            {
                if (model.Type != EWalletTransactionType.Manually && model.Type != EWalletTransactionType.Money && model.Type != EWalletTransactionType.BoXung && model.Type != EWalletTransactionType.PointRefund)
                {
                    return new ApiResponseData<WalletTransaction>() { Data = null, Status = 0 };
                }

                if (model.Amount <= 0 && model.SubAmount <= 0)
                {
                    return new ApiResponseData<WalletTransaction>() { Data = null, Status = 5, Message = "Bạn chưa nhập số điểm, vui lòng thử lại" };
                }

                var dl = await _iAccountRepository.SearchOneAsync(x => x.Id == model.AccountId);
                if (dl == null)
                {
                    return new ApiResponseData<WalletTransaction>() { Data = null, Status = 0 };
                }

                var checkLocation = await _iWalletRepository.LatLngToCustomerGroup(model.AccountId);

                // Kiểm tra xem có sự kiện nạp tiền không, nếu có và thuộc nhóm kh thì tự add
                var getW = await _iWalletRepository.GetWalletAsync(dl.Id);
                var dlAdd = new WalletTransaction
                {
                    AccountId = model.AccountId,
                    WalletId = getW.Id,
                    Type = model.Type,
                    TransactionCode = model.TransactionCode,
                    Status = EWalletTransactionStatus.Done,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    IsAsync = false,
                    Wallet_Balance = getW.Balance,
                    Wallet_SubBalance = getW.SubBalance,
                    Wallet_HashCode = getW.HashCode,
                    Wallet_TripPoint = getW.TripPoint,
                    TripPoint = 0,
                    PaymentGroup = model.PaymentGroup,
                    PadTime = DateTime.Now,
                    OrderIdRef = Function.GenOrderId((int)model.Type),
                    Note = model.Note,
                    Files = model.Files,
                    Price = model.Amount,
                    Amount = model.Amount,
                    SubAmount = model.SubAmount,
                    LocationProjectId = checkLocation?.ProjectId,
                    LocationCustomerGroupId = checkLocation?.CustomerGroupId,
                    LocationProjectOverwrite = checkLocation?.LocationProjectOverwrite
                };
                
                var depsitEvent = await _iDepositEventRepository.EventByAccount(model.AccountId, model.Amount, checkLocation?.ProjectId ?? 0, checkLocation?.CustomerGroupId ?? 0);
                if(depsitEvent != null && (model.Type == EWalletTransactionType.Manually || model.Type == EWalletTransactionType.Money))
                {
                    dlAdd.DepositEventId = depsitEvent.Id;
                    dlAdd.CampaignId = depsitEvent.CampaignId;
                    dlAdd.DepositEvent_SubAmount = depsitEvent.ToSubPrice;
                    dlAdd.DepositEvent_Amount = depsitEvent.ToPrice;
                    dlAdd.ReqestId = depsitEvent.TransactionCode;
                    dlAdd.Amount += depsitEvent.ToPrice;
                    dlAdd.SubAmount += depsitEvent.ToSubPrice;
                    dlAdd.Note += $" (Trong sự kiện '{ depsitEvent.Campaign?.Name}' giao dịch được +{Function.FormatMoney(depsitEvent.ToPrice)} điểm, +{Function.FormatMoney(depsitEvent.ToSubPrice)} điểm khuyến mãi, +{depsitEvent.SubPointExpiry} ngày sử dụng điểm KM)";
                }

                dlAdd.TotalAmount = dlAdd.Amount + dlAdd.SubAmount;
                dlAdd.HashCode = Function.TransactionHash(dlAdd.OrderIdRef, dlAdd.Amount, dlAdd.SubAmount, dlAdd.TripPoint, _baseSettings.TransactionSecretKey);

                await _iWalletTransactionRepository.AddAsync(dlAdd);
                await _iWalletTransactionRepository.Commit();

                if (model.Type == EWalletTransactionType.Manually || model.Type == EWalletTransactionType.Money)
                {
                    await _iWalletRepository.UpdateAddWalletAsync(dlAdd.AccountId, dlAdd.Amount, dlAdd.SubAmount, 0, _baseSettings.TransactionSecretKey, true, true, depsitEvent?.SubPointExpiry ?? 0);
                    if(depsitEvent!= null)
                    {
                        await _iDepositEventRepository.UpdateUsed(depsitEvent.Id);
                    }
                    // Gửi cho admin phần quản lý
                    var data = await _iEventSystemRepository.EventSend(_logSettings.Value, EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, dlAdd.AccountId, null, 0, 0, 0, 0, EEventSystemType.M_4005_CoTaiKhoanNapTien.ToEnumGetDisplayName(),
                       $"[{model.Type.ToEnumGetDisplayName()}] Khách hàng vừa nạp thành công {Function.FormatMoney(dlAdd.Price)} đ vào tài khoản nhận được {Function.FormatMoney(dlAdd.Amount)} điểm, {Function.FormatMoney(dlAdd.SubAmount)} điểm KM");
                    if(data!= null)
                    {
                        await _hubContext.Clients.All.SendAsync("ReceiveMessageEventSystemSendMessage", 0, data);
                    }    
                }
                else if (model.Type == EWalletTransactionType.BoXung || model.Type == EWalletTransactionType.PointRefund)
                {
                    await _iWalletRepository.UpdateAddWalletAsync(dlAdd.AccountId, dlAdd.Amount, dlAdd.SubAmount, 0, _baseSettings.TransactionSecretKey, true, false, 0);
                    // Gửi cho admin phần quản lý
                    var data = await _iEventSystemRepository.EventSend(_logSettings.Value, EEventSystemType.M_4005_CoTaiKhoanNapTien, EEventSystemWarningType.Info, dlAdd.AccountId, null, 0, 0, 0, 0, EEventSystemType.M_4005_CoTaiKhoanNapTien.ToEnumGetDisplayName(),
                       $"[{model.Type.ToEnumGetDisplayName()}] quản trị vừa bổ xung điểm cho khách hàng {Function.FormatMoney(dlAdd.Amount)} điểm, {Function.FormatMoney(dlAdd.SubAmount)} điểm KM");
                    if (data != null)
                    {
                        await _hubContext.Clients.All.SendAsync("ReceiveMessageEventSystemSendMessage", 0, data);
                    }
                }

                await AddLog(dlAdd.AccountId, $"{dlAdd.Type.ToEnumGetDisplayName()} cho tài khoản #'{dlAdd.AccountId}' ({dlAdd.TotalAmount} điểm,{dlAdd.SubAmount} điểm km)", LogType.Update);

                if (dlAdd.Type == EWalletTransactionType.BoXung)
                {
                    await _iNotificationJobRepository.SendNow(dlAdd.AccountId, "TNGO thông báo", $"Tài khoản của bạn được bổ sung { (dlAdd.Amount <= 0 ? "" : $"{Function.FormatMoney(dlAdd.Amount)} điểm ")} { (dlAdd.SubAmount <= 0 ? "" : $"{ Function.FormatMoney(dlAdd.SubAmount)} điểm khuyến mãi") }, lý do {dlAdd.Type.ToEnumGetDisplayName()}", UserInfo.UserId, DockEventType.Not, null);
                }
                else if (dlAdd.Type == EWalletTransactionType.PointRefund)
                {
                    await _iNotificationJobRepository.SendNow(dlAdd.AccountId, "TNGO thông báo", $"Tài khoản của bạn được hoàn điểm chuyến đi { (dlAdd.Amount <= 0 ? "" : $"{Function.FormatMoney(dlAdd.Amount)} điểm ")} { (dlAdd.SubAmount <= 0 ? "" : $"{ Function.FormatMoney(dlAdd.SubAmount)} điểm khuyến mãi") }", UserInfo.UserId, DockEventType.Not, null);
                }
                else if (model.Type == EWalletTransactionType.Manually || model.Type == EWalletTransactionType.Money)
                {
                    string content = string.Format(GetNotificationText(ENotificationTempType.NapTienThanhCong), $"{ (dlAdd.Amount <= 0 ? "" : $"{Function.FormatMoney(dlAdd.Amount)} điểm ")} {(dlAdd.SubAmount <= 0 ? "" : $"{Function.FormatMoney(dlAdd.SubAmount)} điểm khuyến mãi")}", $"{dlAdd.Type.ToEnumGetDisplayName()}");
                    await _iNotificationJobRepository.SendNow(dlAdd.AccountId, "TNGO thông báo", content, UserInfo.UserId, DockEventType.Not, null);
                    // Gửi email cho khách hàng
                    var account = await _iAccountRepository.SearchOneAsync(x => x.Id == dlAdd.AccountId);
                    if(account != null && !string.IsNullOrEmpty(account.Email))
                    {
                        var emailManager = await _iEmailManageRepository.SearchOneAsync(x => x.Id > 0);
                        if(emailManager!= null)
                        {
                            var template = $"{_iWebHostEnvironment.ContentRootPath}/Export/Templates/bill.html";
                            if(File.Exists(template))
                            {
                                var fileData = File.ReadAllText(template);
                                fileData = fileData.Replace("{{PadTime}}", dlAdd.PadTime?.ToString("HH:mm dd/MM/yyyy"))
                                                   .Replace("{{OrderIdRef}}", dlAdd.OrderIdRef ?? "#")
                                                   .Replace("{{TransactionType}}", dlAdd.Type.ToEnumGetDisplayName())
                                                   .Replace("{{CustomerCode}}", account.Code)
                                                   .Replace("{{CustomerName}}", account.FullName)
                                                   .Replace("{{Amount}}", Function.FormatMoney(dlAdd.Amount))
                                                   .Replace("{{SubAmount}}", Function.FormatMoney(dlAdd.SubAmount))
                                                   .Replace("{{Price}}", Function.FormatMoney(dlAdd.Price))
                                                   .Replace("{{PaymentGroup}}", dlAdd.PaymentGroup.ToEnumGetDisplayName())
                                                   .Replace("{{TotalAmount}}", Function.FormatMoney(dlAdd.Amount + dlAdd.SubAmount));

                                await _iEmailBoxRepository.AddAsync(new EmailBox
                                {
                                    IsSend = false,
                                    From = emailManager.From,
                                    To = account.Email,
                                    Subject = $"[TNGO MAIL] Bạn vừa thực hiện giao dịch nạp điểm cho TNGO #{dlAdd.TransactionCode}",
                                    Body = fileData,
                                    CreatedDate = DateTime.Now,
                                    NotificationDataId = 0,
                                    NotSend = false,
                                    ObjectId = dlAdd.Id,
                                    SendDate = DateTime.Now,
                                    Type = 0
                                });
                                await _iEmailBoxRepository.Commit();
                            }    
                        }    
                    }    
                }
                return new ApiResponseData<WalletTransaction>() { Data = dlAdd, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError($"WalletTransactionSevice.Create {ex}");
                return new ApiResponseData<WalletTransaction>();
            }
        }

        private string GetNotificationText(ENotificationTempType type)
        {
            try
            {
                return _notificationTempSettings.FirstOrDefault(x => x.Language == "vi")?.Data?.FirstOrDefault(x => x.Type == type)?.Body;
            }
            catch
            {
                return type.ToString();
            }
        }

        public async Task<ApiResponseData<WalletTransaction>> Edit(WalletTransactionEditCommand model)
        {
            try
            {
                var kt = await _iWalletTransactionRepository.SearchOneAsync(x => x.Id == model.Id);
                if (kt == null)
                {
                    return new ApiResponseData<WalletTransaction>() { Data = null, Status = 0 };
                }
                kt.AdminNote = model.AdminNote;
                kt.Files = model.Files;
                kt.UpdatedDate = DateTime.Now;
                kt.UpdatedUser = UserInfo.UserId;
                kt.TransactionCode = model.TransactionCode;

                _iWalletTransactionRepository.Update(kt);
                await _iWalletTransactionRepository.Commit();
                return new ApiResponseData<WalletTransaction>() { Data = kt, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletTransactionSevice.Edit", ex.ToString());
                return new ApiResponseData<WalletTransaction>();
            }
        }

        private Func<IQueryable<WalletTransaction>, IOrderedQueryable<WalletTransaction>> OrderByExtention(string sortby, string sorttype) => sortby switch
        {
            "id" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.Id)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            "type" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.Type)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.Type)),
            "transactionCode" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.TransactionCode)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.TransactionCode)),
            "amount" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.Amount)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.Amount)),
            "status" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.Status)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.Status)),
            "note" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.Note)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.Note)),
            "createdDate" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.CreatedDate)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.CreatedDate)),
            "campaignId" => sorttype == "asc" ? EntityExtention<WalletTransaction>.OrderBy(m => m.OrderBy(x => x.CampaignId)) : EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.CampaignId)),
            _ => EntityExtention<WalletTransaction>.OrderBy(m => m.OrderByDescending(x => x.Id)),
        };

        private async Task AddLog(int objectId, string action, LogType type, string objectType = null)
        {
            string objectName = "Account";
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = objectName,
                    ObjectId = objectId,
                    ObjectType = objectType ?? objectName,
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }

        public async Task<ApiResponseData<List<Account>>> SearchByAccount(string key)
        {
            try
            {
                _ = int.TryParse(key, out int idSearch);
                var data = await _iAccountRepository.SearchTop(10, x => key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.Phone.Contains(key) || x.Email.Contains(key) || x.RFID.Contains(key));
                return new ApiResponseData<List<Account>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<Account>>();
            }
        }


        #region [Upload file]
        public async Task<ApiResponseData<List<FileDataDTO>>> Upload(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/WalletTransaction/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 2};
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<List<FileDataDTO>> { Data = new List<FileDataDTO>(), Status = 3};
                    }
                }

                var outData = new List<FileDataDTO>();

                foreach (var file in files)
                {
                    var newFilename = $"{DateTime.Now:yyyyMMddHHmmssfff}_{Path.GetFileName(file.FileName)}";

                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}";
                    pathServer = $"{pathServer}/{newFilename}";

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);

                    outData.Add(new FileDataDTO { CreatedDate = DateTime.Now, CreatedUser = UserInfo.UserId, FileName = Path.GetFileName(file.FileName), Path = pathServer, FileSize = file.Length });
                }
                return new ApiResponseData<List<FileDataDTO>>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<FileDataDTO>>();
            }
        }

        public FileStream View(string pathData, bool isPrivate)
        {
            try
            {
                if (isPrivate)
                {
                    string file = $"{_baseSettings.PrivateDataPath}{pathData}";
                    return new FileStream(file, FileMode.Open, FileAccess.Read);
                }
                else
                {
                    return new FileStream(pathData, FileMode.Open, FileAccess.Read);
                }    
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region [Gift]
        public async Task<ApiResponseData<WalletTransaction>> Gift(WalletTransactionGiftCommand model)
        {
            try
            {
                if(model.GiftFromAccountId == model.GiftToAccountId)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = "Tài khoản gửi và tài khoản nhận không được giống nhau", Status = 2 };
                }    

                var from = await _iAccountRepository.SearchOneAsync(x => x.Id == model.GiftFromAccountId);
                if (from == null)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = "Tài khoản gửi không tồn tại", Status = 0 };
                }
                var to = await _iAccountRepository.SearchOneAsync(x => x.Id == model.GiftToAccountId);
                if (to == null)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = "Tài khoản nhận không tồn tại", Status = 0 };
                }

                if (to.VerifyStatus != EAccountVerifyStatus.Ok)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = "Tài khoản nhận phải cần được xác thực mới có thể nhận được điểm", Status = 0 };
                }

                if (model.Amount < _baseSettings.MinGiftPoint)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = $"Số điểm tặng phải tối thiểu {Function.FormatMoney(_baseSettings.MinGiftPoint)}", Status = 0 };
                }

                // đang trong chuyến đi không thể chia sẻ điểm
                var kt = await _iBookingRepository.AnyAsync(x => x.AccountId == model.GiftFromAccountId && x.Status == EBookingStatus.Start);
                if (kt)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = $"Bạn đang trong chuyến đi không thể chia sẻ điểm", Status = 0 };
                }

                if (model.PointType != 0)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = "Chỉ cho phép chia sẻ tài khoản chính", Status = 0 };
                }

                var walletFrom = await _iWalletRepository.GetWalletAsync(from.Id);
                var walletTo = await _iWalletRepository.GetWalletAsync(to.Id);

                if (walletFrom.Balance < model.Amount)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = "Số điểm dư không đủ để thự hiện thao tác này", Status = 0 };
                }

                if ((walletFrom.Balance + walletFrom.SubBalance - model.Amount) < _baseSettings.MinGiftPoint)
                {
                    return new ApiResponseData<WalletTransaction>() { Message = $"Không thể chia sẻ điểm, điều kiện sau khi chia sẻ thì tài khoản phải còn ít nhất {Function.FormatMoney(_baseSettings.MinGiftPoint)}", Status = 0 };
                }

                var orderId = $"{DateTime.Now:yyyyMMddHHmmssfff}";

                // tạo log lịch sử cho tài khoản gửi
                var dlAddFrom = new WalletTransaction
                {
                    AccountId = from.Id,
                    WalletId = walletFrom.Id,
                    Type = EWalletTransactionType.GiftPoint,
                    Status = EWalletTransactionStatus.Done,
                    TotalAmount = model.Amount,
                    Amount = model.Amount,
                    SubAmount = 0,
                    CampaignId = 0,
                    DepositEvent_SubAmount = 0,
                    TripPoint = 0,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    IsAsync = false,
                    Wallet_Balance = walletFrom.Balance,
                    Wallet_SubBalance = walletFrom.SubBalance,
                    Wallet_HashCode = walletFrom.HashCode,
                    PaymentGroup = EPaymentGroup.Default,
                    PadTime = DateTime.Now,
                    OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.GiftPoint),
                    Note = $"Tặng cho tài khoản có SĐT {to.Phone} số điểm là {Functions.FormatMoney(model.Amount)}",
                    Files = model.Files,
                    AdminNote = model.Note,
                    HashCode = Function.SignSHA256((model.Amount).ToString(), _baseSettings.TransactionSecretKey),
                    GiftFromAccountId = from.Id,
                    GiftToAccountId = to.Id,
                    Wallet_TripPoint = walletFrom.TripPoint
                };
                dlAddFrom.HashCode = Function.TransactionHash(dlAddFrom.OrderIdRef, dlAddFrom.Amount, dlAddFrom.SubAmount, dlAddFrom.TripPoint, _baseSettings.TransactionSecretKey);

                // tạo log lịch sử cho tài khoản nhận
                var dlAddTo = new WalletTransaction
                {
                    AccountId = to.Id,
                    WalletId = walletTo.Id,
                    Type = EWalletTransactionType.ReceivedPoint,
                    TransactionCode = $"{to.Id:D6}-{orderId}",
                    Amount = model.Amount,
                    SubAmount = 0,
                    Status = EWalletTransactionStatus.Done,
                    TotalAmount = model.Amount,
                    CampaignId = 0,
                    DepositEvent_SubAmount = 0,
                    TripPoint = 0,
                    CreatedDate = DateTime.Now,
                    CreatedUser = UserInfo.UserId,
                    IsAsync = false,
                    Wallet_Balance = walletTo.Balance,
                    Wallet_SubBalance = walletTo.SubBalance,
                    Wallet_HashCode = walletTo.HashCode,
                    PaymentGroup = EPaymentGroup.Default,
                    PadTime = DateTime.Now,
                    OrderIdRef = Function.GenOrderId((int)EWalletTransactionType.ReceivedPoint),
                    Note = $"Nhận từ tài khoản có SĐT {from.Phone} số điểm là {Functions.FormatMoney(model.Amount)}",
                    Files = model.Files,
                    AdminNote = model.Note,
                    HashCode = Function.SignSHA256((model.Amount).ToString(), _baseSettings.TransactionSecretKey),
                    GiftFromAccountId = from.Id,
                    GiftToAccountId = to.Id,
                    Wallet_TripPoint = walletTo.TripPoint
                };
                dlAddTo.HashCode = Function.TransactionHash(dlAddTo.OrderIdRef, dlAddTo.Amount, dlAddTo.SubAmount, dlAddTo.TripPoint, _baseSettings.TransactionSecretKey);

                await _iWalletTransactionRepository.AddAsync(dlAddFrom);
                await _iWalletTransactionRepository.AddAsync(dlAddTo);
                await _iWalletTransactionRepository.Commit();
                //Trừ ví của tài khoản
                await _iWalletRepository.UpdateAddWalletAsync(from.Id, -model.Amount, 0, 0, _baseSettings.TransactionSecretKey, false, false, 0);
                await _iWalletRepository.UpdateAddWalletAsync(to.Id, model.Amount, 0, 0, _baseSettings.TransactionSecretKey, true, false, 0);
                //Gửi notify
                string formatFrom = string.Format(GetNotificationText(ENotificationTempType.TangDiemThanhCong), to.Phone, (model.PointType == 1 ? "khuyến mãi" : ""), Function.FormatMoney(model.Amount));
                string formatTo = string.Format(GetNotificationText(ENotificationTempType.NhanDiemThanhCong), from.Phone, (model.PointType == 1 ? "khuyến mãi" : ""), Function.FormatMoney(model.Amount));

                await _iNotificationJobRepository.SendNow(dlAddFrom.AccountId, "TNGO thông báo", formatFrom, UserInfo.UserId, DockEventType.Not, null);
                await _iNotificationJobRepository.SendNow(dlAddTo.AccountId, "TNGO thông báo", formatTo, UserInfo.UserId, DockEventType.Not, null);

                await AddLog(dlAddFrom.AccountId, $"Tặng điểm điểm từ tài khoản #{from.Id}({from.Phone}) đến tài khoản #{to.Id}({to.Phone}) số điểm {Functions.FormatMoney(model.Amount)}", LogType.Insert);

                return new ApiResponseData<WalletTransaction>() { Data = dlAddFrom, Status = 1, Message ="Chia sẻ thành công" };
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletTransactionSevice.Create", ex.ToString());
                return new ApiResponseData<WalletTransaction>();
            }
        }
        #endregion

        public async Task<ApiResponseData<VoucherCode>> AddGiftCard(string code, int accountId)
        {
            code = code.Trim();
            var getCode = await _iVoucherCodeRepository.SearchOneAsync(x => x.Code == code);
            if (getCode == null)
            {
                return new ApiResponseData<VoucherCode> { Status = 0, Message = "Mã khuyến mãi không tồn tại, vui lòng kiểm tra lại" };
            }

            if (getCode.ProvisoType == ECampaignProvisoType.VerifyOke)
            {
                var account = await _iAccountRepository.SearchOneAsync(x => x.Id == accountId);
                if (account == null)
                {
                    return new ApiResponseData<VoucherCode> { Status = 0, Message = "Mã khuyến mãi không tồn tại, vui lòng kiểm tra lại" };
                }

                if (account.VerifyStatus != EAccountVerifyStatus.Ok)
                {
                    return new ApiResponseData<VoucherCode> { Status = 0, Message = "Mã này chỉ sử dụng cho tài khoản đã được xác nhận" };
                }
            }
            // Kiểm tra còn không
            if (getCode.CurentLimit <= 0)
            {
                return new ApiResponseData<VoucherCode> { Status = 5, Message = "Mã này đã sử dụng hết số lần, vui lòng kiểm tra lại" };
            }
            // Kiểm tra chiến dịch tồn tại không
            var camp = await _iCampaignRepository.SearchOneAsync(x => x.Id == getCode.CampaignId && x.Status == ECampaignStatus.Active);
            if (camp == null)
            {
                return new ApiResponseData<VoucherCode> { Status = 0, Message = "Chiến dịch khuyến mãi không tồn tại, vui lòng kiểm tra lại" };
            }
            // Kiểm tra mã còn hiệu lực không
            if (!(getCode.StartDate <= DateTime.Now && getCode.EndDate.AddDays(1) > DateTime.Now))
            {
                return new ApiResponseData<VoucherCode> { Status = 2, Message = "Mã khuyến mãi chưa sử dụng được, hoặc quá hạn" };
            }
            // Kiểm tra khách hàng này đã add code cùng chiến dịch
            var isCampaign = await _iWalletTransactionRepository.AnyAsync(x => x.CampaignId == getCode.CampaignId && x.AccountId == accountId);
            if (isCampaign)
            {
                return new ApiResponseData<VoucherCode> { Status = 3, Message = $"Chiến dịch khuyến mãi '{camp.Name}' bạn đã tham gia rồi nên không thể sử dụng mã này nữa" };
            }
            // Kiểm tra xem có áp dụng với nhóm khách hàng nào không
            //if (getCode.CustomerGroupId > 0)
            //{
            //    var kt = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId && x.CustomerGroupId == getCode.CustomerGroupId);
            //    if (!kt)
            //    {
            //        return new ApiResponseData<VoucherCode> { Status = 4, Message = "Mã khuyến mãi này không áp dụng với tài khoản này, vui lòng kiểm tra lại" };
            //    }
            //}

            var findProject = new ProjectAccountModel();
            bool isOke = true;
            if (getCode.ProjectId > 0)
            {
                if (getCode.CustomerGroupId > 0)
                {
                    isOke = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId && x.CustomerGroupId == getCode.CustomerGroupId);
                }
                else
                {
                    isOke = await _iProjectAccountRepository.AnyAsync(x => x.AccountId == accountId && x.ProjectId == getCode.ProjectId);
                }
                // nếu ko đạt thì check 1 lần nữa theo tọa độ người đứng
                if (!isOke)
                {
                    findProject = await _iWalletRepository.LatLngToCustomerGroup(accountId);
                    if (getCode.CustomerGroupId > 0 && getCode.CustomerGroupId != findProject.CustomerGroupId)
                    {
                        return new ApiResponseData<VoucherCode> { Status = 4, Message = "Mã khuyến mãi này không áp dụng với tài khoản này, vui lòng kiểm tra lại" };
                    }
                    else if (getCode.CustomerGroupId <= 0 && getCode.ProjectId != findProject.ProjectId)
                    {
                        return new ApiResponseData<VoucherCode> { Status = 4, Message = "Mã khuyến mãi này không áp dụng với tài khoản này, vui lòng kiểm tra lại" };
                    }
                }
            }

            // lấy ví
            var getW = await _iWalletRepository.GetWalletAsync(accountId);
            // Thêm transaction
            // Add log
            string noteText = $"Nạp mã giảm giá {code} của chiến dịch khuyến mãi '{camp.Name}', tài khoản được +{Functions.FormatMoney(getCode.Point)} điểm vào tài khoản khuyến mãi";
            await AddLog( accountId, noteText, LogType.Insert, "WalletTransaction");

            // Tạo giao dịch log
            await _iWalletTransactionRepository.AddAsync(new WalletTransaction
            {
                AccountId = accountId,
                CampaignId = getCode.CampaignId,
                IsAsync = true,
                Note = noteText,
                OrderIdRef = null,
                CreatedDate = DateTime.Now,
                CreatedUser = UserInfo.UserId,
                TransactionId = 0,
                TransactionCode = code,
                WalletId = getW.Id,
                Status = EWalletTransactionStatus.Done,
                SubAmount = getCode.Point ?? 0,
                Amount = 0,
                TotalAmount = getCode.Point ?? 0,
                Type = EWalletTransactionType.Voucher,
                Wallet_Balance = getW.Balance,
                Wallet_SubBalance = getW.SubBalance,
                Wallet_HashCode = getW.HashCode,
                HashCode = Function.SignSHA256((getCode.Point ?? 0).ToString(), _baseSettings.TransactionSecretKey),
                VoucherCodeId = getCode.Id
            });
            await _iWalletTransactionRepository.Commit();

            // Trừ tiền trong ví
            await _iWalletRepository.UpdateAddWalletAsync(accountId, 0, getCode.Point ?? 0, 0, _baseSettings.TransactionSecretKey, true, true, 0);

            getCode.CurentLimit -= 1;
            _iVoucherCodeRepository.Update(getCode);
            await _iVoucherCodeRepository.Commit();

            await _iNotificationJobRepository.SendNow(accountId, "TNGO thông báo", string.Format(GetNotificationText(ENotificationTempType.NapMaKhuyenMaiThanhCong), $"{Function.FormatMoney(getCode.Point)}"), UserInfo.UserId, DockEventType.Not, null);

            return new ApiResponseData<VoucherCode> { Data = getCode, Status = 1, Message = $"Nhập mã giảm giá {code} của chiến dịch khuyến mãi '{camp.Name}' thành công, tài khoản được +{Functions.FormatMoney(getCode.Point)} điểm vào tài khoản khuyến mãi" };
        }


        public async Task<ApiResponseData<object>> GiftCardGetData(int accountId)
        {
            var redeemPoints = await _iRedeemPointRepository.Search();
            var voucherCodes = await _iVoucherCodeRepository.VoucherCodePublicByAccount(accountId);
            var customerGroups = await _iCustomerGroupRepository.Search(x => voucherCodes.Select(x => x.CustomerGroupId).Contains(x.Id));
            var projects = await _iProjectRepository.Search(x => voucherCodes.Select(x => x.ProjectId).Contains(x.Id));
            foreach (var item in voucherCodes)
            {
                item.CustomerGroup = customerGroups.FirstOrDefault(x => x.Id == item.CustomerGroupId);
                item.Project = projects.FirstOrDefault(x => x.Id == item.ProjectId);
            }
            var w = await _iWalletRepository.GetWalletAsync(accountId);
            return new ApiResponseData<object>
            {
                Status = 1,
                Data = new { redeemPoints, voucherCodes, totalTripPoint = w?.TripPoint }
            };
        }

        public async Task<ApiResponseData<RedeemPoint>> RedeemPointAdd(int accountId, int id)
        {
            var getVi = await _iWalletRepository.GetWalletAsync(accountId);
            var getReadeem = await _iRedeemPointRepository.SearchOneAsync(x => x.Id == id);
            if (getReadeem == null)
            {
                return new ApiResponseData<RedeemPoint> { Status = 0, Message = "Không tồn tại danh mục quy đổi" };
            }

            if (getVi.TripPoint < getReadeem.TripPoint)
            {
                return new ApiResponseData<RedeemPoint> { Status = 0, Message = $"Điểm tích chuyến đi {Functions.FormatMoney(getVi.TripPoint)} điểm không đủ để quy đổi" };
            }

            string noteText = $"Quy đổi {Functions.FormatMoney(getReadeem.TripPoint)} điểm tích chuyển đi qua {Functions.FormatMoney(getReadeem.ToPoint)} điểm vào tài khoản khuyến mãi";
            await AddLog( accountId, noteText, LogType.Insert, "WalletTransaction");
            // Cộng điểm tặng
            await _iWalletRepository.UpdateAddWalletAsync(accountId, 0, getReadeem.ToPoint, -getReadeem.TripPoint, _baseSettings.TransactionSecretKey, true, true, 0);
            // Ghi lịch sử giao dịch
            await _iWalletTransactionRepository.AddAsync(new WalletTransaction
            {
                AccountId = accountId,
                CampaignId = 0,
                IsAsync = true,
                Note = noteText,
                OrderIdRef = null,
                CreatedDate = DateTime.Now,
                CreatedUser = UserInfo.UserId,
                TransactionId = 0,
                TransactionCode = null,
                WalletId = getVi.Id,
                Status = EWalletTransactionStatus.Done,
                SubAmount = getReadeem.ToPoint,
                Amount = 0,
                TotalAmount = getReadeem.ToPoint,
                Type = EWalletTransactionType.RedeemPoint,
                Wallet_Balance = getVi.Balance,
                Wallet_HashCode = getVi.HashCode,
                Wallet_SubBalance = getVi.SubBalance,
                HashCode = Function.SignSHA256(getReadeem.ToPoint.ToString(), _baseSettings.TransactionSecretKey),
                TripPoint = getReadeem.TripPoint
            });
            await _iWalletTransactionRepository.Commit();

            await _iNotificationJobRepository.SendNow(accountId, "TNGO thông báo", string.Format(GetNotificationText(ENotificationTempType.DoiDiemTichLuyThanhCong), $"{Function.FormatMoney(getReadeem.TripPoint)}", $"{Function.FormatMoney(getReadeem.ToPoint)}"), UserInfo.UserId, DockEventType.Not, null);

            return new ApiResponseData<RedeemPoint> { Status = 1, Message = "Quy đổi điểm thành công" };
        }

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}
