﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PT.Base.Command;
using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TN.Base.Services;
using TN.Domain.Model;
using TN.Domain.Model.Common;
using TN.Infrastructure.Interfaces;
using TN.Shared;
using TN.Utility;

namespace PT.Base.Services
{
    public interface IUsersService : IService<ApplicationUser>
    {
        Task<ApiResponseData<ApplicationUser>> Create(ManagerRegisterCommand use);

        Task<ApiResponseData<ApplicationUser>> GetById(int id);

        Task<ApiResponseData<List<ApplicationUser>>> SearchPagedAsync(
            int pageIndex,
            int pageSize,
            string key,
            int? roleId,
            bool? isLock,
            bool? isRetired,
            string code,
            string sortby,
            string sorttype
            );
        Task<ApiResponseData<ApplicationUser>> Edit(ManagerRegisterCommand use);
        Task<ApiResponseData<ApplicationUser>> Delete(int id);
        Task<ApiResponseData<FileDataDTO>> UploadImage(HttpRequest request);
        Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key);
    }
    public class UsersService : IUsersService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;
        private readonly IUserRepository _aspNetUsers;
        private readonly IRoleRepository _iRoleRepository;
        private readonly IParameterRepository _iParameterRepository;
        private readonly IHubContext<CategoryHub> _hubContext;
        private readonly BaseSettings _baseSettings;
        private readonly ILogRepository _iLogRepository;
        private readonly IAccountRepository _iAccountRepository;

        public UsersService
        (
            ILogger<UsersService> logger,
            UserManager<ApplicationUser> userManager,
            IUserRepository aspNetUsers,
            IRoleRepository iRoleRepository,
            IParameterRepository iParameterRepository,
            IHubContext<CategoryHub> hubContext,
            IOptions<BaseSettings> baseSettings,
            ILogRepository iLogRepository,
            IAccountRepository iAccountRepository
        )
        {
            _logger = logger;
            _userManager = userManager;
            _aspNetUsers = aspNetUsers;
            _iRoleRepository = iRoleRepository;
            _iParameterRepository = iParameterRepository;
            _hubContext = hubContext;
            this._baseSettings = baseSettings.Value;
            _iLogRepository = iLogRepository;
            _iAccountRepository = iAccountRepository;
        }

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchPagedAsync(
            int pageIndex,
            int pageSize, 
            string key,
            int? roleId, 
            bool? isLock,
            bool? isRetired,
            string code,
            string sortby,
            string sorttype
            )
        {
            try
            {
                var data = await _aspNetUsers.SearchPagedAsync(
                 pageIndex,
                 pageSize,
                 roleId,
                 m => 
                 (m.UserName.Contains(key) || key == null || m.DisplayName.Contains(key) || m.PhoneNumber.Contains(key) || m.FullName.Contains(key) || m.IdentificationID.Contains(key)) 
                 && (m.IsLock == isLock || isLock == null)
                 && (m.IsRetired == isRetired || isRetired == null)
                 && (m.Code.Contains(code) || code == null)
                 && m.IsSuperAdmin == false
                 ,
                 OrderByExtention(sortby, sorttype));
               
                var users = await _aspNetUsers.SeachByIds(data.Data.Select(x => x.CreatedUserId ?? 0).ToList(), data.Data.Select(x => x.UpdatedUserId ?? 0).ToList(), data.Data.Where(x => x.ManagerUserId > 0).Select(x => x.ManagerUserId).ToList());

                foreach (var item in data.Data)
                {
                    item.ManagerUser = users.FirstOrDefault(x=>x.Id == item.ManagerUserId);
                    item.CreatedUserObject = users.FirstOrDefault(x => x.Id == item.CreatedUserId);
                    item.UpdatedUserObject = users.FirstOrDefault(x => x.Id == item.UpdatedUserId);
                }
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.SearchPageAsync", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }

        public async Task<ApiResponseData<ApplicationUser>> GetById(int id)
        {
            try
            {
                var data = await _aspNetUsers.SearchOneAsync(x => x.Id == id);
                if(data == null)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 0 };
                }
                var roles = await _iRoleRepository.RolesByUser(id);
                data.IsSuperAdmin = false;
                data.IsReLogin = false;
                data.LockoutEnabled = false;
                data.EmailConfirmed = false;
                data.NormalizedEmail = null;
                data.PhoneNumberConfirmed = false;
                data.Roles = roles;
                var manager = await _aspNetUsers.SearchOneAsync(x => x.Id == data.ManagerUserId);
                if(manager!=null)
                {
                    data.ManagerUser = new ApplicationUser { 
                        Id = manager.Id,
                        FullName = manager.FullName,
                        UserName = manager.UserName,
                        Sex = manager.Sex,
                        PhoneNumber = manager.PhoneNumber
                    };
                }    
                return new ApiResponseData<ApplicationUser>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.Get", ex.ToString());
                return new ApiResponseData<ApplicationUser>();
            }
        }
      
        public async Task<ApiResponseData<ApplicationUser>> Create(ManagerRegisterCommand use)
        {
            try
            {
                string defaultPassword = "Matkhau@68";
                var ktUsername = await _userManager.FindByNameAsync(use.Username);
                if (ktUsername != null)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 2, Message = "Tài khoản này đã có người sử dụng, vui lòng thử lại"};
                }

                var ktEmail = await _userManager.FindByEmailAsync(use.Email);
                if (ktEmail != null)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 3, Message = "Email này đã có người sử dụng, vui lòng thử lại" };
                }

                var role = await _iRoleRepository.SearchOneAsync(x => x.Id == use.RoleId);


                if (!string.IsNullOrEmpty(use.RFID))
                {
                    var ktRFIDAccount = await _iAccountRepository.AnyAsync(x => x.RFID == use.RFID);
                    if (ktRFIDAccount)
                    {
                        return new ApiResponseData<ApplicationUser>() { Status = 6, Message = "RFID này đã có khách hàng sử dụng, vui lòng thử lại" };
                    }

                    var ktRFID = await _aspNetUsers.AnyAsync(x => x.RFID == use.RFID);
                    if (ktRFID)
                    {
                        return new ApiResponseData<ApplicationUser>() { Status = 7, Message = "RFID này đã có quản trị người sử dụng, vui lòng thử lại" };
                    }
                }

                var user = new ApplicationUser
                {
                    UserName = use.Username,
                    Email = use.Email,
                    EmailConfirmed = true,
                    DisplayName = use.DisplayName,
                    PhoneNumber = use.PhoneNumber,
                    Note = use.Note,
                    IsLock = use.IsLock,
                    CreatedDate = DateTime.Now,
                    CreatedUserId = UserInfo.UserId,
                    Address = use.Address,
                    Birthday = use.Birthday,
                    FullName = use.FullName,
                    IdentificationAdress = use.IdentificationAdress,
                    IdentificationID = use.IdentificationID,
                    IdentificationPhotoBackside = use.IdentificationPhotoBackside,
                    IdentificationPhotoFront = use.IdentificationPhotoFront,
                    IdentificationSupplyAdress = use.IdentificationSupplyAdress,
                    IdentificationSupplyDate = use.IdentificationSupplyDate,
                    IsRetired = use.IsRetired,
                    IsReLogin= false,
                    IsSuperAdmin = false,
                    RoleId = use.RoleId,
                    RoleType = role?.Type ?? RoleManagerType.Default,
                    Sex = use.Sex,
                    WorkingDateFrom= use.WorkingDateFrom,
                    WorkingDateTo = use.WorkingDateTo,
                    ManagerUserId = use.ManagerUserId,
                    Education =use.Education,
                    RFID = use.RFID,
                    ObjectIds = use.ObjectIds
                };

                if(use.IsChangePassword)
                {
                    defaultPassword = use.Password;
                }

                var result = await _userManager.CreateAsync(user, defaultPassword);
                if (result.Succeeded)
                {
                    user.Code = user.RoleType == RoleManagerType.CTV_VTS ? $"CTVTN{user.Id:D4}" : $"NVTN{user.Id:D4}";
                    _aspNetUsers.Update(user);
                    await  _aspNetUsers.Commit();

                    var listRole = new List<int>();
                    // Cập nhật quyền tài khoản
                    if (use.RoleId > 0)
                    {
                        listRole = new List<int>() { use.RoleId };
                    }
                    await _aspNetUsers.UpdateRolesAsync(user.Id, listRole);
                    return new ApiResponseData<ApplicationUser>() { Status = 1, Message = "Tạo mới tài khoản thành công" };
                }

                await AddLog(ktEmail.Id, $"Thêm mới tài khoản $'{user.UserName}'", LogType.Insert);

                return new ApiResponseData<ApplicationUser>() { Status = 0 };
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.Create", ex.ToString());
                return new ApiResponseData<ApplicationUser>();
            }
        }

        public async Task<ApiResponseData<ApplicationUser>> Edit(ManagerRegisterCommand use)
        {
            try
            {
                var dl = await _aspNetUsers.SearchOneAsync(x => x.Id == use.Id);
                if (dl == null)
                {
                    return new ApiResponseData<ApplicationUser>() { Data = null, Status = 0 };
                }

                var ktEmail = await _aspNetUsers.AnyAsync(x=>x.Id!=x.Id && x.Email==use.Email);
                if (ktEmail)
                {
                    return new ApiResponseData<ApplicationUser>() { Status = 3, Message = "Email này đã có người sử dụng, vui lòng thử lại" };
                }

                if (!string.IsNullOrEmpty(use.RFID))
                {
                    var ktRFIDAccount = await _iAccountRepository.AnyAsync(x => x.RFID == use.RFID);
                    if (ktRFIDAccount)
                    {
                        return new ApiResponseData<ApplicationUser>() { Status = 6, Message = "RFID này đã có khách hàng sử dụng, vui lòng thử lại" };
                    }

                    var ktRFID = await _aspNetUsers.AnyAsync(x => x.RFID == use.RFID && x.Id != dl.Id);
                    if (ktRFID)
                    {
                        return new ApiResponseData<ApplicationUser>() { Status = 7, Message = "RFID này đã có quản trị người sử dụng, vui lòng thử lại" };
                    }
                }

                var role = await _iRoleRepository.SearchOneAsync(x => x.Id == use.RoleId);

                dl.Email = use.Email;
                dl.DisplayName = use.DisplayName;
                dl.PhoneNumber = use.PhoneNumber;
                dl.Note = use.Note;
                dl.IsLock = use.IsLock;
                dl.UpdatedDate = DateTime.Now;
                dl.UpdatedUserId = UserInfo.UserId;
                dl.Address = use.Address;
                dl.Birthday = use.Birthday;
                dl.FullName = use.FullName;
                dl.IdentificationAdress = use.IdentificationAdress;
                dl.IdentificationID = use.IdentificationID;
                dl.IdentificationPhotoBackside = use.IdentificationPhotoBackside;
                dl.IdentificationPhotoFront = use.IdentificationPhotoFront;
                dl.IdentificationSupplyAdress = use.IdentificationSupplyAdress;
                dl.IdentificationSupplyDate = use.IdentificationSupplyDate;
                dl.IsRetired = use.IsRetired;
                dl.RoleId = use.RoleId;
                dl.RoleType = role?.Type ?? RoleManagerType.Default;
                dl.Sex = use.Sex;
                dl.WorkingDateFrom = use.WorkingDateFrom;
                dl.WorkingDateTo = use.WorkingDateTo;
                dl.ManagerUserId = use.ManagerUserId;
                dl.Education = use.Education;
                dl.RFID = use.RFID;
                dl.ObjectIds = use.ObjectIds;
                dl.Code = dl.RoleType == RoleManagerType.CTV_VTS ? $"CTVTN{dl.Id:D4}" : $"NVTN{dl.Id:D4}";
                
                _aspNetUsers.Update(dl);
                await _aspNetUsers.Commit();

                if (use.IsChangePassword)
                {
                    await _userManager.RemovePasswordAsync(dl);
                    await _userManager.AddPasswordAsync(dl, use.Password);
                }

                var listRole = new List<int>();
                if (use.RoleId > 0)
                {
                    listRole = new List<int>() { use.RoleId  };
                }
                await _aspNetUsers.UpdateRolesAsync(dl.Id, listRole);

                await AddLog(dl.Id, $"Cập nhật tài khoản $'{dl.UserName}'", LogType.Update);

                return new ApiResponseData<ApplicationUser>() { Data = dl, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.Edit", ex.ToString());
                return new ApiResponseData<ApplicationUser>();
            }
        }

        public async Task<ApiResponseData<ApplicationUser>> Delete(int id)
        {
            try
            {
                var dl = await _aspNetUsers.SearchOneAsync(x => x.Id == id);
                if (dl == null)
                {
                    return new ApiResponseData<ApplicationUser>() { Data = null, Status = 0 };
                }
                _aspNetUsers.DeleteRoles(id);
                _aspNetUsers.Delete(dl);
                await _aspNetUsers.Commit();

                await AddLog(dl.Id, $"Xóa tài khoản $'{dl.UserName}'", LogType.Delete);

                return new ApiResponseData<ApplicationUser>() { Data = null, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("UsersSevice.Delete", ex.ToString());
                return new ApiResponseData<ApplicationUser>();
            }
        }

        private Func<IQueryable<ApplicationUser>, IOrderedQueryable<ApplicationUser>> OrderByExtention(string orderby, string ordertype)
        {
            return orderby switch
            {
                "displayName" => ordertype == "asc" ? EntityExtention<ApplicationUser>.OrderBy(m => m.OrderBy(x => x.DisplayName)) : EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.DisplayName)),
                "userName" => ordertype == "asc" ? EntityExtention<ApplicationUser>.OrderBy(m => m.OrderBy(x => x.UserName)) : EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.UserName)),
                "email" => ordertype == "asc" ? EntityExtention<ApplicationUser>.OrderBy(m => m.OrderBy(x => x.Email)) : EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.Email)),
                "phoneNumber" => ordertype == "asc" ? EntityExtention<ApplicationUser>.OrderBy(m => m.OrderBy(x => x.PhoneNumber)) : EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.PhoneNumber)),
                _ => EntityExtention<ApplicationUser>.OrderBy(m => m.OrderByDescending(x => x.Id)),
            };
        }

        public async Task<ApiResponseData<List<ApplicationUser>>> SearchByUser(string key)
        {
            try
            {
                int.TryParse(key, out int idSearch);
                var data = await _aspNetUsers.SearchTop(10, x => (key == null || key == " " || x.Id == idSearch || x.FullName.Contains(key) || x.PhoneNumber.Contains(key) || x.Email.Contains(key) || x.UserName.Contains(key)), null, x=> new ApplicationUser { 
                    Id = x.Id,
                    FullName = x.FullName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    UserName = x.UserName,
                    Sex = x.Sex,
                    Birthday = x.Birthday
                });
                return new ApiResponseData<List<ApplicationUser>>() { Data = data, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<List<ApplicationUser>>();
            }
        }

        #region [Upload file]
        public async Task<ApiResponseData<FileDataDTO>> UploadImage(HttpRequest request)
        {
            try
            {
                string[] allowedExtensions = _baseSettings.ImagesType.Split(',');
                string pathServer = $"/Data/User/{DateTime.Now.Year}/{DateTime.Now.Month:D2}/{DateTime.Now.Day:D2}";
                string path = $"{_baseSettings.PrivateDataPath}{pathServer}";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var files = request.Form.Files;

                foreach (var file in files)
                {
                    if (!allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                    {
                        return new ApiResponseData<FileDataDTO> { Data = new FileDataDTO(), Status = 2 };
                    }
                    else if (_baseSettings.ImagesMaxSize < file.Length)
                    {
                        return new ApiResponseData<FileDataDTO> { Data = new FileDataDTO(), Status = 3 };
                    }
                }

                var outData = new FileDataDTO();

                foreach (var file in files)
                {
                    var newFilename = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";

                    string pathFile = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                    pathFile = $"{path}/{newFilename}".Replace("-","");
                    pathServer = $"{pathServer}/{newFilename}".Replace("-", "");

                    using var stream = new FileStream(pathFile, FileMode.Create);
                    await file.CopyToAsync(stream);

                    outData = new FileDataDTO { 
                        CreatedDate = DateTime.Now, 
                        CreatedUser = UserInfo.UserId, 
                        FileName = Path.GetFileName(file.FileName), 
                        Path = pathServer, 
                        FileSize = file.Length,
                        PathView = $"{(request.IsHttps ? "https://" : "http://")}{request.Host}{pathServer}"
                    };
                }
                return new ApiResponseData<FileDataDTO>() { Data = outData, Status = 1 };
            }
            catch (Exception ex)
            {
                _logger.LogError("TicketPrepaidSevice.SearchByAccount", ex.ToString());
                return new ApiResponseData<FileDataDTO>();
            }
        }
        #endregion

        private async Task AddLog(int objectId, string action, LogType type)
        {
            await _iLogRepository.AddAsync(
                new TN.Domain.Model.Log
                {
                    Action = action,
                    CreatedDate = DateTime.Now,
                    Object = AppHttpContext.Current.Request.RouteValues["controller"].ToString(),
                    ObjectId = objectId,
                    ObjectType = $"{AppHttpContext.Current.Request.RouteValues["area"]}.{AppHttpContext.Current.Request.RouteValues["controller"]}.{AppHttpContext.Current.Request.RouteValues["action"]}",
                    SystemUserId = UserInfo.UserId,
                    Type = type
                });
            await _iLogRepository.Commit();
        }
    }
}
