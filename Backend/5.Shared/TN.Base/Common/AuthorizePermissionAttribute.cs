﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PT.Domain.Model;
using TN.Domain.Model;

namespace PT.Base
{
    public class Functions
    {
        public static string FormatMoney(decimal? number)
        {
            if (number == null)
            {
                return "";
            }
            if (number < 1000)
            {
                return Convert.ToInt32(number).ToString();
            }
            var culture = CultureInfo.GetCultureInfo("vi-VN");
            return string.Format(culture, "{0:00,0}", number);
        }
    }

    public static class LabelExtensions
    {
        public static string ToEnumGetDisplayName(this Enum value)
        {
            try
            {
                Type enumType = value.GetType();
                var enumValue = Enum.GetName(enumType, value);
                MemberInfo member = enumType.GetMember(enumValue)[0];
                var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
                var outString = ((DisplayAttribute)attrs[0]).Name;

                if (((DisplayAttribute)attrs[0]).ResourceType != null)
                {
                    outString = ((DisplayAttribute)attrs[0]).GetName();
                }
                return string.Format("{0}", outString);

            }
            catch
            {
                return value.ToString();
            }
        }
    }
     public class RoleType
    {
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    }

    public class AuthorizePermissionAttribute : TypeFilterAttribute
    {
        public AuthorizePermissionAttribute(string action = "Index", string controller = null, string area = null)
            : base(typeof(PermissionFilter))
        {
            Arguments = new object[] {new RoleType {Action = action, Area = area, Controller = controller}};
        }
    }


    public class PermissionFilter : Attribute, IAsyncAuthorizationFilter
    {
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly RoleType _roleType;

        public PermissionFilter(UserManager<ApplicationUser> userManager, RoleType roleType)
        {
            _userManager = userManager;
            _roleType = roleType;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var listAction = new List<string>();
            var area = "Null";

            var controller = _roleType.Controller ?? context.RouteData.Values["controller"].ToString();
            var action = _roleType.Action ?? context.RouteData.Values["action"].ToString();
            if (_roleType.Area != null)
            {
                area = _roleType.Area;
            }
            else
            {
                if (context.RouteData.Values.TryGetValue("area", out var areaObj))
                    area = _roleType.Area ?? areaObj.ToString();
            }

            if (action != null) listAction = action.Split("|").ToList();
            var userId = Convert.ToInt32(context.HttpContext.User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value);
            if (userId == 0)
            {
                context.Result = new ForbidResult();
                return;
            }

            var user = await _userManager.FindByIdAsync(userId.ToString());
            // Không tồn tại
            if (user == null)
            {
                context.Result = new ForbidResult();
                return;
            }

            if (user.IsLock && !user.IsSuperAdmin)
            {
                context.Result = new ForbidResult();
                return;
            }

            if (user.IsReLogin)
            {
                context.Result = new ForbidResult();
                return;
            }

            if (!user.IsSuperAdmin)
            {
                //Check quyền
                var listRoleUser = JsonConvert.DeserializeObject<List<DataRoleActionModel>>(
                    context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "RoleActions")?.Value ??
                    string.Empty);
                if (listRoleUser.Any(m => listAction.Contains(m.ActionName) && m.AreaName == area && m.ControllerName == controller)) 
                    return;
                context.Result = new UnauthorizedResult();
            }
        }
    }

    public class AppVerificationAttribute : TypeFilterAttribute
    {
        public AppVerificationAttribute(): base(typeof(AppVerificationFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class AppVerificationFilter : Attribute, IAuthorizationFilter
    {
        private readonly ApiSettings _apiSetting;
        public AppVerificationFilter(IOptions<ApiSettings> apiSetting)
        {
            _apiSetting = apiSetting.Value;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var origin = context.HttpContext.Request.Headers["Origin"].ToString();
            var allowOrigins = _apiSetting.UseCors.Split(',').ToList();
            if (!allowOrigins.Any(x => x == origin))
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}