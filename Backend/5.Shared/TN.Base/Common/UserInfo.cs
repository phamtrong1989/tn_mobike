﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using TN.Domain.Model;
using TN.Shared;

namespace PT.Base
{
    public class UserInfo
    {
        public static int UserId
        {
            get
            {
                return Convert.ToInt32(AppHttpContext.Current.User.FindFirst(JwtRegisteredClaimNames.Sid)?.Value);
            }
        }

        public static int ObjectId
        {
            get
            {
                return Convert.ToInt32(AppHttpContext.Current.User.FindFirst("ObjectId")?.Value);
            }
        }

        public static string ObjectIds
        {
            get
            {
                return AppHttpContext.Current.User.FindFirst("ObjectIds")?.Value;
            }
        }

        public static int InvesterId
        {
            get
            {
                try
                {
                    var data = ObjectIds.Split(',').ToList();
                    if (data.Any())
                    {
                        return Convert.ToInt32(data[0]);
                    }
                    return 0;
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static bool IsSuperadmin
        {
            get
            {
                if(AppHttpContext.Current.User.FindFirst("IsSuperadmin")==null)
                {
                    return false;
                }
                return Convert.ToBoolean(AppHttpContext.Current.User.FindFirst("IsSuperadmin")?.Value);
            }
        }
        public static RoleManagerType RoleType
        {
            get
            {
                if (AppHttpContext.Current.User.FindFirst("RoleType") == null)
                {
                    return RoleManagerType.Admin;
                }
                return (RoleManagerType)Convert.ToInt32(AppHttpContext.Current.User.FindFirst("RoleType")?.Value);
            }
        }
    }
}
