﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
namespace PT.Base
{
    public class CategoryHub : Hub
    {
        public async Task SendMessageUpdateCategory(string type)
        {
            await Clients.All.SendAsync("ReceiveMessageUpdateCategory", type);
        }
    }

    public class EventSystemHub : Hub
    {
        public async Task SendMessage(int userId, object data)
        {
            await Clients.All.SendAsync("ReceiveMessageEventSystemSendMessage", userId, data);
        }
    }
}
