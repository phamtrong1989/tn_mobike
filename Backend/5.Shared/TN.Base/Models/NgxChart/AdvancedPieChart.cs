﻿using System.Collections.Generic;

namespace TN.Base.Models.NgxChart
{
    public class ChartSingleData
    {
        public string name { get; set; }
        public double value { get; set; }
    }

    public class ChartMultiData
    {
        public string name { get; set; }
        public List<ChartSingleData> series { get; set; }
    }

    public class AdvancedPieChart : ChartSingleData
    {
    }

    public class StackedAreaChart : ChartMultiData
    {
    }
}