﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace PT.Domain.Model
{
    public class AccountProfilesDTO
    {
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Avatar { get; set; }
   
        public AccountProfilesDTO(ApplicationUser user)
        {
            DisplayName = user.DisplayName;
            Email = user.Email;
            PhoneNumber = user.PhoneNumber;
            Avatar = user.Avatar;
        }
    }
}
