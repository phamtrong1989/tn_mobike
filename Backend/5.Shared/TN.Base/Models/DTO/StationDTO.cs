﻿using System;
using System.Collections.Generic;
using TN.Domain.Model;
using static TN.Domain.Model.Bike;

namespace PT.Domain.Model
{
    public class StationDTO
    {
        public int Id { get; set; }
        public int? InvestorId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string Description { get; set; }
        public ESationStatus Status { get; set; }
        public int TotalDock { get; set; } = 0;
        public int TotalDockFree { get; set; } = 0;
        public List<DockDTO> Docks { get; set; } = new List<DockDTO>();

        public StationDTO(Station data, List<DockDTO> docks = null, int totalDock = 0, int totalDockFree = 0)
        {
            Id = data.Id;
            InvestorId = data.InvestorId;
            Name = data.Name;
            Address = data.Address;
            Lat = data.Lat;
            Lng = data.Lng;
            Description = data.Description;
            Status = data.Status;
            TotalDock = totalDock;
            TotalDockFree = totalDockFree;
            Docks = docks;
        }
    }

    public class DockDTO
    {
        public int Id { get; set; }
        public int? TenantId { get; set; }
        public int? StationId { get; set; }
        public int? BikeId { get; set; }
        public string IMEI { get; set; }
        public string SerialNumber { get; set; }
        public string Model { get; set; }
        public string SIM { get; set; }
        public EBikeStatus Status { get; set; }
        public bool? LockStatus { get; set; }
        public double? Battery { get; set; }
        public bool? Charging { get; set; }
        public bool? ConnectionStatus { get; set; }
        public DateTime? LastConnectionTime { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
        public BikeDTO Bike { get; set; }

        public DockDTO(Dock model, object tenant, BikeDTO bike)
        {
            Id = model.Id;
            TenantId = model.InvestorId;
            StationId = model.StationId;
            IMEI = model.IMEI;
            SerialNumber = model.SerialNumber;
            SIM = model.SIM;
            Status = model.Status;
            LockStatus = model.LockStatus;
            Battery = model.Battery;
            Charging = model.Charging;
            ConnectionStatus = model.ConnectionStatus;
            LastConnectionTime = model.LastConnectionTime;
            Lat = model.Lat;
            Long = model.Long;
            Bike = bike;
        }
    }

    public class BikeDTO
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string IMEI { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public EBikeStatus Status { get; set; }
        public EBikeType Type { get; set; }
        public string Description { get; set; }
        public BikeDTO(Bike model)
        {
            Id = model.Id;
            TenantId = model.ProjectId;
            SerialNumber = model.SerialNumber;
            Status = model.Status;
            Type = model.Type;
            Description = model.Description;
        }
    }
}
