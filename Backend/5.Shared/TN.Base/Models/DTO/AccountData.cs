﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace PT.Domain.Model
{
    public class FileDataDTO
    {
        public string Path { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public long FileSize { get; set; }
        public string PathView { get; set; }
    }

    public class AccountData
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string RoleName { get; set; }
        public string Avartar { get; set; }
        public string Token { get; set; }
        public string TokenHub { get; set; }
        public bool IsSuperAdmin { get; set; }
        public RoleManagerType RoleType { get; set; }
        public List<DataRoleActionModel> RoleActions { get; set; }
        
        public AccountData(ApplicationUser user, RoleManagerType roleType, List<DataRoleActionModel> roleActions, string token, string tokenHub)
        {
            Id = user.Id;
            Token = token;
            TokenHub = tokenHub;
            DisplayName = user.DisplayName;
            Email = user.Email;
            PhoneNumber = user.PhoneNumber;
            RoleName = roleType.GetDisplayName();
            Avartar = user.Avatar;
            RoleType = roleType;
            RoleActions = roleActions;
            IsSuperAdmin = user.IsSuperAdmin;
        }
    }
}
