using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public  class BannerCommand
    {
       public int Id { get; set; }
        public int BannerGroupId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Data { get; set; }
        public EBannerType Type { get; set; }
        public EBannerStatus Status { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Description { get; set; }
        public int LanguageId { get; set; }
    }
}
