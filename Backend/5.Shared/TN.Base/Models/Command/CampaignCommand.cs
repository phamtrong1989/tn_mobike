using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class CampaignCommand
    {
        public int Id { get; set; }
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        [Required]
        [StringLength(200)]
        public string Name { get; set; }
        public ECampaignType? Type { get; set; }
        public ECampaignStatus Status { get; set; }
        [StringLength(4000)]
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
