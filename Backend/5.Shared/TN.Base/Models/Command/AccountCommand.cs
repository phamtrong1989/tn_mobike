using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static TN.Domain.Model.Account;

namespace TN.Base.Command
{
    public class AdminMenuDTO
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string RouterLink { get; set; }
        public string Href { get; set; }
        public string Icon { get; set; }
        public string Target { get; set; }
        public bool HasSubMenu { get; set; }
        public string RoleAcctionIds { get; set; }
        public bool IsActive { get; set; }
        public int ActionId { get; set; }
    }
    public class EditCustomerGroupCommand
    {
        public int ProjectId { get; set; }
        public int CustomerGroupId { get; set; }
    }
    public class AccountEditCommand
    {
        public int Id { get; set; }

        [StringLength(200, MinimumLength = 0)]
        public string RFID { get; set; }

        [StringLength(12, MinimumLength = 0)]
        public string Password { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public ESex Sex { get; set; }

        [StringLength(100)]
        public string FullName { get; set; }

        public DateTime? Birthday { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 9)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        public EAccountStatus Status { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        public EAccountType Type { get; set; }

        public int LanguageId { get; set; }

        public int MaxBookingBike { get; set; }
    }

    public class AccountEditCommand2
    {
        public int Id { get; set; }
        public int UserId { get; set; }
    }


    public class AccountVerifyCommand
    {
        public int Id { get; set; }

        [Required]
        public EAccountIdentificationType IdentificationType { get; set; }

        [StringLength(50)]
        public string IdentificationID { get; set; }

        [StringLength(500)]
        public string IdentificationPhotoFront { get; set; }

        [StringLength(500)]
        public string IdentificationPhotoBackside { get; set; }

        public EAccountVerifyStatus VerifyStatus { get; set; }

        [StringLength(1000)]
        public string VerifyNote { get; set; }
    }
}
