using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class LatLngModel
    {
        public double Lat { get; set; }
        public double Lng { get; set; }

    }
    public class BookingCancelCommand
    {
        public int BookingId { get; set; }
        public bool CheckCloseDock { get; set; }
        public bool CheckInStation { get; set; }
        public string Feedback { get; set; }
    }

    public class BookingAddCommand
    {
        public int Id { get; set; }
        public string Note { get; set; }
    }

    public class TransctionRefundCommand
    {
        public int Id { get; set; }
        public string Note { get; set; }
    }

    public class BookingEndCommand
    {
        public int BookingId { get; set; }
        public string Feedback { get; set; }
        public bool CheckCloseDock { get; set; }
        public bool CheckInStation { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class RFIDBookingEndCommand
    {
        public int RFIDBookingId { get; set; }
        public string Feedback { get; set; }
        public bool CheckCloseDock { get; set; }
        public bool CheckInStation { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class ReCallCommand
    {
        public int BookingId { get; set; }
        [Required]
        [StringLength(500)]
        public string Feedback { get; set; }
        public bool CheckCloseDock { get; set; }
    }

    public class BookingOpenLockCommand
    {
        public int DockId { get; set; }
        public string Feedback { get; set; }
    }

    public class BookingCommand
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public int StationIN { get; set; }
        public int? StationOut { get; set; }
        public int BikeId { get; set; }
        public int DockId { get; set; }
        public int AccountId { get; set; }
        public int CustomerGroupId { get; set; }
        public string TransactionCode { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartDate { get; set; }
        public byte Vote { get; set; }
        public string Feedback { get; set; }
        public ETicketType TicketType { get; set; }
        public decimal TicketValue { get; set; }
        public EBookingStatus Status { get; set; }
        public decimal? AccountBlance { get; set; }
        public decimal SubAccountBlance { get; set; }
        public decimal ChargeInAccount { get; set; }
        public bool AllowChargeInSubAccount { get; set; }
        public decimal ChargeInSubAccount { get; set; }
        public int TicketPriceId { get; set; }
        public int BlockPerMinute { get; set; }
        public int BlockPerViolation { get; set; }
        public decimal BlockViolationValue { get; set; }
        public int LimitMinutes { get; set; }
        public int TotalMinutes { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime? DateOfPayment { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
