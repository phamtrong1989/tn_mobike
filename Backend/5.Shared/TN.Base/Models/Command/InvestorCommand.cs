using TN.Domain.Model;
using System;

namespace TN.Base.Command
{
    public class InvestorCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public EInvestorStatus Status { get; set; }
    }
}
