using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class RoleAreaCommand
    {
        [Required]
        [MaxLength(200)]
        public string Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        public int Order { get; set; }
        public bool IsShow { get; set; }
    }
}
