using TN.Domain.Model;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static TN.Domain.Model.Bike;
using System;

namespace TN.Base.Command
{
    public class BikeCommand
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int InvestorId { get; set; }
        public int ModelId { get; set; }
        public int ProducerId { get; set; }
        public int ColorId { get; set; }
        public int WarehouseId { get; set; }
        public string Images { get; set; }

        [Required]
        [MaxLength(50)]
        public string Plate { get; set; }
        [MaxLength(50)]
        public string SerialNumber { get; set; }
        public EBikeStatus Status { get; set; }
        public EBikeType Type { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }

        public double? Lat { get; set; }
        public double? Long { get; set; }
        public int StationId { get; set; }
        public int DockId { get; set; }
        public string Note { get; set; }
        public bool IsSetLocation { get; set; }
        public DateTime? StartDate { get;  set; }
        public DateTime? ProductionDate { get;  set; }

        public int MinuteRequiredMaintenance { get; set; }
    }
}
