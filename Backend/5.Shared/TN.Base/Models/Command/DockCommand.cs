using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static TN.Domain.Model.Bike;

namespace TN.Base.Command
{
    public class DockOpenManualCommand
    {
        public int DockId { get; set; }
        public ECommandType CommandType { get; set; }
        public string Note { get; set; }
    }

    public class DockCommand
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int InvestorId { get; set; }
        public int StationId { get; set; }
        public int ModelId { get; set; }
        public int ProducerId { get; set; }
        public int WarehouseId { get; set; }

        [Required]
        [MaxLength(50)]
        public string IMEI { get; set; }

        [MaxLength(50)]
        public string SerialNumber { get; set; }

        [MaxLength(100)]
        public string Model { get; set; }

        public EBikeStatus Status { get; set; }

        [MaxLength(20)]
        public string SIM { get; set; }

        public bool LockStatus { get; set; }

        public float Battery { get; set; }

        public bool Charging { get; set; }

        public bool ConnectionStatus { get; set; }

        public DateTime? LastConnectionTime { get; set; }

        public EDockBookingStatus BookingStatus { get; set; }

        public float? Lat { get; set; }
        public float? Long { get; set; }
        public  string Note { get; set; }
    }

    public class BikeEditLocationCommand
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public float? Lat { get; set; }
        public float? Lng { get; set; }
    }
}
