using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class TicketPriceCommand
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        public int ProjectId { get; set; }
        public int CustomerGroupId { get; set; }
        public ETicketType TicketType { get; set; }
        public bool AllowChargeInSubAccount { get; set; }
        public decimal TicketValue { get; set; }
        public int? BlockPerMinute { get; set; }
        public int? BlockPerViolation { get; set; }
        public decimal? BlockViolationValue { get; set; }
        public int LimitMinutes { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Note { get;  set; }
    }
}
