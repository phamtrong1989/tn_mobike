﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PT.Base.Models
{
    public class ChangePasswordCommand
    {
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Mật khẩu hiện tại")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "{0} không được để trống!")]
        [StringLength(15, ErrorMessage = "{0} có độ dài từ {2} đến {1} ký tự!", MinimumLength = 5)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{5,15}$", ErrorMessage = "{0} phải từ 5 đến 15 ký tự, chữ cái đầu là viết hoa, và phải có ký tự đặc biệt và số.")]
        [Display(Name = "Mật khẩu mới")]
        public string NewPassword { get; set; }

        [Display(Name = "Nhập lại mật khẩu mới")]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu mới và mật khẩu xác nhận không khớp!")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        public string ConfirmPassword { get; set; }
    }
}