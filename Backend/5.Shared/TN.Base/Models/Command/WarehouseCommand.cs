using TN.Domain.Model;

namespace TN.Base.Command
{
    public  class WarehouseCommand
    {
       public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public EWarehouseType Type { get; set; }
        public int EmployeeId { get; set; }
    }
}