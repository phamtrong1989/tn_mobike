using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class VoucherCodeCommand
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int CustomerGroupId { get; set; }

        public int CampaignId { get; set; }

        [Required]
        public string Code { get; set; }

        public EVoucherCodeType Type { get; set; }

        [Required]
        [Range(1,10000)]
        public int Limit { get; set; }

        public DateTime CreatedDate { get; set; }

        [Range(1000, 10000000)]
        public decimal? Point { get; set; }

        public bool Stacked { get;  set; }

        public bool Public { get;  set; }

        public ECampaignProvisoType ProvisoType { get; set; } = ECampaignProvisoType.Default;
    }

    public class VoucherCodeEditCommand
    {
        public int Id { get; set; }
        public int CustomerGroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [Required]
        [Range(1, 10000)]
        public int Limit { get; set; }
        [Range(1000, 10000000)]
        public decimal? Point { get; set; }
        public bool Public { get; set; }
        public ECampaignProvisoType ProvisoType { get;  set; }
    }
}
