using System;

namespace TN.Base.Command
{
    public class AccountRatingItemCommand
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int ProjectId { get; set; }
        public int Order { get; set; }
        public int AccountId { get; set; }
        public decimal Value { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool Status { get; set; }
    }
}
