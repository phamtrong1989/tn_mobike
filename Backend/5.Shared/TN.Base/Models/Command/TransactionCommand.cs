using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public  class TransactionCommand
    {
       public int Id { get; set; }
public int TenantId { get; set; }
public int StationIN { get; set; }
public int? StationOut { get; set; }
public int BikeId { get; set; }
public int DockId { get; set; }
public int AccountId { get; set; }
public int CustomerGroupId { get; set; }
public string TransactionCode { get; set; }
public DateTime? EndDate { get; set; }
public DateTime? StartDate { get; set; }
public byte Vote { get; set; }
public string Feedback { get; set; }
public string VoucherCode { get; set; }
public byte TicketType { get; set; }
public decimal TicketValue { get; set; }
public int Status { get; set; }
public decimal AccountBlance { get; set; }
public decimal SubAccountBlance { get; set; }
public decimal ChargeInAccount { get; set; }
public bool AllowChargeInSubAccount { get; set; }
public decimal ChargeInSubAccount { get; set; }
public int TicketPriceId { get; set; }
public int BlockPerMinute { get; set; }
public int BlockPerViolation { get; set; }
public decimal BlockViolationValue { get; set; }
public int LimitMinutes { get; set; }
public int TotalMinutes { get; set; }
public decimal TotalPrice { get; set; }
        public DateTime? DateOfPayment { get; set; }
public DateTime CreatedDate { get; set; }
public int SourceType { get; set; }
public bool IsDefault { get; set; }

    }
}
