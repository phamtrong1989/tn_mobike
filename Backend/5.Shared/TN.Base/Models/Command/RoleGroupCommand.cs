using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class RoleGroupCommand
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool IsShow { get; set; }
        public string Icon { get;  set; }
    }
}
