using System;

namespace TN.Base.Command
{
    public class ImportBillItemCommand
    {
        public int Id { get; set; }
        public int ImportBillId { get; set; }
        public int SuppliesId { get; set; }
        public int Count { get; set; }
        public float Price { get; set; }
        public float Total { get; set; }
        public string Note { get; set; }
        public string Files { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
    }
}