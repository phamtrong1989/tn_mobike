using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class WalletTransactionCommand
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public EWalletTransactionType Type { get; set; }
        public EPaymentGroup PaymentGroup { get; set; }
        [MaxLength(50)]
        public string TransactionCode { get; set; }
        public decimal Amount { get; set; }
        public decimal SubAmount { get; set; }
        [MaxLength(1000)]
        public string Note { get; set; }
        public string Files { get; set; }
    }

    public class WalletTransactionArrearsCommand
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public EPaymentGroup PaymentGroup { get; set; }
        [MaxLength(50)]
        public string TransactionCode { get; set; }
        public decimal Amount { get; set; }
        public decimal SubAmount { get; set; }
        [MaxLength(1000)]
        public string Note { get; set; }
        public string Files { get; set; }
    }

    public class WalletTransactionEditCommand
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string TransactionCode { get; set; }

        [MaxLength(1000)]
        public string AdminNote { get; set; }
        public string Files { get; set; }

    }

    public class WalletTransactionGiftCommand
    {
        [Required]
        public int PointType { get; set; }

        [Required]
        public int GiftFromAccountId { get; set; }

        [Required]
        public int GiftToAccountId { get; set; }

        [Required]
        [Range(1000, 1000000)]
        public decimal Amount { get; set; }

        [MaxLength(1000)]
        public string Note { get; set; }

        public string Files { get; set; }
    }
}
