﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TN.Domain.Model;

namespace PT.Base.Command
{
    public  class RoleCommand
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100, MinimumLength =5)]
        public string Name { get; set; }
        [Required]
        public RoleManagerType? Type { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }
        public List<RoleAction> RoleActions { get; set; }
    }
}
