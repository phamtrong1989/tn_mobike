using PT.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TN.Domain.Model;

namespace PT.Base.Command
{
    public class CustomerGroupCommand
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Note { get; set; }
        public int Order { get; set; }
        public ECustomerGroupStatus Status { get; set; } = ECustomerGroupStatus.Inactive;
        public bool IsDefault { get; set; }
    
    }
}