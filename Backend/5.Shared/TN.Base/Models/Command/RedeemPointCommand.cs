using System;

namespace TN.Base.Command
{
    public class RedeemPointCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal TripPoint { get; set; }
        public decimal ToPoint { get; set; }
        public string Note { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}