﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PT.Base.Models
{
    public class AccountChangePasswordCommand
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [StringLength(12, ErrorMessage = "{0} có độ dài từ {2} đến {1} ký tự!", MinimumLength = 6)]
        [Display(Name = "Mật khẩu mới")]
        public string NewPassword { get; set; }

        [Display(Name = "Nhập lại mật khẩu mới")]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu mới và mật khẩu xác nhận không khớp!")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        public string ConfirmPassword { get; set; }
    }
}