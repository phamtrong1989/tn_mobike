using TN.Domain.Model;

namespace TN.Base.Command
{
    public class BannerGroupCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public EBannerGroupType Type { get; set; }
        public EBannerGroupStatus Status { get; set; }
        public string Description { get; set; }
        public int LanguageId { get; set; }
        public string Language { get; set; }
    }
}
