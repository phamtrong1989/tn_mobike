using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Model;

namespace TN.Base.Command
{
    public class BikeReportCommand
    {
        public int Id { get; set; }

        public EBikeReportType Type { get; set; }

        public string Description { get; set; }

        public int AccountId { get; set; }

        [Required]
        [StringLength(2000)]
        public string Note { get; set; }

        public bool? Status { get; set; }

        public string File { get; set; }
        public string Files { get;  set; }
    }
}
