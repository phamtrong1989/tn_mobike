using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public  class MaintenanceRepairItemCommand
    {
       public int Id { get; set; }
       public int MaintenanceRepairId { get; set; }
       public int BikeId { get; set; }
       public int ProjectId { get; set; }
       public int InvestorId { get; set; }
       public EMaintenanceRepairItemType Type { get; set; }
       public bool Status { get; set; }
       public string Descriptions { get; set; }
       public string Files { get; set; }
       public DateTime? CompleteDate { get; set; }
       public int SuppliesId { get; set; }
       public string SuppliesCode { get; set; }
       public int SuppliesQuantity { get; set; }
       public int EmployeeId { get; set; }
    }
}
