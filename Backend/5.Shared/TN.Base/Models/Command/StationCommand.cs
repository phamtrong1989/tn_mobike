using TN.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace TN.Base.Command
{
    public class StationCommand
    {
        public int Id { get; set; }
        [Required]
        public int InvestorId { get; set; }
        [Required]
        public int ProjectId { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Spaces { get; set; }
        [StringLength(1000)]
        public string Address { get; set; }
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string DisplayName { get; set; }

        public double Lat { get; set; }
        public double Lng { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }
        public ESationStatus Status { get; set; }
        public int ManagerUserId { get;  set; }
        [Required]
        public double ReturnDistance { get;  set; }
    }
}
