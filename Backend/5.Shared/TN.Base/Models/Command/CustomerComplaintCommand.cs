using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class CustomerComplaintCommand
    {
        public int Id { get; set; }

        [Required]
        public int AccountId { get; set; }

        public string TransactionCode { get; set; }

        public int BikeId { get; set; }

        [Required]
        public ECustomerComplaintType BikeReportType { get; set; }
        [Required]
        public ECustomerComplaintPattern Pattern { get; set; }

        [Required]
        public string Description { get; set; }

        public string Note { get; set; }

        [Required]
        public DateTime ComplaintDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string Files { get; set; }

        public ECustomerComplaintStatus? Status { get; set; }

    }
}
