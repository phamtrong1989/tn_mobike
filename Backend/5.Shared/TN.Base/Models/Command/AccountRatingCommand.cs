using System;
using TN.Domain.Model;

namespace TN.Base.Command
{
    public class AccountRatingCommand
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Reward { get; set; }
        public EAccountRatingType Type { get; set; }
        public EAccountRatingGroup Group { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int ProjectId { get; set; }
    }
}
