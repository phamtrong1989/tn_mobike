using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class ExportBillItemCommand
    {
        public int Id { get; set; }
        [Required]
        public int ExportBillId { get; set; }
        [Required]
        public int SuppliesId { get; set; }
        [Required]
        public int Count { get; set; }
        public double Price { get; set; }
        public decimal Total { get; set; }
        public string Note { get; set; }
    }
}
