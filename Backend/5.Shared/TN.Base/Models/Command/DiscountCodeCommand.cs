﻿using TN.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace TN.Base.Command
{
    public class DiscountCodeCommand
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int CustomerGroupId { get; set; }

        public int CampaignId { get; set; }

        [MaxLength(20)]
        public string Code { get; set; }

        public EDiscountCodeType Type { get; set; }

        public int Limit { get; set; }

        public decimal Point { get; set; }

        public bool Public { get; set; }

        public ECampaignProvisoType ProvisoType { get; set; } = ECampaignProvisoType.Default;
        public int? BeneficiaryAccountId { get;  set; }
        public int Percent { get;  set; }
    }
}
