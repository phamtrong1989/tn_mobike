﻿using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class TicketPrepaidCommand
    {
        public int Id { get; set; }

        [Required]
        public int ProjectId { get; set; }

        [Required]
        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        [Required]
        public int TicketPriceId { get; set; }

        [Required]
        public DateTime DateOfPayment { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public int Month { get; set; }

        [Required]
        public int Year { get; set; }

        public string Note { get; set; }
    }
    public class TicketPrepaidEditCommand
    {
        public int Id { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public int TicketPriceId { get; set; }
        [Required]
        public DateTime DateOfPayment { get; set; }
        public string Note { get; set; }
    }
}
