using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class ProjectAccountCommand
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int InvestorId { get; set; }
        public int AccountId { get; set; }
        public int CustomerGroupId { get; set; }
        public EProjectAccountSourceType SourceType { get; set; }
        public DateTime? DateImport { get; set; }
    }
}
