using System;
using System.ComponentModel.DataAnnotations;

namespace PT.Base.Command
{
    public class DiscountCommand 
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public string CustomerGroupIds { get; set; }
        public double DiscountAmount { get; set; }
        public int DiscountUnitType { get; set; }
        public string Url { get; set; }
        public string Note { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
    }
}
