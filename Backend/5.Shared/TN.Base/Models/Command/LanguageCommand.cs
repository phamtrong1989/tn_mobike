using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class LanguageCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code1 { get; set; }
        public string Code2 { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public string Flag1 { get; set; }
        public string Flag2 { get; set; }

    }
}
