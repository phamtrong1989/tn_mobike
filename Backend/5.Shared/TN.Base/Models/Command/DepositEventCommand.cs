using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class DepositEventCommand
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int InvestorId { get; set; }
        public int CustomerGroupId { get; set; }

        [Required]
        public int CampaignId { get; set; }

        public string TransactionCode { get; set; }

        [Required]
        [Range(10000,99000000)]
        public decimal StartAmount { get; set; }

        public decimal Point { get; set; }

        public EDepositEventType Type { get; set; }

        public int Percent { get; set; }

        public int Limit { get; set; }

        public ECampaignProvisoType ProvisoType { get;  set; }

        public decimal MaxAmount { get; set; }

        public decimal SubPoint { get; set; }

        public int SubPercent { get; set; }
        public int SubPointExpiry { get;  set; }
    }
}
