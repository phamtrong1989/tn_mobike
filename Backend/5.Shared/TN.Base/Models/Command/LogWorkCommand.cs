﻿using TN.Domain.Model;
using TN.Domain.Model.Work;

namespace TN.Base.Models.Command
{
    public class LogWorkCommand
    {
        public long Id { get; set; }
        public EShift Shift { get; set; }
        public RoleManagerType Role { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Files { get; set; }
    }
}