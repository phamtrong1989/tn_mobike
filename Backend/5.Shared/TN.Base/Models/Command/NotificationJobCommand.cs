﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TN.Base.Command
{
    public class NotificationJobCommand
    {

        public int Id { get; set; }
        public int AccountId { get; set; }
        public string DeviceId { get; set; }
        public string Icon { get; set; }

        [StringLength(200)]
        public string Tile { get; set; }

        [StringLength(500)]
        public string Message { get; set; }

        public DateTime PublicDate { get; set; }
        public string PublicTime { get; set; }
        public int LanguageId { get; set; }
    }
}
