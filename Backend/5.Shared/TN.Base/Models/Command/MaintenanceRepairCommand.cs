using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class MaintenanceRepairCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public string Files { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public EMaintenanceRepairStatus Status { get; set; }
        public int? WarehouseId { get; set; }
        public string LastDescriptions { get;  set; }
        public string LastFiles { get;  set; }
    }
}
