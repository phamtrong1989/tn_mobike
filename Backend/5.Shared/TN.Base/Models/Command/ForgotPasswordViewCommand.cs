﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PT.Domain.Model
{
    public class ForgotPasswordViewCommand
    {
        [Required(ErrorMessage = "{0} không được để trống!")]
        [EmailAddress(ErrorMessage = "Không đúng định dạng email")]
        public string Email { get; set; }
        public bool IsSendEmail { get; set; }
        public string Capcha { get; set; }
        public string CallbackUrl { get; set; }
        public string Language { get; set; } = "vi";
    }
}
