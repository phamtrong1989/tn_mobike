using System;
using TN.Domain.Model;

namespace TN.Base.Command
{
    public class ExportBillCommand
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public DateTime DateBill { get; set; }
        public int WarehouseId { get; set; }
        public string Note { get; set; }
        public string Files { get; set; }
        public EExportBillStatus? Status { get; set; }
        public int ToWarehouseId { get;  set; }
    }

    public class ExportBillApproveCommand
    {
        public int Id { get; set; }
        public EExportBillStatus Status { get; set; }
    }
}