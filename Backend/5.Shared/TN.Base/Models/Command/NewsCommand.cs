using System;

namespace TN.Base.Command
{
    public class NewsCommand
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public DateTime PublicDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public byte Type { get; set; }
        public int AccountId { get; set; }
        public string Name { get; set; }
        public int LanguageId { get; set; }
        public int ProjectId { get; set; }
    }
}