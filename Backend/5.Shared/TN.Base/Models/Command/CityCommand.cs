using TN.Domain.Model;
using System;
using System.Text;

namespace TN.Base.Command
{
    public class CityCommand
    {
        public int Id { get; set; }
        public int PrivateId { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }

    }

    public class AccountBlackListCommand
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public EAccountBlackListType Type { get; set; }
        public string Note { get; set; }
        public DateTime Time { get; set; }
    }
}
