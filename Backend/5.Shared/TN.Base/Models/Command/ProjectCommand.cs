using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class ProjectCommand
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        [MaxLength(20)]
        public string Code { get; set; }
        public bool IsPrivate { get; set; }
        public EProjectStatus Status { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public int? DefaultZoom { get; set; }
    }

    public class ProjectResourceCommand
    {
        public int Id { get; set; }

        [Required]
        public int ProjectId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        public string Language { get; set; }
    }


    public class CustomerGroupResourceCommand
    {
        public int Id { get; set; }
        [Required]
        public int CustomerGroupId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        public string Language { get; set; }
    }

    public class StationResourceCommand
    {
        public int Id { get; set; }
        [Required]
        public int StationId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Language { get; set; }

        [Required]
        public string DisplayName { get;  set; }
    }

    public class HeadquartersEditCommand
    {
        public int Id { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
    }
}
