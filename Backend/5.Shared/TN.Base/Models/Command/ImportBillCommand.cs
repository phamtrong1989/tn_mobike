using System;
using TN.Domain.Model;

namespace TN.Base.Command
{
    public class ImportBillCommand
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public DateTime DateBill { get; set; }
        public float Total { get; set; }
        public string ReceiverUser { get; set; }
        public int WarehouseId { get; set; }
        public string Note { get; set; }
        public string Files { get; set; }
        public byte Type { get; set; }
        public EImportBillStatus? Status { get; set; }
        public DateTime? PCUConfirm { get; set; }
        public int? PCUUser { get; set; }
        public DateTime? TKVTSConfirm { get; set; }
        public int? TKVTSUser { get; set; }
        public DateTime? BODVTSConfirm { get; set; }
        public int? BODVTSUser { get; set; }
        public DateTime? CompleteConfirm { get; set; }
        public int? CompleteUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
    }
}