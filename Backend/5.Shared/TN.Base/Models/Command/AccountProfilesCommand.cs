﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PT.Domain.Model
{
    public class AccountProfilesCommand
    {
        [Required]
        public string DisplayName { get; set; }

        [Required]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Education { get; set; }

        [Required]
        public DateTime Birthday { get; set; }

        [Required]
        public int NationalityId { get; set; }

        public string Address { get; set; }
        public string Avatar { get;  set; }
    }
}
