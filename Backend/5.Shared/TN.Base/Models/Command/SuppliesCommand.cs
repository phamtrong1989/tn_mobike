using System;

namespace TN.Base.Command
{
    public class SuppliesCommand
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public int Quantity { get; set; }
        public string Unit { get; set; }
        public byte Type { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
    }
}