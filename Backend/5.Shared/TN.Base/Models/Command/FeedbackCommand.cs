using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static TN.Domain.Model.Feedback;

namespace TN.Base.Command
{
    public  class FeedbackCommand
    {
       public int Id { get; set; }
public int AccountId { get; set; }
public string Message { get; set; }
public EFeedbackStatus Status { get; set; }
public DateTime CreatedDate { get; set; }
public int Rating { get; set; }

    }
}
