using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public class DistrictCommand
    {
        public int Id { get; set; }
        public int PrivateId { get; set; }
        public int CityId { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }

    }
}
