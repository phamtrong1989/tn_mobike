using TN.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Base.Command
{
    public  class WalletCommand
    {
       public int Id { get; set; }
public int AccountId { get; set; }
public decimal Balance { get; set; }
public decimal SubBalance { get; set; }
public string HashCode { get; set; }
public EWalletStatus Status { get; set; }
public DateTime CreateDate { get; set; }
public DateTime? UpdateDate { get; set; }

    }
}
