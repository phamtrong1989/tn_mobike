﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Model;

namespace PT.Base.Command
{
    public class ManagerRegisterCommand
    {
        public int Id { get; set; }
        [StringLength(15, MinimumLength = 4)]
        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 10)]
        public string Email { get; set; }

        [StringLength(50)]
        public string RFID { get; set; }

        [Required]
        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(30, MinimumLength = 4)]
        [Required]
        public string DisplayName { get; set; }

        public bool IsLock { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        [Required]
        public int RoleId { get; set; }

        public string Education { get; set; }

        public bool IsChangePassword { get; set; }

        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{6,12}$", ErrorMessage = "Mật khẩu phải từ 6 đến 12 ký tự, chữ cái đầu là viết hoa, và phải có ký tự đặc biệt và số.")]
        public string Password { get;  set; }

        [StringLength(100)]
        [Required]
        public string FullName { get; set; }

        [Required]
        public DateTime? Birthday { get; set; }

        [Required]
        public ESex Sex { get; set; }

        // Địa chỉ tạm trú
        [MaxLength(200)]
        public string Address { get; set; }
        // Mã CMTND
        [MaxLength(50)]
        public string IdentificationID { get; set; }
        // Ngày cấp
        public DateTime? IdentificationSupplyDate { get; set; }
        // Nơi cấp
        [MaxLength(200)]
        public string IdentificationSupplyAdress { get; set; }
        // Địa chỉ CMTND
        [MaxLength(200)]
        public string IdentificationAdress { get; set; }
        // Mặt trước
        [MaxLength(500)]
        public string IdentificationPhotoFront { get; set; }
        // Mặt sau
        [MaxLength(500)]
        public string IdentificationPhotoBackside { get; set; }

        public int ManagerUserId { get; set; }

        // Nghỉ việc
        public bool IsRetired { get; set; }
        // Làm việc từ ngày
        public DateTime? WorkingDateFrom { get; set; }
        // Làm việc đến hết ngày
        public DateTime? WorkingDateTo { get; set; }
        public string ObjectIds { get; set; }
    }

    public class CustomerUserEditCommand
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        [Display(Name = "Tài khoản")]
        [StringLength(15, ErrorMessage = "{0} ít nhất phải là {2} và tối đa {1} ký tự.", MinimumLength = 4)]
        [Required(ErrorMessage = "{0} không được để trống.")]
        public string Username { get; set; }

        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Không đúng định dạng email.")]
        [StringLength(200, ErrorMessage = "{0} ít nhất phải là {2} và tối đa {1} ký tự.", MinimumLength = 10)]
        [Required(ErrorMessage = "{0} không được để trống.")]
        public string Email { get; set; }

        [Display(Name = "Tích chọn để khóa tài khoản")]
        public bool IsLock { get; set; }

        public bool IsChangePassword { get; set; }

        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{6,12}$", ErrorMessage = "Mật khẩu phải từ 6 đến 12 ký tự, chữ cái đầu là viết hoa, và phải có ký tự đặc biệt và số.")]
        public string Password { get; set; }
    }

    public class WorkingScheduleCommand
    {
        public string Id { get; set; }
        public int CompanyBranchId { get; set; }
        public string Name { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}
