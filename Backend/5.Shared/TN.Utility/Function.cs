﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using log4net;
using System.Collections;
using System.Reflection;
using System.Xml;
using System.Security.Cryptography;
using System.Drawing;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Principal;
using System.ComponentModel.DataAnnotations;

namespace TN.Utility
{
   

    public class EntityExtention<T> where T:class
    {
        public static Func<IQueryable<T>, IOrderedQueryable<T>> OrderBy(Func<IQueryable<T>, IOrderedQueryable<T>> a)
        {
            return a;
        }
    }

    public static class LabelExtensions
    {
        public static string ToDisplayName(this Enum value)
        {
            try
            {
                Type enumType = value.GetType();
                var enumValue = Enum.GetName(enumType, value);
                MemberInfo member = enumType.GetMember(enumValue)[0];
                var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
                var outString = ((DisplayAttribute)attrs[0]).Name;

                if (((DisplayAttribute)attrs[0]).ResourceType != null)
                {
                    outString = ((DisplayAttribute)attrs[0]).GetName();
                }
                return string.Format("{0}", outString);

            }
            catch
            {
                return value.ToString();
            }
        }
    }

    public class WeekYearModel
    {
        public int WeekNumber { get; set; }
        public int Year { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }


    public class Function
    {
        public static WeekYearModel GetIso8601WeekOfYear(DateTime time)
        {
            var modelObj = new WeekYearModel();
            int year = time.Year;
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            var day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            modelObj.StartDate = time.AddDays(-((day == DayOfWeek.Sunday ? 7 : (int)day) - 1));
            modelObj.Year = modelObj.StartDate.Year;
            modelObj.EndDate = modelObj.StartDate.AddDays(6);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            // Return the week of our adjusted day
            var weekNumber = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            modelObj.WeekNumber = weekNumber;
            return modelObj;
        }
        public static int TotalMilutes(DateTime startTime, DateTime endTime)
        {
            return Convert.ToInt32(Math.Ceiling((endTime - startTime).TotalSeconds / 60));
            //double totalMilutes = (endTime - startTime).TotalSeconds / 60;
            //if(totalMilutes % 1 > 0)
            //{
            //    return Convert.ToInt32(Math.Truncate(totalMilutes)) + 1;
            //}  
            //else
            //{
            //    return Convert.ToInt32(Math.Truncate(totalMilutes));
            //}
        }

        public static string PhoneReplate(string phone)
        {
            try
            {
                if (string.IsNullOrEmpty(phone) || phone.Length < 3)
                {
                    return phone;
                }
                return $"{phone.Substring(0, 3)}****{phone.Substring(7, phone.Length - 7)}";
            }
            catch
            {
                return phone;
            }
            //098***5582
        }

        public static string DistanceString(decimal distance)
        {
            try
            {
                if (distance <= 999)
                {
                    return $"{FormatMoney(distance)} m";
                }
                else
                {
                    double km = Math.Round(Convert.ToDouble(distance) / 1000, 1);
                    return $"{km} km";
                }
            }
            catch 
            {
                return distance.ToString();
            }
        }

        public static string DistanceString(int distance)
        {
            try
            {
                if (distance <= 999)
                {
                    return $"{FormatMoney(Convert.ToDouble(distance))} m";
                }
                else
                {
                    double km = Math.Round(Convert.ToDouble(distance) / 1000, 1);
                    return $"{km} km";
                }
            }
            catch
            {
                return distance.ToString();
            }
        }

        public static string DistanceString(double distance)
        {
            try
            {
                if (distance <= 999)
                {
                    return $"{FormatMoney(distance)} m";
                }
                else
                {
                    double km = Math.Round(distance / 1000, 1);
                    return $"{km} km";
                }
            }
            catch 
            {
                return distance.ToString();
            }
        }

        public static string GenOrderId(int transactionType)
        {
            return $"{DateTime.Now:yyMMdd}_{DateTime.Now:HHmmssfffffff}_{transactionType:D2}";
        }
        //public int? ValidateJwtToken(string token, string securityKey)
        //{
        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var key = Encoding.ASCII.GetBytes(securityKey);
        //    try
        //    {
        //        tokenHandler.ValidateToken(token, new TokenValidationParameters
        //        {
        //            ValidateIssuerSigningKey = true,
        //            IssuerSigningKey = new SymmetricSecurityKey(key),
        //            ValidateIssuer = false,
        //            ValidateAudience = false,
        //            ClockSkew = TimeSpan.Zero
        //        }, out SecurityToken validatedToken);

        //        var jwtToken = (JwtSecurityToken)validatedToken;
        //        var accountId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

        //        // return account id from JWT token if validation successful
        //        return accountId;
        //    }
        //    catch
        //    {
        //        // return null if validation fails
        //        return null;
        //    }
        //}

        public static int ValidateToken(string authToken, string issuer, string audience, string securityKey)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var validationParameters = GetValidationParameters(issuer, audience, securityKey);
                SecurityToken validatedToken;
                IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                return int.Parse(jwtToken.Claims.First(x => x.Type == "sid").Value);
            }
            catch
            {
                return 0;
            }
        }

        private static TokenValidationParameters GetValidationParameters(string issuer,string audience, string securityKey)
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = true, // Because there is no expiration in the generated token
                ValidateAudience = false, // Because there is no audiance in the generated token
                ValidateIssuer = true,   // Because there is no issuer in the generated token
                ValidIssuer = issuer,
                ValidAudience = audience,
                ClockSkew = TimeSpan.Zero,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey)) // The same key as the one that generate the token
            };
        }

        public static string TransactionHash(string orderIdRef, decimal amount, decimal subAmount, decimal tripPoint, string key)
        {
            return SignSHA256($"orderIdRef={orderIdRef}&amount={amount}&subAmount={subAmount}&tripPoint={tripPoint}&key={key}", key);
        }

        public static string SignSHA256(string message, string key)
        {
            try
            {
                byte[] keyByte = Encoding.UTF8.GetBytes(key);
                byte[] messageBytes = Encoding.UTF8.GetBytes(message);
                using var hmacsha256 = new HMACSHA256(keyByte);
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                string hex = BitConverter.ToString(hashmessage);
                hex = hex.Replace("-", "").ToLower();
                return hex;
            }
            catch
            {
                return $"EXEPTION-NULL|{message}|{key}";
            }
        }

        public static string RSAEncryption(string text, string public_key)
        {
            try
            {
                byte[] data = Encoding.UTF8.GetBytes(text);
                string result = null;
                using (var rsa = new RSACryptoServiceProvider(4096)) //KeySize
                {
                    try
                    {
                        // MoMo's public key has format PEM.
                        // You must convert it to XML format. Recommend tool: https://superdry.apphb.com/tools/online-rsa-key-converter
                        rsa.FromXmlString(public_key);
                        var encryptedData = rsa.Encrypt(data, false);
                        var base64Encrypted = Convert.ToBase64String(encryptedData);
                        result = base64Encrypted;
                    }
                    finally
                    {
                        rsa.PersistKeyInCsp = false;
                    }

                }
                return result;
            }
            catch
            {
                return null;
            }
        }

        public static string FormatMoney(double? number)
        {
            if (number == null)
            {
                return "";
            }
            if (number < 1000)
            {
                return Convert.ToInt32(number).ToString();
            }
            var culture = CultureInfo.GetCultureInfo("vi-VN");
            return string.Format(culture, "{0:00,0}", number);
        }

        public static string FormatMoney(decimal? number)
        {
            if (number == null)
            {
                return "";
            }
            if (number < 1000)
            {
                return Convert.ToInt32(number).ToString();
            }
            var culture = CultureInfo.GetCultureInfo("vi-VN");
            return string.Format(culture, "{0:00,0}", number);
        }

        public static bool ThumbnailCallback()
        {
            return false;
        }

        public static bool ByteArrayToImage(byte[] byteArrayIn, string path, int width)
        {
            if (byteArrayIn == null || byteArrayIn.Length == 0)
            {
                return false;
            }

            using var ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
            if (width <= 0)
            {
                ms.Write(byteArrayIn, 0, byteArrayIn.Length);
                var img = Image.FromStream(ms, true);
                using var newBitmap = new Bitmap(img);
                newBitmap.Save(path);
            }
            else
            {
                ms.Write(byteArrayIn, 0, byteArrayIn.Length);
                var img = Image.FromStream(ms, true);
                using var newBitmap = new Bitmap(img);
                newBitmap.Save(path);
                // img.Save(path);
                //ReducedImage(ms, width, path);
            }
            return true;
        }

        public static bool ByteArrayToImage(byte[] byteArrayIn, string path, string thumb, int width)
        {
            if (byteArrayIn == null || byteArrayIn.Length == 0)
            {
                return false;
            }

            using (var ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length))
            {
                ms.Write(byteArrayIn, 0, byteArrayIn.Length);
                var img = Image.FromStream(ms, true);
                using var newBitmap = new Bitmap(img);
                newBitmap.Save(path);
                ReducedImage(ms, width, thumb);
            }    
            return true;
        }

        public static bool ReducedImage(Stream stream, int Width, string saveMap)
        {
            try
            {
                double dblCoef;
                var image = Image.FromStream(stream);
                int baseWidth = image.Width;
                int baseHeight = image.Height;
                int newWidth = baseWidth, newHeight = baseHeight;

                if (baseWidth < baseHeight)
                {
                    dblCoef = (double)Width / (double)baseHeight;
                    if(dblCoef < 1)
                    {
                        newWidth = Convert.ToInt32(dblCoef * baseWidth);
                        newHeight = Convert.ToInt32(dblCoef * baseHeight);
                    }    
                }
                else
                {
                    dblCoef = (double)Width / (double)baseWidth;
                    if (dblCoef < 1)
                    {
                        newWidth = Convert.ToInt32(dblCoef * baseWidth);
                        newHeight = Convert.ToInt32(dblCoef * baseHeight);
                    }    
                }    

                Image reducedImage;
                Image.GetThumbnailImageAbort callb = new Image.GetThumbnailImageAbort(ThumbnailCallback);
                reducedImage = image.GetThumbnailImage(newWidth, newHeight, callb, IntPtr.Zero);
                reducedImage.Save(saveMap, System.Drawing.Imaging.ImageFormat.Jpeg);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
       
        public static string HMACMD5Password(string password)
        {
            using var md5 = MD5.Create();
            var result = md5.ComputeHash(Encoding.ASCII.GetBytes(password));
            var strResult = BitConverter.ToString(result);
            return strResult.Replace("-", "").Substring(5);
        }

        public static string FormatReturnUrl(string returnUrl, string urlDefault)
        {
            try
            {
                return string.IsNullOrEmpty(returnUrl) ? urlDefault : returnUrl.ToString();
            }
            catch
            {
                return urlDefault;
            }
        }

        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return (long)(datetime - sTime).TotalSeconds;
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string GetBitStr(byte[] data)
        {
            BitArray bits = new BitArray(data);

            string strByte = string.Empty;
            for (int i = 0; i <= bits.Count - 1; i++)
            {
                if (i % 8 == 0)
                {
                    strByte += " ";
                }
                strByte += (bits[i] ? "1" : "0");
            }

            return strByte;
        }

        public static bool ValidateIPv4(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }

        //public static string RandomString(int size, bool lowerCase = false)
        //{
        //    var random = new Random();
        //    var builder = new StringBuilder(size);
        //    char offset = lowerCase ? 'a' : 'A';
        //    const int lettersOffset = 26; // A...Z or a..z: length=26  

        //    for (var i = 0; i < size; i++)
        //    {
        //        var @char = (char)random.Next(offset, offset + lettersOffset);
        //        builder.Append(@char);
        //    }

        //    return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        //}

        //private static double DegreesToRadians(double degrees)
        //{
        //    return degrees * Math.PI / 180.0;
        //}

        //public static double DistanceInMeter(double lat1, double lon1, double lat2, double lon2)
        //{
        //    try
        //    {
        //        double circumference = 40075.6; // Earth's circumference at the equator in km
        //        double distance = 0.0;
        //        //Calculate radians
        //        double latitude1Rad = DegreesToRadians(lat1);
        //        double longitude1Rad = DegreesToRadians(lon1);
        //        double latititude2Rad = DegreesToRadians(lat2);
        //        double longitude2Rad = DegreesToRadians(lon2);

        //        double logitudeDiff = Math.Abs(longitude1Rad - longitude2Rad);

        //        if (logitudeDiff > Math.PI)
        //        {
        //            logitudeDiff = 2.0 * Math.PI - logitudeDiff;
        //        }

        //        double angleCalculation =
        //            Math.Acos(
        //              Math.Sin(latititude2Rad) * Math.Sin(latitude1Rad) +
        //              Math.Cos(latititude2Rad) * Math.Cos(latitude1Rad) * Math.Cos(logitudeDiff));

        //        distance = circumference * angleCalculation / (2.0 * Math.PI);

        //        return distance;
        //    }
        //    catch
        //    {
        //        return 99999;
        //    }
        //}

        public static double DistanceInMeter(double lat1, double lon1, double lat2, double lon2)
        {
            try
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(Deg2rad(lat1)) * Math.Sin(Deg2rad(lat2)) + Math.Cos(Deg2rad(lat1)) * Math.Cos(Deg2rad(lat2)) * Math.Cos(Deg2rad(theta));
                dist = Math.Acos(dist);
                dist = Rad2deg(dist);
                dist = dist * 60 * 1.1515;
                // meter
                dist = dist * 1.609344 * 1000;
                if(Double.IsNaN(dist))
                {
                    return 0;
                }    
                return Math.Round(dist, 1);
            }
            catch
            {
                return 99999;
            }
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double Deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double Rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
    }
}
