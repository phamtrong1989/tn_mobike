﻿using System.IO;
using log4net;
using System.Reflection;
using System.Xml;

namespace TN.Utility
{
    public class Log
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Log));

        public static void StartLog()
        {
            var log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));
            var repo = LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }

        public static void CloseLog()
        {
            foreach (log4net.Appender.IAppender app in logger.Logger.Repository.GetAppenders())
            {
                app.Close();
            }
        }

        public static void Error(string logtype, string logcontent)
        {
            try
            {
                logger.Error($"{logtype} \t {logcontent}");
            }
            catch
            {
            }
        }

        public static void Info(string logtype, string logcontent)
        {
            try
            {
                logger.Info($"{logtype} \t {logcontent}");
            }
            catch
            {
            }
        }

        public static void Debug(string logtype, string logcontent, string prefix = "Process")
        {
            try
            {
                logger.Debug($"{logtype} \t {logcontent}");
            }
            catch
            {
            }
        }

        //private static void WriteHashLog(string logtype, string logcontent, string prefix)
        //{
        //    try
        //    {
        //        logcontent = SimpleDES.Crypt($"{prefix}_{logtype} \t {logcontent}");
        //        logger.Debug(logcontent);
        //    }
        //    catch
        //    {
        //    }
        //}
    }
}
