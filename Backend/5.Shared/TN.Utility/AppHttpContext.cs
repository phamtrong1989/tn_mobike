﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
namespace TN.Shared
{
    public static class AppHttpContext
    {
        static IServiceProvider _services = null;
        public static IServiceProvider Services
        {
            get => _services;
            set
            {
                if (_services != null)
                    throw new Exception("Can't set once a value has already been set.");
                _services = value;
            }
        }

        public static string AppLanguage
        {
            get
            {
                try
                {
                    var data = Current?.Request?.Headers["Accept-Language"].ToString();
                    return (data == "" || data == null) ? "vi" : data;
                }
                catch
                {
                    return "";
                }
            }
        }

        public static HttpContext Current
        {
            get
            {
                IHttpContextAccessor httpContextAccessor =
                    _services.GetService(typeof(IHttpContextAccessor)) as IHttpContextAccessor;
                return httpContextAccessor?.HttpContext;
            }
        }
    }
}
