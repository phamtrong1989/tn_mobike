﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using log4net;
using System.Collections;
using System.Reflection;
using System.Xml;

namespace TN.UtilityNetF
{
    public class MNLog
    {
        static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static void Error(string logtype, string logcontent)
        {
            try
            {
                logger.Error($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        public static void Info(string logtype, string logcontent)
        {
            try
            {
                logger.Info($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        public static void Debug(string logtype, string logcontent, bool hash = false, string prefix = "Process")
        {
            try
            {
                if (hash)
                    Task.Run(() => WriteHashLog(logtype, logcontent, prefix)).ConfigureAwait(false);
                else
                    logger.Debug($"{logtype} \t {logcontent}");
            }
            catch
            {
                // ignored
            }
        }

        private static void WriteHashLog(string logtype, string logcontent, string prefix)
        {
            try
            {
                logcontent = SimpleDES.Crypt($"{prefix}_{logtype} \t {logcontent}");
                logger.Debug(logcontent);
            }
            catch
            {

            }
        }
    }
}
