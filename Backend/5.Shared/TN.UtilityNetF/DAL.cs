﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Dapper;
using Newtonsoft.Json;

namespace TN.UtilityNetF
{
    public class DAL
    {
        public static IEnumerable<T> Query<T>(string connectionStr, string proc, object p = null, bool log = false)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(connectionStr))
                {
                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Param", JsonConvert.SerializeObject(p));

                    var result = cnn.Query<T>(proc, p, commandType: CommandType.StoredProcedure);

                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Result", JsonConvert.SerializeObject(result));
                    return result;
                }
            }
            catch (Exception ex)
            {
                MNLog.Error((new StackFrame(1).GetMethod().Name) + "\t" + proc, ex.ToString());
                return default(IEnumerable<T>);
            }
        }

        public static async Task<IEnumerable<T>> QueryAsync<T>(string connectionStr, string proc, object p = null, bool log = false)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(connectionStr))
                {
                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Param", JsonConvert.SerializeObject(p));

                    var result = await cnn.QueryAsync<T>(proc, p, commandType: CommandType.StoredProcedure);

                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Result", JsonConvert.SerializeObject(result));
                    return result;
                }
            }
            catch (Exception ex)
            {
                MNLog.Error((new StackFrame(1).GetMethod().Name) + "\t" + proc, ex.ToString());
                return default(IEnumerable<T>);
            }
        }

        public static bool Execute(string connectionStr, string proc, object p = null, bool log = false)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(connectionStr))
                {
                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Param", JsonConvert.SerializeObject(p));
                    Convert.ToInt32(cnn.ExecuteScalar(proc, p, commandType: CommandType.StoredProcedure));
                    return true;
                }
            }
            catch (Exception ex)
            {
                MNLog.Error((new StackFrame(1).GetMethod().Name) + "\t" + proc, ex.ToString());
                return false;
            }
        }

        public static async Task<bool> ExecuteAsync(string connectionStr, string proc, object p = null, bool log = false)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(connectionStr))
                {
                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Param", JsonConvert.SerializeObject(p));
                    await cnn.ExecuteScalarAsync(proc, p, commandType: CommandType.StoredProcedure);
                    return true;
                }
            }
            catch (Exception ex)
            {
                MNLog.Error((new StackFrame(1).GetMethod().Name) + "\t" + proc, ex.ToString());
                return false;
            }
        }

        public static T Execute<T>(string connectionStr, string proc, object p = null, bool log = false)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(connectionStr))
                {
                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Param", JsonConvert.SerializeObject(p));
                    var result = cnn.ExecuteScalar<T>(proc, p, commandType: CommandType.StoredProcedure);
                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Result", JsonConvert.SerializeObject(result));
                    return result;
                }
            }
            catch (Exception ex)
            {
                MNLog.Error((new StackFrame(1).GetMethod().Name) + "\t" + proc, ex.ToString());
                return default(T);
            }
        }

        public static async Task<T> ExecuteAsync<T>(string connectionStr, string proc, object p = null, bool log = false)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(connectionStr))
                {
                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Param", JsonConvert.SerializeObject(p));
                    var result = await cnn.ExecuteScalarAsync<T>(proc, p, commandType: CommandType.StoredProcedure);
                    if (log)
                        MNLog.Debug((new StackFrame(1).GetMethod().Name) + "\t" + proc + ":Result", JsonConvert.SerializeObject(result));
                    return result;
                }
            }
            catch (Exception ex)
            {
                MNLog.Error((new StackFrame(1).GetMethod().Name) + "\t" + proc, ex.ToString());
                return default(T);
            }
        }
    }
}
