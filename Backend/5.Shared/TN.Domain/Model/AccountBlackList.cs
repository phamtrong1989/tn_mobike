﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EAccountBlackListType
    {
        Other = 0
    }

    public class AccountBlackList : IAggregateRoot
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public EAccountBlackListType Type { get; set; }
        public string Note { get; set; }
        public DateTime Time { get; set; }
        public int CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class SystemConfiguration : IAggregateRoot
    {
        public int Id { get; set; }
        public string SystemStartTime { get; set; }
        public string SystemEndTime { get; set; }
        public int UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
