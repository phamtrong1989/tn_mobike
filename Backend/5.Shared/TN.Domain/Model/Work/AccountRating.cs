﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
 
    public enum EAccountRatingType : byte
    {
        Week = 0,
        Month = 1
    }

    public enum EAccountRatingGroup : byte
    {
        KM = 0,
        Image = 1
    }

    public class AccountRating : IAggregateRoot
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }

        [MaxLength(2000)]
        public string Reward { get; set; }

        public EAccountRatingType Type { get; set; }

        public EAccountRatingGroup Group { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
    }
}