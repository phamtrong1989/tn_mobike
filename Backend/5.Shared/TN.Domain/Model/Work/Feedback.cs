﻿using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Feedback : IAggregateRoot
    {
        public enum EFeedbackStatus : byte
        {
            UnRead = 0,
            Read = 1,
        }

        public enum FeedbackType : byte
        {
            [Display(Name = "Liên hệ góp ý")]
            Default = 0,
            [Display(Name = "Phản hồi chuyến đi")]
            Transaction = 1
        }

        public int Id { get; set; }

        public int AccountId { get; set; }

        public int TransactionId { get; set; }

        public string TransactionCode { get; set; }

        [MaxLength(2000)]
        public string Message { get; set; }

        public byte Rating { get; set; }

        public FeedbackType Type { get; set; }

        public EFeedbackStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}