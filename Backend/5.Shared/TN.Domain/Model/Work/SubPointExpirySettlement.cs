﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    // Chốt số điểm bị trừ 
    public class SubPointExpirySettlement : IAggregateRoot
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        [Column(TypeName = "decimal (18,2)")]
        public decimal SubBalance { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
