﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{

    public class TicketPrice : IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int ProjectId { get; set; }

        public int CustomerGroupId { get; set; }

        public ETicketType TicketType { get; set; }

        public bool AllowChargeInSubAccount { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TicketValue { get; set; }

        public int? BlockPerMinute { get; set; }

        public int? BlockPerViolation { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal? BlockViolationValue { get; set; }

        public int LimitMinutes { get; set; }

        public bool IsDefault { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        [NotMapped]
        public string Note { get; set; }

        [NotMapped]
        public Project Project { get; set; }
    }

    public class TicketPriceBindModel
    {
        public int TicketPriceId { get; set; }
        public int TicketPrepaidId { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public int Order { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

    }
}