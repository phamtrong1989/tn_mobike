﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum LockRequestStatus
    {
        Waiting = 0,
        Success = 1,
        Failure = 2,
        Retrying = 3
    }

    public enum ECommandType:byte
    {
        L0 = 0,
        D0 = 1,
        D1 = 2,
        S5 = 3,
        S8 = 4,
        G0 = 5,
        I0 = 6,
        M0 = 7,
        S0 = 8,
        S1 = 9,
        C0 = 10,
        C1 = 11,
        D10 = 12
    }

    public class OpenLockRequest : IAggregateRoot
    {
        public int Id { get; set; }

        public int? ProjectId { get; set; }

        public int? InvestorId { get; set; }

        public int StationId { get; set; }

        public int DockId { get; set; }

        public int BikeId { get; set; }

        public int AccountId { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        [MaxLength(100)]
        public string IMEI { get; set; }

        [MaxLength(100)]
        public string SerialNumber { get; set; }

        [MaxLength(100)]
        public string RFID { get; set; }

        public int Retry { get; set; }

        public LockRequestStatus Status { get; set; }

        public ECommandType? CommandType { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? OpenTime { get; set; }

        public int? CreatedUser { get; set; }
    }
}
