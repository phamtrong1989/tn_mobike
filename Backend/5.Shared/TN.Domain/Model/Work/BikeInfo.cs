﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class BikeInfo : IAggregateRoot
    {
        public int Id { get; set; }

        public int BikeId { get; set; }

        public int TotalMinutes { get; set; }

        // Thời gian bảo dưỡng gần nhất
        public DateTime? LastMaintenanceTime { get; set; }

        //Thời gian sửa chữa gần nhất
        public DateTime? RepairTime { get; set; }

        public string MaintenanceData { get; set; }

        public string RepairData { get; set; }

        public string MaintenanceDataItem { get; set; }

        public string RepairDataItem { get; set; }

        // Last repairId
        public int MaintenanceId { get; set; }

        public int RepairId { get; set; }
    }
}
