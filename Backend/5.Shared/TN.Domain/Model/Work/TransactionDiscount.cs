﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public  class TransactionDiscount : IAggregateRoot
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public DateTime Date { get; set; }

        // Tổng số điểm - điểm thu hồi
        [Column(TypeName = "decimal (18,2)")]
        public decimal TotalPoint { get; set; }

        public int CyclingWeekId { get; set; }

        public int DiscountValue { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal DiscountPoint { get; set; }

        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
