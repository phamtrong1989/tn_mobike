﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum ESuppliesType : byte
    {
        [Display(Name = "Vật tư mới")]
        New = 0,
        [Display(Name = "Vật tư cũ")]
        Old = 1
    }

    public enum ESuppliesStatus
    {
        Enabled,
        Disabled
    }

    public class Supplies : IAggregateRoot
    {

        public int Id { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(2000)]
        public string Descriptions { get; set; }

        public int Quantity { get; set; }

        [MaxLength(50)]
        public string Unit { get; set; }

        public ESuppliesType Type { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Price { get; set; }

        public ESuppliesStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public IEnumerable<SuppliesWarehouse> SuppliesWarehouses { get; set; }
    }
}
