﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Language : IAggregateRoot
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code1 { get; set; }

        public string Code2 { get; set; }

        public string Flag1 { get; set; }

        public string Flag2 { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
    }
}