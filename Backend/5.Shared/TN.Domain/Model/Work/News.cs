﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum NewsType : byte
    {
        News = 0
    }

    public class News : IAggregateRoot
    {
        public int Id { get; set; }

        public int CampaignId { get; set; }

        public int ProjectId { get; set; }

        [MaxLength(100)]
        public string Author { get; set; }

        public int AccountId { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        [MaxLength(400)]
        public string Name { get; set; }

        public string Content { get; set; }

        public int LanguageId { get; set; }

        public DateTime PublicDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public NewsType Type { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
    }
}
