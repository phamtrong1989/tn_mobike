﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class SuppliesWarehouse : IAggregateRoot
    {
        public int Id { get; set; }

        public int SuppliesId { get; set; }

        public int WarehouseId { get; set; }

        public int Quantity { get; set; }

        [NotMapped]
        public Supplies Supplies { get; set; }

        [NotMapped]
        public Warehouse Warehouse { get; set; }
    }
}
