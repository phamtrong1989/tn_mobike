﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class AccountRatingItem : IAggregateRoot
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        public int ProjectId { get; set; }

        public int Order { get; set; }

        public int AccountId { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Value { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool Status { get; set; }

        [NotMapped]
        public Account Account { get; set; }
    }
}