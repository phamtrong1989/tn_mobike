﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EDiscountCodeType : byte
    {
        Point = 0,
        Percent = 1
    }

    // Mã giảm giá chỉ áp dụng cho vé lượt
    public class DiscountCode : IAggregateRoot
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int CustomerGroupId { get; set; }

        public int CampaignId { get; set; }

        public int? BeneficiaryAccountId { get; set; }

        [MaxLength(20)]
        public string Code { get; set; }

        public EDiscountCodeType Type { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Limit { get; set; }

        public int CurentLimit { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Point { get; set; }

        public bool Public { get; set; }

        public int Percent { get; set; }

        public ECampaignProvisoType ProvisoType { get; set; } = ECampaignProvisoType.Default;

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public Account BeneficiaryAccount { get; set; }
    }
}