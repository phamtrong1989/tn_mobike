﻿using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class DockInfo : IAggregateRoot
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string IMEI { get; set; }

        public int DockId { get; set; }

        public string L0 { get; set; }

        // Tổng số phút đi lũy kế
        public string D0 { get; set; }

        // Thời gian bảo dưỡng gần nhất
        public string D1 { get; set; }

        //Tổng số giờ đã sử dụng cần bảo dưỡng
        public string S5 { get; set; }

        // Thời gian bảo dưỡng tiếp theo
        public string S8 { get; set; }

        // Xe sự cố
        public string G0 { get; set; }

        // Mô tả lỗi trước khi sửa chữa
        public string I0 { get; set; }

        // File đính kèm trước khi sửa chữa
        public string M0 { get; set; }

        public string S0 { get; set; }

        public string S1 { get; set; }

        // Mô tả lỗi trước khi sửa chữa
        public string C0 { get; set; }

        // File đính kèm trước khi sửa chữa
        public string C1 { get; set; }

        public string D1O { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}
