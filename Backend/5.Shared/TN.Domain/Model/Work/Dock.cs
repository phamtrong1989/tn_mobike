﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;
using static TN.Domain.Model.Bike;

namespace TN.Domain.Model
{
    public enum EDockBookingStatus
    {
        None,
        Free = 0,
        Moving = 2
    }

    public class Dock : IAggregateRoot
    {
        private double battery;

        public int Id { get; set; }

        public int InvestorId { get; set; }

        public int StationId { get; set; }

        public int ProjectId { get; set; }

        public int ModelId { get; set; }

        public int WarehouseId { get; set; }

        public int ProducerId { get; set; }

        [MaxLength(50)]
        public string IMEI { get; set; }

        [MaxLength(50)]
        public string SerialNumber { get; set; }

        [MaxLength(20)]
        public string SIM { get; set; }

        public EBikeStatus Status { get; set; }

        public bool LockStatus { get; set; }

        public double Battery { get => Math.Round(battery,1); set => battery = value; }

        public bool Charging { get; set; }

        public bool ConnectionStatus { get; set; }

        public DateTime? LastConnectionTime { get; set; }
        public DateTime? LastGPSTime { get; set; }

        public double? Lat { get; set; }

        public double? Long { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public string Note { get; set; }

        [MaxLength(100)]
        public string RFID { get; set; }

        [NotMapped]
        public EDockBookingStatus BookingStatus { get; set; } = EDockBookingStatus.None;

        [NotMapped]
        public Bike Bike { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public Warehouse Warehouse { get; set; }
        [NotMapped]
        public Project Project { get; set; }
        [NotMapped]
        public Investor Investor { get; set; }
        [NotMapped]
        public Station Station { get; set; }
        [NotMapped]
        public Producer Producer { get; set; }
        [NotMapped]
        public Model Model { get; set; }
    }
}
