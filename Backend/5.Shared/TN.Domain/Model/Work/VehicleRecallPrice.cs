﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class VehicleRecallPrice : IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        [Column(TypeName = "decimal (18,2)")]
        public decimal Price { get; set; }
    }
}
