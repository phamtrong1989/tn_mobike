﻿using System.ComponentModel.DataAnnotations;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class StationResource : IAggregateRoot
    {
        public int Id { get; set; }

        public int StationId { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string DisplayName { get; set; }

        public string Address { get; set; }

        public int LanguageId { get; set; }
        public string Language { get; set; }
    }
}