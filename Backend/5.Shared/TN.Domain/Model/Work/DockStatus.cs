﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;
using static TN.Domain.Model.Bike;

namespace TN.Domain.Model
{
    public class DockStatus : IAggregateRoot
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public int DockId { get; set; }

        public bool IsProcess { get; set; }

        public DateTime UpdatedTime { get; set; }
    }
}
