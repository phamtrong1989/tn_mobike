﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum ECustomerGroupStatus
    {
        Inactive = 0,
        Active = 1
    }

    public class CustomerGroup : IAggregateRoot
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public int Order { get; set; }

        public ECustomerGroupStatus Status { get; set; } = ECustomerGroupStatus.Inactive;

        public bool IsDefault { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int UpdatedUser { get; set; }

        [NotMapped]
        public List<TicketPrice> TicketPrices { get; set; }
        [NotMapped]
        public bool IsAccountActive { get; set; }
        [NotMapped]
        public Project Project { get; set; }
    }
}
