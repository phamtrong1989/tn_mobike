﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{

    public enum EMaintenanceRepairStatus : byte
    {
        Add = 0,
        End = 1,
        Cancel = 2
    }

    public enum EMaintenanceRepairItemType : byte
    {
        Maintenane = 0,
        Repair = 1
    }

    public class MaintenanceRepair : IAggregateRoot
    {
        // Bảng đặt lịch bảo dưỡng, sửa chữa
        public int Id { get; set; }

        public string Code { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(2000)]
        public string Descriptions { get; set; }

        [MaxLength(2000)]
        public string LastDescriptions { get; set; }

        public string Files { get; set; }

        public string LastFiles { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public EMaintenanceRepairStatus Status { get; set; }

        public int WarehouseId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }
        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
    }
}
