﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum ETicketType : byte
    {
        [Display(Name = "Vé block|Block Ticket")]
        Block = 1,
        [Display(Name = "Vé ngày|Day Ticket")]
        Day = 2,
        [Display(Name = "Vé tháng|Month Ticket")]
        Month = 3
    }

    public class TicketPrepaid : IAggregateRoot
    {

        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int TicketPriceId { get; set; }

        public ETicketType TicketPrice_TicketType { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TicketPrice_TicketValue { get; set; }
        // Cho phép trừ vào tài khoản phụ ko
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        // Là bảng giá mặc định
        public bool TicketPrice_IsDefault { get; set; }
        // Quá bao phút thì x tiền
        public int TicketPrice_BlockPerMinute { get; set; }
        // Số tiền x lên quá
        public int TicketPrice_BlockPerViolation { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TicketPrice_BlockViolationValue { get; set; }
        //Nếu là vé ngày: 1 ngày tối đi đa 12 tiếng = 720 phút,  Nếu vé tháng: Đi thoải mái các ngày trong tháng.Mỗi chuyến đi không quá 2 tiếng = 120 phút, Nếu vé quý: Tương tự tháng
        public int TicketPrice_LimitMinutes { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal ChargeInAccount { get; set; }

        // Điểm trừ vào tài khoản khuyến mãi
        [Column(TypeName = "decimal (18,2)")]
        public decimal ChargeInSubAccount { get; set; }

        // Tổng số tiền
        [Column(TypeName = "decimal (18,2)")]
        public decimal TicketValue { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }

        public DateTime DateOfPayment { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public int MinutesSpent { get; set; }

        public int NumberExpirations { get; set; } = 0;

        [NotMapped]
        public CustomerGroup CustomerGroup { get; set; }

        [NotMapped]
        public string TicketTypeNote { get; set; }

        [NotMapped]
        public Account Account { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public TicketPrice TicketPrice { get; set; }
    }
}