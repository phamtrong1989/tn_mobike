﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EImportBillStatus
    {
        [Display(Name = "Đã hủy")]
        Cancel = 0,

        [Display(Name = "Bản nháp")]
        Draft,

        [Display(Name = "Thủ kho xác nhận từ phòng cung ứng, lập phiếu nhập kho trình ký KTT")]
        TK_XacNhanPCU,

        [Display(Name = "Kế toán trưởng VTS kiếm tra ký phiếu nhập kho từ Thủ kho")]
        KTTVTS_KiemTraXacNhan,

        [Display(Name = "Quản trị VTS kiếm tra và xác nhận từ kế toán trưởng")]
        BODVTS_KiemTraXacNhan,

        [Display(Name = "Kế toán trưởng VTS xác nhận nhập kho")]
        TKVTS_NhapKho
    }

    public enum EImportBillType
    {
        [Display(Name = "Nhập từ kho Trí Nam")]
        TriNam,

        [Display(Name = "Nhập từ nguồn khác")]
        Khac
    }

    public class ImportBill : IAggregateRoot
    {
        public int Id { get; set; }

        public EImportBillType Type { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        public DateTime DateBill { get; set; }

        public double Total { get; set; }

        public string ReceiverUser { get; set; }

        public int WarehouseId { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }

        public string Files { get; set; }

        public EImportBillStatus Status { get; set; }

        public DateTime? PCUConfirm { get; set; }

        public int? PCUUser { get; set; }

        public DateTime? TKVTSConfirm { get; set; }

        public int? TKVTSUser { get; set; }

        public DateTime? KTTConfirm { get; set; }

        public int? KTTUser { get; set; }

        public DateTime? BODVTSConfirm { get; set; }

        public int? BODVTSUser { get; set; }

        public DateTime? CompleteConfirm { get; set; }

        public int? CompleteUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public bool CancelBillPermission { get; set; }

        [NotMapped]
        public bool ApproveBillPermission { get; set; }

        [NotMapped]
        public bool DeniedBillPermission { get; set; }

        [NotMapped]
        public bool EditItemPermission { get; set; }
    }
}
