﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class SubPointExpiry : IAggregateRoot
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }
        public bool IsFirst { get; set; }
        public DateTime? PrvExpiryDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
}
