﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class CyclingWeek : IAggregateRoot
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int WeekNo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        // Tương ứng vừa là % được giảm trong ngày
        public int? MondayCycling { get; set; }
        public int? TuesdayCycling { get; set; }
        public int? WednesdayCycling { get; set; }
        public int? ThursdayCycling { get; set; }
        public int? FridayCycling { get; set; }
        public int? SaturdayCycling { get; set; }
        public int? SundayCycling { get; set; }
    }
}