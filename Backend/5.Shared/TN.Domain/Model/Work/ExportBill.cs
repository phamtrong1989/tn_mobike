﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EExportBillStatus
    {
        // NOTE: THÊM TRẠNG THÁI MỚI XUỐNG CUỐI CÙNG, TRÁNH LÀM XÁO TRỘN THỨ TỰ.
        [Display(Name = "Đã hủy")]
        Cancel = 0,

        [Display(Name = "Bản nháp")]
        Draft = 1,

        [Display(Name = "Nhân viên điều phối lập yêu cầu, trình kế toán")]
        NVDP_LapPhieu = 2,

        [Display(Name = "Kế toán xác nhận, trình thủ kho")]
        KT_LapLenhXuatKho = 3,

        [Display(Name = "Thủ kho xác nhận, lập phiếu xuất gửi GĐ VTS")]
        TK_LapPhieuXuat = 4,

        [Display(Name = "GĐ VTS xác nhận phiếu xuất kho từ thủ kho")]
        BODVTS_XacNhanPhieuXuatKho = 6,

        [Display(Name = "Thủ kho xác nhận xuất kho")]
        TK_XacNhanXuatKho = 7
    }

    public enum EExportBillCompleteStatus
    {
        [Display(Name = "Chưa có xác nhận")]
        Default = 0,

        [Display(Name = "Số liệu khớp")]
        MatchingFigures = 1,

        [Display(Name = "Số liệu không khớp")]
        WrongData = 2,

        [Display(Name = "Tái nhập kho")]
        ReImport = 3
    }

    public class ExportBill : IAggregateRoot
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        public DateTime DateBill { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public double Total { get; set; }

        public int Count { get; set; }

        public int WarehouseId { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }

        public string Files { get; set; }

        public EExportBillStatus Status { get; set; }

        public DateTime? NVDPConfirm { get; set; }

        public int? NVDPUser { get; set; }

        public DateTime? KTConfirm { get; set; }

        public int? KTUser { get; set; }

        public DateTime? TKConfirm { get; set; }

        public int? TKUser { get; set; }

        public DateTime? KT2Confirm { get; set; }

        public int? KT2User { get; set; }

        public DateTime? BODVTSConfirm { get; set; }

        public int? BODVTSUser { get; set; }

        public DateTime? TK2Confirm { get; set; }

        public int? TK2User { get; set; }

        public DateTime? CompleteConfirm { get; set; }

        public int? CompleteUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public int ToWarehouseId { get; set; }

        public EExportBillCompleteStatus CompleteStatus { get; set; } = EExportBillCompleteStatus.Default;

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }
    }
}
