﻿using PT.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{

    public class NotificationJob : IAggregateRoot
    {
 
        public int Id { get; set; }

        public int AccountId { get; set; }

        [MaxLength(100)]
        public string DeviceId { get; set; }

        [MaxLength(200)]
        public string Icon { get; set; }

        [MaxLength(200)]
        public string Tile { get; set; }

        [MaxLength(500)]
        public string Message { get; set; }

        public DateTime PublicDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public int LanguageId { get; set; }

        public bool IsSend { get; set; }

        public DateTime? SendDate { get; set; }

        public DockEventType DataDockEventType { get; set; } = DockEventType.Not;

        public string DataTime { get; set; }

        public string DataContent { get; set; }

        public bool IsAdmin { get; set; } = false;

        public bool IsSystem { get; set; } = false;

        [MaxLength(100)]
        public string ActionType { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public Account Account { get; set; }
    }
}