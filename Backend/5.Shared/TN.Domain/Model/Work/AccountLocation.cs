﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class AccountLocation : IAggregateRoot
    {
        [Key]
        public int Id { get; set; }

        public int AcountId { get; set; }

        public int StationId { get; set; }

        public string DeviceId { get; set; }

        public double Lat { get; set; }

        public double Lng { get; set; }

        public DateTime UpdatedTime { get; set; }

        public DateTime NextTime { get; set; }
    }
}
