﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EBikeWarningType
    {
        Undefind,
        DoXe,
        DiChuyenBatHopPhap,
        XeDiQuaXa,
        XeKhongTrongTram,
        XeLoiGPS,
        GPSDiChuyenBatHopPhap
    }

    public class BikeWarning : IAggregateRoot
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string IMEI { get; set; }
        public string Transaction { get; set; }
        public EBikeWarningType WarningType { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public bool Status { get; set; }
        [MaxLength(200)]
        public string Note { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class BikeWarningLog : IAggregateRoot
    {
        public long Id { get; set; }
        [MaxLength(50)]
        public string IMEI { get; set; }
        public string Transaction { get; set; }
        public EBikeWarningType WarningType { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public bool Status { get; set; }
        [MaxLength(200)]
        public string Note { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
