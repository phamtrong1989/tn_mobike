﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EEventSystemType: int
    {
        [Display(Name = "Xe mất kết nối BE")]
        M_1001_XeMatKetNoiBE = 1001,
        [Display(Name = "Xe đổ")]
        M_1002_XeDo = 1002,
        [Display(Name = "Xe không nằm trong trạm")]
        M_1003_XeKhongLamTrongTram = 1003,
        [Display(Name = "Xe hết PIN/sắp hết pin")]
        M_1004_XeHetPinSapHetPin = 1004,
        [Display(Name = "Xe mất GPS")]
        M_1005_XeMatGPS = 1005,
        [Display(Name = "Xe đang mở khóa mà không trong chuyến đi nào")]
        M_1006_XeMoNhungKhongTrongChuyenDiNao = 1006,
        [Display(Name = "Xe di chuyển bất hợp pháp")]
        M_1007_DiChuyenBatHopPhap = 1007,
        //B
        [Display(Name = "Thừa xe/thiếu xe")]
        M_2001_ThuaXeThieuXe = 2001,

        [Display(Name = "Khách hàng mở xe không thành công")]
        M_3001_KhachHangMoXeKhongThanhCong = 3001,
        [Display(Name = "Có giao dịch mới")]
        M_3002_CoGiaoDichMoi = 3002,
        [Display(Name = "Kết thúc giao dịch thành công")]
        M_3003_KetThucGiaoDichThanhCong = 3003,
        [Display(Name = "Hủy chuyến")]
        M_3004_HuyChuyen = 3004,
        [Display(Name = "Kết thúc giao dịch cưỡng bức")]
        M_3005_KetThucGiaoDichCuongBuc = 3005,
        [Display(Name = "Yêu cầu thu hồi xe")]
        M_3006_YeuCauThuHoiXe = 3006,
        [Display(Name = "Khách hàng cố kết thúc giao dịch không thành công")]
        M_3007_KhachHangCoKetThucGiaoDichKhongThanhCong = 3007,
        [Display(Name = "Khách Sắp hết điểm khi đang đi")]
        M_3008_KhachSapHetTienKhiDangDi = 3008,
        [Display(Name = "Cước chuyến đi vượt quá số dư")]
        M_3009_CuocChuyenVuotQuaSoDu = 3009,
        [Display(Name = "Khách hàng đi quá xa phạm vi")]
        M_3010_KhachHangDiQuaXaPhamVi = 3010,
        [Display(Name = "Khách hàng báo hỏng xe")]
        M_3011_KhachBaoHongXe = 3011,
        [Display(Name = "Khách hàng báo lỗi trả xe")]
        M_3012_KhachBaoLoiTraXe = 3012,
        [Display(Name = "Khách hàng dừng xe quá lâu")]
        M_3013_KhachHangDungXeQuaLau = 3013,
        [Display(Name = "Nghi vấn khóa mở nhưng không ghi nhận giao dịch")]
        M_3014_NghiVanMatGiaoDich = 3014,
        [Display(Name = "Khách quên trả xe")]
        M_3015_KhachQuenTraXe= 3015,
        //D
        [Display(Name = "Có yêu cầu xác minh thông tin khác hàng")]
        M_4001_CoYeuCauXacMinHThongTinKhachHang = 4001,
        [Display(Name = "Phản hồi chuyến đi")]
        M_4002_PhanHoiChuyenDi = 4002,
        [Display(Name = "Chuyến đi nợ cước")]
        M_4003_ChuyenDiNoCuoc = 4003,
        [Display(Name = "Có tài khoản mới tạo")]
        M_4004_CoTaiKhoanMoiTao = 4004,
        [Display(Name = "Có tài khoản nạp tiền")]
        M_4005_CoTaiKhoanNapTien = 4005,
        [Display(Name = "Có khách hàng đăng nhập")]
        M_4006_KhachHangDangNhap = 4006,

        [Display(Name = "Có khách hàng đăng xuất")]
        M_4007_KhachHangDangXuat = 4007
    }

    public enum EEventSystemGroup : byte
    {
        CanhBaoXeKhoa = 1,
        CanhBaoTram = 2,
        CanhBaoGiaoDich = 3,
        CanhBaoCSKH = 4
    }

    public enum EEventSystemWarningType : byte
    {
        Info,
        Warning,
        Danger
    }
}