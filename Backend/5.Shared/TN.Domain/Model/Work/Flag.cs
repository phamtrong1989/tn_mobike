﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;
using static TN.Domain.Model.Bike;

namespace TN.Domain.Model
{
    public class Flag : IAggregateRoot
    {
        public long Id { get; set; }

        public string Transaction { get; set; }

        public int Type { get; set; }

        public bool IsProcess { get; set; }

        public DateTime UpdatedTime { get; set; }
    }
}
