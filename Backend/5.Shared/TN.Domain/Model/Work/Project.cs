﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EProjectStatus
    {
        Inactive = 0,
        Active = 1,
    }

    public class Project : IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool IsPrivate { get; set; }
        public EProjectStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public int? DefaultZoom { get; set; }

        public double? HeadquartersLat { get; set; }
        public double? HeadquartersLng { get; set; }

        public string CityAreaData { get; set; }

        [NotMapped]
        public double DistanceToProject { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public List<TicketPriceBindModel> TicketPriceBinds { get; set; }

        [NotMapped]
        public List<LatLngCityAreaDataModel> ListCityAreaData { get; set; }
    }

    public class LatLngCityAreaDataModel
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}