﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EWalletTransactionStatus : byte
    {
        [Display(Name = "Chờ xử lý")]
        Pending = 0,
        [Display(Name = "Thành công")]
        Done = 1,
        [Display(Name = "Hủy giao dịch")]
        Cancel = 2,
        [Display(Name = "Khởi tạo")]
        Draft = 3
    }

    public enum EPaymentGroup : byte
    {
        [Display(Name = "---")]
        Default,

        [Display(Name = "VTCPAY")]
        VTCPay,

        [Display(Name = "MOMO")]
        MomoPay,

        [Display(Name = "ZALOPAY")]
        ZaloPay,

        [Display(Name = "VIETTELPAY")]
        ViettelPay,

        [Display(Name = "PAYOOPAY")]
        PayooPay
    }

    public enum EWalletTransactionType
    {
        [Display(Name = "Thanh toán trực tuyến")]
        Deposit = 0,

        [Display(Name = "Truy thu điểm")]
        Withdraw = 1,

        [Display(Name = "Thanh toán chuyến đi")]
        Paid = 2,

        [Display(Name = "Nạp mã khuyến mãi")]
        Voucher = 3,

        [Display(Name = "Nhập mã giới thiệu")]
        AddShareCode = 4,

        [Display(Name = "Thanh toán chuyển khoản")]
        Manually = 5,

        [Display(Name = "Mua vé trả trước thủ công")]
        WithdrawPrepaid = 6,

        [Display(Name = "Quy đổi tích điểm")]
        RedeemPoint = 7,

        [Display(Name = "Thanh toán tiền mặt")]
        Money = 8,

        [Display(Name = "Tặng điểm cho người khác")]
        GiftPoint = 9,

        [Display(Name = "Nhận điểm từ người khác")]
        ReceivedPoint = 10,

        [Display(Name = "Mua vé trả trước")]
        BuyPrepaid = 11,

        [Display(Name = "Thu hồi điểm khuyến mãi")]
        ClearTripPoint = 12,

        [Display(Name = "Người chia sẻ mã giới thiệu")]
        OwnerShareCode = 13,

        [Display(Name = "Trừ điểm nợ cước")]
        TruTienNoCuoc = 14,

        [Display(Name = "Hệ thống bổ sung")]
        BoXung = 15,

        [Display(Name = "Hoàn điểm chuyến đi")]
        PointRefund = 16,

        [Display(Name = "Bảy ngày lành mạnh cùng TNGO")]
        DiscountTransaction = 17
    }

    public class WalletTransaction : IAggregateRoot
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public int WalletId { get; set; }

        public int CampaignId { get; set; }

        public EWalletTransactionType Type { get; set; }

        public int TransactionId { get; set; }

        [MaxLength(50)]
        public string OrderIdRef { get; set; }

        [MaxLength(50)]
        public string TransactionCode { get; set; }

        public string HashCode { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Price { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Amount { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal SubAmount { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal? DebtAmount { get; set; }

        // Số point km được + thêm theo sự kiện
        [Column(TypeName = "decimal (18,2)")]
        public decimal DepositEvent_SubAmount { get; set; }

        // Số point được + thêm theo sự kiện
        [Column(TypeName = "decimal (18,2)")]
        public decimal DepositEvent_Amount { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TotalAmount { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TripPoint { get; set; }

        public bool IsAsync { get; set; } = false;

        public EWalletTransactionStatus Status { get; set; }

        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        [NotMapped]
        public Campaign Campaign { get; set; }

        public EPaymentGroup PaymentGroup { get; set; } = EPaymentGroup.Default;

        public DateTime? SetPenddingTime { get; set; }

        public DateTime? PadTime { get; set; }

        public string ReqestId { get; set; }

        [MaxLength(1000)]
        public string AdminNote { get; set; }

        public string WarningHistoryData { get; set; }

        public bool IsWarningHistory { get; set; }

        //Giá trị ví hiện tại trước khi giao dịch này +
        [Column(TypeName = "decimal (18,2)")]
        public decimal Wallet_Balance { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Wallet_SubBalance { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Wallet_TripPoint { get; set; }

        public string Wallet_HashCode { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public string Files { get; set; }

        public int GiftFromAccountId { get; set; }

        public int GiftToAccountId { get; set; }

        public int VoucherCodeId { get; set; }

        public int DepositEventId { get; set; }

        public int? LocationProjectId { get; set; }

        public int? LocationCustomerGroupId { get; set; }

        public bool? LocationProjectOverwrite { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public Account GiftFromAccountObject { get; set; }

        [NotMapped]
        public Account GiftToAccountObject { get; set; }

        [NotMapped]
        public Account Account { get; set; }

        [NotMapped]
        public IEnumerable<CustomerGroup> CustomerGroups { get; set; }

    }
}
