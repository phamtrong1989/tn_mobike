﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EGroupAppointment:byte
    {
        Default = 0,
        End = 2,
        Cancel = 3
    }

    public class GroupAppointment : IAggregateRoot
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }
        public int ProjectId { get; set; }

        public int MaxAmount { get; set; }
        public int CurrentAmount { get; set; }

        public DateTime StartTime { get; set; }

        public int StationId { get; set; }

        public double StationLat { get; set; }

        public double StationLng { get; set; }

        public int LeaderId { get; set; }

        [MaxLength(100)]
        public string LeaderFullName { get; set; }

        [MaxLength(20)]
        public string LeaderPhone { get; set; }
        [MaxLength(300)]

        public string LeaderAvartar { get; set; }
        public ESex LeaderSex { get; set; }

        [MaxLength(300)]
        public string Descriptions
        { get; set; }

        public int? ReputationLevel { get; set; }

        public EGroupAppointment Status { get; set; }

        public DateTime CreatedDate { get; set; }

        [MaxLength(100)]
        public string TransactionCode { get; set; }
    }

    public class GroupAppointmentItem : IAggregateRoot
    {
        public int Id { get; set; }

        public int GroupAppointmentId { get; set; }

        public int MemberId { get; set; }

        [MaxLength(100)]
        public string MemberFullName { get; set; }

        [MaxLength(20)]
        public string MemberPhone { get; set; }

        public ESex MemberSex { get; set; }

        public bool IsConfirm { get; set; }

        [MaxLength(100)]
        public string TransactionCode { get; set; }
    }

    public class GroupAppointmentAccount : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }
        public int? ReputationLevel { get; set; }
        public bool Lock { get; set; }
    }
}