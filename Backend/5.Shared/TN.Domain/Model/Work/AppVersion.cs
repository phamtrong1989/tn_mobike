﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class AppVersion : IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
    }
}