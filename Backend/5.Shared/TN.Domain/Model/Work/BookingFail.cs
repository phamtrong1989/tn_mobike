﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EBookingFailStatus: byte
    {
        Default = 0,
        ViewAdmin = 1,
        AddBooking = 2,
        CancelSystem = 3,
        CancelAdmin = 4,
        AddBookingByNotOpen = 5
    }

    public class BookingFail : IAggregateRoot
    {
        public int Id { get; set; }

        public string TicketPrepaidCode { get; set; }

        public int TicketPrepaidId { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int StationIn { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public int TicketPriceId { get; set; }
        public ETicketType TicketPrice_TicketType { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TicketPrice_TicketValue { get; set; }
        // Cho phép trừ vào tài khoản phụ ko
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        // Là bảng giá mặc định
        public bool TicketPrice_IsDefault { get; set; }
        // Quá bao phút thì x tiền
        public int TicketPrice_BlockPerMinute { get; set; }
        // Số tiền x lên quá
        public int TicketPrice_BlockPerViolation { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TicketPrice_BlockViolationValue { get; set; }
        //Nếu là vé ngày: 1 ngày tối đi đa 12 tiếng = 720 phút,  Nếu vé tháng: Đi thoải mái các ngày trong tháng.Mỗi chuyến đi không quá 2 tiếng = 120 phút, Nếu vé quý: Tương tự tháng
        public int TicketPrice_LimitMinutes { get; set; }
        // Điểm trừ vào tài khoản chính
        [Column(TypeName = "decimal (18,2)")]
        public decimal ChargeInAccount { get; set; }
        // Điểm trừ vào tài khoản khuyến mãi
        [Column(TypeName = "decimal (18,2)")]
        public decimal ChargeInSubAccount { get; set; }
        // Total price
        [Column(TypeName = "decimal (18,2)")]
        public decimal TotalPrice { get; set; }
        // Thời gian giao dịch
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int UpdatedUser { get; set; }

        [MaxLength(500)]
        public string Note { get; set; }

        // Với trường hợp là vé trả trước dụng chung trường nhưng ý nghĩa khác nhau
        // Tổng số phút đã sử dụng trong ngày (vé ngày), chưa tính giao dịch hiện tại
        // Tổng số phút tối đa sử dụng 1 lần giao dịch (vé tháng)
        public int Prepaid_MinutesSpent { get; set; }
        // Ngày hiệu lực bắt đầu
        public DateTime? Prepaid_StartDate { get; set; }
        // Ngày hết hiệu lực của vé trả trước
        public DateTime? Prepaid_EndDate { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_StartTime { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_EndTime { get; set; }

        public EBookingFailStatus Status { get; set; }

        [NotMapped]
        public Account Account { get; set; }

        [NotMapped]
        public Bike Bike { get; set; }

        [NotMapped]
        public Dock Dock { get; set; }

        [NotMapped]
        public string TicketPrice_Note { get; set; }
        [NotMapped]

        public Wallet Wallet { get; set; }

        [NotMapped]
        public int TotalMinutes { get; set; }
    }
}