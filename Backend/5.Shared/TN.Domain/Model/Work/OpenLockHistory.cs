﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    [Table("OpenLockHistory")]
    public class OpenLockHistory : IAggregateRoot
    {
        public int Id { get; set; }

        public int? ProjectId { get; set; }

        public int? InvestorId { get; set; }

        public int StationId { get; set; }

        public int DockId { get; set; }

        public int BikeId { get; set; }

        public int AccountId { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        [MaxLength(100)]
        public string IMEI { get; set; }

        [MaxLength(100)]
        public string SerialNumber { get; set; }

        public int Retry { get; set; }

        public LockRequestStatus Status { get; set; }

        public int? CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? OpenTime { get; set; }

        public ECommandType? CommandType { get; set; }
    }
}
