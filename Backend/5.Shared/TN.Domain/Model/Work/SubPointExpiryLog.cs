﻿using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class SubPointExpiryLog : IAggregateRoot
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int Days { get; set; }
        public DateTime? ExpiryDate { get; set; }
        [MaxLength]
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
