﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class EmailBox : IAggregateRoot
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Add { get; set; }
        public string Cc { get; set; }
        public string Bc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachments { get; set; }
        public bool IsSend { get; set; }
        public bool NotSend { get; set; } = false;
        public byte Type { get; set; }
        public int ObjectId { get; set; }
        public long NotificationDataId { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? SuccessfulSendDate { get; set; }
        [NotMapped] 
        public List<EmailBox> TypeEmailBoxs { get; set; }
    }
}