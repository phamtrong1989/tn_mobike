﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class MaintenanceRepairItem : IAggregateRoot
    {
        // Bảng chi tiết đặt lịch bảo dưỡng, sửa chữa
        public int Id { get; set; }

        public int MaintenanceRepairId { get; set; }

        public int BikeId { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public EMaintenanceRepairItemType Type { get; set; }

        public bool Status { get; set; }

        public string Descriptions { get; set; }

        public string Files { get; set; }

        public DateTime ? CompleteDate { get; set; }

        public int SuppliesId { get; set; }

        public string SuppliesCode { get; set; }

        public int SuppliesQuantity { get; set; }

        public int EmployeeId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public Bike Bike { get; set; }
        [NotMapped]
        public Supplies Supplies { get; set; }
        [NotMapped]
        public Project Project { get; set; }
        [NotMapped]
        public Investor Investor { get; set; }
        [NotMapped]
        public ApplicationUser Employee { get; set; }
    }
}