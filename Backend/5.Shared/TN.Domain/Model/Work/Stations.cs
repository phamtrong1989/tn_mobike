﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;
using static TN.Domain.Model.Bike;

namespace TN.Domain.Model
{
    
    public enum ESationStatus
    {
        Inactive = 0,
        Active = 1,
    }


    public class Station : IAggregateRoot
    {
        public int Id { get; set; }
        public int InvestorId { get; set; }
        public int ProjectId { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int Width { get; set; }
        public int Spaces { get; set; }
        public int Height { get; set; }
        [MaxLength(1000)]
        public string Address { get; set; }
        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(200)]
        public string DisplayName { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public int TotalDock { get; set; } = 0;

        public ESationStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }

        public int ManagerUserId { get; set; }

        public double ReturnDistance { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }
        [NotMapped]

        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public int TotalBike { get; set; } = 0;

        [NotMapped]
        public int TotalBikeGood { get; set; } = 0;

        [NotMapped]
        public int TotalBikeNotGood { get; set; } = 0;

        [NotMapped]
        public List<Bike> Bikes { get; set; }

        [NotMapped]
        public ApplicationUser ManagerUserObject { get; set; }

        // Báo cáo
        [NotMapped]
        public Project Project { get; set; }

        [NotMapped]
        public Investor Investor { get; set; }

        [NotMapped]
        public District District { get; set; }

        [NotMapped]
        public City City { get; set; }

        [NotMapped]
        public int? NumElectricBikeAvailable { get; set; }

        [NotMapped]
        public int? NumMechanicBikeAvailable { get; set; }

        [NotMapped]
        public double Distance { get; set; }
    }

    public class StationReport
    {
        public int StationId { get; set; }
        public EBikeType Type { get; set; }
        public int Count { get; set; }
    }
    public class StationTotalDockModel
    {
        public int StationId { get; set; }
        public int TotalDock { get; set; }
    }
}