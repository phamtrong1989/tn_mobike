﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;
using static TN.Domain.Model.Bike;

namespace TN.Domain.Model
{
    public enum EDataInfoType
    {
        SumAccountLive = 0
    }

    public class DataInfo : IAggregateRoot
    {
        public int Id { get; set; }
        public EDataInfoType Type { get; set; }
        [MaxLength(4000)]
        public string Data { get; set; }
        public DateTime UpdatedTime { get; set; }
    }
}
