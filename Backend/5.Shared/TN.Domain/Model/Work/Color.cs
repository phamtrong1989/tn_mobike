﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Color : IAggregateRoot
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        public string Code { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
    }
}
