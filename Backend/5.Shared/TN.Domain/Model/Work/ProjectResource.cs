﻿using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class ProjectResource : IAggregateRoot
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public int LanguageId { get; set; }
        public string Language { get; set; }
    }
}