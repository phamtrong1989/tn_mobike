﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class ExportBillItem : IAggregateRoot
    {
        public int Id { get; set; }

        public int ExportBillId { get; set; }

        public int SuppliesId { get; set; }

        public int Count { get; set; }

        public double Price { get; set; }


        public double Total { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }

        public string Files { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public Supplies Supplies { get; set; }
    }
}
