﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model.Work
{
    public class LogWork : IAggregateRoot
    {
        public long Id { get; set; }
        public EShift Shift { get; set; }
        public RoleManagerType Role { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Files { get; set; }

        public int CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime LogDate { get; set; }
        public bool IsLate { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }
        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
    }

    public enum EShift
    {
        [Display(Name = "Hành chính")]
        HC = 0,
        [Display(Name = "Ca 1 (5:00 - 13:00)")]
        S1 = 1,
        [Display(Name = "Ca 2 (13:00 - 21:00)")]
        S2 = 2,
        [Display(Name = "Ca 3 (21:00 - 5:00)")]
        S3 = 3,
    }
}