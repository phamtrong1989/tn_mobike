﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{

    public enum EBannerGroupStatus
    {
        Inactive = 0,
        Active = 1
    }

    public enum EBannerGroupType
    {
        [Display(Name = "Banner khu vực giao dịch")]
        B1 = 0,
    }

    public class BannerGroup : IAggregateRoot
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public EBannerGroupType Type { get; set; }

        public EBannerGroupStatus Status { get; set; } = EBannerGroupStatus.Inactive;

        [MaxLength(4000)]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public int LanguageId { get; set; }

        public string Language { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public Project Project { get; set; }

        [NotMapped]
        public IEnumerable<CustomerGroup> CustomerGroups { get; set; }

        [NotMapped]
        public IEnumerable<DepositEvent> DepositEvents { get; set; }

        [NotMapped]
        public IEnumerable<VoucherCode> VoucherCodes { get; set; }

        [NotMapped]
        public IEnumerable<Banner> Banners { get; set; }
    }
}
