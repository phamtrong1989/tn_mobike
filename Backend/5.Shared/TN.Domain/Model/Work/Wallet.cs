﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EWalletStatus
    {
        Inactive = 0,
        Active = 1,
    }

    public class Wallet : IAggregateRoot
    {
        public int Id { get; set; }
        public int AccountId { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Balance { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal SubBalance { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TripPoint { get; set; }

        public string HashCode { get; set; }

        public EWalletStatus Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        [NotMapped]
        public DateTime? SubBalanceExpiryDate { get; set; }

        [NotMapped]
        public decimal Debt { get; set; }
    }
}
