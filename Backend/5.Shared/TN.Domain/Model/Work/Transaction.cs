﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class TotalPriceModel
    {
        public decimal TotalPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public decimal Price { get; set; }
    }

    public enum ETransactionSourceType
    {
        Default = 0,
        Offline = 1,
        Cancel = 2,
        CancelOffline = 3
    }

    public enum EVehicleRecallStatus : byte
    {
        Default = 0,
        Unfulfilled = 1,
        Processed = 2
    }

    public class Transaction : IAggregateRoot
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string RFID { get; set; }
        public bool IsOpenRFID { get; set; } = false;
        public bool IsEndRFID { get; set; } = false;

        public string TicketPrepaidCode { get; set; }

        public int TicketPrepaidId { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int? StationIn { get; set; }

        public int? StationOut { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public byte Vote { get; set; }

        [MaxLength(1000)]
        public string Feedback { get; set; }

        public int TicketPriceId { get; set; }

        public ETicketType TicketPrice_TicketType { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TicketPrice_TicketValue { get; set; }
        // Cho phép trừ vào tài khoản phụ ko
        public bool TicketPrice_AllowChargeInSubAccount { get; set; }
        // Là bảng giá mặc định
        public bool TicketPrice_IsDefault { get; set; }
        // Quá bao phút thì x tiền
        public int TicketPrice_BlockPerMinute { get; set; }
        // Số tiền x lên quá
        public int TicketPrice_BlockPerViolation { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TicketPrice_BlockViolationValue { get; set; }
        //Nếu là vé ngày: 1 ngày tối đi đa 12 tiếng = 720 phút,  Nếu vé tháng: Đi thoải mái các ngày trong tháng.Mỗi chuyến đi không quá 2 tiếng = 120 phút, Nếu vé quý: Tương tự tháng
        public int TicketPrice_LimitMinutes { get; set; }
        // Điểm trừ vào tài khoản chính
        [Column(TypeName = "decimal (18,2)")]
        public decimal ChargeInAccount { get; set; }
        // Điểm trừ vào tài khoản khuyến mãi
        [Column(TypeName = "decimal (18,2)")]
        public decimal ChargeInSubAccount { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal? ChargeDebt { get; set; }

        // Total price
        [Column(TypeName = "decimal (18,2)")]
        public decimal TotalPrice { get; set; }
        // Thời gian giao dịch
        public DateTime? DateOfPayment { get; set; }
        public int TotalMinutes { get; set; }
        public DateTime CreatedDate { get; set; }
        public double? KCal { get; set; }
        public double? KG { get; set; }
        public double? KM { get; set; }
        public EBookingStatus Status { get; set; }

        [MaxLength(500)]
        public string Note { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TripPoint { get; set; }
        // Với trường hợp là vé trả trước dụng chung trường nhưng ý nghĩa khác nhau
        // Tổng số phút đã sử dụng trong ngày (vé ngày), chưa tính giao dịch hiện tại
        // Tổng số phút tối đa sử dụng 1 lần giao dịch (vé tháng)
        public int Prepaid_MinutesSpent { get; set; }
        // Ngày hiệu lực bắt đầu
        public DateTime? Prepaid_StartDate { get; set; }
        // Ngày hết hiệu lực của vé trả trước
        public DateTime? Prepaid_EndDate { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_StartTime { get; set; }
        // Thời gian hiệu lực vé trả trước trong ngày
        public DateTime? Prepaid_EndTime { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal EstimateChargeInAccount { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal EstimateChargeInSubAccount { get; set; }

        public EVehicleRecallStatus VehicleRecallStatus { get; set; } = EVehicleRecallStatus.Default;

        [Column(TypeName = "decimal (18,2)")]
        public decimal VehicleRecallPrice { get; set; }

        public double ReCallM { get; set; } = 0;

        public bool IsDockNotOpen { get; set; } = false;
        public bool IsForgotrReturnTheBike { get; set; } = false;

        public int? DiscountCodeId { get; set; }
        public int? DiscountCode_ProjectId { get; set; }

        public int? DiscountCode_CampaignId { get; set; }
        public int? DiscountCode_Percent { get; set; }
        public EDiscountCodeType? DiscountCode_Type { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal? DiscountCode_Point { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal? DiscountCode_Price { get; set; }

        [MaxLength(50)]
        public string OpenLockTransaction { get; set; }

        [MaxLength(50)]
        public string GroupTransactionCode { get; set; }

        public int? GroupTransactionOrder { get; set; }


        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public Account Account { get; set; }

        [NotMapped]
        public Bike Bike { get; set; }

        [NotMapped]
        public TicketPrice TicketPrice { get; set; }

        [NotMapped]
        public CustomerGroup CustomerGroup { get; set; }

        [NotMapped]
        public string TicketPrice_Note { get; set; }

        [NotMapped]
        public Station ObjStationIN { get; set; }

        [NotMapped]
        public Station ObjStationOut { get; set; }

        [NotMapped]
        public List<GPSData> GPS { get; set; }

        [NotMapped]
        public Project Project { get; set; }

        [NotMapped]
        public Investor Investor { get; set; }

        [NotMapped]
        public Dock Dock { get; set; }

        [NotMapped]
        public decimal Wallet_AccountBlance { get; set; }

        [NotMapped]
        public decimal Wallet_SubAccountBlance { get; set; }
    }

    public class BikeTransaction
    {
        public int BikeId { get; set; }
        public int TotalMinutesRent { get; set; }
    }
}
