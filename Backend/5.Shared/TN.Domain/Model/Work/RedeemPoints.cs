﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class RedeemPoint : IAggregateRoot
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal TripPoint { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal ToPoint { get; set; }

        [MaxLength(1000)]
        public string Note { get; set; }

        public int CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }
        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
    }
}
