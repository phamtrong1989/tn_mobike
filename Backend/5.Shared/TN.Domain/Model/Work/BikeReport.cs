﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EBikeReportType
    {
        [Display(Name = "Khác")]
        Khac = 0,

        [Display(Name = "Mở khóa")]
        MoKhoa = 1,

        [Display(Name = "Mã QR")]
        MaQR = 2,

        [Display(Name = "Bị phá hoại")]
        BiPhaHoai = 3,

        [Display(Name = "Trạm trông xe")]
        TramTrongXe = 4,

        [Display(Name = "Khóa hỏng")]
        KhoaHong = 5,

        [Display(Name = "Không nạp được điểm")]
        KhongNapDuocDiem = 6,

        [Display(Name = "Không nhận được điểm khi nạp")]
        KhongNhanDuocDiemKhiNap = 7,

        [Display(Name = "Lỗi trả xe")]
        LoiTraXe = 8
    }

    public class BikeReport : IAggregateRoot
    {
        public int Id { get; set; }

        public int BookingId { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int DockId { get; set; }

        public int BikeId { get; set; }

        public int AccountId { get; set; }

        public string Files { get; set; }

        public EBikeReportType Type { get; set; }

        public string File { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        public bool Status { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool? IsSendEmailReport { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public Account Account { get; set; }

        [NotMapped]
        public Bike Bike { get; set; }

        [NotMapped]
        public Dock Dock { get; set; }
    }
}
