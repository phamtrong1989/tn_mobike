﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EBannerStatus
    {
        Inactive = 0,
        Active = 1
    }

    public enum EBannerType
    {
        [Display(Name = "Không làm gì")]
        Default = 0,
        [Display(Name = "Link đến 1 path website")]
        Link = 1,
        [Display(Name = "Link đến 1 tin tức trong APP")]
        DEPLink = 2,
    }

    public class Banner : IAggregateRoot
    {
        public int Id { get; set; }
        public int BannerGroupId { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Image { get; set; }

        [MaxLength(500)]
        public string Data { get; set; }

        public EBannerType Type { get; set; }

        public EBannerStatus Status { get; set; } = EBannerStatus.Inactive;

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        [MaxLength(4000)]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public int LanguageId { get; set; }

        public string Language { get; set; }

        [NotMapped]
        public object DataObject { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
    }
}
