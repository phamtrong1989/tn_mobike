﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class AccountDevice : IAggregateRoot
    {
        [Key]
        public int Id { get; set; }

        public int? AcountId { get; set; }

        public string Token { get; set; }

        [MaxLength(50)]
        public string DeviceId { get; set; }

        [MaxLength(50)]
        public string IP { get; set; }

        public string CloudMessagingToken { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
