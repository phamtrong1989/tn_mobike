﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public static class LabelExtensions
    {
        public static string ToName(this Enum value)
        {
            try
            {
                Type enumType = value.GetType();
                var enumValue = Enum.GetName(enumType, value);
                MemberInfo member = enumType.GetMember(enumValue)[0];
                var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
                var outString = ((DisplayAttribute)attrs[0]).Name;

                if (((DisplayAttribute)attrs[0]).ResourceType != null)
                {
                    outString = ((DisplayAttribute)attrs[0]).GetName();
                }
                return string.Format("{0}", outString);
            }
            catch
            {
                return value.ToString();
            }
        }
    }

    public enum ESex : byte
    {
        [Display(Name = "Khác")]
        Khac = 2,
        [Display(Name = "Nam")]
        Nam = 1,
        [Display(Name = "Nữ")]
        Nu = 0,
    }

    public enum EAccountType : byte
    {
        [Display(Name = "App mobile")]
        Normal = 0,
        [Display(Name = "RFID")]
        RFID = 1,
        [Display(Name = "App mobile và RFID")]
        NormalAndRFID = 2
    }

    public enum EAccountStatus : byte
    {
        [Display(Name = "Khóa")]
        Lock = 0,
        [Display(Name = "Kích hoạt")]
        Active = 1
    }

    public enum EAccountCreateType : byte
    {
        Customer = 0,
        Admin = 1
    }

    public class Account : IAggregateRoot
    {
        public Account()
        {

        }

        public Account(Account account)
        {
            if (account != null)
            {
                Id = account.Id;
                Username = account.Username;
                Phone = account.Phone;
                RFID = account.RFID;
                Status = account.Status;
                Type = account.Type;
                FullName = account.FullName;
                MaxBookingBike = account.MaxBookingBike;
            }
        }

        public int Id { get; set; }

        public EAccountCreateType CreateType { get; set; }

        [MaxLength(30)]
        public string Username { get; set; }

        [MaxLength(50)]
        public string RFID { get; set; }

        [MaxLength(200)]
        public string Password { get; set; }

        [MaxLength(200)]
        public string PasswordSalt { get; set; }

        [MaxLength(50)]
        public string Etag { get; set; }

        [MaxLength(500)]
        public string Avatar { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        public ESex Sex { get; set; }

        [MaxLength(30)]
        public string FirstName { get; set; }

        [MaxLength(30)]
        public string LastName { get; set; }

        [MaxLength(200)]
        public string FullName { get; set; }

        public DateTime? Birthday { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(200)]
        public string Address { get; set; }

        [MaxLength(2000)]
        public string MobileToken { get; set; }

        public EAccountStatus Status { get; set; }

        [MaxLength(1000)]
        public string Note { get; set; }


        [MaxLength(1000)]
        public string NoteLock { get; set; }

        public EAccountType Type { get; set; }

        public int LanguageId { get; set; }

        [MaxLength(10)]
        public string Language { get; set; }

        [MaxLength(50)]
        public string Code { get; set; }

        public string ShareCode { get; set; }

        public EAccountIdentificationType IdentificationType { get; set; } = EAccountIdentificationType.CMTND;

        [MaxLength(50)]
        public string IdentificationID { get; set; }

        [MaxLength(500)]
        public string IdentificationPhotoFront { get; set; }

        [MaxLength(500)]
        public string IdentificationPhotoBackside { get; set; }

        public EAccountVerifyStatus VerifyStatus { get; set; }

        [MaxLength(1000)]
        public string VerifyNote { get; set; }

        public DateTime? VerifyDate { get; set; }

        public int VerifyUserId { get; set; }

        public DateTime? CreatedSystemDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int CreatedUserId { get; set; }

        public int CreatedSystemUserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public DateTime? UpdatedSystemDate { get; set; }

        public int UpdatedUserId { get; set; }

        public int UpdatedSystemUserId { get; set; }

        public int SubManagerUserId { get; set; }

        public int ManagerUserId { get; set; }

        public bool IsLock { get; set; }
        public bool IsReLogin { get; set; }
        public int? NumberWrongPasswords { get; set; }
        public DateTime? ExpirationWrongPassword { get; set; }

        public int MaxBookingBike { get; set; }

        public int? LocationProjectId { get; set; }

        public int? LocationCustomerGroupId { get; set; }

        public bool? LocationProjectOverwrite { get; set; }

        [NotMapped]
        public Wallet Wallet { get; set; }

        [NotMapped]
        public List<Transaction> Transactions { get; set; }

        [NotMapped]
        public List<WalletTransaction> WalletTransactions { get; set; }

        [NotMapped]
        public List<TicketPrepaid> TicketPrepaids { get; set; }

        [NotMapped]
        public List<ProjectAccount> ProjectAccounts { get; set; }

        // Begin Báo cáo - Hải
        [NotMapped]
        public string ManagerUserCode { get; set; }

        [NotMapped]
        public string SubManagerUserCode { get; set; }

        [NotMapped]
        public string VerifyUserCode { get; set; }

        [NotMapped]
        public string UpdateUserCode { get; set; }

        [NotMapped]
        public IEnumerable<Project> Projects { get; set; }

        [NotMapped]
        public IEnumerable<Investor> Investors { get; set; }

        [NotMapped]
        public ApplicationUser ManagerUserObject { get; set; }

        [NotMapped]
        public ApplicationUser SubManagerUserObject { get; set; }

        [NotMapped]
        public IEnumerable<CustomerGroup> CustomerGroups { get; set; }

        [NotMapped]
        public decimal AmountPlus { get; set; }

        [NotMapped]
        public decimal AmountMinus { get; set; }

        [NotMapped]
        public decimal SubAmountMinus { get; set; }

        [NotMapped]
        public IEnumerable<EPaymentGroup> PaymentGroups { get; set; }

        [NotMapped]
        public decimal Wallet_Balance { get; set; }

        [NotMapped]
        public decimal Wallet_SubBalance { get; set; }
        // End Báo cáo

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser User { get; set; }

        [NotMapped]
        public int UserId { get; set; }
    }

    public enum EAccountVerifyStatus
    {
        [Display(Name = "Chưa thực hiện")]
        Not = 0,
        [Display(Name = "Gửi chờ duyệt")]
        Waiting = 1,
        [Display(Name = "Xác nhận thông tin")]
        Ok = 2,
        [Display(Name = "Trả lại yêu cầu hoàn thiện")]
        Refuse = 3
    }

    public enum EAccountIdentificationType
    {
        CMTND = 0,
        CCCD = 1,
        Passport = 2
    }
}
