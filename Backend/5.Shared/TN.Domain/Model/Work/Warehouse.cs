﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{

    public enum EWarehouseType : byte
    {
        [Display(Name = "Kho tổng VTS")]
        Defaut,
        [Display(Name = "Kho điều phối bảo dưỡng VTS")]
        Virtual
    }

    public class Warehouse : IAggregateRoot
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Address { get; set; }
        public EWarehouseType Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int EmployeeId { get; set; }
        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }
        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
        [NotMapped]
        public ApplicationUser Employee { get; set; }
    }
}