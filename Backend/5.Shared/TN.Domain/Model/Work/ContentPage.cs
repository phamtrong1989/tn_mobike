﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EContentPageType
    {
        TermsOfUse = 1,
        TentalAgreement = 2,
        Introduction = 3
    }

    public class ContentPage : IAggregateRoot
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public EContentPageType Type { get; set; }

        public int LanguageId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
    }
}