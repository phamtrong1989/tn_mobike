﻿using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum ENotificationTempType
    {
        Free = 0,
        Test = 1,
        NapTienThanhCong = 2,
        NapTienKhongThanhCong = 3,
        KhongDuTienThucHienChuyenDi = 4,
        KHDangOTrongTram = 5,
        LayXeThanhCong = 6,
        NhacTraXeKetThucChuyenDi = 6,
        HoanThanhChuyenDiNhacDongKhoa = 7,
        KetThucChuyenDi = 8,
        HuyChuyenDi = 9,
        NhapMaGioiThieu = 10,
        SuKienDongKhoa = 11,
        MuaVeTraTruocThanhCong = 12,
        TangDiemThanhCong = 13,
        NhanDiemThanhCong = 14,
        NapMaKhuyenMaiThanhCong = 15,
        DoiDiemTichLuyThanhCong = 16,
        TaiKhoanDuocDuyet = 17,
        TaiKhoanKhongDuocDuyet = 18,
        KetThucChuyenDiNoCuoc = 19,
        DauThangTruTienTKKM = 20,
        ChuaCungCapDichVuTaiDay = 21,
        CoNguoiDungMaChiaSeCuaBan = 22,
        BanDiXeQuaXa = 23,
        KHDiXaKhoiXeMaKhongKhoaXe = 24,
        KHKhoaXeNhanCanhBaoBatHopPhap = 25,
        BayNgayLanhManhCungTNGO = 26,
    }

    public enum ENotificationActionType
    {
        WalletTransaction = 0,
        Transaction = 1
    }

    public class NotificationData : IAggregateRoot
    {
 
        public long Id { get; set; }

        public int AccountId { get; set; }

        [MaxLength(100)]
        public string DeviceId { get; set; }

        [MaxLength(200)]
        public string Icon { get; set; }

        [MaxLength(100)]
        public string TransactionCode { get; set; }

        public ENotificationTempType Type { get; set; }

        [MaxLength(200)]
        public string Tile { get; set; }

        [MaxLength(500)]
        public string Message { get; set; }

        [MaxLength(100)]
        public string ActionType { get; set; }
        public int LanguageId { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsSeen { get; set; } = false;

        public bool IsAdmin { get; set; } = false;
    }
}