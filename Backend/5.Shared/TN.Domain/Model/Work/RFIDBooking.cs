﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum ERFIDCoordinatorStatus:byte
    {
        [Display(Name = "Bắt đầu")]
        Start = 0,

        [Display(Name = "Kết thúc")]
        End = 1
    }

    public enum ERFIDBookingType
    {
        [Display(Name = "Điều phối mở")]
        Admin = 0,

        [Display(Name = "Tài khoản khách hàng mở")]
        Account = 1
    }

    public class RFIDBooking : IAggregateRoot
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string RFID { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public int ProjectId { get; set; }
        public int InvestorId { get; set; }
        public int StationIn { get; set; }
        public int StationOut { get; set; }
        public int BikeId { get; set; }
        public int DockId { get; set; }
        public int UserId { get; set; }
        public int AccountId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        [MaxLength(1000)]
        public string Note { get; set; }
        public ERFIDCoordinatorStatus Status { get; set; }
        public ERFIDBookingType Type { get; set; }
        public double StartLat { get; set; }
        public double StartLng { get; set; }
        public double? EndLat { get; set; }
        public double? EndLng { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpatedUser { get; set; }

        [NotMapped]
        public Bike Bike { get; set; }

        [NotMapped]
        public Dock Dock { get; set; }

        [NotMapped]
        public Station StationInObject { get; set; }

        [NotMapped]
        public Account Account { get; set; }

        [NotMapped]
        public ApplicationUser User { get; set; }

        [NotMapped]
        public int TotalMinutes { get; set; }
    }

    public class RFIDTransaction : IAggregateRoot
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string RFID { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public int ProjectId { get; set; }
        public int InvestorId { get; set; }
        public int StationIn { get; set; }
        public int StationOut { get; set; }
        public int BikeId { get; set; }
        public int DockId { get; set; }
        public int UserId { get; set; }
        public int AccountId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        [MaxLength(1000)]
        public string Note { get; set; }
        public ERFIDCoordinatorStatus Status { get; set; }
        public ERFIDBookingType Type { get; set; }
        public double StartLat { get; set; }
        public double StartLng { get; set; }
        public double? EndLat { get; set; }
        public double? EndLng { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpatedUser { get; set; }

        [NotMapped]
        public Bike Bike { get; set; }

        [NotMapped]
        public Dock Dock { get; set; }

        [NotMapped]
        public Station StationInObject { get; set; }

        [NotMapped]
        public Account Account { get; set; }

        [NotMapped]
        public ApplicationUser User { get; set; }
    }
}