﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EDepositEventType
    {
        Point,
        Percent
    }

    public class DepositEvent : IAggregateRoot
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int CustomerGroupId { get; set; }

        public int CampaignId { get; set; }

        public string TransactionCode { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal StartAmount { get; set; }

        // Lĩnh thưởng tối đa
        [Column(TypeName = "decimal (18,2)")]
        public decimal MaxAmount { get; set; } = 0;

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal Point { get; set; }

        [Column(TypeName = "decimal (18,2)")]
        public decimal SubPoint { get; set; } = 0;

        public int Percent { get; set; }

        public int SubPercent { get; set; } = 0;

        public EDepositEventType Type { get; set; }
        // Tối đa số lần sử dụng
        public int Limit { get; set; }

        public int Used { get; set; }

        public int SubPointExpiry { get; set; } = 0;

        public ECampaignProvisoType ProvisoType { get; set; } = ECampaignProvisoType.Default;

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public CustomerGroup CustomerGroup { get; set; }

        [NotMapped]
        public Project Project { get; set; }

        [NotMapped]
        public Campaign Campaign { get; set; }

        [NotMapped]
        public decimal ToSubPrice { get; set; }

        [NotMapped]
        public decimal ToPrice { get; set; }

        [NotMapped]
        public IEnumerable<WalletTransaction> WalletTransactions { get; set; }
    }
}
