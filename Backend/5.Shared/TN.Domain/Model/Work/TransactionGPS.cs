﻿using PT.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;
namespace TN.Domain.Model
{
    public class TransactionGPS
    {
        public object Id { get; set; }
        public int TransactionId { get; set; }
        public int Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string TransactionCode { get; set; }
        public string Data { get; set; }
    }
}
