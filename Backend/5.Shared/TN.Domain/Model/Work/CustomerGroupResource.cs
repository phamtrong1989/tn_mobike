﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class CustomerGroupResource : IAggregateRoot
    {
        public int Id { get; set; }

        public int CustomerGroupId { get; set; }

        public string Name { get; set; }

        public int LanguageId { get; set; }
        public string Language { get; set; }
    }
}
