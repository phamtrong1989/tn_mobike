﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class EmailManage : IAggregateRoot
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public int Port { get; set; }

        public string Host { get; set; }

        public string From { get; set; }

        public string CustomerDates { get; set; }

        public string CustomerTime { get; set; }

        public bool CustomerStatus { get; set; }

        public string EmployeeTime { get; set; }

        public bool EmployeeStatus { get; set; }

        public string AddEmail { get; set; }

        public string CCEmail { get; set; }

        public string BCEmail { get; set; }
    }
}