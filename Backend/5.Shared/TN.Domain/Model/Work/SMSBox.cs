﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class SMSBox : IAggregateRoot
    {
        public int Id { get; set; }

        [MaxLength(1000)] 
        public string PhoneNumber { get; set; }

        public long NotificationDataId { get; set; }

        [MaxLength(500)] 
        public string Content { get; set; }

        public bool IsSend { get; set; }

        public int ReSend { get; set; }

        public DateTime SendDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? SuccessfulSendDate { get; set; }
    }
}