﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class ProjectInvestor : IAggregateRoot
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }
    }
}