﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{

    public enum ECampaignProvisoType
    {
        [Display(Name = "Không điều kiện")]
        Default = 0,
        [Display(Name = "Đã xác thực tài khoản")]
        VerifyOke = 1,
        [Display(Name = "Áp dụng với 1 khách hàng chỉ định")]
        ForAccount = 2
    }

    
    public enum ECampaignStatus
    {
        Inactive = 0,
        Active = 1
    }

    public enum ECampaignType
    {
        [Display(Name = "Mã khuyến mại")]
        GiftCode = 0,

        [Display(Name = "Nạp điểm tặng điểm")]
        DepositEvent = 1,

        [Display(Name = "Mã giảm giá")]
        DiscountCode = 2
    }

    public class Campaign : IAggregateRoot
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public string Name { get; set; }

        public ECampaignStatus Status { get; set; } = ECampaignStatus.Inactive;

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public ECampaignType Type { get; set; }

        [NotMapped]
        public int TotalLimit { get; set; }

        [NotMapped]
        public int TotalCurentLimit { get; set; }

        public ApplicationUser CreatedUserObject { get; set; }

        public ApplicationUser UpdatedUserObject { get; set; }


        [NotMapped]
        public Project Project { get; set; }

        [NotMapped]
        public IEnumerable<CustomerGroup> CustomerGroups { get; set; }

        [NotMapped]
        public IEnumerable<DepositEvent> DepositEvents { get; set; }

        [NotMapped]
        public IEnumerable<VoucherCode> VoucherCodes { get; set; }
    }

    public class SumLimitModel
    {
        public int CampaignId { get; set; }
        public int Limit { get; set; }
        public int CurentLimit { get; set; }
    }
}
