﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EPostStatus: byte
    {
        // Chưa duyệt
        Default = 0,
        NotApproved = 1,
        Approved = 2
    }

    public class Post : IAggregateRoot
    {
        public int Id { get; set; }

        public int AccountRatingId { get; set; }

        [MaxLength(500)]
        public string Content { get; set; }

        [MaxLength(500)]
        public string Image { get; set; }

        [MaxLength(500)]
        public string Thumbnail { get; set; }

        public DateTime CreatedDate { get; set; }

        public int AccountId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int LikeCount { get; set; }

        public EPostStatus Status { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int? ApprovedUse { get; set; }

        [MaxLength(500)]
        public string NotApprovedNote { get; set; }

        [NotMapped]
        public bool IsLike { get; set; }

        [NotMapped]
        public Account Account { get; set; }

        [NotMapped]
        public string AccountRatingName { get; set; }
    }

    public class PostLike : IAggregateRoot
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public int AccountId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
