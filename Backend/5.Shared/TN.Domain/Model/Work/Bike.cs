﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Bike : IAggregateRoot
    {
        public enum EBikeStatus
        {
            [Display(Name = "Không hoạt động")]
            Inactive = 0,
            [Display(Name = "Đang hoạt động")]
            Active = 1,
            [Display(Name = "Đang lưu kho")]
            Warehouse = 2,
            [Display(Name = "Đang lỗi trong kho")]
            ErrorWarehouse = 3
        }

        public enum EBikeType
        {
            [Display(Name = "Xe đạp cơ")]
            XeDap = 0,
            [Display(Name = "Xe đạp điện")]
            XeDapDien = 1,
            [Display(Name = "Xe lai")]
            XeLai = 2
        }

        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int ModelId { get; set; }

        public int ProducerId { get; set; }

        public int ColorId { get; set; }

        public int WarehouseId { get; set; }

        public string Images { get; set; }

        [MaxLength(50)]
        public string Plate { get; set; }

        [MaxLength(50)]
        public string SerialNumber { get; set; }

        public EBikeStatus Status { get; set; }

        public EBikeType Type { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public double? Lat { get; set; }

        public double? Long { get; set; }

        public int StationId { get; set; }

        public int DockId { get; set; }

        public string Note { get; set; }

        // Ngày sản xuất
        public DateTime? ProductionDate { get; set; }
        // Ngày bắt đầu sử dụng
        public DateTime? StartDate { get; set; }
        // Xe sự cố
        public bool IsTrouble { get; set; } = false;
        // 
        public int MinuteRequiredMaintenance { get; set; } = 4800;

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }
        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }
        [NotMapped]
        public Dock Dock { get; set; }
        [NotMapped]
        public Warehouse Warehouse { get; set; }
        [NotMapped]
        public EDockBookingStatus BookingStatus { get; set; }
        [NotMapped]
        public Producer Producer { get; set; }
        [NotMapped]
        public Color Color { get; set; }
        [NotMapped]
        public Project Project { get; set; }
        [NotMapped]
        public Investor Investor { get; set; }
        [NotMapped]
        public Station Station { get; set; }
        [NotMapped]
        public Model Model { get; set; }
        [NotMapped]
        public BikeInfo BikeInfo { get; set; }
        [NotMapped]
        public int? NumMinuteRent { get; set; }
        [NotMapped]
        public BikeReport BikeReport { get; set; }
    }
}
