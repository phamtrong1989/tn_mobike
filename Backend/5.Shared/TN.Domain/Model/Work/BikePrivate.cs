﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class BikePrivate : IAggregateRoot
    {

        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        [MaxLength(50)]
        public string RFID { get; set; }

        [MaxLength(30)]
        public string Fullname { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        public DateTime? ScanDate { get; set; }
    }
}
