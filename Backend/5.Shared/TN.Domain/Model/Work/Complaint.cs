﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum ECustomerComplaintStatus : byte
    {
        [Display(Name = "Chưa xử lý")]
        NoProcess = 0,
        [Display(Name = "Đã xử lý")]
        Processed = 1
    }

    public enum ECustomerComplaintType
    {
        [Display(Name = "KNHT01 - KH yêu cầu hoàn lại tiền sau khi đã nạp mà không sử dụng dịch vụ")]
        KNHT01 = 1001,
        [Display(Name = "Không nạp được tiền do app TNGO lỗi, app nạp tiền đối tác lỗi")]
        KN01 = 2001,
        [Display(Name = "KN01 - Xe hỏng không sử dụng được")]
        KN02 = 2002,
        [Display(Name = "KN02 - Hết xe")]
        KN03 = 2003,
        [Display(Name = "KN04 - Không còn chỗ đậu xe")]
        KN04 = 2004
    }

    public enum ECustomerComplaintPattern
    {
        [Display(Name = "Hotline")]
        Hotline = 0,
        [Display(Name = "APP")]
        APP = 1,
        [Display(Name = "Email")]
        Email = 2
    }

    public class CustomerComplaint : IAggregateRoot
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        [MaxLength(30)]
        public string TransactionCode { get; set; }

        public int BikeId { get; set; }

        public ECustomerComplaintType BikeReportType { get; set; }

        // Mô tả lỗi
        [MaxLength(2000)]
        public string Description { get; set; }

        // Ghi chú
        [MaxLength(2000)]
        public string Note { get; set; }

        public string Files { get; set; }

        public ECustomerComplaintPattern Pattern { get; set; }

        public ECustomerComplaintStatus Status { get; set; }

        // Thời gian khiếu nại
        public DateTime ComplaintDate { get; set; }

        // Thời gian kết thúc (hoàn thành)
        public DateTime StartDate { get; set; }

        // Thời gian tiếp cận khách hàng
        public DateTime EndDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public Account Account { get; set; }
        [NotMapped]
        public string BikeReportTypeText { get; set; }
        [NotMapped]
        public Transaction Transaction { get; set; }
    }
}