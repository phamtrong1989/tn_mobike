﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum EProjectAccountSourceType:byte
    {
        Import = 1,
        Edit = 2,
        Auto = 3
    }

    public class ProjectAccount : IAggregateRoot
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int InvestorId { get; set; }

        public int AccountId { get; set; }

        public int CustomerGroupId { get; set; }

        public EProjectAccountSourceType SourceType { get; set; }

        public DateTime? DateImport { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int UpdatedUser { get; set; }

        [NotMapped]
        public Account Account { get; set; }
    }

    public class ProjectAccountModel
    {
        public int ProjectId { get; set; }
        public int CustomerGroupId { get; set; }
        public bool LocationProjectOverwrite { get; set; }
        public double Lng { get; set; }
        public double Lat { get; set; }
    }
}