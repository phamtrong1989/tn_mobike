﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class ApplicationUser : IdentityUser<int>, IAggregateRoot
    {
        [MaxLength(20)]
        public string Code { get; set; }

        public string Avatar { get; set; }

        [MaxLength(100)]
        public string DisplayName { get; set; }

        public bool IsLock { get; set; }

        [MaxLength(1000)]
        public string NoteLock { get; set; }

        public bool IsReLogin { get; set; }

        public bool IsSuperAdmin { get; set; } = false;

        public string Note { get; set; }

        public int? CreatedUserId { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? UpdatedUserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string Education { get; set; }

        [MaxLength(100)]
        public string FullName { get; set; }

        public DateTime? Birthday { get; set; }

        public ESex Sex { get; set; }

        // Địa chỉ tạm trú
        [MaxLength(200)]
        public string Address { get; set; }
        // Mã CMTND
        [MaxLength(50)]
        public string IdentificationID { get; set; }
        // Ngày cấp
        public DateTime? IdentificationSupplyDate { get; set; }
        // Nơi cấp
        [MaxLength(200)]
        public string IdentificationSupplyAdress { get; set; }
        // Địa chỉ CMTND
        [MaxLength(200)]
        public string IdentificationAdress { get; set; }
        // Mặt trước
        [MaxLength(500)]
        public string IdentificationPhotoFront { get; set; }
        // Mặt sau
        [MaxLength(500)]
        public string IdentificationPhotoBackside { get; set; }

        public int ManagerUserId { get; set; }

        public int RoleId { get; set; }

        public RoleManagerType RoleType { get; set; } = RoleManagerType.Default;
        // Nghỉ việc
        public bool IsRetired { get; set; }
        // Làm việc từ ngày
        public DateTime? WorkingDateFrom { get; set; }
        // Làm việc đến hết ngày
        public DateTime? WorkingDateTo { get; set; }

        [MaxLength(100)]
        public string ObjectIds { get; set; }

        // RFID
        [MaxLength(50)]
        public string RFID { get; set; }

        [NotMapped]
        public virtual List<ApplicationRole> Roles { get; set; }

        [NotMapped]
        public ApplicationUser ManagerUser { get; set; }

        [NotMapped]
        public ApplicationUser CreatedUserObject { get; set; }

        [NotMapped]
        public ApplicationUser UpdatedUserObject { get; set; }

        [NotMapped]
        public string RoleName { get; set; }

        [NotMapped]
        public decimal Revenue { get; set; }

        [NotMapped]
        public decimal RevenueSelf { get; set; }

        [NotMapped]
        public IEnumerable<ApplicationUser> CTVs { get; set; }
    }

    public class UserEditManagerModel
    {
        public int Id { get; set; }
        [Display(Name = "Ảnh đại diện")]
        public string Avatar { get; set; }
        [Display(Name = "Tên tài khoản")]
        public string UserName { get; set; }
        [Display(Name = "Số điện thoại")]
        [StringLength(100, ErrorMessage = "{0} ít nhất phải là {2} và tối đa {1} ký tự.", MinimumLength = 0)]
        public string PhoneNumber { get; set; }
        [Display(Name = "Tên hiển thị")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        [StringLength(100, ErrorMessage = "{0} ít nhất phải là {2} và tối đa {1} ký tự.", MinimumLength = 6)]
        public string DisplayName { get; set; }
        [Display(Name = "Mô tả lý do khóa")]
        [StringLength(1000, ErrorMessage = "{0} ít nhất phải là {2} và tối đa {1} ký tự.", MinimumLength = 0)]
        public string NoteLock { get; set; }
        [Display(Name = "Tích chọn để khóa tài khoản")]
        public bool IsLock { get; set; }
        [Display(Name = "Mô tả")]
        public string Note { get; set; }
        [Display(Name = "Chọn quyền tài khoản")]
        public List<int> Roles { get; set; }
        public IEnumerable<SelectListItem> RoleSelectListItem { get; set; }
        [Display(Name = "Mật khẩu mới")]
        [StringLength(15, ErrorMessage = "{0} ít nhất phải là {2} và tối đa {1} ký tự.", MinimumLength = 8)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$", ErrorMessage = "Mật khẩu phải từ 8 đến 15 ký tự, chữ cái đàu là viết hoa, và phải có ký tự đặc biệt và số.")]
        public string NewPassword { get; set; }
        [Display(Name = "Xác nhận thay đổi mật khẩu tài khoản này")]
        public bool IsChangePassword { get; set; }
    }
    public class UserProfileModel
    {
        public int Id { get; set; }
        [Display(Name = "Ảnh đại diện")]
        public string Avatar { get; set; }
        [Display(Name = "Tên tài khoản")]
        public string UserName { get; set; }
        [Display(Name = "Số điện thoại")]
        [StringLength(100, ErrorMessage = "{0} ít nhất phải là {2} và tối đa {1} ký tự.", MinimumLength = 0)]
        public string PhoneNumber { get; set; }
        [Display(Name = "Tên hiển thị")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        [StringLength(100, ErrorMessage = "{0} ít nhất phải là {2} và tối đa {1} ký tự.", MinimumLength = 6)]
        public string DisplayName { get; set; }
        [Display(Name = "Mô tả")]
        public string Note { get; set; }
    }
}
