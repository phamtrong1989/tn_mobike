﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum RoleManagerType
    {
        Default = -1,
        [Display(Name = "IT trí Nam")]
        Admin = 0,

        [Display(Name = "Kế toán Trí Nam")]
        Accounting_TN = 1,

        [Display(Name = "Hội đồng quản trị VTS")]
        BOD_VTS = 2,

        [Display(Name = "Kế toán trưởng VTS")]
        KTT_VTS = 3,

        [Display(Name = "Chăm sóc khách hàng & kinh doanh VTS")]
        CSKH_VTS = 4,

        [Display(Name = "Cộng tác viên VTS")]
        CTV_VTS = 5,

        [Display(Name = "Điều phối bảo dưỡng VTS")]
        Coordinator_VTS = 6,

        //[Display(Name = "Nhân viên quản lý trạm")]
        //StationManager_VTS = 7,
        //[Display(Name = "Quản lý cộng tác viên VTS")]
        //Manager_CTV_VTS = 8,

        [Display(Name = "Nhân viên cung ứng Trí Nam")]
        NVCU_TN = 9,

        [Display(Name = "Thủ kho VTS")]
        ThuKho_VTS = 10,

        [Display(Name = "Nhà đầu tư")]
        Investor = 11
    }

    public class ApplicationRole : IdentityRole<int>, IAggregateRoot
    {
        [Display(Name= "Description")]
        public string Description { get; set; }

        public RoleManagerType Type { get; set; }

        [NotMapped]
        public string TypeName { get; set; }

        [NotMapped]
        public RoleDetailObjModel RoleDetailObj { get; set; }

        [NotMapped]
        public List<RoleAction> RoleActions { get; set; }
    }

    public class RoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Description { get; set; }
        public string NormalizedName { get; set; }
        public int NumberOfUsers { get; set; }
    }

    public class RoleDetailObjModel
    {
        public List<RoleController> RoleControllers { get; set; }
        public List<RoleAction> RoleActions { get; set; }
        public List<RoleDetail> RoleDetails { get; set; }
        public List<RoleGroup> RoleGroups { get; set; }
    }
}
