﻿using System.ComponentModel.DataAnnotations;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Parameter : IAggregateRoot
    {
        public enum ParameterType: byte
        {
            Category
        }

        [Key]
        public int Id { get; set; }
        public ParameterType Type { get; set; }
        public string Value { get; set; }
    }
}
