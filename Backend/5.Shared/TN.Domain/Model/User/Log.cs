﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum LogStatus
    {
        Data,
        Default
    }
    public enum LogType
    {
        Other,
        Insert,
        Update,
        Delete
    }
    public class Log : IAggregateRoot
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(1000)]
        public string Action { get; set; }
        [MaxLength(50)]
        public string Object { get; set; }
        public int ObjectId { get; set; }
        [MaxLength(50)]
        public string ObjectType { get; set; }
        public DateTime CreatedDate { get; set; }
        public int SystemUserId { get; set; }
        public LogType Type { get; set; }

        [NotMapped]
        public ApplicationUser SystemUserObject { get; set; }

        [NotMapped]
        public RoleController RoleController { get; set; }
    }

    public class BaseLogDataModel<T> where T : class
    {
        public T DataBefore { get; set; }
        public T DataAfter { get; set; }
    }
    public class BaseLogModel<T> where T : class
    {
        public T DataBefore { get; set; }
        public T DataAfter { get; set; }
        public Log Log { get; set; }
    }
    public class WriteLogModel
    {
        //string name,int objectId,object valueBefore, object valueAfter, string table=null
        public string Name { get; set; }
        public int ObjectId { get; set; }
    }
    public class LogModel
    {
        public string Action { get; set; }
        public string ControllerName { get; set; }
        public int ObjectId { get; set; }
        public string TableName { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime CreatedDate { get; set; }
        public object ValueBefore
        {
            set
            {
                if (value == null)
                {
                    StrValueBefore = null;
                }
                else
                {
                    StrValueBefore = "";
                }
            }
        }
        public object ValueAfter
        {
            set
            {
                if (value == null)
                {
                    StrValueAfter = null;
                }
                else
                {
                    StrValueAfter = "";
                }
            }
        }
        public LogType Type { get; set; } = LogType.Other;
        public LogStatus Status { get; set; } = LogStatus.Data;
        public string StrValueBefore { get; set; }
        public string StrValueAfter { get; set; }
    }
}
