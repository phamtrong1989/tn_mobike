﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Model;

namespace PT.Domain.Model
{
   
    public enum DockEventType:byte
    {
        [Display(Name = "Không")]
        Not = 0,
        [Display(Name = "Đóng khóa")]
        DockClose = 1,
        [Display(Name = "Mở khóa")]
        ScanRFID = 2,
        [Display(Name = "Quản trị kết thúc chuyến")]
        EndBooking = 3,
        [Display(Name = "Quản trị kết hủy chuyến")]
        CancelBooking = 4,
        [Display(Name = "Momo phản hồi chuyển tiền")]
        MomoRespond = 5,
        [Display(Name = "Zalo phản hồi chuyển tiền")]
        ZaloRespond = 6,
        [Display(Name = "Viettel phản hồi chuyển tiền")]
        ViettelRespond = 7,
        [Display(Name = "Payoo phản hồi chuyển tiền")]
        PayooRespond = 8,
    }

    public class FireBaseMSGModel
    {
        public DockEventType Type { get; set; }
        public string Content { get; set; }
        public string Time { get; set; }
    }

    public class GPSData
    {
        public object Id { get; set; }
        public string IMEI { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public string CreateTime { get; set; }
        public long CreateTimeTicks { get; set; }
        public long Date { get; set; }
        public Station Station { get; set; }
    }

    public enum EnumMongoLogType
    {
        Start = 0,
        Center,
        End
    }

    public enum EnumSystemType
    {
        APIAPP = 0,
        APIBackend = 1,
        DockService = 2,
        ControlService = 3,
        None = 99
    }

    public class MongoLog
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public EnumMongoLogType Type { get; set; }
        public string Content { get; set; }
        public int AccountId { get; set; }
        public string LogKey { get; set; }
        public string DeviceKey { get; set; }
        public string FunctionName { get; set; }
        public string CreateTime { get; set; }
        public EnumSystemType SystemType { get; set; }
        public long CreateTimeTicks { get; set; }
        public bool IsExeption { get; set; }
        public long Date { get; set; }
    }

    public class EventSystemMongo
    {
        public EventSystemMongo()
        {
            int type = (int)Type;
            if (type >= 1000 && type < 2000)
            {
                Group = EEventSystemGroup.CanhBaoXeKhoa;
            }
            else if (type >= 2000 && type < 3000)
            {
                Group = EEventSystemGroup.CanhBaoTram;
            }
            else if (type >= 3000 && type < 4000)
            {
                Group = EEventSystemGroup.CanhBaoGiaoDich;
            }
            else if (type >= 4000 && type < 5000)
            {
                Group = EEventSystemGroup.CanhBaoCSKH;
            }
        }

        [BsonId]
        public ObjectId _id { get; set; }
        public long Id { get; set; }
        public int ProjectId { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public long ParentId { get; set; } = 0;

        public int UserId { get; set; }

        public int AccountId { get; set; }

        public int StationId { get; set; }

        public string TransactionCode { get; set; }

        public int BikeId { get; set; }

        public int DockId { get; set; }

        public string Content { get; set; }

        public string Datas { get; set; }

        public bool IsMultiple { get; set; }

        public bool IsFlag { get; set; } = false;

        public bool IsProcessed { get; set; } = false;

        public EEventSystemType Type { get; set; }

        public EEventSystemGroup Group { get; set; }

        public EEventSystemWarningType WarningType { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public RoleManagerType RoleType { get; set; } = RoleManagerType.Default;
        public bool IsJob { get; set; } = false;
        public int Date { get; set; }
        public long CreatedDateTicks { get; set; }
        public long UpdatedDateTicks { get; set; }
    }
}
