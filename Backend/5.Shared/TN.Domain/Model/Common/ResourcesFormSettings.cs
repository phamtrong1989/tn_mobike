﻿using System.Collections.Generic;

namespace TN.Domain.Model
{
    public class ResponseCodeResources
    {
        public string Language { get; set; }
        public List<ResponseCodeResourcesData> Data { get; set; }
    }

    public class ResponseCodeResourcesData
    {
        public int Code { get; set; }
        public string Text { get; set; }
    }

    public class ResourcesForm
    {
        public string Language { get; set; }
        public LandingForm Landing { get; set; }
        public LoginForm Login { get; set; }
        public RegisterOTPForm RegisterOTP { get; set; }
        public RegisterOTPConfirmForm RegisterOTPConfirm { get; set; }
        public RegisterCreateForm RegisterCreate { get; set; }
        public RegisterTermsOfUseForm RegisterTermsOfUse { get; set; }

        public AccountInfoForm AccountInfo { get; set; }
        public AccountInfoEditForm AccountInfoEdit { get; set; }
        public AccountVerifyEditForm AccountVerifyEdit { get; set; }
        public ChangePasswordRetypeForm ChangePasswordRetype { get; set; }
        public ChangePasswordConfirmForm ChangePasswordConfirm { get; set; }
        public SettingForm Setting { get; set; }
        public MenuLeftForm MenuLeft { get; set; }
        public UserManualForm UserManual { get; set; }

        public PromotionForm Promotion { get; set; }
        public InviteFriendsForm InviteFriends { get; set; }
        public MailboxForm Mailbox { get; set; }
        public MyTripForm MyTrip { get; set; }
        public MyTripDetailForm MyTripDetail { get; set; }
        public NearestStationForm NearestStation { get; set; }
        public BuyTicketForm BuyTicket { get; set; }
        public MyWalletForm MyWallet { get; set; }
        public SharePointForm SharePoint { get; set; }
        public AddPointForm AddPoint { get; set; }
        public IndexForm Index { get; set; }

        public ViewBikeForm ViewBike { get; set; }
        public BookingForm Booking { get; set; }
        public ReportErrorForm ReportError { get; set; }
        public BookingFinishForm BookingFinish { get; set; }

    }

    public class BookingFinishForm
    {
        public string Title { get; set; }
        public string Price { get; set; }
        public string Stars { get; set; }
        public string BtnDetail { get; set; }
        public string Description { get; set; }
        public string NotePlaceholder { get; set; }
        public string BtnSend { get; set; }
    }

    public class ReportErrorForm
    {
        public string Title { get; set; }
        public string Plate { get; set; }
        public string Problem { get; set; }
        public string BikeReportType_0 { get; set; }
        public string BikeReportType_1 { get; set; }
        public string BikeReportType_2 { get; set; }
        public string BikeReportType_3 { get; set; }
        public string BikeReportType_4 { get; set; }
        public string BikeReportType_5 { get; set; }
        public string BikeReportType_6 { get; set; }
        public string BikeReportType_7 { get; set; }
        public string BikeReportType_8 { get; set; }
        public string BtnTakePhoto { get; set; }
        public string NotePlaceholder { get; set; }
        public string BtnSubmit { get; set; }
        public string BtnCallHotline { get; set; }
    }

    public class BookingForm
    {
        public string StartNote1 { get; set; }
        public string StartNote2 { get; set; }
        public string BtnNext { get; set; }
        public string Status0 { get; set; }
        public string Status1 { get; set; }
        public string Time { get; set; }
        public string TotalPrice { get; set; }
        public string BatteryLife { get; set; }
        public string BtnHelp { get; set; }
        public string BtnCancel { get; set; }
        public string BtnEnd { get; set; }
        public string BtnReport { get; set; }
        public string BtnOpenLock { get; set; }
        public string AlertEndNote { get; set; }
        public string AlertEndOk { get; set; }
        public string AlertEndNot { get; set; }
    }

    public class ViewBikeForm
    {
        public string Plate { get; set; }
        public string Pin { get; set; }
        public string Help { get; set; }
        public string Report { get; set; }
        public string AccountPoint { get; set; }
        public string AccountSubPoint { get; set; }
        public string TicketType { get; set; }
        public string Promotion { get; set; }
        public string TicketNote { get; set; }
        public string BtnStart { get; set; }
        public string BtnOpen { get; set; }
    }

    public class IndexForm
    {
        public string SearchPlaceholder { get; set; }
        public string FromPlaceholder { get; set; }
        public string ToPlaceholder { get; set; }
        public string ListStation { get; set; }
        public string BikeCount { get; set; }
        public string Places { get; set; }
        public string BtnReport { get; set; }
        public string BtnHelp { get; set; }
        public string BtnOpen { get; set; }
        public string AlertLowBatteryNote { get; set; }
        public string AlertLowBatteryBtnOke { get; set; }
        public string AlertLowBatteryBtnNo { get; set; }
        public string QRTitle { get; set; }
        public string QRNote { get; set; }
        public string QRBtnInsertCode { get; set; }
        public string QRBtnFlashlight { get; set; }
        public string QRCodeTitle { get; set; }
        public string QRCodeTitleTiny { get; set; }
        public string QRCodePlaceholder { get; set; }
        public string QRCodeBtnOpen { get; set; }
        public string QRCodeBtnScanCode { get; set; }
    }

    public class AddPointForm
    {
        public string Title { get; set; }
        public string ChoseMoney { get; set; }
        public string Pad { get; set; }
        public string Type { get; set; }
        public string VTCPAY { get; set; }
        public string Momo { get; set; }
        public string ZaloPay { get; set; }
    }

    public class SharePointForm
    {
        public string Title { get; set; }
        public string BtnBuy { get; set; }
        public string History { get; set; }
        public string BtnShare { get; set; }
        public string BtnReShare { get; set; }
        public string ChoseType { get; set; }
        public string SelectPoint { get; set; }
        public string ToAccount { get; set; }
        public string WarningNote { get; set; }
        public string MsgTitle { get; set; }
        public string AccountPoint { get; set; }
        public string AccountSubPoint { get; set; }
        public string BtnShareHistory { get; set; }
    }

    public class MyWalletForm
    {
        public string Title { get; set; }
        public string AccountPoint { get; set; }
        public string AccountSubPoint { get; set; }
        public string Point { get; set; }
        public string Expiry { get; set; }
        public string Debt { get; set; }
        public string BtnAdd { get; set; }
        public string BtnPromotion { get; set; }
        public string BtnSharePoint { get; set; }
        public string History { get; set; }
        public string Time { get; set; }
        public string WalletTransactionType_0 { get; set; }
        public string WalletTransactionType_1 { get; set; }
        public string WalletTransactionType_2 { get; set; }
        public string WalletTransactionType_3 { get; set; }
        public string WalletTransactionType_4 { get; set; }
        public string WalletTransactionType_5 { get; set; }
        public string WalletTransactionType_6 { get; set; }
        public string WalletTransactionType_7 { get; set; }
        public string WalletTransactionType_8 { get; set; }
        public string WalletTransactionType_9 { get; set; }
        public string WalletTransactionType_10 { get; set; }
        public string WalletTransactionType_11 { get; set; }
        public string WalletTransactionType_12 { get; set; }
        public string WalletTransactionType_13 { get; set; }
        public string WalletTransactionType_14 { get; set; }
        public string WalletTransactionType_15 { get; set; }
        public string WalletTransactionType_16 { get; set; }
    }

    public class BuyTicketForm
    {
        public string Title { get; set; }
        public string BtnBuy { get; set; }
        public string History { get; set; }
        public string Point { get; set; }
        public string Project { get; set; }
        public string TicketType { get; set; }
        public string Expiry { get; set; }
        public string Price { get; set; }
        public string Note { get; set; }
        public string BtnSubmit { get; set; }
    }

    public class NearestStationForm
    {
        public string Title { get; set; }
        public string BikeCount { get; set; }
        public string Places { get; set; }
    }

    public class MyTripDetailForm
    {
        public string Point { get; set; }
        public string Distance { get; set; }
        public string Plate { get; set; }
        public string Type { get; set; }
        public string PricePoint { get; set; }
        public string PriceSubPoint { get; set; }
        public string PriceDebt { get; set; }
        public string Minute { get; set; }
        public string Carbon { get; set; }
        public string Calories { get; set; }
        public string BtnShare { get; set; }
        public string TransactionCode { get; set; }
        public string BtnCopy { get; set; }
    }

    public class MyTripForm
    {
        public string Title { get; set; }
        public string Point { get; set; }
        public string TicketPrice_TicketType_1 { get; set; }
        public string TicketPrice_TicketType_2 { get; set; }
        public string TicketPrice_TicketType_3 { get; set; }
        public string Distance { get; set; }
        public string Carbon { get; set; }
        public string Calories { get; set; }
    }

    public class MailboxForm
    {
        public string Title { get; set; }
        public string Notify { get; set; }
        public string NotifyTiny { get; set; }
        public string Promotion { get; set; }
        public string PromotionTiny { get; set; }
        public string News { get; set; }
        public string NewsTiny { get; set; }
    }

    public class InviteFriendsForm
    {
        public string Title { get; set; }
        public string Code { get; set; }
        public string BtnShare { get; set; }
    }

    public class PromotionForm
    {
        public string CodePlaceholder { get; set; }
        public string Star { get; set; }
        public string Tab1 { get; set; }
        public string Tab2 { get; set; }
        public string Tab2Title { get; set; }
        public string Tab2Note { get; set; }
        public string Tab2Name { get; set; }
        public string BtnSubmit { get; set; }
    }

    public class UserManualForm
    {
        public string NameTab1 { get; set; }
        public string NoteTab1 { get; set; }
        public string NameTab2 { get; set; }
        public string NoteTab2 { get; set; }
        public string NameTab3 { get; set; }
        public string NoteTab3 { get; set; }
        public string BtnStart { get; set; }
        public string BtnNext { get; set; }
        public string BtnSkip { get; set; }
    }

    public class MenuLeftForm
    {
        public string AccountInfo { get; set; }
        public string MyWallet { get; set; }
        public string BuyTicket { get; set; }
        public string NearestStation { get; set; }
        public string MyTrip { get; set; }
        public string Mailbox { get; set; }
        public string InviteFriends { get; set; }
        public string Promotion { get; set; }
        public string UserManual { get; set; }
        public string Introduce { get; set; }
        public string Setting { get; set; }
        public string Version { get; set; }
        public string Point { get; set; }
        public string Gold { get; set; }
        public string AccountVerifyStatus_0 { get; set; }
        public string AccountVerifyStatus_1 { get; set; }
        public string AccountVerifyStatus_2 { get; set; }
        public string AccountVerifyStatus_3 { get; set; }
    }

    public class SettingForm
    {
        public string Title { get; set; }
        public string BtnChangePassword { get; set; }
        public string BtnTermsOfUse { get; set; }
        public string BtnPolicyRegulations { get; set; }
        public string BtnAppReviews { get; set; }
        public string BtnLogOut { get; set; }
        public string AlertLogoutText { get; set; }
        public string BtnAlertLogoutYes { get; set; }
        public string BtnAlertLogoutNo { get; set; }
    }

    public class ChangePasswordConfirmForm
    {
        public string Title { get; set; }
        public string Note { get; set; }
        public string PINRetype { get; set; }
        public string BtnHiden { get; set; }
        public string BtnShow { get; set; }
        public string BtnNext { get; set; }
    }

    public class ChangePasswordRetypeForm
    {
        public string Title { get; set; }
        public string Note { get; set; }
        public string BtnHiden { get; set; }
        public string BtnShow { get; set; }
        public string BtnNext { get; set; }
    }

    public class AccountVerifyEditForm
    {
        public string Title { get; set; }
        public string IdentificationType_0 { get; set; }
        public string IdentificationType_1 { get; set; }
        public string IdentificationType_2 { get; set; }
        public string PhotoFront { get; set; }
        public string PhotoFrontNote { get; set; }
        public string AlertConfirm { get; set; }
        public string BtnAlertConfirmRetry { get; set; }
        public string BtnAlertConfirmOk { get; set; }
        public string Backside { get; set; }
        public string BacksideNote { get; set; }
    }

    public class AccountInfoEditForm
    {
        public string Point { get; set; }
        public string Fullname { get; set; }
        public string FullnamePlaceholder { get; set; }
        public string FullnameRequired { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string IdentificationID { get; set; }
        public string IdentificationIDPlaceholder { get; set; }
        public string BirthdayDayPlaceholder { get; set; }
        public string BirthdayMonthPlaceholder { get; set; }
        public string BirthdayYearPlaceholder { get; set; }
        public string Sex { get; set; }
        public string SexFemale { get; set; }
        public string SexMale { get; set; }
        public string SexOther { get; set; }
        public string Address { get; set; }
        public string AddressPlaceholder { get; set; }
        public string BtnVerify { get; set; }
        public string BtnSave { get; set; }
        public string AccountVerifyStatus_0 { get; set; }
        public string AccountVerifyStatus_1 { get; set; }
        public string AccountVerifyStatus_2 { get; set; }
        public string AccountVerifyStatus_3 { get; set; }
    }

    public class AccountInfoForm
    {
        public string Point { get; set; }
        public string Fullname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string IdentificationID { get; set; }
        public string Birthday { get; set; }
        public string Sex { get; set; }
        public string SexFemale { get; set; }
        public string SexMale { get; set; }
        public string SexOther { get; set; }
        public string Address { get; set; }
        public string AddressNull { get; set; }
        public string BtnVerify { get; set; }
        public string BtnEdit { get; set; }
        public string InfoNull { get; set; }
        public string AccountVerifyStatus_0 { get; set; }
        public string AccountVerifyStatus_1 { get; set; }
        public string AccountVerifyStatus_2 { get; set; }
        public string AccountVerifyStatus_3 { get; set; }
    }

    public class RegisterTermsOfUseForm
    {
        public string Title { get; set; }
        public string Note { get; set; }
        public string CheckTermsOfUse { get; set; }
        public string BtnTermsOfUse { get; set; }
        public string CheckUserAgreement { get; set; }
        public string BtnUserAgreement { get; set; }
        public string BtnNext { get; set; }
    }

    public class RegisterCreateForm
    {
        public string Title { get; set; }
        public string Fullname { get; set; }
        public string FullnamePlaceholder { get; set; }
        public string Email { get; set; }
        public string Birthday { get; set; }
        public string BirthdayDayPlaceholder { get; set; }
        public string BirthdayMonthPlaceholder { get; set; }
        public string BirthdayYearPlaceholder { get; set; }
        public string Sex { get; set; }
        public string SexFemale { get; set; }
        public string SexMale { get; set; }
        public string SexOther { get; set; }
        public string ShareCode { get; set; }
        public string ShareCodePlaceholder { get; set; }
        public string BtnNext { get; set; }
    }

    public class RegisterOTPConfirmForm
    {
        public string Note { get; set; }
        public string Time { get; set; }
        public string BtnRetry { get; set; }
        public string BtnNext { get; set; }
    }

    public class RegisterOTPForm
    {
        public string EnterPhoneNumber { get; set; }
        public string ChooseOTPType { get; set; }
        public string BtnOTPZalo { get; set; }
        public string BtnOTPSMS { get; set; }
        public string Note { get; set; }
        public string BtnNext { get; set; }
    }

    public class LandingForm
    {
        public string Login { get; set; }
        public string Register { get; set; }
        public string ForgotPassword { get; set; }
    }

    public class LoginForm
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string BtnLogin { get; set; }
    }
}
