﻿using Newtonsoft.Json;

namespace TN.Domain.Model
{
    public class ZNSMessage
    {
        [JsonProperty(PropertyName = "mode")]
        public string Mode { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "template_id")]
        public string TemplateId { get; set; }  

        [JsonProperty(PropertyName = "template_data")]
        public object TemplateData { get; set; }

        [JsonProperty(PropertyName = "tracking_id")]
        public string TrackingId { get; set; } 
    }

    public class ZNSTemplatWarningData
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "plate")]
        public string Plate { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string Date { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }

    public class ZNSTemplatOTPData     
    {
        [JsonProperty(PropertyName = "otp")]
        public string OTP { get; set; }
    }

    public class ZNSRespone
    {
        [JsonProperty(PropertyName = "error")]
        public int Error { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }

    public class ZNSSendMsg : ZNSRespone
    {
        [JsonProperty(PropertyName = "data")]
        public SendMsgData Data { get; set; }
    }

    public class SendMsgData
    {
        [JsonProperty(PropertyName = "msg_id")]
        public string MsgId { get; set; }

        [JsonProperty(PropertyName = "sent_time")]
        public string SentTime { get; set; }

        [JsonProperty(PropertyName = "quota")]
        public ZNSQuota Quota { get; set; }
    }

    public class ZNSQuota
    {
        [JsonProperty(PropertyName = "dailyQuota")]
        public int DailyQuota { get; set; }

        [JsonProperty(PropertyName = "remainingQuota")]
        public int RemainingQuota { get; set; }
    }

    public class ZaloSettings
    {
        public string AppId { get; set; }
        public string SecretKeyCreateOrder { get; set; }
        public string SecretKeyCallback { get; set; }
        public string RefundUrl { get; set; }
        public string QueryOrderUrl { get; set; }
        public string CreateOrderUrl { get; set; }
        public string QueryRefundUrl { get; set; }
        public string Description { get; set; }
        public string MerchantCode { get; set; }
        public string MerchantName { get; set; }
        public string MerchantNameLabel { get; set; }
        public string Language { get; set; }
        public string NotifyResultUrl { get; set; }
        public string ZaloSMSToken { get; set; }

        public string ZaloSMSMode { get; set; }
        public string ZaloSMSOTPRegisterTemplateId { get; set; }
        public string ZaloSMSSendWarningTemplateId { get; set; }
        public string ZaloSMSSendPath { get; set; }
        public string ZaloRecheckOrderPath { get; set; }
        public bool IsSendWarning { get; set; }

    }
}
