﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TN.Domain.Model.Common
{

    public class SMSModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string brandname { get; set; }
        public string message { get; set; }
        public string phone { get; set; }
        public string @ref { get; set; }
        public int type { get; set; }
    }

    public class SMSV2Model
    {
        
        public string from { get; set; }
        public string text { get; set; }
        public string to { get; set; }
    }

    public class SMSResultModel
    {
        public int status;
        public string transaction;
        public string data;
    }

    public class SMSResultV2Model
    {
        public int Status { get; set; }
        public string Errorcode { get; set; }
        public string Carrier { get; set; }
        public string Mnp { get; set; }
    }

    public class DockInfoByStationModel
    {
        public int StationId { get; set; }
        public int TotalDock { get; set; }
        public int TotalDockFree { get; set; }
    }

    public class ApiResponseData<T> where T : class
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long Count { get; set; }
        public T Data { get; set; }
        public int Status { get; set; } = 0;
        public string Message { get; set; }
        public object OutData { get; set; }
    }

    public class DataItem
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }

    public class ListData
    {
        public static List<LanguageModel> ListLanguage
        {
            get
            {
                return new List<LanguageModel>()
                {
                    new LanguageModel()
                    {
                        Id = "vi",
                        Id2="vi-VN",
                        Name = "Tiếng Việt",
                        Name2 = "Tiếng Việt",
                        Icon = "/Content/Admin/images/vi.png",
                        Icon2 = "/Content/Admin/images/vi.png"

                    }
                    //,
                    //new LanguageModel()
                    //{
                    //    Id = "en",
                    //    Id2="en-US",
                    //    Name = "Tiếng Anh",
                    //    Name2 = "English",
                    //    Icon = "/Content/Admin/images/en.png",
                    //    Icon2 = "/Content/Admin/images/en.png"
                    //}
                };
            }
        }
    }

    public class LanguageModel
    {
        public string Id { get; set; }
        public string Id2 { get; set; }

        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Icon { get; set; }
        public string Icon2 { get; set; }
    }

    public class DashboardTransactionsModel
    {
        public double? TotalKCal { get; set; }
        public double? TotalKG { get; set; }
        public double? TotalKM { get; set; }
        public string TotalKMStr { get; set; }
    }
}
