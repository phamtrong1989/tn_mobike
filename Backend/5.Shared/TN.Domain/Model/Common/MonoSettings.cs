﻿namespace TN.Domain.Model
{
    public class MonoSettings
    {
        public string Action { get; set; }
        public string Partner { get; set; }
        public string AppScheme { get; set; }
        public string Description { get; set; }
        public string MerchantCode { get; set; }
        public string MerchantName { get; set; }
        public string MerchantNameLabel { get; set; }
        public string Language { get; set; }
        public int Fee { get; set; }
        public string Username { get; set; }
        public string PublicKey { get; set; }
        public string PathSetPendingMoney { get; set; }
        public string PathNotifyConfirm { get; set; }
        public string StoreId { get; set; }
        public string StoreName { get; set; }
        public string SecretKey { get; set; }
        public string PathQueryStatus { get; set; }
        public string PathRefund { get; set; }
    }
}
