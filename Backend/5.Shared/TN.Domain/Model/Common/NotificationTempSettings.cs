﻿using System;
using System.Collections.Generic;

namespace TN.Domain.Model
{
    public class NotificationTemp
    {
        public string Language { get; set; }
        public List<NotificationTempData> Data { get; set; }
    }

    public class NotificationTempData
    {
        public ENotificationTempType Type { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string ImageUrl { get; set; }
    }
}
