﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PT.Domain.Model
{
    public class ApiSettings
    {
        public string TokenIssuer { get; set; }
        public string TokenAudience { get; set; }
        public string TokenKey { get; set; }
        public string ApplicationAeskey { get; set; }
        public string UrlHub { get; set; }
        public string APILogin { get; set; }
        public string UseCors { get; set; }
        public string AllowReferer { get; set; }

        public string ApplicationAesIV
        {
            get
            {
                return "0000TN@@(68TRONg";
            }
        }
    }
}
