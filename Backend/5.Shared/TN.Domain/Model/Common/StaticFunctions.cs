﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace PT.Domain.Model
{
    public static class StaticFunctions
    {
        public static string GetDisplayName(this Enum value)
        {
            try
            {
                Type enumType = value.GetType();
                var enumValue = Enum.GetName(enumType, value);
                MemberInfo member = enumType.GetMember(enumValue)[0];

                var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
                var outString = ((DisplayAttribute)attrs[0]).Name;

                if (((DisplayAttribute)attrs[0]).ResourceType != null)
                {
                    outString = ((DisplayAttribute)attrs[0]).GetName();
                }
                return outString;
            }
            catch
            {
                return value.ToString();
            }
        }

        public static string GetDisplayName(this Enum value, string lang)
        {
            try
            {
                Type enumType = value.GetType();
                var enumValue = Enum.GetName(enumType, value);
                MemberInfo member = enumType.GetMember(enumValue)[0];

                var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
                var outString = ((DisplayAttribute)attrs[0]).Name;

                if (((DisplayAttribute)attrs[0]).ResourceType != null)
                {
                    outString = ((DisplayAttribute)attrs[0]).GetName();
                }
                var arr = outString.Split("|");
                if (lang == "vi")
                {
                    return arr[0];
                }
                else if (lang == "en")
                {
                    return arr[1];
                }
                else
                {
                    return arr[0];
                }
            }
            catch
            {
                return value.ToString();
            }
        }
    }
}
