﻿using TN.Domain.Model;

namespace TN.Domain.ViewModels
{
    public class CampaignResult
    {
        public Project Project { get; set; }
        public CustomerGroup CustomerGroup { get; set; }
        public Campaign Campaign { get; set; }
        public string Code { get; set; }
        public decimal? Point { get; set; }
        public int? Percent { get; set; }
        public decimal? StartAmount { get; set; }
        public int Limit { get; set; }

        public Account Customer { get; set; }

        public WalletTransaction WalletTransaction { get; set; }
    }
}