﻿using APIZaloPay.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZaloPay.Helper;
using ZaloPay.Helper.Crypto;

namespace APIZaloPay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class APIZaloPayController : ControllerBase
    {
        static string app_id = "1404";
        static string key1 = "B1vdr2c1pWFURbP4dN1apFD1o0pUpBSW";
        static string key2 = "gLaaduaCBxpjrlABp5OLjNQge4pPq8mJ";
        static string refund_url = "https://openapi.zalopay.vn/v2/refund";
        static string query_order_url = "https://openapi.zalopay.vn/v2/query";
        static string create_order_url = "https://openapi.zalopay.vn/v2/create";
        static string query_refund_url = "https://openapi.zalopay.vn/v2/query_refund";

        private readonly ILogger _logger;

        public APIZaloPayController(ILogger<APIZaloPayController> logger)
        {
            _logger = logger;
        }

        [Route("CreateOrder")]
        [HttpPost]
        public async Task<ActionResult<string>> CreateOrderAsync([FromBody] Order order)
        {
            var param = new Dictionary<string, string>();
            var app_trans_id = DateTime.Now.ToString("yyMMdd") + "_" + Guid.NewGuid().ToString("N");
            // Field request
            param.Add("app_user", order.app_user);
            param.Add("amount", order.amount);
            param.Add("embed_data", JsonConvert.SerializeObject(order.embed_data));
            param.Add("item", JsonConvert.SerializeObject(order.item));
            param.Add("bank_code", order.bank_code);

            // Field setting
         
            param.Add("app_trans_id", app_trans_id);
            param.Add("app_id", app_id);
            param.Add("app_time", Utils.GetTimeStamp().ToString());
            param.Add("description", "TNGO - Thanh toán đơn hàng " + app_trans_id);
            //param.Add("callback_url", $"{this.Request.Scheme}://{this.Request.Host}/api/apizalopay/callback");

            var hmacinput = app_id + "|" + param["app_trans_id"] + "|" + param["app_user"] + "|" + param["amount"] + "|" + param["app_time"] + "|" + param["embed_data"] + "|" + param["item"];
            param.Add("mac", HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, key1, hmacinput));

            var result = await HttpHelper.PostFormAsync(create_order_url, param);
            //
            return JsonConvert.SerializeObject(result);
        }

        [Route("Callback")]
        [HttpPost]
        public ActionResult<string> Callback([FromBody] Callback cb)
        {
            var result = new Dictionary<string, object>();

            try
            {
                var mac = HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, key2, cb.data);

                // kiểm tra callback hợp lệ (đến từ ZaloPay server)
                if (!cb.mac.Equals(mac))
                {
                    // callback không hợp lệ
                    result["return_code"] = -1;
                    result["return_message"] = "mac not equal";
                }
                else
                {
                    // thanh toán thành công
                    // merchant cập nhật trạng thái cho đơn hàng

                    // ĐỔI TRẠNG THÁI ĐƠN HÀNG TRONG DB

                    _logger.LogInformation("ZALO CALLBACK");
                    _logger.LogInformation(cb.ToString());

                    result["return_code"] = 1;
                    result["return_message"] = "success";
                }
            }
            catch (Exception ex)
            {
                result["return_code"] = 0; // ZaloPay server sẽ callback lại (tối đa 3 lần)
                result["return_message"] = ex.Message;
            }

            // thông báo kết quả cho ZaloPay server
            return Ok(result);
        }

        [Route("GetOrderStatus/{app_trans_id}")]
        [HttpGet]
        public async Task<ActionResult<string>> GetOrderStatusAsync(string app_trans_id)
        {
            var param = new Dictionary<string, string>
            {
                { "app_id", "1404" },
                { "app_trans_id", app_trans_id }
            };
            var hmacinput = app_id + "|" + app_trans_id + "|" + key1;

            param.Add("mac", HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, key1, hmacinput));

            var result = await HttpHelper.PostFormAsync(query_order_url, param);

            return JsonConvert.SerializeObject(result);
        }

        [Route("Refund")]
        [HttpPost]
        public async Task<ActionResult<string>> RefundAsync([FromBody] Refund refund)
        {
            //Dictionary<string, string> param = new Dictionary<string, string>();
            Dictionary<string, string> param = refund.GetType().GetProperties().ToDictionary(x => x.Name, x => x.GetValue(refund)?.ToString() ?? "");

            //param.Add("m_refund_id", refund.m_refund_id);
            //param.Add("zp_trans_id", refund.zp_trans_id);
            //param.Add("description", refund.description);
            //param.Add("amount", refund.amount.ToString());
            param.Add("timestamp", Utils.GetTimeStamp().ToString());
            param.Add("app_id", app_id);

            var hmacinput = app_id + "|" + param["zp_trans_id"] + "|" + param["amount"] + "|" + param["description"] + "|" + param["timestamp"];
            param.Add("mac", HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, key1, hmacinput));

            var result = await HttpHelper.PostFormAsync(refund_url, param);
            //
            return JsonConvert.SerializeObject(result);
        }

        [Route("GetRefundStatus")]
        [HttpGet]
        public async Task<ActionResult<string>> GetRefundStatusAsync(string m_refund_id)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("app_id", app_id);
            param.Add("timestamp", Utils.GetTimeStamp().ToString());
            param.Add("m_refund_id", m_refund_id);

            var hmacinput = app_id + "|" + param["m_refund_id"] + "|" + param["timestamp"];
            param.Add("mac", HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, key1, hmacinput));

            var result = await HttpHelper.PostFormAsync(query_refund_url, param);

            return JsonConvert.SerializeObject(result);
        }

        [Route("VerifyMUID")]
        [HttpGet]
        public async Task<ActionResult<string>> VerifyMUIDAsync(string muid, string maccesstoken, string systemlogin = "1")
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("muid", muid);
            param.Add("maccesstoken", maccesstoken);
            param.Add("systemlogin", systemlogin);

            var result = await HttpHelper.PostFormAsync(query_refund_url, param);

            return JsonConvert.SerializeObject(result);
        }
    }
}