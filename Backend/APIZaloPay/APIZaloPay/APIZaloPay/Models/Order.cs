﻿using System.ComponentModel.DataAnnotations;

namespace APIZaloPay.Models
{
    public class Order
    {
        //[Required(ErrorMessage = "{0} không được để trống")]
        public string app_id { get; set; }

        //[Required(ErrorMessage = "{0} không được để trống")]
        [MaxLength(50, ErrorMessage = "{0} tối đa 50 kí tự")]
        public string app_user { get; set; }

        //[Required(ErrorMessage = "{0} không được để trống")]
        public string amount { get; set; }

        //[Required(ErrorMessage = "{0} không được để trống")]
        [MaxLength(256, ErrorMessage = "{0} tối đa 256 kí tự")]
        public object item { get; set; }

        //[Required(ErrorMessage = "{0} không được để trống")]
        [MaxLength(20, ErrorMessage = "{0} tối đa 20 kí tự")]
        public string description { get; set; }

        //[Required(ErrorMessage = "{0} không được để trống")]
        [MaxLength(1024, ErrorMessage = "{0} tối đa 1024 kí tự")]
        public object embed_data { get; set; }

        [MaxLength(20, ErrorMessage = "{0} tối đa 20 kí tự")]
        public string bank_code { get; set; }

        [MaxLength(256, ErrorMessage = "{0} tối đa 256 kí tự")]
        public object device_info { get; set; }

        [MaxLength(50, ErrorMessage = "{0} tối đa 50 kí tự")]
        public string sub_app_id { get; set; }

        [MaxLength(256, ErrorMessage = "{0} tối đa 256 kí tự")]
        public string title { get; set; }

        public string currency { get; set; } = "VND";

        [MaxLength(50, ErrorMessage = "{0} tối đa 50 kí tự")]
        public string phone { get; set; }

        [MaxLength(100, ErrorMessage = "{0} tối đa 100 kí tự")]
        public string email { get; set; }

        [MaxLength(1024, ErrorMessage = "{0} tối đa 1024 kí tự")]
        public string address { get; set; }

        public class Response
        {
            public ReturnCode return_code { get; set; }
            public string return_message { get; set; }
            public int sub_return_code { get; set; }
            public string sub_return_message { get; set; }
            public string order_url { get; set; }
            public string zp_trans_token { get; set; }
            public string order_token { get; set; }
        }

        public enum ReturnCode
        {
            [Display(Name = "Thành công")]
            SUCCESS = 1,
            [Display(Name = "Thất bại")]
            FAIL = 2,
            [Display(Name = "Đơn hàng chưa thanh toán hoặc giao dịch đang xử lý")]
            PROCESSING = 3
        }
    }
}
