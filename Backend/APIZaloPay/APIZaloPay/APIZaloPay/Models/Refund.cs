﻿using System.ComponentModel.DataAnnotations;

namespace APIZaloPay.Models
{
    public class Refund
    {
        //[Required(ErrorMessage = "{0} không được để trống")]
        [MaxLength(45, ErrorMessage = "{0} tối đa 45 kí tự")]
        public string m_refund_id { get; set; }

        //[Required(ErrorMessage = "{0} không được để trống")]
        [MaxLength(45, ErrorMessage = "{0} tối đa 45 kí tự")]
        public string zp_trans_id { get; set; }

        //[Required(ErrorMessage = "{0} không được để trống")]
        public string amount { get; set; }

        [MaxLength(45, ErrorMessage = "{0} tối đa 45 kí tự")]
        public string description { get; set; }
    }
}
