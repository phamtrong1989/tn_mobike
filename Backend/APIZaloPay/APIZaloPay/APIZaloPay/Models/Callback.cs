﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIZaloPay.Models
{
    public class Callback
    {
        public string data { get; set; }
        public string mac { get; set; }
        public string type { get; set; }
    }
}
