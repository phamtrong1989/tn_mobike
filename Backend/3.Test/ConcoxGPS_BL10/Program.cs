﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Globalization;
using System.Timers;

namespace ConcoxServer
{
    public class StateObject
    {
        public long connectionNumber = -1;
        public Socket workSocket { get; set; }
        public byte[] buffer { get; set; }
        public int fifoCount { get; set; }
        public string IMEI { get; set; }
    }

    public class StateObjectDB
    {
        public string terminalID { get; set; }
        public DateTime date { get; set; }
    }
    public enum PROTOCOL_NUMBER
    {
        LOGIN_INFORMATION = 0x01,
        HEARTBEAT_PACKET = 0x23,
        ONLINE_COMMAND_RESPONSE_BY_TERMINAL = 0x21,
        GPS_LOCATION_INFORMATION = 0x32,
        LOCATION_INFORMATION_ALARM = 0x33,
        ONLINECOMMAND = 0x80,
        INFORMATION_TRANSMISSION_PACKET = 0x98,
        NONE
    }
    public enum UNPACK_STATUS
    {
        ERROR,
        NOT_ENOUGH_BYTES,
        BAD_CRC,
        GOOD_MESSAGE,
        DEFAULT
    }
    public enum PROCESS_STATE
    {
        ACCEPT,
        READ,
        PROCESS,
        UNPACK
    }
    public enum TERMINAL_INFOR
    {
        GPS_POSITIONING = 1,
        GPS_NOT_POSITIONING = 0,
        CHARGING = 1,
        NOT_CHARGE = 0,
        LOCK_ON = 1,
        LOCK_OFF = 0,

    }
    public enum ALARM : Byte
    {
        NORMAL = 0,
        SHOCK_ALARM = 1,
        POWER_CUT_ALARM = 2,
        LOW_BATTERY = 3,
        SOS = 4
    }
    class Program
    {
        //const string IP = "192.168.2.85";
        //const int PORT = 6060;
        //const Boolean test = true;
        static void Main(string[] args)
        {
            //GPS gps = new GPS(IP, PORT, test);

            //var utcTime = "094937.00";

            //var hour = Convert.ToDouble(utcTime.Substring(0, 2));
            //var minutes = Convert.ToDouble(utcTime.Substring(2, 2));
            //var second = Convert.ToDouble(utcTime.Substring(4, 2));

            //var time = DateTime.UtcNow.AddHours(hour).AddMinutes(minutes).AddSeconds(second).ToLocalTime();

            //Console.WriteLine("Connection Ended");

            var data = new List<string>() {"29A12345", "29A1234", "29ND12345", "29ND1234", "KC7280", "KA9911"};

            //foreach (var plate in data)
            //{
            //    var result = ConvertData(plate);

            //    Console.WriteLine(result);
            //}

            var a = true;
            var b = false;
            var c = false;

            if (!c || (!a || !b))
            {
                Console.WriteLine("Disconnect");
            }
            else
            {
                Console.WriteLine("Connect");
            }

            Console.ReadLine();
        }

        public static string ConvertData(string plate)
        {
            var data = plate;
            if (Char.IsLetter(plate[2]))
            {
                if (char.IsLetter(plate[3]))
                {
                    if ((plate.Length - 4) >= 5)
                    {
                        var x = plate[plate.Length - 3];
                        data = plate.Remove(plate.Length - 3, 1).Insert(plate.Length - 3, $"{x}.").Replace($"{plate[3]}", $"{plate[3]}-");
                    }
                    else
                    {
                        data = plate.Replace($"{plate[3]}", $"{plate[3]}-");
                    }
                }
                else
                {
                    if ((plate.Length - 3) >= 5)
                    {
                        var x = plate[plate.Length - 3];
                        data = plate.Remove(plate.Length - 3, 1)
                            .Insert(plate.Length - 3, $"{x}.").Replace($"{plate[2]}", $"{plate[2]}-");
                    }
                    else
                    {
                        data = plate.Replace($"{plate[2]}", $"{plate[2]}-");
                    }
                }
            }
            else if(Char.IsLetter(plate[0]) && char.IsLetter(plate[1]))
            {
                data = plate.Replace($"{plate[1]}", $"{plate[1]}-");
            }

            return data;
        }
    }
    public class GPS
    {
        const int BUFFER_SIZE = 1024;
        static long connectionNumber = 0;
        //mapping of connection number to StateObject
        static Dictionary<long, KeyValuePair<List<byte>, StateObject>> connectionDict = new Dictionary<long, KeyValuePair<List<byte>, StateObject>>();
        //fifo contains list of connections number wait with receive data
        public static List<long> fifo = new List<long>();
        public static AutoResetEvent allDone = new AutoResetEvent(false);
        public static AutoResetEvent acceptDone = new AutoResetEvent(false);
        public GPS(string IP, int port, Boolean test)
        {
            try
            {
                StartListening(IP, port, test);
                ProcessMessages();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : '{0}'", ex.Message);
                return;
            }
        }
        public void StartListening(string IP, int port, Boolean test)
        {
            try
            {
                // Establish the local endpoint for the socket.
                // The DNS name of the computer
                // running the listener is "host.contoso.com".
                IPHostEntry ipHostInfo = Dns.GetHostEntry(IP);  //Dns.Resolve(Dns.GetHostName());
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                //IPAddress local = IPAddress.Parse(IP);

                IPEndPoint localEndPoint = null;
                if (test)
                {
                    localEndPoint = new IPEndPoint(IPAddress.Any, port);
                }
                else
                {
                    localEndPoint = new IPEndPoint(ipAddress, port);
                }

                // Create a TCP/IP socket.
                Socket listener = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
                // Bind the socket to the local endpoint and listen for incoming connections.
                allDone.Reset();
                acceptDone.Reset();
                listener.Bind(localEndPoint);
                listener.Listen(100);

                //login code, wait for 1st message
                Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} - Wait 5 seconds for login message");

                listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public DateTime getDateTimeLocal(byte[] DateTimeUtc)
        {
            DateTime dateTimeUTC = new DateTime(DateTimeUtc[0] + 2000, DateTimeUtc[1], DateTimeUtc[2], DateTimeUtc[3], DateTimeUtc[4], DateTimeUtc[5]);
            return dateTimeUTC.ToLocalTime();
        }
        public double getLatitudeLongitude(byte[] LatitudeLongitude)
        {
            return (double)(Convert.ToInt32(BytesToString(LatitudeLongitude), 16)) / 1800000;
        }
        public double getPercentVoltage(byte[] VoltageLevel)
        {
            double volNumber = (Convert.ToInt64(BytesToString(VoltageLevel), 16));
            return Math.Round((((volNumber / 100 - 3.50d) / 0.66) * 100),2);
        }
        public char[] getTerminalInformationContent(byte[] TerminalInformationContent)
        {
            Console.WriteLine($"TerminalInformationContent                   : {TerminalInformationContent[0]}");
            Console.WriteLine($"TerminalInformationContent bit               : {(Convert.ToString(TerminalInformationContent[0], 2))}");
            Console.WriteLine($"TerminalInformationContent bit arr           : {(Convert.ToString(TerminalInformationContent[0], 2)).ToArray()}");
            char[] TerminalInfo = (Convert.ToString(TerminalInformationContent[0], 2)).ToArray();
            Console.WriteLine($"TerminalInformationContent bit arr to str    : { string.Join(",", TerminalInfo)}");

            Console.WriteLine($"GPS info                                     : {TerminalInfo[1]}");
            Console.WriteLine($"Battery status                               : {TerminalInfo[5]}");
            Console.WriteLine($"Lock status                                  : {TerminalInfo[7]}");
            return TerminalInfo;
        }
        public void ProcessMessages()
        {
            UInt16 sendCRC = 0;
            KeyValuePair<List<byte>, StateObject> byteState;
            KeyValuePair<UNPACK_STATUS, byte[]> status;
            byte[] receiveMessage = null;
            StateObject state = null;
            byte[] packetStartBit = null;
            byte[] packetStopBit = { 0x0D, 0x0A };//fix
            byte[] packetReceiveLength = null;
            byte[] packetSendLength = null;
            byte[] packetProtocol = null;
            byte[] packetSerialNumber = null;
            byte[] packetErrorCheck = null;

            //Login packet
            byte[] login_imei = null;
            byte[] login_modelCode = null;
            byte[] login_timeZone = null;

            //Heartbeat packet
            byte[] heartbeat_terminalInformationContent = null;
            byte[] heartbeat_voltageLevel = null;
            byte[] heartbeat_gSMSignalStrength = null;
            byte[] heartbeat_Language = null;

            //Location packet
            byte[] location_DateTimeUTC = null;
            byte[] location_GPSInfoLength = null;
            byte[] location_QuantityGPSInfo = null;
            byte[] location_Latitude = null;
            byte[] location_Longitude = null;
            byte[] location_CourseStatus = null;
            byte[] location_MainBaseStation_Length = null;
            byte[] location_MCC = null;
            byte[] location_MNC = null;
            byte[] location_LAC = null;
            byte[] location_CI = null;
            byte[] location_RSSI = null;
            byte[] location_SubBaseStation_Length = null;
            byte[] location_NLAC1 = null;
            byte[] location_NLAC2 = null;
            byte[] location_NCI1 = null;
            byte[] location_NCI2 = null;
            byte[] location_NRSSI1 = null;
            byte[] location_NRSSI2 = null;
            byte[] location_WIFI_MessageLength = null;
            byte[] location_WIFI_MAC_1 = null;
            byte[] location_WIFI_MAC_2 = null;
            byte[] location_WIFI_Strength_1 = null;
            byte[] location_WIFI_Strength_2 = null;
            byte[] location_Status = null;
            byte[] location_ReservedExtensionBit_Length = null;
            byte[] location_ReservedExtensionBit = null;
            byte[] location_Speed = null;



            bool isLogin = false;
            bool isSendLock = false;
            PROTOCOL_NUMBER protocolNumber = PROTOCOL_NUMBER.NONE;
            try
            {
                Boolean firstMessage = true;
                acceptDone.Set();
                //loop forever
                while (true)
                {
                    allDone.WaitOne();

                    //read fifo until empty
                    while (true)
                    {
                        //read one connection until buffer doesn't contain any more packets
                        byteState = ReadWrite(PROCESS_STATE.PROCESS, null, null, -1);

                        if (byteState.Value.fifoCount == -1) break;

                        state = byteState.Value;
                        while (true)
                        {
                            status = Unpack(byteState);
                            if (status.Key == UNPACK_STATUS.NOT_ENOUGH_BYTES)
                                break;
                            if (status.Key == UNPACK_STATUS.ERROR)
                            {
                                Console.WriteLine("Error : Bad Receive Message, Data");
                                break;
                            }
                            receiveMessage = status.Value;

                            if (status.Key != UNPACK_STATUS.GOOD_MESSAGE)
                            {
                                break;
                            }
                            else
                            {
                                try
                                {
                                    if (firstMessage)
                                   {
                                        if (receiveMessage[3] != 0x01)
                                        {
                                            Console.WriteLine("Error : Expected Login Message : '{0}'", BytesToString(receiveMessage));
                                            break;
                                        }
                                        firstMessage = false;
                                    }
                                    
                                    if (receiveMessage.Length < 1)
                                    {
                                        protocolNumber = PROTOCOL_NUMBER.NONE;
                                    }
                                    else if (receiveMessage[1] == 0x79)
                                    {
                                        protocolNumber = (PROTOCOL_NUMBER)receiveMessage[4];
                                    }
                                    else
                                    {
                                        protocolNumber = (PROTOCOL_NUMBER)receiveMessage[3];
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                    continue;
                                }
                                //Test mở khóa
                                if (isLogin && !isSendLock)
                                {
                                    //byte[] sendUnlock = { 0x78, 0x78, 0x11, 0x80, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x55, 0x4E, 0x4C, 0x4F, 0x43, 0x4B, 0x23, 0x00, 0x01, 0x53, 0x54, 0x0D, 0x0A };
                                    //Send(state.workSocket, sendUnlock);
                                    //isSendLock = true;
                                    Console.WriteLine("Send unlock successfull");
                                }
                                if (status.Key == UNPACK_STATUS.GOOD_MESSAGE)
                                {
                                    Console.ForegroundColor = ConsoleColor.Green;
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                                }

                                Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Status: {(status.Key == UNPACK_STATUS.GOOD_MESSAGE ? "Good" : "Bad")}; Message Type: {protocolNumber.ToString()}");
                                Console.ResetColor();
                                List<byte> DateTimeUTC = new List<byte>();
                                DateTimeUTC.AddRange(TransformBytes(19, 1));
                                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Month, 1));
                                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Day, 1));
                                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Hour, 1));
                                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Minute, 1));
                                DateTimeUTC.AddRange(TransformBytes(DateTime.Now.Second, 1));

                                switch (protocolNumber)
                                {
                                    #region LOGIN_INFORMATION
                                    case PROTOCOL_NUMBER.LOGIN_INFORMATION:
                                        packetStartBit = receiveMessage.Take(2).ToArray();
                                        packetReceiveLength = receiveMessage.Skip(2).Take(1).ToArray();
                                        packetSendLength = new byte[] { 0x0C };
                                        packetProtocol = new byte[] { 0x01 };
                                        login_imei = receiveMessage.Skip(4).Take(8).ToArray();
                                        login_modelCode = receiveMessage.Skip(12).Take(2).ToArray();
                                        login_timeZone = receiveMessage.Skip(14).Take(2).ToArray();
                                        packetSerialNumber = receiveMessage.Skip(16).Take(2).ToArray();
                                        packetErrorCheck = receiveMessage.Skip(18).Take(2).ToArray();

                                        byte[] loginMessageResponse = packetStartBit
                                            .Concat(packetSendLength)
                                            .Concat(packetProtocol)
                                            .Concat(DateTimeUTC.ToArray())
                                            .Concat(new byte[] { 0x00 })
                                            .Concat(packetSerialNumber)
                                            .Concat(packetErrorCheck)
                                            .Concat(packetStopBit)
                                            .ToArray();

                                        byte[] checkCRC = loginMessageResponse.Skip(2).Take(11).ToArray();
                                        sendCRC = Crc_bytes(checkCRC); //https://crccalc.com/ CRC-16/X-25
                                        loginMessageResponse[loginMessageResponse.Length - 4] = (byte)((sendCRC >> 8) & 0xFF);
                                        loginMessageResponse[loginMessageResponse.Length - 3] = (byte)((sendCRC) & 0xFF);
                                        string errorCheck = BytesToString(loginMessageResponse.Skip(13).Take(2).ToArray());

                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Received Login Message Packet");
                                        Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                                        Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");

                                        //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                                        //Console.WriteLine($"- Packet Length                (1 byte): {BytesToString(packetReceiveLength)}");
                                        //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                                        Console.WriteLine($"- IMEI                         (8 byte): {BytesToString(login_imei)}");
                                        //Console.WriteLine($"- Model Indentification Code   (2 byte): {BytesToString(login_modelCode)}");
                                        //Console.WriteLine($"- Time Zone Language           (2 byte): {BytesToString(login_timeZone)}");
                                        Console.WriteLine($"- Information Serial Number    (2 byte): {BytesToString(packetSerialNumber)}");
                                        //Console.WriteLine($"- Error Check                  (2 byte): {BytesToString(packetErrorCheck)}");
                                        //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Send Login Message Response");
                                        Console.WriteLine($"Hexadecimal Value: : {BytesToString(loginMessageResponse)}");
                                        Console.WriteLine($"Error Check: {BytesToString(checkCRC)} ~ {sendCRC} ~ {errorCheck}");

                                        //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                                        //Console.WriteLine($"- Packet Length                (1 byte): {BytesToString(packetSendLength)}");
                                        //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                                        //Console.WriteLine($"- DateTime (UTC)               (6 byte): {BytesToString(DateTimeUTC.ToArray())}");
                                        //Console.WriteLine($"- Reserved Extension BitLength (1 byte): {BytesToString(new byte[] { 0x00 })}");
                                        //Console.WriteLine($"- Information Serial Number    (2 byte): {BytesToString(packetSerialNumber)}");
                                        //Console.WriteLine($"- Error Check                  (2 byte): {errorCheck}");
                                        //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                                        Console.WriteLine("===============================================================");
                                        Send(state.workSocket, loginMessageResponse);
                                        isLogin = true;
                                        break;
                                    #endregion
                                    #region HEARTBEAT_PACKET
                                    case PROTOCOL_NUMBER.HEARTBEAT_PACKET:
                                        packetStartBit = receiveMessage.Take(2).ToArray();
                                        packetReceiveLength = receiveMessage.Skip(2).Take(1).ToArray();
                                        packetSendLength = new byte[] { 0x05 };
                                        packetProtocol = new byte[] { 0x23 };
                                        heartbeat_terminalInformationContent = receiveMessage.Skip(4).Take(1).ToArray();
                                        heartbeat_voltageLevel = receiveMessage.Skip(5).Take(2).ToArray();
                                        heartbeat_gSMSignalStrength = receiveMessage.Skip(7).Take(1).ToArray();
                                        heartbeat_Language = receiveMessage.Skip(9).Take(2).ToArray();
                                        packetSerialNumber = receiveMessage.Skip(10).Take(2).ToArray();
                                        packetErrorCheck = receiveMessage.Skip(12).Take(2).ToArray();

                                        byte[] hearbeatMessageResponse = packetStartBit
                                            .Concat(packetSendLength)
                                            .Concat(packetProtocol)
                                            .Concat(packetSerialNumber)
                                            .Concat(packetErrorCheck)
                                            .Concat(packetStopBit)
                                            .ToArray();

                                        byte[] checkCRC_heartbeat = hearbeatMessageResponse.Skip(2).Take(4).ToArray();
                                        sendCRC = Crc_bytes(checkCRC_heartbeat); //https://crccalc.com/ CRC-16/X-25

                                        hearbeatMessageResponse[hearbeatMessageResponse.Length - 4] = (byte)((sendCRC >> 8) & 0xFF);
                                        hearbeatMessageResponse[hearbeatMessageResponse.Length - 3] = (byte)((sendCRC) & 0xFF);
                                        string errorCheck_heartbeat = BytesToString(hearbeatMessageResponse.Skip(6).Take(2).ToArray());

                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Received Heartbeat Packet Sent By Terminal (16 bytes)");
                                        Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                                        Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");

                                        //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                                        //Console.WriteLine($"- Packet Length                (1 byte): {BytesToString(packetReceiveLength)}");
                                        //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                                        Console.WriteLine($"- Terminal Information Content   (1 byte): {BytesToString(heartbeat_terminalInformationContent)} = {getTerminalInformationContent(heartbeat_terminalInformationContent)}");
                                        //Console.WriteLine($"- VoltageLevel                   (2 byte): {BytesToString(heartbeat_voltageLevel)} ");
                                        Console.WriteLine($"- VoltageLevel                   (2 byte): {BytesToString(heartbeat_voltageLevel)} = {getPercentVoltage(heartbeat_voltageLevel)}%");
                                        //Console.WriteLine($"- GSM Signal Strength          (1 byte): {BytesToString(login_timeZone)}");
                                        //Console.WriteLine($"- Language                     (2 byte): {BytesToString(login_timeZone)}");
                                        Console.WriteLine($"- Serial Number                (2 byte): {BytesToString(packetSerialNumber)}");
                                        //Console.WriteLine($"- Error Check                  (2 byte): {BytesToString(packetErrorCheck)}");
                                        //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Send  Server Responds The Heartbeat Packet (10 bytes)");
                                        Console.WriteLine($"Hexadecimal Value: : {BytesToString(hearbeatMessageResponse)}");
                                        Console.WriteLine($"Error Check: {BytesToString(checkCRC_heartbeat)} ~ {sendCRC} ~ {errorCheck_heartbeat}");

                                        //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                                        //Console.WriteLine($"- Packet Length                (1 byte): {BytesToString(packetSendLength)}");
                                        //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                                        //Console.WriteLine($"- SerialNumber                 (1 byte): {BytesToString(packetSerialNumber)}");
                                        //Console.WriteLine($"- Error Check                  (2 byte): {errorCheck_heartbeat}");
                                        //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                                        Console.WriteLine("===============================================================");
                                        Send(state.workSocket, hearbeatMessageResponse);
                                        break;
                                    #endregion
                                    #region GPS_LOCATION_INFORMATION
                                    case PROTOCOL_NUMBER.GPS_LOCATION_INFORMATION:
                                        packetStartBit = receiveMessage.Take(2).ToArray();
                                        packetReceiveLength = receiveMessage.Skip(2).Take(2).ToArray();
                                        packetSendLength = new byte[] { 0x0C }; //quest ?
                                        packetProtocol = receiveMessage.Skip(4).Take(1).ToArray();
                                        location_DateTimeUTC = receiveMessage.Skip(5).Take(6).ToArray();
                                        location_GPSInfoLength = receiveMessage.Skip(11).Take(1).ToArray();
                                        location_QuantityGPSInfo = receiveMessage.Skip(12).Take(1).ToArray();
                                        location_Latitude = receiveMessage.Skip(13).Take(4).ToArray();
                                        location_Longitude = receiveMessage.Skip(17).Take(4).ToArray();
                                        location_Speed = receiveMessage.Skip(21).Take(1).ToArray();
                                        location_CourseStatus = receiveMessage.Skip(22).Take(2).ToArray();
                                        location_MainBaseStation_Length = receiveMessage.Skip(24).Take(1).ToArray();
                                        location_MCC = receiveMessage.Skip(25).Take(2).ToArray();
                                        location_MNC = receiveMessage.Skip(27).Take(1).ToArray();
                                        location_LAC = receiveMessage.Skip(28).Take(2).ToArray();
                                        location_CI = receiveMessage.Skip(30).Take(3).ToArray();
                                        location_RSSI = receiveMessage.Skip(33).Take(1).ToArray();
                                        location_SubBaseStation_Length = receiveMessage.Skip(34).Take(1).ToArray();
                                        location_NLAC1 = receiveMessage.Skip(35).Take(2).ToArray();
                                        location_NCI1 = receiveMessage.Skip(37).Take(3).ToArray();
                                        location_NRSSI1 = receiveMessage.Skip(40).Take(1).ToArray();
                                        location_NLAC2 = receiveMessage.Skip(41).Take(2).ToArray();
                                        location_NCI2 = receiveMessage.Skip(43).Take(3).ToArray();
                                        location_NRSSI2 = receiveMessage.Skip(46).Take(1).ToArray();
                                        location_WIFI_MessageLength = receiveMessage.Skip(47).Take(1).ToArray();
                                        location_WIFI_MAC_1 = receiveMessage.Skip(48).Take(6).ToArray();
                                        location_WIFI_Strength_1 = receiveMessage.Skip(54).Take(1).ToArray();
                                        location_WIFI_MAC_2 = receiveMessage.Skip(55).Take(6).ToArray();
                                        location_WIFI_Strength_2 = receiveMessage.Skip(61).Take(1).ToArray();
                                        location_Status = receiveMessage.Skip(62).Take(1).ToArray();
                                        location_ReservedExtensionBit_Length = receiveMessage.Skip(63).Take(1).ToArray();
                                        packetSerialNumber = receiveMessage.Skip(64).Take(2).ToArray();
                                        packetErrorCheck = receiveMessage.Skip(66).Take(2).ToArray();

                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}  Received GPS_LOCATION_INFORMATION");
                                        Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                                        Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");


                                        //Console.WriteLine($"- Start Bit                    (2 byte): {BytesToString(packetStartBit)}");
                                        //Console.WriteLine($"- Packet Length                (2 byte): {BytesToString(packetReceiveLength)}");
                                        //Console.WriteLine($"- Protocol Number              (1 byte): {BytesToString(packetProtocol)}");
                                        //Console.WriteLine($"- DateTime UTC                 (6 byte): {BytesToString(location_DateTimeUTC)} = DateTime Local: {getDateTimeLocal(location_DateTimeUTC.ToArray())}");
                                        //Console.WriteLine($"- GPS Info Length              (1 byte): {BytesToString(location_GPSInfoLength)}");
                                        //Console.WriteLine($"- Quantity GPS Info            (1 byte): {BytesToString(location_QuantityGPSInfo)}");
                                        //Console.WriteLine($"- Latitude                     (4 byte): {BytesToString(location_Latitude)} = {getLatitudeLongitude(location_Latitude)} ");
                                        //Console.WriteLine($"- Longitude                    (4 byte): {BytesToString(location_Longitude)} = {getLatitudeLongitude(location_Longitude)} ");
                                        //Console.WriteLine($"- Speed                        (1 byte): {BytesToString(location_Speed)}");
                                        //Console.WriteLine($"- CourseStatus                 (2 byte): {BytesToString(location_CourseStatus)}");
                                        //Console.WriteLine($"- Main Base Station Length     (2 byte): {BytesToString(location_MainBaseStation_Length)}");
                                        //Console.WriteLine($"- MCC                          (2 byte): {BytesToString(location_MCC)}");
                                        //Console.WriteLine($"- MNC                          (1 byte): {BytesToString(location_MNC)}");
                                        //Console.WriteLine($"- LAC                          (2 byte): {BytesToString(location_LAC)}");
                                        //Console.WriteLine($"- CI                           (3 byte): {BytesToString(location_CI)}");
                                        //Console.WriteLine($"- RSSI                         (1 byte): {BytesToString(location_RSSI)}");
                                        //Console.WriteLine($"- Sub BaseStationbLength       (1 byte): {BytesToString(location_SubBaseStation_Length)}");
                                        //Console.WriteLine($"- NLAC1                        (2 byte): {BytesToString(location_NLAC1)}");
                                        //Console.WriteLine($"- NCI1                         (3 byte): {BytesToString(location_NCI1)}");
                                        //Console.WriteLine($"- NRSSI1                       (1 byte): {BytesToString(location_NRSSI1)}");
                                        //Console.WriteLine($"- NLAC2                        (2 byte): {BytesToString(location_NLAC2)}");
                                        //Console.WriteLine($"- NCI2                         (3 byte): {BytesToString(location_NCI2)}");
                                        //Console.WriteLine($"- NRSSI2                       (1 byte): {BytesToString(location_NRSSI2)}");
                                        //Console.WriteLine($"- WIFI Message Length          (1 byte): {BytesToString(location_WIFI_MessageLength)}");
                                        //Console.WriteLine($"- WIFI MAC 1                   (6 byte): {BytesToString(location_WIFI_MAC_1)}");
                                        //Console.WriteLine($"- WIFI Strength 1              (1 byte): {BytesToString(location_WIFI_Strength_1)}");
                                        //Console.WriteLine($"- WIFI MAC 2                   (6 byte): {BytesToString(location_WIFI_MAC_2)}");
                                        //Console.WriteLine($"- WIFI Strength 2              (1 byte): {BytesToString(location_WIFI_Strength_2)}");
                                        //Console.WriteLine($"- Status                       (1 byte): {BytesToString(location_Status)}");
                                        //Console.WriteLine($"- Reserved Extension Bit Length(1 byte): {BytesToString(location_ReservedExtensionBit_Length)}");
                                        //Console.WriteLine($"- SerialNumber                 (2 byte): {BytesToString(packetSerialNumber)}");
                                        //Console.WriteLine($"- ErrorCheck                   (2 byte): {BytesToString(packetErrorCheck)}");
                                        //Console.WriteLine($"- Stop Bit                     (2 byte): {BytesToString(packetStopBit)}");

                                        break;
                                    #endregion

                                    case PROTOCOL_NUMBER.LOCATION_INFORMATION_ALARM:
                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Received LOCATION_INFORMATION_ALARM");
                                        Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                                        Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");
                                        break;
                                    case PROTOCOL_NUMBER.INFORMATION_TRANSMISSION_PACKET:
                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Received INFORMATION_TRANSMISSION_PACKET");
                                        Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                                        Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");
                                        break;
                                    case PROTOCOL_NUMBER.ONLINECOMMAND:
                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Received ONLINECOMMAND");
                                        Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                                        Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");
                                        break;
                                    case PROTOCOL_NUMBER.ONLINE_COMMAND_RESPONSE_BY_TERMINAL:
                                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Received ONLINE_COMMAND_RESPONSE_BY_TERMINAL");
                                        Console.WriteLine($"Decimal Value: {string.Join(",", receiveMessage.ToArray())}");
                                        Console.WriteLine($"Hexadecimal Value: {BytesToString(receiveMessage.ToArray())}");
                                        break;
                                }
                            }
                        }
                    }
                    allDone.Reset();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        static byte[] TransformBytes(int num, int byteLength)
        {
            byte[] res = new byte[byteLength];

            byte[] temp = BitConverter.GetBytes(num);

            Array.Copy(temp, res, byteLength);

            return res;
        }
        static string BytesToString(byte[] bytes)
        {
            return string.Join("", bytes.Select(x => x.ToString("X2")));
        }
        static KeyValuePair<UNPACK_STATUS, byte[]> Unpack(KeyValuePair<List<byte>, StateObject> bitState)
        {
            List<byte> working_buffer = null;
            try
            {
                working_buffer = bitState.Key;
                Console.WriteLine($"Data: {BytesToString(working_buffer.ToArray())} ; Length: {working_buffer.Count()}");
                //return null indicates an error
                if (working_buffer.Count() < 3)
                {
                    Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} Received new packet. Status: NOT ENOUGH BYTES. Total: {working_buffer.Count()} byte ");
                    Console.WriteLine("===============================================================");
                    return new KeyValuePair<UNPACK_STATUS, byte[]>(UNPACK_STATUS.NOT_ENOUGH_BYTES, null);
                }
                KeyValuePair<List<byte>, StateObject> byteState = ReadWrite(PROCESS_STATE.UNPACK, null, null, bitState.Value.connectionNumber);
                List<byte> packet = byteState.Key;

                return new KeyValuePair<UNPACK_STATUS, byte[]>(UNPACK_STATUS.GOOD_MESSAGE, packet.ToArray());
            }
            catch (Exception ex)
            {
                return new KeyValuePair<UNPACK_STATUS, byte[]>(UNPACK_STATUS.NOT_ENOUGH_BYTES, working_buffer.ToArray());
            }
        }
        static public UInt16 Crc_bytes(byte[] data)
        {
            ushort crc = 0xFFFF;

            for (int i = 0; i < data.Length; i++)
            {
                crc ^= (ushort)(Reflect(data[i], 8) << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x8000) > 0)
                        crc = (ushort)((crc << 1) ^ 0x1021);
                    else
                        crc <<= 1;
                }
            }
            crc = Reflect(crc, 16);
            crc = (ushort)~crc;
            return crc;
        }
        static public ushort Reflect(ushort data, int size)
        {
            ushort output = 0;
            for (int i = 0; i < size; i++)
            {
                int lsb = data & 0x01;
                output = (ushort)((output << 1) | lsb);
                data >>= 1;
            }
            return output;
        }
        static KeyValuePair<List<byte>, StateObject> ReadWrite(PROCESS_STATE ps, Socket handler, IAsyncResult ar, long unpackConnectionNumber)
        {
            KeyValuePair<List<byte>, StateObject> byteState = new KeyValuePair<List<byte>, StateObject>(); ;
            StateObject stateObject = null;
            int bytesRead = -1;
            int workingBufferLen = 0;
            List<byte> working_buffer = null;
            byte[] buffer = null;

            Object thisLock1 = new Object();

            lock (thisLock1)
            {
                switch (ps)
                {
                    case PROCESS_STATE.ACCEPT:

                        acceptDone.WaitOne();
                        acceptDone.Reset();
                        stateObject = new StateObject();
                        stateObject.buffer = new byte[BUFFER_SIZE];
                        connectionDict.Add(connectionNumber, new KeyValuePair<List<byte>, StateObject>(new List<byte>(), stateObject));
                        stateObject.connectionNumber = connectionNumber++;

                        stateObject.workSocket = handler;

                        byteState = new KeyValuePair<List<byte>, StateObject>(null, stateObject);
                        acceptDone.Set();
                        break;

                    case PROCESS_STATE.READ:
                        //catch when client disconnects

                        //wait if accept is being called
                        //acceptDone.WaitOne();
                        try
                        {
                            stateObject = ar.AsyncState as StateObject;
                            // Read data from the client socket. 
                            bytesRead = stateObject.workSocket.EndReceive(ar);

                            if (bytesRead > 0)
                            {
                                byteState = connectionDict[stateObject.connectionNumber];

                                buffer = new byte[bytesRead];
                                Array.Copy(byteState.Value.buffer, buffer, bytesRead);
                                string data = BytesToString(buffer);
                                byteState.Key.AddRange(buffer);
                            }
                            //only put one instance of connection number into fifo
                            if (!fifo.Contains(byteState.Value.connectionNumber))
                            {

                                fifo.Add(byteState.Value.connectionNumber);
                            }
                        }
                        catch (Exception ex)
                        {
                            //will get here if client disconnects
                            fifo.RemoveAll(x => x == byteState.Value.connectionNumber);
                            connectionDict.Remove(byteState.Value.connectionNumber);
                            byteState = new KeyValuePair<List<byte>, StateObject>(new List<byte>(), null);
                        }
                        break;
                    case PROCESS_STATE.PROCESS:
                        if (fifo.Count > 0)
                        {
                            //get message from working buffer
                            //unpack will later delete message
                            //remove connection number from fifo
                            // the list in the key in known as the working buffer
                            byteState = new KeyValuePair<List<byte>, StateObject>(connectionDict[fifo[0]].Key, connectionDict[fifo[0]].Value);
                            fifo.RemoveAt(0);
                            //put a valid value in fifoCount so -1 below can be detected.
                            byteState.Value.fifoCount = fifo.Count;
                        }
                        else
                        {
                            //getting here is normal when there is no more work to be performed
                            //set fifocount to zero so rest of code know fifo was empty so code waits for next receive message
                            byteState = new KeyValuePair<List<byte>, StateObject>(null, new StateObject() { fifoCount = -1 });
                        }
                        break;
                    case PROCESS_STATE.UNPACK:
                        try
                        {
                            working_buffer = connectionDict[unpackConnectionNumber].Key;

                            if (
                                (working_buffer[0] == 0x78) &&
                                (working_buffer[1] == 0x78))
                            {
                                workingBufferLen = working_buffer[2];

                            }
                            else if ((working_buffer[0] == 0x79) &&
                              (working_buffer[1] == 0x79))
                            {
                                workingBufferLen = working_buffer[2] + working_buffer[3];

                            }
                            Console.WriteLine($"working_buffer :    {working_buffer[0]}");
                            if (
                                (working_buffer[0] != 0x78) &&
                                (working_buffer[0] != 0x79) &&
                                (working_buffer[workingBufferLen + 3] != 0x0D) &&
                                (working_buffer[workingBufferLen + 4] != 0x0A))
                            {
                                working_buffer.Clear();
                                return new KeyValuePair<List<byte>, StateObject>(new List<byte>(), null);
                            }
                            List<byte> packet = working_buffer.GetRange(0, workingBufferLen + 5);
                            working_buffer.RemoveRange(0, workingBufferLen + 5);
                            byteState = new KeyValuePair<List<byte>, StateObject>(packet, null);
                        }
                        catch (Exception ex)
                        {
                            int testPoint = 0;
                        }
                        break;
                }// end switch
            }
            return byteState;
        }
        static void Send(Socket socket, byte[] data)
        {
            // Convert the string data to byte data using ASCII encoding.
            // byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            socket.BeginSend(data, 0, data.Length, 0,
                new AsyncCallback(SendCallback), socket);
        }
        static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = ar.AsyncState as Socket;
                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                // Console.WriteLine("Sent {0} bytes to client.", bytesSent);
            }
            catch (Exception e)
            {
                // Console.WriteLine(e.ToString());
                int myerror = -1;
            }
        }
        public static void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                // Get the socket that handles the client request.
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.

                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                // Create the state object.
                StateObject state = ReadWrite(PROCESS_STATE.ACCEPT, handler, ar, -1).Value;

                handler.BeginReceive(state.buffer, 0, BUFFER_SIZE, 0,
                    new AsyncCallback(ReadCallback), state);
            }
            catch (Exception ex)
            {
                int myerror = -1;
            }
        }
        public static void ReadCallback(IAsyncResult ar)
        {
            try
            {
                StateObject state = ar.AsyncState as StateObject;
                Socket handler = state.workSocket;

                // Read data from the client socket. 
                KeyValuePair<List<byte>, StateObject> byteState = ReadWrite(PROCESS_STATE.READ, handler, ar, -1);

                if (byteState.Value != null)
                {
                    allDone.Set();
                    handler.BeginReceive(state.buffer, 0, BUFFER_SIZE, 0,
                        new AsyncCallback(ReadCallback), state);
                }
                else
                {
                    int testPoint = 0;
                }
            }
            catch (Exception ex)
            {
                int myerror = -1;
            }

            // Signal the main thread to continue.  
            allDone.Set();
        }
    }
}