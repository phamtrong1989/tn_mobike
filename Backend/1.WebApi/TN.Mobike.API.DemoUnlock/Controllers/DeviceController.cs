﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace TN.Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly IHubContext<DeviceHub> _hubContext;

        public DeviceController(IHubContext<DeviceHub> hubContext)
        {
            _hubContext = hubContext;
        }

        [HttpPost("{id}")]
        public async Task<object> Post(string id)
        {
            var dl = Devices.FirstOrDefault(x => x.IMEI == id);
            if(dl!=null)
            {
                await _hubContext.Clients.All.SendAsync("DeviceReceiveMessage", Newtonsoft.Json.JsonConvert.SerializeObject(dl));
            }
            return new { Result = dl };
        }
        public List<Device> Devices
        {
            get
            {
                return new List<Device>()
                {
                    new Device(){ No = "7551032821", IMEI = "0355951092277172", MAC = "C4:A8:28:07:FE:53" },
                    new Device(){ No = "7551032515", IMEI = "0355951092276208", MAC = "C4:A8:28:07:B4:83" },
                };
            }
        }
    }
    public class Device
    {
        public string No { get; set; }
        public string IMEI { get; set; }
        public string MAC { get; set; }
    }
}