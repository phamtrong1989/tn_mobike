﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class TicketTransaction : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Pending = 0,
            Running = 1,
            Done = 2,
            Cancel = -1,
        }

        [ForeignKey("Account")]
        public int AccountId { get; set; }
        [ForeignKey("Ticket")]
        public int TicketId { get; set; }
        [ForeignKey("Station")]
        public int StartStationId { get; set; }
        [ForeignKey("Station")]
        public int EndStationId { get; set; }
        [ForeignKey("Dock")]
        public int StartDockId { get; set; }
        [ForeignKey("Dock")]
        public int EndDockId { get; set; }
        [ForeignKey("Bike")]
        public int BikeId { get; set; }
        public double Price { get; set; }
        public EStatus Status { get; set; }
        public string Note { get; set; }
        public bool InAlarm { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int BookingId { get; set; }
        public TicketTransaction() { }

        public TicketTransaction(int accountid, int ticketId, int startStationId, int startDockId, int bikeId, EStatus status)
        {
            this.AccountId = accountid;
            this.TicketId = ticketId;
            this.StartStationId = startStationId;
            this.EndStationId = 0;
            this.StartDockId = startDockId;
            this.EndDockId = 0;
            this.BikeId = bikeId;
            this.Status = status;
            this.StartDate = DateTime.Now;
            this.EndDate = DateTime.Now;
        }
    }
}
