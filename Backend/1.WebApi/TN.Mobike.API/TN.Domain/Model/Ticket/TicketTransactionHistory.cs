﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class TicketTransactionHistory : Entity, IAggregateRoot
    {
        public enum EPaymentStatus
        {
            Processing = 0,
            Done = 1,
            Failed = 2,
        }

        public int TicketId { get; set; }
        public int TicketTransactionId { get; set; }
        public int StartStationId { get; set; }
        public int EndStationId { get; set; }
        public int StartDockId { get; set; }
        public int EndDockId { get; set; }
        public int BikeId { get; set; }
        public string Note { get; set; }
        public string Comment { get; set; }
        public int Rating { get; set; }
        public double Price { get; set; }
        public int AccountId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public TicketTransaction.EStatus Status { get; set; }
        public EPaymentStatus PaymentStatus { get; set; }

        public TicketTransactionHistory() { }

        public TicketTransactionHistory(TicketTransaction transaction, double price)
        {
            BikeId = transaction.BikeId;
            StartStationId = transaction.StartStationId;
            EndStationId = transaction.EndStationId;
            StartDockId = transaction.StartDockId;
            EndDockId = transaction.EndDockId;
            Rating = 0;
            StartDate = transaction.StartDate;
            EndDate = DateTime.Now;
            TicketId = transaction.TicketId;
            TicketTransactionId = transaction.Id;
            Price = price;
            AccountId = transaction.AccountId;
            Status = TicketTransaction.EStatus.Done;
            PaymentStatus = EPaymentStatus.Processing;
        }

        public void UpdatePaymentStatus(EPaymentStatus status)
        {
            PaymentStatus = status;
        }
    }

}
