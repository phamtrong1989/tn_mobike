﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Ticket : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Inactive = 0,
            Active = 1,
            Expired = 2,
        }

        public enum EType
        {
            PayAsYouGo = 1,
            Hourly = 2,
            Dayly = 3,
            Monthly = 4,
            Quarterly = 4,
        }
        [ForeignKey("Account")]
        public int AccountId { get; set; }
        public EType Type { get; set; }
        public EStatus Status { get; set; }
        public DateTime ExpiredDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int BookingId { get; set; }

        public Ticket()
        {
            Type = EType.PayAsYouGo;
            Status = EStatus.Active;
            CreatedDate = DateTime.Now;
            ExpiredDate = DateTime.Now;
        }

        public Ticket(int accountid)
        {
            AccountId = accountid;
            Type = EType.PayAsYouGo;
            Status = EStatus.Active;
            CreatedDate = DateTime.Now;
            ExpiredDate = DateTime.Now;
        }

        public Ticket(int accountid, EType type)
        {
            AccountId = accountid;
            Type = type;
            Status = EStatus.Active;
            CreatedDate = DateTime.Now;
            ExpiredDate = DateTime.Now;
        }
    }
}
