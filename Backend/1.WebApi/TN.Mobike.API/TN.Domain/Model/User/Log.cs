﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public enum LogStatus
    {
        Data,
        Default
    }
    public enum LogType
    {
        Normal,
        Warning,
        Danger
    }
    public class Log : IAggregateRoot
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(1000)]
        public string Action { get; set; }
        [MaxLength(50)]
        public string Object { get; set; }
        public int ObjectId { get; set; }
        [MaxLength(50)]
        public string ObjectType { get; set; }
        public long Timestamp { get; set; }
        public DateTime CreatedDate { get; set; }
        public string SystemUser { get; set; }
        public int SystemUserId { get; set; }
        public string ValueBefore { get; set; }
        public string ValueAfter { get; set; }
        public LogType Type { get; set; }
        public LogStatus Status { get; set; }
    }
    public class BaseLogDataModel<T> where T : class
    {
        public T DataBefore { get; set; }
        public T DataAfter { get; set; }
    }
    public class BaseLogModel<T> where T : class
    {
        public T DataBefore { get; set; }
        public T DataAfter { get; set; }
        public Log Log { get; set; }
    }
    public class WriteLogModel
    {
        //string name,int objectId,object valueBefore, object valueAfter, string table=null
        public string Name { get; set; }
        public int ObjectId { get; set; }
    }
    public class LogModel
    {
        public string Action { get; set; }
        public string ControllerName { get; set; }
        public int ObjectId { get; set; }
        public string TableName { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime CreatedDate { get; set; }
        public object ValueBefore
        {
            set
            {
                if (value == null)
                {
                    StrValueBefore = null;
                }
                else
                {
                    string json = JsonConvert.SerializeObject(value, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    StrValueBefore = json;
                }
            }
        }
        public object ValueAfter
        {
            set
            {
                if (value == null)
                {
                    StrValueAfter = null;
                }
                else
                {
                    string json = JsonConvert.SerializeObject(value, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    StrValueAfter = json;
                }
            }
        }
        public LogType Type { get; set; } = LogType.Normal;
        public LogStatus Status { get; set; } = LogStatus.Data;
        public string StrValueBefore { get; set; }
        public string StrValueAfter { get; set; }
    }
}
