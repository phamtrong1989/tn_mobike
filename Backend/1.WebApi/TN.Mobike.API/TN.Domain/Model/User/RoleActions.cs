﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class RoleAction :  IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [ForeignKey("RoleController")]
        public string ControllerId { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string ActionName { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool IsShow { get; set; }
        public virtual RoleController RoleController { get; set; }
    }
    public class RoleActionModel
    {
        public int Id { get; set; }
        public string AreaName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
    }
    //public class RoleActionModel
    //{
    //    public int Id { get; set; }
    //    public int ControllerId { get; set; }
    //    public string Name { get; set; }
    //    public string ControllerName { get; set; }
    //    public string AreaName { get; set; }
    //    public string Description { get; set; }
    //    public int Order { get; set; }
    //    public bool IsShow { get; set; }
    //}
}
