﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class ApplicationRole : IdentityRole<int>, IAggregateRoot
    {
        [Display(Name= "Description")]
        public string Description { get; set; }
    }
    public class RoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Description { get; set; }
        public string NormalizedName { get; set; }
        public int NumberOfUsers { get; set; }
    }
}
