﻿using TN.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class RoleController : IAggregateRoot
    {
        [Key]
        public string Id { get; set; }
        [ForeignKey("RoleArea")]
        public string AreaId { get; set; }
        [ForeignKey("RoleGroups")]
        public int GroupId { get; set; }
        [MaxLength(200)]
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool IsShow { get; set; }
        public virtual RoleGroup RoleGroups { get; set; }
        public virtual RoleArea RoleArea { get; set; }

    }
}
