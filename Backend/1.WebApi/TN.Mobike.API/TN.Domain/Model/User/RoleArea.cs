﻿using System.ComponentModel.DataAnnotations;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class RoleArea : IAggregateRoot
    {
        [Key]
        public string Id { get; set; }
        [MaxLength(200)]
        public string Name { get; set; }
        public int Order { get; set; }
        public bool IsShow { get; set; }
    }
}
