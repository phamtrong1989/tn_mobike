﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class RoleGroup : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [MaxLength(200)]
        public string Name { get; set; }
        public string Description { get; set; }

        public int Order { get; set; }
        public bool IsShow { get; set; }
    }
}
