﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Booking : Entity, IAggregateRoot
    {
        

        public enum BookingStatus
        {
            Booking = 0,
            Start = 1,
            End = 2,
        }

        public int AccountId { get; set; }
        public BookingStatus StatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Vote { get; set; }
        public string Messenger { get; set; }
    }
    public class BookingModel
    {
        public int Id { get; set; }
        [Display(Name = "AccountId")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        public int AccountId { get; set; }
        public Booking.BookingStatus StatusId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Vote { get; set; }
        public string Messenger { get; set; }
    }
}
