﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Bike : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Default = -1,
            Inactive = 0,
            Active = 1,
            Error = 2,
        }

        public enum EType
        {
            Unknow = -1,
            Nam = 0,
            Nữ = 1,
        }
        public enum ERent
        {
            ChuaThue = 0,
            DangThue = 1,
        }
        public string Etag { get; set; }
        public EStatus Status { get; set; }
        public EType Type { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string IMEI { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedSystemUserId { get; set; }
        public ERent RentId { get; set; }

        public Bike()
        {
            RentId = ERent.ChuaThue;
            Status = EStatus.Default;
            Type = EType.Unknow;
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }
    public class BikeModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Mã Etag của xe")]
        [RegularExpression("^[0-9,a-z,A-Z]{0,20}$", ErrorMessage = "{0} không dùng ký tự đặc biệt!")]
        public string Etag { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Trạng thái sử dụng")]
        public Bike.EStatus Status { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Model")]
        public string Model { get; set; }
        [Display(Name = "Kiểu dáng")]
        public Bike.EType Type { get; set; } = Bike.EType.Unknow;
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Số Serial")]
        public string SerialNumber { get; set; }
        [Display(Name = "Mã IMEI")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        [RegularExpression("^[0-9]{15}$", ErrorMessage = "{0} là chuỗi số có 15 ký tự!")]
        public string IMEI { get; set; }
        public DateTime CreatedDate { get; set; }
        [ForeignKey("SystemUser")]
        public int CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        [ForeignKey("SystemUser")]
        public int UpdatedSystemUserId { get; set; }
        public SelectList TypeSelectList { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Trạng thái thuê xe")]
        public Bike.ERent RentId { get; set; }
    }
}
