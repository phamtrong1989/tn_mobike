﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Account: Entity, IAggregateRoot
    {
        public enum ESex
        {
            Nam = 1,
            Nu = 0,
            Khong = -1
        }

        public enum EAccountType
        {
            Normal = 1,
            Gold = 2,
            Diamond = 3
        }

        public enum EStatus
        {
            Inactive = 0,
            Active = 1,
        }
        
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string Etag { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public ESex Sex { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string MobileToken { get; set; }
        public bool IsConfirmedPhone { get; set; }
        public bool IsConfirmedIdentity { get; set; }
        public EStatus Status { get; set; }
        public string Note { get; set; }
        public string NoteLock { get; set; }
        public EAccountType Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedSystemUserId { get; set; }
        public string Language { get; set; }
        public string Code { get; set; }
        public int Points { get; set; }
        public virtual AccountToken AccountToken { get; set; }
        public DateTime ExpTocken { get; set; }
        public string CodeNguoiChiaSe { get; set; }
    }
    public class AccountModel
    {
        public int Id { get; set; }
        [Display(Name ="Tên tài khoản")]
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        [Display(Name = "Mã thẻ Etag")]
        [Required(ErrorMessage ="{0} không được để trống!")]
        public string Etag { get; set; }
        public string Avatar { get; set; }
        [Display(Name = "Địa chỉ Email")]
        public string Email { get; set; }
        [Display(Name = "Giới tính")]
        public Account.ESex Sex { get; set; }
        [Display(Name = "Tên")]
        public string FirstName { get; set; }
        [Display(Name = "Họ")]
        public string LastName { get; set; }
        [Display(Name = "Ngày sinh")]
        public DateTime? Birthday { get; set; }
        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }
        public string MobileToken { get; set; }
        public bool? IsConfirmedPhone { get; set; }
        public bool? IsConfirmedIdentity { get; set; }
        [Display(Name = "Trạng thái hoạt động")]
        public Account.EStatus? Status { get; set; }
        public string Note { get; set; }
        public string NoteLock { get; set; }
        [Display(Name = "Hạng hội viên")]
        public Account.EAccountType? Type { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedSystemUserId { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedSystemUserId { get; set; }
        public virtual AccountToken AccountToken { get; set; }
        public bool IsChangePassword { get; set; }
        public string Language { get; set; }
    }
   
}
