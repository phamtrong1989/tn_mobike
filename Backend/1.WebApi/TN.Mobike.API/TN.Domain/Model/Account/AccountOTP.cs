﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class AccountOTP: IAggregateRoot
    {
        public enum OTPType
        {
            Register = 0,
            Login = 1
        }
        [Key]
        public int Id { get; set; }
        public int? AcountId { get; set; }
        public string Phone { get; set; }
        public int OTP { get; set; }
        public int? InvalidCount { get; set; } = 0;
        public OTPType Type { get; set; } = OTPType.Register;
        // Xác nhận nhập đúng mã = true
        public bool Status { get; set; } = false;
        // Thời gian xác nhận
        public DateTime? ConfirmDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
