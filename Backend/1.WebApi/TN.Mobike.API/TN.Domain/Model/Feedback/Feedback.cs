﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Feedback : Entity, IAggregateRoot
    {
        public enum FBStatus
        {
            UnRead = 0,
            Read = 1,
        }
        public int AccountId { get; set; }
        public string Message { get; set; }
        public FBStatus StatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Rating { get; set; }
    }
}
