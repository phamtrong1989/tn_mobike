﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class StationControlRequest : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Pending = 0,
            Done = 1,
            Cancel = 2,
        }

        public enum EType
        {
            Lock = 0,
            Unlock = 1,
        }
        [ForeignKey("Station")]
        public int StationId { get; set; }
        [ForeignKey("Dock")]
        public int DockId { get; set; }
        [ForeignKey("Account")]
        public int AccountId { get; set; }
        [ForeignKey("Ticket")]
        public int TicketId { get; set; }
        public int TransactionId { get; set; }
        public EType Type { get; set; }
        public EStatus Status { get; set; }
        public DateTime RequestedDate { get; set; }

        public StationControlRequest() { }

        //public StationControlRequest(int stationId, int dockId, int accountId, int ticketId, EType type)
        //{
        //    this.StationId = stationId;
        //    this.DockId = dockId;
        //    this.AccountId = accountId;
        //    this.TicketId = ticketId;
        //    this.Type = type;
        //    this.Status = EStatus.Pending;
        //    RequestedDate = DateTime.Now;
        //}

        public static StationControlRequest CreateUnlockRequest(int stationId, int dockId, int accountId, int ticketId)
        {
            return new StationControlRequest
            {
                StationId = stationId,
                DockId = dockId,
                AccountId = accountId,
                TicketId = ticketId,
                Type = EType.Unlock,
                Status = EStatus.Pending,
                RequestedDate = DateTime.Now,
            };
        }

        public static StationControlRequest CreateLockRequest(int stationId, int dockId, int accountId, int transactionid)
        {
            return new StationControlRequest
            {
                StationId = stationId,
                DockId = dockId,
                AccountId = accountId,
                TransactionId = transactionid,
                Type = EType.Lock,
                Status = EStatus.Pending,
                RequestedDate = DateTime.Now,
            };
        }
    }
}
