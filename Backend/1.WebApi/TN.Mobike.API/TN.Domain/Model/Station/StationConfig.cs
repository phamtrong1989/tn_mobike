﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class StationConfig : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Inactive = 0,
            Active = 1,
        }
        public string Name { get; set; }
        public string Note { get; set; }
        public string ServerIP { get; set; }
        public int ServerPort { get; set; }
        public EStatus Status { get; set; }
        public bool IsDefault { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedSystemUserId { get; set; }

        public StationConfig()
        {
            Status = EStatus.Active;
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            IsDefault = false;
        }
    }
    public class StationConfigModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Tên cấu hình")]
        public string Name { get; set; }
        [Display(Name = "Mô tả")]
        public string Note { get; set; }
        [Display(Name = "Server IP")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        public string ServerIP { get; set; }
        [Display(Name = "Server Port")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Range(1, 999999, ErrorMessage = "{0} phải là số!")]
        public int ServerPort { get; set; }
        [Display(Name = "Trạng thái sử dụng")]
        public StationConfig.EStatus Status { get; set; }
        [Display(Name = "Chọn làm cấu hình mặc định")]
        public bool IsDefault { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedSystemUserId { get; set; }
    }
}
