﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Station : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Inactive = 0,
            Active = 1,
        }
#pragma warning disable CS0114 // Member hides inherited member; missing override keyword
        public int Id { get; set; }
#pragma warning restore CS0114 // Member hides inherited member; missing override keyword
        public string Name { get; set; }
        public string Address { get; set; }
        [MaxLength(20)]
        public string IMEI { get; set; }
        public string Token { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        [ForeignKey("StationConfig")]
        public int StationConfigId { get; set; }
        public int TotalDock { get; set; }
        public int ErrorDock { get; set; }
        public int TotalBike { get; set; }
        public int BookedBike { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public EStatus Status { get; set; }
        public bool UpdateFlag { get; set; }
        public bool ConnectionStatus { get; set; }
        public bool ErrorStatus { get; set; }
        public float UpsVoltage { get; set; }
        public float BatteryVoltage { get; set; }
        public DateTime LastConnectionTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedSystemUserId { get; set; }
        //[JsonIgnore]
        public virtual StationConfig StationConfig { get; set; }
       // [JsonIgnore]
        public virtual IEnumerable<Dock> Docks { get; set; }

        public Station()
        {
            Status = EStatus.Active;
            ConnectionStatus = false;
            UpdateFlag = false;
            TotalDock = 0;
            ErrorDock = 0;
            TotalBike = 0;
            BookedBike = 0;
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            LastConnectionTime = DateTime.Now;
            this.Docks = new HashSet<Dock>();
        }
    }
    public class StationModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Tên trạm")]
        public string Name { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Model")]
        public string Model { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Số IMEI")]
        [RegularExpression("^[0-9]{15}$", ErrorMessage = "{0} là chuỗi số có 15 ký tự!")]
        public string IMEI { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Số Serial")]
        public string SerialNumber { get; set; }
        [Display(Name = "Tổng số dock")]
        public int? TotalDock { get; set; }
        [Display(Name = "Tổng số xe")]
        public int? TotalBike { get; set; }
        [Display(Name = "Xe đã đặt")]
        public int? BookedBike { get; set; }
        [Display(Name = "Số dock lỗi")]
        public int? ErrorDock { get; set; }
        [Display(Name = "Trạng thái sử dụng")]
        public Station.EStatus? Status { get; set; }
        public bool? ConnectionStatus { get; set; }
        public DateTime? CreatedDate { get; set; }
        [ForeignKey("SystemUser")]
        public int? CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        [ForeignKey("SystemUser")]
        public int? UpdatedSystemUserId { get; set; }
        [Display(Name = "Vĩ độ")]
        public double Lat { get; set; }
        [Display(Name = "Kinh độ")]
        public double Lng { get; set; }
        [Required(ErrorMessage = "Vui lòng chọn cấu hình!")]
        [Display(Name = "Chọn cấu hình")]
        public int StationConfigId { get; set; }
    }
}
