﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Dock : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Inactive = 0,
            Active = 1,
            Error = 2,
        }
#pragma warning disable CS0114 // Member hides inherited member; missing override keyword
        public int Id { get; set; }
#pragma warning restore CS0114 // Member hides inherited member; missing override keyword
        [ForeignKey("Station")]
        public int StationId { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string IMEI { get; set; }
        public int OrderNumber { get; set; }
        public EStatus Status { get; set; }
        public bool LockStatus { get; set; }
        public bool ErrorStatus { get; set; }
        public bool BikeAlarm { get; set; }
        public int? BikeAlarmId { get; set; }
        public int? CurrentBikeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedSystemUserId { get; set; }
        public virtual Station Station { get; set; }

        public Dock()
        {
            Status = EStatus.Active;
            BikeAlarm = false;
            LockStatus = true;
            OrderNumber = 0;
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }
    public class DockModel
    {
        public int Id { get; set; }
        [Display(Name = "Trạm")]
        [Required(ErrorMessage = "Vui lòng chọn trạm!")]
        public int StationId { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Model")]
        public string Model { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Số Serial")]
        public string SerialNumber { get; set; }
        [Required(ErrorMessage = "{0} không được để trống!")]
        [Display(Name = "Số IMEI")]
        [RegularExpression("^[0-9]{15}$", ErrorMessage = "{0} là chuỗi số có 15 ký tự!")]
        public string IMEI { get; set; }
        [Display(Name = "Thứ tự trong trạm")]
        [RegularExpression("^[0-9]{0,100}$", ErrorMessage = "{0} là số nguyên lớn hơn 0!")]
        public int OrderNumber { get; set; }

        [Display(Name = "Trạng thái")]
        public Dock.EStatus Status { get; set; }

        public int? CurrentBikeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedSystemUserId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedSystemUserId { get; set; }
        public SelectList StationSelectList { get; set; }
    }
}
