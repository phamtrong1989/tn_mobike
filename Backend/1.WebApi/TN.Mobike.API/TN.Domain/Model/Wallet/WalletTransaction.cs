﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class WalletTransaction : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Pending = 0,
            Done = 1,
            Cancel = 2,
        }

        public enum EType
        {
            Deposit = 1,
            Withdraw = 2,
            Paid = 3,
        }
        [ForeignKey("Wallet")]
        public int WalletId { get; set; }
        [ForeignKey("Ticket")]
        public int TicketId { get; set; }
        public EType Type { get; set; }
        public double Amount { get; set; }
        public EStatus Status { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
