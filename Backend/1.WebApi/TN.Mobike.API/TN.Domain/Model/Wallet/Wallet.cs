﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Wallet : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Inactive = 0,
            Active = 1,
        }
        [ForeignKey("Account")]
        public int AccountId { get; set; }
        public double Balance { get; set; }
        public EStatus Status { get; set; }
    }
}
