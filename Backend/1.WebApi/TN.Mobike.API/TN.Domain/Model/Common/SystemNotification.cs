﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class SystemNotification : Entity, IAggregateRoot
    {
        public enum EStatus
        {
            Undefined,
            Error,
            Normal,
            Offnormal
        }

        public enum EType
        {
            Notification,
            Alarm,
        }

        public enum EObjectType
        {
            Account,
            Station,
            Dock,
            Bike,
            Ticket,
            TicketTransaction,
        }

        public enum EContentType
        {
            Blank,
            Bike_Lock_Mismatch,
            Bike_Status_Mismatch,
            Bike_Etag_Invalid,
        }

        public int ObjectId { get; set; }
        public EObjectType ObjectType { get; set; }
        public EStatus Status { get; set; }
        public EType Type { get; set; }
        public EContentType ContentType { get; set; }
        public string Message { get => BuildMessage(); }

        public SystemNotification() { }

        public SystemNotification(int objectId, EObjectType objectType, EStatus status, EType type, EContentType contentType = EContentType.Blank)
        {
            this.ObjectId = objectId;
            this.ObjectType = objectType;
            this.Status = status;
            this.Type = type;
            this.ContentType = contentType;
        }

        private string BuildMessage()
        {
            string msg = "";
            //switch (ContentType)
            //{
            //    case EType.Undefined:
            //        msg = "Không xác định";
            //        break;
            //    case EType.LockDockFailed:
            //        msg = "Khóa xe không thành công";
            //        break;
            //    case EType.LockDockSuccessfully:
            //        msg = "Khóa xe thành công";
            //        break;
            //    case EType.UnlockDockFailed:
            //        msg = "Mở khóa xe không thành công";
            //        break;
            //    case EType.UnlockDockSuccessfully:
            //        msg = "Mở khóa xe thành công";
            //        break;
            //}
            return msg;
        }
    }
}
