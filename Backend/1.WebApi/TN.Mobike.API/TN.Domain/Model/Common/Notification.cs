﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using TN.Domain.Seedwork;

namespace TN.Domain.Model
{
    public class Notification : Entity, IAggregateRoot
    {
        private IStringLocalizer<string> _localizer;

        public enum EType
        {
            Undefined = 0,
            UnlockDockSuccessfully = 1,
            UnlockDockFailed = 2,
            LockDockSuccessfully = 3,
            LockDockFailed = 4,
            PaymentSuccessfully = 5,
            PaymentFailed = 6
        }

        public int AccountId { get; set; }
        public int TicketId { get; set; }
        public int RequestId { get; set; }
        public int TransactionId { get; set; }
        public EType Type { get; set; }
        public string Message { get => BuildMessage(); }
        [NotMapped]
        public string Data { get; set; }
        [NotMapped]
        public string Language { get; set; }

        public Notification() { }

        public Notification(IStringLocalizer<string> localizer, string lang, int accountId, int ticketId, EType type)
        {
            this.AccountId = accountId;
            this.TicketId = ticketId;
            this.RequestId = 0;
            this.TransactionId = 0;
            this.Type = type;
            this._localizer = localizer;
            this.Language = lang;
        }


        public Notification(IStringLocalizer<string> localizer, string lang, int accountId, int ticketId, int requestId, EType type)
        {
            this.AccountId = accountId;
            this.TicketId = ticketId;
            this.RequestId = requestId;
            this.TransactionId = 0;
            this.Type = type;
            this._localizer = localizer;
            this.Language = lang;
        }

        public Notification(IStringLocalizer<string> localizer, string lang, int accountId, int ticketId, int requestId, int transactionId, EType type)
        {
            this.AccountId = accountId;
            this.TicketId = ticketId;
            this.RequestId = requestId;
            this.TransactionId = transactionId;
            this.Type = type;
            this._localizer = localizer;
            this.Language = lang;
        }

        private string BuildMessage()
        {
            //_localizer = _localizer.WithCulture(new System.Globalization.CultureInfo(Language));
            //return GetString(Type.ToString());
            switch (Language.ToLower())
            {
                case "en-us":
                    switch (Type)
                    {
                        case EType.Undefined:
                            return "Undefined";
                        case EType.LockDockFailed:
                            return "Lock dock failed";
                        case EType.LockDockSuccessfully:
                            return "Lock dock successfully";
                        case EType.UnlockDockFailed:
                            return "Unlock dock failed";
                        case EType.UnlockDockSuccessfully:
                            return "Unlock dock successfully";
                        default:
                            return Type.ToString();
                    }
                default:
                    switch (Type)
                    {
                        case EType.Undefined:
                            return "Không xác định";
                        case EType.LockDockFailed:
                            return "Khóa dock thất bại";
                        case EType.LockDockSuccessfully:
                            return "Khóa dock thành công";
                        case EType.UnlockDockFailed:
                            return "Mở khóa thất bại";
                        case EType.UnlockDockSuccessfully:
                            return "Mở khóa thành công";
                        case EType.PaymentSuccessfully:
                            return "Thanh toán thành công";
                        case EType.PaymentFailed:
                            return "Thanh toán thất bại";
                        default:
                            return Type.ToString();
                    }
            }
        }

        private string GetString(string name) => _localizer[name];
    }
}
