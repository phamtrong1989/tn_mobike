﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TN.Domain.Model
{
    public class RoleManagerModel
    {
        public int Id { get; set; } = 0;
        [Display(Name = "Tên nhóm quyền")]
        [Required(ErrorMessage = "{0} không được để trống!")]
        [StringLength(100, ErrorMessage = "{0} từ {2} đến {1} ký tự!", MinimumLength = 10)]
        public  string Name { get; set; }
        [Display(Name = "Mô tả")]
        [StringLength(1000, ErrorMessage = "{0} từ {2} đến {1} ký tự!", MinimumLength = 0)]
        public string Description { get; set; }
        public string TreeData { get; set; }
    }
}
