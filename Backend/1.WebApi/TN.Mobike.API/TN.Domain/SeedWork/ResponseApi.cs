﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TN.Domain.Seedwork
{
    public class ApiResponse<T> where T : class
    {
        public enum Code
        {
            Ok = 0,
            InvalidData = 1,
            ServerError = 2,
            DockNotExist = 3,
            DockNotActive = 4,
            DockError = 5,
            DockHasNoBike = 6,
            StationNotExist = 7,
            DockControlFailed = 8,
            DockAlreadyHasBike = 9,
            TicketInvalid = 10,
            AccountNotExist = 11,
            BikeNotExist = 12,
            TransactionNotExist = 13,
            RequestNotExist = 14,
            PhoneNumberExist = 15,
            UsernameNotExist = 16,
            EmailExist = 17,
            InvalidLogin = 18,
            OTPOutTime = 19,
            TransactionInvalid = 20,
            OTPInvalid = 21,
            OrderDone = 22,
            InvalidAccount = 23,
            BookingNotExist = 24,
            BalanceNotExist = 25,
            BalanceNotMoney = 26,
            DeviceNotExist = 27,
            EmailNotExist = 28,
            BikeIsError = 29,
            BikeIsRented = 30,
            BookingEnd = 31,
            BookingStart = 32,
            BookingIsNotRented = 33,
        }

        public bool Status { get; set; } = false;
        public string Message { get; set; }
        public Code ErrorCode { get; set; }
        public T Data { get; set; }
        public long? TotalRecord { get; set; }

        public static string BuildMessage(Code code, string errorMsg = null)
        {
            string msg = string.Empty;
            switch (code)
            {
                case Code.Ok:
                    msg = "Thành công";
                    break;
                case Code.InvalidData:
                    msg = "Dữ liệu không hợp lệ";
                    break;
                case Code.DockNotExist:
                    msg = "Dock không tồn tại";
                    break;
                case Code.DockNotActive:
                    msg = "Dock chưa kích hoạt";
                    break;
                case Code.DockError:
                    msg = "Dock lỗi";
                    break;
                case Code.DockHasNoBike:
                    msg = "Dock không có xe";
                    break;
                case Code.StationNotExist:
                    msg = "Trạm không tồn tại";
                    break;
                case Code.DockControlFailed:
                    msg = "Điều khiển dock không thành công";
                    break;
                case Code.DockAlreadyHasBike:
                    msg = "Dock đã tồn tại xe khác";
                    break;
                case Code.TicketInvalid:
                    msg = "Mã vé không hợp lệ";
                    break;
                case Code.AccountNotExist:
                    msg = "Tài khoản không tồn tại";
                    break;
                case Code.BikeNotExist:
                    msg = "Xe không tồn tại";
                    break;
                case Code.TransactionNotExist:
                    msg = "Giao dịch không tồn tại";
                    break;
                case Code.RequestNotExist:
                    msg = "Lệnh điều khiển không tồn tại";
                    break;
                case Code.PhoneNumberExist:
                    msg = "Số điện thoại đã tồn tại, vui lòng chọn số điện thoại khác";
                    break;
                case Code.UsernameNotExist:
                    msg = "Tài khoản không tồn tại";
                    break;
                case Code.EmailExist:
                    msg = "Email này đã có người sử dụng, vui lòng chọn email khác";
                    break;
                case Code.InvalidLogin:
                    msg = "Thông tin đăng nhập không hợp lệ";
                    break;
                case Code.OTPOutTime:
                    msg = "Xác nhận OTP hết hạn hoặc không chính xác vui lòng quay lại và thực hiện lại";
                    break;
                case Code.TransactionInvalid:
                    msg = "Mã giao dịch không hợp lệ";
                    break;
                case Code.OTPInvalid:
                    msg = "Mã OTP không chính xác, vui lòng nhập lại";
                    break;
                case Code.OrderDone:
                    msg = "Trừ tiền thành công";
                    break;
                case Code.InvalidAccount:
                    msg = "Tài khoản không hợp lệ";
                    break;
                case Code.BookingNotExist:
                    msg = "Thông tin booking không hợp lệ";
                    break;
                case Code.BalanceNotExist:
                    msg = "Thông tin tài khoản thanh toán không hợp lệ";
                    break;
                case Code.BalanceNotMoney:
                    msg = "Tài khoản không còn tiền";
                    break;
                case Code.DeviceNotExist:
                    msg = "Thiết bị không hợp lệ";
                    break;
                case Code.EmailNotExist:
                    msg = "Email chưa tồn tại";
                    break;
                case Code.BikeIsError:
                    msg = "Xe đang bị hỏng hoặc chưa sẵn sàng cho thuê";
                    break;
                case Code.BikeIsRented:
                    msg = "Xe đang được người khác thuê thuê";
                    break;
                case Code.BookingEnd:
                    msg = "Lịch đặt xe đã thực hiện thuê xe và kết thúc";
                    break;
                case Code.BookingStart:
                    msg = "Lịch đặt xe đã thực hiện thuê xe và chưa trả xe";
                    break;
                case Code.BookingIsNotRented:
                    msg = "Bạn mới đặt lịch nhưng chưa thuê xe";
                    break;
                default:
                    msg = code.ToString();
                    break;
            }

            return string.IsNullOrEmpty(errorMsg) ? msg : $"{msg} : {errorMsg}";
        }
    }

    public class ResponseApi<T> : ApiResponse<T> where T : class
    {
    }

    public class ApiResponse : ApiResponse<object>
    {
        

        public static ApiResponse WithData(IEnumerable<object> list, string message = null)
        {
            var lst = list.ToList();
            var response = new ApiResponse
            {
                Data = lst,
                TotalRecord = lst.Count,
                Status = true,
                ErrorCode = 0
            };
            response.Message = message == null ? BuildMessage((Code)response.ErrorCode) : BuildMessage((Code)response.ErrorCode, message);

            return response;
        }

        public static ApiResponse WithData(object data, int size = 1, string message = null)
        {
            var response = new ApiResponse
            {
                Data = data,
                TotalRecord = data == null ? 0 : size,
                Status = true,
                ErrorCode = 0
            };
            response.Message = message == null ? BuildMessage((Code)response.ErrorCode) : BuildMessage((Code)response.ErrorCode, message);

            return response;
        }

        public static ApiResponse WithStatus(bool status, Code errorCode = ApiResponse<object>.Code.Ok, string message = null)
        {
            var response = new ApiResponse
            {
                Status = status,
                ErrorCode = errorCode,
                Message = message
            };

            response.Message = message == null ? BuildMessage((Code)response.ErrorCode) : BuildMessage((Code)response.ErrorCode, message);

            return response;
        }
    }
}
