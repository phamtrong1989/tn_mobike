﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.Infrastructure
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser, ApplicationRole,int>, IUnitOfWork
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
             : base(options)
        {
        }

        // System User
        public DbSet<RoleController> RoleControllers { get; set; }
        public DbSet<RoleAction> RoleActions { get; set; }
        public DbSet<RoleDetail> RoleDetails { get; set; }
        public DbSet<RoleGroup> RoleGroups { get; set; }
        public DbSet<RoleArea> RoleAreas { get; set; }

        // Account
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountToken> AccountTokens { get; set; }
        public DbSet<AccountOTP> AccountOTPs { get; set; }

        // Station
        public DbSet<Station> Stations { get; set; }
        public DbSet<Dock> Docks { get; set; }
        public DbSet<StationConfig> StationConfigs { get; set; }
        public DbSet<StationControlRequest> StationControlRequests { get; set; }
        public DbSet<Bike> Bikes { get; set; }

        // Wallet
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<WalletTransaction> WalletTransactions { get; set; }

        // Ticket
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketTransaction> TicketTransactions { get; set; }
        public DbSet<TicketTransactionHistory> TicketTransactionHistory { get; set; }

        // Common
        public DbSet<Log> Logs { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<SystemNotification> SystemNotifications { get; set; }

        //Booking
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(builder);

            builder.Entity<RoleDetail>().ToTable("RoleDetail", "adm").HasKey(c => new { c.ActionId, c.RoleId });
            builder.Entity<RoleController>().ToTable("RoleController", "adm");
            builder.Entity<RoleAction>().ToTable("RoleAction", "adm");
            builder.Entity<RoleGroup>().ToTable("RoleGroup", "adm");
            builder.Entity<RoleArea>().ToTable("RoleArea", "adm");
            builder.Entity<ApplicationUser>().ToTable("User", "adm");
            builder.Entity<ApplicationRole>().ToTable("Role", "adm");
            builder.Entity<IdentityUserRole<int>>().ToTable("UserRole", "adm");
            builder.Entity<IdentityRoleClaim<int>>().ToTable("RoleClaim", "adm");
            builder.Entity<IdentityUserLogin<int>>().ToTable("UserLogin", "adm");
            builder.Entity<IdentityUserToken<int>>().ToTable("UserToken", "adm");
            builder.Entity<IdentityUserClaim<int>>().ToTable("UserClaim", "adm");
            builder.Entity<Log>().ToTable("Log", "adm").HasIndex(x => new { x.CreatedDate, x.Type, x.ObjectType, x.Action, x.SystemUser });
            builder.Entity<Account>().ToTable("Account");
            builder.Entity<AccountToken>().ToTable("AccountToken");
            builder.Entity<AccountOTP>().ToTable("AccountOTP");
            builder.Entity<Station>().ToTable("Station").HasIndex(x => new { x.Status, x.ConnectionStatus });
            builder.Entity<StationConfig>().ToTable("StationConfig");
            builder.Entity<StationControlRequest>().ToTable("StationControlRequest");
            builder.Entity<Dock>().ToTable("Dock").HasIndex(x => new { x.StationId, x.Status, x.BikeAlarmId, x.ErrorStatus });
            builder.Entity<Bike>().ToTable("Bike").HasIndex(x => new { x.Status, x.Type });
            builder.Entity<Ticket>().ToTable("Ticket");
            builder.Entity<TicketTransaction>().ToTable("TicketTransaction");
            builder.Entity<TicketTransactionHistory>().ToTable("TicketTransactionHistory");

            builder.Entity<Wallet>().ToTable("Wallet");
            builder.Entity<WalletTransaction>().ToTable("WalletTransaction");
            builder.Entity<Notification>().ToTable("Notification");
            builder.Entity<SystemNotification>().ToTable("SystemNotification");
            builder.Entity<Booking>().ToTable("Booking");
            builder.Entity<Feedback>().ToTable("Feedback");
        }
        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await base.SaveChangesAsync();
            return true;
        }
    }
}
