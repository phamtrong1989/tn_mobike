﻿using System.Collections.Generic;
using TN.Domain.Model;
using System.Threading.Tasks;
using System.Linq;
using System;
using System.Linq.Expressions;

namespace TN.Infrastructure.Interfaces
{
    public interface IStationRepository : IEntityBaseRepository<Station>
    {
        Task<List<Station>> FindByLatLng(double min_lat, double max_lat, double min_lng, double max_lng);
        Task<Station> FindByImei(string imei);
        Task<bool> CheckAvaiable(int stationId);
        Task<List<Station>> FindByMap(Expression<Func<Station, bool>> predicate = null, Func<IQueryable<Station>, IOrderedQueryable<Station>> orderBy = null, Expression<Func<Station, Station>> select = null);
    }
}
