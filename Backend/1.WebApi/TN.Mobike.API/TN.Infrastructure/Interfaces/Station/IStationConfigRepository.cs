﻿using System;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;

namespace TN.Infrastructure.Interfaces
{
    public interface IStationConfigRepository : IEntityBaseRepository<StationConfig>
    {
        Task<bool> CheckAvaiable(int stationConfigId);
    }
}
