﻿using TN.Domain.Model;
using System.Threading.Tasks;

namespace TN.Infrastructure.Interfaces
{
    public interface ITicketTransactionRepository : IEntityBaseRepository<TicketTransaction>
    {
    }
}
