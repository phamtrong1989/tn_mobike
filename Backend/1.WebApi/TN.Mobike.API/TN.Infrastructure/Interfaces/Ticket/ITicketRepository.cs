﻿using TN.Domain.Model;

namespace TN.Infrastructure.Interfaces
{
    public interface ITicketRepository : IEntityBaseRepository<Ticket>
    {
    }
}
