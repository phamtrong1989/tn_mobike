﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class TicketTransactionRepository : EntityBaseRepository<TicketTransaction>, ITicketTransactionRepository
    {
        public TicketTransactionRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
