﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class TicketRepository : EntityBaseRepository<Ticket>, ITicketRepository
    {
        public TicketRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
