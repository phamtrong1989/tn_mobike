﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace TN.Infrastructure.Repositories
{
    public class StationRepository : EntityBaseRepository<Station>, IStationRepository
    {
        public StationRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<Station>> FindByLatLng(double min_lat, double max_lat, double min_lng, double max_lng)
        {
            return await _context.Stations.Where(x => x.Lat >= min_lat && x.Lat <= max_lat
                                     && x.Lng >= min_lng && x.Lng <= max_lng).Include(x => x.Docks).ToListAsync();
                                   //  .ThenInclude(y => y.CurrentBike)
        }

        public async Task<Station> FindByImei(string imei)
        {
            return await SearchOneAsync(x => x.IMEI == imei);
        }

        public async Task<bool> CheckAvaiable(int stationId)
        {
            return await _context.Docks.AnyAsync(x => x.StationId == stationId);
        }
        public virtual async Task<List<Station>> FindByMap(Expression<Func<Station, bool>> predicate = null, Func<IQueryable<Station>, IOrderedQueryable<Station>> orderBy = null, Expression<Func<Station, Station>> select = null)
        {
            IQueryable<Station> query = _context.Set<Station>();
            if (predicate != null)
            {
                query = _context.Set<Station>().Where(predicate).AsQueryable();
            }
            if (orderBy != null)
            {
                query = orderBy(query).AsQueryable();
            }
            query = query.Include(m => m.Docks);
            //    .ThenInclude(m=>m.CurrentBike);
            if (select != null)
            {
                query = query.Select(select).AsQueryable();
            }
            return await query.ToListAsync();
        }
    }
}
