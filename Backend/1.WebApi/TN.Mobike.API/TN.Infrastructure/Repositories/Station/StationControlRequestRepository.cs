﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.Infrastructure.Repositories
{
    public class StationControlRequestRepository : EntityBaseRepository<StationControlRequest>, IStationControlRequestRepository
    {
        public StationControlRequestRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<StationControlRequest> GetByStationAndDock(int stationid, int dockid, StationControlRequest.EType command)
        {
            return await SearchOneAsync(x => x.StationId == stationid && x.DockId == dockid && x.Type == command);
        }
    }
}
