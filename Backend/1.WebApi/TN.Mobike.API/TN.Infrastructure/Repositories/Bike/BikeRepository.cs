﻿using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;

namespace TN.Infrastructure.Repositories
{
    public class BikeRepository : EntityBaseRepository<Bike>, IBikeRepository
    {
        public BikeRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<Bike> FindByEtag(string eTag)
        {
            return await SearchOneAsync(x => x.Etag == eTag);
        }
    }
}
