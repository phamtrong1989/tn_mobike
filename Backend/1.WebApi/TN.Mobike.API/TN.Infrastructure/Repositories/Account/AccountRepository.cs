﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.Infrastructure.Repositories
{
    public class AccountRepository : EntityBaseRepository<Account>, IAccountRepository
    {
        public AccountRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
