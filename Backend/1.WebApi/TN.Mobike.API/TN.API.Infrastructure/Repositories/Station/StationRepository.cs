﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class StationRepository : EntityBaseRepository<Station>, IStationRepository
    {
        public StationRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<List<Station>> FindByLatLng(double min_lat, double max_lat, double min_lng, double max_lng)
        {
            return await _context.Stations.Where(x => x.Lat >= min_lat && x.Lat <= max_lat
                                     && x.Lng >= min_lng && x.Lng <= max_lng).Include(x => x.Docks).ToListAsync();
                                     //.ThenInclude(y => y.CurrentBike)
        }

        public async Task<Station> FindByImei(string imei)
        {
            return await SearchOneAsync(x => x.IMEI == imei);
        }

        public async Task<bool> CheckAvaiable(int stationId)
        {
            return await _context.Docks.AnyAsync(x => x.StationId == stationId);
        }
    }
}
