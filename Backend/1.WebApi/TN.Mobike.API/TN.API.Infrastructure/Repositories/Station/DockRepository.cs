﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class DockRepository : EntityBaseRepository<Dock>, IDockRepository
    {
        public DockRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<Dock> FindByNumber(int stationid, int dock_number)
        {
            return await SearchOneAsync(x => x.StationId == stationid && x.OrderNumber == dock_number);
        }

        public async Task<Dock> FindByImei(string imei)
        {
            return await SearchOneAsync(x => x.IMEI == imei);
        }
    }
}
