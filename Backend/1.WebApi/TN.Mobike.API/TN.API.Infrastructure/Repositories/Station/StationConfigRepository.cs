﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class StationConfigRepository : EntityBaseRepository<StationConfig>, IStationConfigRepository
    {
        public StationConfigRepository(MobikeContext context) : base(context)
        {
        }
        public async Task<bool> CheckAvaiable(int stationConfigId)
        {
            return await _context.Stations.AnyAsync(x => x.StationConfigId == stationConfigId);
        }
    }
}
