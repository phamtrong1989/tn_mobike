﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class NotificationRepository : EntityBaseRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(MobikeContext context) : base(context)
        {

        }
    }
}
