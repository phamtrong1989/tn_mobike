﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class TicketTransactionRepository : EntityBaseRepository<TicketTransaction>, ITicketTransactionRepository
    {
        public TicketTransactionRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<TicketTransaction> FindByBike(int bikeid)
        {
            return await SearchOneAsync(x => x.BikeId == bikeid);
        }
        public async Task<TicketTransaction> FindByAccountIdBooking(int Accountid)
        {
            return await SearchOneAsync(x => x.AccountId == Accountid && x.Status == TicketTransaction.EStatus.Running);
        }
        public async Task<TicketTransaction> FindByTicket(int ticketid)
        {
            return await SearchOneAsync(x => x.TicketId == ticketid);
        }
        public bool UseStatus(int accountId)
        {
            var getLast = _context.TicketTransactions.Where(m => _context.Tickets.Any(x=>x.AccountId==accountId && m.TicketId== x.Id)).OrderByDescending(m=>m.Id).Skip(0).Take(1).AsQueryable();
            var data = getLast.FirstOrDefault();
            if (data == null) return false;
            return (data.Status == TicketTransaction.EStatus.Running ? true : false);
        }
        public TicketTransaction LastTransaction(int accountId)
        {
            var getLast = _context.TicketTransactions.Where(m => _context.Tickets.Any(x => x.AccountId == accountId && m.TicketId == x.Id)).OrderByDescending(m => m.Id).Skip(0).Take(1).AsQueryable();
            return getLast.FirstOrDefault();
        }
    }
}
