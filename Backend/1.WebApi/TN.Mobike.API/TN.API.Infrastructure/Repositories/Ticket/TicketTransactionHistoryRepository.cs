﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class TicketTransactionHistoryRepository : EntityBaseRepository<TicketTransactionHistory>, ITicketTransactionHistoryRepository
    {
        public TicketTransactionHistoryRepository(MobikeContext context) : base(context)
        {
           
        }
        public virtual TicketTransactionHistory GetByTransactionId(int accountId,int ticketTransactionId)
        {
            return  _context.TicketTransactionHistory.LastOrDefault(m => m.TicketTransactionId == ticketTransactionId && _context.Tickets.Any(x => x.Id == m.TicketId && x.AccountId == accountId));
        }
        public virtual async Task<int> UpdateByTransactionHistoryAsync(int accountId,int ticketTransactionId,int rating,string comment)
        {
            var data = _context.TicketTransactionHistory.LastOrDefault(m => m.TicketTransactionId == ticketTransactionId && _context.Tickets.Any(x => x.Id == m.TicketId && x.AccountId == accountId));
            if(data==null)
            {
                return 0;
            }
            data.Rating = rating;
            data.Comment = comment;
            _context.TicketTransactionHistory.Update(data);
            await _context.SaveChangesAsync();
            return 1;
        }
        public virtual async Task<object> TransactionHistoryGetByAccount(int accountId)
        {
            return await (from tth in _context.TicketTransactionHistory
                       join t in _context.Tickets on tth.TicketId equals t.Id
                       join st in _context.Stations on tth.StartStationId equals st.Id into sts from _sts in sts.DefaultIfEmpty()
                       join et in _context.Stations on tth.EndStationId equals et.Id  into ets from _ets in sts.DefaultIfEmpty()
                       join m  in _context.Bikes on tth.BikeId equals m.Id
                       where t.AccountId == accountId orderby tth.Id descending
                       select new  {
                           tth.BikeId,
                           tth.TicketId,
                           tth.Comment,
                           tth.EndDate,
                           tth.EndDockId,
                           tth.EndStationId,
                           tth.Note,
                           tth.Rating,
                           tth.StartDockId,
                           tth.StartDate,
                           tth.TicketTransactionId,
                           tth.StartStationId,
                           StartStation = _sts,
                           EndStation = _ets,
                           Bike = m,
                           tth.Price
                       }).ToListAsync();
        }
    }
}
