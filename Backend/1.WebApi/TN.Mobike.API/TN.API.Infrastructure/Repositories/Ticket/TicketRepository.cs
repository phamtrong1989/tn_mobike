﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class TicketRepository : EntityBaseRepository<Ticket>, ITicketRepository
    {
        public TicketRepository(MobikeContext context) : base(context)
        {
        }

        public async Task<List<Ticket>> FindByAccount(int accountid)
        {
            return await Search(x => x.AccountId == accountid);
        }
    }
}
