﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class AccountOTPRepository : EntityBaseRepository<AccountOTP>, IAccountOTPRepository
    {
        private readonly MobikeContext _mobikeContext;
        public AccountOTPRepository(MobikeContext context) : base(context)
        {
            _mobikeContext = context;
        }
        public virtual AccountOTP CheckConfigOTP(string Phone,int OTP)
        {
            var dateNow = DateTime.Now;
            var qr = _mobikeContext.AccountOTPs.OrderByDescending(m=>m.Id).Where(m => m.Phone == Phone && m.OTP == OTP && m.Status == false &&m.Type==AccountOTP.OTPType.Register).Skip(0).Take(1).AsQueryable();
            var data = qr.FirstOrDefault();
            if(data==null)
            {
                return null;
            }
            return (data.StartDate <= dateNow && dateNow <= data.EndDate)? data:null;
        }
        public virtual AccountOTP CheckOTP(string Phone)
        {
            var qr = _mobikeContext.AccountOTPs.OrderByDescending(m => m.Id).Where(m => m.Phone == Phone && m.StartDate.Date == DateTime.Now.Date && m.Type == AccountOTP.OTPType.Register).Skip(0).Take(1).AsQueryable();
            var data = qr.FirstOrDefault();
            if (data==null)
            {
                return null;
            }
            return data.Status == true ? data : null;
        }
        public virtual AccountOTP GetLastOTPByUsername(string Phone)
        {
            var qr = _mobikeContext.AccountOTPs.OrderByDescending(m => m.Id).Where(m => m.Phone == Phone && m.Type == AccountOTP.OTPType.Login).Skip(0).Take(1).AsQueryable();
            return qr.FirstOrDefault();
        }
    }
}
