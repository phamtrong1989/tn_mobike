﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class AccountRepository : EntityBaseRepository<Account>, IAccountRepository
    {
        private readonly MobikeContext _mobikeContext;
        public AccountRepository(MobikeContext context) : base(context)
        {
            _mobikeContext = context;
        }

        public async Task<Account> Login(string username, string password)
        {
            return await SearchOneAsync(x => (x.Email == username || x.Phone == username) && x.Password == password);
        }

        public async Task<Tuple<int, Account>> LoginWithOTP(string username, int otp,int otpInvalidMax)
        {
            var kt = await SearchOneAsync(x => (x.Email == username || x.Phone == username || x.Phone == username));
            if (kt == null) return new Tuple<int, Account>(0, null);
            var dateNow = DateTime.Now;
            var qr = _mobikeContext.AccountOTPs.OrderByDescending(m => m.Id).Where(m => m.AcountId == kt.Id && m.OTP == otp && m.Type == AccountOTP.OTPType.Login).Skip(0).Take(1).AsQueryable();
            var data = qr.FirstOrDefault();
            if (data == null)
            {
                // +1 cho OTP cuối cùng tài khoản này
                var otplast= _mobikeContext.AccountOTPs.OrderByDescending(m => m.Id).Where(m => m.AcountId == kt.Id && m.Type == AccountOTP.OTPType.Login && m.CreatedDate.Date==dateNow.Date).Skip(0).Take(1).AsQueryable();
                var dataOtplast = otplast.FirstOrDefault();
                if (dataOtplast != null)
                {
                    if(dataOtplast.InvalidCount <= otpInvalidMax)
                    {
                        dataOtplast.InvalidCount = (dataOtplast.InvalidCount ?? 0) + 1;
                        _mobikeContext.AccountOTPs.Update(dataOtplast);
                        await _mobikeContext.SaveChangesAsync();
                    }
                }
                // Sai OTP
                return new Tuple<int, Account>(2, null);
            }
            var IsOTP = (data.StartDate <= dateNow && dateNow <= data.EndDate && data.InvalidCount <= otpInvalidMax) ? true : false;
            if (!IsOTP)
            {
                // OTP hết hạn
                return new Tuple<int, Account>(3, null);
            }
            data.Status = true;
            data.ConfirmDate = dateNow;
            _mobikeContext.AccountOTPs.Update(data);
            await _mobikeContext.SaveChangesAsync();
            return new Tuple<int, Account>(1, kt);
        }

        public async Task<Account> FindByEtag(string etag)
        {
            return await SearchOneAsync(x => x.Etag == etag);
        }
        
        public virtual Account GetByUserName(string username)
        {
            var dateNow = DateTime.Now;
            var qr = _mobikeContext.Accounts.OrderByDescending(m => m.Id).Where(m => m.Phone == username).Skip(0).Take(1).AsQueryable();
            var data = qr.FirstOrDefault();
            if (data == null)
            {
                return null;
            }
            return data;
        }
    }
}
