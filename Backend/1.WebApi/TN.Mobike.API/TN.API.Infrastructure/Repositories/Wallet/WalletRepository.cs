﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class WalletRepository : EntityBaseRepository<Wallet>, IWalletRepository
    {
        public WalletRepository(MobikeContext context) : base(context)
        {
        }
        public async Task<int> UpdateOrInsert(int accountid, int amount, int type)
        {
            var info = await SearchOneAsync(x => x.AccountId == accountid);
            if (info == null)
            {
                info = new Wallet
                {
                    AccountId = accountid,
                    Balance = type == 1 ? amount : -amount,
                    Status = Wallet.EStatus.Active
                };
                await AddAsync(info);
                await Commit();
                Utility.Log.Info("Wallet_Register", $"{info.AccountId} - {true}");
            }
            else
            {
                if (type == 1)
                    info.Balance += amount;
                else info.Balance -= amount;
                Update(info);
                await Commit();
                Utility.Log.Info("Wallet_Update", $"{info.AccountId} - {true}");
            }
            return (int)info.Balance;
        }
        public async Task<int> GetBalance(int accountid)
        {
            try
            {
                var info = await SearchOneAsync(x => x.AccountId == accountid);
                if (info == null)
                {
                    info = new Wallet
                    {
                        AccountId = accountid,
                        Balance = 0,
                        Status = Wallet.EStatus.Active
                    };
                    await AddAsync(info);
                    await Commit();
                    Utility.Log.Info("Wallet_Register", $"{info.AccountId} - {true}");
                }
                return (int)info.Balance;
            }
            catch 
            {
                return -1;
            }
            
        }
    }
}


