﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;

namespace TN.API.Infrastructure.Repositories
{
    public class WalletTransactionRepository : EntityBaseRepository<WalletTransaction>, IWalletTransactionRepository
    {
        public WalletTransactionRepository(MobikeContext context) : base(context)
        {
        }
        public async Task<int> UpdateOrInsert(int amount, string note, int ticketid, WalletTransaction.EType typeid, int walletid)
        {
            var info = new WalletTransaction
            {
                Amount = amount,
                CreatedDate=DateTime.Now,
                Note=note,
                TicketId = ticketid,
                Type = typeid,
                WalletId = walletid,
                Status = WalletTransaction.EStatus.Done
            };
            await AddAsync(info);
            await Commit();
            Utility.Log.Info("WalletTransaction_Register", $"{info.WalletId} - {true}");
            return (int)info.Id;
        }
    }
}


