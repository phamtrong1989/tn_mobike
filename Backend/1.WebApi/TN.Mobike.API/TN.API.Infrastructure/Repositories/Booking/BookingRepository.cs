﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TN.Domain.Seedwork;
using TN.Domain.Model;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TN.API.Infrastructure.Repositories
{
    public class BookingRepository : EntityBaseRepository<Booking>, IBookingRepository
    {
        private readonly MobikeContext _mobikeContext;
        public BookingRepository(MobikeContext context) : base(context)
        {
            _mobikeContext = context;
        }
        public async Task<List<Booking>> GetListBooking(int accountid)
        {
            return await _context.Bookings.Where(x => x.AccountId ==accountid).ToListAsync();
        }
        
    }
}
