﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IWalletTransactionRepository : IEntityBaseRepository<WalletTransaction>
    {
        Task<int> UpdateOrInsert(int amount, string note, int ticketid, WalletTransaction.EType typeid, int walletid);
    }
}
