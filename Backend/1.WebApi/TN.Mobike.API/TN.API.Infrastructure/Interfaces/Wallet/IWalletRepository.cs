﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IWalletRepository : IEntityBaseRepository<Wallet>
    {
        Task<int> UpdateOrInsert(int accountid, int amount, int type);
        Task<int> GetBalance(int accountid);
    }
}
