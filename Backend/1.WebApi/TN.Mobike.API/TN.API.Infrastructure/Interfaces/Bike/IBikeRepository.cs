﻿using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IBikeRepository : IEntityBaseRepository<Bike>
    {
        Task<Bike> FindByEtag(string eTag);
        Task<Bike> FindById(int id);
    }
}
