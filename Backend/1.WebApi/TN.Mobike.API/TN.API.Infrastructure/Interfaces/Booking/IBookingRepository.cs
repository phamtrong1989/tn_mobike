﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IBookingRepository : IEntityBaseRepository<Booking>
    {
        Task<List<Booking>> GetListBooking(int accountid);
    }
}
