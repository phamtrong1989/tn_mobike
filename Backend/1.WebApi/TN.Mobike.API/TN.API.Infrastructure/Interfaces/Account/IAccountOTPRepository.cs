﻿using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IAccountOTPRepository : IEntityBaseRepository<AccountOTP>
    {
        AccountOTP CheckConfigOTP(string Phone, int OTP);
        AccountOTP CheckOTP(string Phone);
        AccountOTP GetLastOTPByUsername(string Phone);
    }
}
