﻿using System;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IAccountRepository : IEntityBaseRepository<Account>
    {
        Task<Account> Login(string username, string password);
        Task<Account> FindByEtag(string etag);
        Task<Tuple<int, Account>> LoginWithOTP(string username, int otp, int otpInvalidMax);
        Account GetByUserName(string username);
    }
}
