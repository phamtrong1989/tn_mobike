﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface ITicketRepository : IEntityBaseRepository<Ticket>
    {
        Task<List<Ticket>> FindByAccount(int accountid);
    }
}
