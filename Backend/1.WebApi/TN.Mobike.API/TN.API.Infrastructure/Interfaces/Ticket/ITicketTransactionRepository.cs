﻿using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface ITicketTransactionRepository : IEntityBaseRepository<TicketTransaction>
    {
        Task<TicketTransaction> FindByBike(int bikeid);
        Task<TicketTransaction> FindByTicket(int ticketId);
        bool UseStatus(int accountId);
        TicketTransaction LastTransaction(int accountId);

        Task<TicketTransaction> FindByAccountIdBooking(int AccountId);
    }
}
