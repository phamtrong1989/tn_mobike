﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface ITicketTransactionHistoryRepository : IEntityBaseRepository<TicketTransactionHistory>
    {
        TicketTransactionHistory GetByTransactionId(int accountId, int ticketTransactionId);
        Task<int> UpdateByTransactionHistoryAsync(int accountId, int ticketTransactionId, int rating, string comment);
        Task<object> TransactionHistoryGetByAccount(int accountId);
    }
}
