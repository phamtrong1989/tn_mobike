﻿using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IStationConfigRepository : IEntityBaseRepository<StationConfig>
    {
        Task<bool> CheckAvaiable(int stationConfigId);
    }
}
