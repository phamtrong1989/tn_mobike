﻿using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IStationControlRequestRepository : IEntityBaseRepository<StationControlRequest>
    {
        Task<StationControlRequest> GetByStationAndDock(int stationid, int dockid, StationControlRequest.EType command);
        Task<bool> AddControlRequest(StationControlRequest request);

    }
}
