﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TN.Domain.Interfaces;
using TN.Domain.Model;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IStationRepository : IEntityBaseRepository<Station>
    {
        Task<List<Station>> FindByLatLng(double min_lat, double max_lat, double min_lng, double max_lng);
        Task<Station> FindByImei(string imei);
        Task<bool> CheckAvaiable(int stationId);
    }
}
