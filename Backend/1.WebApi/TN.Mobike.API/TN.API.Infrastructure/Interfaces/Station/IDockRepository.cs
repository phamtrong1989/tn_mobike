﻿using System.Threading.Tasks;
using TN.Domain.Model;
using TN.Domain.Interfaces;

namespace TN.API.Infrastructure.Interfaces
{
    public interface IDockRepository : IEntityBaseRepository<Dock>
    {
        Task<Dock> FindByNumber(int stationid, int dock_number);
        Task<Dock> FindByImei(string imei);
    }
}
