﻿using System.Globalization;
using System.IO;
using System.Text;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using TN.API.HubListen;
using TN.API.Infrastructure;
using TN.API.Infrastructure.Interfaces;
using TN.API.Infrastructure.Repositories;
using TN.API.Middleware;
using TN.API.Model.Setting;
using TN.API.Services;

namespace TN.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            Configuration = configuration;

            Serilog.Log.Logger = new LoggerConfiguration()
                  .MinimumLevel.Debug()
                  .WriteTo.RollingFile(Path.Combine(env.ContentRootPath, "logs/log-{Date}.txt"))
                  .CreateLogger();
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Utility.Log.StartLog();

            services.AddDbContext<MobikeContext>(c =>
                {
                    c.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                },
                ServiceLifetime.Scoped
            );
            services.AddCors();

            services.AddMediatR(typeof(Startup));


            // Dependency Injection - Settings
            services.Configure<BaseSetting>(Configuration.GetSection("BaseSettings"));

            // Dependency Injection Config
            services.AddSingleton<IConfiguration>(Configuration);

            // Dependency Injection - Services
            services.AddScoped<IStationService, StationService>();
            services.AddScoped<ITicketTransactionService, TicketTransactionService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IStationSocketService, StationSocketService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<IBikeService, BikeService>();
            services.AddScoped<IFeedbackService, FeedbackService>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<IWalletTransactionService, WalletTransactionService>();

            // Dependency Injection - Repositories
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IStationRepository, StationRepository>();
            services.AddScoped<IDockRepository, DockRepository>();
            services.AddScoped<IBikeRepository, BikeRepository>();
            services.AddScoped<IAccountOTPRepository, AccountOTPRepository>();
            services.AddScoped<IBookingRepository,BookingRepository>();
            services.AddScoped<IWalletRepository, WalletRepository>();
            services.AddScoped<IWalletTransactionRepository, WalletTransactionRepository>();
            services.AddScoped<IFeedbackRepository, FeedbackRepository>();

            services.AddScoped<IStationControlRequestRepository, StationControlRequestRepository>();
            services.AddScoped<ITicketRepository, TicketRepository>();
            services.AddScoped<ITicketTransactionRepository, TicketTransactionRepository>();
            services.AddScoped<ITicketTransactionHistoryRepository, TicketTransactionHistoryRepository>();

            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<ISystemNotificationRepository, SystemNotificationRepository>();
            services.AddScoped<IPaymentService, PaymentService>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "mobike.tn",
                    ValidAudience = "mobike.tn",
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(Configuration["BaseSettings:SecurityKey"]))
                };
            });

            services.AddLocalization(o => o.ResourcesPath = "Resources");
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en-US"),
                    new CultureInfo("vi-VN"),
                };
                options.DefaultRequestCulture = new RequestCulture("en-US", "en-US");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSignalR();

            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "TN.Mobike - Catalog HTTP API",
                    Version = "v1",
                    Description = "The Catalog Microservice HTTP API",
                    TermsOfService = "Terms Of Service"
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 31536000;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = "public,max-age=" + durationInSeconds;
                }
            });

            //if(!Directory.Exists(Configuration["BaseSettings:FolderMobikeAvatarServer"]))
            //{
            //    Directory.CreateDirectory(Configuration["BaseSettings:FolderMobikeAvatarServer"]);
            //}

            //app.UseFileServer(new FileServerOptions
            //{
            //    FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider($"{Configuration["BaseSettings:FolderMobikeAvatarServer"]}"),
            //    RequestPath = new Microsoft.AspNetCore.Http.PathString(Configuration["BaseSettings:FolderMobikeAvatarWeb"]),
            //    EnableDirectoryBrowsing = true
            //});

            // Seed data through our custom class
            DataContextSeed.SeedAsync(app).Wait();

            app.UseAuthentication();

            app.UseMiddleware<LogRequestMiddleware>();
            app.UseMiddleware<LogResponseMiddleware>();

            app.UseMvc();
            app.UseSignalR(routes =>
            {
                routes.MapHub<DeviceHub>("/DeviceHub");
            });
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mobike.API V1");
                });
        }
    }
}
