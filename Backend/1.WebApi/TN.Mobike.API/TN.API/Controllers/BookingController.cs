﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Controllers
{
    [Authorize]
    [AuthorizeXDOL]
    [Route("api/[controller]")]
    public class BookingController : Controller<Booking, IBookingService>
    {
        public BookingController(IBookingService service)
           : base(service)
        {
        }
        [HttpPost("register")]
        public async Task<object> RegisterAsync([FromBody]BookingRegisterCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.Register(request);
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("BookingController_Register", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [HttpGet("profile/{id}")]
        public async Task<object> Profile(int id)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.Profile(id);
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("BookingController_Profile", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [HttpPost("update-booking/{id}")]
        public async Task<object> UpdateStatusIdAsync([FromBody]BookingUpdateCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.UpdateBooking(request);
                if (result == 0)
                    return ApiResponse.WithStatus(false, ApiResponse.Code.BookingNotExist);
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("BoookingController_UpdateStatusId", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [HttpPost("update-booking-vote/{id}")]
        public async Task<object> UpdateVote([FromBody]BookingUpdateVoteCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.UpdateVote(request);
                if (result == 0)
                    return ApiResponse.WithStatus(false, ApiResponse.Code.BookingNotExist);
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("BoookingController_UpdateVote", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
    }
}
