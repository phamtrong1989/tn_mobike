﻿using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PaymentController : Controller<TicketTransaction, IPaymentService>
    {
        public PaymentController(IPaymentService service)
          : base(service)
        {
        }

        [HttpGet("deposit-done")]
        [AllowAnonymous]
        public async Task<IActionResult> DepositDone(
            int accountid,
            int amount,
            string message,
            string payment_type,
            string reference_number,
            int status,
            int trans_ref_no,
            int website_id,
            string signature
        )
        {
            if (status > 0)
            {
                await Service.DepositDone(accountid, amount, message, payment_type, reference_number, status, trans_ref_no, website_id, signature, 1);
                return View("_Success");
            }
            else
            {
                await Service.DepositDone(accountid, amount, message, payment_type, reference_number, status, trans_ref_no, website_id, signature, 1, false);
                return View("_Failure");
            }
        }

        //[AllowAnonymous]
        [AuthorizeXDOL]
        [HttpPost("deposit")]
        public async Task<object> Deposit([FromBody]PaymentDepositCommand model)
        {
            try
            {
                var data = await Service.Deposit(model);
                if (data != null)
                {
                    return ApiResponse.WithData(data.Data.UrlTemp);
                }
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("PaymentController_DepositAsync", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }

        //[AllowAnonymous]
        [AuthorizeXDOL]
        [HttpPost("order")]
        public async Task<object> Order([FromBody]PaymentOrderCommand model)
        {
            try
            {
                return ApiResponse.WithData(await Service.Order(model));
            }
            catch (Exception ex)
            {
                Utility.Log.Error("PaymentController_Order", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }

        //[AllowAnonymous]
        [AuthorizeXDOL]
        [HttpPost("balance")]
        public async Task<object> Balance([FromBody]PaymentQueryBalanceCommand model)
        {
            try
            {
                var data = await Service.GetBalance(model.AccountId);
                if (data >= -9999999)
                {
                    return ApiResponse.WithData(data);
                }
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
                //var data = await Service.Balance(model);
                //if (data != null)
                //{
                //    return ApiResponse.WithData(data.Data);
                //}
                //return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("PaymentController_DepositAsync", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }

        //[AllowAnonymous]
        [AuthorizeXDOL]
        [HttpPost("history")]
        public async Task<object> History([FromBody]PaymentHistoryCommand model, int start = 0, int size = 10)
        {
            try
            {
                var data = await Service.History(model);
                if (data != null)
                {
                    return ApiResponse.WithData(data);
                }
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("PaymentController_HistoryAsync", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
    }
}