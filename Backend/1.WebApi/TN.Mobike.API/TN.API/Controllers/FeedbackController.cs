﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Controllers
{
    [Authorize]
    [AuthorizeXDOL]
    [Route("api/[controller]")]
    public class FeedbackController : Controller<Feedback, IFeedbackService>
    {
        public FeedbackController(IFeedbackService service)
           : base(service)
        {
        }
        [HttpPost("register")]
        public async Task<object> RegisterAsync([FromBody]FeedbackRegisterCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.Register(request);
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("FeedbackController_Register", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [HttpGet("profile/{id}")]
        public async Task<object> Profile(int id)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.Profile(id);
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("FeedbackController_Profile", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
    }
}
