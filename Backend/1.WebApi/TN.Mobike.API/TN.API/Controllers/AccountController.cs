﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Controllers
{
    [Authorize]   
    [Route("api/[controller]")]
    public class AccountController : Controller<Account, IAccountService>
    {
        public AccountController(IAccountService service)
           : base(service)
        {
        }

        [AllowAnonymous]
        [HttpPost("otp-register")]
        public async Task<object> OTPRegisterAsync([FromBody]AccountOTPRegisterCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.OTPRegister(request);
                if (data == 2)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.PhoneNumberExist);
                }
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Register", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError,  ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpPost("confirm-otp-register")]
        public async Task<object> ConfirmOTPRegisterAsync([FromBody]AccountConfirmOTPRegisterCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.ConfirmOTPRegister(request);
                if (data == 2)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.PhoneNumberExist);
                }
                else if (data==3)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.OTPOutTime);
                }
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Register", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<object> RegisterAsync([FromBody]AccountRegisterCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.Register(request);
                if (data.Item1 == 2)
                {
                     return ApiResponse.WithStatus(false, ApiResponse.Code.PhoneNumberExist);
                }
                else if (data.Item1 == 3)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.OTPOutTime);
                }
                if (data.Item1 == 4)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.EmailExist);
                }
                else if(data.Item2 != null && data.Item1 == 1)
                {
                    return ApiResponse.WithData(data.Item2);
                }
                else
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
                }
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Register", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpPost("update-profile")]
        public async Task<object> UpdateProfileAsync([FromBody]AccountUpdateProfileCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.UpdateProfile(request);
                if (result == 0)
                    return ApiResponse.WithStatus(false, ApiResponse.Code.UsernameNotExist);
                else if (result == 2)
                    return ApiResponse.WithStatus(false, ApiResponse.Code.EmailExist);
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Profile", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AuthorizeXDOL]
        [HttpPost("upload-avatar")]
        public async Task<object> UploadAvatarAsync([FromBody]AccountUploadAvatarCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.UploadAvatar(request);
                if (!result)
                    return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
                return ApiResponse.WithStatus(result);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Profile", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AllowAnonymous]
        [HttpPost("request-token/{id}")]
        public object RequestToken(string id)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var token = Service.RequestToken(id);

                if (!string.IsNullOrEmpty(token))
                {
                    return ApiResponse.WithData(token);
                }
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidLogin);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Login", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpPost("create-otp-login")]
        public async Task<object> CreateOTPLogin([FromBody]AccountCreateOTPLoginCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data =await Service.CreateOTPLogin(request);
                if(data==2)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.UsernameNotExist);
                }
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Login", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError,  ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpPost("login-with-otp")]
        public async Task<object> LoginWithOTP([FromBody]AccountLoginWithOTPCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.LoginWithOTP(request);
                if(data.Item1 ==0)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.UsernameNotExist);
                }
                else if (data.Item1 == 2)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.OTPInvalid);
                }
                else if (data.Item1 == 3)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.OTPOutTime);
                }
                else
                {
                    if (data.Item2 != null)
                    {
                        data.Item2.Avatar = $"{(HttpContext.Request.IsHttps ? "https://" : "http://")}{HttpContext.Request.Host}{  data.Item2.Avatar}";
                        return ApiResponse.WithData(data.Item2);
                    }
                }
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidLogin);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_LoginWithOTP", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [HttpPost("logout")]
        public async Task<object> Logout([FromBody]AccountLogoutCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.Logout(request);
                if (data>0)
                {
                    return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
                }
                return ApiResponse.WithStatus(false, ApiResponse.Code.UsernameNotExist);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Login", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AuthorizeXDOL]
        [HttpGet("profile/{id}")]
        public async Task<object> Profile(int id)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.Profile(id);
                if (data != null)
                {
                    data.Avatar = $"{(HttpContext.Request.IsHttps ? "https://" : "http://")}{HttpContext.Request.Host}{ data.Avatar}";
                    return ApiResponse.WithData(data);
                }
                return ApiResponse.WithStatus(false, ApiResponse.Code.UsernameNotExist);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Profile", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<object> Login([FromBody]LoginCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
               
                var account = await Service.Login(request.Username, request.Password);

                if (account != null)
                {
                    account.Avatar = $"{(HttpContext.Request.IsHttps? "https://" : "http://")}{HttpContext.Request.Host}{ account.Avatar}";
                    return ApiResponse.WithData(account);
                }
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidLogin);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Login", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AuthorizeXDOL]
        [HttpPost("mobile/{id:int}")]
        public async Task<object> UpdateMobileInfo(int id, [FromBody]UpdateMobileInfoCommand command)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);

            try
            {
                var result = await Service.UpdateAccountMobileInfo(id, command.Token);
                return ApiResponse.WithStatus(result);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_UpdateMobileInfo", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AuthorizeXDOL]
        [HttpPost("mobile/{id:int}/test")]
        public async Task<object> TestMobileNotification(int id)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.TestAccountMobileNotification(id);
                return ApiResponse.WithStatus(result);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_TestMobileNotification", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AuthorizeXDOL]
        [HttpPost("update-poin")]
        public async Task<object> UpdatePoin([FromBody]AccountUpdatePointCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.UpdatePoin(request);                
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Profile", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AllowAnonymous]
        [HttpGet("checkemail/{email}")]
        public async Task<object> CheckEmail(string email)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = await Service.CheckEmail(email);
                if (data == 1)
                    return ApiResponse.WithStatus(false, ApiResponse.Code.EmailExist);
                else
                    return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
                //return ApiResponse.WithStatus(false, ApiResponse.Code.EmailNotExist);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("AccountController_Profile", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
    }
}
