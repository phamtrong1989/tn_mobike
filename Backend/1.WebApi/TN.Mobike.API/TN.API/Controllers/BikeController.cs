﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;
using TN.Domain.Seedwork;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BikeController : Controller<Bike, IBikeService>
    {
        public BikeController(IBikeService service)
           : base(service)
        {
        }
        [AuthorizeXDOL]
        [HttpPost("update-status/{id}")]
        public async Task<object> UpdateStatusIdAsync([FromBody]BikeUpdateRentIdCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.UpdateRentId(request);
                if (result == 0)
                    return ApiResponse.WithStatus(false, ApiResponse.Code.BikeNotExist);
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("BikeController_UpdateRentId", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AllowAnonymous]
        [HttpGet("getappsetting/{key}")]
        public async Task<object> getAppSetting(string key)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var data = Service.getAppSetting(key);
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("BikeController_GetAppSetting", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AuthorizeXDOL]
        [HttpPost("dock-update-status/{id}")]
        public async Task<object> UpdateDockStatus([FromBody]DockErrorCommand request)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.UpdateDockStatus(request);
                if (result == 0)
                    return ApiResponse.WithStatus(false, ApiResponse.Code.DockNotExist);
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("BikeController_UpdateStatusId", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
        [AuthorizeXDOL]
        [HttpPost("dock-error/{key}")]
        public async Task<object> UpdateDockError(string key)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);
            try
            {
                var result = await Service.UpdateDockError(key);
                if (result == 0)
                    return ApiResponse.WithStatus(false, ApiResponse.Code.DockNotExist);
                return ApiResponse.WithStatus(true, ApiResponse.Code.Ok);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("BikeController_UpdateError", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
    }
}
