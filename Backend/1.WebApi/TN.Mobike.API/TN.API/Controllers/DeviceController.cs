﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TN.API.HubListen;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;

namespace TN.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly IHubContext<DeviceHub> _hubContext;
        private readonly IDockRepository _dockRepository;
        public DeviceController(IHubContext<DeviceHub> hubContext,
            IDockRepository dockRepository
            )
        {
            _hubContext = hubContext;
            _dockRepository = dockRepository;
        }

        [HttpPost("{id}")]
        public async Task<object> Post(string id)
        {
           var dl = await _dockRepository.FindByImei(id);
            if (dl != null)
            {
                await _hubContext.Clients.All.SendAsync("DeviceReceiveMessage", Newtonsoft.Json.JsonConvert.SerializeObject(dl));
            }
            return new { Result = dl };
        }
       
    }
}
