﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Utility;

namespace TN.API.Controllers
{
    [Authorize]
    [AuthorizeXDOL]
    [Route("api/[controller]")]
    public class StationController : Controller<Station, IStationService>
    {
        public StationController(IStationService service)
           : base(service)
        {
        }

        [HttpGet("{lat_lng}")]
        public async Task<object> Get(string lat_lng)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);

            try
            {
                var result = await Service.FindByLatLng(lat_lng);
                return ApiResponse.WithData(result);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("StationController_Get", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }

        [HttpPost("status/{imei}")]
        public async Task<object> PostStationStatus(string imei, [FromBody]UpdateStationStatusCommand command)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);

            try
            {
                var result = await Service.UpdateStationStatus(imei, command);
                return ApiResponse.WithStatus(result);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("StationController_PostStationStatus", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError, ex.ToString());
            }
        }
    }
}
