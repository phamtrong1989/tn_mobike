﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActionFilters.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TN.API.Model;
using TN.API.Services;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Utility;

namespace TN.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TransactionController : Controller<TicketTransaction, ITicketTransactionService>
    {
        public TransactionController(ITicketTransactionService service)
           : base(service)
        {
        }
        [AuthorizeXDOL]
        [HttpPost("{account_id:int}/control-dock")]
        public async Task<object> ControlDock(int account_id, [FromBody]TransactionControlCommand command)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);

            try
            {
                var result = await Service.ControlDock_New(account_id, command);

                if (result.Item1 > 0) // request id
                    return ApiResponse.WithData(new StationControlRequestDTO(result.Item1, result.Item3, result.Item4, result.Item5,result.Item6,result.Item7));
                else
                    return ApiResponse.WithStatus(false, result.Item2);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_ControlDock", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpGet("check-rented/{account_id:int}")]
        public async Task<object> CheckRented(int account_id)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);

            try
            {
                var result = await Service.Check_Rented(account_id);

                if (result.Item1 > 0) // request id
                    return ApiResponse.WithData(new RentedRequestDTO(result.Item1, result.Item3, result.Item4));
                else
                    return ApiResponse.WithStatus(false, result.Item2);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_ControlDock", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpPost("control-dock-manual/{imei}/{dock_number:int}/{etag}/{command:int}")]
        public async Task<object> ControlDockManual(string imei, int dock_number, string etag, int command)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);

            try
            {
                var result = await Service.ControlDockManual(imei, dock_number, etag, command);

                if (result != null && result.Item1)
                    return ApiResponse.WithStatus(result.Item1);
                else
                    return ApiResponse.WithStatus(false, result.Item2);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_ControlDockManual", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpPost("control-dock-confirm/{imei}/{dock_number:int}/{command:int}/{status:int}")]
        public async Task<object> ControlDockConfirm(string imei, int dock_number, int command, int status)
        {
            if (!ModelState.IsValid)
                return ApiResponse.WithStatus(false, ApiResponse.Code.InvalidData);

            try
            {
                var result = await Service.ControlDockConfirm(imei, dock_number, command, status);
                if (result != null && result.Item1)
                    return ApiResponse.WithStatus(result.Item1);
                else
                    return ApiResponse.WithStatus(false, result.Item2);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_ControlDockConfirm", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpPost("use-status")]
        public object UseStatus([FromBody]AccountUseStatusCommand model)
        {
            try
            {
                var data = Service.UseStatus(model.AccountId);
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_UseStatus", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpPost("get-transaction-history")]
        public object GetTransactionHistory([FromBody]GetTransactionHistoryCommand model)
        {
            try
            {
                var data = Service.GetTransactionHistory(model);
                if (data == null)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.TransactionInvalid);
                }
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_TransactionHistory", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpPost("update-transaction-history")]
        public async Task<object> UpdateTransactionHistoryAsync([FromBody]UpdateTransactionHistoryCommand model)
        {
            try
            {
                var data = await Service.UpdateByTransactionHistoryAsync(model);
                if (data == 0)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.TransactionInvalid);
                }
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_UpdateTransactionHistoryAsync", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpPost("list-transaction-history")]
        public async Task<object> ListTransactionHistoryAsync([FromBody]TransactionHistoryGetByAccountCommand model)
        {
            try
            {
                var data = await Service.TransactionHistoryGetByAccount(model);

                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_ListTransactionHistoryAsync", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
        [AuthorizeXDOL]
        [HttpPost("last-transaction")]
        public object LastTransaction([FromBody]AccountLastTransactionCommand model)
        {
            try
            {
                var data = Service.LastTransaction(model);
                if (data == null)
                {
                    return ApiResponse.WithStatus(false, ApiResponse.Code.TransactionInvalid);
                }
                return ApiResponse.WithData(data);
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TransactionController_LastTransaction", ex.ToString());
                return ApiResponse.WithStatus(false, ApiResponse.Code.ServerError);
            }
        }
    }
}
