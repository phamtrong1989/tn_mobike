﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.API.Infrastructure;
using TN.Domain.Model;

namespace TN.API
{
    public class DataContextSeed
    {
        public static async Task SeedAsync(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<MobikeContext>();
                using (context)
                {
                    context.Database.Migrate();
                    if (!context.Accounts.Any())
                    {
                        context.Accounts.AddRange(GetPreconfiguredAccounts());
                        await context.SaveChangesAsync();
                    }
                    if (!context.Tickets.Any())
                    {
                        context.Tickets.AddRange(GetPreconfiguredTickets());
                        await context.SaveChangesAsync();
                    }
                    if (!context.StationConfigs.Any())
                    {
                        context.StationConfigs.AddRange(GetPreconfiguredStationConfigs());
                        await context.SaveChangesAsync();
                    }
                    if (!context.Stations.Any())
                    {
                        context.Stations.AddRange(GetPreconfiguredStations());
                        await context.SaveChangesAsync();
                    }
                    if (!context.Bikes.Any())
                    {
                        context.Bikes.AddRange(GetPreconfiguredBikes());
                        await context.SaveChangesAsync();
                    }
                    if (!context.Docks.Any())
                    {
                        context.Docks.AddRange(GetPreconfiguredDocks());
                        await context.SaveChangesAsync();
                    }
                }
            }

           
        }
        static IEnumerable<Account> GetPreconfiguredAccounts()
        {
            return new List<Account>()
            {
                new Account() {
                    //Id = 1,
                    FirstName = "Duy",
                    LastName = "Pham",
                    Phone = "0907983995",
                    Email = "phamhaduy1203@gmail.com",
                    MobileToken = "123456"
                },
            };
        }
        static IEnumerable<Ticket> GetPreconfiguredTickets()
        {
            return new List<Ticket>()
            {
                new Ticket() {
                    //Id = 1,
                    AccountId = 1
                },
            };
        }
        static IEnumerable<StationConfig> GetPreconfiguredStationConfigs()
        {
            return new List<StationConfig>()
            {
                new StationConfig() {
                    //Id = 1,
                    IsDefault = true,
                    ServerIP = "14.225.3.30",
                    ServerPort = 451
                },
            };
        }
        static IEnumerable<Station> GetPreconfiguredStations()
        {
            return new List<Station>()
            {
                new Station() {
                    //Id = 1,
                    Name = "Tram Tri Nam",
                    Address = "96 Nguyen Khanh Toan, Cau Giay, Ha Noi",
                    IMEI = "123456",
                    Lat = 21.027764,
                    Lng = 105.834160,
                    StationConfigId = 1,
                },
            };
        }
        static IEnumerable<Bike> GetPreconfiguredBikes()
        {
            return new List<Bike>()
            {
                new Bike() {
                    //Id = 1,
                    Etag = "123456",
                    IMEI = "123456",
                },
            };
        }
        static IEnumerable<Dock> GetPreconfiguredDocks()
        {
            return new List<Dock>()
            {
                new Dock() {
                    //Id = 1,
                    StationId = 1,
                    CurrentBikeId = 1,
                    LockStatus = false,
                    OrderNumber = 1,
                    IMEI = "123456",
                },
            };
        }

    }
}
