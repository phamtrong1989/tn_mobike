﻿using MediatR;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Utility;

namespace TN.API.Services
{
    public enum ETransactionErrorCode
    {

    }

    public interface ITicketTransactionService : IService<TicketTransaction>
    {
        Task<Tuple<int, ApiResponse.Code>> ControlDock(int userid, TransactionControlCommand command);
        Task<Tuple<int, ApiResponse.Code, int, int, int, int, Account.EAccountType>> ControlDock_New(int userid, TransactionControlCommand command);
        Task<Tuple<bool, ApiResponse.Code>> ControlDockManual(string imei, int dock_number, string etag, int command);
        Task<Tuple<bool, ApiResponse.Code>> ControlDockConfirm(string imei, int dock_number, int command, int status);
        bool UseStatus(int accountId);
        TicketTransactionHistory GetTransactionHistory(GetTransactionHistoryCommand model);
        Task<int> UpdateByTransactionHistoryAsync(UpdateTransactionHistoryCommand model);
        Task<object> TransactionHistoryGetByAccount(TransactionHistoryGetByAccountCommand model);
        TicketTransaction LastTransaction(AccountLastTransactionCommand model);
        Task<Tuple<int, ApiResponse.Code, int, int>> Check_Rented(int userid);
    }

    public class TicketTransactionService : ITicketTransactionService
    {
        private readonly ITicketTransactionRepository _repository;
        private readonly ITicketTransactionHistoryRepository _iTicketTransactionHistoryRepository;
        private readonly IBookingRepository _iBookingRepository;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;

        private readonly IStationRepository _stationRepository;
        private readonly IDockRepository _dockRepository;
        private readonly IBikeRepository _bikeRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly ITicketTransactionRepository _ticketTransactionRepository;
        private readonly IStationControlRequestRepository _stationControlRequestRepository;

        private readonly IAccountRepository _accountRepository;
        private readonly IStationSocketService _stationSocketService;
        private readonly INotificationService _notificationService;
        private readonly IPaymentService _paymentService;
        private readonly IBikeService _bikeService;

        private readonly IBookingService _bookingService;
        private readonly IAccountService _accountService;
        private readonly IMediator _mediator;
        private readonly IStringLocalizer<string> _localizer;
        private readonly BaseSetting _baseSetting;

        public TicketTransactionService(ITicketTransactionRepository repository,
                                        IStationRepository stationRepository,
                                        IDockRepository dockRepository,
                                        IBikeRepository bikeRepository,
                                        ITicketRepository ticketRepository,
                                        ITicketTransactionRepository ticketTransactionRepository,
                                        IStationSocketService stationSocketService,
                                        IStationControlRequestRepository stationControlRequestRepository,
                                        IAccountRepository accountRepository,
                                        INotificationService notificationService,
                                        IPaymentService paymentService,
                                        IBikeService biketService,
                                        IBookingService bookingService,
                                        IAccountService accountService,
                                        IStringLocalizer<string> localizer,
                                        IMediator mediator,
                                        ITicketTransactionHistoryRepository iTicketTransactionHistoryRepository,
                                        IBookingRepository iBookingRepository,
                                        IOptions<BaseSetting> baseSetting,
                                        IWalletRepository iWalletRepository,
                                        IWalletTransactionRepository iWalletTransactionRepository
            )
        {
            _repository = repository;
            _stationRepository = stationRepository;
            _dockRepository = dockRepository;
            _bikeRepository = bikeRepository;
            _stationControlRequestRepository = stationControlRequestRepository;
            _accountRepository = accountRepository;
            _ticketRepository = ticketRepository;
            _ticketTransactionRepository = ticketTransactionRepository;
            _stationSocketService = stationSocketService;
            _notificationService = notificationService;
            _paymentService = paymentService;
            _bookingService = bookingService;
            _accountService = accountService;
            _bikeService = biketService;
            _localizer = localizer;
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _iTicketTransactionHistoryRepository = iTicketTransactionHistoryRepository;
            _iBookingRepository = iBookingRepository;
            _baseSetting = baseSetting.Value;
            _iWalletRepository = iWalletRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
        }



        public virtual async Task<Tuple<int, ApiResponse.Code, int, int, int, int, Account.EAccountType>> ControlDock_New(int accountid, TransactionControlCommand command)
        {
            int requestId = 0;
            int transactionId = 0;
            int money = 0;
            int point = 0;
            int acc_point = 0;
            Account.EAccountType acc_typeid = 0;
            ApiResponse.Code errorCode = ApiResponse.Code.Ok;
            //1. Kiểm tra xem tài khoản còn tiền
            PaymentQueryBalanceCommand model = new PaymentQueryBalanceCommand() { AccountId = accountid };
            //var balance = await _paymentService.Balance(model);
            var balance = await _paymentService.GetBalance(accountid);
            //if (balance >0)
            //{
            if (balance > 0)
            {
                //2. Kiểm tra booking
                var booking = await _bookingService.Profile(command.BookingId);
                if (booking != null)
                {

                    //3. lấy thông tin khóa
                    var dock = await _dockRepository.FindByImei(command.DockImei);
                    //nếu khóa đang active
                    if (dock != null && dock.Status == Dock.EStatus.Active)
                    {
                        //lấy thông tin trạm
                        var station = await _stationRepository.GetOne(dock.StationId);

                        if (station != null)
                        {
                            //đóng khóa
                            if (command.Command == StationControlRequest.EType.Unlock)
                            {
                                if (booking.StatusId == Booking.BookingStatus.Booking)
                                {
                                    if (dock.CurrentBikeId > 0)
                                    {
                                        //Lấy thông tin xe
                                        var bike = await _bikeRepository.FindById((int)dock.CurrentBikeId);
                                        if (bike != null)
                                        {
                                            if (bike.Status == Bike.EStatus.Active)
                                            {
                                                if (bike.RentId == Bike.ERent.ChuaThue)
                                                {
                                                    if (command.TicketId == 0)
                                                    {
                                                        // save ticket
                                                        var ticket = new Ticket(accountid);
                                                        ticket.BookingId = booking.Id;
                                                        await _ticketRepository.AddAsync(ticket);
                                                        await _ticketRepository.Commit();

                                                        command.TicketId = ticket.Id;

                                                        var TicketTransaction = new TicketTransaction(accountid, ticket.Id, station.Id, dock.Id, (int)dock.CurrentBikeId, TN.Domain.Model.TicketTransaction.EStatus.Running);
                                                        TicketTransaction.BookingId = booking.Id;
                                                        await _ticketTransactionRepository.AddAsync(TicketTransaction);
                                                        await _ticketTransactionRepository.Commit();
                                                        transactionId = TicketTransaction.Id;
                                                    }

                                                    // save request
                                                    var request = StationControlRequest.CreateUnlockRequest(dock.StationId, dock.Id, accountid, command.TicketId);
                                                    await _stationControlRequestRepository.AddAsync(request);
                                                    await _stationControlRequestRepository.Commit();

                                                    ////call unlock device
                                                    var device = await _stationSocketService.CallUnlockDevice(dock.SerialNumber);
                                                    if (device != null)
                                                    {
                                                        //update trạng thái xe là đang thuê
                                                        var result = await _bikeService.UpdateRentId(new BikeUpdateRentIdCommand() { Id = (int)dock.CurrentBikeId, RentId = Bike.ERent.DangThue });
                                                        if (result == 1)
                                                        {
                                                            requestId = request.Id;
                                                            //update trạng thái booking là đang đi
                                                            await _bookingService.UpdateBooking(new BookingUpdateCommand() { Id = booking.Id, StatusId = Booking.BookingStatus.Start });
                                                        }
                                                        else
                                                        {
                                                            transactionId = 0;
                                                            errorCode = ApiResponse.Code.BikeNotExist;
                                                        }
                                                        //update trạng thái xe là đang thuê
                                                    }
                                                    else
                                                    {
                                                        errorCode = ApiResponse.Code.DeviceNotExist;
                                                    }
                                                }
                                                else
                                                {
                                                    errorCode = ApiResponse.Code.BikeIsRented;
                                                }
                                            }
                                            else
                                            {
                                                errorCode = ApiResponse.Code.BikeIsError;
                                            }

                                        }
                                        else
                                        {
                                            errorCode = ApiResponse.Code.BikeNotExist;
                                        }

                                    }
                                    else
                                    {
                                        errorCode = ApiResponse.Code.DockHasNoBike;
                                    }
                                }
                                else
                                {
                                    if (booking.StatusId == Booking.BookingStatus.End)
                                        errorCode = ApiResponse.Code.BookingEnd;

                                    if (booking.StatusId == Booking.BookingStatus.Start)
                                        errorCode = ApiResponse.Code.BookingStart;
                                }
                            }
                            else if (command.Command == StationControlRequest.EType.Lock)
                            {
                                if (booking.StatusId == Booking.BookingStatus.Start)
                                {
                                    //if (dock.CurrentBikeId <= 0)
                                    if (dock.CurrentBikeId >= 0)
                                    {
                                        if (command.TransactionId > 0)
                                        {
                                            var transaction = await _repository.GetOne(command.TransactionId);
                                            if (transaction != null)
                                            {
                                                if (transaction.AccountId == accountid)
                                                {
                                                    // save request
                                                    var request = StationControlRequest.CreateLockRequest(dock.StationId, dock.Id, accountid, command.TransactionId);
                                                    await _stationControlRequestRepository.AddAsync(request);
                                                    await _stationControlRequestRepository.Commit();
                                                    //update trạng thái xe là chưa thuê
                                                    var result = await _bikeService.UpdateRentId(new BikeUpdateRentIdCommand() { Id = (int)dock.CurrentBikeId, RentId = Bike.ERent.ChuaThue });
                                                    if (result == 1)
                                                    {
                                                        requestId = request.Id;

                                                        //todo: trừ tiền trong tài khoản
                                                        money = 50000;

                                                        var walletid = await _iWalletRepository.UpdateOrInsert(accountid, money, 2);
                                                        string note = string.Format("accountid={0} <br/>TransactionId={0} <br/>BookingId={1}",
                                                            accountid, command.TransactionId, command.BookingId);
                                                        await _iWalletTransactionRepository.UpdateOrInsert(money, note, command.TicketId, WalletTransaction.EType.Paid, walletid);

                                                        //cộng điểm vào tài khoản
                                                        var account = await _accountService.UpdatePoin(new AccountUpdatePointCommand() { Distance = 1000, Id = accountid });
                                                        point = account.Item2;
                                                        acc_point = account.Item1.Points;
                                                        acc_typeid = account.Item1.Type;
                                                        //update trạng thái booking là kết thúc
                                                        await _bookingService.UpdateBooking(new BookingUpdateCommand() { Id = booking.Id, StatusId = Booking.BookingStatus.End });

                                                    }
                                                    else
                                                    {
                                                        errorCode = ApiResponse.Code.BikeNotExist;
                                                    }

                                                }
                                                else
                                                {
                                                    errorCode = ApiResponse.Code.InvalidAccount;
                                                }
                                            }
                                            else
                                            {
                                                errorCode = ApiResponse.Code.TransactionNotExist;
                                            }
                                        }
                                        else
                                        {
                                            errorCode = ApiResponse.Code.TransactionInvalid;
                                        }
                                    }
                                    else
                                    {
                                        errorCode = ApiResponse.Code.DockAlreadyHasBike;
                                    }
                                }
                                else
                                {
                                    if (booking.StatusId == Booking.BookingStatus.End)
                                        errorCode = ApiResponse.Code.BookingEnd;

                                    if (booking.StatusId == Booking.BookingStatus.Booking)
                                        errorCode = ApiResponse.Code.BookingIsNotRented;
                                }
                            }
                        }
                        else
                        {
                            errorCode = ApiResponse.Code.StationNotExist;
                        }
                    }
                    else
                    {
                        if (dock == null)
                            errorCode = ApiResponse.Code.DockNotExist;
                        else
                        {
                            if (dock.Status == Dock.EStatus.Inactive)
                                errorCode = ApiResponse.Code.DockNotActive;

                            if (dock.Status == Dock.EStatus.Error)
                                errorCode = ApiResponse.Code.DockError;
                        }
                    }

                }
                else
                {
                    errorCode = ApiResponse.Code.BookingNotExist;
                }
            }
            else
            {
                errorCode = ApiResponse.Code.BalanceNotMoney;
            }
            //}
            //else
            //{
            //    //chưa có thông tin tài khoản nạp tiền
            //    errorCode = ApiResponse.Code.BalanceNotExist;
            //}
            return new Tuple<int, ApiResponse.Code, int, int, int, int, Account.EAccountType>(requestId, errorCode, transactionId, money, point, acc_point, acc_typeid);
        }
        public virtual async Task<Tuple<int, ApiResponse.Code, int, int>> Check_Rented(int accountid)
        {
            int bookingid = 0;
            int transactionId = 0;
            int ticketid = 0;
            ApiResponse.Code errorCode = ApiResponse.Code.Ok;
            //2. Kiểm tra booking
            var booking = await _bookingService.GetBookingByAccountId(accountid);
            if (booking != null)
            {
                bookingid = booking.Id;
                var transaction = await _ticketTransactionRepository.FindByAccountIdBooking(accountid);
                if(transaction != null)
                {
                    ticketid = transaction.TicketId;
                    transactionId = transaction.Id;
                }
            }
            return new Tuple<int, ApiResponse.Code, int, int>(bookingid, errorCode, transactionId, ticketid);
        }
        public virtual async Task<Tuple<int, ApiResponse.Code>> ControlDock(int accountid, TransactionControlCommand command)
        {
            int requestId = 0;
            ApiResponse.Code errorCode = ApiResponse.Code.Ok;
            var dock = await _dockRepository.FindByImei(command.DockImei);

            if (dock != null && dock.Status == Dock.EStatus.Active)
            {
                var station = await _stationRepository.GetOne(dock.StationId);

                if (station != null)
                {
                    if (command.Command == StationControlRequest.EType.Unlock)
                    {
                        if (dock.CurrentBikeId > 0)
                        {
                            if (command.TicketId == 0)
                            {
                                // save ticket
                                var ticket = new Ticket(accountid);
                                await _ticketRepository.AddAsync(ticket);
                                await _ticketRepository.Commit();

                                command.TicketId = ticket.Id;
                            }

                            // save request
                            var request = StationControlRequest.CreateUnlockRequest(dock.StationId, dock.Id, accountid, command.TicketId);
                            await _stationControlRequestRepository.AddAsync(request);
                            await _stationControlRequestRepository.Commit();

                            // send control request to Station Socket Service
                            bool result = await _stationSocketService.CallStationApi(station.IMEI, dock.OrderNumber, (int)command.Command);

                            if (result)
                            {
                                requestId = request.Id;
                                Utility.Log.Info("TicketTransactionService_ControlDock", $"AccountId: {accountid} control Command: {JsonConvert.SerializeObject(command)} - Open RequestId: {requestId} - Success: TRUE");
                            }
                            else
                            {
                                errorCode = ApiResponse.Code.DockControlFailed;
                                // delete request 
                                _stationControlRequestRepository.Delete(request);
                                await _stationControlRequestRepository.Commit();

                                // Send failed notification to Notification Service
                                bool notify = false;
                                var account = await _accountRepository.GetOne(accountid);
                                if (account != null)
                                {
                                    Notification.EType notiType = (command.Command == StationControlRequest.EType.Lock) ? Notification.EType.LockDockFailed : Notification.EType.UnlockDockFailed;
                                    Notification notification = new Notification(_localizer, account.Language, accountid, command.TicketId, notiType);
                                    notify = await _notificationService.PushControlDockNotification(account.Id, notification, "");
                                }

                                Utility.Log.Info("TicketTransactionService_ControlDock", $"AccountId: {accountid} control Command: {JsonConvert.SerializeObject(command)} - Success: FALSE - Notify: {accountid} - {notify}");
                            }
                        }
                        else
                        {
                            errorCode = ApiResponse.Code.DockHasNoBike;
                        }

                    }
                    else if (command.Command == StationControlRequest.EType.Lock)
                    {
                        if (dock.CurrentBikeId <= 0)
                        {
                            if (command.TransactionId > 0)
                            {
                                var transaction = await _repository.GetOne(command.TransactionId);
                                if (transaction != null)
                                {
                                    if (transaction.AccountId == accountid)
                                    {
                                        // save request
                                        var request = StationControlRequest.CreateLockRequest(dock.StationId, dock.Id, accountid, command.TransactionId);
                                        await _stationControlRequestRepository.AddAsync(request);
                                        await _stationControlRequestRepository.Commit();

                                        // send control request to Station Socket Service
                                        bool result = await _stationSocketService.CallStationApi(station.IMEI, dock.OrderNumber, (int)command.Command);

                                        if (result)
                                        {
                                            requestId = request.Id;
                                            Utility.Log.Info("TicketTransactionService_ControlDock", $"AccountId: {accountid} control Command: {JsonConvert.SerializeObject(command)} - Open RequestId: {requestId} - Success: TRUE");
                                        }
                                        else
                                        {
                                            errorCode = ApiResponse.Code.DockControlFailed;
                                            // delete request 
                                            _stationControlRequestRepository.Delete(request);
                                            await _stationControlRequestRepository.Commit();

                                            // Send failed notification to Notification Service
                                            bool notify = false;
                                            var account = await _accountRepository.GetOne(accountid);
                                            if (account != null)
                                            {
                                                Notification.EType notiType = (command.Command == StationControlRequest.EType.Lock) ? Notification.EType.LockDockFailed : Notification.EType.UnlockDockFailed;
                                                Notification notification = new Notification(_localizer, account.Language, accountid, command.TicketId, notiType);
                                                notify = await _notificationService.PushControlDockNotification(account.Id, notification, "");
                                            }

                                            Utility.Log.Info("TicketTransactionService_ControlDock", $"AccountId: {accountid} control Command: {JsonConvert.SerializeObject(command)} - Success: FALSE - Notify: {accountid} - {notify}");
                                        }
                                    }
                                    else
                                    {
                                        errorCode = ApiResponse.Code.InvalidAccount;
                                    }
                                }
                                else
                                {
                                    errorCode = ApiResponse.Code.TransactionNotExist;
                                }
                            }
                            else
                            {
                                errorCode = ApiResponse.Code.TransactionInvalid;
                            }
                        }
                        else
                        {
                            errorCode = ApiResponse.Code.DockAlreadyHasBike;
                        }
                    }
                }
                else
                {
                    errorCode = ApiResponse.Code.StationNotExist;
                }
            }
            else
            {
                if (dock == null)
                    errorCode = ApiResponse.Code.DockNotExist;
                else
                {
                    if (dock.Status == Dock.EStatus.Inactive)
                        errorCode = ApiResponse.Code.DockNotActive;

                    if (dock.Status == Dock.EStatus.Error)
                        errorCode = ApiResponse.Code.DockError;
                }
            }

            return new Tuple<int, ApiResponse.Code>(requestId, errorCode);
        }

        public virtual async Task<Tuple<bool, ApiResponse.Code>> ControlDockManual(string imei, int dock_number, string etag, int command)
        {
            var success = false;
            ApiResponse.Code errorCode = ApiResponse.Code.Ok;

            StationControlRequest.EType commandType = (StationControlRequest.EType)command;

            switch (commandType)
            {
                case StationControlRequest.EType.Unlock:
                    var account = await _accountRepository.FindByEtag(etag);
                    if (account != null)
                    {
                        var station = await _stationRepository.FindByImei(imei);
                        if (station != null)
                        {
                            var dock = await _dockRepository.FindByNumber(station.Id, dock_number);
                            if (dock != null)
                            {
                                var ticket = GetBestTicket(await _ticketRepository.FindByAccount(account.Id));
                                if (ticket == null)
                                {
                                    // save ticket
                                    ticket = new Ticket(account.Id);
                                    await _ticketRepository.AddAsync(ticket);
                                    await _ticketRepository.Commit();
                                }

                                // save request
                                var request = StationControlRequest.CreateUnlockRequest(station.Id, dock.Id, account.Id, ticket.Id);
                                await _stationControlRequestRepository.AddAsync(request);
                                await _stationControlRequestRepository.Commit();

                                // send control request to Station Socket Service
                                bool result = await _stationSocketService.CallStationApi(station.IMEI, dock.OrderNumber, command);

                                if (result)
                                {
                                    success = true;
                                    Utility.Log.Info("TicketTransactionService_ControlDockManual", $"Imei: {imei} - Dock: {dock_number} - Etag: {etag} - Command: {StationControlRequest.EType.Unlock} - Open RequestId: {request.Id} - Success: {success}");
                                }
                                else
                                {
                                    // delete request 
                                    _stationControlRequestRepository.Delete(request);
                                    await _stationControlRequestRepository.Commit();

                                    // Send failed notification to Notification Service
                                    Notification.EType notiType = Notification.EType.UnlockDockFailed;
                                    Notification notification = new Notification(_localizer, account.Language, account.Id, 0, notiType);
                                    bool notify = await _notificationService.PushControlDockNotification(account.Id, notification, "");
                                    Utility.Log.Info("TicketTransactionService_ControlDockManual", $"Imei: {imei} - Dock: {dock_number} - Etag: {etag} - Command: {StationControlRequest.EType.Unlock} - Success: {success} - Notify: {account.Id} - {notify}");
                                }
                            }
                            else
                            {
                                errorCode = ApiResponse.Code.DockNotExist;
                            }
                        }
                        else
                        {
                            errorCode = ApiResponse.Code.StationNotExist;
                        }
                    }
                    else
                    {
                        errorCode = ApiResponse.Code.AccountNotExist;
                    }
                    break;
                case StationControlRequest.EType.Lock:
                    var bike = await _bikeRepository.FindByEtag(etag);
                    if (bike != null)
                    {
                        var station = await _stationRepository.FindByImei(imei);
                        if (station != null)
                        {
                            var dock = await _dockRepository.FindByNumber(station.Id, dock_number);
                            if (dock != null)
                            {
                                var transaction = await _repository.FindByBike(bike.Id);
                                if (transaction != null)
                                {
                                    var ticket = await _ticketRepository.GetOne(transaction.TicketId);
                                    if (ticket != null)
                                    {
                                        var bikerAcc = await _accountRepository.GetOne(ticket.AccountId);
                                        bool notify = false;
                                        int notifyId = 0;
                                        if (bikerAcc != null)
                                        {
                                            // delete transaction
                                            _repository.Delete(transaction);
                                            await _repository.Commit();

                                            // add bike to dock
                                            dock.CurrentBikeId = bike.Id;
                                            _dockRepository.Update(dock);
                                            await _dockRepository.Commit();

                                            // add bike to station
                                            station.TotalBike++;
                                            if (station.TotalBike > station.TotalDock)
                                                station.TotalBike = station.TotalDock;
                                            _stationRepository.Update(station);
                                            await _stationRepository.Commit();

                                            // checkout fee and add history
                                            double money = CalculatePrice(transaction);
                                            var transactionHistory = new TicketTransactionHistory(transaction, money);
                                            await _iTicketTransactionHistoryRepository.AddAsync(transactionHistory);
                                            await _iTicketTransactionHistoryRepository.Commit();

                                            // call payment api in another thread
                                            if (transactionHistory != null && money > 0)
                                            {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                                                Task.Run(() => CheckOut(transaction.AccountId, transaction.Id, money, bikerAcc.Language)).ConfigureAwait(false);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                                            }

                                            // Send notification to Notification Service
                                            Notification.EType notiType = Notification.EType.LockDockSuccessfully;
                                            Notification notification = new Notification(_localizer, bikerAcc.Language, bikerAcc.Id, 0, 0, transaction.Id, notiType);
                                            notifyId = bikerAcc.Id;
                                            notify = await _notificationService.PushControlDockNotification(bikerAcc.Id, notification, "");
                                        }

                                        success = true;
                                        Utility.Log.Info("TicketTransactionService_ControlDockManual", $"Imei: {imei} - Dock: {dock_number} - Etag: {etag} - Command: {StationControlRequest.EType.Lock} - Close TransactionId: {transaction.Id} - Success: {success} - Notify: {notifyId} - {notify}");
                                    }
                                    else
                                    {
                                        errorCode = ApiResponse.Code.TicketInvalid;
                                    }
                                }
                                else
                                {
                                    errorCode = ApiResponse.Code.TransactionNotExist;
                                }
                            }
                            else
                            {
                                errorCode = ApiResponse.Code.DockNotExist;
                            }
                        }
                        else
                        {
                            errorCode = ApiResponse.Code.StationNotExist;
                        }


                    }
                    else
                    {
                        errorCode = ApiResponse.Code.BikeNotExist;
                    }

                    break;
            }

            return new Tuple<bool, ApiResponse.Code>(success, errorCode);
        }

        public virtual async Task<Tuple<bool, ApiResponse.Code>> ControlDockConfirm(string imei, int dock_number, int command, int status)
        {
            var success = false;
            ApiResponse.Code errorCode = ApiResponse.Code.Ok;

            var station = await _stationRepository.FindByImei(imei);
            if (station != null)
            {
                var dock = await _dockRepository.FindByNumber(station.Id, dock_number);
                if (dock != null)
                {
                    var request = await _stationControlRequestRepository.GetByStationAndDock(station.Id, dock.Id, (StationControlRequest.EType)command);

                    if (request != null)
                    {
                        // Send request to Notification Service
                        var account = await _accountRepository.GetOne(request.AccountId);
                        if (account != null)
                        {
                            int transactionId = 0;
                            Notification.EType notiType = Notification.EType.Undefined;

                            switch (request.Type)
                            {
                                case StationControlRequest.EType.Unlock:
                                    // create transaction
                                    var transaction = new TicketTransaction(request.AccountId, request.TicketId, station.Id, dock.Id, dock.CurrentBikeId.HasValue ? dock.CurrentBikeId.Value : 0, TicketTransaction.EStatus.Running);
                                    await _repository.AddAsync(transaction);
                                    await _repository.Commit();
                                    transactionId = transaction.Id;

                                    // delete bike
                                    dock.CurrentBikeId = 0;
                                    _dockRepository.Update(dock);
                                    await _dockRepository.Commit();

                                    // recalculate station info
                                    station.TotalBike--;
                                    if (station.TotalBike < 0)
                                        station.TotalBike = 0;
                                    _stationRepository.Update(station);
                                    await _stationRepository.Commit();

                                    notiType = (status == 1) ? Notification.EType.UnlockDockSuccessfully : Notification.EType.UnlockDockFailed;
                                    break;
                                case StationControlRequest.EType.Lock:
                                    // delete transaction
                                    transaction = await _repository.GetOne(request.TransactionId);
                                    if (transaction != null)
                                    {
                                        transactionId = transaction.Id;
                                        _repository.Delete(transaction);
                                        await _repository.Commit();

                                        // add bike to dock
                                        dock.CurrentBikeId = transaction.BikeId;
                                        _dockRepository.Update(dock);
                                        await _dockRepository.Commit();

                                        // add bike to station
                                        station.TotalBike++;
                                        if (station.TotalBike > station.TotalDock)
                                            station.TotalBike = station.TotalDock;
                                        _stationRepository.Update(station);
                                        await _stationRepository.Commit();

                                        // add lịch sử
                                        double money = CalculatePrice(transaction);
                                        var transactionHistory = new TicketTransactionHistory(transaction, money);
                                        await _iTicketTransactionHistoryRepository.AddAsync(transactionHistory);
                                        await _iTicketTransactionHistoryRepository.Commit();

                                        // call payment api in another thread
                                        if (transactionHistory != null && money > 0)
                                        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                                            Task.Run(() => CheckOut(transaction.AccountId, transaction.Id, money, account.Language)).ConfigureAwait(false);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                                        }
                                    }

                                    notiType = (status == 1) ? Notification.EType.LockDockSuccessfully : Notification.EType.LockDockFailed;
                                    break;
                            }

                            // delete request
                            _stationControlRequestRepository.Delete(request);
                            await _stationControlRequestRepository.Commit();

                            Notification notification = new Notification(_localizer, account.Language, request.AccountId, request.TicketId, request.Id, transactionId, notiType);
                            bool notify = await _notificationService.PushControlDockNotification(account.Id, notification, "");

                            success = true;
                            Utility.Log.Info("TicketTransactionService_ControlDockConfirm", $"Imei: {imei} - Dock: {dock_number}- Command: {(StationControlRequest.EType)command} - Close RequestId: {request.Id} - Open TransactionId: {transactionId} - Status: {status} - Success: {success} - Notify: {account.Id} - {notify}");
                        }
                        else
                        {
                            errorCode = ApiResponse.Code.AccountNotExist;
                        }
                    }
                    else
                    {
                        errorCode = ApiResponse.Code.RequestNotExist;
                    }
                }
                else
                {
                    errorCode = ApiResponse.Code.DockNotExist;
                }
            }
            else
            {
                errorCode = ApiResponse.Code.StationNotExist;
            }

            return new Tuple<bool, ApiResponse.Code>(success, errorCode);
        }

        public virtual bool UseStatus(int accountId)
        {
            return _repository.UseStatus(accountId);
        }
        public virtual TicketTransactionHistory GetTransactionHistory(GetTransactionHistoryCommand model)
        {
            return _iTicketTransactionHistoryRepository.GetByTransactionId(model.AccountId, model.TicketTransactionId);
        }
        public virtual async Task<int> UpdateByTransactionHistoryAsync(UpdateTransactionHistoryCommand model)
        {
            return await _iTicketTransactionHistoryRepository.UpdateByTransactionHistoryAsync(model.AccountId, model.TicketTransactionId, model.Rating, model.Comment);
        }
        public virtual async Task<object> TransactionHistoryGetByAccount(TransactionHistoryGetByAccountCommand model)
        {
            if (model.TypeId == 1)
                return await _iTicketTransactionHistoryRepository.TransactionHistoryGetByAccount(model.AccountId);
            else
                return await _iBookingRepository.GetListBooking(model.AccountId);
        }
        public TicketTransaction LastTransaction(AccountLastTransactionCommand model)
        {
            return _repository.LastTransaction(model.AccountId);
        }

        private Ticket GetBestTicket(List<Ticket> tickets)
        {
            if (tickets == null || tickets.Count == 0)
                return null;

            return tickets.FirstOrDefault();
        }

        private double CalculatePrice(TicketTransaction transaction)
        {
            double money = 0;
            int seconds = Convert.ToInt32((DateTime.Now - transaction.StartDate).TotalSeconds);
            //if (seconds < 30)
            //    return money;

            int blockCount = seconds / 60 / 15;
            if (blockCount <= 0)
                blockCount = 1;
            money = blockCount * 2500;

            return money;
        }

        private async Task<bool> Payment(PaymentOrderCommand model)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    model.Merchantcode = _baseSetting.MerchantCode;
                    model.Smartcontract = _baseSetting.SmartContract;
                    model.Billid = Guid.NewGuid().ToString();

                    PaymentApi api = new PaymentApi(_baseSetting.AddressAPIPayment, _baseSetting.ApiRequestTimeout);
                    var data = await api.Order(model);
                    if (data.Status >= 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TicketTransactionService_Payment", ex.ToString());
                return false;
            }
        }

        private async Task CheckOut(int accountid, int transactionid, double money, string lang)
        {
            try
            {
                var paymentOut = await Payment(new PaymentOrderCommand { Accountid = accountid, TicketTransactionId = transactionid, Total = money });
                var transactionHistory = _iTicketTransactionHistoryRepository.GetByTransactionId(accountid, transactionid);
                if (transactionHistory != null)
                {
                    if (paymentOut)
                    {
                        transactionHistory.PaymentStatus = TicketTransactionHistory.EPaymentStatus.Done;
                    }
                    else
                    {
                        transactionHistory.PaymentStatus = TicketTransactionHistory.EPaymentStatus.Failed;
                    }
                    _iTicketTransactionHistoryRepository.Update(transactionHistory);
                    await _iTicketTransactionHistoryRepository.Commit();
                    try
                    {
                        // Send notification to Notification Service
                        Notification.EType notiType = Notification.EType.PaymentSuccessfully;
                        Notification notification = new Notification(_localizer, lang, accountid, 0, 0, transactionid, notiType);
                        bool notify = await _notificationService.PushControlDockNotification(accountid, notification, "");
                        Utility.Log.Info("TicketTransactionService_CheckOut", $"AccountId: {accountid} - TransactionId: {transactionid} - Money: {money} - Success: {paymentOut} - Notify: {accountid} - {notify}");
                    }
                    catch (Exception ex)
                    {
                        Utility.Log.Error("TicketTransactionService_CheckOut_Notification", ex.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                Utility.Log.Error("TicketTransactionService_CheckOut", ex.ToString());
            }
        }


    }
}
