﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Services
{
    public interface IWalletService : IService<Wallet>
    {
    }
   
    public class WalletService : IWalletService
    {
        private readonly IWalletRepository _WalletRepository;

        public WalletService(IWalletRepository WalletRepository)
        {
            _WalletRepository = WalletRepository;
            
        }
        
    }
}