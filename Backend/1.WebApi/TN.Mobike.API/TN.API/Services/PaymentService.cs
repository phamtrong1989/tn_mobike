﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.Domain.Model;
using TN.Domain.Seedwork;
using TN.Utility;

namespace TN.API.Services
{
    public interface IPaymentService : IService<TicketTransaction>
    {
        Task<PaymentResponseDTO<PaymentHistoryResponseDTO>> Deposit(PaymentDepositCommand model);
        Task<PaymentResponseDTO<object>> Balance(PaymentQueryBalanceCommand model);
        Task<List<PaymentHistoryResponseDTO>> History(PaymentHistoryCommand model, int start = 0, int size = 10);
        Task<bool> Order(PaymentOrderCommand model);
        Task<int> DepositDone(int accountid,
            int amount,
            string message,
            string payment_type,
            string reference_number,
            int status,
            int trans_ref_no,
            int website_id,
            string signature, int type, bool result = true);
        Task<int> GetBalance(int accountid);
    }
    public class PaymentApi : RestApi
    {
        public PaymentApi(string baseAddress, int timeout) : base(baseAddress, timeout)
        {
        }

        public async Task<PaymentResponseDTO<PaymentHistoryResponseDTO>> Deposit(PaymentDepositCommand info, CancellationToken ctoken = default(CancellationToken))
        {
            return await SendRequestAsync<PaymentResponseDTO<PaymentHistoryResponseDTO>>(HttpMethod.Post, $"/api/payment/deposit", info, ctoken);
        }
        public async Task<PaymentResponseDTO<object>> Order(PaymentOrderCommand info, CancellationToken ctoken = default(CancellationToken))
        {
            return await SendRequestAsync<PaymentResponseDTO<object>>(HttpMethod.Post, $"/api/Payment/pay", info, ctoken);
        }
        public async Task<PaymentResponseDTO<object>> Balance(PaymentQueryBalanceCommand info, CancellationToken ctoken = default(CancellationToken))
        {
            return await SendRequestAsync<PaymentResponseDTO<object>>(HttpMethod.Post, $"/api/Payment/cmd", info, ctoken);
        }
        public async Task<PaymentResponseDTO<List<PaymentHistoryResponseDTO>>> History(PaymentHistoryCommand info, CancellationToken ctoken = default(CancellationToken))
        {
            return await SendRequestAsync<PaymentResponseDTO<List<PaymentHistoryResponseDTO>>>(HttpMethod.Post, $"/api/Payment/cmd", info, ctoken);
        }
    }
    public class PaymentService : IPaymentService
    {
        private readonly BaseSetting _baseSetting;
        private readonly IWalletRepository _iWalletRepository;
        private readonly IWalletTransactionRepository _iWalletTransactionRepository;
        //private readonly IWalletService _WalletService;

        public PaymentService(IOptions<BaseSetting> baseSetting, IWalletRepository iWalletRepository, IWalletTransactionRepository iWalletTransactionRepository)
        {
            _baseSetting = baseSetting.Value;
            _iWalletRepository = iWalletRepository;
            _iWalletTransactionRepository = iWalletTransactionRepository;
        }
        public virtual async Task<PaymentResponseDTO<PaymentHistoryResponseDTO>> Deposit(PaymentDepositCommand model)
        {
            using (var client = new HttpClient())
            {
                model.Cmd = "deposit";
                model.Code = Guid.NewGuid().ToString();
                model.Merchantcode = _baseSetting.MerchantCode;
                PaymentApi api = new PaymentApi(_baseSetting.AddressAPIPayment, _baseSetting.ApiRequestTimeout);
                var data = await api.Deposit(model);
                if (data.Status == 1)
                {
                    return data;
                }
                else
                {
                    return null;
                }
            }
        }
        public virtual async Task<bool> Order(PaymentOrderCommand model)
        {
            using (var client = new HttpClient())
            {
                model.Merchantcode = _baseSetting.MerchantCode;
                model.Smartcontract = _baseSetting.SmartContract;
                model.Billid = Guid.NewGuid().ToString();
                model.Cmd = "pay";
                PaymentApi api = new PaymentApi(_baseSetting.AddressAPIPayment, _baseSetting.ApiRequestTimeout);
                var data = await api.Order(model);
                if (data.Status == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public virtual async Task<PaymentResponseDTO<object>> Balance(PaymentQueryBalanceCommand model)
        {
            using (var client = new HttpClient())
            {
                model.CMD = "balance";
                model.MerchantCode = _baseSetting.MerchantCode;
                model.Smartcontract = _baseSetting.SmartContract;
                PaymentApi api = new PaymentApi(_baseSetting.AddressAPIPayment, _baseSetting.ApiRequestTimeout);
                var data = await api.Balance(model);
                if (data.Status == 1)
                {
                    return data;
                }
                else
                {
                    return null;
                }
            }
        }
        public virtual async Task<List<PaymentHistoryResponseDTO>> History(PaymentHistoryCommand model, int start = 0, int size = 10)
        {
            using (var client = new HttpClient())
            {
                model.Cmd = "history";
                model.MerchantCode = _baseSetting.MerchantCode;
                model.Start = start <= 0 ? 0 : start;
                model.Size = (size >= 100 || size < 0) ? 10 : size;

                PaymentApi api = new PaymentApi(_baseSetting.AddressAPIPayment, _baseSetting.ApiRequestTimeout);
                var data = await api.History(model);
                if (data.Status == 1)
                {
                    return data.Data;
                }
                else
                {
                    return null;
                }
            }
        }
        public virtual async Task<int> DepositDone(int accountid,
            int amount,
            string message,
            string payment_type,
            string reference_number,
            int status,
            int trans_ref_no,
            int website_id,
            string signature, int type, bool rst)
        {
            if (rst)
            {
                var walletid = await _iWalletRepository.UpdateOrInsert(accountid, amount, type);
                string note = string.Format("accountid={0} <br/>amount={1}<br/>message={2}<br/>payment_type={3}<br/>reference_number={4}<br/>status={5}<br/>trans_ref_no={6}<br/>website_id={7}<br/>signature={8}",
                    accountid, amount, message, payment_type, reference_number, status, trans_ref_no, website_id, signature);
                await _iWalletTransactionRepository.UpdateOrInsert(amount, note, 0, WalletTransaction.EType.Deposit, walletid);
                return walletid;
            }
            else
            {
                string note = string.Format("accountid={0} <br/>amount={1}<br/>message={2}<br/>payment_type={3}<br/>reference_number={4}<br/>status={5}<br/>trans_ref_no={6}<br/>website_id={7}<br/>signature={8}",
                    accountid, amount, message, payment_type, reference_number, status, trans_ref_no, website_id, signature);
                var result = await _iWalletTransactionRepository.UpdateOrInsert(amount, note, 0, WalletTransaction.EType.Deposit, 0);
                return result;
            }

        }
        public virtual async Task<int> GetBalance(int accountid)
        {
            var result = await _iWalletRepository.GetBalance(accountid);
            return result;
        }
    }
}
