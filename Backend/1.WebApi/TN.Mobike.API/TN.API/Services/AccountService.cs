﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Services
{
    public interface IAccountService : IService<Account>
    {
        Task<AccountDTO> Login(string username, string password);
        string RequestToken(string id);
        Task<bool> UpdateAccountMobileInfo(int id, string token);
        Task<bool> TestAccountMobileNotification(int id);
        Task<Tuple<int, AccountDTO>> Register(AccountRegisterCommand model);
        Task<int> UpdateProfile(AccountUpdateProfileCommand model);
        Task<bool> UploadAvatar(AccountUploadAvatarCommand model);
        Task<int> OTPRegister(AccountOTPRegisterCommand model);
        Task<int> ConfirmOTPRegister(AccountConfirmOTPRegisterCommand model);
        Task<int> CreateOTPLogin(AccountCreateOTPLoginCommand model);
        Task<Tuple<int, AccountDTO>> LoginWithOTP(AccountLoginWithOTPCommand model);
        Task<int> Logout(AccountLogoutCommand model);
        Task<AccountDTO> Profile(int id);
        Task<Tuple<Account, int>> UpdatePoin(AccountUpdatePointCommand model);
        Task<int> CheckEmail(string email);
    }

    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly INotificationService _notificationService;
        private readonly IMediator _mediator;
        private readonly IOptions<BaseSetting> _baseSetting;
        private readonly IHostingEnvironment _iHostingEnvironment;
        private readonly IAccountOTPRepository _iAccountOTPRepository;

        public AccountService(IAccountRepository accountRepository,
                            INotificationService notificationService,
                            IOptions<BaseSetting> baseSetting,
                            IMediator mediator,
                            IHostingEnvironment iHostingEnvironment,
                            IAccountOTPRepository iAccountOTPRepository
            )
        {
            _accountRepository = accountRepository;
            _notificationService = notificationService;
            _baseSetting = baseSetting;
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _iHostingEnvironment = iHostingEnvironment;
            _iAccountOTPRepository = iAccountOTPRepository;
        }
        public virtual async Task<int> CreateOTPLogin(AccountCreateOTPLoginCommand model)
        {
            // Kiểm tra số điện thoại này đã tồn tại chưa
            var kttt = await _accountRepository.SearchOneAsync(m => m.Phone == model.Phone);
            if (kttt == null)
            {
                // Số điện thoại đã tồn tại
                return 2;
            }
            int AutoGenOTP = 1234;
            // Tạo một OTP mặc định 1234 vì chưa có sdt tổng đài sinh OTP
            await _iAccountOTPRepository.AddAsync(
                new AccountOTP
                {
                    CreatedDate = DateTime.Now,
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddHours(_baseSetting.Value.OTPLoginExpiredMinute),
                    Phone = model.Phone,
                    Type = AccountOTP.OTPType.Login,
                    OTP = AutoGenOTP,
                    AcountId = kttt.Id
                });
            await _iAccountOTPRepository.Commit();
            // Đặt lệnh gửi đến số điện thoại mã OTP
            // Lét go gửi đê
            return 1;
        }
        public virtual async Task<int> OTPRegister(AccountOTPRegisterCommand model)
        {
            // Kiểm tra số điện thoại này đã tồn tại chưa
            var kttt = await _accountRepository.AnyAsync(m => m.Phone == model.Phone);
            if (kttt)
            {
                // Số điện thoại đã tồn tại
                return 2;
            }
            int AutoGenOTP = 1234;
            // Tạo một OTP mặc định 1234 vì chưa có sdt tổng đài sinh OTP
            await _iAccountOTPRepository.AddAsync(
                new AccountOTP
                {
                    CreatedDate = DateTime.Now,
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddMinutes(_baseSetting.Value.OTPRegiterExpiredMinute),
                    Phone = model.Phone,
                    Type = AccountOTP.OTPType.Register,
                    OTP = AutoGenOTP
                });
            await _iAccountOTPRepository.Commit();
            // Đặt lệnh gửi đến số điện thoại mã OTP
            // Lét go gửi đê
            return 1;
        }

        public virtual async Task<int> ConfirmOTPRegister(AccountConfirmOTPRegisterCommand model)
        {
            // Kiểm tra số điện thoại này đã tồn tại chưa
            var kttt = await _accountRepository.AnyAsync(m => m.Phone == model.Phone);
            if (kttt)
            {
                // Số điện thoại đã tồn tại
                return 2;
            }
            var ktOTP = _iAccountOTPRepository.CheckConfigOTP(model.Phone, model.OTP);
            if (ktOTP == null)
            {
                return 3;
            }
            else
            {
                ktOTP.Status = true;
                ktOTP.ConfirmDate = DateTime.Now;
                _iAccountOTPRepository.Update(ktOTP);
                await _iAccountOTPRepository.Commit();
                return 1;
            }
        }

        public virtual async Task<Tuple<int, AccountDTO>> Register(AccountRegisterCommand model)
        {
            if (!Directory.Exists(_baseSetting.Value.FolderMobikeAvatarServer))
            {
                Directory.CreateDirectory(_baseSetting.Value.FolderMobikeAvatarServer);
            }
            string lastName = "", firstName = "";
            string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}/{model.Phone}.jpg";
            string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/{model.Phone}.jpg";

            // Kiểm tra số điện thoại này đã tồn tại chưa
            var kttt = await _accountRepository.AnyAsync(m => m.Phone == model.Phone);
            if (kttt)
            {
                // Đã tồn tại
                return new Tuple<int, AccountDTO>(2, null);
            }
            // Kiểm tra Email này đã có người sử dụng chưa
            var anyEmail = await _accountRepository.AnyAsync(m=> m.Email == model.Email);
            if (anyEmail)
            {
                // Đã tồn tại
                return new Tuple<int, AccountDTO>(4, null);
            }
            // Kiểm tra đã xác thực OTP chưa
            //var ktOTP =  _iAccountOTPRepository.CheckOTP(model.Phone);
            //if(ktOTP==null)
            //{
            //    // Chưa xác nhận OTP hoặc OTP hết hạn
            //    return new Tuple<int, AccountDTO>(3,null);
            //}
            var arrName = model.FullName.Split(" ");
            if (arrName.Length > 0)
            {
                lastName = arrName[arrName.Length - 1];
            }
            if (arrName.Length > 1)
            {
                firstName = model.FullName.Replace(arrName[arrName.Length - 1], "");
            }
            // Thêm tài khoản
            var dataAdd = new Account
            {
                Phone = model.Phone,
                Username = model.Phone,
                Email = model.Email,
                Avatar = pathAvatarWeb,
                FirstName = firstName.Trim(),
                LastName = lastName.Trim(),
                Sex = model.Sex,
                Birthday = model.Birthday,
                Status = Account.EStatus.Active,
                Type = Account.EAccountType.Normal,
                CreatedDate = DateTime.Now,
                Password = Function.HMACMD5Password(model.Password),
                Code = RandomString(25),
                CodeNguoiChiaSe = model.CodeChiaSe
            };
            await _accountRepository.AddAsync(dataAdd);
            await _accountRepository.Commit();
            //string str = "E:\\ITS\\Projects\\Mobike\\Services\\MobikeAPI\\data\\Avatar\\1.jpg";
            //model.AvatarData = imageConversion(str);
            Function.ByteArrayToImage(model.AvatarData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
            if (!File.Exists(pathAvatarServer))
                dataAdd.Avatar = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/noimage.jpg";

            Utility.Log.Info("AccountService_Register", $"{dataAdd.Username} - {true}");
            // Login 
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, dataAdd.Username)
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_baseSetting.Value.SecurityKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            DateTime exp = DateTime.Now.AddMinutes(_baseSetting.Value.JwtTokenExpiredMinute);
            var token = new JwtSecurityToken(
                issuer: "mobike.tn",
                audience: "mobike.tn",
                claims: claims,
                expires: exp,
                signingCredentials: creds);

            Utility.Log.Info("AccountService_Login", $"{dataAdd.Username} - {true}");
            var kttt2 = await _accountRepository.SearchOneAsync(m => m.Username == model.Phone);
            kttt2.ExpTocken = exp;
            _accountRepository.Update(kttt2);
            await _accountRepository.Commit();

            return new Tuple<int, AccountDTO>(1, new AccountDTO(dataAdd, new JwtSecurityTokenHandler().WriteToken(token)));
        }
        public static byte[] imageConversion(string imageName)
        {

            //Initialize a file stream to read the image file
            FileStream fs = new FileStream(imageName, FileMode.Open, FileAccess.Read);

            //Initialize a byte array with size of stream
            byte[] imgByteArr = new byte[fs.Length];

            //Read data from the file stream and put into the byte array
            fs.Read(imgByteArr, 0, Convert.ToInt32(fs.Length));

            //Close a file stream
            fs.Close();

            return imgByteArr;
        }
        public virtual async Task<int> UpdateProfile(AccountUpdateProfileCommand model)
        {
            var kttt = await _accountRepository.SearchOneAsync(m => m.Id == model.Id);
            if (kttt == null)
            {
                return 0;
            }
            // Kiểm tra Email này đã có người sử dụng chưa
            var anyEmail = await _accountRepository.AnyAsync(m => m.Id != model.Id && m.Email == model.Email);
            if (anyEmail)
            {
                return 2;
            }
            kttt.Email = model.Email;
            kttt.Sex = model.Sex;
            kttt.FirstName = model.FirstName;
            kttt.LastName = model.LastName;
            kttt.Birthday = model.Birthday;
            kttt.Address = model.Address;
            kttt.Note = model.Note;
            if (model.AvatarData != null)
            {
                string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}/{kttt.Phone}.jpg";
                string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/{kttt.Phone}.jpg";
                kttt.Avatar = pathAvatarWeb;
                Function.ByteArrayToImage(model.AvatarData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
            }
            _accountRepository.Update(kttt);
            await _accountRepository.Commit();
            Utility.Log.Info("AccountService_UpdateProfile", $"{kttt.Username} - {true}");
            return 1;
        }
        public virtual async Task<bool> UploadAvatar(AccountUploadAvatarCommand model)
        {
            var kttt = await _accountRepository.SearchOneAsync(m => m.Id == model.Id);
            if (kttt == null)
            {
                return false;
            }
            if (!Directory.Exists(_baseSetting.Value.FolderMobikeAvatarServer))
            {
                Directory.CreateDirectory(_baseSetting.Value.FolderMobikeAvatarServer);
            }
            string pathAvatarServer = $"{_baseSetting.Value.FolderMobikeAvatarServer}/{kttt.Phone}.jpg";
            string pathAvatarWeb = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/{kttt.Phone}.jpg";
            kttt.Avatar = pathAvatarWeb;
            _accountRepository.Update(kttt);
            await _accountRepository.Commit();
            return Function.ByteArrayToImage(model.AvatarData, pathAvatarServer, _baseSetting.Value.ZipAvatarWidth);
        }
        public virtual async Task<AccountDTO> Profile(int id)
        {
            var account = await _accountRepository.SearchOneAsync(m => m.Id == id);
            if (account != null)
            {
                return new AccountDTO(account, null);
            }
            else
            {
                return null;
            }
        }
        public virtual async Task<AccountDTO> Login(string username, string password)
        {
            var account = await _accountRepository.Login(username, Function.HMACMD5Password(password));

            if (account != null)
            {
                string Avatar = $"{_baseSetting.Value.FolderMobikeAvatarServer}".Replace("\\Data\\Avatar", "") + account.Avatar;
                if (!File.Exists(Avatar))
                    account.Avatar = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/noimage.jpg";
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, username)
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_baseSetting.Value.SecurityKey));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                DateTime exp = DateTime.Now.AddMinutes(_baseSetting.Value.JwtTokenExpiredMinute);
                var token = new JwtSecurityToken(
                    issuer: "mobike.tn",
                    audience: "mobike.tn",
                    claims: claims,
                    expires: exp,
                    signingCredentials: creds);

                Utility.Log.Info("AccountService_Login", $"{username} - {true}");

                var kttt = await _accountRepository.SearchOneAsync(m => m.Id == account.Id);
                kttt.ExpTocken = exp;
                _accountRepository.Update(kttt);
                await _accountRepository.Commit();
                Utility.Log.Info("AccountService_UpdateProfile", $"{kttt.Username} - {true}");

                return new AccountDTO(account, new JwtSecurityTokenHandler().WriteToken(token));
            }

            Utility.Log.Info("AccountService_Login", $"{username} - {false}");

            return null;
        }
        public virtual async Task<int> Logout(AccountLogoutCommand model)
        {
            var kt = await _accountRepository.SearchOneAsync(m => m.Username == model.Username || m.Phone == model.Username);
            if (kt == null)
            {
                return 0;
            }
            else
            {
                // set thời gian hết hạn = thời gian bắt đầu
                var dlOTP = _iAccountOTPRepository.GetLastOTPByUsername(model.Username);
                if (dlOTP != null)
                {
                    dlOTP.EndDate = dlOTP.StartDate;
                    await _iAccountOTPRepository.Commit();
                }
                return 1;
            }
        }
        public virtual async Task<Tuple<int, AccountDTO>> LoginWithOTP(AccountLoginWithOTPCommand model)
        {
            var data = await _accountRepository.LoginWithOTP(model.Username, model.OTP, _baseSetting.Value.OTPInvalidMax);
            if (data.Item1 == 1)
            {
                if (data.Item2 != null)
                {
                    string Avatar = $"{_baseSetting.Value.FolderMobikeAvatarServer}".Replace("\\Data\\Avatar", "") + data.Item2.Avatar;
                    if (!File.Exists(Avatar))
                        data.Item2.Avatar = $"{_baseSetting.Value.FolderMobikeAvatarWeb}/noimage.jpg";
                    var claims = new[]
                    {
                        new Claim(ClaimTypes.Name, model.Username)
                    };

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_baseSetting.Value.SecurityKey));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    DateTime exp = DateTime.Now.AddMinutes(_baseSetting.Value.JwtTokenExpiredMinute);
                    var token = new JwtSecurityToken(
                        issuer: "mobike.tn",
                        audience: "mobike.tn",
                        claims: claims,
                        expires: exp,
                        signingCredentials: creds);
                    Utility.Log.Info("AccountService_LoginWithOTP", $"{model.Username} - {true}");

                    var kttt = await _accountRepository.SearchOneAsync(m => m.Username == model.Username);
                    kttt.ExpTocken = exp;
                    _accountRepository.Update(kttt);
                    await _accountRepository.Commit();

                    return new Tuple<int, AccountDTO>(data.Item1, new AccountDTO(data.Item2, new JwtSecurityTokenHandler().WriteToken(token)));
                }
                Utility.Log.Info("AccountService_LoginWithOTP", $"{model.Username} - {false}");
            }
            else
            {
                return new Tuple<int, AccountDTO>(data.Item1, null);
            }
            return null;
        }

        public virtual string RequestToken(string id)
        {
            if (string.IsNullOrEmpty(id) || !_baseSetting.Value.MobikeServiceId.ToUpper().Split(';').Contains(id.ToUpper()))
                return string.Empty;

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, id)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_baseSetting.Value.SecurityKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "mobike.tn",
                audience: "mobike.tn",
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                //expires: null,
                signingCredentials: creds);

            Utility.Log.Info("AccountService_RequestToken", $"{id}");

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public virtual async Task<bool> UpdateAccountMobileInfo(int id, string token)
        {
            bool success = false;

            var account = await _accountRepository.GetOne(id);

            if (account != null)
            {
                account.MobileToken = token;
                _accountRepository.Update(account);
                await _accountRepository.Commit();

                bool requestApi = await _notificationService.UpdateMobileInfo(id, token, "");
                Utility.Log.Info("AccountService_UpdateAccountMobileInfo", $"{id} - {requestApi} - {token}");
                success = true;
            }

            return success;
        }

        public virtual async Task<bool> TestAccountMobileNotification(int id)
        {
            bool success = false;

            var account = await _accountRepository.GetOne(id);

            if (account != null)
            {
                success = await _notificationService.TestMobileNotification(id, "");
            }

            return success;
        }
        public virtual async Task<Tuple<Account, int>> UpdatePoin(AccountUpdatePointCommand model)
        {
            var kttt = await _accountRepository.SearchOneAsync(m => m.Id == model.Id);
            if (kttt == null)
            {
                return new Tuple<Account, int>(kttt, 0);
            }
            int point = CaculatePointFromDistance(model.Distance);
            kttt.Points += point;
            _accountRepository.Update(kttt);
            await _accountRepository.Commit();
            Utility.Log.Info("AccountService_UpdatePoint", $"{kttt.Username} - {true}");
            return new Tuple<Account, int>(kttt, point);
        }
        public static string RandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = "";
            var random = new Random();
            for (int i = 0; i <= length - 1; i++)
            {
                stringChars += chars[random.Next(chars.Length)];
            }
            return stringChars;
        }
        public int CaculatePointFromDistance(double Distance)
        {
            return 30;
        }
        public virtual async Task<int> CheckEmail(string email)
        {
            var account = await _accountRepository.SearchOneAsync(m => m.Email == email);
            if (account != null)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
