﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Model;

namespace TN.API.Services
{
    public interface IBookingService : IService<Booking>
    {
        Task<BookingDTO> Register(BookingRegisterCommand model);

        Task<BookingDTO> Profile(int id);

        Task<int> UpdateBooking(BookingUpdateCommand model);

        Task<int> UpdateVote(BookingUpdateVoteCommand model);

        Task<BookingDTO> GetBookingByAccountId(int userid);
    }

    public class BookingService : IBookingService
    {
        private readonly IBookingRepository _bookingRepository;

        public BookingService(IBookingRepository bookingRepository)
        {
            _bookingRepository = bookingRepository;
        }

        public virtual async Task<BookingDTO> Register(BookingRegisterCommand model)
        {
            // Thêm thông tin booking
            var dataAdd = new Booking
            {
                AccountId = model.AccountId,
                StatusId = Booking.BookingStatus.Booking,
                CreatedDate = DateTime.Now
                //StartDate = DateTime.Now,
                //EndDate = DateTime.Now
            };
            await _bookingRepository.AddAsync(dataAdd);
            await _bookingRepository.Commit();
            Utility.Log.Info("Booking_Register", $"{dataAdd.AccountId} - {true}");

            return new BookingDTO(dataAdd);
        }

        public virtual async Task<BookingDTO> Profile(int id)
        {
            var account = await _bookingRepository.SearchOneAsync(m => m.Id == id);
            if (account != null)
            {
                return new BookingDTO(account);
            }
            else
            {
                return null;
            }
        }
        public virtual async Task<BookingDTO> GetBookingByAccountId(int AccountId)
        {
            var account = await _bookingRepository.SearchOneAsync(m => m.AccountId == AccountId && m.StatusId == Booking.BookingStatus.Start);
            if (account != null)
            {
                return new BookingDTO(account);
            }
            else
            {
                return null;
            }
        }
        [HttpPost("update-booking-status/{id}")]
        public async Task<int> UpdateBooking([FromBody]BookingUpdateCommand request)
        {
            var kttt = await _bookingRepository.SearchOneAsync(m => m.Id == request.Id);
            if (kttt == null)
            {
                return 0;
            }
            kttt.StatusId = request.StatusId;
            if (kttt.StatusId == Booking.BookingStatus.Start)
                kttt.StartDate = DateTime.Now;
            if (kttt.StatusId == Booking.BookingStatus.End)
                kttt.EndDate = DateTime.Now;
            _bookingRepository.Update(kttt);
            await _bookingRepository.Commit();
            Utility.Log.Info("BoookingService_UpdateBooking", $"{kttt.AccountId} - {true}");
            return 1;
        }

        public virtual async Task<object> GetListBooking(TransactionHistoryGetByAccountCommand model)
        {
            return await _bookingRepository.GetListBooking(model.AccountId);
        }

        [HttpPost("update-vote/{id}")]
        public async Task<int> UpdateVote([FromBody]BookingUpdateVoteCommand request)
        {
            var kttt = await _bookingRepository.SearchOneAsync(m => m.Id == request.Id);
            if (kttt == null)
            {
                return 0;
            }
            kttt.Vote = request.Vote;
            kttt.Messenger = request.Messenger;
            _bookingRepository.Update(kttt);
            await _bookingRepository.Commit();
            Utility.Log.Info("BoookingService_UpdateVote", $"{kttt.AccountId} - {true}");
            return 1;
        }
    }
}