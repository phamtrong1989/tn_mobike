﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.Domain.Model;

namespace TN.API.Services
{
    public interface IStationService : IService<Station>
    {
        Task<List<StationDTO>> FindByLatLng(string lat_lng);
        Task<bool> UpdateStationStatus(string imei, UpdateStationStatusCommand command);
    }

    public class StationService : IStationService
    {
        private readonly IStationRepository _stationRepository;
        private readonly IDockRepository _dockRepository;
        private readonly IBikeRepository _bikeRepository;
        private readonly ISystemNotificationRepository _systemNotificationRepository;
        private readonly IMediator _mediator;

        public StationService(IStationRepository stationRepository,
                            IDockRepository dockRepository,
                            IBikeRepository bikeRepository,
                            ISystemNotificationRepository systemNotificationRepository,
                            IMediator mediator)
        {
            _stationRepository = stationRepository;
            _dockRepository = dockRepository;
            _bikeRepository = bikeRepository;
            _systemNotificationRepository = systemNotificationRepository;
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public virtual async Task<List<StationDTO>> FindByLatLng(string lat_lng)
        {
            string[] comps = lat_lng.Split(';');
            double lat = Convert.ToDouble(comps[0]);
            double lng = Convert.ToDouble(comps[1]);
            double delta = 0.022;
            var stations = await _stationRepository.FindByLatLng(lat - delta, lat + delta, lng - delta, lng + delta);

            List<StationDTO> dtoStations = new List<StationDTO>();
            foreach (var station in stations)
            {
                double distance = Utility.Function.DistanceInMeter(lat, lng, station.Lat, station.Lng);
                if(distance<=3000)
                {
                    StationDTO dtoStation = new StationDTO(station, Convert.ToInt32(distance));
                    if (station.Docks != null)
                    {
                        foreach (var dock in station.Docks)
                        {
                            DockDTO dtoDock = new DockDTO(dock);
                            // dtoDock.CurrentBike = new BikeDTO(dock.CurrentBike);
                            dtoDock.CurrentBike = new BikeDTO(new Bike { });
                            dtoStation.Docks.Add(dtoDock);
                        }
                    }
                    dtoStations.Add(dtoStation);
                }
            }

            return dtoStations;
        }

        public virtual async Task<bool> UpdateStationStatus(string imei, UpdateStationStatusCommand command)
        {
            bool success = false;

            var station = await _stationRepository.FindByImei(imei);

            if (station != null)
            {
                string[] dockComps = command.DockStatusStr.Split(';');
                foreach (string dockStatusStr in dockComps)
                {
                    try
                    {
                        string[] statusComps = dockStatusStr.Split('|');
                        if (statusComps.Length >= 3)
                        {
                            int dock_number = Convert.ToInt16(statusComps[0]);
                            int dock_status = Convert.ToInt16(statusComps[1]);
                            bool bike_status = Convert.ToBoolean(Convert.ToInt16(statusComps[2]));
                            bool lock_status = !Convert.ToBoolean(Convert.ToInt16(statusComps[3]));
                            string bike_etag = statusComps[4];

                            var dock = await _dockRepository.FindByNumber(station.Id, dock_number);
                            if (dock != null)
                            {
                                if (dock_status != 0)
                                {
                                    dock.ErrorStatus = true;
                                    // alert
                                    var notification = new SystemNotification(dock.Id, SystemNotification.EObjectType.Dock, SystemNotification.EStatus.Error, SystemNotification.EType.Alarm);
                                    await _systemNotificationRepository.AddAsync(notification);
                                    await _systemNotificationRepository.Commit();
                                }
                                else
                                {
                                    dock.ErrorStatus = false;

                                    bool bikeAlarm = false;
                                    int bikeAlarmId = 0;
                                    dock.LockStatus = lock_status;

                                    if (bike_status == dock.LockStatus)
                                    {
                                        // alert: have bike but unlock or dont have bike but lock
                                        bikeAlarm = true;
                                        var notification = new SystemNotification(dock.Id, SystemNotification.EObjectType.Dock, SystemNotification.EStatus.Error, SystemNotification.EType.Alarm, SystemNotification.EContentType.Bike_Lock_Mismatch);
                                        await _systemNotificationRepository.AddAsync(notification);
                                        await _systemNotificationRepository.Commit();
                                    }

                                    var bike = await _bikeRepository.FindByEtag(bike_etag);
                                    if (bike != null)
                                    {
                                        if ((bike_status && dock.CurrentBikeId <= 0)
                                            || (bike_status && dock.CurrentBikeId != bike.Id)
                                            || (!bike_status && dock.CurrentBikeId > 0))
                                        {

                                            // alert: not consistent 
                                            bikeAlarm = true;
                                            bikeAlarmId = bike.Id;
                                            var notification = new SystemNotification(dock.Id, SystemNotification.EObjectType.Dock, SystemNotification.EStatus.Error, SystemNotification.EType.Alarm, SystemNotification.EContentType.Bike_Status_Mismatch);
                                            await _systemNotificationRepository.AddAsync(notification);
                                            await _systemNotificationRepository.Commit();
                                        }
                                    }
                                    else
                                    {
                                        // alert: bike etag invalid
                                        bikeAlarm = true;
                                        var notification = new SystemNotification(dock.Id, SystemNotification.EObjectType.Dock, SystemNotification.EStatus.Error, SystemNotification.EType.Alarm, SystemNotification.EContentType.Bike_Etag_Invalid);
                                        await _systemNotificationRepository.AddAsync(notification);
                                        await _systemNotificationRepository.Commit();
                                    }

                                    dock.BikeAlarm = bikeAlarm;
                                    dock.BikeAlarmId = bikeAlarmId;
                                }

                                _dockRepository.Update(dock);
                                await _dockRepository.Commit();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.Log.Error("StationService_UpdateStationStatus", ex.ToString());
                    }

                }

                string[] otherComps = command.OtherStatusStr.Split('|');
                if (otherComps.Length >= 3)
                {
                    // update station status
                    int electric_status = Convert.ToInt16(otherComps[0]);
                    float ups_voltage = Convert.ToSingle(otherComps[1]);
                    float battery_voltage = Convert.ToSingle(otherComps[2]);

                    if (electric_status != 0)
                    {
                        station.ErrorStatus = true;
                        // alert
                        var notification = new SystemNotification(station.Id, SystemNotification.EObjectType.Station, SystemNotification.EStatus.Error, SystemNotification.EType.Alarm);
                        await _systemNotificationRepository.AddAsync(notification);
                        await _systemNotificationRepository.Commit();
                    }
                    else
                    {
                        station.ErrorStatus = false;
                        station.UpsVoltage = ups_voltage;
                        station.BatteryVoltage = battery_voltage;
                    }
                }

                _stationRepository.Update(station);
                await _stationRepository.Commit();

                success = true;
            }

            return success;
        }
    }
}
