﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Services
{
    public interface IFeedbackService : IService<Feedback>
    {

        Task<FeedbackDTO> Register(FeedbackRegisterCommand model);
        Task<FeedbackDTO> Profile(int id);
    }

    public class FeedbackService : IFeedbackService
    {
        private readonly IFeedbackRepository _FeedbackRepository;

        public FeedbackService(IFeedbackRepository FeedbackRepository)
        {
            _FeedbackRepository = FeedbackRepository;
        }
        public virtual async Task<FeedbackDTO> Register(FeedbackRegisterCommand model)
        {

            // Thêm thông tin Feedback
            var dataAdd = new Feedback
            {
                AccountId = model.AccountId,
                StatusId = Feedback.FBStatus.UnRead,
                CreatedDate = DateTime.Now,
                Message = model.Message,
                Rating = model.Rating
            };
            await _FeedbackRepository.AddAsync(dataAdd);
            await _FeedbackRepository.Commit();
            Utility.Log.Info("Feedback_Register", $"{dataAdd.AccountId} - {true}");

            return new FeedbackDTO(dataAdd);
        }

        public virtual async Task<FeedbackDTO> Profile(int id)
        {
            var account = await _FeedbackRepository.SearchOneAsync(m => m.Id == id);
            if (account != null)
            {
                return new FeedbackDTO(account);
            }
            else
            {
                return null;
            }
        }
    }
}
