﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TN.API.Model.Setting;
using TN.Utility;
using TN.API.Model;
namespace TN.API.Services
{
    public interface IStationSocketService
    {
        Task<bool> CallStationApi(string imei, int dock_number, int command);
        Task<DeviceDTO> CallUnlockDevice(string id);
    }

    public class StationSocketService : IStationSocketService
    {
        private readonly IOptions<BaseSetting> _baseSetting;

        public StationSocketService(IOptions<BaseSetting> baseSetting)
        {
            _baseSetting = baseSetting;
        }

        public async Task<bool> CallStationApi(string imei, int dock_number, int command)
        {
            int retry = 0;
            bool success = false;

            StationSocketApi api = new StationSocketApi(_baseSetting.Value.ApiStationSocket, _baseSetting.Value.ApiRequestTimeout);
            do
            {
                try
                {
                    success = await api.RequestControlStation(imei, dock_number, command);
                }
                catch
                {
                    await Task.Delay(100).ConfigureAwait(false);
                }
                finally
                {
                    retry++;
                }
            }
            while (!success && retry < _baseSetting.Value.ApiNumberRetry) ;

            return success;
        }

        public virtual async Task<DeviceDTO> CallUnlockDevice(string id)
        {
            using (var client = new HttpClient())
            {
                StationSocketApi api = new StationSocketApi(_baseSetting.Value.ApiUnlockDevice, _baseSetting.Value.ApiRequestTimeout);
                var data = await api.RequestUnlockDevice(id);
                return data;
            }
        }
    }

    public class StationSocketApi : RestApi
    {
        public StationSocketApi(string baseAddress, int timeout) : base(baseAddress, timeout)
        {

        }
        public Task<bool> RequestControlStation(string imei, int dock_number, int command, CancellationToken ctoken = default(CancellationToken))
          => SendRequestAsync<bool>(HttpMethod.Post, $"/{imei}/{dock_number}/control-dock/{command}", ctoken);

        public async Task<DeviceDTO> RequestUnlockDevice(string id, CancellationToken ctoken = default(CancellationToken))
        {
            return await SendRequestAsync<DeviceDTO>(HttpMethod.Post, $"/{id}", ctoken);
        }
    }
}
