﻿using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model.Setting;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Services
{
    public interface INotificationService : IService<Notification>
    {
        Task<bool> PushControlDockNotification(int accountid, Notification notification, string token_security);
        Task<bool> UpdateMobileInfo(int accountid, string token, string token_security);
        Task<bool> TestMobileNotification(int accountid, string token_security);
    }

    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly IOptions<BaseSetting> _baseSetting;

        private readonly IMediator _mediator;

        public NotificationService(INotificationRepository notificationRepository,
                                IOptions<BaseSetting> baseSetting,
                                IMediator mediator)
        {
            _notificationRepository = notificationRepository;
            _baseSetting = baseSetting;
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public virtual async Task<bool> PushControlDockNotification(int accountid, Notification notification, string security)
        {
            if (accountid <= 0)
                return false;

            await _notificationRepository.AddAsync(notification);
            await _notificationRepository.Commit();

            // Send request to Notification Service
            return await CallNotificationApi(accountid, notification.Message, JsonConvert.SerializeObject(notification), security);
        }

        public virtual async Task<bool> TestMobileNotification(int accountid, string security)
        {
            if (accountid <= 0)
                return false;

            // Send request to Notification Service
            return await CallNotificationApi(accountid, "Mobike notification testing", "Mobike notification testing", security);
        }

        public virtual async Task<bool> UpdateMobileInfo(int accountid, string token, string token_security)
        {
            if (accountid <= 0)
                return false;

            int retry = 0;
            bool success = false;

            NotifcationApi api = new NotifcationApi(_baseSetting.Value.ApiNotification, _baseSetting.Value.ApiRequestTimeout);
            do
            {
                try
                {
                    var result = await api.UpdateMobileInfo(new MobileInfo(_baseSetting.Value.ApiNotificationSysCode, accountid.ToString(), token, token_security));
                    if (result != null && result.status)
                        success = true;
                }
                catch
                {
                    await Task.Delay(100).ConfigureAwait(false);
                }
                finally
                {
                    retry++;
                }
            }
            while (!success && retry < _baseSetting.Value.ApiNumberRetry);

            return success;
        }

        private async Task<bool> CallNotificationApi(int accountid, string message, string data, string security)
        {
            int retry = 0;
            bool success = false;

            NotifcationApi api = new NotifcationApi(_baseSetting.Value.ApiNotification, _baseSetting.Value.ApiRequestTimeout);
            do
            {
                try
                {
                    var result = await api.PushNotification(new MobileNotification(_baseSetting.Value.ApiNotificationSysCode, accountid.ToString(), message, data, security));
                    if (result != null && result.status)
                        success = true;
                }
                catch
                {
                    await Task.Delay(100).ConfigureAwait(false);
                }
                finally
                {
                    retry++;
                }
            }
            while (!success && retry < _baseSetting.Value.ApiNumberRetry);

            return success;
        }
    }

    public class NotifcationApi : RestApi
    {
        public NotifcationApi(string baseAddress, int timeout) : base(baseAddress, timeout)
        {
        }

        public Task<MobileNotificationResponse> UpdateMobileInfo(MobileInfo info, CancellationToken ctoken = default(CancellationToken))
          => SendRequestAsync<MobileNotificationResponse>(HttpMethod.Post, $"/apimobiletoken", info, ctoken);

        public Task<MobileNotificationResponse> PushNotification(MobileNotification notification, CancellationToken ctoken = default(CancellationToken))
          => SendRequestAsync<MobileNotificationResponse>(HttpMethod.Post, $"/apimobile", notification, ctoken);
    }

    public class MobileNotification
    {
        public string syscode { get; set; }
        public string id { get; set; }
        public string cmd { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
        public long timestamp { get; set; }
        public string token_security { get; set; }


        public MobileNotification(string syscode, string id, string message, string data, string token_security)
        {
            this.syscode = syscode;
            this.id = id;
            this.cmd = "notify";
            this.msg = message;
            this.data = data;
            this.token_security = token_security;
            this.timestamp = Function.ConvertToUnixTime(DateTime.Now);
        }
    }

    public class MobileInfo
    {
        public string syscode { get; set; }
        public string id { get; set; }
        public string token { get; set; }
        public string token_security { get; set; }
        public long timestamp { get; set; }

        public MobileInfo(string syscode, string id, string token, string token_security)
        {
            this.syscode = syscode;
            this.id = id;
            this.token = token;
            this.token_security = token_security;
            this.timestamp = Function.ConvertToUnixTime(DateTime.Now);
        }
    }

    public class MobileNotificationResponse
    {
        public bool status { get; set; }
        public string error { get; set; }

    }
}
