﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TN.API.Infrastructure.Interfaces;
using TN.API.Model;
using TN.API.Model.Setting;
using TN.Domain.Model;
using TN.Utility;

namespace TN.API.Services
{
    public interface IBikeService : IService<Bike>
    {
        Task<int> UpdateRentId(BikeUpdateRentIdCommand model);
        string getAppSetting(string setting);
        Task<int> UpdateDockStatus(DockErrorCommand model);
        Task<int> UpdateDockError(string key);
    }
   
    public class BikeService : IBikeService
    {
        private readonly IBikeRepository _BikeRepository;
        private readonly IDockRepository _DockRepository;
        public IConfigurationRoot Configuration { get; }

        public BikeService(IBikeRepository BikeRepository, IDockRepository DockRepository)
        {
            _BikeRepository = BikeRepository;
            _DockRepository = DockRepository;
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public virtual async Task<int> UpdateRentId(BikeUpdateRentIdCommand model)
        {
            var kttt = await _BikeRepository.SearchOneAsync(m => m.Id == model.Id);
            if (kttt == null)
            {
                return 0;
            }
            kttt.RentId = model.RentId;
            _BikeRepository.Update(kttt);
            await _BikeRepository.Commit();
            Utility.Log.Info("BikeService_UpdateRentId", $"{kttt.Id} - {true}");
            return 1;
        }
        public string getAppSetting(string setting)
        {
            string key = "KeySetting:" + setting;
            string Config = Configuration[key];
            return Config;
        }
        public virtual async Task<int> UpdateDockStatus(DockErrorCommand model)
        {
            var kttt = await _DockRepository.SearchOneAsync(m => m.Id == model.Id);
            if (kttt == null)
            {
                return 0;
            }
            kttt.Status = model.Status;
            _DockRepository.Update(kttt);
            await _DockRepository.Commit();
            Utility.Log.Info("BikeService_UpdateDocStatus", $"{kttt.Id} - {true}");
            return 1;
        }
        public virtual async Task<int> UpdateDockError(string key)
        {
            var kttt = await _DockRepository.SearchOneAsync(m => m.SerialNumber == key);
            if (kttt == null)
            {
                return 0;
            }
            kttt.Status = Dock.EStatus.Error;
            _DockRepository.Update(kttt);
            await _DockRepository.Commit();
            Utility.Log.Info("BikeService_UpdateDocErr", $"{kttt.Id} - {true}");
            return 1;
        }
    }
}