﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TN.API.HubListen
{
    public class DeviceHub : Hub
    {
        public async Task SendMessage(string data)
        {
            await Clients.All.SendAsync("DeviceReceiveMessage", data);
        }
    }
}
