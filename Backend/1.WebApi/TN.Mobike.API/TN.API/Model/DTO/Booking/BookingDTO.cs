﻿using System;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class BookingDTO
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public Booking.BookingStatus StatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Vote { get; set; }
        public string Messenger { get; set; }

        public BookingDTO(Booking booking)
        {
            Id = booking.Id;
            AccountId = booking.AccountId;
            StatusId = booking.StatusId;
            StartDate = booking.StartDate;
            EndDate = booking.EndDate;
            CreatedDate = booking.CreatedDate;
            Vote = booking.Vote;
            Messenger = booking.Messenger;
        }
    }
}