﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class StationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int TotalDock { get; set; }
        public int ErrorDock { get; set; }
        public int TotalBike { get; set; }
        public int BookedBike { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Distance { get; set; }

        public virtual List<DockDTO> Docks { get; set; }

        public StationDTO(Station station)
        {
            if (station == null)
                return;

            Id = station.Id;
            Name = station.Name;
            Address = station.Address;
            TotalDock = station.TotalDock;
            ErrorDock = station.ErrorDock;
            TotalBike = station.TotalBike;
            BookedBike = station.BookedBike;
            Lat = station.Lat;
            Lng = station.Lng;
            Distance = 0;
            Docks = new List<DockDTO>();
        }

        public StationDTO(Station station, int distance)
        {
            if (station == null)
                return;

            Id = station.Id;
            Name = station.Name;
            Address = station.Address;
            TotalDock = station.TotalDock;
            ErrorDock = station.ErrorDock;
            TotalBike = station.TotalBike;
            BookedBike = station.BookedBike;
            Lat = station.Lat;
            Lng = station.Lng;
            Distance = distance;
            Docks = new List<DockDTO>();
        }
    }

    public class DockDTO
    {
        public int Id { get; set; }
        public int OrderNumber { get; set; }
        public Dock.EStatus Status { get; set; }
        public bool LockStatus { get; set; }
        public BikeDTO CurrentBike { get; set; }

        public DockDTO(Dock dock)
        {
            if (dock == null)
                return;

            Id = dock.Id;
            OrderNumber = dock.OrderNumber;
            Status = dock.Status;
            LockStatus = dock.LockStatus;
        }
    }

    public class BikeDTO
    {
        public int Id { get; set; }
        public string Etag { get; set; }
        public Bike.EStatus Status { get; set; }
        public Bike.EType Type { get; set; }

        public BikeDTO(Bike bike)
        {
            if (bike == null)
                return;

            Id = bike.Id;
            Etag = bike.Etag;
            Status = bike.Status;
            Type = bike.Type;
        }
    }
}
