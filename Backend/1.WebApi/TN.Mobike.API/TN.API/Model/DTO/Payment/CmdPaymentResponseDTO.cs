﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.API.Model
{
    public class CmdPaymentResponseDTO
    {
        public int Status { get; set; }
        public string Error { get; set; }
        public object Data { get; set; }
    }
}
