﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TN.API.Model.Setting
{
    public class BaseSetting
    {
        
        public string ApiStationSocket { get; set; }
        public string ApiUnlockDevice { get; set; }
        public string ApiNotification { get; set; }
        public string ApiNotificationSysCode { get; set; }
        public int ApiNumberRetry { get; set; }
        public int ApiRequestTimeout { get; set; }
        public string SecurityKey { get; set; }
        public int JwtTokenExpiredMinute { get; set; }
        public string MobikeServiceId { get; set; }
        public string FolderMobikeAvatarWeb { get; set; }
        public string FolderMobikeAvatarServer { get; set; }

        public int OTPInvalidMax { get; set; }
        public int OTPRegiterExpiredMinute { get; set; }
        public int OTPLoginExpiredMinute { get; set; }
        public int ZipAvatarWidth { get; set; }

        public string AddressAPIPayment { get; set; }
        public string SmartContract { get; set; }
        public string MerchantCode { get; set; }

    }
}