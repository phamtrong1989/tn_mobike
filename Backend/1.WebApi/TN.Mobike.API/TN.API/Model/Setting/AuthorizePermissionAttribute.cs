﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using TN.Domain.Model;

namespace TN.API
{
    public class Authorize2Attribute : TypeFilterAttribute
    {
        public Authorize2Attribute(string action = null, string controller = null, string area = null)
            : base(typeof(PermissionFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class PermissionFilter : Attribute, IAsyncAuthorizationFilter
    {
        public PermissionFilter()
        {
        }
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            // context.Result = new ForbidResult();
            context.Result = new UnauthorizedResult();
            return;
        }
    }
}
