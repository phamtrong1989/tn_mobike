﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class BookingUpdateVoteCommand
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int Vote { get; set; }
        [Required]
        public string Messenger { get; set; }

    }
}
