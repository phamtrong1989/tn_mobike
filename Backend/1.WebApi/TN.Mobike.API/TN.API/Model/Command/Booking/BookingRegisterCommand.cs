﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class BookingRegisterCommand
    {
        [Required]
        public int AccountId { get; set; }
        public Booking.BookingStatus StatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Vote { get; set; }
    }
}
