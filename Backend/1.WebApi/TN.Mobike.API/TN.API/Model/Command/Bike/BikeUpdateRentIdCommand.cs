﻿using System;
using System.ComponentModel.DataAnnotations;
using TN.Domain.Model;
using static TN.Domain.Model.Account;

namespace TN.API.Model
{
    public class BikeUpdateRentIdCommand
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public Bike.ERent RentId { get; set; }
    }
}
