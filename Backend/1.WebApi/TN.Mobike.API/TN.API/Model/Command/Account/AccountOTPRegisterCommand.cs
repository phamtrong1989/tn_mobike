﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountOTPRegisterCommand
    {
        [Required]
        public string Phone { get; set; }
    }
}
