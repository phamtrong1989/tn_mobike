﻿using System;
using System.ComponentModel.DataAnnotations;
using static TN.Domain.Model.Account;

namespace TN.API.Model
{
    public class AccountUpdatePointCommand
    {
        [Required]
        public int Id { get; set; }
        public double Distance { get; set; }
    }
}
