﻿using System.ComponentModel.DataAnnotations;

namespace TN.API.Model
{
    public class AccountLogoutCommand
    {
        [Required]
        public string Username { get; set; }
    }
}
