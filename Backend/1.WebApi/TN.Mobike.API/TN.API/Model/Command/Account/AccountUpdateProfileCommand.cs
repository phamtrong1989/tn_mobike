﻿using System;
using System.ComponentModel.DataAnnotations;
using static TN.Domain.Model.Account;

namespace TN.API.Model
{
    public class AccountUpdateProfileCommand
    {
        [Required]
        public int Id { get; set; }
        public string Email { get; set; }
        [Required]
        public ESex Sex { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public DateTime Birthday { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public byte[] AvatarData { get; set; }
    }
}
