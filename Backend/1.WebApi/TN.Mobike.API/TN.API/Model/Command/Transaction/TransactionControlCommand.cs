﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class TransactionControlCommand
    {
        public string DockImei { get; set; }
        public int TicketId { get; set; }
        public int TransactionId { get; set; }
        public StationControlRequest.EType Command { get; set; }
        public int BookingId { get; set; }
    }
}
