﻿namespace TN.API.Model
{
    public class TransactionHistoryGetByAccountCommand
    {
        public int AccountId { get; set; }
        public int TypeId { get; set; }
    }
}
