﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TN.Domain.Model;

namespace TN.API.Model
{
    public class ControlDockNotificationCommand
    {
        public int StationId { get; set; }
        public int DockNumber { get; set; }
        public StationControlRequest.EType Command { get; set; }
    }
}
