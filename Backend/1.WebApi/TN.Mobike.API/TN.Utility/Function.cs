﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using log4net;
using System.Collections;
using System.Reflection;
using System.Xml;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Drawing;

namespace TN.Utility
{
    public class EntityExtention<T> where T:class
    {
        public static Func<IQueryable<T>, IOrderedQueryable<T>> OrderBy(Func<IQueryable<T>, IOrderedQueryable<T>> a)
        {
            return a;
        }
    }
    public class Function
    {
        public static bool ThumbnailCallback()
        {
            return false;
        }
        public static bool ReducedImage(Stream stream, int Width, string SaveMap)
        {
            try
            {
                int NewWidth;
                int NewHeight;
                Image image = System.Drawing.Image.FromStream(stream);
                int BaseWidth = image.Width;
                int BaseHeight = image.Height;


                double dblCoef = (double)Width / (double)BaseWidth;
                NewWidth = Convert.ToInt32(dblCoef * BaseWidth);
                NewHeight = Convert.ToInt32(dblCoef * BaseHeight);

                Image ReducedImage;
                Image.GetThumbnailImageAbort callb = new Image.GetThumbnailImageAbort(ThumbnailCallback);
                ReducedImage = image.GetThumbnailImage(NewWidth, NewHeight, callb, IntPtr.Zero);
                ReducedImage.Save(SaveMap, System.Drawing.Imaging.ImageFormat.Jpeg);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static byte[] GetTestImg(string fileName)
        {
            byte[] buff = null;
            FileStream fs = new FileStream(fileName,
                                           FileMode.Open,
                                           FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numBytes = new FileInfo(fileName).Length;
            buff = br.ReadBytes((int)numBytes);
            return buff;
        }
        public static bool ByteArrayToImage(byte[] byteArrayIn,string path,int width)
        {
            try
            {
                if(byteArrayIn.Length==0)
                {
                    return false;
                }
                using (var ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length))
                {
              
                    if(width<=0)
                    {
                        ms.Write(byteArrayIn, 0, byteArrayIn.Length);
                        var img = Image.FromStream(ms, true);
                        img.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    else
                    {
                        ReducedImage(ms, width, path);
                    }
                    return true;
                }
            }
            catch(Exception) {
                return false;
            }
        }
        public static string HMACMD5Password(string password)
        {
            using (var md5 = MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.ASCII.GetBytes(password));
                var strResult = BitConverter.ToString(result);
                return strResult.Replace("-", "").Substring(5);
            }
        }

        public static string FormatReturnUrl(string returnUrl, string urlDefault)
        {
            try
            {
                return string.IsNullOrEmpty(returnUrl) ? urlDefault : returnUrl.ToString();
            }
            catch
            {
                return urlDefault;
            }
        }
       
        public static string ToJson(object data)
        {
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            };
            var json = JsonConvert.SerializeObject(data, serializerSettings);
            return json;
        }
        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return (long)(datetime - sTime).TotalSeconds;
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string GetBitStr(byte[] data)
        {
            BitArray bits = new BitArray(data);

            string strByte = string.Empty;
            for (int i = 0; i <= bits.Count - 1; i++)
            {
                if (i % 8 == 0)
                {
                    strByte += " ";
                }
                strByte += (bits[i] ? "1" : "0");
            }

            return strByte;
        }

        public static bool ValidateIPv4(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }


        public static double DistanceInMeter(double lat1, double lon1, double lat2, double lon2)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;

            // meter
            dist = dist * 1.609344 * 1000;

            return (dist);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
    }
}
